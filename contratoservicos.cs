/*
               File: ContratoServicos
        Description: Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:49.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICOGRUPO_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvSERVICOGRUPO_CODIGO0X34( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
         {
            AV14ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvSERVICO_CODIGO0X34( AV14ServicoGrupo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSUBSERVICO_CODIGO") == 0 )
         {
            AV17Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvSUBSERVICO_CODIGO0X34( AV17Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATOSERVICOS_UNIDADECONTRATADA") == 0 )
         {
            AV27Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATOSERVICOS_UNIDADECONTRATADA0X34( AV27Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"SERVICO_RESPONSAVEL") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASASERVICO_RESPONSAVEL0X34( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel9"+"_"+"SERVICO_VINCULADOS") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX9ASASERVICO_VINCULADOS0X34( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_69") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_69( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_72") == 0 )
         {
            A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_72( A157ServicoGrupo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_71") == 0 )
         {
            A1212ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_71( A1212ContratoServicos_UnidadeContratada) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_70") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_70( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_73") == 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_73( A39Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_74") == 0 )
         {
            A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_74( A40Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
         {
            nRC_GXsfl_312 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_312_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_312_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGrid_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
               AV27Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynavServicogrupo_codigo.Name = "vSERVICOGRUPO_CODIGO";
         dynavServicogrupo_codigo.WebTags = "";
         dynavServico_codigo.Name = "vSERVICO_CODIGO";
         dynavServico_codigo.WebTags = "";
         dynavSubservico_codigo.Name = "vSUBSERVICO_CODIGO";
         dynavSubservico_codigo.WebTags = "";
         cmbContratoServicos_PrazoTpDias.Name = "CONTRATOSERVICOS_PRAZOTPDIAS";
         cmbContratoServicos_PrazoTpDias.WebTags = "";
         cmbContratoServicos_PrazoTpDias.addItem("", "(Nenhum)", 0);
         cmbContratoServicos_PrazoTpDias.addItem("U", "�teis", 0);
         cmbContratoServicos_PrazoTpDias.addItem("C", "Corridos", 0);
         if ( cmbContratoServicos_PrazoTpDias.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) )
            {
               A1454ContratoServicos_PrazoTpDias = "U";
               n1454ContratoServicos_PrazoTpDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
            }
            A1454ContratoServicos_PrazoTpDias = cmbContratoServicos_PrazoTpDias.getValidValue(A1454ContratoServicos_PrazoTpDias);
            n1454ContratoServicos_PrazoTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
         }
         cmbContratoServicos_PrazoInicio.Name = "CONTRATOSERVICOS_PRAZOINICIO";
         cmbContratoServicos_PrazoInicio.WebTags = "";
         cmbContratoServicos_PrazoInicio.addItem("1", "Dia seguinte", 0);
         cmbContratoServicos_PrazoInicio.addItem("2", "Dia �til seguinte", 0);
         cmbContratoServicos_PrazoInicio.addItem("3", "Imediato", 0);
         if ( cmbContratoServicos_PrazoInicio.ItemCount > 0 )
         {
            if ( (0==A1649ContratoServicos_PrazoInicio) )
            {
               A1649ContratoServicos_PrazoInicio = 1;
               n1649ContratoServicos_PrazoInicio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
            }
            A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cmbContratoServicos_PrazoInicio.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0))), "."));
            n1649ContratoServicos_PrazoInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
         }
         cmbContratoServicos_PrazoTipo.Name = "CONTRATOSERVICOS_PRAZOTIPO";
         cmbContratoServicos_PrazoTipo.WebTags = "";
         cmbContratoServicos_PrazoTipo.addItem("A", "Acumulado", 0);
         cmbContratoServicos_PrazoTipo.addItem("C", "Complexidade", 0);
         cmbContratoServicos_PrazoTipo.addItem("S", "Solicitado", 0);
         cmbContratoServicos_PrazoTipo.addItem("F", "Fixo", 0);
         cmbContratoServicos_PrazoTipo.addItem("E", "El�stico", 0);
         cmbContratoServicos_PrazoTipo.addItem("V", "Vari�vel", 0);
         cmbContratoServicos_PrazoTipo.addItem("P", "Progress�o Aritm�tica", 0);
         cmbContratoServicos_PrazoTipo.addItem("O", "Combinado", 0);
         if ( cmbContratoServicos_PrazoTipo.ItemCount > 0 )
         {
            A913ContratoServicos_PrazoTipo = cmbContratoServicos_PrazoTipo.getValidValue(A913ContratoServicos_PrazoTipo);
            n913ContratoServicos_PrazoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
         }
         cmbContratoServicos_PrazoCorrecaoTipo.Name = "CONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         cmbContratoServicos_PrazoCorrecaoTipo.WebTags = "";
         cmbContratoServicos_PrazoCorrecaoTipo.addItem("", "(Nenhum)", 0);
         cmbContratoServicos_PrazoCorrecaoTipo.addItem("F", "Fixo", 0);
         cmbContratoServicos_PrazoCorrecaoTipo.addItem("I", "Igual ao prazo de execu��o", 0);
         cmbContratoServicos_PrazoCorrecaoTipo.addItem("P", "Percentual do prazo de execu��o", 0);
         if ( cmbContratoServicos_PrazoCorrecaoTipo.ItemCount > 0 )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = cmbContratoServicos_PrazoCorrecaoTipo.getValidValue(A1225ContratoServicos_PrazoCorrecaoTipo);
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
         }
         cmbContratoServicos_StatusPagFnc.Name = "CONTRATOSERVICOS_STATUSPAGFNC";
         cmbContratoServicos_StatusPagFnc.WebTags = "";
         cmbContratoServicos_StatusPagFnc.addItem("", "(Nenhum)", 0);
         cmbContratoServicos_StatusPagFnc.addItem("B", "Stand by", 0);
         cmbContratoServicos_StatusPagFnc.addItem("S", "Solicitada", 0);
         cmbContratoServicos_StatusPagFnc.addItem("E", "Em An�lise", 0);
         cmbContratoServicos_StatusPagFnc.addItem("A", "Em execu��o", 0);
         cmbContratoServicos_StatusPagFnc.addItem("R", "Resolvida", 0);
         cmbContratoServicos_StatusPagFnc.addItem("C", "Conferida", 0);
         cmbContratoServicos_StatusPagFnc.addItem("D", "Retornada", 0);
         cmbContratoServicos_StatusPagFnc.addItem("H", "Homologada", 0);
         cmbContratoServicos_StatusPagFnc.addItem("O", "Aceite", 0);
         cmbContratoServicos_StatusPagFnc.addItem("P", "A Pagar", 0);
         cmbContratoServicos_StatusPagFnc.addItem("L", "Liquidada", 0);
         cmbContratoServicos_StatusPagFnc.addItem("X", "Cancelada", 0);
         cmbContratoServicos_StatusPagFnc.addItem("N", "N�o Faturada", 0);
         cmbContratoServicos_StatusPagFnc.addItem("J", "Planejamento", 0);
         cmbContratoServicos_StatusPagFnc.addItem("I", "An�lise Planejamento", 0);
         cmbContratoServicos_StatusPagFnc.addItem("T", "Validacao T�cnica", 0);
         cmbContratoServicos_StatusPagFnc.addItem("Q", "Validacao Qualidade", 0);
         cmbContratoServicos_StatusPagFnc.addItem("G", "Em Homologa��o", 0);
         cmbContratoServicos_StatusPagFnc.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratoServicos_StatusPagFnc.addItem("U", "Rascunho", 0);
         if ( cmbContratoServicos_StatusPagFnc.ItemCount > 0 )
         {
            A1325ContratoServicos_StatusPagFnc = cmbContratoServicos_StatusPagFnc.getValidValue(A1325ContratoServicos_StatusPagFnc);
            n1325ContratoServicos_StatusPagFnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
         }
         chkContratoServicos_NaoRequerAtr.Name = "CONTRATOSERVICOS_NAOREQUERATR";
         chkContratoServicos_NaoRequerAtr.WebTags = "";
         chkContratoServicos_NaoRequerAtr.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_NaoRequerAtr_Internalname, "TitleCaption", chkContratoServicos_NaoRequerAtr.Caption);
         chkContratoServicos_NaoRequerAtr.CheckedValue = "false";
         cmbContratoServicos_LocalExec.Name = "CONTRATOSERVICOS_LOCALEXEC";
         cmbContratoServicos_LocalExec.WebTags = "";
         cmbContratoServicos_LocalExec.addItem("A", "Contratada", 0);
         cmbContratoServicos_LocalExec.addItem("E", "Contratante", 0);
         cmbContratoServicos_LocalExec.addItem("O", "Opcional", 0);
         if ( cmbContratoServicos_LocalExec.ItemCount > 0 )
         {
            A639ContratoServicos_LocalExec = cmbContratoServicos_LocalExec.getValidValue(A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
         }
         dynContratoServicos_UnidadeContratada.Name = "CONTRATOSERVICOS_UNIDADECONTRATADA";
         dynContratoServicos_UnidadeContratada.WebTags = "";
         cmbServicoContrato_Faturamento.Name = "SERVICOCONTRATO_FATURAMENTO";
         cmbServicoContrato_Faturamento.WebTags = "";
         cmbServicoContrato_Faturamento.addItem("B", "Bruto", 0);
         cmbServicoContrato_Faturamento.addItem("L", "Liquido", 0);
         if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A607ServicoContrato_Faturamento)) )
            {
               A607ServicoContrato_Faturamento = "B";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
            }
            A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
         }
         cmbContratoServicos_CalculoRmn.Name = "CONTRATOSERVICOS_CALCULORMN";
         cmbContratoServicos_CalculoRmn.WebTags = "";
         cmbContratoServicos_CalculoRmn.addItem("0", "Configurado", 0);
         cmbContratoServicos_CalculoRmn.addItem("1", "Vari�vel", 0);
         if ( cmbContratoServicos_CalculoRmn.ItemCount > 0 )
         {
            A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cmbContratoServicos_CalculoRmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0))), "."));
            n2074ContratoServicos_CalculoRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
         }
         chkContratoServicos_EspelhaAceite.Name = "CONTRATOSERVICOS_ESPELHAACEITE";
         chkContratoServicos_EspelhaAceite.WebTags = "";
         chkContratoServicos_EspelhaAceite.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "TitleCaption", chkContratoServicos_EspelhaAceite.Caption);
         chkContratoServicos_EspelhaAceite.CheckedValue = "false";
         cmbContratoServicos_Momento.Name = "CONTRATOSERVICOS_MOMENTO";
         cmbContratoServicos_Momento.WebTags = "";
         cmbContratoServicos_Momento.addItem("", "(Nenhum)", 0);
         cmbContratoServicos_Momento.addItem("I", "Inicial", 0);
         cmbContratoServicos_Momento.addItem("F", "Final", 0);
         if ( cmbContratoServicos_Momento.ItemCount > 0 )
         {
            A1266ContratoServicos_Momento = cmbContratoServicos_Momento.getValidValue(A1266ContratoServicos_Momento);
            n1266ContratoServicos_Momento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
         }
         cmbContratoServicos_TipoVnc.Name = "CONTRATOSERVICOS_TIPOVNC";
         cmbContratoServicos_TipoVnc.WebTags = "";
         cmbContratoServicos_TipoVnc.addItem("", "N/A", 0);
         cmbContratoServicos_TipoVnc.addItem("D", "D�bito/Cr�dito", 0);
         cmbContratoServicos_TipoVnc.addItem("C", "Compara��o", 0);
         cmbContratoServicos_TipoVnc.addItem("A", "D�bito/Cr�dito e Compara��o", 0);
         if ( cmbContratoServicos_TipoVnc.ItemCount > 0 )
         {
            A868ContratoServicos_TipoVnc = cmbContratoServicos_TipoVnc.getValidValue(A868ContratoServicos_TipoVnc);
            n868ContratoServicos_TipoVnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
         }
         chkContratoServicos_SolicitaGestorSistema.Name = "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         chkContratoServicos_SolicitaGestorSistema.WebTags = "";
         chkContratoServicos_SolicitaGestorSistema.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_SolicitaGestorSistema_Internalname, "TitleCaption", chkContratoServicos_SolicitaGestorSistema.Caption);
         chkContratoServicos_SolicitaGestorSistema.CheckedValue = "false";
         chkContratoServicos_Ativo.Name = "CONTRATOSERVICOS_ATIVO";
         chkContratoServicos_Ativo.WebTags = "";
         chkContratoServicos_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_Ativo_Internalname, "TitleCaption", chkContratoServicos_Ativo.Caption);
         chkContratoServicos_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Servicos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicos_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicos_Codigo = aP1_ContratoServicos_Codigo;
         this.AV27Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.AV27Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavServicogrupo_codigo = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         dynavSubservico_codigo = new GXCombobox();
         cmbContratoServicos_PrazoTpDias = new GXCombobox();
         cmbContratoServicos_PrazoInicio = new GXCombobox();
         cmbContratoServicos_PrazoTipo = new GXCombobox();
         cmbContratoServicos_PrazoCorrecaoTipo = new GXCombobox();
         cmbContratoServicos_StatusPagFnc = new GXCombobox();
         chkContratoServicos_NaoRequerAtr = new GXCheckbox();
         cmbContratoServicos_LocalExec = new GXCombobox();
         dynContratoServicos_UnidadeContratada = new GXCombobox();
         cmbServicoContrato_Faturamento = new GXCombobox();
         cmbContratoServicos_CalculoRmn = new GXCombobox();
         chkContratoServicos_EspelhaAceite = new GXCheckbox();
         cmbContratoServicos_Momento = new GXCombobox();
         cmbContratoServicos_TipoVnc = new GXCombobox();
         chkContratoServicos_SolicitaGestorSistema = new GXCheckbox();
         chkContratoServicos_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavServicogrupo_codigo.ItemCount > 0 )
         {
            AV14ServicoGrupo_Codigo = (int)(NumberUtil.Val( dynavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV17Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
         }
         if ( dynavSubservico_codigo.ItemCount > 0 )
         {
            AV15SubServico_Codigo = (int)(NumberUtil.Val( dynavSubservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15SubServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0)));
         }
         if ( cmbContratoServicos_PrazoTpDias.ItemCount > 0 )
         {
            A1454ContratoServicos_PrazoTpDias = cmbContratoServicos_PrazoTpDias.getValidValue(A1454ContratoServicos_PrazoTpDias);
            n1454ContratoServicos_PrazoTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
         }
         if ( cmbContratoServicos_PrazoInicio.ItemCount > 0 )
         {
            A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cmbContratoServicos_PrazoInicio.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0))), "."));
            n1649ContratoServicos_PrazoInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
         }
         if ( cmbContratoServicos_PrazoTipo.ItemCount > 0 )
         {
            A913ContratoServicos_PrazoTipo = cmbContratoServicos_PrazoTipo.getValidValue(A913ContratoServicos_PrazoTipo);
            n913ContratoServicos_PrazoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
         }
         if ( cmbContratoServicos_PrazoCorrecaoTipo.ItemCount > 0 )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = cmbContratoServicos_PrazoCorrecaoTipo.getValidValue(A1225ContratoServicos_PrazoCorrecaoTipo);
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
         }
         if ( cmbContratoServicos_StatusPagFnc.ItemCount > 0 )
         {
            A1325ContratoServicos_StatusPagFnc = cmbContratoServicos_StatusPagFnc.getValidValue(A1325ContratoServicos_StatusPagFnc);
            n1325ContratoServicos_StatusPagFnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
         }
         if ( cmbContratoServicos_LocalExec.ItemCount > 0 )
         {
            A639ContratoServicos_LocalExec = cmbContratoServicos_LocalExec.getValidValue(A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
         }
         if ( dynContratoServicos_UnidadeContratada.ItemCount > 0 )
         {
            A1212ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( dynContratoServicos_UnidadeContratada.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
         }
         if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
         {
            A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
         }
         if ( cmbContratoServicos_CalculoRmn.ItemCount > 0 )
         {
            A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cmbContratoServicos_CalculoRmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0))), "."));
            n2074ContratoServicos_CalculoRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
         }
         if ( cmbContratoServicos_Momento.ItemCount > 0 )
         {
            A1266ContratoServicos_Momento = cmbContratoServicos_Momento.getValidValue(A1266ContratoServicos_Momento);
            n1266ContratoServicos_Momento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
         }
         if ( cmbContratoServicos_TipoVnc.ItemCount > 0 )
         {
            A868ContratoServicos_TipoVnc = cmbContratoServicos_TipoVnc.getValidValue(A868ContratoServicos_TipoVnc);
            n868ContratoServicos_TipoVnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 327,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,327);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, edtServico_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicos.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_319_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table3_319_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0X34e( true) ;
         }
         else
         {
            wb_table1_2_0X34e( false) ;
         }
      }

      protected void wb_table3_319_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 322,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 324,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 326,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_319_0X34e( true) ;
         }
         else
         {
            wb_table3_319_0X34e( false) ;
         }
      }

      protected void wb_table2_5_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleServico"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Servi�o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Servico"+"\" style=\"display:none;\">") ;
            wb_table4_14_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table4_14_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleRemuneracao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Remunera��o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Remuneracao"+"\" style=\"display:none;\">") ;
            wb_table5_309_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table5_309_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0X34e( true) ;
         }
         else
         {
            wb_table2_5_0X34e( false) ;
         }
      }

      protected void wb_table5_309_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.AddObjectProperty("GridName", "Grid");
            GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("CmpContext", "");
            GridContainer.AddObjectProperty("InMasterPage", "false");
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Inicio_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Fim_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Valor_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
            GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
            GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            nGXsfl_312_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount228 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_228 = 1;
                  ScanStart0X228( ) ;
                  while ( RcdFound228 != 0 )
                  {
                     init_level_properties228( ) ;
                     getByPrimaryKey0X228( ) ;
                     AddRow0X228( ) ;
                     ScanNext0X228( ) ;
                  }
                  ScanEnd0X228( ) ;
                  nBlankRcdCount228 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               B638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               B557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
               standaloneNotModal0X228( ) ;
               standaloneModal0X228( ) ;
               sMode228 = Gx_mode;
               while ( nGXsfl_312_idx < nRC_GXsfl_312 )
               {
                  ReadRow0X228( ) ;
                  edtContratoServicosRmn_Sequencial_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0)));
                  edtContratoServicosRmn_Inicio_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Inicio_Enabled), 5, 0)));
                  edtContratoServicosRmn_Fim_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Fim_Enabled), 5, 0)));
                  edtContratoServicosRmn_Valor_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Valor_Enabled), 5, 0)));
                  if ( ( nRcdExists_228 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal0X228( ) ;
                  }
                  SendRow0X228( ) ;
               }
               Gx_mode = sMode228;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A2073ContratoServicos_QtdRmn = B2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               A638ContratoServicos_Ativo = B638ContratoServicos_Ativo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               A557Servico_VlrUnidadeContratada = B557Servico_VlrUnidadeContratada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount228 = 5;
               nRcdExists_228 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart0X228( ) ;
                  while ( RcdFound228 != 0 )
                  {
                     sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_312228( ) ;
                     init_level_properties228( ) ;
                     standaloneNotModal0X228( ) ;
                     getByPrimaryKey0X228( ) ;
                     standaloneModal0X228( ) ;
                     AddRow0X228( ) ;
                     ScanNext0X228( ) ;
                  }
                  ScanEnd0X228( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode228 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_312228( ) ;
               InitAll0X228( ) ;
               init_level_properties228( ) ;
               B2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               B638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               B557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
               standaloneNotModal0X228( ) ;
               standaloneModal0X228( ) ;
               nRcdExists_228 = 0;
               nIsMod_228 = 0;
               nRcdDeleted_228 = 0;
               nBlankRcdCount228 = (short)(nBlankRcdUsr228+nBlankRcdCount228);
               fRowAdded = 0;
               while ( nBlankRcdCount228 > 0 )
               {
                  AddRow0X228( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtContratoServicosRmn_Inicio_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount228 = (short)(nBlankRcdCount228-1);
               }
               Gx_mode = sMode228;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A2073ContratoServicos_QtdRmn = B2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               A638ContratoServicos_Ativo = B638ContratoServicos_Ativo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               A557Servico_VlrUnidadeContratada = B557Servico_VlrUnidadeContratada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_309_0X34e( true) ;
         }
         else
         {
            wb_table5_309_0X34e( false) ;
         }
      }

      protected void wb_table4_14_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableprincipal_Internalname, tblTableprincipal_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup2_Internalname, "Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicos.htm");
            wb_table6_18_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table6_18_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup3_Internalname, "Servi�o", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicos.htm");
            wb_table7_55_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table7_55_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "Prazos", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicos.htm");
            wb_table8_83_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table8_83_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup5_Internalname, "Par�metros", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoServicos.htm");
            wb_table9_184_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table9_184_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_0X34e( true) ;
         }
         else
         {
            wb_table4_14_0X34e( false) ;
         }
      }

      protected void wb_table9_184_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblParametros_Internalname, tblParametros_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_indicedivergencia_Internalname, "Indice de Aceita��o", "", "", lblTextblockcontratoservicos_indicedivergencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table10_189_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table10_189_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_naorequeratr_Internalname, "N�o requer atribui��o", "", "", lblTextblockcontratoservicos_naorequeratr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 198,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_NaoRequerAtr_Internalname, StringUtil.BoolToStr( A1397ContratoServicos_NaoRequerAtr), "", "", 1, chkContratoServicos_NaoRequerAtr.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(198, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,198);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_limiteproposta_Internalname, "Limite das Propostas", "", "", lblTextblockcontratoservicos_limiteproposta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table11_202_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table11_202_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_localexec_Internalname, "Local de Execu��o", "", "", lblTextblockcontratoservicos_localexec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 212,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_LocalExec, cmbContratoServicos_LocalExec_Internalname, StringUtil.RTrim( A639ContratoServicos_LocalExec), 1, cmbContratoServicos_LocalExec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicos_LocalExec.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,212);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_LocalExec.CurrentValue = StringUtil.RTrim( A639ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_LocalExec_Internalname, "Values", (String)(cmbContratoServicos_LocalExec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_unidadecontratada_Internalname, "Unidade Contratada", "", "", lblTextblockcontratoservicos_unidadecontratada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 216,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratoServicos_UnidadeContratada, dynContratoServicos_UnidadeContratada_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)), 1, dynContratoServicos_UnidadeContratada_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratoServicos_UnidadeContratada.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,216);\"", "", true, "HLP_ContratoServicos.htm");
            dynContratoServicos_UnidadeContratada.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicos_UnidadeContratada_Internalname, "Values", (String)(dynContratoServicos_UnidadeContratada.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_fatorcnvundcnt_Internalname, "Fator de convers�o em horas", "", "", lblTextblockcontratoservicos_fatorcnvundcnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 220,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_FatorCnvUndCnt_Internalname, StringUtil.LTrim( StringUtil.NToC( A1341ContratoServicos_FatorCnvUndCnt, 9, 4, ",", "")), ((edtContratoServicos_FatorCnvUndCnt_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1341ContratoServicos_FatorCnvUndCnt, "ZZZ9.9999")) : context.localUtil.Format( A1341ContratoServicos_FatorCnvUndCnt, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,220);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_FatorCnvUndCnt_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_FatorCnvUndCnt_Enabled, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_qtdcontratada_Internalname, "Qtde. Contratada", "", "", lblTextblockservico_qtdcontratada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 225,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_QtdContratada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A555Servico_QtdContratada), 13, 0, ",", "")), ((edtServico_QtdContratada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")) : context.localUtil.Format( (decimal)(A555Servico_QtdContratada), "Z,ZZZ,ZZZ,ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,225);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_QtdContratada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_QtdContratada_Enabled, 0, "text", "", 80, "px", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_vlrunidadecontratada_Internalname, "Valor da Unidade", "", "", lblTextblockservico_vlrunidadecontratada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 229,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_VlrUnidadeContratada_Internalname, StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ",", "")), ((edtServico_VlrUnidadeContratada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A557Servico_VlrUnidadeContratada, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,229);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_VlrUnidadeContratada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_VlrUnidadeContratada_Enabled, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_produtividade_Internalname, "Produtividade", "", "", lblTextblockcontratoservicos_produtividade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table12_233_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table12_233_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicocontrato_faturamento_Internalname, "Faturamento", "", "", lblTextblockservicocontrato_faturamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 243,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServicoContrato_Faturamento, cmbServicoContrato_Faturamento_Internalname, StringUtil.RTrim( A607ServicoContrato_Faturamento), 1, cmbServicoContrato_Faturamento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "svchar", "", 1, cmbServicoContrato_Faturamento.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,243);\"", "", true, "HLP_ContratoServicos.htm");
            cmbServicoContrato_Faturamento.CurrentValue = StringUtil.RTrim( A607ServicoContrato_Faturamento);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServicoContrato_Faturamento_Internalname, "Values", (String)(cmbServicoContrato_Faturamento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percentual_Internalname, "Valor do Pagamento", "", "", lblTextblockservico_percentual_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table13_247_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table13_247_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_calculormn_Internalname, "C�lculo da Remunera��o", "", "", lblTextblockcontratoservicos_calculormn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 256,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_CalculoRmn, cmbContratoServicos_CalculoRmn_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)), 1, cmbContratoServicos_CalculoRmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoServicos_CalculoRmn.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,256);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_CalculoRmn.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_CalculoRmn_Internalname, "Values", (String)(cmbContratoServicos_CalculoRmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_perccnc_Internalname, "Valor do Cancelamento", "", "", lblTextblockcontratoservicos_perccnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table14_261_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table14_261_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_espelhaaceite_Internalname, "Espelhar Aceite na FS", "", "", lblTextblockcontratoservicos_espelhaaceite_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratoservicos_espelhaaceite_Visible, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 275,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_EspelhaAceite_Internalname, StringUtil.BoolToStr( A1217ContratoServicos_EspelhaAceite), "", "", chkContratoServicos_EspelhaAceite.Visible, chkContratoServicos_EspelhaAceite.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(275, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,275);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_momento_Internalname, "Momento", "", "", lblTextblockcontratoservicos_momento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 279,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_Momento, cmbContratoServicos_Momento_Internalname, StringUtil.RTrim( A1266ContratoServicos_Momento), 1, cmbContratoServicos_Momento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicos_Momento.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,279);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_Momento.CurrentValue = StringUtil.RTrim( A1266ContratoServicos_Momento);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_Momento_Internalname, "Values", (String)(cmbContratoServicos_Momento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tipovnc_Internalname, "Tipo de Vinculo", "", "", lblTextblockcontratoservicos_tipovnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 283,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_TipoVnc, cmbContratoServicos_TipoVnc_Internalname, StringUtil.RTrim( A868ContratoServicos_TipoVnc), 1, cmbContratoServicos_TipoVnc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicos_TipoVnc.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,283);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_TipoVnc.CurrentValue = StringUtil.RTrim( A868ContratoServicos_TipoVnc);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_TipoVnc_Internalname, "Values", (String)(cmbContratoServicos_TipoVnc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_qntuntcns_Internalname, "Qtde. Unit. de Consumo", "", "", lblTextblockcontratoservicos_qntuntcns_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 288,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_QntUntCns_Internalname, StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ",", "")), ((edtContratoServicos_QntUntCns_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")) : context.localUtil.Format( A1340ContratoServicos_QntUntCns, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,288);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_QntUntCns_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_QntUntCns_Enabled, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_codigofiscal_Internalname, "C�digo Fiscal", "", "", lblTextblockcontratoservicos_codigofiscal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 292,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_CodigoFiscal_Internalname, StringUtil.RTrim( A1723ContratoServicos_CodigoFiscal), StringUtil.RTrim( context.localUtil.Format( A1723ContratoServicos_CodigoFiscal, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,292);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_CodigoFiscal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_CodigoFiscal_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "CodigoFiscal", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_solicitagestorsistema_Internalname, "Exibir Gestor Respons�vel pelo Sistema", "", "", lblTextblockcontratoservicos_solicitagestorsistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table15_296_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table15_296_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_ativo_Internalname, "Ativo?", "", "", lblTextblockcontratoservicos_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratoservicos_ativo_Visible, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 306,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_Ativo_Internalname, StringUtil.BoolToStr( A638ContratoServicos_Ativo), "", "", chkContratoServicos_Ativo.Visible, chkContratoServicos_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(306, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,306);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_184_0X34e( true) ;
         }
         else
         {
            wb_table9_184_0X34e( false) ;
         }
      }

      protected void wb_table15_296_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_solicitagestorsistema_Internalname, tblTablemergedcontratoservicos_solicitagestorsistema_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 299,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicos_SolicitaGestorSistema_Internalname, StringUtil.BoolToStr( A2094ContratoServicos_SolicitaGestorSistema), "", "", 1, chkContratoServicos_SolicitaGestorSistema.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(299, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,299);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image BootstrapTooltipRight";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgContratoservicos_solicitagestorsistema_infoicon_Internalname, context.GetImagePath( "2e4f3057-53d8-4ff3-81df-57002c73064d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgContratoservicos_solicitagestorsistema_infoicon_Visible, 1, "", "Exibe Gestor Respons�vel pelo Sistema ao cadastrar uma OS", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_296_0X34e( true) ;
         }
         else
         {
            wb_table15_296_0X34e( false) ;
         }
      }

      protected void wb_table14_261_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_perccnc_Internalname, tblTablemergedcontratoservicos_perccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 264,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PercCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1539ContratoServicos_PercCnc), 4, 0, ",", "")), ((edtContratoServicos_PercCnc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1539ContratoServicos_PercCnc), "ZZZ9")) : context.localUtil.Format( (decimal)(A1539ContratoServicos_PercCnc), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,264);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PercCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_PercCnc_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_perccnc_righttext_Internalname, "�%", "", "", lblContratoservicos_perccnc_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_261_0X34e( true) ;
         }
         else
         {
            wb_table14_261_0X34e( false) ;
         }
      }

      protected void wb_table13_247_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_percentual_Internalname, tblTablemergedservico_percentual_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 250,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21Servico_Percentual, 7, 3, ",", "")), ((edtavServico_percentual_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV21Servico_Percentual, "ZZ9.999")) : context.localUtil.Format( AV21Servico_Percentual, "ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','3');"+";gx.evt.onblur(this,250);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_percentual_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServico_percentual_Enabled, 0, "text", "", 60, "px", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_percentual_righttext_Internalname, "%", "", "", lblServico_percentual_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_247_0X34e( true) ;
         }
         else
         {
            wb_table13_247_0X34e( false) ;
         }
      }

      protected void wb_table12_233_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_produtividade_Internalname, tblTablemergedcontratoservicos_produtividade_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( A1191ContratoServicos_Produtividade, 14, 5, ",", "")), ((edtContratoServicos_Produtividade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1191ContratoServicos_Produtividade, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1191ContratoServicos_Produtividade, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,236);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Produtividade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_Produtividade_Enabled, 0, "text", "", 70, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_produtividade_righttext_Internalname, lblContratoservicos_produtividade_righttext_Caption, "", "", lblContratoservicos_produtividade_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_233_0X34e( true) ;
         }
         else
         {
            wb_table12_233_0X34e( false) ;
         }
      }

      protected void wb_table11_202_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_limiteproposta_Internalname, tblTablemergedcontratoservicos_limiteproposta_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 205,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_LimiteProposta_Internalname, StringUtil.LTrim( StringUtil.NToC( A1799ContratoServicos_LimiteProposta, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( A1799ContratoServicos_LimiteProposta, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,205);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_LimiteProposta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_LimiteProposta_Enabled, 1, "text", "", 60, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_limiteproposta_righttext_Internalname, lblContratoservicos_limiteproposta_righttext_Caption, "", "", lblContratoservicos_limiteproposta_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_202_0X34e( true) ;
         }
         else
         {
            wb_table11_202_0X34e( false) ;
         }
      }

      protected void wb_table10_189_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_indicedivergencia_Internalname, tblTablemergedcontratoservicos_indicedivergencia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 192,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_IndiceDivergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A1455ContratoServicos_IndiceDivergencia, 6, 2, ",", "")), ((edtContratoServicos_IndiceDivergencia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1455ContratoServicos_IndiceDivergencia, "ZZ9.99")) : context.localUtil.Format( A1455ContratoServicos_IndiceDivergencia, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,192);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_IndiceDivergencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_IndiceDivergencia_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_indicedivergencia_righttext_Internalname, "�% (0 aceita tudo)", "", "", lblContratoservicos_indicedivergencia_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_189_0X34e( true) ;
         }
         else
         {
            wb_table10_189_0X34e( false) ;
         }
      }

      protected void wb_table8_83_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPrazos_Internalname, tblPrazos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazotpdias_Internalname, "Tipo de dias", "", "", lblTextblockcontratoservicos_prazotpdias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoTpDias, cmbContratoServicos_PrazoTpDias_Internalname, StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias), 1, cmbContratoServicos_PrazoTpDias_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicos_PrazoTpDias.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_PrazoTpDias.CurrentValue = StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoTpDias_Internalname, "Values", (String)(cmbContratoServicos_PrazoTpDias.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoinicio_Internalname, "Inicio", "", "", lblTextblockcontratoservicos_prazoinicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoInicio, cmbContratoServicos_PrazoInicio_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)), 1, cmbContratoServicos_PrazoInicio_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoServicos_PrazoInicio.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_PrazoInicio.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoInicio_Internalname, "Values", (String)(cmbContratoServicos_PrazoInicio.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoanalise_Internalname, "����������������������An�lise", "", "", lblTextblockcontratoservicos_prazoanalise_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoAnalise_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0, ",", "")), ((edtContratoServicos_PrazoAnalise_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")) : context.localUtil.Format( (decimal)(A1152ContratoServicos_PrazoAnalise), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoAnalise_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_PrazoAnalise_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazotipo_Internalname, "Execu��o", "", "", lblTextblockcontratoservicos_prazotipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table16_103_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table16_103_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoresposta_Internalname, "������������������Resposta", "", "", lblTextblockcontratoservicos_prazoresposta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoResposta_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0, ",", "")), ((edtContratoServicos_PrazoResposta_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1153ContratoServicos_PrazoResposta), "ZZZ9")) : context.localUtil.Format( (decimal)(A1153ContratoServicos_PrazoResposta), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "Tramita��es", "", edtContratoServicos_PrazoResposta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_PrazoResposta_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazogarantia_Internalname, "Garantia", "", "", lblTextblockcontratoservicos_prazogarantia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoGarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0, ",", "")), ((edtContratoServicos_PrazoGarantia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1181ContratoServicos_PrazoGarantia), "ZZZ9")) : context.localUtil.Format( (decimal)(A1181ContratoServicos_PrazoGarantia), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoGarantia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_PrazoGarantia_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazoatendegarantia_Internalname, "������������Execu��o da Gtia.", "", "", lblTextblockcontratoservicos_prazoatendegarantia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoAtendeGarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0, ",", "")), ((edtContratoServicos_PrazoAtendeGarantia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), "ZZZ9")) : context.localUtil.Format( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoAtendeGarantia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_PrazoAtendeGarantia_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazocorrecaotipo_Internalname, "Corre��o", "", "", lblTextblockcontratoservicos_prazocorrecaotipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table17_132_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table17_132_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tmpestanl_Internalname, "Estimado de an�lise", "", "", lblTextblockcontratoservicos_tmpestanl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table18_146_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table18_146_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tmpestexc_Internalname, "Estimado de execu��o", "", "", lblTextblockcontratoservicos_tmpestexc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table19_155_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table19_155_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_tmpestcrr_Internalname, "Estimado de corre��o", "", "", lblTextblockcontratoservicos_tmpestcrr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table20_164_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table20_164_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_statuspagfnc_Internalname, "Pagar em", "", "", lblTextblockcontratoservicos_statuspagfnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_StatusPagFnc, cmbContratoServicos_StatusPagFnc_Internalname, StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc), 1, cmbContratoServicos_StatusPagFnc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicos_StatusPagFnc.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,176);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_StatusPagFnc.CurrentValue = StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_StatusPagFnc_Internalname, "Values", (String)(cmbContratoServicos_StatusPagFnc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_83_0X34e( true) ;
         }
         else
         {
            wb_table8_83_0X34e( false) ;
         }
      }

      protected void wb_table20_164_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_tmpestcrr_Internalname, tblTablemergedcontratoservicos_tmpestcrr_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_TmpEstCrr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0, ",", "")), ((edtContratoServicos_TmpEstCrr_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1502ContratoServicos_TmpEstCrr), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1502ContratoServicos_TmpEstCrr), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,167);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_TmpEstCrr_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_TmpEstCrr_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_tmpestcrr_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_tmpestcrr_righttext_Internalname, " �min.����", "", "", lblContratoservicos_tmpestcrr_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnreplicar_Internalname, "", "Replicar tempos estimados", bttBtnreplicar_Jsonclick, 5, "Replicar tempos estimados", "", StyleString, ClassString, bttBtnreplicar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOREPLICAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table20_164_0X34e( true) ;
         }
         else
         {
            wb_table20_164_0X34e( false) ;
         }
      }

      protected void wb_table19_155_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_tmpestexc_Internalname, tblTablemergedcontratoservicos_tmpestexc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_TmpEstExc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0, ",", "")), ((edtContratoServicos_TmpEstExc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1501ContratoServicos_TmpEstExc), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1501ContratoServicos_TmpEstExc), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,158);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_TmpEstExc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_TmpEstExc_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_tmpestexc_righttext_Internalname, "�min.", "", "", lblContratoservicos_tmpestexc_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table19_155_0X34e( true) ;
         }
         else
         {
            wb_table19_155_0X34e( false) ;
         }
      }

      protected void wb_table18_146_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_tmpestanl_Internalname, tblTablemergedcontratoservicos_tmpestanl_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_TmpEstAnl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0, ",", "")), ((edtContratoServicos_TmpEstAnl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1516ContratoServicos_TmpEstAnl), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1516ContratoServicos_TmpEstAnl), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_TmpEstAnl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicos_TmpEstAnl_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_tmpestanl_righttext_Internalname, "�min.", "", "", lblContratoservicos_tmpestanl_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_146_0X34e( true) ;
         }
         else
         {
            wb_table18_146_0X34e( false) ;
         }
      }

      protected void wb_table17_132_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname, tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoCorrecaoTipo, cmbContratoServicos_PrazoCorrecaoTipo_Internalname, StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo), 1, cmbContratoServicos_PrazoCorrecaoTipo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e110x34_client"+"'", "char", "", 1, cmbContratoServicos_PrazoCorrecaoTipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoCorrecaoTipo_Internalname, "Values", (String)(cmbContratoServicos_PrazoCorrecaoTipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_prazocorrecao_Internalname, "", "", "", lblTextblockcontratoservicos_prazocorrecao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoCorrecao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0, ",", "")), ((edtContratoServicos_PrazoCorrecao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1224ContratoServicos_PrazoCorrecao), "ZZZ9")) : context.localUtil.Format( (decimal)(A1224ContratoServicos_PrazoCorrecao), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoCorrecao_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContratoServicos_PrazoCorrecao_Visible, edtContratoServicos_PrazoCorrecao_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_prazocorrecao_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_prazocorrecao_righttext_Internalname, lblContratoservicos_prazocorrecao_righttext_Caption, "", "", lblContratoservicos_prazocorrecao_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_132_0X34e( true) ;
         }
         else
         {
            wb_table17_132_0X34e( false) ;
         }
      }

      protected void wb_table16_103_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicos_prazotipo_Internalname, tblTablemergedcontratoservicos_prazotipo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicos_PrazoTipo, cmbContratoServicos_PrazoTipo_Internalname, StringUtil.RTrim( A913ContratoServicos_PrazoTipo), 1, cmbContratoServicos_PrazoTipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicos_PrazoTipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicos.htm");
            cmbContratoServicos_PrazoTipo.CurrentValue = StringUtil.RTrim( A913ContratoServicos_PrazoTipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoTipo_Internalname, "Values", (String)(cmbContratoServicos_PrazoTipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_prazodias_cell_Internalname+"\"  class='"+cellContratoservicos_prazodias_cell_Class+"'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_PrazoDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A914ContratoServicos_PrazoDias), 4, 0, ",", "")), ((edtContratoServicos_PrazoDias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A914ContratoServicos_PrazoDias), "ZZZ9")) : context.localUtil.Format( (decimal)(A914ContratoServicos_PrazoDias), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_PrazoDias_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContratoServicos_PrazoDias_Visible, edtContratoServicos_PrazoDias_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratoservicos_prazodias_righttext_cell_Internalname+"\"  class='"+cellContratoservicos_prazodias_righttext_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicos_prazodias_righttext_Internalname, "�dias", "", "", lblContratoservicos_prazodias_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table21_112_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table21_112_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_103_0X34e( true) ;
         }
         else
         {
            wb_table16_103_0X34e( false) ;
         }
      }

      protected void wb_table21_112_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbtnprzexc_Internalname, tblTblbtnprzexc_Internalname, "", "Table", 0, "", "", 2, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgUpdprazo_Internalname, context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgUpdprazo_Visible, imgUpdprazo_Enabled, "", imgUpdprazo_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgUpdprazo_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOUPDPRAZO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table21_112_0X34e( true) ;
         }
         else
         {
            wb_table21_112_0X34e( false) ;
         }
      }

      protected void wb_table7_55_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblGruposervico_Internalname, tblGruposervico_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_codigo_Internalname, "Grupo", "", "", lblTextblockservicogrupo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicogrupo_codigo, dynavServicogrupo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0)), 1, dynavServicogrupo_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavServicogrupo_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_ContratoServicos.htm");
            dynavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo_Internalname, "Values", (String)(dynavServicogrupo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, lblTextblockservico_codigo_Caption, "", "", lblTextblockservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICO_CODIGO.CLICK."+"'", "int", "", 1, dynavServico_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_ContratoServicos.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksubservico_codigo_Internalname, lblTextblocksubservico_codigo_Caption, "", "", lblTextblocksubservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblocksubservico_codigo_Visible, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSubservico_codigo, dynavSubservico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0)), 1, dynavSubservico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSUBSERVICO_CODIGO.CLICK."+"'", "int", "", dynavSubservico_codigo.Visible, dynavSubservico_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_ContratoServicos.htm");
            dynavSubservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSubservico_codigo_Internalname, "Values", (String)(dynavSubservico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavServico_nome_Internalname, StringUtil.RTrim( AV32Servico_Nome), StringUtil.RTrim( context.localUtil.Format( AV32Servico_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServico_nome_Enabled, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalias_Internalname, "Alias", "", "", lblTextblockalias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavAlias_Internalname, StringUtil.RTrim( AV30Alias), StringUtil.RTrim( context.localUtil.Format( AV30Alias, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavAlias_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_55_0X34e( true) ;
         }
         else
         {
            wb_table7_55_0X34e( false) ;
         }
      }

      protected void wb_table6_18_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table22_23_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table22_23_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table23_39_0X34( true) ;
         }
         return  ;
      }

      protected void wb_table23_39_0X34e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_valorunidadecontratacao_Internalname, "Valor da Unidade", "", "", lblTextblockcontrato_valorunidadecontratacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontrato_valorunidadecontratacao_Visible, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_ValorUnidadeContratacao_Internalname, StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")), ((edtContrato_ValorUnidadeContratacao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_ValorUnidadeContratacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContrato_ValorUnidadeContratacao_Visible, edtContrato_ValorUnidadeContratacao_Enabled, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_0X34e( true) ;
         }
         else
         {
            wb_table6_18_0X34e( false) ;
         }
      }

      protected void wb_table23_39_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table23_39_0X34e( true) ;
         }
         else
         {
            wb_table23_39_0X34e( false) ;
         }
      }

      protected void wb_table22_23_0X34( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table22_23_0X34e( true) ;
         }
         else
         {
            wb_table22_23_0X34e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E120X2 */
         E120X2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               A116Contrato_ValorUnidadeContratacao = context.localUtil.CToN( cgiGet( edtContrato_ValorUnidadeContratacao_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               dynavServicogrupo_codigo.Name = dynavServicogrupo_codigo_Internalname;
               dynavServicogrupo_codigo.CurrentValue = cgiGet( dynavServicogrupo_codigo_Internalname);
               AV14ServicoGrupo_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServicogrupo_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0)));
               dynavServico_codigo.Name = dynavServico_codigo_Internalname;
               dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
               AV17Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
               dynavSubservico_codigo.Name = dynavSubservico_codigo_Internalname;
               dynavSubservico_codigo.CurrentValue = cgiGet( dynavSubservico_codigo_Internalname);
               AV15SubServico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSubservico_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15SubServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0)));
               AV32Servico_Nome = StringUtil.Upper( cgiGet( edtavServico_nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
               AV30Alias = StringUtil.Upper( cgiGet( edtavAlias_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
               cmbContratoServicos_PrazoTpDias.Name = cmbContratoServicos_PrazoTpDias_Internalname;
               cmbContratoServicos_PrazoTpDias.CurrentValue = cgiGet( cmbContratoServicos_PrazoTpDias_Internalname);
               A1454ContratoServicos_PrazoTpDias = cgiGet( cmbContratoServicos_PrazoTpDias_Internalname);
               n1454ContratoServicos_PrazoTpDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
               n1454ContratoServicos_PrazoTpDias = (String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) ? true : false);
               cmbContratoServicos_PrazoInicio.Name = cmbContratoServicos_PrazoInicio_Internalname;
               cmbContratoServicos_PrazoInicio.CurrentValue = cgiGet( cmbContratoServicos_PrazoInicio_Internalname);
               A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cgiGet( cmbContratoServicos_PrazoInicio_Internalname), "."));
               n1649ContratoServicos_PrazoInicio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
               n1649ContratoServicos_PrazoInicio = ((0==A1649ContratoServicos_PrazoInicio) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAnalise_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAnalise_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PRAZOANALISE");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_PrazoAnalise_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1152ContratoServicos_PrazoAnalise = 0;
                  n1152ContratoServicos_PrazoAnalise = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
               }
               else
               {
                  A1152ContratoServicos_PrazoAnalise = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAnalise_Internalname), ",", "."));
                  n1152ContratoServicos_PrazoAnalise = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
               }
               n1152ContratoServicos_PrazoAnalise = ((0==A1152ContratoServicos_PrazoAnalise) ? true : false);
               cmbContratoServicos_PrazoTipo.Name = cmbContratoServicos_PrazoTipo_Internalname;
               cmbContratoServicos_PrazoTipo.CurrentValue = cgiGet( cmbContratoServicos_PrazoTipo_Internalname);
               A913ContratoServicos_PrazoTipo = cgiGet( cmbContratoServicos_PrazoTipo_Internalname);
               n913ContratoServicos_PrazoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
               A914ContratoServicos_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoDias_Internalname), ",", "."));
               n914ContratoServicos_PrazoDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoResposta_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoResposta_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PRAZORESPOSTA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_PrazoResposta_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1153ContratoServicos_PrazoResposta = 0;
                  n1153ContratoServicos_PrazoResposta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
               }
               else
               {
                  A1153ContratoServicos_PrazoResposta = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoResposta_Internalname), ",", "."));
                  n1153ContratoServicos_PrazoResposta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
               }
               n1153ContratoServicos_PrazoResposta = ((0==A1153ContratoServicos_PrazoResposta) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoGarantia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoGarantia_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PRAZOGARANTIA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_PrazoGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1181ContratoServicos_PrazoGarantia = 0;
                  n1181ContratoServicos_PrazoGarantia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
               }
               else
               {
                  A1181ContratoServicos_PrazoGarantia = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoGarantia_Internalname), ",", "."));
                  n1181ContratoServicos_PrazoGarantia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
               }
               n1181ContratoServicos_PrazoGarantia = ((0==A1181ContratoServicos_PrazoGarantia) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAtendeGarantia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAtendeGarantia_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PRAZOATENDEGARANTIA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_PrazoAtendeGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1182ContratoServicos_PrazoAtendeGarantia = 0;
                  n1182ContratoServicos_PrazoAtendeGarantia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
               }
               else
               {
                  A1182ContratoServicos_PrazoAtendeGarantia = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoAtendeGarantia_Internalname), ",", "."));
                  n1182ContratoServicos_PrazoAtendeGarantia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
               }
               n1182ContratoServicos_PrazoAtendeGarantia = ((0==A1182ContratoServicos_PrazoAtendeGarantia) ? true : false);
               cmbContratoServicos_PrazoCorrecaoTipo.Name = cmbContratoServicos_PrazoCorrecaoTipo_Internalname;
               cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = cgiGet( cmbContratoServicos_PrazoCorrecaoTipo_Internalname);
               A1225ContratoServicos_PrazoCorrecaoTipo = cgiGet( cmbContratoServicos_PrazoCorrecaoTipo_Internalname);
               n1225ContratoServicos_PrazoCorrecaoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
               n1225ContratoServicos_PrazoCorrecaoTipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoCorrecao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoCorrecao_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PRAZOCORRECAO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_PrazoCorrecao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1224ContratoServicos_PrazoCorrecao = 0;
                  n1224ContratoServicos_PrazoCorrecao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
               }
               else
               {
                  A1224ContratoServicos_PrazoCorrecao = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PrazoCorrecao_Internalname), ",", "."));
                  n1224ContratoServicos_PrazoCorrecao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
               }
               n1224ContratoServicos_PrazoCorrecao = ((0==A1224ContratoServicos_PrazoCorrecao) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstAnl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstAnl_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_TMPESTANL");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_TmpEstAnl_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1516ContratoServicos_TmpEstAnl = 0;
                  n1516ContratoServicos_TmpEstAnl = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
               }
               else
               {
                  A1516ContratoServicos_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstAnl_Internalname), ",", "."));
                  n1516ContratoServicos_TmpEstAnl = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
               }
               n1516ContratoServicos_TmpEstAnl = ((0==A1516ContratoServicos_TmpEstAnl) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstExc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstExc_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_TMPESTEXC");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_TmpEstExc_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1501ContratoServicos_TmpEstExc = 0;
                  n1501ContratoServicos_TmpEstExc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
               }
               else
               {
                  A1501ContratoServicos_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstExc_Internalname), ",", "."));
                  n1501ContratoServicos_TmpEstExc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
               }
               n1501ContratoServicos_TmpEstExc = ((0==A1501ContratoServicos_TmpEstExc) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstCrr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstCrr_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_TMPESTCRR");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_TmpEstCrr_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1502ContratoServicos_TmpEstCrr = 0;
                  n1502ContratoServicos_TmpEstCrr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
               }
               else
               {
                  A1502ContratoServicos_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_TmpEstCrr_Internalname), ",", "."));
                  n1502ContratoServicos_TmpEstCrr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
               }
               n1502ContratoServicos_TmpEstCrr = ((0==A1502ContratoServicos_TmpEstCrr) ? true : false);
               cmbContratoServicos_StatusPagFnc.Name = cmbContratoServicos_StatusPagFnc_Internalname;
               cmbContratoServicos_StatusPagFnc.CurrentValue = cgiGet( cmbContratoServicos_StatusPagFnc_Internalname);
               A1325ContratoServicos_StatusPagFnc = cgiGet( cmbContratoServicos_StatusPagFnc_Internalname);
               n1325ContratoServicos_StatusPagFnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
               n1325ContratoServicos_StatusPagFnc = (String.IsNullOrEmpty(StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_IndiceDivergencia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_IndiceDivergencia_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_INDICEDIVERGENCIA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_IndiceDivergencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1455ContratoServicos_IndiceDivergencia = 0;
                  n1455ContratoServicos_IndiceDivergencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
               }
               else
               {
                  A1455ContratoServicos_IndiceDivergencia = context.localUtil.CToN( cgiGet( edtContratoServicos_IndiceDivergencia_Internalname), ",", ".");
                  n1455ContratoServicos_IndiceDivergencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
               }
               n1455ContratoServicos_IndiceDivergencia = ((Convert.ToDecimal(0)==A1455ContratoServicos_IndiceDivergencia) ? true : false);
               A1397ContratoServicos_NaoRequerAtr = StringUtil.StrToBool( cgiGet( chkContratoServicos_NaoRequerAtr_Internalname));
               n1397ContratoServicos_NaoRequerAtr = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1397ContratoServicos_NaoRequerAtr", A1397ContratoServicos_NaoRequerAtr);
               n1397ContratoServicos_NaoRequerAtr = ((false==A1397ContratoServicos_NaoRequerAtr) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_LimiteProposta_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_LimiteProposta_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_LIMITEPROPOSTA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_LimiteProposta_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1799ContratoServicos_LimiteProposta = 0;
                  n1799ContratoServicos_LimiteProposta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
               }
               else
               {
                  A1799ContratoServicos_LimiteProposta = context.localUtil.CToN( cgiGet( edtContratoServicos_LimiteProposta_Internalname), ",", ".");
                  n1799ContratoServicos_LimiteProposta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
               }
               n1799ContratoServicos_LimiteProposta = ((Convert.ToDecimal(0)==A1799ContratoServicos_LimiteProposta) ? true : false);
               cmbContratoServicos_LocalExec.Name = cmbContratoServicos_LocalExec_Internalname;
               cmbContratoServicos_LocalExec.CurrentValue = cgiGet( cmbContratoServicos_LocalExec_Internalname);
               A639ContratoServicos_LocalExec = cgiGet( cmbContratoServicos_LocalExec_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
               dynContratoServicos_UnidadeContratada.Name = dynContratoServicos_UnidadeContratada_Internalname;
               dynContratoServicos_UnidadeContratada.CurrentValue = cgiGet( dynContratoServicos_UnidadeContratada_Internalname);
               A1212ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( cgiGet( dynContratoServicos_UnidadeContratada_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_FatorCnvUndCnt_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_FatorCnvUndCnt_Internalname), ",", ".") > 9999.9999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_FATORCNVUNDCNT");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_FatorCnvUndCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1341ContratoServicos_FatorCnvUndCnt = 0;
                  n1341ContratoServicos_FatorCnvUndCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
               }
               else
               {
                  A1341ContratoServicos_FatorCnvUndCnt = context.localUtil.CToN( cgiGet( edtContratoServicos_FatorCnvUndCnt_Internalname), ",", ".");
                  n1341ContratoServicos_FatorCnvUndCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
               }
               n1341ContratoServicos_FatorCnvUndCnt = ((Convert.ToDecimal(0)==A1341ContratoServicos_FatorCnvUndCnt) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_QtdContratada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_QtdContratada_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_QTDCONTRATADA");
                  AnyError = 1;
                  GX_FocusControl = edtServico_QtdContratada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A555Servico_QtdContratada = 0;
                  n555Servico_QtdContratada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
               }
               else
               {
                  A555Servico_QtdContratada = (long)(context.localUtil.CToN( cgiGet( edtServico_QtdContratada_Internalname), ",", "."));
                  n555Servico_QtdContratada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
               }
               n555Servico_QtdContratada = ((0==A555Servico_QtdContratada) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_VlrUnidadeContratada_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_VlrUnidadeContratada_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_VLRUNIDADECONTRATADA");
                  AnyError = 1;
                  GX_FocusControl = edtServico_VlrUnidadeContratada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A557Servico_VlrUnidadeContratada = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
               }
               else
               {
                  A557Servico_VlrUnidadeContratada = context.localUtil.CToN( cgiGet( edtServico_VlrUnidadeContratada_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Produtividade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Produtividade_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PRODUTIVIDADE");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Produtividade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1191ContratoServicos_Produtividade = 0;
                  n1191ContratoServicos_Produtividade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
               }
               else
               {
                  A1191ContratoServicos_Produtividade = context.localUtil.CToN( cgiGet( edtContratoServicos_Produtividade_Internalname), ",", ".");
                  n1191ContratoServicos_Produtividade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
               }
               n1191ContratoServicos_Produtividade = ((Convert.ToDecimal(0)==A1191ContratoServicos_Produtividade) ? true : false);
               cmbServicoContrato_Faturamento.Name = cmbServicoContrato_Faturamento_Internalname;
               cmbServicoContrato_Faturamento.CurrentValue = cgiGet( cmbServicoContrato_Faturamento_Internalname);
               A607ServicoContrato_Faturamento = cgiGet( cmbServicoContrato_Faturamento_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") > 999.999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCENTUAL");
                  AnyError = 1;
                  GX_FocusControl = edtavServico_percentual_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV21Servico_Percentual = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
               }
               else
               {
                  AV21Servico_Percentual = context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
               }
               cmbContratoServicos_CalculoRmn.Name = cmbContratoServicos_CalculoRmn_Internalname;
               cmbContratoServicos_CalculoRmn.CurrentValue = cgiGet( cmbContratoServicos_CalculoRmn_Internalname);
               A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cgiGet( cmbContratoServicos_CalculoRmn_Internalname), "."));
               n2074ContratoServicos_CalculoRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
               n2074ContratoServicos_CalculoRmn = ((0==A2074ContratoServicos_CalculoRmn) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PercCnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_PercCnc_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_PERCCNC");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_PercCnc_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1539ContratoServicos_PercCnc = 0;
                  n1539ContratoServicos_PercCnc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
               }
               else
               {
                  A1539ContratoServicos_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtContratoServicos_PercCnc_Internalname), ",", "."));
                  n1539ContratoServicos_PercCnc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
               }
               n1539ContratoServicos_PercCnc = ((0==A1539ContratoServicos_PercCnc) ? true : false);
               A1217ContratoServicos_EspelhaAceite = StringUtil.StrToBool( cgiGet( chkContratoServicos_EspelhaAceite_Internalname));
               n1217ContratoServicos_EspelhaAceite = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1217ContratoServicos_EspelhaAceite", A1217ContratoServicos_EspelhaAceite);
               n1217ContratoServicos_EspelhaAceite = ((false==A1217ContratoServicos_EspelhaAceite) ? true : false);
               cmbContratoServicos_Momento.Name = cmbContratoServicos_Momento_Internalname;
               cmbContratoServicos_Momento.CurrentValue = cgiGet( cmbContratoServicos_Momento_Internalname);
               A1266ContratoServicos_Momento = cgiGet( cmbContratoServicos_Momento_Internalname);
               n1266ContratoServicos_Momento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
               n1266ContratoServicos_Momento = (String.IsNullOrEmpty(StringUtil.RTrim( A1266ContratoServicos_Momento)) ? true : false);
               cmbContratoServicos_TipoVnc.Name = cmbContratoServicos_TipoVnc_Internalname;
               cmbContratoServicos_TipoVnc.CurrentValue = cgiGet( cmbContratoServicos_TipoVnc_Internalname);
               A868ContratoServicos_TipoVnc = cgiGet( cmbContratoServicos_TipoVnc_Internalname);
               n868ContratoServicos_TipoVnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
               n868ContratoServicos_TipoVnc = (String.IsNullOrEmpty(StringUtil.RTrim( A868ContratoServicos_TipoVnc)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_QntUntCns_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_QntUntCns_Internalname), ",", ".") > 9999.9999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_QNTUNTCNS");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_QntUntCns_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1340ContratoServicos_QntUntCns = 0;
                  n1340ContratoServicos_QntUntCns = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
               }
               else
               {
                  A1340ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( edtContratoServicos_QntUntCns_Internalname), ",", ".");
                  n1340ContratoServicos_QntUntCns = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
               }
               n1340ContratoServicos_QntUntCns = ((Convert.ToDecimal(0)==A1340ContratoServicos_QntUntCns) ? true : false);
               A1723ContratoServicos_CodigoFiscal = cgiGet( edtContratoServicos_CodigoFiscal_Internalname);
               n1723ContratoServicos_CodigoFiscal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1723ContratoServicos_CodigoFiscal", A1723ContratoServicos_CodigoFiscal);
               n1723ContratoServicos_CodigoFiscal = (String.IsNullOrEmpty(StringUtil.RTrim( A1723ContratoServicos_CodigoFiscal)) ? true : false);
               A2094ContratoServicos_SolicitaGestorSistema = StringUtil.StrToBool( cgiGet( chkContratoServicos_SolicitaGestorSistema_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
               A638ContratoServicos_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicos_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A155Servico_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               }
               else
               {
                  A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z160ContratoServicos_Codigo"), ",", "."));
               Z1225ContratoServicos_PrazoCorrecaoTipo = cgiGet( "Z1225ContratoServicos_PrazoCorrecaoTipo");
               n1225ContratoServicos_PrazoCorrecaoTipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) ? true : false);
               Z1858ContratoServicos_Alias = cgiGet( "Z1858ContratoServicos_Alias");
               n1858ContratoServicos_Alias = (String.IsNullOrEmpty(StringUtil.RTrim( A1858ContratoServicos_Alias)) ? true : false);
               Z555Servico_QtdContratada = (long)(context.localUtil.CToN( cgiGet( "Z555Servico_QtdContratada"), ",", "."));
               n555Servico_QtdContratada = ((0==A555Servico_QtdContratada) ? true : false);
               Z557Servico_VlrUnidadeContratada = context.localUtil.CToN( cgiGet( "Z557Servico_VlrUnidadeContratada"), ",", ".");
               Z558Servico_Percentual = context.localUtil.CToN( cgiGet( "Z558Servico_Percentual"), ",", ".");
               n558Servico_Percentual = ((Convert.ToDecimal(0)==A558Servico_Percentual) ? true : false);
               Z607ServicoContrato_Faturamento = cgiGet( "Z607ServicoContrato_Faturamento");
               Z639ContratoServicos_LocalExec = cgiGet( "Z639ContratoServicos_LocalExec");
               Z868ContratoServicos_TipoVnc = cgiGet( "Z868ContratoServicos_TipoVnc");
               n868ContratoServicos_TipoVnc = (String.IsNullOrEmpty(StringUtil.RTrim( A868ContratoServicos_TipoVnc)) ? true : false);
               Z888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cgiGet( "Z888ContratoServicos_HmlSemCnf"));
               n888ContratoServicos_HmlSemCnf = ((false==A888ContratoServicos_HmlSemCnf) ? true : false);
               Z1454ContratoServicos_PrazoTpDias = cgiGet( "Z1454ContratoServicos_PrazoTpDias");
               n1454ContratoServicos_PrazoTpDias = (String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) ? true : false);
               Z1152ContratoServicos_PrazoAnalise = (short)(context.localUtil.CToN( cgiGet( "Z1152ContratoServicos_PrazoAnalise"), ",", "."));
               n1152ContratoServicos_PrazoAnalise = ((0==A1152ContratoServicos_PrazoAnalise) ? true : false);
               Z1153ContratoServicos_PrazoResposta = (short)(context.localUtil.CToN( cgiGet( "Z1153ContratoServicos_PrazoResposta"), ",", "."));
               n1153ContratoServicos_PrazoResposta = ((0==A1153ContratoServicos_PrazoResposta) ? true : false);
               Z1181ContratoServicos_PrazoGarantia = (short)(context.localUtil.CToN( cgiGet( "Z1181ContratoServicos_PrazoGarantia"), ",", "."));
               n1181ContratoServicos_PrazoGarantia = ((0==A1181ContratoServicos_PrazoGarantia) ? true : false);
               Z1182ContratoServicos_PrazoAtendeGarantia = (short)(context.localUtil.CToN( cgiGet( "Z1182ContratoServicos_PrazoAtendeGarantia"), ",", "."));
               n1182ContratoServicos_PrazoAtendeGarantia = ((0==A1182ContratoServicos_PrazoAtendeGarantia) ? true : false);
               Z1224ContratoServicos_PrazoCorrecao = (short)(context.localUtil.CToN( cgiGet( "Z1224ContratoServicos_PrazoCorrecao"), ",", "."));
               n1224ContratoServicos_PrazoCorrecao = ((0==A1224ContratoServicos_PrazoCorrecao) ? true : false);
               Z1649ContratoServicos_PrazoInicio = (short)(context.localUtil.CToN( cgiGet( "Z1649ContratoServicos_PrazoInicio"), ",", "."));
               n1649ContratoServicos_PrazoInicio = ((0==A1649ContratoServicos_PrazoInicio) ? true : false);
               Z1190ContratoServicos_PrazoImediato = StringUtil.StrToBool( cgiGet( "Z1190ContratoServicos_PrazoImediato"));
               n1190ContratoServicos_PrazoImediato = ((false==A1190ContratoServicos_PrazoImediato) ? true : false);
               Z1191ContratoServicos_Produtividade = context.localUtil.CToN( cgiGet( "Z1191ContratoServicos_Produtividade"), ",", ".");
               n1191ContratoServicos_Produtividade = ((Convert.ToDecimal(0)==A1191ContratoServicos_Produtividade) ? true : false);
               Z1217ContratoServicos_EspelhaAceite = StringUtil.StrToBool( cgiGet( "Z1217ContratoServicos_EspelhaAceite"));
               n1217ContratoServicos_EspelhaAceite = ((false==A1217ContratoServicos_EspelhaAceite) ? true : false);
               Z1266ContratoServicos_Momento = cgiGet( "Z1266ContratoServicos_Momento");
               n1266ContratoServicos_Momento = (String.IsNullOrEmpty(StringUtil.RTrim( A1266ContratoServicos_Momento)) ? true : false);
               Z1325ContratoServicos_StatusPagFnc = cgiGet( "Z1325ContratoServicos_StatusPagFnc");
               n1325ContratoServicos_StatusPagFnc = (String.IsNullOrEmpty(StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc)) ? true : false);
               Z1340ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( "Z1340ContratoServicos_QntUntCns"), ",", ".");
               n1340ContratoServicos_QntUntCns = ((Convert.ToDecimal(0)==A1340ContratoServicos_QntUntCns) ? true : false);
               Z1341ContratoServicos_FatorCnvUndCnt = context.localUtil.CToN( cgiGet( "Z1341ContratoServicos_FatorCnvUndCnt"), ",", ".");
               n1341ContratoServicos_FatorCnvUndCnt = ((Convert.ToDecimal(0)==A1341ContratoServicos_FatorCnvUndCnt) ? true : false);
               Z1397ContratoServicos_NaoRequerAtr = StringUtil.StrToBool( cgiGet( "Z1397ContratoServicos_NaoRequerAtr"));
               n1397ContratoServicos_NaoRequerAtr = ((false==A1397ContratoServicos_NaoRequerAtr) ? true : false);
               Z1455ContratoServicos_IndiceDivergencia = context.localUtil.CToN( cgiGet( "Z1455ContratoServicos_IndiceDivergencia"), ",", ".");
               n1455ContratoServicos_IndiceDivergencia = ((Convert.ToDecimal(0)==A1455ContratoServicos_IndiceDivergencia) ? true : false);
               Z1516ContratoServicos_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( "Z1516ContratoServicos_TmpEstAnl"), ",", "."));
               n1516ContratoServicos_TmpEstAnl = ((0==A1516ContratoServicos_TmpEstAnl) ? true : false);
               Z1501ContratoServicos_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( "Z1501ContratoServicos_TmpEstExc"), ",", "."));
               n1501ContratoServicos_TmpEstExc = ((0==A1501ContratoServicos_TmpEstExc) ? true : false);
               Z1502ContratoServicos_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( "Z1502ContratoServicos_TmpEstCrr"), ",", "."));
               n1502ContratoServicos_TmpEstCrr = ((0==A1502ContratoServicos_TmpEstCrr) ? true : false);
               Z1531ContratoServicos_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "Z1531ContratoServicos_TipoHierarquia"), ",", "."));
               n1531ContratoServicos_TipoHierarquia = ((0==A1531ContratoServicos_TipoHierarquia) ? true : false);
               Z1537ContratoServicos_PercTmp = (short)(context.localUtil.CToN( cgiGet( "Z1537ContratoServicos_PercTmp"), ",", "."));
               n1537ContratoServicos_PercTmp = ((0==A1537ContratoServicos_PercTmp) ? true : false);
               Z1538ContratoServicos_PercPgm = (short)(context.localUtil.CToN( cgiGet( "Z1538ContratoServicos_PercPgm"), ",", "."));
               n1538ContratoServicos_PercPgm = ((0==A1538ContratoServicos_PercPgm) ? true : false);
               Z1539ContratoServicos_PercCnc = (short)(context.localUtil.CToN( cgiGet( "Z1539ContratoServicos_PercCnc"), ",", "."));
               n1539ContratoServicos_PercCnc = ((0==A1539ContratoServicos_PercCnc) ? true : false);
               Z638ContratoServicos_Ativo = StringUtil.StrToBool( cgiGet( "Z638ContratoServicos_Ativo"));
               Z1723ContratoServicos_CodigoFiscal = cgiGet( "Z1723ContratoServicos_CodigoFiscal");
               n1723ContratoServicos_CodigoFiscal = (String.IsNullOrEmpty(StringUtil.RTrim( A1723ContratoServicos_CodigoFiscal)) ? true : false);
               Z1799ContratoServicos_LimiteProposta = context.localUtil.CToN( cgiGet( "Z1799ContratoServicos_LimiteProposta"), ",", ".");
               n1799ContratoServicos_LimiteProposta = ((Convert.ToDecimal(0)==A1799ContratoServicos_LimiteProposta) ? true : false);
               Z2074ContratoServicos_CalculoRmn = (short)(context.localUtil.CToN( cgiGet( "Z2074ContratoServicos_CalculoRmn"), ",", "."));
               n2074ContratoServicos_CalculoRmn = ((0==A2074ContratoServicos_CalculoRmn) ? true : false);
               Z2094ContratoServicos_SolicitaGestorSistema = StringUtil.StrToBool( cgiGet( "Z2094ContratoServicos_SolicitaGestorSistema"));
               Z155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z155Servico_Codigo"), ",", "."));
               Z74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               Z1212ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( "Z1212ContratoServicos_UnidadeContratada"), ",", "."));
               A1858ContratoServicos_Alias = cgiGet( "Z1858ContratoServicos_Alias");
               n1858ContratoServicos_Alias = false;
               n1858ContratoServicos_Alias = (String.IsNullOrEmpty(StringUtil.RTrim( A1858ContratoServicos_Alias)) ? true : false);
               A558Servico_Percentual = context.localUtil.CToN( cgiGet( "Z558Servico_Percentual"), ",", ".");
               n558Servico_Percentual = false;
               n558Servico_Percentual = ((Convert.ToDecimal(0)==A558Servico_Percentual) ? true : false);
               A888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cgiGet( "Z888ContratoServicos_HmlSemCnf"));
               n888ContratoServicos_HmlSemCnf = false;
               n888ContratoServicos_HmlSemCnf = ((false==A888ContratoServicos_HmlSemCnf) ? true : false);
               A1190ContratoServicos_PrazoImediato = StringUtil.StrToBool( cgiGet( "Z1190ContratoServicos_PrazoImediato"));
               n1190ContratoServicos_PrazoImediato = false;
               n1190ContratoServicos_PrazoImediato = ((false==A1190ContratoServicos_PrazoImediato) ? true : false);
               A1531ContratoServicos_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "Z1531ContratoServicos_TipoHierarquia"), ",", "."));
               n1531ContratoServicos_TipoHierarquia = false;
               n1531ContratoServicos_TipoHierarquia = ((0==A1531ContratoServicos_TipoHierarquia) ? true : false);
               A1537ContratoServicos_PercTmp = (short)(context.localUtil.CToN( cgiGet( "Z1537ContratoServicos_PercTmp"), ",", "."));
               n1537ContratoServicos_PercTmp = false;
               n1537ContratoServicos_PercTmp = ((0==A1537ContratoServicos_PercTmp) ? true : false);
               A1538ContratoServicos_PercPgm = (short)(context.localUtil.CToN( cgiGet( "Z1538ContratoServicos_PercPgm"), ",", "."));
               n1538ContratoServicos_PercPgm = false;
               n1538ContratoServicos_PercPgm = ((0==A1538ContratoServicos_PercPgm) ? true : false);
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               O2073ContratoServicos_QtdRmn = (short)(context.localUtil.CToN( cgiGet( "O2073ContratoServicos_QtdRmn"), ",", "."));
               O638ContratoServicos_Ativo = StringUtil.StrToBool( cgiGet( "O638ContratoServicos_Ativo"));
               O557Servico_VlrUnidadeContratada = context.localUtil.CToN( cgiGet( "O557Servico_VlrUnidadeContratada"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_312 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_312"), ",", "."));
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               N155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "N155Servico_Codigo"), ",", "."));
               N1212ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( "N1212ContratoServicos_UnidadeContratada"), ",", "."));
               A1551Servico_Responsavel = (int)(context.localUtil.CToN( cgiGet( "SERVICO_RESPONSAVEL"), ",", "."));
               A827ContratoServicos_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_SERVICOCOD"), ",", "."));
               A640Servico_Vinculados = (short)(context.localUtil.CToN( cgiGet( "SERVICO_VINCULADOS"), ",", "."));
               A605Servico_Sigla = cgiGet( "SERVICO_SIGLA");
               A608Servico_Nome = cgiGet( "SERVICO_NOME");
               A2107Servico_Identificacao = cgiGet( "SERVICO_IDENTIFICACAO");
               A826ContratoServicos_ServicoSigla = cgiGet( "CONTRATOSERVICOS_SERVICOSIGLA");
               AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOS_CODIGO"), ",", "."));
               A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               AV12Insert_Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICO_CODIGO"), ",", "."));
               AV24Insert_ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATOSERVICOS_UNIDADECONTRATADA"), ",", "."));
               A558Servico_Percentual = context.localUtil.CToN( cgiGet( "SERVICO_PERCENTUAL"), ",", ".");
               n558Servico_Percentual = ((Convert.ToDecimal(0)==A558Servico_Percentual) ? true : false);
               AV26CemPorCento = StringUtil.StrToBool( cgiGet( "vCEMPORCENTO"));
               A1858ContratoServicos_Alias = cgiGet( "CONTRATOSERVICOS_ALIAS");
               n1858ContratoServicos_Alias = (String.IsNullOrEmpty(StringUtil.RTrim( A1858ContratoServicos_Alias)) ? true : false);
               A516Contratada_TipoFabrica = cgiGet( "CONTRATADA_TIPOFABRICA");
               A1712ContratoServicos_UndCntSgl = cgiGet( "CONTRATOSERVICOS_UNDCNTSGL");
               n1712ContratoServicos_UndCntSgl = false;
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A453Contrato_IndiceDivergencia = context.localUtil.CToN( cgiGet( "CONTRATO_INDICEDIVERGENCIA"), ",", ".");
               AV27Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATO_CODIGO"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV29AuditingObject);
               A888ContratoServicos_HmlSemCnf = StringUtil.StrToBool( cgiGet( "CONTRATOSERVICOS_HMLSEMCNF"));
               n888ContratoServicos_HmlSemCnf = ((false==A888ContratoServicos_HmlSemCnf) ? true : false);
               A1190ContratoServicos_PrazoImediato = StringUtil.StrToBool( cgiGet( "CONTRATOSERVICOS_PRAZOIMEDIATO"));
               n1190ContratoServicos_PrazoImediato = ((false==A1190ContratoServicos_PrazoImediato) ? true : false);
               A1531ContratoServicos_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_TIPOHIERARQUIA"), ",", "."));
               n1531ContratoServicos_TipoHierarquia = ((0==A1531ContratoServicos_TipoHierarquia) ? true : false);
               A1537ContratoServicos_PercTmp = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_PERCTMP"), ",", "."));
               n1537ContratoServicos_PercTmp = ((0==A1537ContratoServicos_PercTmp) ? true : false);
               A1538ContratoServicos_PercPgm = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_PERCPGM"), ",", "."));
               n1538ContratoServicos_PercPgm = ((0==A1538ContratoServicos_PercPgm) ? true : false);
               A911ContratoServicos_Prazos = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_PRAZOS"), ",", "."));
               n911ContratoServicos_Prazos = false;
               A1061Servico_Tela = cgiGet( "SERVICO_TELA");
               n1061Servico_Tela = false;
               A632Servico_Ativo = StringUtil.StrToBool( cgiGet( "SERVICO_ATIVO"));
               A156Servico_Descricao = cgiGet( "SERVICO_DESCRICAO");
               n156Servico_Descricao = false;
               A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cgiGet( "SERVICO_ISORIGEMREFERENCIA"));
               A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "SERVICO_VINCULADO"), ",", "."));
               n631Servico_Vinculado = false;
               A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "SERVICOGRUPO_CODIGO"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A75Contrato_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_AREATRABALHOCOD"), ",", "."));
               A2132ContratoServicos_UndCntNome = cgiGet( "CONTRATOSERVICOS_UNDCNTNOME");
               n2132ContratoServicos_UndCntNome = false;
               A158ServicoGrupo_Descricao = cgiGet( "SERVICOGRUPO_DESCRICAO");
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               A1377ContratoServicos_Indicadores = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_INDICADORES"), ",", "."));
               n1377ContratoServicos_Indicadores = false;
               A2073ContratoServicos_QtdRmn = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_QTDRMN"), ",", "."));
               AV36Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Gxuitabspanel_tab_Width = cgiGet( "GXUITABSPANEL_TAB_Width");
               Gxuitabspanel_tab_Height = cgiGet( "GXUITABSPANEL_TAB_Height");
               Gxuitabspanel_tab_Cls = cgiGet( "GXUITABSPANEL_TAB_Cls");
               Gxuitabspanel_tab_Enabled = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Enabled"));
               Gxuitabspanel_tab_Class = cgiGet( "GXUITABSPANEL_TAB_Class");
               Gxuitabspanel_tab_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autowidth"));
               Gxuitabspanel_tab_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autoheight"));
               Gxuitabspanel_tab_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autoscroll"));
               Gxuitabspanel_tab_Activetabid = cgiGet( "GXUITABSPANEL_TAB_Activetabid");
               Gxuitabspanel_tab_Designtimetabs = cgiGet( "GXUITABSPANEL_TAB_Designtimetabs");
               Gxuitabspanel_tab_Selectedtabindex = (int)(context.localUtil.CToN( cgiGet( "GXUITABSPANEL_TAB_Selectedtabindex"), ",", "."));
               Gxuitabspanel_tab_Visible = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV36Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1531ContratoServicos_TipoHierarquia), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1537ContratoServicos_PercTmp), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1538ContratoServicos_PercPgm), "ZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV36Pgmname, "")));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_HmlSemCnf:"+StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_PrazoImediato:"+StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_TipoHierarquia:"+context.localUtil.Format( (decimal)(A1531ContratoServicos_TipoHierarquia), "ZZZ9"));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_PercTmp:"+context.localUtil.Format( (decimal)(A1537ContratoServicos_PercTmp), "ZZZ9"));
                  GXUtil.WriteLog("contratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_PercPgm:"+context.localUtil.Format( (decimal)(A1538ContratoServicos_PercPgm), "ZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode34 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode34;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound34 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0X0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120X2 */
                           E120X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130X2 */
                           E130X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOREPLICAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140X2 */
                           E140X2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOUPDPRAZO'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E150X2 */
                           E150X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSERVICO_CODIGO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E160X2 */
                           E160X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSUBSERVICO_CODIGO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E170X2 */
                           E170X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E130X2 */
            E130X2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0X34( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0X34( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSubservico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSubservico_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAlias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAlias_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0X0( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0X34( ) ;
            }
            else
            {
               CheckExtendedTable0X34( ) ;
               CloseExtendedTableCursors0X34( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode34 = Gx_mode;
            CONFIRM_0X228( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode34;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode34;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_0X228( )
      {
         s2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         nGXsfl_312_idx = 0;
         while ( nGXsfl_312_idx < nRC_GXsfl_312 )
         {
            ReadRow0X228( ) ;
            if ( ( nRcdExists_228 != 0 ) || ( nIsMod_228 != 0 ) )
            {
               GetKey0X228( ) ;
               if ( ( nRcdExists_228 == 0 ) && ( nRcdDeleted_228 == 0 ) )
               {
                  if ( RcdFound228 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate0X228( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0X228( ) ;
                        CloseExtendedTableCursors0X228( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                        n2073ContratoServicos_QtdRmn = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound228 != 0 )
                  {
                     if ( nRcdDeleted_228 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey0X228( ) ;
                        Load0X228( ) ;
                        BeforeValidate0X228( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0X228( ) ;
                           O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                           n2073ContratoServicos_QtdRmn = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_228 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate0X228( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0X228( ) ;
                              CloseExtendedTableCursors0X228( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                              n2073ContratoServicos_QtdRmn = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_228 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            ChangePostValue( edtContratoServicosRmn_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosRmn_Inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosRmn_Fim_Internalname, StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosRmn_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2069ContratoServicosRmn_Sequencial_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2069ContratoServicosRmn_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2070ContratoServicosRmn_Inicio_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( Z2070ContratoServicosRmn_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2071ContratoServicosRmn_Fim_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( Z2071ContratoServicosRmn_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2072ContratoServicosRmn_Valor_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( Z2072ContratoServicosRmn_Valor, 14, 5, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_228_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_228), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_228_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_228), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_228_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_228), 4, 0, ",", ""))) ;
            if ( nIsMod_228 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Inicio_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Fim_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Valor_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O2073ContratoServicos_QtdRmn = s2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption0X0( )
      {
      }

      protected void E120X2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.1 - Data: 15/03/2020", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV36Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV37GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GXV1), 8, 0)));
            while ( AV37GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV37GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Servico_Codigo") == 0 )
               {
                  AV12Insert_Servico_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Servico_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoServicos_UnidadeContratada") == 0 )
               {
                  AV24Insert_ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Insert_ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Insert_ContratoServicos_UnidadeContratada), 6, 0)));
               }
               AV37GXV1 = (int)(AV37GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GXV1), 8, 0)));
            }
         }
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
         GX_FocusControl = Gxuitabspanel_tab_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgUpdprazo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdprazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgUpdprazo_Enabled), 5, 0)));
            imgUpdprazo_Tooltiptext = "Dispon�vel ap�s Confirmar!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdprazo_Internalname, "Tooltiptext", imgUpdprazo_Tooltiptext);
         }
         else
         {
            imgUpdprazo_Tooltiptext = "Configurar o prazo de Execu��o!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdprazo_Internalname, "Tooltiptext", imgUpdprazo_Tooltiptext);
            new prc_getcontratoservico(context ).execute( ref  AV7ContratoServicos_Codigo, out  AV14ServicoGrupo_Codigo, out  AV17Servico_Codigo, out  AV15SubServico_Codigo, out  AV20TemSubServicos) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ServicoGrupo_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15SubServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TemSubServicos", AV20TemSubServicos);
            dynavSubservico_codigo.Visible = (AV20TemSubServicos ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSubservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSubservico_codigo.Visible), 5, 0)));
            lblTextblocksubservico_codigo_Visible = (AV20TemSubServicos ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocksubservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocksubservico_codigo_Visible), 5, 0)));
            lblTextblockservico_codigo_Caption = (AV20TemSubServicos ? "Vinculado a" : "Servi�o");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockservico_codigo_Internalname, "Caption", lblTextblockservico_codigo_Caption);
            lblTextblocksubservico_codigo_Caption = (AV20TemSubServicos ? "Servi�o" : "Sub servi�o");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocksubservico_codigo_Internalname, "Caption", lblTextblocksubservico_codigo_Caption);
            if ( new prc_existeintegridadereferencial(context).executeUdp(  AV7ContratoServicos_Codigo,  "Servico") )
            {
               dynavServicogrupo_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo.Enabled), 5, 0)));
               dynavServico_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_codigo.Enabled), 5, 0)));
               dynavSubservico_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSubservico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSubservico_codigo.Enabled), 5, 0)));
            }
         }
         cmbContratoServicos_StatusPagFnc.removeItem("B");
         cmbContratoServicos_StatusPagFnc.removeItem("S");
         cmbContratoServicos_StatusPagFnc.removeItem("E");
         cmbContratoServicos_StatusPagFnc.removeItem("A");
         cmbContratoServicos_StatusPagFnc.removeItem("X");
         cmbContratoServicos_StatusPagFnc.removeItem("N");
         cmbContratoServicos_StatusPagFnc.removeItem("J");
         cmbContratoServicos_StatusPagFnc.removeItem("U");
      }

      protected void E130X2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            new prc_insprioridadespadrao(context ).execute(  A160ContratoServicos_Codigo,  A155Servico_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            new prc_insartefatospadrao(context ).execute( ref  A160ContratoServicos_Codigo, ref  A155Servico_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         else if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) && ( A557Servico_VlrUnidadeContratada != O557Servico_VlrUnidadeContratada ) )
         {
            new prc_atualizavalorpf(context ).execute( ref  A75Contrato_AreaTrabalhoCod,  A557Servico_VlrUnidadeContratada,  A155Servico_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         if ( false )
         {
            new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV36Pgmname) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Pgmname", AV36Pgmname);
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwcontratoservicos.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {(int)AV27Contrato_Codigo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV36Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Pgmname", AV36Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV27Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E140X2( )
      {
         /* 'DoReplicar' Routine */
         if ( A1516ContratoServicos_TmpEstAnl + A1501ContratoServicos_TmpEstExc + A1502ContratoServicos_TmpEstCrr > 0 )
         {
            new prc_replicartemposestimados(context ).execute( ref  AV7ContratoServicos_Codigo,  A1516ContratoServicos_TmpEstAnl,  A1501ContratoServicos_TmpEstExc,  A1502ContratoServicos_TmpEstCrr) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
         }
         else
         {
            GX_msglist.addItem("N�o existem tempos estiimados preenchidos para replicar!");
         }
      }

      protected void E150X2( )
      {
         /* 'DoUpdPrazo' Routine */
         context.wjLoc = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim(((A911ContratoServicos_Prazos==0) ? "INS" : "UPD"))) + "," + UrlEncode("" +A160ContratoServicos_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         edtContratoServicos_PrazoDias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoDias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Visible), 5, 0)));
         cellContratoservicos_prazodias_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicos_prazodias_cell_Internalname, "Class", cellContratoservicos_prazodias_cell_Class);
         cellContratoservicos_prazodias_righttext_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicos_prazodias_righttext_cell_Internalname, "Class", cellContratoservicos_prazodias_righttext_cell_Class);
      }

      protected void E160X2( )
      {
         /* Servico_codigo_Click Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV30Alias = dynavServico_codigo.Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
         }
         if ( (0==AV15SubServico_Codigo) )
         {
            GXt_char1 = AV32Servico_Nome;
            new prc_serviconome(context ).execute( ref  AV17Servico_Codigo, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0)));
            AV32Servico_Nome = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
         }
      }

      protected void E170X2( )
      {
         /* Subservico_codigo_Click Routine */
         GXt_char1 = AV32Servico_Nome;
         new prc_serviconome(context ).execute( ref  AV15SubServico_Codigo, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15SubServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0)));
         AV32Servico_Nome = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
      }

      protected void ZM0X34( short GX_JID )
      {
         if ( ( GX_JID == 67 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1225ContratoServicos_PrazoCorrecaoTipo = T000X5_A1225ContratoServicos_PrazoCorrecaoTipo[0];
               Z1858ContratoServicos_Alias = T000X5_A1858ContratoServicos_Alias[0];
               Z555Servico_QtdContratada = T000X5_A555Servico_QtdContratada[0];
               Z557Servico_VlrUnidadeContratada = T000X5_A557Servico_VlrUnidadeContratada[0];
               Z558Servico_Percentual = T000X5_A558Servico_Percentual[0];
               Z607ServicoContrato_Faturamento = T000X5_A607ServicoContrato_Faturamento[0];
               Z639ContratoServicos_LocalExec = T000X5_A639ContratoServicos_LocalExec[0];
               Z868ContratoServicos_TipoVnc = T000X5_A868ContratoServicos_TipoVnc[0];
               Z888ContratoServicos_HmlSemCnf = T000X5_A888ContratoServicos_HmlSemCnf[0];
               Z1454ContratoServicos_PrazoTpDias = T000X5_A1454ContratoServicos_PrazoTpDias[0];
               Z1152ContratoServicos_PrazoAnalise = T000X5_A1152ContratoServicos_PrazoAnalise[0];
               Z1153ContratoServicos_PrazoResposta = T000X5_A1153ContratoServicos_PrazoResposta[0];
               Z1181ContratoServicos_PrazoGarantia = T000X5_A1181ContratoServicos_PrazoGarantia[0];
               Z1182ContratoServicos_PrazoAtendeGarantia = T000X5_A1182ContratoServicos_PrazoAtendeGarantia[0];
               Z1224ContratoServicos_PrazoCorrecao = T000X5_A1224ContratoServicos_PrazoCorrecao[0];
               Z1649ContratoServicos_PrazoInicio = T000X5_A1649ContratoServicos_PrazoInicio[0];
               Z1190ContratoServicos_PrazoImediato = T000X5_A1190ContratoServicos_PrazoImediato[0];
               Z1191ContratoServicos_Produtividade = T000X5_A1191ContratoServicos_Produtividade[0];
               Z1217ContratoServicos_EspelhaAceite = T000X5_A1217ContratoServicos_EspelhaAceite[0];
               Z1266ContratoServicos_Momento = T000X5_A1266ContratoServicos_Momento[0];
               Z1325ContratoServicos_StatusPagFnc = T000X5_A1325ContratoServicos_StatusPagFnc[0];
               Z1340ContratoServicos_QntUntCns = T000X5_A1340ContratoServicos_QntUntCns[0];
               Z1341ContratoServicos_FatorCnvUndCnt = T000X5_A1341ContratoServicos_FatorCnvUndCnt[0];
               Z1397ContratoServicos_NaoRequerAtr = T000X5_A1397ContratoServicos_NaoRequerAtr[0];
               Z1455ContratoServicos_IndiceDivergencia = T000X5_A1455ContratoServicos_IndiceDivergencia[0];
               Z1516ContratoServicos_TmpEstAnl = T000X5_A1516ContratoServicos_TmpEstAnl[0];
               Z1501ContratoServicos_TmpEstExc = T000X5_A1501ContratoServicos_TmpEstExc[0];
               Z1502ContratoServicos_TmpEstCrr = T000X5_A1502ContratoServicos_TmpEstCrr[0];
               Z1531ContratoServicos_TipoHierarquia = T000X5_A1531ContratoServicos_TipoHierarquia[0];
               Z1537ContratoServicos_PercTmp = T000X5_A1537ContratoServicos_PercTmp[0];
               Z1538ContratoServicos_PercPgm = T000X5_A1538ContratoServicos_PercPgm[0];
               Z1539ContratoServicos_PercCnc = T000X5_A1539ContratoServicos_PercCnc[0];
               Z638ContratoServicos_Ativo = T000X5_A638ContratoServicos_Ativo[0];
               Z1723ContratoServicos_CodigoFiscal = T000X5_A1723ContratoServicos_CodigoFiscal[0];
               Z1799ContratoServicos_LimiteProposta = T000X5_A1799ContratoServicos_LimiteProposta[0];
               Z2074ContratoServicos_CalculoRmn = T000X5_A2074ContratoServicos_CalculoRmn[0];
               Z2094ContratoServicos_SolicitaGestorSistema = T000X5_A2094ContratoServicos_SolicitaGestorSistema[0];
               Z155Servico_Codigo = T000X5_A155Servico_Codigo[0];
               Z74Contrato_Codigo = T000X5_A74Contrato_Codigo[0];
               Z1212ContratoServicos_UnidadeContratada = T000X5_A1212ContratoServicos_UnidadeContratada[0];
            }
            else
            {
               Z1225ContratoServicos_PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
               Z1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
               Z555Servico_QtdContratada = A555Servico_QtdContratada;
               Z557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
               Z558Servico_Percentual = A558Servico_Percentual;
               Z607ServicoContrato_Faturamento = A607ServicoContrato_Faturamento;
               Z639ContratoServicos_LocalExec = A639ContratoServicos_LocalExec;
               Z868ContratoServicos_TipoVnc = A868ContratoServicos_TipoVnc;
               Z888ContratoServicos_HmlSemCnf = A888ContratoServicos_HmlSemCnf;
               Z1454ContratoServicos_PrazoTpDias = A1454ContratoServicos_PrazoTpDias;
               Z1152ContratoServicos_PrazoAnalise = A1152ContratoServicos_PrazoAnalise;
               Z1153ContratoServicos_PrazoResposta = A1153ContratoServicos_PrazoResposta;
               Z1181ContratoServicos_PrazoGarantia = A1181ContratoServicos_PrazoGarantia;
               Z1182ContratoServicos_PrazoAtendeGarantia = A1182ContratoServicos_PrazoAtendeGarantia;
               Z1224ContratoServicos_PrazoCorrecao = A1224ContratoServicos_PrazoCorrecao;
               Z1649ContratoServicos_PrazoInicio = A1649ContratoServicos_PrazoInicio;
               Z1190ContratoServicos_PrazoImediato = A1190ContratoServicos_PrazoImediato;
               Z1191ContratoServicos_Produtividade = A1191ContratoServicos_Produtividade;
               Z1217ContratoServicos_EspelhaAceite = A1217ContratoServicos_EspelhaAceite;
               Z1266ContratoServicos_Momento = A1266ContratoServicos_Momento;
               Z1325ContratoServicos_StatusPagFnc = A1325ContratoServicos_StatusPagFnc;
               Z1340ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
               Z1341ContratoServicos_FatorCnvUndCnt = A1341ContratoServicos_FatorCnvUndCnt;
               Z1397ContratoServicos_NaoRequerAtr = A1397ContratoServicos_NaoRequerAtr;
               Z1455ContratoServicos_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
               Z1516ContratoServicos_TmpEstAnl = A1516ContratoServicos_TmpEstAnl;
               Z1501ContratoServicos_TmpEstExc = A1501ContratoServicos_TmpEstExc;
               Z1502ContratoServicos_TmpEstCrr = A1502ContratoServicos_TmpEstCrr;
               Z1531ContratoServicos_TipoHierarquia = A1531ContratoServicos_TipoHierarquia;
               Z1537ContratoServicos_PercTmp = A1537ContratoServicos_PercTmp;
               Z1538ContratoServicos_PercPgm = A1538ContratoServicos_PercPgm;
               Z1539ContratoServicos_PercCnc = A1539ContratoServicos_PercCnc;
               Z638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
               Z1723ContratoServicos_CodigoFiscal = A1723ContratoServicos_CodigoFiscal;
               Z1799ContratoServicos_LimiteProposta = A1799ContratoServicos_LimiteProposta;
               Z2074ContratoServicos_CalculoRmn = A2074ContratoServicos_CalculoRmn;
               Z2094ContratoServicos_SolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
               Z155Servico_Codigo = A155Servico_Codigo;
               Z74Contrato_Codigo = A74Contrato_Codigo;
               Z1212ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            }
         }
         if ( GX_JID == -67 )
         {
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1225ContratoServicos_PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
            Z1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
            Z555Servico_QtdContratada = A555Servico_QtdContratada;
            Z557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
            Z558Servico_Percentual = A558Servico_Percentual;
            Z607ServicoContrato_Faturamento = A607ServicoContrato_Faturamento;
            Z639ContratoServicos_LocalExec = A639ContratoServicos_LocalExec;
            Z868ContratoServicos_TipoVnc = A868ContratoServicos_TipoVnc;
            Z888ContratoServicos_HmlSemCnf = A888ContratoServicos_HmlSemCnf;
            Z1454ContratoServicos_PrazoTpDias = A1454ContratoServicos_PrazoTpDias;
            Z1152ContratoServicos_PrazoAnalise = A1152ContratoServicos_PrazoAnalise;
            Z1153ContratoServicos_PrazoResposta = A1153ContratoServicos_PrazoResposta;
            Z1181ContratoServicos_PrazoGarantia = A1181ContratoServicos_PrazoGarantia;
            Z1182ContratoServicos_PrazoAtendeGarantia = A1182ContratoServicos_PrazoAtendeGarantia;
            Z1224ContratoServicos_PrazoCorrecao = A1224ContratoServicos_PrazoCorrecao;
            Z1649ContratoServicos_PrazoInicio = A1649ContratoServicos_PrazoInicio;
            Z1190ContratoServicos_PrazoImediato = A1190ContratoServicos_PrazoImediato;
            Z1191ContratoServicos_Produtividade = A1191ContratoServicos_Produtividade;
            Z1217ContratoServicos_EspelhaAceite = A1217ContratoServicos_EspelhaAceite;
            Z1266ContratoServicos_Momento = A1266ContratoServicos_Momento;
            Z1325ContratoServicos_StatusPagFnc = A1325ContratoServicos_StatusPagFnc;
            Z1340ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
            Z1341ContratoServicos_FatorCnvUndCnt = A1341ContratoServicos_FatorCnvUndCnt;
            Z1397ContratoServicos_NaoRequerAtr = A1397ContratoServicos_NaoRequerAtr;
            Z1455ContratoServicos_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
            Z1516ContratoServicos_TmpEstAnl = A1516ContratoServicos_TmpEstAnl;
            Z1501ContratoServicos_TmpEstExc = A1501ContratoServicos_TmpEstExc;
            Z1502ContratoServicos_TmpEstCrr = A1502ContratoServicos_TmpEstCrr;
            Z1531ContratoServicos_TipoHierarquia = A1531ContratoServicos_TipoHierarquia;
            Z1537ContratoServicos_PercTmp = A1537ContratoServicos_PercTmp;
            Z1538ContratoServicos_PercPgm = A1538ContratoServicos_PercPgm;
            Z1539ContratoServicos_PercCnc = A1539ContratoServicos_PercCnc;
            Z638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
            Z1723ContratoServicos_CodigoFiscal = A1723ContratoServicos_CodigoFiscal;
            Z1799ContratoServicos_LimiteProposta = A1799ContratoServicos_LimiteProposta;
            Z2074ContratoServicos_CalculoRmn = A2074ContratoServicos_CalculoRmn;
            Z2094ContratoServicos_SolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z1212ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z608Servico_Nome = A608Servico_Nome;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z156Servico_Descricao = A156Servico_Descricao;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z2132ContratoServicos_UndCntNome = A2132ContratoServicos_UndCntNome;
            Z1712ContratoServicos_UndCntSgl = A1712ContratoServicos_UndCntSgl;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvSERVICOGRUPO_CODIGO_html0X34( ) ;
         cmbContratoServicos_PrazoTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_PrazoTipo.Enabled), 5, 0)));
         edtContratoServicos_PrazoDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Enabled), 5, 0)));
         lblTextblockcontrato_valorunidadecontratacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_valorunidadecontratacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_valorunidadecontratacao_Visible), 5, 0)));
         edtContrato_ValorUnidadeContratacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_ValorUnidadeContratacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_ValorUnidadeContratacao_Visible), 5, 0)));
         AV36Pgmname = "ContratoServicos";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Pgmname", AV36Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         cmbContratoServicos_PrazoTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_PrazoTipo.Enabled), 5, 0)));
         edtContratoServicos_PrazoDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicos_Codigo) )
         {
            A160ContratoServicos_Codigo = AV7ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         /* Using cursor T000X7 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A911ContratoServicos_Prazos = T000X7_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = T000X7_n911ContratoServicos_Prazos[0];
         }
         else
         {
            A911ContratoServicos_Prazos = 0;
            n911ContratoServicos_Prazos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A911ContratoServicos_Prazos", StringUtil.LTrim( StringUtil.Str( (decimal)(A911ContratoServicos_Prazos), 4, 0)));
         }
         pr_default.close(4);
         /* Using cursor T000X14 */
         pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A913ContratoServicos_PrazoTipo = T000X14_A913ContratoServicos_PrazoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            n913ContratoServicos_PrazoTipo = T000X14_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = T000X14_A914ContratoServicos_PrazoDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
            n914ContratoServicos_PrazoDias = T000X14_n914ContratoServicos_PrazoDias[0];
         }
         else
         {
            A914ContratoServicos_PrazoDias = 0;
            n914ContratoServicos_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
            A913ContratoServicos_PrazoTipo = "";
            n913ContratoServicos_PrazoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
         }
         pr_default.close(11);
         edtContratoServicos_PrazoDias_Visible = ((StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoDias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Visible), 5, 0)));
         if ( ! ( ( StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F") == 0 ) ) )
         {
            cellContratoservicos_prazodias_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicos_prazodias_cell_Internalname, "Class", cellContratoservicos_prazodias_cell_Class);
         }
         else
         {
            if ( StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F") == 0 )
            {
               cellContratoservicos_prazodias_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicos_prazodias_cell_Internalname, "Class", cellContratoservicos_prazodias_cell_Class);
            }
         }
         if ( ! ( ( StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F") == 0 ) ) )
         {
            cellContratoservicos_prazodias_righttext_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicos_prazodias_righttext_cell_Internalname, "Class", cellContratoservicos_prazodias_righttext_cell_Class);
         }
         else
         {
            if ( StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "F") == 0 )
            {
               cellContratoservicos_prazodias_righttext_cell_Class = "";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratoservicos_prazodias_righttext_cell_Internalname, "Class", cellContratoservicos_prazodias_righttext_cell_Class);
            }
         }
         /* Using cursor T000X16 */
         pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            A1377ContratoServicos_Indicadores = T000X16_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = T000X16_n1377ContratoServicos_Indicadores[0];
         }
         else
         {
            A1377ContratoServicos_Indicadores = 0;
            n1377ContratoServicos_Indicadores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1377ContratoServicos_Indicadores", StringUtil.LTrim( StringUtil.Str( (decimal)(A1377ContratoServicos_Indicadores), 4, 0)));
         }
         pr_default.close(12);
         /* Using cursor T000X18 */
         pr_default.execute(13, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A2073ContratoServicos_QtdRmn = T000X18_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = T000X18_n2073ContratoServicos_QtdRmn[0];
         }
         else
         {
            A2073ContratoServicos_QtdRmn = 0;
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         }
         O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         pr_default.close(13);
         GXACONTRATOSERVICOS_UNIDADECONTRATADA_html0X34( AV27Contrato_Codigo) ;
         edtContratoServicos_LimiteProposta_Enabled = (AV8WWPContext.gxTpr_Userehcontratante||AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_LimiteProposta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_LimiteProposta_Enabled), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Servico_Codigo) )
         {
            edtServico_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtServico_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV24Insert_ContratoServicos_UnidadeContratada) )
         {
            dynContratoServicos_UnidadeContratada.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicos_UnidadeContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicos_UnidadeContratada.Enabled), 5, 0)));
         }
         else
         {
            dynContratoServicos_UnidadeContratada.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicos_UnidadeContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicos_UnidadeContratada.Enabled), 5, 0)));
         }
         GXVvSERVICO_CODIGO_html0X34( AV14ServicoGrupo_Codigo) ;
         GXVvSUBSERVICO_CODIGO_html0X34( AV17Servico_Codigo) ;
      }

      protected void standaloneModal( )
      {
         chkContratoServicos_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_Ativo.Visible), 5, 0)));
         lblTextblockcontratoservicos_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicos_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicos_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV24Insert_ContratoServicos_UnidadeContratada) )
         {
            A1212ContratoServicos_UnidadeContratada = AV24Insert_ContratoServicos_UnidadeContratada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Servico_Codigo) )
         {
            A155Servico_Codigo = AV12Insert_Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         else
         {
            if ( AV17Servico_Codigo > 0 )
            {
               A155Servico_Codigo = AV17Servico_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            }
            else
            {
               if ( AV15SubServico_Codigo > 0 )
               {
                  A155Servico_Codigo = AV15SubServico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               }
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2094ContratoServicos_SolicitaGestorSistema) && ( Gx_BScreen == 0 ) )
         {
            A2094ContratoServicos_SolicitaGestorSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A638ContratoServicos_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A638ContratoServicos_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1649ContratoServicos_PrazoInicio) && ( Gx_BScreen == 0 ) )
         {
            A1649ContratoServicos_PrazoInicio = 1;
            n1649ContratoServicos_PrazoInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A607ServicoContrato_Faturamento)) && ( Gx_BScreen == 0 ) )
         {
            A607ServicoContrato_Faturamento = "B";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) && ( Gx_BScreen == 0 ) )
         {
            A1454ContratoServicos_PrazoTpDias = "U";
            n1454ContratoServicos_PrazoTpDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A558Servico_Percentual) && ( Gx_BScreen == 0 ) )
         {
            A558Servico_Percentual = (decimal)(1);
            n558Servico_Percentual = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A558Servico_Percentual", StringUtil.LTrim( StringUtil.Str( A558Servico_Percentual, 7, 3)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000X10 */
            pr_default.execute(7, new Object[] {A1212ContratoServicos_UnidadeContratada});
            A2132ContratoServicos_UndCntNome = T000X10_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = T000X10_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = T000X10_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = T000X10_n1712ContratoServicos_UndCntSgl[0];
            pr_default.close(7);
            lblContratoservicos_produtividade_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_produtividade_righttext_Internalname, "Caption", lblContratoservicos_produtividade_righttext_Caption);
            lblContratoservicos_limiteproposta_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_limiteproposta_righttext_Internalname, "Caption", lblContratoservicos_limiteproposta_righttext_Caption);
            /* Using cursor T000X8 */
            pr_default.execute(5, new Object[] {A155Servico_Codigo});
            A608Servico_Nome = T000X8_A608Servico_Nome[0];
            A605Servico_Sigla = T000X8_A605Servico_Sigla[0];
            A1061Servico_Tela = T000X8_A1061Servico_Tela[0];
            n1061Servico_Tela = T000X8_n1061Servico_Tela[0];
            A632Servico_Ativo = T000X8_A632Servico_Ativo[0];
            A156Servico_Descricao = T000X8_A156Servico_Descricao[0];
            n156Servico_Descricao = T000X8_n156Servico_Descricao[0];
            A2092Servico_IsOrigemReferencia = T000X8_A2092Servico_IsOrigemReferencia[0];
            A631Servico_Vinculado = T000X8_A631Servico_Vinculado[0];
            n631Servico_Vinculado = T000X8_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = T000X8_A157ServicoGrupo_Codigo[0];
            pr_default.close(5);
            AV32Servico_Nome = A608Servico_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            /* Using cursor T000X11 */
            pr_default.execute(8, new Object[] {A157ServicoGrupo_Codigo});
            A158ServicoGrupo_Descricao = T000X11_A158ServicoGrupo_Descricao[0];
            pr_default.close(8);
            GXt_int2 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1551Servico_Responsavel = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
            GXt_int3 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A640Servico_Vinculados = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
            /* Using cursor T000X9 */
            pr_default.execute(6, new Object[] {A74Contrato_Codigo});
            A77Contrato_Numero = T000X9_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000X9_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000X9_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000X9_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A116Contrato_ValorUnidadeContratacao = T000X9_A116Contrato_ValorUnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            A453Contrato_IndiceDivergencia = T000X9_A453Contrato_IndiceDivergencia[0];
            A39Contratada_Codigo = T000X9_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = T000X9_A75Contrato_AreaTrabalhoCod[0];
            pr_default.close(6);
            /* Using cursor T000X12 */
            pr_default.execute(9, new Object[] {A39Contratada_Codigo});
            A516Contratada_TipoFabrica = T000X12_A516Contratada_TipoFabrica[0];
            A40Contratada_PessoaCod = T000X12_A40Contratada_PessoaCod[0];
            pr_default.close(9);
            chkContratoServicos_EspelhaAceite.Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_EspelhaAceite.Visible), 5, 0)));
            lblTextblockcontratoservicos_espelhaaceite_Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicos_espelhaaceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicos_espelhaaceite_Visible), 5, 0)));
            /* Using cursor T000X13 */
            pr_default.execute(10, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T000X13_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000X13_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000X13_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000X13_n42Contratada_PessoaCNPJ[0];
            pr_default.close(10);
            AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
         }
      }

      protected void Load0X34( )
      {
         /* Using cursor T000X21 */
         pr_default.execute(14, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound34 = 1;
            A1225ContratoServicos_PrazoCorrecaoTipo = T000X21_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            n1225ContratoServicos_PrazoCorrecaoTipo = T000X21_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A77Contrato_Numero = T000X21_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000X21_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000X21_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000X21_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A116Contrato_ValorUnidadeContratacao = T000X21_A116Contrato_ValorUnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            A41Contratada_PessoaNom = T000X21_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000X21_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000X21_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000X21_n42Contratada_PessoaCNPJ[0];
            A516Contratada_TipoFabrica = T000X21_A516Contratada_TipoFabrica[0];
            A1858ContratoServicos_Alias = T000X21_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = T000X21_n1858ContratoServicos_Alias[0];
            A608Servico_Nome = T000X21_A608Servico_Nome[0];
            A605Servico_Sigla = T000X21_A605Servico_Sigla[0];
            A1061Servico_Tela = T000X21_A1061Servico_Tela[0];
            n1061Servico_Tela = T000X21_n1061Servico_Tela[0];
            A632Servico_Ativo = T000X21_A632Servico_Ativo[0];
            A156Servico_Descricao = T000X21_A156Servico_Descricao[0];
            n156Servico_Descricao = T000X21_n156Servico_Descricao[0];
            A158ServicoGrupo_Descricao = T000X21_A158ServicoGrupo_Descricao[0];
            A2092Servico_IsOrigemReferencia = T000X21_A2092Servico_IsOrigemReferencia[0];
            A2132ContratoServicos_UndCntNome = T000X21_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = T000X21_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = T000X21_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = T000X21_n1712ContratoServicos_UndCntSgl[0];
            A555Servico_QtdContratada = T000X21_A555Servico_QtdContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
            n555Servico_QtdContratada = T000X21_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = T000X21_A557Servico_VlrUnidadeContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            A558Servico_Percentual = T000X21_A558Servico_Percentual[0];
            n558Servico_Percentual = T000X21_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = T000X21_A607ServicoContrato_Faturamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
            A639ContratoServicos_LocalExec = T000X21_A639ContratoServicos_LocalExec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
            A868ContratoServicos_TipoVnc = T000X21_A868ContratoServicos_TipoVnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
            n868ContratoServicos_TipoVnc = T000X21_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = T000X21_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = T000X21_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = T000X21_A1454ContratoServicos_PrazoTpDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
            n1454ContratoServicos_PrazoTpDias = T000X21_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = T000X21_A1152ContratoServicos_PrazoAnalise[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
            n1152ContratoServicos_PrazoAnalise = T000X21_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = T000X21_A1153ContratoServicos_PrazoResposta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
            n1153ContratoServicos_PrazoResposta = T000X21_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = T000X21_A1181ContratoServicos_PrazoGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
            n1181ContratoServicos_PrazoGarantia = T000X21_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = T000X21_A1182ContratoServicos_PrazoAtendeGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
            n1182ContratoServicos_PrazoAtendeGarantia = T000X21_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = T000X21_A1224ContratoServicos_PrazoCorrecao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            n1224ContratoServicos_PrazoCorrecao = T000X21_n1224ContratoServicos_PrazoCorrecao[0];
            A1649ContratoServicos_PrazoInicio = T000X21_A1649ContratoServicos_PrazoInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
            n1649ContratoServicos_PrazoInicio = T000X21_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = T000X21_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = T000X21_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = T000X21_A1191ContratoServicos_Produtividade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
            n1191ContratoServicos_Produtividade = T000X21_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = T000X21_A1217ContratoServicos_EspelhaAceite[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1217ContratoServicos_EspelhaAceite", A1217ContratoServicos_EspelhaAceite);
            n1217ContratoServicos_EspelhaAceite = T000X21_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = T000X21_A1266ContratoServicos_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
            n1266ContratoServicos_Momento = T000X21_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = T000X21_A1325ContratoServicos_StatusPagFnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
            n1325ContratoServicos_StatusPagFnc = T000X21_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = T000X21_A1340ContratoServicos_QntUntCns[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
            n1340ContratoServicos_QntUntCns = T000X21_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = T000X21_A1341ContratoServicos_FatorCnvUndCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
            n1341ContratoServicos_FatorCnvUndCnt = T000X21_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = T000X21_A1397ContratoServicos_NaoRequerAtr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1397ContratoServicos_NaoRequerAtr", A1397ContratoServicos_NaoRequerAtr);
            n1397ContratoServicos_NaoRequerAtr = T000X21_n1397ContratoServicos_NaoRequerAtr[0];
            A453Contrato_IndiceDivergencia = T000X21_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = T000X21_A1455ContratoServicos_IndiceDivergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
            n1455ContratoServicos_IndiceDivergencia = T000X21_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = T000X21_A1516ContratoServicos_TmpEstAnl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
            n1516ContratoServicos_TmpEstAnl = T000X21_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = T000X21_A1501ContratoServicos_TmpEstExc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
            n1501ContratoServicos_TmpEstExc = T000X21_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = T000X21_A1502ContratoServicos_TmpEstCrr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
            n1502ContratoServicos_TmpEstCrr = T000X21_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = T000X21_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = T000X21_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = T000X21_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = T000X21_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = T000X21_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = T000X21_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = T000X21_A1539ContratoServicos_PercCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
            n1539ContratoServicos_PercCnc = T000X21_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = T000X21_A638ContratoServicos_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
            A1723ContratoServicos_CodigoFiscal = T000X21_A1723ContratoServicos_CodigoFiscal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1723ContratoServicos_CodigoFiscal", A1723ContratoServicos_CodigoFiscal);
            n1723ContratoServicos_CodigoFiscal = T000X21_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = T000X21_A1799ContratoServicos_LimiteProposta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
            n1799ContratoServicos_LimiteProposta = T000X21_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = T000X21_A2074ContratoServicos_CalculoRmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
            n2074ContratoServicos_CalculoRmn = T000X21_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = T000X21_A2094ContratoServicos_SolicitaGestorSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
            A155Servico_Codigo = T000X21_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A74Contrato_Codigo = T000X21_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = T000X21_A1212ContratoServicos_UnidadeContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
            A631Servico_Vinculado = T000X21_A631Servico_Vinculado[0];
            n631Servico_Vinculado = T000X21_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = T000X21_A157ServicoGrupo_Codigo[0];
            A39Contratada_Codigo = T000X21_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = T000X21_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = T000X21_A75Contrato_AreaTrabalhoCod[0];
            A913ContratoServicos_PrazoTipo = T000X21_A913ContratoServicos_PrazoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            n913ContratoServicos_PrazoTipo = T000X21_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = T000X21_A914ContratoServicos_PrazoDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A914ContratoServicos_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A914ContratoServicos_PrazoDias), 4, 0)));
            n914ContratoServicos_PrazoDias = T000X21_n914ContratoServicos_PrazoDias[0];
            A1377ContratoServicos_Indicadores = T000X21_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = T000X21_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = T000X21_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = T000X21_n2073ContratoServicos_QtdRmn[0];
            ZM0X34( -67) ;
         }
         pr_default.close(14);
         OnLoadActions0X34( ) ;
      }

      protected void OnLoadActions0X34( )
      {
         O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         AV32Servico_Nome = A608Servico_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
         A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         A827ContratoServicos_ServicoCod = A155Servico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
         GXt_int3 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         lblContratoservicos_produtividade_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_produtividade_righttext_Internalname, "Caption", lblContratoservicos_produtividade_righttext_Caption);
         lblContratoservicos_limiteproposta_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_limiteproposta_righttext_Internalname, "Caption", lblContratoservicos_limiteproposta_righttext_Caption);
         AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26CemPorCento", AV26CemPorCento);
         if ( AV26CemPorCento )
         {
            lblContratoservicos_prazocorrecao_righttext_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_prazocorrecao_righttext_Internalname, "Caption", lblContratoservicos_prazocorrecao_righttext_Caption);
         }
         if ( ( A1224ContratoServicos_PrazoCorrecao > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = "F";
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
         }
         else
         {
            if ( AV26CemPorCento )
            {
               A1225ContratoServicos_PrazoCorrecaoTipo = "I";
               n1225ContratoServicos_PrazoCorrecaoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            }
         }
         if ( ( A1224ContratoServicos_PrazoCorrecao > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) )
         {
            A1224ContratoServicos_PrazoCorrecao = 0;
            n1224ContratoServicos_PrazoCorrecao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            n1224ContratoServicos_PrazoCorrecao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 ) || AV26CemPorCento )
            {
               A1224ContratoServicos_PrazoCorrecao = 0;
               n1224ContratoServicos_PrazoCorrecao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            }
         }
         edtContratoServicos_PrazoCorrecao_Visible = (((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I")!=0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoCorrecao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoCorrecao_Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV30Alias = A1858ContratoServicos_Alias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               AV30Alias = A1858ContratoServicos_Alias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
            }
         }
         AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
         chkContratoServicos_EspelhaAceite.Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_EspelhaAceite.Visible), 5, 0)));
         lblTextblockcontratoservicos_espelhaaceite_Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicos_espelhaaceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicos_espelhaaceite_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1455ContratoServicos_IndiceDivergencia) && ( Gx_BScreen == 0 ) )
         {
            A1455ContratoServicos_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            n1455ContratoServicos_IndiceDivergencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A557Servico_VlrUnidadeContratada) && ( Gx_BScreen == 0 ) )
         {
            A557Servico_VlrUnidadeContratada = A116Contrato_ValorUnidadeContratacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
         }
      }

      protected void CheckExtendedTable0X34( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T000X8 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A608Servico_Nome = T000X8_A608Servico_Nome[0];
         A605Servico_Sigla = T000X8_A605Servico_Sigla[0];
         A1061Servico_Tela = T000X8_A1061Servico_Tela[0];
         n1061Servico_Tela = T000X8_n1061Servico_Tela[0];
         A632Servico_Ativo = T000X8_A632Servico_Ativo[0];
         A156Servico_Descricao = T000X8_A156Servico_Descricao[0];
         n156Servico_Descricao = T000X8_n156Servico_Descricao[0];
         A2092Servico_IsOrigemReferencia = T000X8_A2092Servico_IsOrigemReferencia[0];
         A631Servico_Vinculado = T000X8_A631Servico_Vinculado[0];
         n631Servico_Vinculado = T000X8_n631Servico_Vinculado[0];
         A157ServicoGrupo_Codigo = T000X8_A157ServicoGrupo_Codigo[0];
         pr_default.close(5);
         AV32Servico_Nome = A608Servico_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
         A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
         /* Using cursor T000X11 */
         pr_default.execute(8, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A158ServicoGrupo_Descricao = T000X11_A158ServicoGrupo_Descricao[0];
         pr_default.close(8);
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         A827ContratoServicos_ServicoCod = A155Servico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
         GXt_int3 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         if ( (0==A155Servico_Codigo) )
         {
            GX_msglist.addItem("Deve indicar o servi�o a incluir no contrato!", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000X10 */
         pr_default.execute(7, new Object[] {A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_UNIDADECONTRATADA");
            AnyError = 1;
            GX_FocusControl = dynContratoServicos_UnidadeContratada_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2132ContratoServicos_UndCntNome = T000X10_A2132ContratoServicos_UndCntNome[0];
         n2132ContratoServicos_UndCntNome = T000X10_n2132ContratoServicos_UndCntNome[0];
         A1712ContratoServicos_UndCntSgl = T000X10_A1712ContratoServicos_UndCntSgl[0];
         n1712ContratoServicos_UndCntSgl = T000X10_n1712ContratoServicos_UndCntSgl[0];
         pr_default.close(7);
         lblContratoservicos_produtividade_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_produtividade_righttext_Internalname, "Caption", lblContratoservicos_produtividade_righttext_Caption);
         lblContratoservicos_limiteproposta_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_limiteproposta_righttext_Internalname, "Caption", lblContratoservicos_limiteproposta_righttext_Caption);
         if ( ! ( ( StringUtil.StrCmp(A639ContratoServicos_LocalExec, "A") == 0 ) || ( StringUtil.StrCmp(A639ContratoServicos_LocalExec, "E") == 0 ) || ( StringUtil.StrCmp(A639ContratoServicos_LocalExec, "O") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Local de Execu��o fora do intervalo", "OutOfRange", 1, "CONTRATOSERVICOS_LOCALEXEC");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicos_LocalExec_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1454ContratoServicos_PrazoTpDias, "U") == 0 ) || ( StringUtil.StrCmp(A1454ContratoServicos_PrazoTpDias, "C") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) ) )
         {
            GX_msglist.addItem("Campo Tipo de dias fora do intervalo", "OutOfRange", 1, "CONTRATOSERVICOS_PRAZOTPDIAS");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26CemPorCento", AV26CemPorCento);
         if ( AV26CemPorCento )
         {
            lblContratoservicos_prazocorrecao_righttext_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_prazocorrecao_righttext_Internalname, "Caption", lblContratoservicos_prazocorrecao_righttext_Caption);
         }
         if ( ( A1224ContratoServicos_PrazoCorrecao > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = "F";
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
         }
         else
         {
            if ( AV26CemPorCento )
            {
               A1225ContratoServicos_PrazoCorrecaoTipo = "I";
               n1225ContratoServicos_PrazoCorrecaoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            }
         }
         if ( ( A1224ContratoServicos_PrazoCorrecao > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) )
         {
            A1224ContratoServicos_PrazoCorrecao = 0;
            n1224ContratoServicos_PrazoCorrecao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            n1224ContratoServicos_PrazoCorrecao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 ) || AV26CemPorCento )
            {
               A1224ContratoServicos_PrazoCorrecao = 0;
               n1224ContratoServicos_PrazoCorrecao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            }
         }
         edtContratoServicos_PrazoCorrecao_Visible = (((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I")!=0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoCorrecao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoCorrecao_Visible), 5, 0)));
         if ( ! ( ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "B") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "S") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "E") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "A") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "R") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "C") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "D") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "H") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "O") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "P") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "L") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "X") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "N") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "J") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "I") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "T") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "Q") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "G") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "M") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc)) ) )
         {
            GX_msglist.addItem("Campo Pagar ao funcion�rio em fora do intervalo", "OutOfRange", 1, "CONTRATOSERVICOS_STATUSPAGFNC");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicos_StatusPagFnc_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( ( AV21Servico_Percentual >= Convert.ToDecimal( 0 )) && ( AV21Servico_Percentual <= Convert.ToDecimal( 100 )) ) || (Convert.ToDecimal(0)==AV21Servico_Percentual) ) )
         {
            GX_msglist.addItem("Campo Deflator fora do intervalo", "OutOfRange", 1, "vSERVICO_PERCENTUAL");
            AnyError = 1;
            GX_FocusControl = edtavServico_percentual_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV30Alias = A1858ContratoServicos_Alias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               AV30Alias = A1858ContratoServicos_Alias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
            }
         }
         AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
         /* Using cursor T000X9 */
         pr_default.execute(6, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A77Contrato_Numero = T000X9_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T000X9_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T000X9_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T000X9_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A116Contrato_ValorUnidadeContratacao = T000X9_A116Contrato_ValorUnidadeContratacao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
         A453Contrato_IndiceDivergencia = T000X9_A453Contrato_IndiceDivergencia[0];
         A39Contratada_Codigo = T000X9_A39Contratada_Codigo[0];
         A75Contrato_AreaTrabalhoCod = T000X9_A75Contrato_AreaTrabalhoCod[0];
         pr_default.close(6);
         /* Using cursor T000X12 */
         pr_default.execute(9, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A516Contratada_TipoFabrica = T000X12_A516Contratada_TipoFabrica[0];
         A40Contratada_PessoaCod = T000X12_A40Contratada_PessoaCod[0];
         pr_default.close(9);
         chkContratoServicos_EspelhaAceite.Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_EspelhaAceite.Visible), 5, 0)));
         lblTextblockcontratoservicos_espelhaaceite_Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicos_espelhaaceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicos_espelhaaceite_Visible), 5, 0)));
         /* Using cursor T000X13 */
         pr_default.execute(10, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000X13_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T000X13_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000X13_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T000X13_n42Contratada_PessoaCNPJ[0];
         pr_default.close(10);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1455ContratoServicos_IndiceDivergencia) && ( Gx_BScreen == 0 ) )
         {
            A1455ContratoServicos_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            n1455ContratoServicos_IndiceDivergencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A557Servico_VlrUnidadeContratada) && ( Gx_BScreen == 0 ) )
         {
            A557Servico_VlrUnidadeContratada = A116Contrato_ValorUnidadeContratacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ! A638ContratoServicos_Ativo && ( O638ContratoServicos_Ativo ) && new prc_numaregra(context).executeUdp( ref  A160ContratoServicos_Codigo) )
         {
            GX_msglist.addItem("Servi�o em uso por alguma regra de disparo autom�tico neste ou em outro contrato!", 1, "CONTRATOSERVICOS_ATIVO");
            AnyError = 1;
            GX_FocusControl = chkContratoServicos_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0X34( )
      {
         pr_default.close(5);
         pr_default.close(8);
         pr_default.close(7);
         pr_default.close(6);
         pr_default.close(9);
         pr_default.close(10);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_69( int A155Servico_Codigo )
      {
         /* Using cursor T000X22 */
         pr_default.execute(15, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A608Servico_Nome = T000X22_A608Servico_Nome[0];
         A605Servico_Sigla = T000X22_A605Servico_Sigla[0];
         A1061Servico_Tela = T000X22_A1061Servico_Tela[0];
         n1061Servico_Tela = T000X22_n1061Servico_Tela[0];
         A632Servico_Ativo = T000X22_A632Servico_Ativo[0];
         A156Servico_Descricao = T000X22_A156Servico_Descricao[0];
         n156Servico_Descricao = T000X22_n156Servico_Descricao[0];
         A2092Servico_IsOrigemReferencia = T000X22_A2092Servico_IsOrigemReferencia[0];
         A631Servico_Vinculado = T000X22_A631Servico_Vinculado[0];
         n631Servico_Vinculado = T000X22_n631Servico_Vinculado[0];
         A157ServicoGrupo_Codigo = T000X22_A157ServicoGrupo_Codigo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A608Servico_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A605Servico_Sigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1061Servico_Tela))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A632Servico_Ativo))+"\""+","+"\""+GXUtil.EncodeJSConstant( A156Servico_Descricao)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_72( int A157ServicoGrupo_Codigo )
      {
         /* Using cursor T000X23 */
         pr_default.execute(16, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A158ServicoGrupo_Descricao = T000X23_A158ServicoGrupo_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A158ServicoGrupo_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_71( int A1212ContratoServicos_UnidadeContratada )
      {
         /* Using cursor T000X24 */
         pr_default.execute(17, new Object[] {A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_UNIDADECONTRATADA");
            AnyError = 1;
            GX_FocusControl = dynContratoServicos_UnidadeContratada_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2132ContratoServicos_UndCntNome = T000X24_A2132ContratoServicos_UndCntNome[0];
         n2132ContratoServicos_UndCntNome = T000X24_n2132ContratoServicos_UndCntNome[0];
         A1712ContratoServicos_UndCntSgl = T000X24_A1712ContratoServicos_UndCntSgl[0];
         n1712ContratoServicos_UndCntSgl = T000X24_n1712ContratoServicos_UndCntSgl[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2132ContratoServicos_UndCntNome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1712ContratoServicos_UndCntSgl))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_70( int A74Contrato_Codigo )
      {
         /* Using cursor T000X25 */
         pr_default.execute(18, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A77Contrato_Numero = T000X25_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T000X25_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T000X25_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T000X25_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A116Contrato_ValorUnidadeContratacao = T000X25_A116Contrato_ValorUnidadeContratacao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
         A453Contrato_IndiceDivergencia = T000X25_A453Contrato_IndiceDivergencia[0];
         A39Contratada_Codigo = T000X25_A39Contratada_Codigo[0];
         A75Contrato_AreaTrabalhoCod = T000X25_A75Contrato_AreaTrabalhoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A77Contrato_Numero))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A78Contrato_NumeroAta))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void gxLoad_73( int A39Contratada_Codigo )
      {
         /* Using cursor T000X26 */
         pr_default.execute(19, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A516Contratada_TipoFabrica = T000X26_A516Contratada_TipoFabrica[0];
         A40Contratada_PessoaCod = T000X26_A40Contratada_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A516Contratada_TipoFabrica))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(19) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(19);
      }

      protected void gxLoad_74( int A40Contratada_PessoaCod )
      {
         /* Using cursor T000X27 */
         pr_default.execute(20, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000X27_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T000X27_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000X27_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T000X27_n42Contratada_PessoaCNPJ[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41Contratada_PessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A42Contratada_PessoaCNPJ)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void GetKey0X34( )
      {
         /* Using cursor T000X28 */
         pr_default.execute(21, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound34 = 1;
         }
         else
         {
            RcdFound34 = 0;
         }
         pr_default.close(21);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000X5 */
         pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM0X34( 67) ;
            RcdFound34 = 1;
            A160ContratoServicos_Codigo = T000X5_A160ContratoServicos_Codigo[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = T000X5_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            n1225ContratoServicos_PrazoCorrecaoTipo = T000X5_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1858ContratoServicos_Alias = T000X5_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = T000X5_n1858ContratoServicos_Alias[0];
            A555Servico_QtdContratada = T000X5_A555Servico_QtdContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
            n555Servico_QtdContratada = T000X5_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = T000X5_A557Servico_VlrUnidadeContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            A558Servico_Percentual = T000X5_A558Servico_Percentual[0];
            n558Servico_Percentual = T000X5_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = T000X5_A607ServicoContrato_Faturamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
            A639ContratoServicos_LocalExec = T000X5_A639ContratoServicos_LocalExec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
            A868ContratoServicos_TipoVnc = T000X5_A868ContratoServicos_TipoVnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
            n868ContratoServicos_TipoVnc = T000X5_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = T000X5_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = T000X5_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = T000X5_A1454ContratoServicos_PrazoTpDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
            n1454ContratoServicos_PrazoTpDias = T000X5_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = T000X5_A1152ContratoServicos_PrazoAnalise[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
            n1152ContratoServicos_PrazoAnalise = T000X5_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = T000X5_A1153ContratoServicos_PrazoResposta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
            n1153ContratoServicos_PrazoResposta = T000X5_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = T000X5_A1181ContratoServicos_PrazoGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
            n1181ContratoServicos_PrazoGarantia = T000X5_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = T000X5_A1182ContratoServicos_PrazoAtendeGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
            n1182ContratoServicos_PrazoAtendeGarantia = T000X5_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = T000X5_A1224ContratoServicos_PrazoCorrecao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
            n1224ContratoServicos_PrazoCorrecao = T000X5_n1224ContratoServicos_PrazoCorrecao[0];
            A1649ContratoServicos_PrazoInicio = T000X5_A1649ContratoServicos_PrazoInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
            n1649ContratoServicos_PrazoInicio = T000X5_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = T000X5_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = T000X5_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = T000X5_A1191ContratoServicos_Produtividade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
            n1191ContratoServicos_Produtividade = T000X5_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = T000X5_A1217ContratoServicos_EspelhaAceite[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1217ContratoServicos_EspelhaAceite", A1217ContratoServicos_EspelhaAceite);
            n1217ContratoServicos_EspelhaAceite = T000X5_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = T000X5_A1266ContratoServicos_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
            n1266ContratoServicos_Momento = T000X5_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = T000X5_A1325ContratoServicos_StatusPagFnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
            n1325ContratoServicos_StatusPagFnc = T000X5_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = T000X5_A1340ContratoServicos_QntUntCns[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
            n1340ContratoServicos_QntUntCns = T000X5_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = T000X5_A1341ContratoServicos_FatorCnvUndCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
            n1341ContratoServicos_FatorCnvUndCnt = T000X5_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = T000X5_A1397ContratoServicos_NaoRequerAtr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1397ContratoServicos_NaoRequerAtr", A1397ContratoServicos_NaoRequerAtr);
            n1397ContratoServicos_NaoRequerAtr = T000X5_n1397ContratoServicos_NaoRequerAtr[0];
            A1455ContratoServicos_IndiceDivergencia = T000X5_A1455ContratoServicos_IndiceDivergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
            n1455ContratoServicos_IndiceDivergencia = T000X5_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = T000X5_A1516ContratoServicos_TmpEstAnl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
            n1516ContratoServicos_TmpEstAnl = T000X5_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = T000X5_A1501ContratoServicos_TmpEstExc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
            n1501ContratoServicos_TmpEstExc = T000X5_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = T000X5_A1502ContratoServicos_TmpEstCrr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
            n1502ContratoServicos_TmpEstCrr = T000X5_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = T000X5_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = T000X5_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = T000X5_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = T000X5_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = T000X5_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = T000X5_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = T000X5_A1539ContratoServicos_PercCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
            n1539ContratoServicos_PercCnc = T000X5_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = T000X5_A638ContratoServicos_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
            A1723ContratoServicos_CodigoFiscal = T000X5_A1723ContratoServicos_CodigoFiscal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1723ContratoServicos_CodigoFiscal", A1723ContratoServicos_CodigoFiscal);
            n1723ContratoServicos_CodigoFiscal = T000X5_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = T000X5_A1799ContratoServicos_LimiteProposta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
            n1799ContratoServicos_LimiteProposta = T000X5_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = T000X5_A2074ContratoServicos_CalculoRmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
            n2074ContratoServicos_CalculoRmn = T000X5_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = T000X5_A2094ContratoServicos_SolicitaGestorSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
            A155Servico_Codigo = T000X5_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A74Contrato_Codigo = T000X5_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = T000X5_A1212ContratoServicos_UnidadeContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
            O638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
            O557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            sMode34 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0X34( ) ;
            if ( AnyError == 1 )
            {
               RcdFound34 = 0;
               InitializeNonKey0X34( ) ;
            }
            Gx_mode = sMode34;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound34 = 0;
            InitializeNonKey0X34( ) ;
            sMode34 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode34;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(3);
      }

      protected void getEqualNoModal( )
      {
         GetKey0X34( ) ;
         if ( RcdFound34 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound34 = 0;
         /* Using cursor T000X29 */
         pr_default.execute(22, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(22) != 101) )
         {
            while ( (pr_default.getStatus(22) != 101) && ( ( T000X29_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) ) )
            {
               pr_default.readNext(22);
            }
            if ( (pr_default.getStatus(22) != 101) && ( ( T000X29_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T000X29_A160ContratoServicos_Codigo[0];
               RcdFound34 = 1;
            }
         }
         pr_default.close(22);
      }

      protected void move_previous( )
      {
         RcdFound34 = 0;
         /* Using cursor T000X30 */
         pr_default.execute(23, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            while ( (pr_default.getStatus(23) != 101) && ( ( T000X30_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) ) )
            {
               pr_default.readNext(23);
            }
            if ( (pr_default.getStatus(23) != 101) && ( ( T000X30_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T000X30_A160ContratoServicos_Codigo[0];
               RcdFound34 = 1;
            }
         }
         pr_default.close(23);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0X34( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
            GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0X34( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound34 == 1 )
            {
               if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                  Update0X34( ) ;
                  GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
               {
                  /* Insert record */
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                  GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0X34( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                     n2073ContratoServicos_QtdRmn = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                     GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0X34( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
         {
            A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = cmbContratoServicos_PrazoTpDias_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0X34( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000X4 */
            pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(2) == 101) || ( StringUtil.StrCmp(Z1225ContratoServicos_PrazoCorrecaoTipo, T000X4_A1225ContratoServicos_PrazoCorrecaoTipo[0]) != 0 ) || ( StringUtil.StrCmp(Z1858ContratoServicos_Alias, T000X4_A1858ContratoServicos_Alias[0]) != 0 ) || ( Z555Servico_QtdContratada != T000X4_A555Servico_QtdContratada[0] ) || ( Z557Servico_VlrUnidadeContratada != T000X4_A557Servico_VlrUnidadeContratada[0] ) || ( Z558Servico_Percentual != T000X4_A558Servico_Percentual[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z607ServicoContrato_Faturamento, T000X4_A607ServicoContrato_Faturamento[0]) != 0 ) || ( StringUtil.StrCmp(Z639ContratoServicos_LocalExec, T000X4_A639ContratoServicos_LocalExec[0]) != 0 ) || ( StringUtil.StrCmp(Z868ContratoServicos_TipoVnc, T000X4_A868ContratoServicos_TipoVnc[0]) != 0 ) || ( Z888ContratoServicos_HmlSemCnf != T000X4_A888ContratoServicos_HmlSemCnf[0] ) || ( StringUtil.StrCmp(Z1454ContratoServicos_PrazoTpDias, T000X4_A1454ContratoServicos_PrazoTpDias[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1152ContratoServicos_PrazoAnalise != T000X4_A1152ContratoServicos_PrazoAnalise[0] ) || ( Z1153ContratoServicos_PrazoResposta != T000X4_A1153ContratoServicos_PrazoResposta[0] ) || ( Z1181ContratoServicos_PrazoGarantia != T000X4_A1181ContratoServicos_PrazoGarantia[0] ) || ( Z1182ContratoServicos_PrazoAtendeGarantia != T000X4_A1182ContratoServicos_PrazoAtendeGarantia[0] ) || ( Z1224ContratoServicos_PrazoCorrecao != T000X4_A1224ContratoServicos_PrazoCorrecao[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1649ContratoServicos_PrazoInicio != T000X4_A1649ContratoServicos_PrazoInicio[0] ) || ( Z1190ContratoServicos_PrazoImediato != T000X4_A1190ContratoServicos_PrazoImediato[0] ) || ( Z1191ContratoServicos_Produtividade != T000X4_A1191ContratoServicos_Produtividade[0] ) || ( Z1217ContratoServicos_EspelhaAceite != T000X4_A1217ContratoServicos_EspelhaAceite[0] ) || ( StringUtil.StrCmp(Z1266ContratoServicos_Momento, T000X4_A1266ContratoServicos_Momento[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1325ContratoServicos_StatusPagFnc, T000X4_A1325ContratoServicos_StatusPagFnc[0]) != 0 ) || ( Z1340ContratoServicos_QntUntCns != T000X4_A1340ContratoServicos_QntUntCns[0] ) || ( Z1341ContratoServicos_FatorCnvUndCnt != T000X4_A1341ContratoServicos_FatorCnvUndCnt[0] ) || ( Z1397ContratoServicos_NaoRequerAtr != T000X4_A1397ContratoServicos_NaoRequerAtr[0] ) || ( Z1455ContratoServicos_IndiceDivergencia != T000X4_A1455ContratoServicos_IndiceDivergencia[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1516ContratoServicos_TmpEstAnl != T000X4_A1516ContratoServicos_TmpEstAnl[0] ) || ( Z1501ContratoServicos_TmpEstExc != T000X4_A1501ContratoServicos_TmpEstExc[0] ) || ( Z1502ContratoServicos_TmpEstCrr != T000X4_A1502ContratoServicos_TmpEstCrr[0] ) || ( Z1531ContratoServicos_TipoHierarquia != T000X4_A1531ContratoServicos_TipoHierarquia[0] ) || ( Z1537ContratoServicos_PercTmp != T000X4_A1537ContratoServicos_PercTmp[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1538ContratoServicos_PercPgm != T000X4_A1538ContratoServicos_PercPgm[0] ) || ( Z1539ContratoServicos_PercCnc != T000X4_A1539ContratoServicos_PercCnc[0] ) || ( Z638ContratoServicos_Ativo != T000X4_A638ContratoServicos_Ativo[0] ) || ( StringUtil.StrCmp(Z1723ContratoServicos_CodigoFiscal, T000X4_A1723ContratoServicos_CodigoFiscal[0]) != 0 ) || ( Z1799ContratoServicos_LimiteProposta != T000X4_A1799ContratoServicos_LimiteProposta[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2074ContratoServicos_CalculoRmn != T000X4_A2074ContratoServicos_CalculoRmn[0] ) || ( Z2094ContratoServicos_SolicitaGestorSistema != T000X4_A2094ContratoServicos_SolicitaGestorSistema[0] ) || ( Z155Servico_Codigo != T000X4_A155Servico_Codigo[0] ) || ( Z74Contrato_Codigo != T000X4_A74Contrato_Codigo[0] ) || ( Z1212ContratoServicos_UnidadeContratada != T000X4_A1212ContratoServicos_UnidadeContratada[0] ) )
            {
               if ( StringUtil.StrCmp(Z1225ContratoServicos_PrazoCorrecaoTipo, T000X4_A1225ContratoServicos_PrazoCorrecaoTipo[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoCorrecaoTipo");
                  GXUtil.WriteLogRaw("Old: ",Z1225ContratoServicos_PrazoCorrecaoTipo);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1225ContratoServicos_PrazoCorrecaoTipo[0]);
               }
               if ( StringUtil.StrCmp(Z1858ContratoServicos_Alias, T000X4_A1858ContratoServicos_Alias[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_Alias");
                  GXUtil.WriteLogRaw("Old: ",Z1858ContratoServicos_Alias);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1858ContratoServicos_Alias[0]);
               }
               if ( Z555Servico_QtdContratada != T000X4_A555Servico_QtdContratada[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"Servico_QtdContratada");
                  GXUtil.WriteLogRaw("Old: ",Z555Servico_QtdContratada);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A555Servico_QtdContratada[0]);
               }
               if ( Z557Servico_VlrUnidadeContratada != T000X4_A557Servico_VlrUnidadeContratada[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"Servico_VlrUnidadeContratada");
                  GXUtil.WriteLogRaw("Old: ",Z557Servico_VlrUnidadeContratada);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A557Servico_VlrUnidadeContratada[0]);
               }
               if ( Z558Servico_Percentual != T000X4_A558Servico_Percentual[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"Servico_Percentual");
                  GXUtil.WriteLogRaw("Old: ",Z558Servico_Percentual);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A558Servico_Percentual[0]);
               }
               if ( StringUtil.StrCmp(Z607ServicoContrato_Faturamento, T000X4_A607ServicoContrato_Faturamento[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ServicoContrato_Faturamento");
                  GXUtil.WriteLogRaw("Old: ",Z607ServicoContrato_Faturamento);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A607ServicoContrato_Faturamento[0]);
               }
               if ( StringUtil.StrCmp(Z639ContratoServicos_LocalExec, T000X4_A639ContratoServicos_LocalExec[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_LocalExec");
                  GXUtil.WriteLogRaw("Old: ",Z639ContratoServicos_LocalExec);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A639ContratoServicos_LocalExec[0]);
               }
               if ( StringUtil.StrCmp(Z868ContratoServicos_TipoVnc, T000X4_A868ContratoServicos_TipoVnc[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_TipoVnc");
                  GXUtil.WriteLogRaw("Old: ",Z868ContratoServicos_TipoVnc);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A868ContratoServicos_TipoVnc[0]);
               }
               if ( Z888ContratoServicos_HmlSemCnf != T000X4_A888ContratoServicos_HmlSemCnf[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_HmlSemCnf");
                  GXUtil.WriteLogRaw("Old: ",Z888ContratoServicos_HmlSemCnf);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A888ContratoServicos_HmlSemCnf[0]);
               }
               if ( StringUtil.StrCmp(Z1454ContratoServicos_PrazoTpDias, T000X4_A1454ContratoServicos_PrazoTpDias[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoTpDias");
                  GXUtil.WriteLogRaw("Old: ",Z1454ContratoServicos_PrazoTpDias);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1454ContratoServicos_PrazoTpDias[0]);
               }
               if ( Z1152ContratoServicos_PrazoAnalise != T000X4_A1152ContratoServicos_PrazoAnalise[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoAnalise");
                  GXUtil.WriteLogRaw("Old: ",Z1152ContratoServicos_PrazoAnalise);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1152ContratoServicos_PrazoAnalise[0]);
               }
               if ( Z1153ContratoServicos_PrazoResposta != T000X4_A1153ContratoServicos_PrazoResposta[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoResposta");
                  GXUtil.WriteLogRaw("Old: ",Z1153ContratoServicos_PrazoResposta);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1153ContratoServicos_PrazoResposta[0]);
               }
               if ( Z1181ContratoServicos_PrazoGarantia != T000X4_A1181ContratoServicos_PrazoGarantia[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoGarantia");
                  GXUtil.WriteLogRaw("Old: ",Z1181ContratoServicos_PrazoGarantia);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1181ContratoServicos_PrazoGarantia[0]);
               }
               if ( Z1182ContratoServicos_PrazoAtendeGarantia != T000X4_A1182ContratoServicos_PrazoAtendeGarantia[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoAtendeGarantia");
                  GXUtil.WriteLogRaw("Old: ",Z1182ContratoServicos_PrazoAtendeGarantia);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1182ContratoServicos_PrazoAtendeGarantia[0]);
               }
               if ( Z1224ContratoServicos_PrazoCorrecao != T000X4_A1224ContratoServicos_PrazoCorrecao[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoCorrecao");
                  GXUtil.WriteLogRaw("Old: ",Z1224ContratoServicos_PrazoCorrecao);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1224ContratoServicos_PrazoCorrecao[0]);
               }
               if ( Z1649ContratoServicos_PrazoInicio != T000X4_A1649ContratoServicos_PrazoInicio[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoInicio");
                  GXUtil.WriteLogRaw("Old: ",Z1649ContratoServicos_PrazoInicio);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1649ContratoServicos_PrazoInicio[0]);
               }
               if ( Z1190ContratoServicos_PrazoImediato != T000X4_A1190ContratoServicos_PrazoImediato[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PrazoImediato");
                  GXUtil.WriteLogRaw("Old: ",Z1190ContratoServicos_PrazoImediato);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1190ContratoServicos_PrazoImediato[0]);
               }
               if ( Z1191ContratoServicos_Produtividade != T000X4_A1191ContratoServicos_Produtividade[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_Produtividade");
                  GXUtil.WriteLogRaw("Old: ",Z1191ContratoServicos_Produtividade);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1191ContratoServicos_Produtividade[0]);
               }
               if ( Z1217ContratoServicos_EspelhaAceite != T000X4_A1217ContratoServicos_EspelhaAceite[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_EspelhaAceite");
                  GXUtil.WriteLogRaw("Old: ",Z1217ContratoServicos_EspelhaAceite);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1217ContratoServicos_EspelhaAceite[0]);
               }
               if ( StringUtil.StrCmp(Z1266ContratoServicos_Momento, T000X4_A1266ContratoServicos_Momento[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_Momento");
                  GXUtil.WriteLogRaw("Old: ",Z1266ContratoServicos_Momento);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1266ContratoServicos_Momento[0]);
               }
               if ( StringUtil.StrCmp(Z1325ContratoServicos_StatusPagFnc, T000X4_A1325ContratoServicos_StatusPagFnc[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_StatusPagFnc");
                  GXUtil.WriteLogRaw("Old: ",Z1325ContratoServicos_StatusPagFnc);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1325ContratoServicos_StatusPagFnc[0]);
               }
               if ( Z1340ContratoServicos_QntUntCns != T000X4_A1340ContratoServicos_QntUntCns[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_QntUntCns");
                  GXUtil.WriteLogRaw("Old: ",Z1340ContratoServicos_QntUntCns);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1340ContratoServicos_QntUntCns[0]);
               }
               if ( Z1341ContratoServicos_FatorCnvUndCnt != T000X4_A1341ContratoServicos_FatorCnvUndCnt[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_FatorCnvUndCnt");
                  GXUtil.WriteLogRaw("Old: ",Z1341ContratoServicos_FatorCnvUndCnt);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1341ContratoServicos_FatorCnvUndCnt[0]);
               }
               if ( Z1397ContratoServicos_NaoRequerAtr != T000X4_A1397ContratoServicos_NaoRequerAtr[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_NaoRequerAtr");
                  GXUtil.WriteLogRaw("Old: ",Z1397ContratoServicos_NaoRequerAtr);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1397ContratoServicos_NaoRequerAtr[0]);
               }
               if ( Z1455ContratoServicos_IndiceDivergencia != T000X4_A1455ContratoServicos_IndiceDivergencia[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_IndiceDivergencia");
                  GXUtil.WriteLogRaw("Old: ",Z1455ContratoServicos_IndiceDivergencia);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1455ContratoServicos_IndiceDivergencia[0]);
               }
               if ( Z1516ContratoServicos_TmpEstAnl != T000X4_A1516ContratoServicos_TmpEstAnl[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_TmpEstAnl");
                  GXUtil.WriteLogRaw("Old: ",Z1516ContratoServicos_TmpEstAnl);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1516ContratoServicos_TmpEstAnl[0]);
               }
               if ( Z1501ContratoServicos_TmpEstExc != T000X4_A1501ContratoServicos_TmpEstExc[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_TmpEstExc");
                  GXUtil.WriteLogRaw("Old: ",Z1501ContratoServicos_TmpEstExc);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1501ContratoServicos_TmpEstExc[0]);
               }
               if ( Z1502ContratoServicos_TmpEstCrr != T000X4_A1502ContratoServicos_TmpEstCrr[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_TmpEstCrr");
                  GXUtil.WriteLogRaw("Old: ",Z1502ContratoServicos_TmpEstCrr);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1502ContratoServicos_TmpEstCrr[0]);
               }
               if ( Z1531ContratoServicos_TipoHierarquia != T000X4_A1531ContratoServicos_TipoHierarquia[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_TipoHierarquia");
                  GXUtil.WriteLogRaw("Old: ",Z1531ContratoServicos_TipoHierarquia);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1531ContratoServicos_TipoHierarquia[0]);
               }
               if ( Z1537ContratoServicos_PercTmp != T000X4_A1537ContratoServicos_PercTmp[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PercTmp");
                  GXUtil.WriteLogRaw("Old: ",Z1537ContratoServicos_PercTmp);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1537ContratoServicos_PercTmp[0]);
               }
               if ( Z1538ContratoServicos_PercPgm != T000X4_A1538ContratoServicos_PercPgm[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PercPgm");
                  GXUtil.WriteLogRaw("Old: ",Z1538ContratoServicos_PercPgm);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1538ContratoServicos_PercPgm[0]);
               }
               if ( Z1539ContratoServicos_PercCnc != T000X4_A1539ContratoServicos_PercCnc[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_PercCnc");
                  GXUtil.WriteLogRaw("Old: ",Z1539ContratoServicos_PercCnc);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1539ContratoServicos_PercCnc[0]);
               }
               if ( Z638ContratoServicos_Ativo != T000X4_A638ContratoServicos_Ativo[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z638ContratoServicos_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A638ContratoServicos_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z1723ContratoServicos_CodigoFiscal, T000X4_A1723ContratoServicos_CodigoFiscal[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_CodigoFiscal");
                  GXUtil.WriteLogRaw("Old: ",Z1723ContratoServicos_CodigoFiscal);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1723ContratoServicos_CodigoFiscal[0]);
               }
               if ( Z1799ContratoServicos_LimiteProposta != T000X4_A1799ContratoServicos_LimiteProposta[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_LimiteProposta");
                  GXUtil.WriteLogRaw("Old: ",Z1799ContratoServicos_LimiteProposta);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1799ContratoServicos_LimiteProposta[0]);
               }
               if ( Z2074ContratoServicos_CalculoRmn != T000X4_A2074ContratoServicos_CalculoRmn[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_CalculoRmn");
                  GXUtil.WriteLogRaw("Old: ",Z2074ContratoServicos_CalculoRmn);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A2074ContratoServicos_CalculoRmn[0]);
               }
               if ( Z2094ContratoServicos_SolicitaGestorSistema != T000X4_A2094ContratoServicos_SolicitaGestorSistema[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_SolicitaGestorSistema");
                  GXUtil.WriteLogRaw("Old: ",Z2094ContratoServicos_SolicitaGestorSistema);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A2094ContratoServicos_SolicitaGestorSistema[0]);
               }
               if ( Z155Servico_Codigo != T000X4_A155Servico_Codigo[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"Servico_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z155Servico_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A155Servico_Codigo[0]);
               }
               if ( Z74Contrato_Codigo != T000X4_A74Contrato_Codigo[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"Contrato_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z74Contrato_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A74Contrato_Codigo[0]);
               }
               if ( Z1212ContratoServicos_UnidadeContratada != T000X4_A1212ContratoServicos_UnidadeContratada[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicos_UnidadeContratada");
                  GXUtil.WriteLogRaw("Old: ",Z1212ContratoServicos_UnidadeContratada);
                  GXUtil.WriteLogRaw("Current: ",T000X4_A1212ContratoServicos_UnidadeContratada[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0X34( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0X34( 0) ;
            CheckOptimisticConcurrency0X34( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X34( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0X34( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000X31 */
                     pr_default.execute(24, new Object[] {n1225ContratoServicos_PrazoCorrecaoTipo, A1225ContratoServicos_PrazoCorrecaoTipo, n1858ContratoServicos_Alias, A1858ContratoServicos_Alias, n555Servico_QtdContratada, A555Servico_QtdContratada, A557Servico_VlrUnidadeContratada, n558Servico_Percentual, A558Servico_Percentual, A607ServicoContrato_Faturamento, A639ContratoServicos_LocalExec, n868ContratoServicos_TipoVnc, A868ContratoServicos_TipoVnc, n888ContratoServicos_HmlSemCnf, A888ContratoServicos_HmlSemCnf, n1454ContratoServicos_PrazoTpDias, A1454ContratoServicos_PrazoTpDias, n1152ContratoServicos_PrazoAnalise, A1152ContratoServicos_PrazoAnalise, n1153ContratoServicos_PrazoResposta, A1153ContratoServicos_PrazoResposta, n1181ContratoServicos_PrazoGarantia, A1181ContratoServicos_PrazoGarantia, n1182ContratoServicos_PrazoAtendeGarantia, A1182ContratoServicos_PrazoAtendeGarantia, n1224ContratoServicos_PrazoCorrecao, A1224ContratoServicos_PrazoCorrecao, n1649ContratoServicos_PrazoInicio, A1649ContratoServicos_PrazoInicio, n1190ContratoServicos_PrazoImediato, A1190ContratoServicos_PrazoImediato, n1191ContratoServicos_Produtividade, A1191ContratoServicos_Produtividade, n1217ContratoServicos_EspelhaAceite, A1217ContratoServicos_EspelhaAceite, n1266ContratoServicos_Momento, A1266ContratoServicos_Momento, n1325ContratoServicos_StatusPagFnc, A1325ContratoServicos_StatusPagFnc, n1340ContratoServicos_QntUntCns, A1340ContratoServicos_QntUntCns, n1341ContratoServicos_FatorCnvUndCnt, A1341ContratoServicos_FatorCnvUndCnt, n1397ContratoServicos_NaoRequerAtr, A1397ContratoServicos_NaoRequerAtr, n1455ContratoServicos_IndiceDivergencia, A1455ContratoServicos_IndiceDivergencia, n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, n1502ContratoServicos_TmpEstCrr, A1502ContratoServicos_TmpEstCrr, n1531ContratoServicos_TipoHierarquia, A1531ContratoServicos_TipoHierarquia, n1537ContratoServicos_PercTmp, A1537ContratoServicos_PercTmp, n1538ContratoServicos_PercPgm, A1538ContratoServicos_PercPgm, n1539ContratoServicos_PercCnc, A1539ContratoServicos_PercCnc, A638ContratoServicos_Ativo, n1723ContratoServicos_CodigoFiscal, A1723ContratoServicos_CodigoFiscal, n1799ContratoServicos_LimiteProposta, A1799ContratoServicos_LimiteProposta, n2074ContratoServicos_CalculoRmn, A2074ContratoServicos_CalculoRmn, A2094ContratoServicos_SolicitaGestorSistema, A155Servico_Codigo, A74Contrato_Codigo, A1212ContratoServicos_UnidadeContratada});
                     A160ContratoServicos_Codigo = T000X31_A160ContratoServicos_Codigo[0];
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0X34( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption0X0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0X34( ) ;
            }
            EndLevel0X34( ) ;
         }
         CloseExtendedTableCursors0X34( ) ;
      }

      protected void Update0X34( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X34( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X34( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0X34( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000X32 */
                     pr_default.execute(25, new Object[] {n1225ContratoServicos_PrazoCorrecaoTipo, A1225ContratoServicos_PrazoCorrecaoTipo, n1858ContratoServicos_Alias, A1858ContratoServicos_Alias, n555Servico_QtdContratada, A555Servico_QtdContratada, A557Servico_VlrUnidadeContratada, n558Servico_Percentual, A558Servico_Percentual, A607ServicoContrato_Faturamento, A639ContratoServicos_LocalExec, n868ContratoServicos_TipoVnc, A868ContratoServicos_TipoVnc, n888ContratoServicos_HmlSemCnf, A888ContratoServicos_HmlSemCnf, n1454ContratoServicos_PrazoTpDias, A1454ContratoServicos_PrazoTpDias, n1152ContratoServicos_PrazoAnalise, A1152ContratoServicos_PrazoAnalise, n1153ContratoServicos_PrazoResposta, A1153ContratoServicos_PrazoResposta, n1181ContratoServicos_PrazoGarantia, A1181ContratoServicos_PrazoGarantia, n1182ContratoServicos_PrazoAtendeGarantia, A1182ContratoServicos_PrazoAtendeGarantia, n1224ContratoServicos_PrazoCorrecao, A1224ContratoServicos_PrazoCorrecao, n1649ContratoServicos_PrazoInicio, A1649ContratoServicos_PrazoInicio, n1190ContratoServicos_PrazoImediato, A1190ContratoServicos_PrazoImediato, n1191ContratoServicos_Produtividade, A1191ContratoServicos_Produtividade, n1217ContratoServicos_EspelhaAceite, A1217ContratoServicos_EspelhaAceite, n1266ContratoServicos_Momento, A1266ContratoServicos_Momento, n1325ContratoServicos_StatusPagFnc, A1325ContratoServicos_StatusPagFnc, n1340ContratoServicos_QntUntCns, A1340ContratoServicos_QntUntCns, n1341ContratoServicos_FatorCnvUndCnt, A1341ContratoServicos_FatorCnvUndCnt, n1397ContratoServicos_NaoRequerAtr, A1397ContratoServicos_NaoRequerAtr, n1455ContratoServicos_IndiceDivergencia, A1455ContratoServicos_IndiceDivergencia, n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, n1502ContratoServicos_TmpEstCrr, A1502ContratoServicos_TmpEstCrr, n1531ContratoServicos_TipoHierarquia, A1531ContratoServicos_TipoHierarquia, n1537ContratoServicos_PercTmp, A1537ContratoServicos_PercTmp, n1538ContratoServicos_PercPgm, A1538ContratoServicos_PercPgm, n1539ContratoServicos_PercCnc, A1539ContratoServicos_PercCnc, A638ContratoServicos_Ativo, n1723ContratoServicos_CodigoFiscal, A1723ContratoServicos_CodigoFiscal, n1799ContratoServicos_LimiteProposta, A1799ContratoServicos_LimiteProposta, n2074ContratoServicos_CalculoRmn, A2074ContratoServicos_CalculoRmn, A2094ContratoServicos_SolicitaGestorSistema, A155Servico_Codigo, A74Contrato_Codigo, A1212ContratoServicos_UnidadeContratada, A160ContratoServicos_Codigo});
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
                     if ( (pr_default.getStatus(25) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0X34( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0X34( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0X34( ) ;
         }
         CloseExtendedTableCursors0X34( ) ;
      }

      protected void DeferredUpdate0X34( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0X34( ) ;
            AfterConfirm0X34( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0X34( ) ;
               if ( AnyError == 0 )
               {
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                  ScanStart0X228( ) ;
                  while ( RcdFound228 != 0 )
                  {
                     getByPrimaryKey0X228( ) ;
                     Delete0X228( ) ;
                     ScanNext0X228( ) ;
                     O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                     n2073ContratoServicos_QtdRmn = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                  }
                  ScanEnd0X228( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000X33 */
                     pr_default.execute(26, new Object[] {A160ContratoServicos_Codigo});
                     pr_default.close(26);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode34 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0X34( ) ;
         Gx_mode = sMode34;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0X34( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ! A638ContratoServicos_Ativo && ( O638ContratoServicos_Ativo ) && new prc_numaregra(context).executeUdp( ref  A160ContratoServicos_Codigo) )
            {
               GX_msglist.addItem("Servi�o em uso por alguma regra de disparo autom�tico neste ou em outro contrato!", 1, "CONTRATOSERVICOS_ATIVO");
               AnyError = 1;
               GX_FocusControl = chkContratoServicos_Ativo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            /* Using cursor T000X34 */
            pr_default.execute(27, new Object[] {A155Servico_Codigo});
            A608Servico_Nome = T000X34_A608Servico_Nome[0];
            A605Servico_Sigla = T000X34_A605Servico_Sigla[0];
            A1061Servico_Tela = T000X34_A1061Servico_Tela[0];
            n1061Servico_Tela = T000X34_n1061Servico_Tela[0];
            A632Servico_Ativo = T000X34_A632Servico_Ativo[0];
            A156Servico_Descricao = T000X34_A156Servico_Descricao[0];
            n156Servico_Descricao = T000X34_n156Servico_Descricao[0];
            A2092Servico_IsOrigemReferencia = T000X34_A2092Servico_IsOrigemReferencia[0];
            A631Servico_Vinculado = T000X34_A631Servico_Vinculado[0];
            n631Servico_Vinculado = T000X34_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = T000X34_A157ServicoGrupo_Codigo[0];
            pr_default.close(27);
            AV32Servico_Nome = A608Servico_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            /* Using cursor T000X35 */
            pr_default.execute(28, new Object[] {A157ServicoGrupo_Codigo});
            A158ServicoGrupo_Descricao = T000X35_A158ServicoGrupo_Descricao[0];
            pr_default.close(28);
            GXt_int2 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1551Servico_Responsavel = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
            GXt_int3 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A640Servico_Vinculados = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
            /* Using cursor T000X36 */
            pr_default.execute(29, new Object[] {A1212ContratoServicos_UnidadeContratada});
            A2132ContratoServicos_UndCntNome = T000X36_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = T000X36_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = T000X36_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = T000X36_n1712ContratoServicos_UndCntSgl[0];
            pr_default.close(29);
            lblContratoservicos_produtividade_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_produtividade_righttext_Internalname, "Caption", lblContratoservicos_produtividade_righttext_Caption);
            lblContratoservicos_limiteproposta_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_limiteproposta_righttext_Internalname, "Caption", lblContratoservicos_limiteproposta_righttext_Caption);
            AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26CemPorCento", AV26CemPorCento);
            if ( AV26CemPorCento )
            {
               lblContratoservicos_prazocorrecao_righttext_Caption = "";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratoservicos_prazocorrecao_righttext_Internalname, "Caption", lblContratoservicos_prazocorrecao_righttext_Caption);
            }
            edtContratoServicos_PrazoCorrecao_Visible = (((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I")!=0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoCorrecao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoCorrecao_Visible), 5, 0)));
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV30Alias = A1858ContratoServicos_Alias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  AV30Alias = A1858ContratoServicos_Alias;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
               }
            }
            AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
            /* Using cursor T000X37 */
            pr_default.execute(30, new Object[] {A74Contrato_Codigo});
            A77Contrato_Numero = T000X37_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000X37_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000X37_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000X37_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A116Contrato_ValorUnidadeContratacao = T000X37_A116Contrato_ValorUnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            A453Contrato_IndiceDivergencia = T000X37_A453Contrato_IndiceDivergencia[0];
            A39Contratada_Codigo = T000X37_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = T000X37_A75Contrato_AreaTrabalhoCod[0];
            pr_default.close(30);
            /* Using cursor T000X38 */
            pr_default.execute(31, new Object[] {A39Contratada_Codigo});
            A516Contratada_TipoFabrica = T000X38_A516Contratada_TipoFabrica[0];
            A40Contratada_PessoaCod = T000X38_A40Contratada_PessoaCod[0];
            pr_default.close(31);
            chkContratoServicos_EspelhaAceite.Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_EspelhaAceite.Visible), 5, 0)));
            lblTextblockcontratoservicos_espelhaaceite_Visible = ((StringUtil.StrCmp(A516Contratada_TipoFabrica, "M")==0) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicos_espelhaaceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicos_espelhaaceite_Visible), 5, 0)));
            /* Using cursor T000X39 */
            pr_default.execute(32, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T000X39_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000X39_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000X39_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000X39_n42Contratada_PessoaCNPJ[0];
            pr_default.close(32);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000X40 */
            pr_default.execute(33, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor T000X41 */
            pr_default.execute(34, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor T000X42 */
            pr_default.execute(35, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Custo do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor T000X43 */
            pr_default.execute(36, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prioridade"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor T000X44 */
            pr_default.execute(37, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos Indicador"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor T000X45 */
            pr_default.execute(38, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Agenda Atendimento"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor T000X46 */
            pr_default.execute(39, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tela associada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor T000X47 */
            pr_default.execute(40, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Regra de Vinculo entre Servi�os"+" ("+"Contrato Servicos Vnc_Srv Vnc Contrato Servicos"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor T000X48 */
            pr_default.execute(41, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Regra de Vinculo entre Servi�os"+" ("+"Contrato Servicos Vnc_ContratoServicos"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor T000X49 */
            pr_default.execute(42, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prazo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor T000X50 */
            pr_default.execute(43, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidades de Convers�o do Servi�os do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor T000X51 */
            pr_default.execute(44, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor T000X52 */
            pr_default.execute(45, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servicos (De Para)"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor T000X53 */
            pr_default.execute(46, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
         }
      }

      protected void ProcessNestedLevel0X228( )
      {
         s2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         nGXsfl_312_idx = 0;
         while ( nGXsfl_312_idx < nRC_GXsfl_312 )
         {
            ReadRow0X228( ) ;
            if ( ( nRcdExists_228 != 0 ) || ( nIsMod_228 != 0 ) )
            {
               standaloneNotModal0X228( ) ;
               GetKey0X228( ) ;
               if ( ( nRcdExists_228 == 0 ) && ( nRcdDeleted_228 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert0X228( ) ;
               }
               else
               {
                  if ( RcdFound228 != 0 )
                  {
                     if ( ( nRcdDeleted_228 != 0 ) && ( nRcdExists_228 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete0X228( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_228 != 0 ) && ( nRcdExists_228 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update0X228( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_228 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
            }
            ChangePostValue( edtContratoServicosRmn_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosRmn_Inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosRmn_Fim_Internalname, StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosRmn_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2069ContratoServicosRmn_Sequencial_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2069ContratoServicosRmn_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2070ContratoServicosRmn_Inicio_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( Z2070ContratoServicosRmn_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2071ContratoServicosRmn_Fim_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( Z2071ContratoServicosRmn_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2072ContratoServicosRmn_Valor_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( Z2072ContratoServicosRmn_Valor, 14, 5, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_228_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_228), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_228_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_228), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_228_"+sGXsfl_312_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_228), 4, 0, ",", ""))) ;
            if ( nIsMod_228 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Inicio_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Fim_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Valor_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0X228( ) ;
         if ( AnyError != 0 )
         {
            O2073ContratoServicos_QtdRmn = s2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         }
         nRcdExists_228 = 0;
         nIsMod_228 = 0;
         nRcdDeleted_228 = 0;
      }

      protected void ProcessLevel0X34( )
      {
         /* Save parent mode. */
         sMode34 = Gx_mode;
         ProcessNestedLevel0X228( ) ;
         if ( AnyError != 0 )
         {
            O2073ContratoServicos_QtdRmn = s2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode34;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
      }

      protected void EndLevel0X34( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(2);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(27);
            pr_default.close(30);
            pr_default.close(29);
            pr_default.close(28);
            pr_default.close(31);
            pr_default.close(32);
            context.CommitDataStores( "ContratoServicos");
            if ( AnyError == 0 )
            {
               ConfirmValues0X0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(27);
            pr_default.close(30);
            pr_default.close(29);
            pr_default.close(28);
            pr_default.close(31);
            pr_default.close(32);
            context.RollbackDataStores( "ContratoServicos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0X34( )
      {
         /* Scan By routine */
         /* Using cursor T000X54 */
         pr_default.execute(47);
         RcdFound34 = 0;
         if ( (pr_default.getStatus(47) != 101) )
         {
            RcdFound34 = 1;
            A160ContratoServicos_Codigo = T000X54_A160ContratoServicos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0X34( )
      {
         /* Scan next routine */
         pr_default.readNext(47);
         RcdFound34 = 0;
         if ( (pr_default.getStatus(47) != 101) )
         {
            RcdFound34 = 1;
            A160ContratoServicos_Codigo = T000X54_A160ContratoServicos_Codigo[0];
         }
      }

      protected void ScanEnd0X34( )
      {
         pr_default.close(47);
      }

      protected void AfterConfirm0X34( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0X34( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0X34( )
      {
         /* Before Update Rules */
         new loadauditcontratoservicos(context ).execute(  "Y", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete0X34( )
      {
         /* Before Delete Rules */
         new loadauditcontratoservicos(context ).execute(  "Y", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete0X34( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicos(context ).execute(  "N", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicos(context ).execute(  "N", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate0X34( )
      {
         /* Before Validate Rules */
         A558Servico_Percentual = (decimal)(AV21Servico_Percentual/ (decimal)(100));
         n558Servico_Percentual = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A558Servico_Percentual", StringUtil.LTrim( StringUtil.Str( A558Servico_Percentual, 7, 3)));
         A1858ContratoServicos_Alias = AV30Alias;
         n1858ContratoServicos_Alias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1858ContratoServicos_Alias", A1858ContratoServicos_Alias);
      }

      protected void DisableAttributes0X34( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContrato_ValorUnidadeContratacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_ValorUnidadeContratacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_ValorUnidadeContratacao_Enabled), 5, 0)));
         dynavServicogrupo_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo.Enabled), 5, 0)));
         dynavServico_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_codigo.Enabled), 5, 0)));
         dynavSubservico_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSubservico_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSubservico_codigo.Enabled), 5, 0)));
         edtavServico_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome_Enabled), 5, 0)));
         edtavAlias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAlias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAlias_Enabled), 5, 0)));
         cmbContratoServicos_PrazoTpDias.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoTpDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_PrazoTpDias.Enabled), 5, 0)));
         cmbContratoServicos_PrazoInicio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_PrazoInicio.Enabled), 5, 0)));
         edtContratoServicos_PrazoAnalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoAnalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoAnalise_Enabled), 5, 0)));
         cmbContratoServicos_PrazoTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_PrazoTipo.Enabled), 5, 0)));
         edtContratoServicos_PrazoDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoDias_Enabled), 5, 0)));
         edtContratoServicos_PrazoResposta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoResposta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoResposta_Enabled), 5, 0)));
         edtContratoServicos_PrazoGarantia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoGarantia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoGarantia_Enabled), 5, 0)));
         edtContratoServicos_PrazoAtendeGarantia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoAtendeGarantia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoAtendeGarantia_Enabled), 5, 0)));
         cmbContratoServicos_PrazoCorrecaoTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_PrazoCorrecaoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_PrazoCorrecaoTipo.Enabled), 5, 0)));
         edtContratoServicos_PrazoCorrecao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PrazoCorrecao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PrazoCorrecao_Enabled), 5, 0)));
         edtContratoServicos_TmpEstAnl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_TmpEstAnl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_TmpEstAnl_Enabled), 5, 0)));
         edtContratoServicos_TmpEstExc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_TmpEstExc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_TmpEstExc_Enabled), 5, 0)));
         edtContratoServicos_TmpEstCrr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_TmpEstCrr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_TmpEstCrr_Enabled), 5, 0)));
         cmbContratoServicos_StatusPagFnc.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_StatusPagFnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_StatusPagFnc.Enabled), 5, 0)));
         edtContratoServicos_IndiceDivergencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_IndiceDivergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_IndiceDivergencia_Enabled), 5, 0)));
         chkContratoServicos_NaoRequerAtr.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_NaoRequerAtr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_NaoRequerAtr.Enabled), 5, 0)));
         edtContratoServicos_LimiteProposta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_LimiteProposta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_LimiteProposta_Enabled), 5, 0)));
         cmbContratoServicos_LocalExec.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_LocalExec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_LocalExec.Enabled), 5, 0)));
         dynContratoServicos_UnidadeContratada.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicos_UnidadeContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicos_UnidadeContratada.Enabled), 5, 0)));
         edtContratoServicos_FatorCnvUndCnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_FatorCnvUndCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_FatorCnvUndCnt_Enabled), 5, 0)));
         edtServico_QtdContratada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_QtdContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_QtdContratada_Enabled), 5, 0)));
         edtServico_VlrUnidadeContratada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_VlrUnidadeContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_VlrUnidadeContratada_Enabled), 5, 0)));
         edtContratoServicos_Produtividade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Produtividade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Produtividade_Enabled), 5, 0)));
         cmbServicoContrato_Faturamento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServicoContrato_Faturamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServicoContrato_Faturamento.Enabled), 5, 0)));
         edtavServico_percentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_percentual_Enabled), 5, 0)));
         cmbContratoServicos_CalculoRmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_CalculoRmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_CalculoRmn.Enabled), 5, 0)));
         edtContratoServicos_PercCnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_PercCnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_PercCnc_Enabled), 5, 0)));
         chkContratoServicos_EspelhaAceite.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_EspelhaAceite.Enabled), 5, 0)));
         cmbContratoServicos_Momento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_Momento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_Momento.Enabled), 5, 0)));
         cmbContratoServicos_TipoVnc.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicos_TipoVnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicos_TipoVnc.Enabled), 5, 0)));
         edtContratoServicos_QntUntCns_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_QntUntCns_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_QntUntCns_Enabled), 5, 0)));
         edtContratoServicos_CodigoFiscal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_CodigoFiscal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_CodigoFiscal_Enabled), 5, 0)));
         chkContratoServicos_SolicitaGestorSistema.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_SolicitaGestorSistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_SolicitaGestorSistema.Enabled), 5, 0)));
         chkContratoServicos_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicos_Ativo.Enabled), 5, 0)));
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
      }

      protected void ZM0X228( short GX_JID )
      {
         if ( ( GX_JID == 78 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2070ContratoServicosRmn_Inicio = T000X3_A2070ContratoServicosRmn_Inicio[0];
               Z2071ContratoServicosRmn_Fim = T000X3_A2071ContratoServicosRmn_Fim[0];
               Z2072ContratoServicosRmn_Valor = T000X3_A2072ContratoServicosRmn_Valor[0];
            }
            else
            {
               Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
               Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
               Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
            }
         }
         if ( GX_JID == -78 )
         {
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
      }

      protected void standaloneNotModal0X228( )
      {
         edtContratoServicosRmn_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0)));
      }

      protected void standaloneModal0X228( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A2069ContratoServicosRmn_Sequencial = A2073ContratoServicos_QtdRmn;
         }
      }

      protected void Load0X228( )
      {
         /* Using cursor T000X55 */
         pr_default.execute(48, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
         if ( (pr_default.getStatus(48) != 101) )
         {
            RcdFound228 = 1;
            A2070ContratoServicosRmn_Inicio = T000X55_A2070ContratoServicosRmn_Inicio[0];
            A2071ContratoServicosRmn_Fim = T000X55_A2071ContratoServicosRmn_Fim[0];
            A2072ContratoServicosRmn_Valor = T000X55_A2072ContratoServicosRmn_Valor[0];
            ZM0X228( -78) ;
         }
         pr_default.close(48);
         OnLoadActions0X228( ) ;
      }

      protected void OnLoadActions0X228( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn-1);
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable0X228( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal0X228( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
            n2073ContratoServicos_QtdRmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn-1);
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               }
            }
         }
      }

      protected void CloseExtendedTableCursors0X228( )
      {
      }

      protected void enableDisable0X228( )
      {
      }

      protected void GetKey0X228( )
      {
         /* Using cursor T000X56 */
         pr_default.execute(49, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
         if ( (pr_default.getStatus(49) != 101) )
         {
            RcdFound228 = 1;
         }
         else
         {
            RcdFound228 = 0;
         }
         pr_default.close(49);
      }

      protected void getByPrimaryKey0X228( )
      {
         /* Using cursor T000X3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0X228( 78) ;
            RcdFound228 = 1;
            InitializeNonKey0X228( ) ;
            A2069ContratoServicosRmn_Sequencial = T000X3_A2069ContratoServicosRmn_Sequencial[0];
            A2070ContratoServicosRmn_Inicio = T000X3_A2070ContratoServicosRmn_Inicio[0];
            A2071ContratoServicosRmn_Fim = T000X3_A2071ContratoServicosRmn_Fim[0];
            A2072ContratoServicosRmn_Valor = T000X3_A2072ContratoServicosRmn_Valor[0];
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            sMode228 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0X228( ) ;
            Gx_mode = sMode228;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound228 = 0;
            InitializeNonKey0X228( ) ;
            sMode228 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal0X228( ) ;
            Gx_mode = sMode228;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0X228( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0X228( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000X2 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosRmn"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2070ContratoServicosRmn_Inicio != T000X2_A2070ContratoServicosRmn_Inicio[0] ) || ( Z2071ContratoServicosRmn_Fim != T000X2_A2071ContratoServicosRmn_Fim[0] ) || ( Z2072ContratoServicosRmn_Valor != T000X2_A2072ContratoServicosRmn_Valor[0] ) )
            {
               if ( Z2070ContratoServicosRmn_Inicio != T000X2_A2070ContratoServicosRmn_Inicio[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicosRmn_Inicio");
                  GXUtil.WriteLogRaw("Old: ",Z2070ContratoServicosRmn_Inicio);
                  GXUtil.WriteLogRaw("Current: ",T000X2_A2070ContratoServicosRmn_Inicio[0]);
               }
               if ( Z2071ContratoServicosRmn_Fim != T000X2_A2071ContratoServicosRmn_Fim[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicosRmn_Fim");
                  GXUtil.WriteLogRaw("Old: ",Z2071ContratoServicosRmn_Fim);
                  GXUtil.WriteLogRaw("Current: ",T000X2_A2071ContratoServicosRmn_Fim[0]);
               }
               if ( Z2072ContratoServicosRmn_Valor != T000X2_A2072ContratoServicosRmn_Valor[0] )
               {
                  GXUtil.WriteLog("contratoservicos:[seudo value changed for attri]"+"ContratoServicosRmn_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z2072ContratoServicosRmn_Valor);
                  GXUtil.WriteLogRaw("Current: ",T000X2_A2072ContratoServicosRmn_Valor[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosRmn"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0X228( )
      {
         BeforeValidate0X228( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X228( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0X228( 0) ;
            CheckOptimisticConcurrency0X228( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X228( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0X228( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000X57 */
                     pr_default.execute(50, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial, A2070ContratoServicosRmn_Inicio, A2071ContratoServicosRmn_Fim, A2072ContratoServicosRmn_Valor});
                     pr_default.close(50);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
                     if ( (pr_default.getStatus(50) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0X228( ) ;
            }
            EndLevel0X228( ) ;
         }
         CloseExtendedTableCursors0X228( ) ;
      }

      protected void Update0X228( )
      {
         BeforeValidate0X228( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X228( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X228( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X228( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0X228( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000X58 */
                     pr_default.execute(51, new Object[] {A2070ContratoServicosRmn_Inicio, A2071ContratoServicosRmn_Fim, A2072ContratoServicosRmn_Valor, A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
                     pr_default.close(51);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
                     if ( (pr_default.getStatus(51) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosRmn"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0X228( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey0X228( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0X228( ) ;
         }
         CloseExtendedTableCursors0X228( ) ;
      }

      protected void DeferredUpdate0X228( )
      {
      }

      protected void Delete0X228( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate0X228( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X228( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0X228( ) ;
            AfterConfirm0X228( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0X228( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000X59 */
                  pr_default.execute(52, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
                  pr_default.close(52);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode228 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0X228( ) ;
         Gx_mode = sMode228;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0X228( )
      {
         standaloneModal0X228( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
               n2073ContratoServicos_QtdRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn-1);
                     n2073ContratoServicos_QtdRmn = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
                  }
               }
            }
         }
      }

      protected void EndLevel0X228( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0X228( )
      {
         /* Scan By routine */
         /* Using cursor T000X60 */
         pr_default.execute(53, new Object[] {A160ContratoServicos_Codigo});
         RcdFound228 = 0;
         if ( (pr_default.getStatus(53) != 101) )
         {
            RcdFound228 = 1;
            A2069ContratoServicosRmn_Sequencial = T000X60_A2069ContratoServicosRmn_Sequencial[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0X228( )
      {
         /* Scan next routine */
         pr_default.readNext(53);
         RcdFound228 = 0;
         if ( (pr_default.getStatus(53) != 101) )
         {
            RcdFound228 = 1;
            A2069ContratoServicosRmn_Sequencial = T000X60_A2069ContratoServicosRmn_Sequencial[0];
         }
      }

      protected void ScanEnd0X228( )
      {
         pr_default.close(53);
      }

      protected void AfterConfirm0X228( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0X228( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0X228( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0X228( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0X228( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0X228( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0X228( )
      {
         edtContratoServicosRmn_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0)));
         edtContratoServicosRmn_Inicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Inicio_Enabled), 5, 0)));
         edtContratoServicosRmn_Fim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Fim_Enabled), 5, 0)));
         edtContratoServicosRmn_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Valor_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_312228( )
      {
         edtContratoServicosRmn_Sequencial_Internalname = "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_idx;
         edtContratoServicosRmn_Inicio_Internalname = "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_idx;
         edtContratoServicosRmn_Fim_Internalname = "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_idx;
         edtContratoServicosRmn_Valor_Internalname = "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_idx;
      }

      protected void SubsflControlProps_fel_312228( )
      {
         edtContratoServicosRmn_Sequencial_Internalname = "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_fel_idx;
         edtContratoServicosRmn_Inicio_Internalname = "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_fel_idx;
         edtContratoServicosRmn_Fim_Internalname = "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_fel_idx;
         edtContratoServicosRmn_Valor_Internalname = "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_fel_idx;
      }

      protected void AddRow0X228( )
      {
         nGXsfl_312_idx = (short)(nGXsfl_312_idx+1);
         sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx), 4, 0)), 4, "0");
         SubsflControlProps_312228( ) ;
         SendRow0X228( ) ;
      }

      protected void SendRow0X228( )
      {
         GridRow = GXWebRow.GetNew(context);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_312_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2069ContratoServicosRmn_Sequencial), 4, 0, ",", "")),((edtContratoServicosRmn_Sequencial_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2069ContratoServicosRmn_Sequencial), "ZZZ9")) : context.localUtil.Format( (decimal)(A2069ContratoServicosRmn_Sequencial), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtContratoServicosRmn_Sequencial_Enabled,(short)0,(String)"text",(String)"",(short)25,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)312,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_228_" + sGXsfl_312_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 314,'',false,'" + sGXsfl_312_idx + "',312)\"";
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Inicio_Internalname,StringUtil.LTrim( StringUtil.NToC( A2070ContratoServicosRmn_Inicio, 14, 5, ",", "")),((edtContratoServicosRmn_Inicio_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2070ContratoServicosRmn_Inicio, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A2070ContratoServicosRmn_Inicio, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,314);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosRmn_Inicio_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)312,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_228_" + sGXsfl_312_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 315,'',false,'" + sGXsfl_312_idx + "',312)\"";
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Fim_Internalname,StringUtil.LTrim( StringUtil.NToC( A2071ContratoServicosRmn_Fim, 14, 5, ",", "")),((edtContratoServicosRmn_Fim_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2071ContratoServicosRmn_Fim, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A2071ContratoServicosRmn_Fim, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,315);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosRmn_Fim_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)312,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_228_" + sGXsfl_312_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 316,'',false,'" + sGXsfl_312_idx + "',312)\"";
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosRmn_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A2072ContratoServicosRmn_Valor, 14, 5, ",", "")),((edtContratoServicosRmn_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2072ContratoServicosRmn_Valor, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A2072ContratoServicosRmn_Valor, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,316);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosRmn_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosRmn_Valor_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)312,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(GridRow);
         GXCCtl = "Z2069ContratoServicosRmn_Sequencial_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2069ContratoServicosRmn_Sequencial), 4, 0, ",", "")));
         GXCCtl = "Z2070ContratoServicosRmn_Inicio_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z2070ContratoServicosRmn_Inicio, 14, 5, ",", "")));
         GXCCtl = "Z2071ContratoServicosRmn_Fim_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z2071ContratoServicosRmn_Fim, 14, 5, ",", "")));
         GXCCtl = "Z2072ContratoServicosRmn_Valor_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z2072ContratoServicosRmn_Valor, 14, 5, ",", "")));
         GXCCtl = "nRcdDeleted_228_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_228), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_228_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_228), 4, 0, ",", "")));
         GXCCtl = "nIsMod_228_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_228), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "CONTRATOSERVICOS_CODIGO_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GXCCtl = "CONTRATO_AREATRABALHOCOD_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GXCCtl = "vAUDITINGOBJECT_" + sGXsfl_312_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV29AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV29AuditingObject);
         }
         GXCCtl = "vPGMNAME_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( AV36Pgmname));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_312_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vCONTRATOSERVICOS_CODIGO_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GXCCtl = "CONTRATOSERVICOS_PRAZOS_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A911ContratoServicos_Prazos), 4, 0, ",", "")));
         GXCCtl = "vCONTRATO_CODIGO_" + sGXsfl_312_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Inicio_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Fim_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosRmn_Valor_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         GridContainer.AddRow(GridRow);
      }

      protected void ReadRow0X228( )
      {
         nGXsfl_312_idx = (short)(nGXsfl_312_idx+1);
         sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx), 4, 0)), 4, "0");
         SubsflControlProps_312228( ) ;
         edtContratoServicosRmn_Sequencial_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_SEQUENCIAL_"+sGXsfl_312_idx+"Enabled"), ",", "."));
         edtContratoServicosRmn_Inicio_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_INICIO_"+sGXsfl_312_idx+"Enabled"), ",", "."));
         edtContratoServicosRmn_Fim_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_FIM_"+sGXsfl_312_idx+"Enabled"), ",", "."));
         edtContratoServicosRmn_Valor_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSRMN_VALOR_"+sGXsfl_312_idx+"Enabled"), ",", "."));
         A2069ContratoServicosRmn_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Sequencial_Internalname), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Inicio_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Inicio_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSRMN_INICIO_" + sGXsfl_312_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosRmn_Inicio_Internalname;
            wbErr = true;
            A2070ContratoServicosRmn_Inicio = 0;
         }
         else
         {
            A2070ContratoServicosRmn_Inicio = context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Inicio_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Fim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Fim_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSRMN_FIM_" + sGXsfl_312_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosRmn_Fim_Internalname;
            wbErr = true;
            A2071ContratoServicosRmn_Fim = 0;
         }
         else
         {
            A2071ContratoServicosRmn_Fim = context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Fim_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Valor_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Valor_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSRMN_VALOR_" + sGXsfl_312_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosRmn_Valor_Internalname;
            wbErr = true;
            A2072ContratoServicosRmn_Valor = 0;
         }
         else
         {
            A2072ContratoServicosRmn_Valor = context.localUtil.CToN( cgiGet( edtContratoServicosRmn_Valor_Internalname), ",", ".");
         }
         GXCCtl = "Z2069ContratoServicosRmn_Sequencial_" + sGXsfl_312_idx;
         Z2069ContratoServicosRmn_Sequencial = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z2070ContratoServicosRmn_Inicio_" + sGXsfl_312_idx;
         Z2070ContratoServicosRmn_Inicio = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z2071ContratoServicosRmn_Fim_" + sGXsfl_312_idx;
         Z2071ContratoServicosRmn_Fim = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z2072ContratoServicosRmn_Valor_" + sGXsfl_312_idx;
         Z2072ContratoServicosRmn_Valor = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "nRcdDeleted_228_" + sGXsfl_312_idx;
         nRcdDeleted_228 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_228_" + sGXsfl_312_idx;
         nRcdExists_228 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_228_" + sGXsfl_312_idx;
         nIsMod_228 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtContratoServicosRmn_Sequencial_Enabled = edtContratoServicosRmn_Sequencial_Enabled;
      }

      protected void ConfirmValues0X0( )
      {
         nGXsfl_312_idx = 0;
         sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx), 4, 0)), 4, "0");
         SubsflControlProps_312228( ) ;
         while ( nGXsfl_312_idx < nRC_GXsfl_312 )
         {
            nGXsfl_312_idx = (short)(nGXsfl_312_idx+1);
            sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx), 4, 0)), 4, "0");
            SubsflControlProps_312228( ) ;
            ChangePostValue( "Z2069ContratoServicosRmn_Sequencial_"+sGXsfl_312_idx, cgiGet( "ZT_"+"Z2069ContratoServicosRmn_Sequencial_"+sGXsfl_312_idx)) ;
            DeletePostValue( "ZT_"+"Z2069ContratoServicosRmn_Sequencial_"+sGXsfl_312_idx) ;
            ChangePostValue( "Z2070ContratoServicosRmn_Inicio_"+sGXsfl_312_idx, cgiGet( "ZT_"+"Z2070ContratoServicosRmn_Inicio_"+sGXsfl_312_idx)) ;
            DeletePostValue( "ZT_"+"Z2070ContratoServicosRmn_Inicio_"+sGXsfl_312_idx) ;
            ChangePostValue( "Z2071ContratoServicosRmn_Fim_"+sGXsfl_312_idx, cgiGet( "ZT_"+"Z2071ContratoServicosRmn_Fim_"+sGXsfl_312_idx)) ;
            DeletePostValue( "ZT_"+"Z2071ContratoServicosRmn_Fim_"+sGXsfl_312_idx) ;
            ChangePostValue( "Z2072ContratoServicosRmn_Valor_"+sGXsfl_312_idx, cgiGet( "ZT_"+"Z2072ContratoServicosRmn_Valor_"+sGXsfl_312_idx)) ;
            DeletePostValue( "ZT_"+"Z2072ContratoServicosRmn_Valor_"+sGXsfl_312_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621616471");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicos_Codigo) + "," + UrlEncode("" +AV27Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1225ContratoServicos_PrazoCorrecaoTipo", StringUtil.RTrim( Z1225ContratoServicos_PrazoCorrecaoTipo));
         GxWebStd.gx_hidden_field( context, "Z1858ContratoServicos_Alias", StringUtil.RTrim( Z1858ContratoServicos_Alias));
         GxWebStd.gx_hidden_field( context, "Z555Servico_QtdContratada", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z555Servico_QtdContratada), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.NToC( Z557Servico_VlrUnidadeContratada, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z558Servico_Percentual", StringUtil.LTrim( StringUtil.NToC( Z558Servico_Percentual, 7, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z607ServicoContrato_Faturamento", Z607ServicoContrato_Faturamento);
         GxWebStd.gx_hidden_field( context, "Z639ContratoServicos_LocalExec", StringUtil.RTrim( Z639ContratoServicos_LocalExec));
         GxWebStd.gx_hidden_field( context, "Z868ContratoServicos_TipoVnc", StringUtil.RTrim( Z868ContratoServicos_TipoVnc));
         GxWebStd.gx_boolean_hidden_field( context, "Z888ContratoServicos_HmlSemCnf", Z888ContratoServicos_HmlSemCnf);
         GxWebStd.gx_hidden_field( context, "Z1454ContratoServicos_PrazoTpDias", StringUtil.RTrim( Z1454ContratoServicos_PrazoTpDias));
         GxWebStd.gx_hidden_field( context, "Z1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1152ContratoServicos_PrazoAnalise), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1153ContratoServicos_PrazoResposta), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1181ContratoServicos_PrazoGarantia), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1182ContratoServicos_PrazoAtendeGarantia), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1224ContratoServicos_PrazoCorrecao), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1649ContratoServicos_PrazoInicio), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1190ContratoServicos_PrazoImediato", Z1190ContratoServicos_PrazoImediato);
         GxWebStd.gx_hidden_field( context, "Z1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.NToC( Z1191ContratoServicos_Produtividade, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1217ContratoServicos_EspelhaAceite", Z1217ContratoServicos_EspelhaAceite);
         GxWebStd.gx_hidden_field( context, "Z1266ContratoServicos_Momento", StringUtil.RTrim( Z1266ContratoServicos_Momento));
         GxWebStd.gx_hidden_field( context, "Z1325ContratoServicos_StatusPagFnc", StringUtil.RTrim( Z1325ContratoServicos_StatusPagFnc));
         GxWebStd.gx_hidden_field( context, "Z1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.NToC( Z1340ContratoServicos_QntUntCns, 9, 4, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.NToC( Z1341ContratoServicos_FatorCnvUndCnt, 9, 4, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1397ContratoServicos_NaoRequerAtr", Z1397ContratoServicos_NaoRequerAtr);
         GxWebStd.gx_hidden_field( context, "Z1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.NToC( Z1455ContratoServicos_IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1516ContratoServicos_TmpEstAnl), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1501ContratoServicos_TmpEstExc), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1502ContratoServicos_TmpEstCrr), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1531ContratoServicos_TipoHierarquia", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1531ContratoServicos_TipoHierarquia), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1537ContratoServicos_PercTmp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1537ContratoServicos_PercTmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1538ContratoServicos_PercPgm", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1538ContratoServicos_PercPgm), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1539ContratoServicos_PercCnc), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z638ContratoServicos_Ativo", Z638ContratoServicos_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1723ContratoServicos_CodigoFiscal", StringUtil.RTrim( Z1723ContratoServicos_CodigoFiscal));
         GxWebStd.gx_hidden_field( context, "Z1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.NToC( Z1799ContratoServicos_LimiteProposta, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2074ContratoServicos_CalculoRmn), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2094ContratoServicos_SolicitaGestorSistema", Z2094ContratoServicos_SolicitaGestorSistema);
         GxWebStd.gx_hidden_field( context, "Z155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1212ContratoServicos_UnidadeContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.NToC( (decimal)(O2073ContratoServicos_QtdRmn), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "O638ContratoServicos_Ativo", O638ContratoServicos_Ativo);
         GxWebStd.gx_hidden_field( context, "O557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.NToC( O557Servico_VlrUnidadeContratada, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_312", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_312_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "SERVICO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A827ContratoServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCULADOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "SERVICO_IDENTIFICACAO", A2107Servico_Identificacao);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_SERVICOSIGLA", StringUtil.RTrim( A826ContratoServicos_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATOSERVICOS_UNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Insert_ContratoServicos_UnidadeContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A558Servico_Percentual, 7, 3, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vCEMPORCENTO", AV26CemPorCento);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_ALIAS", StringUtil.RTrim( A1858ContratoServicos_Alias));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_TIPOFABRICA", StringUtil.RTrim( A516Contratada_TipoFabrica));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_UNDCNTSGL", StringUtil.RTrim( A1712ContratoServicos_UndCntSgl));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_INDICEDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV29AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV29AuditingObject);
         }
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_HMLSEMCNF", A888ContratoServicos_HmlSemCnf);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_PRAZOIMEDIATO", A1190ContratoServicos_PrazoImediato);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_TIPOHIERARQUIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1531ContratoServicos_TipoHierarquia), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PERCTMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1537ContratoServicos_PercTmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PERCPGM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1538ContratoServicos_PercPgm), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PRAZOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A911ContratoServicos_Prazos), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_TELA", StringUtil.RTrim( A1061Servico_Tela));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ATIVO", A632Servico_Ativo);
         GxWebStd.gx_hidden_field( context, "SERVICO_DESCRICAO", A156Servico_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ISORIGEMREFERENCIA", A2092Servico_IsOrigemReferencia);
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCULADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_UNDCNTNOME", StringUtil.RTrim( A2132ContratoServicos_UndCntNome));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_DESCRICAO", A158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_INDICADORES", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1377ContratoServicos_Indicadores), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_QTDRMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV36Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Width", StringUtil.RTrim( Gxuitabspanel_tab_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Cls", StringUtil.RTrim( Gxuitabspanel_tab_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Enabled", StringUtil.BoolToStr( Gxuitabspanel_tab_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tab_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tab_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tab_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tab_Designtimetabs));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV36Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1531ContratoServicos_TipoHierarquia), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1537ContratoServicos_PercTmp), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1538ContratoServicos_PercPgm), "ZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV36Pgmname, "")));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_HmlSemCnf:"+StringUtil.BoolToStr( A888ContratoServicos_HmlSemCnf));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_PrazoImediato:"+StringUtil.BoolToStr( A1190ContratoServicos_PrazoImediato));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_TipoHierarquia:"+context.localUtil.Format( (decimal)(A1531ContratoServicos_TipoHierarquia), "ZZZ9"));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_PercTmp:"+context.localUtil.Format( (decimal)(A1537ContratoServicos_PercTmp), "ZZZ9"));
         GXUtil.WriteLog("contratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_PercPgm:"+context.localUtil.Format( (decimal)(A1538ContratoServicos_PercPgm), "ZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicos_Codigo) + "," + UrlEncode("" +AV27Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos" ;
      }

      protected void InitializeNonKey0X34( )
      {
         A74Contrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         A155Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1212ContratoServicos_UnidadeContratada = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
         AV21Servico_Percentual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV21Servico_Percentual, 7, 3)));
         AV30Alias = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Alias", AV30Alias);
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV26CemPorCento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26CemPorCento", AV26CemPorCento);
         AV32Servico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Servico_Nome", AV32Servico_Nome);
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         n1225ContratoServicos_PrazoCorrecaoTipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
         n1225ContratoServicos_PrazoCorrecaoTipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) ? true : false);
         A826ContratoServicos_ServicoSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
         A2107Servico_Identificacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
         A640Servico_Vinculados = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         A827ContratoServicos_ServicoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
         A1551Servico_Responsavel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         A77Contrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = "";
         n78Contrato_NumeroAta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         A79Contrato_Ano = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A116Contrato_ValorUnidadeContratacao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
         A75Contrato_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         A40Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         A516Contratada_TipoFabrica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         A1858ContratoServicos_Alias = "";
         n1858ContratoServicos_Alias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1858ContratoServicos_Alias", A1858ContratoServicos_Alias);
         A608Servico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
         A605Servico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         A1061Servico_Tela = "";
         n1061Servico_Tela = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
         A632Servico_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         A156Servico_Descricao = "";
         n156Servico_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
         A631Servico_Vinculado = 0;
         n631Servico_Vinculado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
         A157ServicoGrupo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         A158ServicoGrupo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
         A2092Servico_IsOrigemReferencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
         A2132ContratoServicos_UndCntNome = "";
         n2132ContratoServicos_UndCntNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2132ContratoServicos_UndCntNome", A2132ContratoServicos_UndCntNome);
         A1712ContratoServicos_UndCntSgl = "";
         n1712ContratoServicos_UndCntSgl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
         A555Servico_QtdContratada = 0;
         n555Servico_QtdContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A555Servico_QtdContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A555Servico_QtdContratada), 10, 0)));
         n555Servico_QtdContratada = ((0==A555Servico_QtdContratada) ? true : false);
         A639ContratoServicos_LocalExec = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
         A868ContratoServicos_TipoVnc = "";
         n868ContratoServicos_TipoVnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
         n868ContratoServicos_TipoVnc = (String.IsNullOrEmpty(StringUtil.RTrim( A868ContratoServicos_TipoVnc)) ? true : false);
         A888ContratoServicos_HmlSemCnf = false;
         n888ContratoServicos_HmlSemCnf = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A888ContratoServicos_HmlSemCnf", A888ContratoServicos_HmlSemCnf);
         A1152ContratoServicos_PrazoAnalise = 0;
         n1152ContratoServicos_PrazoAnalise = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1152ContratoServicos_PrazoAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0)));
         n1152ContratoServicos_PrazoAnalise = ((0==A1152ContratoServicos_PrazoAnalise) ? true : false);
         A1153ContratoServicos_PrazoResposta = 0;
         n1153ContratoServicos_PrazoResposta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1153ContratoServicos_PrazoResposta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1153ContratoServicos_PrazoResposta), 4, 0)));
         n1153ContratoServicos_PrazoResposta = ((0==A1153ContratoServicos_PrazoResposta) ? true : false);
         A1181ContratoServicos_PrazoGarantia = 0;
         n1181ContratoServicos_PrazoGarantia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1181ContratoServicos_PrazoGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1181ContratoServicos_PrazoGarantia), 4, 0)));
         n1181ContratoServicos_PrazoGarantia = ((0==A1181ContratoServicos_PrazoGarantia) ? true : false);
         A1182ContratoServicos_PrazoAtendeGarantia = 0;
         n1182ContratoServicos_PrazoAtendeGarantia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1182ContratoServicos_PrazoAtendeGarantia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1182ContratoServicos_PrazoAtendeGarantia), 4, 0)));
         n1182ContratoServicos_PrazoAtendeGarantia = ((0==A1182ContratoServicos_PrazoAtendeGarantia) ? true : false);
         A1224ContratoServicos_PrazoCorrecao = 0;
         n1224ContratoServicos_PrazoCorrecao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1224ContratoServicos_PrazoCorrecao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0)));
         n1224ContratoServicos_PrazoCorrecao = ((0==A1224ContratoServicos_PrazoCorrecao) ? true : false);
         A1190ContratoServicos_PrazoImediato = false;
         n1190ContratoServicos_PrazoImediato = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1190ContratoServicos_PrazoImediato", A1190ContratoServicos_PrazoImediato);
         A1191ContratoServicos_Produtividade = 0;
         n1191ContratoServicos_Produtividade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1191ContratoServicos_Produtividade", StringUtil.LTrim( StringUtil.Str( A1191ContratoServicos_Produtividade, 14, 5)));
         n1191ContratoServicos_Produtividade = ((Convert.ToDecimal(0)==A1191ContratoServicos_Produtividade) ? true : false);
         A1217ContratoServicos_EspelhaAceite = false;
         n1217ContratoServicos_EspelhaAceite = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1217ContratoServicos_EspelhaAceite", A1217ContratoServicos_EspelhaAceite);
         n1217ContratoServicos_EspelhaAceite = ((false==A1217ContratoServicos_EspelhaAceite) ? true : false);
         A1266ContratoServicos_Momento = "";
         n1266ContratoServicos_Momento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
         n1266ContratoServicos_Momento = (String.IsNullOrEmpty(StringUtil.RTrim( A1266ContratoServicos_Momento)) ? true : false);
         A1325ContratoServicos_StatusPagFnc = "";
         n1325ContratoServicos_StatusPagFnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
         n1325ContratoServicos_StatusPagFnc = (String.IsNullOrEmpty(StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc)) ? true : false);
         A1340ContratoServicos_QntUntCns = 0;
         n1340ContratoServicos_QntUntCns = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1340ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( A1340ContratoServicos_QntUntCns, 9, 4)));
         n1340ContratoServicos_QntUntCns = ((Convert.ToDecimal(0)==A1340ContratoServicos_QntUntCns) ? true : false);
         A1341ContratoServicos_FatorCnvUndCnt = 0;
         n1341ContratoServicos_FatorCnvUndCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1341ContratoServicos_FatorCnvUndCnt", StringUtil.LTrim( StringUtil.Str( A1341ContratoServicos_FatorCnvUndCnt, 9, 4)));
         n1341ContratoServicos_FatorCnvUndCnt = ((Convert.ToDecimal(0)==A1341ContratoServicos_FatorCnvUndCnt) ? true : false);
         A1397ContratoServicos_NaoRequerAtr = false;
         n1397ContratoServicos_NaoRequerAtr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1397ContratoServicos_NaoRequerAtr", A1397ContratoServicos_NaoRequerAtr);
         n1397ContratoServicos_NaoRequerAtr = ((false==A1397ContratoServicos_NaoRequerAtr) ? true : false);
         A453Contrato_IndiceDivergencia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
         A1516ContratoServicos_TmpEstAnl = 0;
         n1516ContratoServicos_TmpEstAnl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1516ContratoServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1516ContratoServicos_TmpEstAnl), 8, 0)));
         n1516ContratoServicos_TmpEstAnl = ((0==A1516ContratoServicos_TmpEstAnl) ? true : false);
         A1501ContratoServicos_TmpEstExc = 0;
         n1501ContratoServicos_TmpEstExc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1501ContratoServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1501ContratoServicos_TmpEstExc), 8, 0)));
         n1501ContratoServicos_TmpEstExc = ((0==A1501ContratoServicos_TmpEstExc) ? true : false);
         A1502ContratoServicos_TmpEstCrr = 0;
         n1502ContratoServicos_TmpEstCrr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1502ContratoServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1502ContratoServicos_TmpEstCrr), 8, 0)));
         n1502ContratoServicos_TmpEstCrr = ((0==A1502ContratoServicos_TmpEstCrr) ? true : false);
         A1531ContratoServicos_TipoHierarquia = 0;
         n1531ContratoServicos_TipoHierarquia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1531ContratoServicos_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1531ContratoServicos_TipoHierarquia), 4, 0)));
         A1537ContratoServicos_PercTmp = 0;
         n1537ContratoServicos_PercTmp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1537ContratoServicos_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1537ContratoServicos_PercTmp), 4, 0)));
         A1538ContratoServicos_PercPgm = 0;
         n1538ContratoServicos_PercPgm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1538ContratoServicos_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1538ContratoServicos_PercPgm), 4, 0)));
         A1539ContratoServicos_PercCnc = 0;
         n1539ContratoServicos_PercCnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1539ContratoServicos_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1539ContratoServicos_PercCnc), 4, 0)));
         n1539ContratoServicos_PercCnc = ((0==A1539ContratoServicos_PercCnc) ? true : false);
         A1723ContratoServicos_CodigoFiscal = "";
         n1723ContratoServicos_CodigoFiscal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1723ContratoServicos_CodigoFiscal", A1723ContratoServicos_CodigoFiscal);
         n1723ContratoServicos_CodigoFiscal = (String.IsNullOrEmpty(StringUtil.RTrim( A1723ContratoServicos_CodigoFiscal)) ? true : false);
         A1799ContratoServicos_LimiteProposta = 0;
         n1799ContratoServicos_LimiteProposta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1799ContratoServicos_LimiteProposta", StringUtil.LTrim( StringUtil.Str( A1799ContratoServicos_LimiteProposta, 14, 5)));
         n1799ContratoServicos_LimiteProposta = ((Convert.ToDecimal(0)==A1799ContratoServicos_LimiteProposta) ? true : false);
         A2074ContratoServicos_CalculoRmn = 0;
         n2074ContratoServicos_CalculoRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
         n2074ContratoServicos_CalculoRmn = ((0==A2074ContratoServicos_CalculoRmn) ? true : false);
         A557Servico_VlrUnidadeContratada = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
         A558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A558Servico_Percentual", StringUtil.LTrim( StringUtil.Str( A558Servico_Percentual, 7, 3)));
         A607ServicoContrato_Faturamento = "B";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
         A1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
         A1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
         A1455ContratoServicos_IndiceDivergencia = 0;
         n1455ContratoServicos_IndiceDivergencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1455ContratoServicos_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A1455ContratoServicos_IndiceDivergencia, 6, 2)));
         A638ContratoServicos_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
         A2094ContratoServicos_SolicitaGestorSistema = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
         O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
         O638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
         O557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A557Servico_VlrUnidadeContratada", StringUtil.LTrim( StringUtil.Str( A557Servico_VlrUnidadeContratada, 18, 5)));
         Z1225ContratoServicos_PrazoCorrecaoTipo = "";
         Z1858ContratoServicos_Alias = "";
         Z555Servico_QtdContratada = 0;
         Z557Servico_VlrUnidadeContratada = 0;
         Z558Servico_Percentual = 0;
         Z607ServicoContrato_Faturamento = "";
         Z639ContratoServicos_LocalExec = "";
         Z868ContratoServicos_TipoVnc = "";
         Z888ContratoServicos_HmlSemCnf = false;
         Z1454ContratoServicos_PrazoTpDias = "";
         Z1152ContratoServicos_PrazoAnalise = 0;
         Z1153ContratoServicos_PrazoResposta = 0;
         Z1181ContratoServicos_PrazoGarantia = 0;
         Z1182ContratoServicos_PrazoAtendeGarantia = 0;
         Z1224ContratoServicos_PrazoCorrecao = 0;
         Z1649ContratoServicos_PrazoInicio = 0;
         Z1190ContratoServicos_PrazoImediato = false;
         Z1191ContratoServicos_Produtividade = 0;
         Z1217ContratoServicos_EspelhaAceite = false;
         Z1266ContratoServicos_Momento = "";
         Z1325ContratoServicos_StatusPagFnc = "";
         Z1340ContratoServicos_QntUntCns = 0;
         Z1341ContratoServicos_FatorCnvUndCnt = 0;
         Z1397ContratoServicos_NaoRequerAtr = false;
         Z1455ContratoServicos_IndiceDivergencia = 0;
         Z1516ContratoServicos_TmpEstAnl = 0;
         Z1501ContratoServicos_TmpEstExc = 0;
         Z1502ContratoServicos_TmpEstCrr = 0;
         Z1531ContratoServicos_TipoHierarquia = 0;
         Z1537ContratoServicos_PercTmp = 0;
         Z1538ContratoServicos_PercPgm = 0;
         Z1539ContratoServicos_PercCnc = 0;
         Z638ContratoServicos_Ativo = false;
         Z1723ContratoServicos_CodigoFiscal = "";
         Z1799ContratoServicos_LimiteProposta = 0;
         Z2074ContratoServicos_CalculoRmn = 0;
         Z2094ContratoServicos_SolicitaGestorSistema = false;
         Z155Servico_Codigo = 0;
         Z74Contrato_Codigo = 0;
         Z1212ContratoServicos_UnidadeContratada = 0;
      }

      protected void InitAll0X34( )
      {
         A160ContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         InitializeNonKey0X34( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2094ContratoServicos_SolicitaGestorSistema = i2094ContratoServicos_SolicitaGestorSistema;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2094ContratoServicos_SolicitaGestorSistema", A2094ContratoServicos_SolicitaGestorSistema);
         A638ContratoServicos_Ativo = i638ContratoServicos_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
         A1649ContratoServicos_PrazoInicio = i1649ContratoServicos_PrazoInicio;
         n1649ContratoServicos_PrazoInicio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
         A607ServicoContrato_Faturamento = i607ServicoContrato_Faturamento;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
         A1454ContratoServicos_PrazoTpDias = i1454ContratoServicos_PrazoTpDias;
         n1454ContratoServicos_PrazoTpDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
         A558Servico_Percentual = i558Servico_Percentual;
         n558Servico_Percentual = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A558Servico_Percentual", StringUtil.LTrim( StringUtil.Str( A558Servico_Percentual, 7, 3)));
      }

      protected void InitializeNonKey0X228( )
      {
         A2070ContratoServicosRmn_Inicio = 0;
         A2071ContratoServicosRmn_Fim = 0;
         A2072ContratoServicosRmn_Valor = 0;
         Z2070ContratoServicosRmn_Inicio = 0;
         Z2071ContratoServicosRmn_Fim = 0;
         Z2072ContratoServicosRmn_Valor = 0;
      }

      protected void InitAll0X228( )
      {
         A2069ContratoServicosRmn_Sequencial = 0;
         InitializeNonKey0X228( ) ;
      }

      protected void StandaloneModalInsert0X228( )
      {
         A2073ContratoServicos_QtdRmn = i2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2073ContratoServicos_QtdRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2073ContratoServicos_QtdRmn), 4, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621616568");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicos.js", "?2020621616568");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties228( )
      {
         edtContratoServicosRmn_Sequencial_Enabled = defedtContratoServicosRmn_Sequencial_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosRmn_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosRmn_Sequencial_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         lblTextblockcontrato_valorunidadecontratacao_Internalname = "TEXTBLOCKCONTRATO_VALORUNIDADECONTRATACAO";
         edtContrato_ValorUnidadeContratacao_Internalname = "CONTRATO_VALORUNIDADECONTRATACAO";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup2_Internalname = "UNNAMEDGROUP2";
         lblTextblockservicogrupo_codigo_Internalname = "TEXTBLOCKSERVICOGRUPO_CODIGO";
         dynavServicogrupo_codigo_Internalname = "vSERVICOGRUPO_CODIGO";
         lblTextblockservico_codigo_Internalname = "TEXTBLOCKSERVICO_CODIGO";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblocksubservico_codigo_Internalname = "TEXTBLOCKSUBSERVICO_CODIGO";
         dynavSubservico_codigo_Internalname = "vSUBSERVICO_CODIGO";
         lblTextblockservico_nome_Internalname = "TEXTBLOCKSERVICO_NOME";
         edtavServico_nome_Internalname = "vSERVICO_NOME";
         lblTextblockalias_Internalname = "TEXTBLOCKALIAS";
         edtavAlias_Internalname = "vALIAS";
         tblGruposervico_Internalname = "GRUPOSERVICO";
         grpUnnamedgroup3_Internalname = "UNNAMEDGROUP3";
         lblTextblockcontratoservicos_prazotpdias_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOTPDIAS";
         cmbContratoServicos_PrazoTpDias_Internalname = "CONTRATOSERVICOS_PRAZOTPDIAS";
         lblTextblockcontratoservicos_prazoinicio_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOINICIO";
         cmbContratoServicos_PrazoInicio_Internalname = "CONTRATOSERVICOS_PRAZOINICIO";
         lblTextblockcontratoservicos_prazoanalise_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOANALISE";
         edtContratoServicos_PrazoAnalise_Internalname = "CONTRATOSERVICOS_PRAZOANALISE";
         lblTextblockcontratoservicos_prazotipo_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOTIPO";
         cmbContratoServicos_PrazoTipo_Internalname = "CONTRATOSERVICOS_PRAZOTIPO";
         edtContratoServicos_PrazoDias_Internalname = "CONTRATOSERVICOS_PRAZODIAS";
         cellContratoservicos_prazodias_cell_Internalname = "CONTRATOSERVICOS_PRAZODIAS_CELL";
         lblContratoservicos_prazodias_righttext_Internalname = "CONTRATOSERVICOS_PRAZODIAS_RIGHTTEXT";
         cellContratoservicos_prazodias_righttext_cell_Internalname = "CONTRATOSERVICOS_PRAZODIAS_RIGHTTEXT_CELL";
         imgUpdprazo_Internalname = "UPDPRAZO";
         tblTblbtnprzexc_Internalname = "TBLBTNPRZEXC";
         tblTablemergedcontratoservicos_prazotipo_Internalname = "TABLEMERGEDCONTRATOSERVICOS_PRAZOTIPO";
         lblTextblockcontratoservicos_prazoresposta_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZORESPOSTA";
         edtContratoServicos_PrazoResposta_Internalname = "CONTRATOSERVICOS_PRAZORESPOSTA";
         lblTextblockcontratoservicos_prazogarantia_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOGARANTIA";
         edtContratoServicos_PrazoGarantia_Internalname = "CONTRATOSERVICOS_PRAZOGARANTIA";
         lblTextblockcontratoservicos_prazoatendegarantia_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOATENDEGARANTIA";
         edtContratoServicos_PrazoAtendeGarantia_Internalname = "CONTRATOSERVICOS_PRAZOATENDEGARANTIA";
         lblTextblockcontratoservicos_prazocorrecaotipo_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         cmbContratoServicos_PrazoCorrecaoTipo_Internalname = "CONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         lblTextblockcontratoservicos_prazocorrecao_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRAZOCORRECAO";
         edtContratoServicos_PrazoCorrecao_Internalname = "CONTRATOSERVICOS_PRAZOCORRECAO";
         lblContratoservicos_prazocorrecao_righttext_Internalname = "CONTRATOSERVICOS_PRAZOCORRECAO_RIGHTTEXT";
         cellContratoservicos_prazocorrecao_righttext_cell_Internalname = "CONTRATOSERVICOS_PRAZOCORRECAO_RIGHTTEXT_CELL";
         tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname = "TABLEMERGEDCONTRATOSERVICOS_PRAZOCORRECAOTIPO";
         lblTextblockcontratoservicos_tmpestanl_Internalname = "TEXTBLOCKCONTRATOSERVICOS_TMPESTANL";
         edtContratoServicos_TmpEstAnl_Internalname = "CONTRATOSERVICOS_TMPESTANL";
         lblContratoservicos_tmpestanl_righttext_Internalname = "CONTRATOSERVICOS_TMPESTANL_RIGHTTEXT";
         tblTablemergedcontratoservicos_tmpestanl_Internalname = "TABLEMERGEDCONTRATOSERVICOS_TMPESTANL";
         lblTextblockcontratoservicos_tmpestexc_Internalname = "TEXTBLOCKCONTRATOSERVICOS_TMPESTEXC";
         edtContratoServicos_TmpEstExc_Internalname = "CONTRATOSERVICOS_TMPESTEXC";
         lblContratoservicos_tmpestexc_righttext_Internalname = "CONTRATOSERVICOS_TMPESTEXC_RIGHTTEXT";
         tblTablemergedcontratoservicos_tmpestexc_Internalname = "TABLEMERGEDCONTRATOSERVICOS_TMPESTEXC";
         lblTextblockcontratoservicos_tmpestcrr_Internalname = "TEXTBLOCKCONTRATOSERVICOS_TMPESTCRR";
         edtContratoServicos_TmpEstCrr_Internalname = "CONTRATOSERVICOS_TMPESTCRR";
         lblContratoservicos_tmpestcrr_righttext_Internalname = "CONTRATOSERVICOS_TMPESTCRR_RIGHTTEXT";
         cellContratoservicos_tmpestcrr_righttext_cell_Internalname = "CONTRATOSERVICOS_TMPESTCRR_RIGHTTEXT_CELL";
         bttBtnreplicar_Internalname = "BTNREPLICAR";
         tblTablemergedcontratoservicos_tmpestcrr_Internalname = "TABLEMERGEDCONTRATOSERVICOS_TMPESTCRR";
         lblTextblockcontratoservicos_statuspagfnc_Internalname = "TEXTBLOCKCONTRATOSERVICOS_STATUSPAGFNC";
         cmbContratoServicos_StatusPagFnc_Internalname = "CONTRATOSERVICOS_STATUSPAGFNC";
         tblPrazos_Internalname = "PRAZOS";
         grpUnnamedgroup4_Internalname = "UNNAMEDGROUP4";
         lblTextblockcontratoservicos_indicedivergencia_Internalname = "TEXTBLOCKCONTRATOSERVICOS_INDICEDIVERGENCIA";
         edtContratoServicos_IndiceDivergencia_Internalname = "CONTRATOSERVICOS_INDICEDIVERGENCIA";
         lblContratoservicos_indicedivergencia_righttext_Internalname = "CONTRATOSERVICOS_INDICEDIVERGENCIA_RIGHTTEXT";
         tblTablemergedcontratoservicos_indicedivergencia_Internalname = "TABLEMERGEDCONTRATOSERVICOS_INDICEDIVERGENCIA";
         lblTextblockcontratoservicos_naorequeratr_Internalname = "TEXTBLOCKCONTRATOSERVICOS_NAOREQUERATR";
         chkContratoServicos_NaoRequerAtr_Internalname = "CONTRATOSERVICOS_NAOREQUERATR";
         lblTextblockcontratoservicos_limiteproposta_Internalname = "TEXTBLOCKCONTRATOSERVICOS_LIMITEPROPOSTA";
         edtContratoServicos_LimiteProposta_Internalname = "CONTRATOSERVICOS_LIMITEPROPOSTA";
         lblContratoservicos_limiteproposta_righttext_Internalname = "CONTRATOSERVICOS_LIMITEPROPOSTA_RIGHTTEXT";
         tblTablemergedcontratoservicos_limiteproposta_Internalname = "TABLEMERGEDCONTRATOSERVICOS_LIMITEPROPOSTA";
         lblTextblockcontratoservicos_localexec_Internalname = "TEXTBLOCKCONTRATOSERVICOS_LOCALEXEC";
         cmbContratoServicos_LocalExec_Internalname = "CONTRATOSERVICOS_LOCALEXEC";
         lblTextblockcontratoservicos_unidadecontratada_Internalname = "TEXTBLOCKCONTRATOSERVICOS_UNIDADECONTRATADA";
         dynContratoServicos_UnidadeContratada_Internalname = "CONTRATOSERVICOS_UNIDADECONTRATADA";
         lblTextblockcontratoservicos_fatorcnvundcnt_Internalname = "TEXTBLOCKCONTRATOSERVICOS_FATORCNVUNDCNT";
         edtContratoServicos_FatorCnvUndCnt_Internalname = "CONTRATOSERVICOS_FATORCNVUNDCNT";
         lblTextblockservico_qtdcontratada_Internalname = "TEXTBLOCKSERVICO_QTDCONTRATADA";
         edtServico_QtdContratada_Internalname = "SERVICO_QTDCONTRATADA";
         lblTextblockservico_vlrunidadecontratada_Internalname = "TEXTBLOCKSERVICO_VLRUNIDADECONTRATADA";
         edtServico_VlrUnidadeContratada_Internalname = "SERVICO_VLRUNIDADECONTRATADA";
         lblTextblockcontratoservicos_produtividade_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PRODUTIVIDADE";
         edtContratoServicos_Produtividade_Internalname = "CONTRATOSERVICOS_PRODUTIVIDADE";
         lblContratoservicos_produtividade_righttext_Internalname = "CONTRATOSERVICOS_PRODUTIVIDADE_RIGHTTEXT";
         tblTablemergedcontratoservicos_produtividade_Internalname = "TABLEMERGEDCONTRATOSERVICOS_PRODUTIVIDADE";
         lblTextblockservicocontrato_faturamento_Internalname = "TEXTBLOCKSERVICOCONTRATO_FATURAMENTO";
         cmbServicoContrato_Faturamento_Internalname = "SERVICOCONTRATO_FATURAMENTO";
         lblTextblockservico_percentual_Internalname = "TEXTBLOCKSERVICO_PERCENTUAL";
         edtavServico_percentual_Internalname = "vSERVICO_PERCENTUAL";
         lblServico_percentual_righttext_Internalname = "SERVICO_PERCENTUAL_RIGHTTEXT";
         tblTablemergedservico_percentual_Internalname = "TABLEMERGEDSERVICO_PERCENTUAL";
         lblTextblockcontratoservicos_calculormn_Internalname = "TEXTBLOCKCONTRATOSERVICOS_CALCULORMN";
         cmbContratoServicos_CalculoRmn_Internalname = "CONTRATOSERVICOS_CALCULORMN";
         lblTextblockcontratoservicos_perccnc_Internalname = "TEXTBLOCKCONTRATOSERVICOS_PERCCNC";
         edtContratoServicos_PercCnc_Internalname = "CONTRATOSERVICOS_PERCCNC";
         lblContratoservicos_perccnc_righttext_Internalname = "CONTRATOSERVICOS_PERCCNC_RIGHTTEXT";
         tblTablemergedcontratoservicos_perccnc_Internalname = "TABLEMERGEDCONTRATOSERVICOS_PERCCNC";
         lblTextblockcontratoservicos_espelhaaceite_Internalname = "TEXTBLOCKCONTRATOSERVICOS_ESPELHAACEITE";
         chkContratoServicos_EspelhaAceite_Internalname = "CONTRATOSERVICOS_ESPELHAACEITE";
         lblTextblockcontratoservicos_momento_Internalname = "TEXTBLOCKCONTRATOSERVICOS_MOMENTO";
         cmbContratoServicos_Momento_Internalname = "CONTRATOSERVICOS_MOMENTO";
         lblTextblockcontratoservicos_tipovnc_Internalname = "TEXTBLOCKCONTRATOSERVICOS_TIPOVNC";
         cmbContratoServicos_TipoVnc_Internalname = "CONTRATOSERVICOS_TIPOVNC";
         lblTextblockcontratoservicos_qntuntcns_Internalname = "TEXTBLOCKCONTRATOSERVICOS_QNTUNTCNS";
         edtContratoServicos_QntUntCns_Internalname = "CONTRATOSERVICOS_QNTUNTCNS";
         lblTextblockcontratoservicos_codigofiscal_Internalname = "TEXTBLOCKCONTRATOSERVICOS_CODIGOFISCAL";
         edtContratoServicos_CodigoFiscal_Internalname = "CONTRATOSERVICOS_CODIGOFISCAL";
         lblTextblockcontratoservicos_solicitagestorsistema_Internalname = "TEXTBLOCKCONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         chkContratoServicos_SolicitaGestorSistema_Internalname = "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         imgContratoservicos_solicitagestorsistema_infoicon_Internalname = "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA_INFOICON";
         tblTablemergedcontratoservicos_solicitagestorsistema_Internalname = "TABLEMERGEDCONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
         lblTextblockcontratoservicos_ativo_Internalname = "TEXTBLOCKCONTRATOSERVICOS_ATIVO";
         chkContratoServicos_Ativo_Internalname = "CONTRATOSERVICOS_ATIVO";
         tblParametros_Internalname = "PARAMETROS";
         grpUnnamedgroup5_Internalname = "UNNAMEDGROUP5";
         tblTableprincipal_Internalname = "TABLEPRINCIPAL";
         edtContratoServicosRmn_Sequencial_Internalname = "CONTRATOSERVICOSRMN_SEQUENCIAL";
         edtContratoServicosRmn_Inicio_Internalname = "CONTRATOSERVICOSRMN_INICIO";
         edtContratoServicosRmn_Fim_Internalname = "CONTRATOSERVICOSRMN_FIM";
         edtContratoServicosRmn_Valor_Internalname = "CONTRATOSERVICOSRMN_VALOR";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Gxuitabspanel_tab_Internalname = "GXUITABSPANEL_TAB";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Gxuitabspanel_tab_Designtimetabs = "[{\"id\":\"Servico\"},{\"id\":\"Remuneracao\"}]";
         Gxuitabspanel_tab_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tab_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tab_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tab_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tab_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Servicos";
         edtContratoServicosRmn_Valor_Jsonclick = "";
         edtContratoServicosRmn_Fim_Jsonclick = "";
         edtContratoServicosRmn_Inicio_Jsonclick = "";
         edtContratoServicosRmn_Sequencial_Jsonclick = "";
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavServico_codigo.Description = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContrato_ValorUnidadeContratacao_Jsonclick = "";
         edtContrato_ValorUnidadeContratacao_Enabled = 0;
         edtContrato_ValorUnidadeContratacao_Visible = 1;
         lblTextblockcontrato_valorunidadecontratacao_Visible = 1;
         edtavAlias_Jsonclick = "";
         edtavAlias_Enabled = 1;
         edtavServico_nome_Jsonclick = "";
         edtavServico_nome_Enabled = 0;
         dynavSubservico_codigo_Jsonclick = "";
         dynavSubservico_codigo.Enabled = 1;
         dynavSubservico_codigo.Visible = 1;
         lblTextblocksubservico_codigo_Caption = "Subservi�o";
         lblTextblocksubservico_codigo_Visible = 1;
         dynavServico_codigo_Jsonclick = "";
         dynavServico_codigo.Enabled = 1;
         lblTextblockservico_codigo_Caption = "Servi�o (Sigla)";
         dynavServicogrupo_codigo_Jsonclick = "";
         dynavServicogrupo_codigo.Enabled = 1;
         imgUpdprazo_Tooltiptext = "";
         imgUpdprazo_Enabled = 1;
         imgUpdprazo_Visible = 1;
         cellContratoservicos_prazodias_righttext_cell_Class = "";
         edtContratoServicos_PrazoDias_Jsonclick = "";
         edtContratoServicos_PrazoDias_Enabled = 0;
         edtContratoServicos_PrazoDias_Visible = 1;
         cellContratoservicos_prazodias_cell_Class = "";
         cmbContratoServicos_PrazoTipo_Jsonclick = "";
         cmbContratoServicos_PrazoTipo.Enabled = 0;
         lblContratoservicos_prazocorrecao_righttext_Caption = "�-";
         edtContratoServicos_PrazoCorrecao_Jsonclick = "";
         edtContratoServicos_PrazoCorrecao_Enabled = 1;
         edtContratoServicos_PrazoCorrecao_Visible = 1;
         cmbContratoServicos_PrazoCorrecaoTipo_Jsonclick = "";
         cmbContratoServicos_PrazoCorrecaoTipo.Enabled = 1;
         edtContratoServicos_TmpEstAnl_Jsonclick = "";
         edtContratoServicos_TmpEstAnl_Enabled = 1;
         edtContratoServicos_TmpEstExc_Jsonclick = "";
         edtContratoServicos_TmpEstExc_Enabled = 1;
         bttBtnreplicar_Visible = 1;
         edtContratoServicos_TmpEstCrr_Jsonclick = "";
         edtContratoServicos_TmpEstCrr_Enabled = 1;
         cmbContratoServicos_StatusPagFnc_Jsonclick = "";
         cmbContratoServicos_StatusPagFnc.Enabled = 1;
         edtContratoServicos_PrazoAtendeGarantia_Jsonclick = "";
         edtContratoServicos_PrazoAtendeGarantia_Enabled = 1;
         edtContratoServicos_PrazoGarantia_Jsonclick = "";
         edtContratoServicos_PrazoGarantia_Enabled = 1;
         edtContratoServicos_PrazoResposta_Jsonclick = "";
         edtContratoServicos_PrazoResposta_Enabled = 1;
         edtContratoServicos_PrazoAnalise_Jsonclick = "";
         edtContratoServicos_PrazoAnalise_Enabled = 1;
         cmbContratoServicos_PrazoInicio_Jsonclick = "";
         cmbContratoServicos_PrazoInicio.Enabled = 1;
         cmbContratoServicos_PrazoTpDias_Jsonclick = "";
         cmbContratoServicos_PrazoTpDias.Enabled = 1;
         edtContratoServicos_IndiceDivergencia_Jsonclick = "";
         edtContratoServicos_IndiceDivergencia_Enabled = 1;
         lblContratoservicos_limiteproposta_righttext_Caption = "Value";
         edtContratoServicos_LimiteProposta_Jsonclick = "";
         edtContratoServicos_LimiteProposta_Enabled = 1;
         lblContratoservicos_produtividade_righttext_Caption = "Value";
         edtContratoServicos_Produtividade_Jsonclick = "";
         edtContratoServicos_Produtividade_Enabled = 1;
         edtavServico_percentual_Jsonclick = "";
         edtavServico_percentual_Enabled = 1;
         edtContratoServicos_PercCnc_Jsonclick = "";
         edtContratoServicos_PercCnc_Enabled = 1;
         imgContratoservicos_solicitagestorsistema_infoicon_Visible = 1;
         chkContratoServicos_SolicitaGestorSistema.Enabled = 1;
         chkContratoServicos_Ativo.Enabled = 1;
         chkContratoServicos_Ativo.Visible = 1;
         lblTextblockcontratoservicos_ativo_Visible = 1;
         edtContratoServicos_CodigoFiscal_Jsonclick = "";
         edtContratoServicos_CodigoFiscal_Enabled = 1;
         edtContratoServicos_QntUntCns_Jsonclick = "";
         edtContratoServicos_QntUntCns_Enabled = 1;
         cmbContratoServicos_TipoVnc_Jsonclick = "";
         cmbContratoServicos_TipoVnc.Enabled = 1;
         cmbContratoServicos_Momento_Jsonclick = "";
         cmbContratoServicos_Momento.Enabled = 1;
         chkContratoServicos_EspelhaAceite.Enabled = 1;
         chkContratoServicos_EspelhaAceite.Visible = 1;
         lblTextblockcontratoservicos_espelhaaceite_Visible = 1;
         cmbContratoServicos_CalculoRmn_Jsonclick = "";
         cmbContratoServicos_CalculoRmn.Enabled = 1;
         cmbServicoContrato_Faturamento_Jsonclick = "";
         cmbServicoContrato_Faturamento.Enabled = 1;
         edtServico_VlrUnidadeContratada_Jsonclick = "";
         edtServico_VlrUnidadeContratada_Enabled = 1;
         edtServico_QtdContratada_Jsonclick = "";
         edtServico_QtdContratada_Enabled = 1;
         edtContratoServicos_FatorCnvUndCnt_Jsonclick = "";
         edtContratoServicos_FatorCnvUndCnt_Enabled = 1;
         dynContratoServicos_UnidadeContratada_Jsonclick = "";
         dynContratoServicos_UnidadeContratada.Enabled = 1;
         cmbContratoServicos_LocalExec_Jsonclick = "";
         cmbContratoServicos_LocalExec.Enabled = 1;
         chkContratoServicos_NaoRequerAtr.Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoServicosRmn_Valor_Enabled = 1;
         edtContratoServicosRmn_Fim_Enabled = 1;
         edtContratoServicosRmn_Inicio_Enabled = 1;
         edtContratoServicosRmn_Sequencial_Enabled = 0;
         subGrid_Backcolorstyle = 3;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Enabled = 1;
         edtServico_Codigo_Visible = 1;
         chkContratoServicos_Ativo.Caption = "";
         chkContratoServicos_SolicitaGestorSistema.Caption = "";
         chkContratoServicos_EspelhaAceite.Caption = "";
         chkContratoServicos_NaoRequerAtr.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO0X34( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOGRUPO_CODIGO_data0X34( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOGRUPO_CODIGO_html0X34( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOGRUPO_CODIGO_data0X34( ) ;
         gxdynajaxindex = 1;
         dynavServicogrupo_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO_data0X34( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000X61 */
         pr_default.execute(54);
         while ( (pr_default.getStatus(54) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000X61_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T000X61_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(54);
         }
         pr_default.close(54);
      }

      protected void GXDLVvSERVICO_CODIGO0X34( int AV14ServicoGrupo_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_data0X34( AV14ServicoGrupo_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_html0X34( int AV14ServicoGrupo_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_data0X34( AV14ServicoGrupo_Codigo) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvSERVICO_CODIGO_data0X34( int AV14ServicoGrupo_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000X62 */
         pr_default.execute(55, new Object[] {AV14ServicoGrupo_Codigo});
         while ( (pr_default.getStatus(55) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000X62_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000X62_A605Servico_Sigla[0]));
            pr_default.readNext(55);
         }
         pr_default.close(55);
      }

      protected void GXDLVvSUBSERVICO_CODIGO0X34( int AV17Servico_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSUBSERVICO_CODIGO_data0X34( AV17Servico_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSUBSERVICO_CODIGO_html0X34( int AV17Servico_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSUBSERVICO_CODIGO_data0X34( AV17Servico_Codigo) ;
         gxdynajaxindex = 1;
         dynavSubservico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSubservico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvSUBSERVICO_CODIGO_data0X34( int AV17Servico_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000X63 */
         pr_default.execute(56, new Object[] {AV17Servico_Codigo});
         while ( (pr_default.getStatus(56) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000X63_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000X63_A608Servico_Nome[0]));
            pr_default.readNext(56);
         }
         pr_default.close(56);
      }

      protected void GXDLACONTRATOSERVICOS_UNIDADECONTRATADA0X34( int AV27Contrato_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOSERVICOS_UNIDADECONTRATADA_data0X34( AV27Contrato_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOSERVICOS_UNIDADECONTRATADA_html0X34( int AV27Contrato_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOSERVICOS_UNIDADECONTRATADA_data0X34( AV27Contrato_Codigo) ;
         gxdynajaxindex = 1;
         dynContratoServicos_UnidadeContratada.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoServicos_UnidadeContratada.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOSERVICOS_UNIDADECONTRATADA_data0X34( int AV27Contrato_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T000X64 */
         pr_default.execute(57, new Object[] {AV27Contrato_Codigo});
         while ( (pr_default.getStatus(57) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000X64_A1204ContratoUnidades_UndMedCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000X64_A1205ContratoUnidades_UndMedNom[0]));
            pr_default.readNext(57);
         }
         pr_default.close(57);
      }

      protected void GX7ASASERVICO_RESPONSAVEL0X34( int A155Servico_Codigo )
      {
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX9ASASERVICO_VINCULADOS0X34( int A155Servico_Codigo )
      {
         GXt_int3 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_57_0X34( wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                 int A160ContratoServicos_Codigo ,
                                 String Gx_mode )
      {
         new loadauditcontratoservicos(context ).execute(  "Y", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_58_0X34( wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                 int A160ContratoServicos_Codigo ,
                                 String Gx_mode )
      {
         new loadauditcontratoservicos(context ).execute(  "Y", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_59_0X34( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                 int A160ContratoServicos_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicos(context ).execute(  "N", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_60_0X34( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                 int A160ContratoServicos_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicos(context ).execute(  "N", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_312228( ) ;
         while ( nGXsfl_312_idx <= nRC_GXsfl_312 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal0X228( ) ;
            standaloneModal0X228( ) ;
            dynavServicogrupo_codigo.Name = "vSERVICOGRUPO_CODIGO";
            dynavServicogrupo_codigo.WebTags = "";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            dynavSubservico_codigo.Name = "vSUBSERVICO_CODIGO";
            dynavSubservico_codigo.WebTags = "";
            cmbContratoServicos_PrazoTpDias.Name = "CONTRATOSERVICOS_PRAZOTPDIAS";
            cmbContratoServicos_PrazoTpDias.WebTags = "";
            cmbContratoServicos_PrazoTpDias.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_PrazoTpDias.addItem("U", "�teis", 0);
            cmbContratoServicos_PrazoTpDias.addItem("C", "Corridos", 0);
            if ( cmbContratoServicos_PrazoTpDias.ItemCount > 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) )
               {
                  A1454ContratoServicos_PrazoTpDias = "U";
                  n1454ContratoServicos_PrazoTpDias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
               }
               A1454ContratoServicos_PrazoTpDias = cmbContratoServicos_PrazoTpDias.getValidValue(A1454ContratoServicos_PrazoTpDias);
               n1454ContratoServicos_PrazoTpDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1454ContratoServicos_PrazoTpDias", A1454ContratoServicos_PrazoTpDias);
            }
            cmbContratoServicos_PrazoInicio.Name = "CONTRATOSERVICOS_PRAZOINICIO";
            cmbContratoServicos_PrazoInicio.WebTags = "";
            cmbContratoServicos_PrazoInicio.addItem("1", "Dia seguinte", 0);
            cmbContratoServicos_PrazoInicio.addItem("2", "Dia �til seguinte", 0);
            cmbContratoServicos_PrazoInicio.addItem("3", "Imediato", 0);
            if ( cmbContratoServicos_PrazoInicio.ItemCount > 0 )
            {
               if ( (0==A1649ContratoServicos_PrazoInicio) )
               {
                  A1649ContratoServicos_PrazoInicio = 1;
                  n1649ContratoServicos_PrazoInicio = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
               }
               A1649ContratoServicos_PrazoInicio = (short)(NumberUtil.Val( cmbContratoServicos_PrazoInicio.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0))), "."));
               n1649ContratoServicos_PrazoInicio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1649ContratoServicos_PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0)));
            }
            cmbContratoServicos_PrazoTipo.Name = "CONTRATOSERVICOS_PRAZOTIPO";
            cmbContratoServicos_PrazoTipo.WebTags = "";
            cmbContratoServicos_PrazoTipo.addItem("A", "Acumulado", 0);
            cmbContratoServicos_PrazoTipo.addItem("C", "Complexidade", 0);
            cmbContratoServicos_PrazoTipo.addItem("S", "Solicitado", 0);
            cmbContratoServicos_PrazoTipo.addItem("F", "Fixo", 0);
            cmbContratoServicos_PrazoTipo.addItem("E", "El�stico", 0);
            cmbContratoServicos_PrazoTipo.addItem("V", "Vari�vel", 0);
            cmbContratoServicos_PrazoTipo.addItem("P", "Progress�o Aritm�tica", 0);
            cmbContratoServicos_PrazoTipo.addItem("O", "Combinado", 0);
            if ( cmbContratoServicos_PrazoTipo.ItemCount > 0 )
            {
               A913ContratoServicos_PrazoTipo = cmbContratoServicos_PrazoTipo.getValidValue(A913ContratoServicos_PrazoTipo);
               n913ContratoServicos_PrazoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A913ContratoServicos_PrazoTipo", A913ContratoServicos_PrazoTipo);
            }
            cmbContratoServicos_PrazoCorrecaoTipo.Name = "CONTRATOSERVICOS_PRAZOCORRECAOTIPO";
            cmbContratoServicos_PrazoCorrecaoTipo.WebTags = "";
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("F", "Fixo", 0);
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("I", "Igual ao prazo de execu��o", 0);
            cmbContratoServicos_PrazoCorrecaoTipo.addItem("P", "Percentual do prazo de execu��o", 0);
            if ( cmbContratoServicos_PrazoCorrecaoTipo.ItemCount > 0 )
            {
               A1225ContratoServicos_PrazoCorrecaoTipo = cmbContratoServicos_PrazoCorrecaoTipo.getValidValue(A1225ContratoServicos_PrazoCorrecaoTipo);
               n1225ContratoServicos_PrazoCorrecaoTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1225ContratoServicos_PrazoCorrecaoTipo", A1225ContratoServicos_PrazoCorrecaoTipo);
            }
            cmbContratoServicos_StatusPagFnc.Name = "CONTRATOSERVICOS_STATUSPAGFNC";
            cmbContratoServicos_StatusPagFnc.WebTags = "";
            cmbContratoServicos_StatusPagFnc.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_StatusPagFnc.addItem("B", "Stand by", 0);
            cmbContratoServicos_StatusPagFnc.addItem("S", "Solicitada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("E", "Em An�lise", 0);
            cmbContratoServicos_StatusPagFnc.addItem("A", "Em execu��o", 0);
            cmbContratoServicos_StatusPagFnc.addItem("R", "Resolvida", 0);
            cmbContratoServicos_StatusPagFnc.addItem("C", "Conferida", 0);
            cmbContratoServicos_StatusPagFnc.addItem("D", "Retornada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("H", "Homologada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("O", "Aceite", 0);
            cmbContratoServicos_StatusPagFnc.addItem("P", "A Pagar", 0);
            cmbContratoServicos_StatusPagFnc.addItem("L", "Liquidada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("X", "Cancelada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("N", "N�o Faturada", 0);
            cmbContratoServicos_StatusPagFnc.addItem("J", "Planejamento", 0);
            cmbContratoServicos_StatusPagFnc.addItem("I", "An�lise Planejamento", 0);
            cmbContratoServicos_StatusPagFnc.addItem("T", "Validacao T�cnica", 0);
            cmbContratoServicos_StatusPagFnc.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoServicos_StatusPagFnc.addItem("G", "Em Homologa��o", 0);
            cmbContratoServicos_StatusPagFnc.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoServicos_StatusPagFnc.addItem("U", "Rascunho", 0);
            if ( cmbContratoServicos_StatusPagFnc.ItemCount > 0 )
            {
               A1325ContratoServicos_StatusPagFnc = cmbContratoServicos_StatusPagFnc.getValidValue(A1325ContratoServicos_StatusPagFnc);
               n1325ContratoServicos_StatusPagFnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1325ContratoServicos_StatusPagFnc", A1325ContratoServicos_StatusPagFnc);
            }
            chkContratoServicos_NaoRequerAtr.Name = "CONTRATOSERVICOS_NAOREQUERATR";
            chkContratoServicos_NaoRequerAtr.WebTags = "";
            chkContratoServicos_NaoRequerAtr.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_NaoRequerAtr_Internalname, "TitleCaption", chkContratoServicos_NaoRequerAtr.Caption);
            chkContratoServicos_NaoRequerAtr.CheckedValue = "false";
            cmbContratoServicos_LocalExec.Name = "CONTRATOSERVICOS_LOCALEXEC";
            cmbContratoServicos_LocalExec.WebTags = "";
            cmbContratoServicos_LocalExec.addItem("A", "Contratada", 0);
            cmbContratoServicos_LocalExec.addItem("E", "Contratante", 0);
            cmbContratoServicos_LocalExec.addItem("O", "Opcional", 0);
            if ( cmbContratoServicos_LocalExec.ItemCount > 0 )
            {
               A639ContratoServicos_LocalExec = cmbContratoServicos_LocalExec.getValidValue(A639ContratoServicos_LocalExec);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A639ContratoServicos_LocalExec", A639ContratoServicos_LocalExec);
            }
            dynContratoServicos_UnidadeContratada.Name = "CONTRATOSERVICOS_UNIDADECONTRATADA";
            dynContratoServicos_UnidadeContratada.WebTags = "";
            cmbServicoContrato_Faturamento.Name = "SERVICOCONTRATO_FATURAMENTO";
            cmbServicoContrato_Faturamento.WebTags = "";
            cmbServicoContrato_Faturamento.addItem("B", "Bruto", 0);
            cmbServicoContrato_Faturamento.addItem("L", "Liquido", 0);
            if ( cmbServicoContrato_Faturamento.ItemCount > 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A607ServicoContrato_Faturamento)) )
               {
                  A607ServicoContrato_Faturamento = "B";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
               }
               A607ServicoContrato_Faturamento = cmbServicoContrato_Faturamento.getValidValue(A607ServicoContrato_Faturamento);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A607ServicoContrato_Faturamento", A607ServicoContrato_Faturamento);
            }
            cmbContratoServicos_CalculoRmn.Name = "CONTRATOSERVICOS_CALCULORMN";
            cmbContratoServicos_CalculoRmn.WebTags = "";
            cmbContratoServicos_CalculoRmn.addItem("0", "Configurado", 0);
            cmbContratoServicos_CalculoRmn.addItem("1", "Vari�vel", 0);
            if ( cmbContratoServicos_CalculoRmn.ItemCount > 0 )
            {
               A2074ContratoServicos_CalculoRmn = (short)(NumberUtil.Val( cmbContratoServicos_CalculoRmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0))), "."));
               n2074ContratoServicos_CalculoRmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2074ContratoServicos_CalculoRmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A2074ContratoServicos_CalculoRmn), 4, 0)));
            }
            chkContratoServicos_EspelhaAceite.Name = "CONTRATOSERVICOS_ESPELHAACEITE";
            chkContratoServicos_EspelhaAceite.WebTags = "";
            chkContratoServicos_EspelhaAceite.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_EspelhaAceite_Internalname, "TitleCaption", chkContratoServicos_EspelhaAceite.Caption);
            chkContratoServicos_EspelhaAceite.CheckedValue = "false";
            cmbContratoServicos_Momento.Name = "CONTRATOSERVICOS_MOMENTO";
            cmbContratoServicos_Momento.WebTags = "";
            cmbContratoServicos_Momento.addItem("", "(Nenhum)", 0);
            cmbContratoServicos_Momento.addItem("I", "Inicial", 0);
            cmbContratoServicos_Momento.addItem("F", "Final", 0);
            if ( cmbContratoServicos_Momento.ItemCount > 0 )
            {
               A1266ContratoServicos_Momento = cmbContratoServicos_Momento.getValidValue(A1266ContratoServicos_Momento);
               n1266ContratoServicos_Momento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1266ContratoServicos_Momento", A1266ContratoServicos_Momento);
            }
            cmbContratoServicos_TipoVnc.Name = "CONTRATOSERVICOS_TIPOVNC";
            cmbContratoServicos_TipoVnc.WebTags = "";
            cmbContratoServicos_TipoVnc.addItem("", "N/A", 0);
            cmbContratoServicos_TipoVnc.addItem("D", "D�bito/Cr�dito", 0);
            cmbContratoServicos_TipoVnc.addItem("C", "Compara��o", 0);
            cmbContratoServicos_TipoVnc.addItem("A", "D�bito/Cr�dito e Compara��o", 0);
            if ( cmbContratoServicos_TipoVnc.ItemCount > 0 )
            {
               A868ContratoServicos_TipoVnc = cmbContratoServicos_TipoVnc.getValidValue(A868ContratoServicos_TipoVnc);
               n868ContratoServicos_TipoVnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A868ContratoServicos_TipoVnc", A868ContratoServicos_TipoVnc);
            }
            chkContratoServicos_SolicitaGestorSistema.Name = "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA";
            chkContratoServicos_SolicitaGestorSistema.WebTags = "";
            chkContratoServicos_SolicitaGestorSistema.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_SolicitaGestorSistema_Internalname, "TitleCaption", chkContratoServicos_SolicitaGestorSistema.Caption);
            chkContratoServicos_SolicitaGestorSistema.CheckedValue = "false";
            chkContratoServicos_Ativo.Name = "CONTRATOSERVICOS_ATIVO";
            chkContratoServicos_Ativo.WebTags = "";
            chkContratoServicos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicos_Ativo_Internalname, "TitleCaption", chkContratoServicos_Ativo.Caption);
            chkContratoServicos_Ativo.CheckedValue = "false";
            dynload_actions( ) ;
            SendRow0X228( ) ;
            nGXsfl_312_idx = (short)(nGXsfl_312_idx+1);
            sGXsfl_312_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_312_idx), 4, 0)), 4, "0");
            SubsflControlProps_312228( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      public void Valid_Contrato_valorunidadecontratacao( String GX_Parm1 ,
                                                          decimal GX_Parm2 ,
                                                          short GX_Parm3 ,
                                                          decimal GX_Parm4 )
      {
         Gx_mode = GX_Parm1;
         A116Contrato_ValorUnidadeContratacao = GX_Parm2;
         Gx_BScreen = GX_Parm3;
         A557Servico_VlrUnidadeContratada = GX_Parm4;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A557Servico_VlrUnidadeContratada) && ( Gx_BScreen == 0 ) )
         {
            A557Servico_VlrUnidadeContratada = A116Contrato_ValorUnidadeContratacao;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Servicogrupo_codigo( GXCombobox dynGX_Parm1 ,
                                              GXCombobox dynGX_Parm2 )
      {
         dynavServicogrupo_codigo = dynGX_Parm1;
         AV14ServicoGrupo_Codigo = (int)(NumberUtil.Val( dynavServicogrupo_codigo.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm2;
         AV17Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         GXVvSERVICO_CODIGO_html0X34( AV14ServicoGrupo_Codigo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0));
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV17Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Servico_codigo( int GX_Parm1 ,
                                         GXCombobox dynGX_Parm2 ,
                                         GXCombobox dynGX_Parm3 ,
                                         int GX_Parm4 )
      {
         AV12Insert_Servico_Codigo = GX_Parm1;
         dynavServico_codigo = dynGX_Parm2;
         AV17Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         dynavSubservico_codigo = dynGX_Parm3;
         AV15SubServico_Codigo = (int)(NumberUtil.Val( dynavSubservico_codigo.CurrentValue, "."));
         A155Servico_Codigo = GX_Parm4;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Servico_Codigo) )
         {
            A155Servico_Codigo = AV12Insert_Servico_Codigo;
         }
         else
         {
            if ( AV17Servico_Codigo > 0 )
            {
               A155Servico_Codigo = AV17Servico_Codigo;
            }
            else
            {
               if ( AV15SubServico_Codigo > 0 )
               {
                  A155Servico_Codigo = AV15SubServico_Codigo;
               }
            }
         }
         GXVvSUBSERVICO_CODIGO_html0X34( AV17Servico_Codigo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynavSubservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
         if ( dynavSubservico_codigo.ItemCount > 0 )
         {
            AV15SubServico_Codigo = (int)(NumberUtil.Val( dynavSubservico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0))), "."));
         }
         dynavSubservico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15SubServico_Codigo), 6, 0));
         isValidOutput.Add(dynavSubservico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicos_prazocorrecao( GXCombobox cmbGX_Parm1 ,
                                                        short GX_Parm2 ,
                                                        bool GX_Parm3 )
      {
         cmbContratoServicos_PrazoCorrecaoTipo = cmbGX_Parm1;
         A1225ContratoServicos_PrazoCorrecaoTipo = cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue;
         n1225ContratoServicos_PrazoCorrecaoTipo = false;
         cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = A1225ContratoServicos_PrazoCorrecaoTipo;
         A1224ContratoServicos_PrazoCorrecao = GX_Parm2;
         n1224ContratoServicos_PrazoCorrecao = false;
         AV26CemPorCento = GX_Parm3;
         AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
         if ( AV26CemPorCento )
         {
            lblContratoservicos_prazocorrecao_righttext_Caption = "";
         }
         if ( ( A1224ContratoServicos_PrazoCorrecao > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = "F";
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
            cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = A1225ContratoServicos_PrazoCorrecaoTipo;
         }
         else
         {
            if ( AV26CemPorCento )
            {
               A1225ContratoServicos_PrazoCorrecaoTipo = "I";
               n1225ContratoServicos_PrazoCorrecaoTipo = false;
               cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = A1225ContratoServicos_PrazoCorrecaoTipo;
            }
         }
         edtContratoServicos_PrazoCorrecao_Visible = (((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I")!=0)) ? 1 : 0);
         if ( ( A1224ContratoServicos_PrazoCorrecao > 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( A1225ContratoServicos_PrazoCorrecaoTipo)) )
         {
            A1224ContratoServicos_PrazoCorrecao = 0;
            n1224ContratoServicos_PrazoCorrecao = false;
            n1224ContratoServicos_PrazoCorrecao = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 ) || AV26CemPorCento )
            {
               A1224ContratoServicos_PrazoCorrecao = 0;
               n1224ContratoServicos_PrazoCorrecao = false;
            }
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(AV26CemPorCento);
         isValidOutput.Add(lblContratoservicos_prazocorrecao_righttext_Caption);
         cmbContratoServicos_PrazoCorrecaoTipo.CurrentValue = A1225ContratoServicos_PrazoCorrecaoTipo;
         isValidOutput.Add(cmbContratoServicos_PrazoCorrecaoTipo);
         isValidOutput.Add(edtContratoServicos_PrazoCorrecao_Visible);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1224ContratoServicos_PrazoCorrecao), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicos_unidadecontratada( GXCombobox dynGX_Parm1 ,
                                                            String GX_Parm2 ,
                                                            String GX_Parm3 )
      {
         dynContratoServicos_UnidadeContratada = dynGX_Parm1;
         A1212ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( dynContratoServicos_UnidadeContratada.CurrentValue, "."));
         A1712ContratoServicos_UndCntSgl = GX_Parm2;
         n1712ContratoServicos_UndCntSgl = false;
         A2132ContratoServicos_UndCntNome = GX_Parm3;
         n2132ContratoServicos_UndCntNome = false;
         /* Using cursor T000X65 */
         pr_default.execute(58, new Object[] {A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(58) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_UNIDADECONTRATADA");
            AnyError = 1;
            GX_FocusControl = dynContratoServicos_UnidadeContratada_Internalname;
         }
         A2132ContratoServicos_UndCntNome = T000X65_A2132ContratoServicos_UndCntNome[0];
         n2132ContratoServicos_UndCntNome = T000X65_n2132ContratoServicos_UndCntNome[0];
         A1712ContratoServicos_UndCntSgl = T000X65_A1712ContratoServicos_UndCntSgl[0];
         n1712ContratoServicos_UndCntSgl = T000X65_n1712ContratoServicos_UndCntSgl[0];
         pr_default.close(58);
         lblContratoservicos_produtividade_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
         lblContratoservicos_limiteproposta_righttext_Caption = " "+A1712ContratoServicos_UndCntSgl;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2132ContratoServicos_UndCntNome = "";
            n2132ContratoServicos_UndCntNome = false;
            A1712ContratoServicos_UndCntSgl = "";
            n1712ContratoServicos_UndCntSgl = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A2132ContratoServicos_UndCntNome));
         isValidOutput.Add(StringUtil.RTrim( A1712ContratoServicos_UndCntSgl));
         isValidOutput.Add(lblContratoservicos_produtividade_righttext_Caption);
         isValidOutput.Add(lblContratoservicos_limiteproposta_righttext_Caption);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicos_ativo( String GX_Parm1 ,
                                                bool GX_Parm2 ,
                                                bool GX_Parm3 ,
                                                int GX_Parm4 )
      {
         Gx_mode = GX_Parm1;
         O638ContratoServicos_Ativo = GX_Parm2;
         A638ContratoServicos_Ativo = GX_Parm3;
         A160ContratoServicos_Codigo = GX_Parm4;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ! A638ContratoServicos_Ativo && ( O638ContratoServicos_Ativo ) && new prc_numaregra(context).executeUdp( ref  A160ContratoServicos_Codigo) )
         {
            GX_msglist.addItem("Servi�o em uso por alguma regra de disparo autom�tico neste ou em outro contrato!", 1, "CONTRATOSERVICOS_ATIVO");
            AnyError = 1;
            GX_FocusControl = chkContratoServicos_Ativo_Internalname;
         }
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_codigo( bool GX_Parm1 ,
                                        int GX_Parm2 ,
                                        String GX_Parm3 ,
                                        String GX_Parm4 ,
                                        int GX_Parm5 ,
                                        String GX_Parm6 ,
                                        bool GX_Parm7 ,
                                        String GX_Parm8 ,
                                        bool GX_Parm9 ,
                                        int GX_Parm10 ,
                                        String GX_Parm11 ,
                                        String GX_Parm12 ,
                                        String GX_Parm13 ,
                                        String GX_Parm14 ,
                                        int GX_Parm15 ,
                                        int GX_Parm16 ,
                                        short GX_Parm17 )
      {
         A638ContratoServicos_Ativo = GX_Parm1;
         A155Servico_Codigo = GX_Parm2;
         A608Servico_Nome = GX_Parm3;
         A605Servico_Sigla = GX_Parm4;
         A157ServicoGrupo_Codigo = GX_Parm5;
         A1061Servico_Tela = GX_Parm6;
         n1061Servico_Tela = false;
         A632Servico_Ativo = GX_Parm7;
         A156Servico_Descricao = GX_Parm8;
         n156Servico_Descricao = false;
         A2092Servico_IsOrigemReferencia = GX_Parm9;
         A631Servico_Vinculado = GX_Parm10;
         n631Servico_Vinculado = false;
         AV32Servico_Nome = GX_Parm11;
         A2107Servico_Identificacao = GX_Parm12;
         A826ContratoServicos_ServicoSigla = GX_Parm13;
         A158ServicoGrupo_Descricao = GX_Parm14;
         A1551Servico_Responsavel = GX_Parm15;
         A827ContratoServicos_ServicoCod = GX_Parm16;
         A640Servico_Vinculados = GX_Parm17;
         /* Using cursor T000X66 */
         pr_default.execute(59, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(59) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
         }
         A608Servico_Nome = T000X66_A608Servico_Nome[0];
         A605Servico_Sigla = T000X66_A605Servico_Sigla[0];
         A1061Servico_Tela = T000X66_A1061Servico_Tela[0];
         n1061Servico_Tela = T000X66_n1061Servico_Tela[0];
         A632Servico_Ativo = T000X66_A632Servico_Ativo[0];
         A156Servico_Descricao = T000X66_A156Servico_Descricao[0];
         n156Servico_Descricao = T000X66_n156Servico_Descricao[0];
         A2092Servico_IsOrigemReferencia = T000X66_A2092Servico_IsOrigemReferencia[0];
         A631Servico_Vinculado = T000X66_A631Servico_Vinculado[0];
         n631Servico_Vinculado = T000X66_n631Servico_Vinculado[0];
         A157ServicoGrupo_Codigo = T000X66_A157ServicoGrupo_Codigo[0];
         pr_default.close(59);
         AV32Servico_Nome = A608Servico_Nome;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
         /* Using cursor T000X67 */
         pr_default.execute(60, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(60) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A158ServicoGrupo_Descricao = T000X67_A158ServicoGrupo_Descricao[0];
         pr_default.close(60);
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         A1551Servico_Responsavel = GXt_int2;
         A827ContratoServicos_ServicoCod = A155Servico_Codigo;
         GXt_int3 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
         A640Servico_Vinculados = GXt_int3;
         if ( (0==A155Servico_Codigo) )
         {
            GX_msglist.addItem("Deve indicar o servi�o a incluir no contrato!", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
         }
         O638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A608Servico_Nome = "";
            A605Servico_Sigla = "";
            A1061Servico_Tela = "";
            n1061Servico_Tela = false;
            A632Servico_Ativo = false;
            A156Servico_Descricao = "";
            n156Servico_Descricao = false;
            A2092Servico_IsOrigemReferencia = false;
            A631Servico_Vinculado = 0;
            n631Servico_Vinculado = false;
            A157ServicoGrupo_Codigo = 0;
            A158ServicoGrupo_Descricao = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A608Servico_Nome));
         isValidOutput.Add(StringUtil.RTrim( A605Servico_Sigla));
         isValidOutput.Add(StringUtil.RTrim( A1061Servico_Tela));
         isValidOutput.Add(A632Servico_Ativo);
         isValidOutput.Add(A156Servico_Descricao);
         isValidOutput.Add(A2092Servico_IsOrigemReferencia);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( AV32Servico_Nome));
         isValidOutput.Add(A2107Servico_Identificacao);
         isValidOutput.Add(StringUtil.RTrim( A826ContratoServicos_ServicoSigla));
         isValidOutput.Add(A158ServicoGrupo_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A827ContratoServicos_ServicoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E130X2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV29AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOREPLICAR'","{handler:'E140X2',iparms:[{av:'A1516ContratoServicos_TmpEstAnl',fld:'CONTRATOSERVICOS_TMPESTANL',pic:'ZZZZZZZ9',nv:0},{av:'A1501ContratoServicos_TmpEstExc',fld:'CONTRATOSERVICOS_TMPESTEXC',pic:'ZZZZZZZ9',nv:0},{av:'A1502ContratoServicos_TmpEstCrr',fld:'CONTRATOSERVICOS_TMPESTCRR',pic:'ZZZZZZZ9',nv:0},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOUPDPRAZO'","{handler:'E150X2',iparms:[{av:'A911ContratoServicos_Prazos',fld:'CONTRATOSERVICOS_PRAZOS',pic:'ZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VSERVICO_CODIGO.CLICK","{handler:'E160X2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'dynavServico_codigo'},{av:'AV15SubServico_Codigo',fld:'vSUBSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30Alias',fld:'vALIAS',pic:'@!',nv:''},{av:'AV32Servico_Nome',fld:'vSERVICO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VSUBSERVICO_CODIGO.CLICK","{handler:'E170X2',iparms:[{av:'AV15SubServico_Codigo',fld:'vSUBSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV32Servico_Nome',fld:'vSERVICO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("CONTRATOSERVICOS_PRAZOCORRECAOTIPO.CLICK","{handler:'E110X34',iparms:[{av:'A1225ContratoServicos_PrazoCorrecaoTipo',fld:'CONTRATOSERVICOS_PRAZOCORRECAOTIPO',pic:'',nv:''}],oparms:[{av:'edtContratoServicos_PrazoCorrecao_Visible',ctrl:'CONTRATOSERVICOS_PRAZOCORRECAO',prop:'Visible'},{av:'lblContratoservicos_prazocorrecao_righttext_Caption',ctrl:'CONTRATOSERVICOS_PRAZOCORRECAO_RIGHTTEXT',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(59);
         pr_default.close(27);
         pr_default.close(30);
         pr_default.close(58);
         pr_default.close(29);
         pr_default.close(60);
         pr_default.close(28);
         pr_default.close(31);
         pr_default.close(32);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1225ContratoServicos_PrazoCorrecaoTipo = "";
         Z1858ContratoServicos_Alias = "";
         Z607ServicoContrato_Faturamento = "";
         Z639ContratoServicos_LocalExec = "";
         Z868ContratoServicos_TipoVnc = "";
         Z1454ContratoServicos_PrazoTpDias = "";
         Z1266ContratoServicos_Momento = "";
         Z1325ContratoServicos_StatusPagFnc = "";
         Z1723ContratoServicos_CodigoFiscal = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1454ContratoServicos_PrazoTpDias = "";
         A913ContratoServicos_PrazoTipo = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A1325ContratoServicos_StatusPagFnc = "";
         A639ContratoServicos_LocalExec = "";
         A607ServicoContrato_Faturamento = "";
         A1266ContratoServicos_Momento = "";
         A868ContratoServicos_TipoVnc = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         GridContainer = new GXWebGrid( context);
         GridColumn = new GXWebColumn();
         sMode228 = "";
         lblTextblockcontratoservicos_indicedivergencia_Jsonclick = "";
         lblTextblockcontratoservicos_naorequeratr_Jsonclick = "";
         lblTextblockcontratoservicos_limiteproposta_Jsonclick = "";
         lblTextblockcontratoservicos_localexec_Jsonclick = "";
         lblTextblockcontratoservicos_unidadecontratada_Jsonclick = "";
         lblTextblockcontratoservicos_fatorcnvundcnt_Jsonclick = "";
         lblTextblockservico_qtdcontratada_Jsonclick = "";
         lblTextblockservico_vlrunidadecontratada_Jsonclick = "";
         lblTextblockcontratoservicos_produtividade_Jsonclick = "";
         lblTextblockservicocontrato_faturamento_Jsonclick = "";
         lblTextblockservico_percentual_Jsonclick = "";
         lblTextblockcontratoservicos_calculormn_Jsonclick = "";
         lblTextblockcontratoservicos_perccnc_Jsonclick = "";
         lblTextblockcontratoservicos_espelhaaceite_Jsonclick = "";
         lblTextblockcontratoservicos_momento_Jsonclick = "";
         lblTextblockcontratoservicos_tipovnc_Jsonclick = "";
         lblTextblockcontratoservicos_qntuntcns_Jsonclick = "";
         lblTextblockcontratoservicos_codigofiscal_Jsonclick = "";
         A1723ContratoServicos_CodigoFiscal = "";
         lblTextblockcontratoservicos_solicitagestorsistema_Jsonclick = "";
         lblTextblockcontratoservicos_ativo_Jsonclick = "";
         lblContratoservicos_perccnc_righttext_Jsonclick = "";
         lblServico_percentual_righttext_Jsonclick = "";
         lblContratoservicos_produtividade_righttext_Jsonclick = "";
         lblContratoservicos_limiteproposta_righttext_Jsonclick = "";
         lblContratoservicos_indicedivergencia_righttext_Jsonclick = "";
         lblTextblockcontratoservicos_prazotpdias_Jsonclick = "";
         lblTextblockcontratoservicos_prazoinicio_Jsonclick = "";
         lblTextblockcontratoservicos_prazoanalise_Jsonclick = "";
         lblTextblockcontratoservicos_prazotipo_Jsonclick = "";
         lblTextblockcontratoservicos_prazoresposta_Jsonclick = "";
         lblTextblockcontratoservicos_prazogarantia_Jsonclick = "";
         lblTextblockcontratoservicos_prazoatendegarantia_Jsonclick = "";
         lblTextblockcontratoservicos_prazocorrecaotipo_Jsonclick = "";
         lblTextblockcontratoservicos_tmpestanl_Jsonclick = "";
         lblTextblockcontratoservicos_tmpestexc_Jsonclick = "";
         lblTextblockcontratoservicos_tmpestcrr_Jsonclick = "";
         lblTextblockcontratoservicos_statuspagfnc_Jsonclick = "";
         lblContratoservicos_tmpestcrr_righttext_Jsonclick = "";
         bttBtnreplicar_Jsonclick = "";
         lblContratoservicos_tmpestexc_righttext_Jsonclick = "";
         lblContratoservicos_tmpestanl_righttext_Jsonclick = "";
         lblTextblockcontratoservicos_prazocorrecao_Jsonclick = "";
         lblContratoservicos_prazocorrecao_righttext_Jsonclick = "";
         lblContratoservicos_prazodias_righttext_Jsonclick = "";
         imgUpdprazo_Jsonclick = "";
         lblTextblockservicogrupo_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblocksubservico_codigo_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         AV32Servico_Nome = "";
         lblTextblockalias_Jsonclick = "";
         AV30Alias = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         lblTextblockcontrato_valorunidadecontratacao_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         A1858ContratoServicos_Alias = "";
         A605Servico_Sigla = "";
         A608Servico_Nome = "";
         A2107Servico_Identificacao = "";
         A826ContratoServicos_ServicoSigla = "";
         A516Contratada_TipoFabrica = "";
         A1712ContratoServicos_UndCntSgl = "";
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A1061Servico_Tela = "";
         A156Servico_Descricao = "";
         A2132ContratoServicos_UndCntNome = "";
         A158ServicoGrupo_Descricao = "";
         AV36Pgmname = "";
         Gxuitabspanel_tab_Height = "";
         Gxuitabspanel_tab_Class = "";
         Gxuitabspanel_tab_Activetabid = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode34 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         GXt_char1 = "";
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z516Contratada_TipoFabrica = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         Z913ContratoServicos_PrazoTipo = "";
         Z608Servico_Nome = "";
         Z605Servico_Sigla = "";
         Z1061Servico_Tela = "";
         Z156Servico_Descricao = "";
         Z158ServicoGrupo_Descricao = "";
         Z2132ContratoServicos_UndCntNome = "";
         Z1712ContratoServicos_UndCntSgl = "";
         T000X7_A911ContratoServicos_Prazos = new short[1] ;
         T000X7_n911ContratoServicos_Prazos = new bool[] {false} ;
         T000X14_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         T000X14_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         T000X14_A914ContratoServicos_PrazoDias = new short[1] ;
         T000X14_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         T000X16_A1377ContratoServicos_Indicadores = new short[1] ;
         T000X16_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         T000X18_A2073ContratoServicos_QtdRmn = new short[1] ;
         T000X18_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         T000X10_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         T000X10_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         T000X10_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T000X10_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T000X8_A608Servico_Nome = new String[] {""} ;
         T000X8_A605Servico_Sigla = new String[] {""} ;
         T000X8_A1061Servico_Tela = new String[] {""} ;
         T000X8_n1061Servico_Tela = new bool[] {false} ;
         T000X8_A632Servico_Ativo = new bool[] {false} ;
         T000X8_A156Servico_Descricao = new String[] {""} ;
         T000X8_n156Servico_Descricao = new bool[] {false} ;
         T000X8_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000X8_A631Servico_Vinculado = new int[1] ;
         T000X8_n631Servico_Vinculado = new bool[] {false} ;
         T000X8_A157ServicoGrupo_Codigo = new int[1] ;
         T000X11_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000X9_A77Contrato_Numero = new String[] {""} ;
         T000X9_A78Contrato_NumeroAta = new String[] {""} ;
         T000X9_n78Contrato_NumeroAta = new bool[] {false} ;
         T000X9_A79Contrato_Ano = new short[1] ;
         T000X9_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000X9_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000X9_A39Contratada_Codigo = new int[1] ;
         T000X9_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000X12_A516Contratada_TipoFabrica = new String[] {""} ;
         T000X12_A40Contratada_PessoaCod = new int[1] ;
         T000X13_A41Contratada_PessoaNom = new String[] {""} ;
         T000X13_n41Contratada_PessoaNom = new bool[] {false} ;
         T000X13_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000X13_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000X21_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T000X21_A160ContratoServicos_Codigo = new int[1] ;
         T000X21_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         T000X21_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         T000X21_A77Contrato_Numero = new String[] {""} ;
         T000X21_A78Contrato_NumeroAta = new String[] {""} ;
         T000X21_n78Contrato_NumeroAta = new bool[] {false} ;
         T000X21_A79Contrato_Ano = new short[1] ;
         T000X21_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000X21_A41Contratada_PessoaNom = new String[] {""} ;
         T000X21_n41Contratada_PessoaNom = new bool[] {false} ;
         T000X21_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000X21_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000X21_A516Contratada_TipoFabrica = new String[] {""} ;
         T000X21_A1858ContratoServicos_Alias = new String[] {""} ;
         T000X21_n1858ContratoServicos_Alias = new bool[] {false} ;
         T000X21_A608Servico_Nome = new String[] {""} ;
         T000X21_A605Servico_Sigla = new String[] {""} ;
         T000X21_A1061Servico_Tela = new String[] {""} ;
         T000X21_n1061Servico_Tela = new bool[] {false} ;
         T000X21_A632Servico_Ativo = new bool[] {false} ;
         T000X21_A156Servico_Descricao = new String[] {""} ;
         T000X21_n156Servico_Descricao = new bool[] {false} ;
         T000X21_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000X21_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000X21_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         T000X21_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         T000X21_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T000X21_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T000X21_A555Servico_QtdContratada = new long[1] ;
         T000X21_n555Servico_QtdContratada = new bool[] {false} ;
         T000X21_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         T000X21_A558Servico_Percentual = new decimal[1] ;
         T000X21_n558Servico_Percentual = new bool[] {false} ;
         T000X21_A607ServicoContrato_Faturamento = new String[] {""} ;
         T000X21_A639ContratoServicos_LocalExec = new String[] {""} ;
         T000X21_A868ContratoServicos_TipoVnc = new String[] {""} ;
         T000X21_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         T000X21_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         T000X21_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         T000X21_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         T000X21_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         T000X21_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         T000X21_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         T000X21_A1153ContratoServicos_PrazoResposta = new short[1] ;
         T000X21_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         T000X21_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         T000X21_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         T000X21_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         T000X21_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         T000X21_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         T000X21_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         T000X21_A1649ContratoServicos_PrazoInicio = new short[1] ;
         T000X21_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         T000X21_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         T000X21_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         T000X21_A1191ContratoServicos_Produtividade = new decimal[1] ;
         T000X21_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         T000X21_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         T000X21_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         T000X21_A1266ContratoServicos_Momento = new String[] {""} ;
         T000X21_n1266ContratoServicos_Momento = new bool[] {false} ;
         T000X21_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         T000X21_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         T000X21_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         T000X21_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         T000X21_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         T000X21_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         T000X21_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         T000X21_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         T000X21_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000X21_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         T000X21_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         T000X21_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         T000X21_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         T000X21_A1501ContratoServicos_TmpEstExc = new int[1] ;
         T000X21_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         T000X21_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         T000X21_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         T000X21_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         T000X21_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         T000X21_A1537ContratoServicos_PercTmp = new short[1] ;
         T000X21_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         T000X21_A1538ContratoServicos_PercPgm = new short[1] ;
         T000X21_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         T000X21_A1539ContratoServicos_PercCnc = new short[1] ;
         T000X21_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         T000X21_A638ContratoServicos_Ativo = new bool[] {false} ;
         T000X21_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         T000X21_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         T000X21_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         T000X21_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         T000X21_A2074ContratoServicos_CalculoRmn = new short[1] ;
         T000X21_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         T000X21_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         T000X21_A155Servico_Codigo = new int[1] ;
         T000X21_A74Contrato_Codigo = new int[1] ;
         T000X21_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T000X21_A631Servico_Vinculado = new int[1] ;
         T000X21_n631Servico_Vinculado = new bool[] {false} ;
         T000X21_A157ServicoGrupo_Codigo = new int[1] ;
         T000X21_A39Contratada_Codigo = new int[1] ;
         T000X21_A40Contratada_PessoaCod = new int[1] ;
         T000X21_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000X21_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         T000X21_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         T000X21_A914ContratoServicos_PrazoDias = new short[1] ;
         T000X21_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         T000X21_A1377ContratoServicos_Indicadores = new short[1] ;
         T000X21_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         T000X21_A2073ContratoServicos_QtdRmn = new short[1] ;
         T000X21_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         T000X22_A608Servico_Nome = new String[] {""} ;
         T000X22_A605Servico_Sigla = new String[] {""} ;
         T000X22_A1061Servico_Tela = new String[] {""} ;
         T000X22_n1061Servico_Tela = new bool[] {false} ;
         T000X22_A632Servico_Ativo = new bool[] {false} ;
         T000X22_A156Servico_Descricao = new String[] {""} ;
         T000X22_n156Servico_Descricao = new bool[] {false} ;
         T000X22_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000X22_A631Servico_Vinculado = new int[1] ;
         T000X22_n631Servico_Vinculado = new bool[] {false} ;
         T000X22_A157ServicoGrupo_Codigo = new int[1] ;
         T000X23_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000X24_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         T000X24_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         T000X24_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T000X24_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T000X25_A77Contrato_Numero = new String[] {""} ;
         T000X25_A78Contrato_NumeroAta = new String[] {""} ;
         T000X25_n78Contrato_NumeroAta = new bool[] {false} ;
         T000X25_A79Contrato_Ano = new short[1] ;
         T000X25_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000X25_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000X25_A39Contratada_Codigo = new int[1] ;
         T000X25_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000X26_A516Contratada_TipoFabrica = new String[] {""} ;
         T000X26_A40Contratada_PessoaCod = new int[1] ;
         T000X27_A41Contratada_PessoaNom = new String[] {""} ;
         T000X27_n41Contratada_PessoaNom = new bool[] {false} ;
         T000X27_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000X27_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000X28_A160ContratoServicos_Codigo = new int[1] ;
         T000X5_A160ContratoServicos_Codigo = new int[1] ;
         T000X5_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         T000X5_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         T000X5_A1858ContratoServicos_Alias = new String[] {""} ;
         T000X5_n1858ContratoServicos_Alias = new bool[] {false} ;
         T000X5_A555Servico_QtdContratada = new long[1] ;
         T000X5_n555Servico_QtdContratada = new bool[] {false} ;
         T000X5_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         T000X5_A558Servico_Percentual = new decimal[1] ;
         T000X5_n558Servico_Percentual = new bool[] {false} ;
         T000X5_A607ServicoContrato_Faturamento = new String[] {""} ;
         T000X5_A639ContratoServicos_LocalExec = new String[] {""} ;
         T000X5_A868ContratoServicos_TipoVnc = new String[] {""} ;
         T000X5_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         T000X5_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         T000X5_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         T000X5_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         T000X5_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         T000X5_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         T000X5_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         T000X5_A1153ContratoServicos_PrazoResposta = new short[1] ;
         T000X5_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         T000X5_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         T000X5_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         T000X5_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         T000X5_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         T000X5_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         T000X5_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         T000X5_A1649ContratoServicos_PrazoInicio = new short[1] ;
         T000X5_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         T000X5_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         T000X5_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         T000X5_A1191ContratoServicos_Produtividade = new decimal[1] ;
         T000X5_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         T000X5_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         T000X5_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         T000X5_A1266ContratoServicos_Momento = new String[] {""} ;
         T000X5_n1266ContratoServicos_Momento = new bool[] {false} ;
         T000X5_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         T000X5_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         T000X5_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         T000X5_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         T000X5_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         T000X5_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         T000X5_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         T000X5_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         T000X5_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         T000X5_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         T000X5_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         T000X5_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         T000X5_A1501ContratoServicos_TmpEstExc = new int[1] ;
         T000X5_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         T000X5_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         T000X5_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         T000X5_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         T000X5_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         T000X5_A1537ContratoServicos_PercTmp = new short[1] ;
         T000X5_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         T000X5_A1538ContratoServicos_PercPgm = new short[1] ;
         T000X5_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         T000X5_A1539ContratoServicos_PercCnc = new short[1] ;
         T000X5_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         T000X5_A638ContratoServicos_Ativo = new bool[] {false} ;
         T000X5_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         T000X5_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         T000X5_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         T000X5_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         T000X5_A2074ContratoServicos_CalculoRmn = new short[1] ;
         T000X5_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         T000X5_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         T000X5_A155Servico_Codigo = new int[1] ;
         T000X5_A74Contrato_Codigo = new int[1] ;
         T000X5_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T000X29_A160ContratoServicos_Codigo = new int[1] ;
         T000X30_A160ContratoServicos_Codigo = new int[1] ;
         T000X4_A160ContratoServicos_Codigo = new int[1] ;
         T000X4_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         T000X4_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         T000X4_A1858ContratoServicos_Alias = new String[] {""} ;
         T000X4_n1858ContratoServicos_Alias = new bool[] {false} ;
         T000X4_A555Servico_QtdContratada = new long[1] ;
         T000X4_n555Servico_QtdContratada = new bool[] {false} ;
         T000X4_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         T000X4_A558Servico_Percentual = new decimal[1] ;
         T000X4_n558Servico_Percentual = new bool[] {false} ;
         T000X4_A607ServicoContrato_Faturamento = new String[] {""} ;
         T000X4_A639ContratoServicos_LocalExec = new String[] {""} ;
         T000X4_A868ContratoServicos_TipoVnc = new String[] {""} ;
         T000X4_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         T000X4_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         T000X4_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         T000X4_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         T000X4_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         T000X4_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         T000X4_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         T000X4_A1153ContratoServicos_PrazoResposta = new short[1] ;
         T000X4_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         T000X4_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         T000X4_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         T000X4_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         T000X4_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         T000X4_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         T000X4_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         T000X4_A1649ContratoServicos_PrazoInicio = new short[1] ;
         T000X4_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         T000X4_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         T000X4_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         T000X4_A1191ContratoServicos_Produtividade = new decimal[1] ;
         T000X4_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         T000X4_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         T000X4_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         T000X4_A1266ContratoServicos_Momento = new String[] {""} ;
         T000X4_n1266ContratoServicos_Momento = new bool[] {false} ;
         T000X4_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         T000X4_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         T000X4_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         T000X4_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         T000X4_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         T000X4_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         T000X4_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         T000X4_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         T000X4_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         T000X4_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         T000X4_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         T000X4_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         T000X4_A1501ContratoServicos_TmpEstExc = new int[1] ;
         T000X4_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         T000X4_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         T000X4_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         T000X4_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         T000X4_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         T000X4_A1537ContratoServicos_PercTmp = new short[1] ;
         T000X4_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         T000X4_A1538ContratoServicos_PercPgm = new short[1] ;
         T000X4_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         T000X4_A1539ContratoServicos_PercCnc = new short[1] ;
         T000X4_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         T000X4_A638ContratoServicos_Ativo = new bool[] {false} ;
         T000X4_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         T000X4_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         T000X4_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         T000X4_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         T000X4_A2074ContratoServicos_CalculoRmn = new short[1] ;
         T000X4_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         T000X4_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         T000X4_A155Servico_Codigo = new int[1] ;
         T000X4_A74Contrato_Codigo = new int[1] ;
         T000X4_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T000X31_A160ContratoServicos_Codigo = new int[1] ;
         T000X34_A608Servico_Nome = new String[] {""} ;
         T000X34_A605Servico_Sigla = new String[] {""} ;
         T000X34_A1061Servico_Tela = new String[] {""} ;
         T000X34_n1061Servico_Tela = new bool[] {false} ;
         T000X34_A632Servico_Ativo = new bool[] {false} ;
         T000X34_A156Servico_Descricao = new String[] {""} ;
         T000X34_n156Servico_Descricao = new bool[] {false} ;
         T000X34_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000X34_A631Servico_Vinculado = new int[1] ;
         T000X34_n631Servico_Vinculado = new bool[] {false} ;
         T000X34_A157ServicoGrupo_Codigo = new int[1] ;
         T000X35_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000X36_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         T000X36_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         T000X36_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T000X36_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T000X37_A77Contrato_Numero = new String[] {""} ;
         T000X37_A78Contrato_NumeroAta = new String[] {""} ;
         T000X37_n78Contrato_NumeroAta = new bool[] {false} ;
         T000X37_A79Contrato_Ano = new short[1] ;
         T000X37_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000X37_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000X37_A39Contratada_Codigo = new int[1] ;
         T000X37_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000X38_A516Contratada_TipoFabrica = new String[] {""} ;
         T000X38_A40Contratada_PessoaCod = new int[1] ;
         T000X39_A41Contratada_PessoaNom = new String[] {""} ;
         T000X39_n41Contratada_PessoaNom = new bool[] {false} ;
         T000X39_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000X39_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000X40_A192Contagem_Codigo = new int[1] ;
         T000X41_A456ContagemResultado_Codigo = new int[1] ;
         T000X42_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T000X43_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         T000X44_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         T000X45_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T000X45_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T000X45_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T000X46_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         T000X46_A938ContratoServicosTelas_Sequencial = new short[1] ;
         T000X47_A917ContratoSrvVnc_Codigo = new int[1] ;
         T000X48_A917ContratoSrvVnc_Codigo = new int[1] ;
         T000X49_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T000X50_A160ContratoServicos_Codigo = new int[1] ;
         T000X50_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T000X51_A160ContratoServicos_Codigo = new int[1] ;
         T000X51_A1749Artefatos_Codigo = new int[1] ;
         T000X52_A160ContratoServicos_Codigo = new int[1] ;
         T000X52_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T000X53_A160ContratoServicos_Codigo = new int[1] ;
         T000X53_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T000X53_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T000X54_A160ContratoServicos_Codigo = new int[1] ;
         T000X55_A160ContratoServicos_Codigo = new int[1] ;
         T000X55_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         T000X55_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         T000X55_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         T000X55_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         T000X56_A160ContratoServicos_Codigo = new int[1] ;
         T000X56_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         T000X3_A160ContratoServicos_Codigo = new int[1] ;
         T000X3_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         T000X3_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         T000X3_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         T000X3_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         T000X2_A160ContratoServicos_Codigo = new int[1] ;
         T000X2_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         T000X2_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         T000X2_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         T000X2_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         T000X60_A160ContratoServicos_Codigo = new int[1] ;
         T000X60_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         GridRow = new GXWebRow();
         subGrid_Linesclass = "";
         ROClassString = "";
         GXCCtl = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i607ServicoContrato_Faturamento = "";
         i1454ContratoServicos_PrazoTpDias = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000X61_A157ServicoGrupo_Codigo = new int[1] ;
         T000X61_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000X62_A155Servico_Codigo = new int[1] ;
         T000X62_A605Servico_Sigla = new String[] {""} ;
         T000X62_A157ServicoGrupo_Codigo = new int[1] ;
         T000X62_A1530Servico_TipoHierarquia = new short[1] ;
         T000X62_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T000X62_A632Servico_Ativo = new bool[] {false} ;
         T000X63_A155Servico_Codigo = new int[1] ;
         T000X63_A608Servico_Nome = new String[] {""} ;
         T000X63_A631Servico_Vinculado = new int[1] ;
         T000X63_n631Servico_Vinculado = new bool[] {false} ;
         T000X63_A632Servico_Ativo = new bool[] {false} ;
         T000X64_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T000X64_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T000X64_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T000X64_A1207ContratoUnidades_ContratoCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T000X65_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         T000X65_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         T000X65_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T000X65_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T000X66_A608Servico_Nome = new String[] {""} ;
         T000X66_A605Servico_Sigla = new String[] {""} ;
         T000X66_A1061Servico_Tela = new String[] {""} ;
         T000X66_n1061Servico_Tela = new bool[] {false} ;
         T000X66_A632Servico_Ativo = new bool[] {false} ;
         T000X66_A156Servico_Descricao = new String[] {""} ;
         T000X66_n156Servico_Descricao = new bool[] {false} ;
         T000X66_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000X66_A631Servico_Vinculado = new int[1] ;
         T000X66_n631Servico_Vinculado = new bool[] {false} ;
         T000X66_A157ServicoGrupo_Codigo = new int[1] ;
         T000X67_A158ServicoGrupo_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicos__default(),
            new Object[][] {
                new Object[] {
               T000X2_A160ContratoServicos_Codigo, T000X2_A2069ContratoServicosRmn_Sequencial, T000X2_A2070ContratoServicosRmn_Inicio, T000X2_A2071ContratoServicosRmn_Fim, T000X2_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               T000X3_A160ContratoServicos_Codigo, T000X3_A2069ContratoServicosRmn_Sequencial, T000X3_A2070ContratoServicosRmn_Inicio, T000X3_A2071ContratoServicosRmn_Fim, T000X3_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               T000X4_A160ContratoServicos_Codigo, T000X4_A1225ContratoServicos_PrazoCorrecaoTipo, T000X4_n1225ContratoServicos_PrazoCorrecaoTipo, T000X4_A1858ContratoServicos_Alias, T000X4_n1858ContratoServicos_Alias, T000X4_A555Servico_QtdContratada, T000X4_n555Servico_QtdContratada, T000X4_A557Servico_VlrUnidadeContratada, T000X4_A558Servico_Percentual, T000X4_n558Servico_Percentual,
               T000X4_A607ServicoContrato_Faturamento, T000X4_A639ContratoServicos_LocalExec, T000X4_A868ContratoServicos_TipoVnc, T000X4_n868ContratoServicos_TipoVnc, T000X4_A888ContratoServicos_HmlSemCnf, T000X4_n888ContratoServicos_HmlSemCnf, T000X4_A1454ContratoServicos_PrazoTpDias, T000X4_n1454ContratoServicos_PrazoTpDias, T000X4_A1152ContratoServicos_PrazoAnalise, T000X4_n1152ContratoServicos_PrazoAnalise,
               T000X4_A1153ContratoServicos_PrazoResposta, T000X4_n1153ContratoServicos_PrazoResposta, T000X4_A1181ContratoServicos_PrazoGarantia, T000X4_n1181ContratoServicos_PrazoGarantia, T000X4_A1182ContratoServicos_PrazoAtendeGarantia, T000X4_n1182ContratoServicos_PrazoAtendeGarantia, T000X4_A1224ContratoServicos_PrazoCorrecao, T000X4_n1224ContratoServicos_PrazoCorrecao, T000X4_A1649ContratoServicos_PrazoInicio, T000X4_n1649ContratoServicos_PrazoInicio,
               T000X4_A1190ContratoServicos_PrazoImediato, T000X4_n1190ContratoServicos_PrazoImediato, T000X4_A1191ContratoServicos_Produtividade, T000X4_n1191ContratoServicos_Produtividade, T000X4_A1217ContratoServicos_EspelhaAceite, T000X4_n1217ContratoServicos_EspelhaAceite, T000X4_A1266ContratoServicos_Momento, T000X4_n1266ContratoServicos_Momento, T000X4_A1325ContratoServicos_StatusPagFnc, T000X4_n1325ContratoServicos_StatusPagFnc,
               T000X4_A1340ContratoServicos_QntUntCns, T000X4_n1340ContratoServicos_QntUntCns, T000X4_A1341ContratoServicos_FatorCnvUndCnt, T000X4_n1341ContratoServicos_FatorCnvUndCnt, T000X4_A1397ContratoServicos_NaoRequerAtr, T000X4_n1397ContratoServicos_NaoRequerAtr, T000X4_A1455ContratoServicos_IndiceDivergencia, T000X4_n1455ContratoServicos_IndiceDivergencia, T000X4_A1516ContratoServicos_TmpEstAnl, T000X4_n1516ContratoServicos_TmpEstAnl,
               T000X4_A1501ContratoServicos_TmpEstExc, T000X4_n1501ContratoServicos_TmpEstExc, T000X4_A1502ContratoServicos_TmpEstCrr, T000X4_n1502ContratoServicos_TmpEstCrr, T000X4_A1531ContratoServicos_TipoHierarquia, T000X4_n1531ContratoServicos_TipoHierarquia, T000X4_A1537ContratoServicos_PercTmp, T000X4_n1537ContratoServicos_PercTmp, T000X4_A1538ContratoServicos_PercPgm, T000X4_n1538ContratoServicos_PercPgm,
               T000X4_A1539ContratoServicos_PercCnc, T000X4_n1539ContratoServicos_PercCnc, T000X4_A638ContratoServicos_Ativo, T000X4_A1723ContratoServicos_CodigoFiscal, T000X4_n1723ContratoServicos_CodigoFiscal, T000X4_A1799ContratoServicos_LimiteProposta, T000X4_n1799ContratoServicos_LimiteProposta, T000X4_A2074ContratoServicos_CalculoRmn, T000X4_n2074ContratoServicos_CalculoRmn, T000X4_A2094ContratoServicos_SolicitaGestorSistema,
               T000X4_A155Servico_Codigo, T000X4_A74Contrato_Codigo, T000X4_A1212ContratoServicos_UnidadeContratada
               }
               , new Object[] {
               T000X5_A160ContratoServicos_Codigo, T000X5_A1225ContratoServicos_PrazoCorrecaoTipo, T000X5_n1225ContratoServicos_PrazoCorrecaoTipo, T000X5_A1858ContratoServicos_Alias, T000X5_n1858ContratoServicos_Alias, T000X5_A555Servico_QtdContratada, T000X5_n555Servico_QtdContratada, T000X5_A557Servico_VlrUnidadeContratada, T000X5_A558Servico_Percentual, T000X5_n558Servico_Percentual,
               T000X5_A607ServicoContrato_Faturamento, T000X5_A639ContratoServicos_LocalExec, T000X5_A868ContratoServicos_TipoVnc, T000X5_n868ContratoServicos_TipoVnc, T000X5_A888ContratoServicos_HmlSemCnf, T000X5_n888ContratoServicos_HmlSemCnf, T000X5_A1454ContratoServicos_PrazoTpDias, T000X5_n1454ContratoServicos_PrazoTpDias, T000X5_A1152ContratoServicos_PrazoAnalise, T000X5_n1152ContratoServicos_PrazoAnalise,
               T000X5_A1153ContratoServicos_PrazoResposta, T000X5_n1153ContratoServicos_PrazoResposta, T000X5_A1181ContratoServicos_PrazoGarantia, T000X5_n1181ContratoServicos_PrazoGarantia, T000X5_A1182ContratoServicos_PrazoAtendeGarantia, T000X5_n1182ContratoServicos_PrazoAtendeGarantia, T000X5_A1224ContratoServicos_PrazoCorrecao, T000X5_n1224ContratoServicos_PrazoCorrecao, T000X5_A1649ContratoServicos_PrazoInicio, T000X5_n1649ContratoServicos_PrazoInicio,
               T000X5_A1190ContratoServicos_PrazoImediato, T000X5_n1190ContratoServicos_PrazoImediato, T000X5_A1191ContratoServicos_Produtividade, T000X5_n1191ContratoServicos_Produtividade, T000X5_A1217ContratoServicos_EspelhaAceite, T000X5_n1217ContratoServicos_EspelhaAceite, T000X5_A1266ContratoServicos_Momento, T000X5_n1266ContratoServicos_Momento, T000X5_A1325ContratoServicos_StatusPagFnc, T000X5_n1325ContratoServicos_StatusPagFnc,
               T000X5_A1340ContratoServicos_QntUntCns, T000X5_n1340ContratoServicos_QntUntCns, T000X5_A1341ContratoServicos_FatorCnvUndCnt, T000X5_n1341ContratoServicos_FatorCnvUndCnt, T000X5_A1397ContratoServicos_NaoRequerAtr, T000X5_n1397ContratoServicos_NaoRequerAtr, T000X5_A1455ContratoServicos_IndiceDivergencia, T000X5_n1455ContratoServicos_IndiceDivergencia, T000X5_A1516ContratoServicos_TmpEstAnl, T000X5_n1516ContratoServicos_TmpEstAnl,
               T000X5_A1501ContratoServicos_TmpEstExc, T000X5_n1501ContratoServicos_TmpEstExc, T000X5_A1502ContratoServicos_TmpEstCrr, T000X5_n1502ContratoServicos_TmpEstCrr, T000X5_A1531ContratoServicos_TipoHierarquia, T000X5_n1531ContratoServicos_TipoHierarquia, T000X5_A1537ContratoServicos_PercTmp, T000X5_n1537ContratoServicos_PercTmp, T000X5_A1538ContratoServicos_PercPgm, T000X5_n1538ContratoServicos_PercPgm,
               T000X5_A1539ContratoServicos_PercCnc, T000X5_n1539ContratoServicos_PercCnc, T000X5_A638ContratoServicos_Ativo, T000X5_A1723ContratoServicos_CodigoFiscal, T000X5_n1723ContratoServicos_CodigoFiscal, T000X5_A1799ContratoServicos_LimiteProposta, T000X5_n1799ContratoServicos_LimiteProposta, T000X5_A2074ContratoServicos_CalculoRmn, T000X5_n2074ContratoServicos_CalculoRmn, T000X5_A2094ContratoServicos_SolicitaGestorSistema,
               T000X5_A155Servico_Codigo, T000X5_A74Contrato_Codigo, T000X5_A1212ContratoServicos_UnidadeContratada
               }
               , new Object[] {
               T000X7_A911ContratoServicos_Prazos, T000X7_n911ContratoServicos_Prazos
               }
               , new Object[] {
               T000X8_A608Servico_Nome, T000X8_A605Servico_Sigla, T000X8_A1061Servico_Tela, T000X8_n1061Servico_Tela, T000X8_A632Servico_Ativo, T000X8_A156Servico_Descricao, T000X8_n156Servico_Descricao, T000X8_A2092Servico_IsOrigemReferencia, T000X8_A631Servico_Vinculado, T000X8_n631Servico_Vinculado,
               T000X8_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000X9_A77Contrato_Numero, T000X9_A78Contrato_NumeroAta, T000X9_n78Contrato_NumeroAta, T000X9_A79Contrato_Ano, T000X9_A116Contrato_ValorUnidadeContratacao, T000X9_A453Contrato_IndiceDivergencia, T000X9_A39Contratada_Codigo, T000X9_A75Contrato_AreaTrabalhoCod
               }
               , new Object[] {
               T000X10_A2132ContratoServicos_UndCntNome, T000X10_n2132ContratoServicos_UndCntNome, T000X10_A1712ContratoServicos_UndCntSgl, T000X10_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T000X11_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000X12_A516Contratada_TipoFabrica, T000X12_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000X13_A41Contratada_PessoaNom, T000X13_n41Contratada_PessoaNom, T000X13_A42Contratada_PessoaCNPJ, T000X13_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000X14_A913ContratoServicos_PrazoTipo, T000X14_n913ContratoServicos_PrazoTipo, T000X14_A914ContratoServicos_PrazoDias, T000X14_n914ContratoServicos_PrazoDias
               }
               , new Object[] {
               T000X16_A1377ContratoServicos_Indicadores, T000X16_n1377ContratoServicos_Indicadores
               }
               , new Object[] {
               T000X18_A2073ContratoServicos_QtdRmn, T000X18_n2073ContratoServicos_QtdRmn
               }
               , new Object[] {
               T000X21_A903ContratoServicosPrazo_CntSrvCod, T000X21_A160ContratoServicos_Codigo, T000X21_A1225ContratoServicos_PrazoCorrecaoTipo, T000X21_n1225ContratoServicos_PrazoCorrecaoTipo, T000X21_A77Contrato_Numero, T000X21_A78Contrato_NumeroAta, T000X21_n78Contrato_NumeroAta, T000X21_A79Contrato_Ano, T000X21_A116Contrato_ValorUnidadeContratacao, T000X21_A41Contratada_PessoaNom,
               T000X21_n41Contratada_PessoaNom, T000X21_A42Contratada_PessoaCNPJ, T000X21_n42Contratada_PessoaCNPJ, T000X21_A516Contratada_TipoFabrica, T000X21_A1858ContratoServicos_Alias, T000X21_n1858ContratoServicos_Alias, T000X21_A608Servico_Nome, T000X21_A605Servico_Sigla, T000X21_A1061Servico_Tela, T000X21_n1061Servico_Tela,
               T000X21_A632Servico_Ativo, T000X21_A156Servico_Descricao, T000X21_n156Servico_Descricao, T000X21_A158ServicoGrupo_Descricao, T000X21_A2092Servico_IsOrigemReferencia, T000X21_A2132ContratoServicos_UndCntNome, T000X21_n2132ContratoServicos_UndCntNome, T000X21_A1712ContratoServicos_UndCntSgl, T000X21_n1712ContratoServicos_UndCntSgl, T000X21_A555Servico_QtdContratada,
               T000X21_n555Servico_QtdContratada, T000X21_A557Servico_VlrUnidadeContratada, T000X21_A558Servico_Percentual, T000X21_n558Servico_Percentual, T000X21_A607ServicoContrato_Faturamento, T000X21_A639ContratoServicos_LocalExec, T000X21_A868ContratoServicos_TipoVnc, T000X21_n868ContratoServicos_TipoVnc, T000X21_A888ContratoServicos_HmlSemCnf, T000X21_n888ContratoServicos_HmlSemCnf,
               T000X21_A1454ContratoServicos_PrazoTpDias, T000X21_n1454ContratoServicos_PrazoTpDias, T000X21_A1152ContratoServicos_PrazoAnalise, T000X21_n1152ContratoServicos_PrazoAnalise, T000X21_A1153ContratoServicos_PrazoResposta, T000X21_n1153ContratoServicos_PrazoResposta, T000X21_A1181ContratoServicos_PrazoGarantia, T000X21_n1181ContratoServicos_PrazoGarantia, T000X21_A1182ContratoServicos_PrazoAtendeGarantia, T000X21_n1182ContratoServicos_PrazoAtendeGarantia,
               T000X21_A1224ContratoServicos_PrazoCorrecao, T000X21_n1224ContratoServicos_PrazoCorrecao, T000X21_A1649ContratoServicos_PrazoInicio, T000X21_n1649ContratoServicos_PrazoInicio, T000X21_A1190ContratoServicos_PrazoImediato, T000X21_n1190ContratoServicos_PrazoImediato, T000X21_A1191ContratoServicos_Produtividade, T000X21_n1191ContratoServicos_Produtividade, T000X21_A1217ContratoServicos_EspelhaAceite, T000X21_n1217ContratoServicos_EspelhaAceite,
               T000X21_A1266ContratoServicos_Momento, T000X21_n1266ContratoServicos_Momento, T000X21_A1325ContratoServicos_StatusPagFnc, T000X21_n1325ContratoServicos_StatusPagFnc, T000X21_A1340ContratoServicos_QntUntCns, T000X21_n1340ContratoServicos_QntUntCns, T000X21_A1341ContratoServicos_FatorCnvUndCnt, T000X21_n1341ContratoServicos_FatorCnvUndCnt, T000X21_A1397ContratoServicos_NaoRequerAtr, T000X21_n1397ContratoServicos_NaoRequerAtr,
               T000X21_A453Contrato_IndiceDivergencia, T000X21_A1455ContratoServicos_IndiceDivergencia, T000X21_n1455ContratoServicos_IndiceDivergencia, T000X21_A1516ContratoServicos_TmpEstAnl, T000X21_n1516ContratoServicos_TmpEstAnl, T000X21_A1501ContratoServicos_TmpEstExc, T000X21_n1501ContratoServicos_TmpEstExc, T000X21_A1502ContratoServicos_TmpEstCrr, T000X21_n1502ContratoServicos_TmpEstCrr, T000X21_A1531ContratoServicos_TipoHierarquia,
               T000X21_n1531ContratoServicos_TipoHierarquia, T000X21_A1537ContratoServicos_PercTmp, T000X21_n1537ContratoServicos_PercTmp, T000X21_A1538ContratoServicos_PercPgm, T000X21_n1538ContratoServicos_PercPgm, T000X21_A1539ContratoServicos_PercCnc, T000X21_n1539ContratoServicos_PercCnc, T000X21_A638ContratoServicos_Ativo, T000X21_A1723ContratoServicos_CodigoFiscal, T000X21_n1723ContratoServicos_CodigoFiscal,
               T000X21_A1799ContratoServicos_LimiteProposta, T000X21_n1799ContratoServicos_LimiteProposta, T000X21_A2074ContratoServicos_CalculoRmn, T000X21_n2074ContratoServicos_CalculoRmn, T000X21_A2094ContratoServicos_SolicitaGestorSistema, T000X21_A155Servico_Codigo, T000X21_A74Contrato_Codigo, T000X21_A1212ContratoServicos_UnidadeContratada, T000X21_A631Servico_Vinculado, T000X21_n631Servico_Vinculado,
               T000X21_A157ServicoGrupo_Codigo, T000X21_A39Contratada_Codigo, T000X21_A40Contratada_PessoaCod, T000X21_A75Contrato_AreaTrabalhoCod, T000X21_A913ContratoServicos_PrazoTipo, T000X21_n913ContratoServicos_PrazoTipo, T000X21_A914ContratoServicos_PrazoDias, T000X21_n914ContratoServicos_PrazoDias, T000X21_A1377ContratoServicos_Indicadores, T000X21_n1377ContratoServicos_Indicadores,
               T000X21_A2073ContratoServicos_QtdRmn, T000X21_n2073ContratoServicos_QtdRmn
               }
               , new Object[] {
               T000X22_A608Servico_Nome, T000X22_A605Servico_Sigla, T000X22_A1061Servico_Tela, T000X22_n1061Servico_Tela, T000X22_A632Servico_Ativo, T000X22_A156Servico_Descricao, T000X22_n156Servico_Descricao, T000X22_A2092Servico_IsOrigemReferencia, T000X22_A631Servico_Vinculado, T000X22_n631Servico_Vinculado,
               T000X22_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000X23_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000X24_A2132ContratoServicos_UndCntNome, T000X24_n2132ContratoServicos_UndCntNome, T000X24_A1712ContratoServicos_UndCntSgl, T000X24_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T000X25_A77Contrato_Numero, T000X25_A78Contrato_NumeroAta, T000X25_n78Contrato_NumeroAta, T000X25_A79Contrato_Ano, T000X25_A116Contrato_ValorUnidadeContratacao, T000X25_A453Contrato_IndiceDivergencia, T000X25_A39Contratada_Codigo, T000X25_A75Contrato_AreaTrabalhoCod
               }
               , new Object[] {
               T000X26_A516Contratada_TipoFabrica, T000X26_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000X27_A41Contratada_PessoaNom, T000X27_n41Contratada_PessoaNom, T000X27_A42Contratada_PessoaCNPJ, T000X27_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000X28_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T000X29_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T000X30_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T000X31_A160ContratoServicos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000X34_A608Servico_Nome, T000X34_A605Servico_Sigla, T000X34_A1061Servico_Tela, T000X34_n1061Servico_Tela, T000X34_A632Servico_Ativo, T000X34_A156Servico_Descricao, T000X34_n156Servico_Descricao, T000X34_A2092Servico_IsOrigemReferencia, T000X34_A631Servico_Vinculado, T000X34_n631Servico_Vinculado,
               T000X34_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000X35_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000X36_A2132ContratoServicos_UndCntNome, T000X36_n2132ContratoServicos_UndCntNome, T000X36_A1712ContratoServicos_UndCntSgl, T000X36_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T000X37_A77Contrato_Numero, T000X37_A78Contrato_NumeroAta, T000X37_n78Contrato_NumeroAta, T000X37_A79Contrato_Ano, T000X37_A116Contrato_ValorUnidadeContratacao, T000X37_A453Contrato_IndiceDivergencia, T000X37_A39Contratada_Codigo, T000X37_A75Contrato_AreaTrabalhoCod
               }
               , new Object[] {
               T000X38_A516Contratada_TipoFabrica, T000X38_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000X39_A41Contratada_PessoaNom, T000X39_n41Contratada_PessoaNom, T000X39_A42Contratada_PessoaCNPJ, T000X39_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000X40_A192Contagem_Codigo
               }
               , new Object[] {
               T000X41_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000X42_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               T000X43_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               T000X44_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               T000X45_A1183AgendaAtendimento_CntSrcCod, T000X45_A1184AgendaAtendimento_Data, T000X45_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               T000X46_A926ContratoServicosTelas_ContratoCod, T000X46_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               T000X47_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               T000X48_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               T000X49_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               T000X50_A160ContratoServicos_Codigo, T000X50_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T000X51_A160ContratoServicos_Codigo, T000X51_A1749Artefatos_Codigo
               }
               , new Object[] {
               T000X52_A160ContratoServicos_Codigo, T000X52_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               T000X53_A160ContratoServicos_Codigo, T000X53_A1067ContratoServicosSistemas_ServicoCod, T000X53_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T000X54_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T000X55_A160ContratoServicos_Codigo, T000X55_A2069ContratoServicosRmn_Sequencial, T000X55_A2070ContratoServicosRmn_Inicio, T000X55_A2071ContratoServicosRmn_Fim, T000X55_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               T000X56_A160ContratoServicos_Codigo, T000X56_A2069ContratoServicosRmn_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000X60_A160ContratoServicos_Codigo, T000X60_A2069ContratoServicosRmn_Sequencial
               }
               , new Object[] {
               T000X61_A157ServicoGrupo_Codigo, T000X61_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000X62_A155Servico_Codigo, T000X62_A605Servico_Sigla, T000X62_A157ServicoGrupo_Codigo, T000X62_A1530Servico_TipoHierarquia, T000X62_n1530Servico_TipoHierarquia, T000X62_A632Servico_Ativo
               }
               , new Object[] {
               T000X63_A155Servico_Codigo, T000X63_A608Servico_Nome, T000X63_A631Servico_Vinculado, T000X63_n631Servico_Vinculado, T000X63_A632Servico_Ativo
               }
               , new Object[] {
               T000X64_A1204ContratoUnidades_UndMedCod, T000X64_A1205ContratoUnidades_UndMedNom, T000X64_n1205ContratoUnidades_UndMedNom, T000X64_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               T000X65_A2132ContratoServicos_UndCntNome, T000X65_n2132ContratoServicos_UndCntNome, T000X65_A1712ContratoServicos_UndCntSgl, T000X65_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T000X66_A608Servico_Nome, T000X66_A605Servico_Sigla, T000X66_A1061Servico_Tela, T000X66_n1061Servico_Tela, T000X66_A632Servico_Ativo, T000X66_A156Servico_Descricao, T000X66_n156Servico_Descricao, T000X66_A2092Servico_IsOrigemReferencia, T000X66_A631Servico_Vinculado, T000X66_n631Servico_Vinculado,
               T000X66_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               T000X67_A158ServicoGrupo_Descricao
               }
            }
         );
         Z2094ContratoServicos_SolicitaGestorSistema = false;
         A2094ContratoServicos_SolicitaGestorSistema = false;
         i2094ContratoServicos_SolicitaGestorSistema = false;
         Z638ContratoServicos_Ativo = true;
         O638ContratoServicos_Ativo = true;
         A638ContratoServicos_Ativo = true;
         i638ContratoServicos_Ativo = true;
         Z1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         A1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         i1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         Z607ServicoContrato_Faturamento = "B";
         A607ServicoContrato_Faturamento = "B";
         i607ServicoContrato_Faturamento = "B";
         AV36Pgmname = "ContratoServicos";
         Z557Servico_VlrUnidadeContratada = 0;
         O557Servico_VlrUnidadeContratada = 0;
         A557Servico_VlrUnidadeContratada = 0;
         Z558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         A558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         i558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         Z1455ContratoServicos_IndiceDivergencia = 0;
         n1455ContratoServicos_IndiceDivergencia = false;
         A1455ContratoServicos_IndiceDivergencia = 0;
         n1455ContratoServicos_IndiceDivergencia = false;
         Z1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         A1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         i1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
      }

      private short Z1152ContratoServicos_PrazoAnalise ;
      private short Z1153ContratoServicos_PrazoResposta ;
      private short Z1181ContratoServicos_PrazoGarantia ;
      private short Z1182ContratoServicos_PrazoAtendeGarantia ;
      private short Z1224ContratoServicos_PrazoCorrecao ;
      private short Z1649ContratoServicos_PrazoInicio ;
      private short Z1531ContratoServicos_TipoHierarquia ;
      private short Z1537ContratoServicos_PercTmp ;
      private short Z1538ContratoServicos_PercPgm ;
      private short Z1539ContratoServicos_PercCnc ;
      private short Z2074ContratoServicos_CalculoRmn ;
      private short O2073ContratoServicos_QtdRmn ;
      private short nRC_GXsfl_312 ;
      private short nGXsfl_312_idx=1 ;
      private short Z2069ContratoServicosRmn_Sequencial ;
      private short nRcdDeleted_228 ;
      private short nRcdExists_228 ;
      private short nIsMod_228 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A2074ContratoServicos_CalculoRmn ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGrid_Backcolorstyle ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nBlankRcdCount228 ;
      private short RcdFound228 ;
      private short B2073ContratoServicos_QtdRmn ;
      private short A2073ContratoServicos_QtdRmn ;
      private short nBlankRcdUsr228 ;
      private short A1539ContratoServicos_PercCnc ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1153ContratoServicos_PrazoResposta ;
      private short A1181ContratoServicos_PrazoGarantia ;
      private short A1182ContratoServicos_PrazoAtendeGarantia ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A914ContratoServicos_PrazoDias ;
      private short A79Contrato_Ano ;
      private short A1531ContratoServicos_TipoHierarquia ;
      private short A1537ContratoServicos_PercTmp ;
      private short A1538ContratoServicos_PercPgm ;
      private short A640Servico_Vinculados ;
      private short Gx_BScreen ;
      private short A911ContratoServicos_Prazos ;
      private short A1377ContratoServicos_Indicadores ;
      private short RcdFound34 ;
      private short s2073ContratoServicos_QtdRmn ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short Z914ContratoServicos_PrazoDias ;
      private short Z1377ContratoServicos_Indicadores ;
      private short Z2073ContratoServicos_QtdRmn ;
      private short subGrid_Backstyle ;
      private short gxajaxcallmode ;
      private short i1649ContratoServicos_PrazoInicio ;
      private short i2073ContratoServicos_QtdRmn ;
      private short wbTemp ;
      private short GXt_int3 ;
      private int wcpOAV7ContratoServicos_Codigo ;
      private int wcpOAV27Contrato_Codigo ;
      private int Z160ContratoServicos_Codigo ;
      private int Z1516ContratoServicos_TmpEstAnl ;
      private int Z1501ContratoServicos_TmpEstExc ;
      private int Z1502ContratoServicos_TmpEstCrr ;
      private int Z155Servico_Codigo ;
      private int Z74Contrato_Codigo ;
      private int Z1212ContratoServicos_UnidadeContratada ;
      private int N74Contrato_Codigo ;
      private int N155Servico_Codigo ;
      private int N1212ContratoServicos_UnidadeContratada ;
      private int AV14ServicoGrupo_Codigo ;
      private int AV17Servico_Codigo ;
      private int AV27Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV7ContratoServicos_Codigo ;
      private int trnEnded ;
      private int AV15SubServico_Codigo ;
      private int edtServico_Codigo_Visible ;
      private int edtServico_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtContratoServicosRmn_Sequencial_Enabled ;
      private int edtContratoServicosRmn_Inicio_Enabled ;
      private int edtContratoServicosRmn_Fim_Enabled ;
      private int edtContratoServicosRmn_Valor_Enabled ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int fRowAdded ;
      private int edtContratoServicos_FatorCnvUndCnt_Enabled ;
      private int edtServico_QtdContratada_Enabled ;
      private int edtServico_VlrUnidadeContratada_Enabled ;
      private int lblTextblockcontratoservicos_espelhaaceite_Visible ;
      private int edtContratoServicos_QntUntCns_Enabled ;
      private int edtContratoServicos_CodigoFiscal_Enabled ;
      private int lblTextblockcontratoservicos_ativo_Visible ;
      private int imgContratoservicos_solicitagestorsistema_infoicon_Visible ;
      private int edtContratoServicos_PercCnc_Enabled ;
      private int edtavServico_percentual_Enabled ;
      private int edtContratoServicos_Produtividade_Enabled ;
      private int edtContratoServicos_LimiteProposta_Enabled ;
      private int edtContratoServicos_IndiceDivergencia_Enabled ;
      private int edtContratoServicos_PrazoAnalise_Enabled ;
      private int edtContratoServicos_PrazoResposta_Enabled ;
      private int edtContratoServicos_PrazoGarantia_Enabled ;
      private int edtContratoServicos_PrazoAtendeGarantia_Enabled ;
      private int A1502ContratoServicos_TmpEstCrr ;
      private int edtContratoServicos_TmpEstCrr_Enabled ;
      private int bttBtnreplicar_Visible ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int edtContratoServicos_TmpEstExc_Enabled ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int edtContratoServicos_TmpEstAnl_Enabled ;
      private int edtContratoServicos_PrazoCorrecao_Enabled ;
      private int edtContratoServicos_PrazoCorrecao_Visible ;
      private int edtContratoServicos_PrazoDias_Enabled ;
      private int edtContratoServicos_PrazoDias_Visible ;
      private int imgUpdprazo_Visible ;
      private int imgUpdprazo_Enabled ;
      private int lblTextblocksubservico_codigo_Visible ;
      private int edtavServico_nome_Enabled ;
      private int edtavAlias_Enabled ;
      private int lblTextblockcontrato_valorunidadecontratacao_Visible ;
      private int edtContrato_ValorUnidadeContratacao_Enabled ;
      private int edtContrato_ValorUnidadeContratacao_Visible ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int A1551Servico_Responsavel ;
      private int A827ContratoServicos_ServicoCod ;
      private int A160ContratoServicos_Codigo ;
      private int AV11Insert_Contrato_Codigo ;
      private int AV12Insert_Servico_Codigo ;
      private int AV24Insert_ContratoServicos_UnidadeContratada ;
      private int A631Servico_Vinculado ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int Gxuitabspanel_tab_Selectedtabindex ;
      private int AV37GXV1 ;
      private int Z39Contratada_Codigo ;
      private int Z75Contrato_AreaTrabalhoCod ;
      private int Z40Contratada_PessoaCod ;
      private int Z631Servico_Vinculado ;
      private int Z157ServicoGrupo_Codigo ;
      private int subGrid_Backcolor ;
      private int subGrid_Allbackcolor ;
      private int defedtContratoServicosRmn_Sequencial_Enabled ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int2 ;
      private long Z555Servico_QtdContratada ;
      private long A555Servico_QtdContratada ;
      private long GRID_nFirstRecordOnPage ;
      private decimal Z557Servico_VlrUnidadeContratada ;
      private decimal Z558Servico_Percentual ;
      private decimal Z1191ContratoServicos_Produtividade ;
      private decimal Z1340ContratoServicos_QntUntCns ;
      private decimal Z1341ContratoServicos_FatorCnvUndCnt ;
      private decimal Z1455ContratoServicos_IndiceDivergencia ;
      private decimal Z1799ContratoServicos_LimiteProposta ;
      private decimal O557Servico_VlrUnidadeContratada ;
      private decimal Z2070ContratoServicosRmn_Inicio ;
      private decimal Z2071ContratoServicosRmn_Fim ;
      private decimal Z2072ContratoServicosRmn_Valor ;
      private decimal A2070ContratoServicosRmn_Inicio ;
      private decimal A2071ContratoServicosRmn_Fim ;
      private decimal A2072ContratoServicosRmn_Valor ;
      private decimal B557Servico_VlrUnidadeContratada ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A1341ContratoServicos_FatorCnvUndCnt ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal AV21Servico_Percentual ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A558Servico_Percentual ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal Z116Contrato_ValorUnidadeContratacao ;
      private decimal Z453Contrato_IndiceDivergencia ;
      private decimal i558Servico_Percentual ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1225ContratoServicos_PrazoCorrecaoTipo ;
      private String Z1858ContratoServicos_Alias ;
      private String Z639ContratoServicos_LocalExec ;
      private String Z868ContratoServicos_TipoVnc ;
      private String Z1454ContratoServicos_PrazoTpDias ;
      private String Z1266ContratoServicos_Momento ;
      private String Z1325ContratoServicos_StatusPagFnc ;
      private String Z1723ContratoServicos_CodigoFiscal ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_312_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String A913ContratoServicos_PrazoTipo ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String chkContratoServicos_NaoRequerAtr_Internalname ;
      private String A639ContratoServicos_LocalExec ;
      private String chkContratoServicos_EspelhaAceite_Internalname ;
      private String A1266ContratoServicos_Momento ;
      private String A868ContratoServicos_TipoVnc ;
      private String chkContratoServicos_SolicitaGestorSistema_Internalname ;
      private String chkContratoServicos_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String cmbContratoServicos_PrazoTpDias_Internalname ;
      private String TempTags ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String sMode228 ;
      private String edtContratoServicosRmn_Sequencial_Internalname ;
      private String edtContratoServicosRmn_Inicio_Internalname ;
      private String edtContratoServicosRmn_Fim_Internalname ;
      private String edtContratoServicosRmn_Valor_Internalname ;
      private String tblTableprincipal_Internalname ;
      private String grpUnnamedgroup2_Internalname ;
      private String grpUnnamedgroup3_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String grpUnnamedgroup5_Internalname ;
      private String tblParametros_Internalname ;
      private String lblTextblockcontratoservicos_indicedivergencia_Internalname ;
      private String lblTextblockcontratoservicos_indicedivergencia_Jsonclick ;
      private String lblTextblockcontratoservicos_naorequeratr_Internalname ;
      private String lblTextblockcontratoservicos_naorequeratr_Jsonclick ;
      private String lblTextblockcontratoservicos_limiteproposta_Internalname ;
      private String lblTextblockcontratoservicos_limiteproposta_Jsonclick ;
      private String lblTextblockcontratoservicos_localexec_Internalname ;
      private String lblTextblockcontratoservicos_localexec_Jsonclick ;
      private String cmbContratoServicos_LocalExec_Internalname ;
      private String cmbContratoServicos_LocalExec_Jsonclick ;
      private String lblTextblockcontratoservicos_unidadecontratada_Internalname ;
      private String lblTextblockcontratoservicos_unidadecontratada_Jsonclick ;
      private String dynContratoServicos_UnidadeContratada_Internalname ;
      private String dynContratoServicos_UnidadeContratada_Jsonclick ;
      private String lblTextblockcontratoservicos_fatorcnvundcnt_Internalname ;
      private String lblTextblockcontratoservicos_fatorcnvundcnt_Jsonclick ;
      private String edtContratoServicos_FatorCnvUndCnt_Internalname ;
      private String edtContratoServicos_FatorCnvUndCnt_Jsonclick ;
      private String lblTextblockservico_qtdcontratada_Internalname ;
      private String lblTextblockservico_qtdcontratada_Jsonclick ;
      private String edtServico_QtdContratada_Internalname ;
      private String edtServico_QtdContratada_Jsonclick ;
      private String lblTextblockservico_vlrunidadecontratada_Internalname ;
      private String lblTextblockservico_vlrunidadecontratada_Jsonclick ;
      private String edtServico_VlrUnidadeContratada_Internalname ;
      private String edtServico_VlrUnidadeContratada_Jsonclick ;
      private String lblTextblockcontratoservicos_produtividade_Internalname ;
      private String lblTextblockcontratoservicos_produtividade_Jsonclick ;
      private String lblTextblockservicocontrato_faturamento_Internalname ;
      private String lblTextblockservicocontrato_faturamento_Jsonclick ;
      private String cmbServicoContrato_Faturamento_Internalname ;
      private String cmbServicoContrato_Faturamento_Jsonclick ;
      private String lblTextblockservico_percentual_Internalname ;
      private String lblTextblockservico_percentual_Jsonclick ;
      private String lblTextblockcontratoservicos_calculormn_Internalname ;
      private String lblTextblockcontratoservicos_calculormn_Jsonclick ;
      private String cmbContratoServicos_CalculoRmn_Internalname ;
      private String cmbContratoServicos_CalculoRmn_Jsonclick ;
      private String lblTextblockcontratoservicos_perccnc_Internalname ;
      private String lblTextblockcontratoservicos_perccnc_Jsonclick ;
      private String lblTextblockcontratoservicos_espelhaaceite_Internalname ;
      private String lblTextblockcontratoservicos_espelhaaceite_Jsonclick ;
      private String lblTextblockcontratoservicos_momento_Internalname ;
      private String lblTextblockcontratoservicos_momento_Jsonclick ;
      private String cmbContratoServicos_Momento_Internalname ;
      private String cmbContratoServicos_Momento_Jsonclick ;
      private String lblTextblockcontratoservicos_tipovnc_Internalname ;
      private String lblTextblockcontratoservicos_tipovnc_Jsonclick ;
      private String cmbContratoServicos_TipoVnc_Internalname ;
      private String cmbContratoServicos_TipoVnc_Jsonclick ;
      private String lblTextblockcontratoservicos_qntuntcns_Internalname ;
      private String lblTextblockcontratoservicos_qntuntcns_Jsonclick ;
      private String edtContratoServicos_QntUntCns_Internalname ;
      private String edtContratoServicos_QntUntCns_Jsonclick ;
      private String lblTextblockcontratoservicos_codigofiscal_Internalname ;
      private String lblTextblockcontratoservicos_codigofiscal_Jsonclick ;
      private String edtContratoServicos_CodigoFiscal_Internalname ;
      private String A1723ContratoServicos_CodigoFiscal ;
      private String edtContratoServicos_CodigoFiscal_Jsonclick ;
      private String lblTextblockcontratoservicos_solicitagestorsistema_Internalname ;
      private String lblTextblockcontratoservicos_solicitagestorsistema_Jsonclick ;
      private String lblTextblockcontratoservicos_ativo_Internalname ;
      private String lblTextblockcontratoservicos_ativo_Jsonclick ;
      private String tblTablemergedcontratoservicos_solicitagestorsistema_Internalname ;
      private String imgContratoservicos_solicitagestorsistema_infoicon_Internalname ;
      private String tblTablemergedcontratoservicos_perccnc_Internalname ;
      private String edtContratoServicos_PercCnc_Internalname ;
      private String edtContratoServicos_PercCnc_Jsonclick ;
      private String lblContratoservicos_perccnc_righttext_Internalname ;
      private String lblContratoservicos_perccnc_righttext_Jsonclick ;
      private String tblTablemergedservico_percentual_Internalname ;
      private String edtavServico_percentual_Internalname ;
      private String edtavServico_percentual_Jsonclick ;
      private String lblServico_percentual_righttext_Internalname ;
      private String lblServico_percentual_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_produtividade_Internalname ;
      private String edtContratoServicos_Produtividade_Internalname ;
      private String edtContratoServicos_Produtividade_Jsonclick ;
      private String lblContratoservicos_produtividade_righttext_Internalname ;
      private String lblContratoservicos_produtividade_righttext_Caption ;
      private String lblContratoservicos_produtividade_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_limiteproposta_Internalname ;
      private String edtContratoServicos_LimiteProposta_Internalname ;
      private String edtContratoServicos_LimiteProposta_Jsonclick ;
      private String lblContratoservicos_limiteproposta_righttext_Internalname ;
      private String lblContratoservicos_limiteproposta_righttext_Caption ;
      private String lblContratoservicos_limiteproposta_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_indicedivergencia_Internalname ;
      private String edtContratoServicos_IndiceDivergencia_Internalname ;
      private String edtContratoServicos_IndiceDivergencia_Jsonclick ;
      private String lblContratoservicos_indicedivergencia_righttext_Internalname ;
      private String lblContratoservicos_indicedivergencia_righttext_Jsonclick ;
      private String tblPrazos_Internalname ;
      private String lblTextblockcontratoservicos_prazotpdias_Internalname ;
      private String lblTextblockcontratoservicos_prazotpdias_Jsonclick ;
      private String cmbContratoServicos_PrazoTpDias_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoinicio_Internalname ;
      private String lblTextblockcontratoservicos_prazoinicio_Jsonclick ;
      private String cmbContratoServicos_PrazoInicio_Internalname ;
      private String cmbContratoServicos_PrazoInicio_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoanalise_Internalname ;
      private String lblTextblockcontratoservicos_prazoanalise_Jsonclick ;
      private String edtContratoServicos_PrazoAnalise_Internalname ;
      private String edtContratoServicos_PrazoAnalise_Jsonclick ;
      private String lblTextblockcontratoservicos_prazotipo_Internalname ;
      private String lblTextblockcontratoservicos_prazotipo_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoresposta_Internalname ;
      private String lblTextblockcontratoservicos_prazoresposta_Jsonclick ;
      private String edtContratoServicos_PrazoResposta_Internalname ;
      private String edtContratoServicos_PrazoResposta_Jsonclick ;
      private String lblTextblockcontratoservicos_prazogarantia_Internalname ;
      private String lblTextblockcontratoservicos_prazogarantia_Jsonclick ;
      private String edtContratoServicos_PrazoGarantia_Internalname ;
      private String edtContratoServicos_PrazoGarantia_Jsonclick ;
      private String lblTextblockcontratoservicos_prazoatendegarantia_Internalname ;
      private String lblTextblockcontratoservicos_prazoatendegarantia_Jsonclick ;
      private String edtContratoServicos_PrazoAtendeGarantia_Internalname ;
      private String edtContratoServicos_PrazoAtendeGarantia_Jsonclick ;
      private String lblTextblockcontratoservicos_prazocorrecaotipo_Internalname ;
      private String lblTextblockcontratoservicos_prazocorrecaotipo_Jsonclick ;
      private String lblTextblockcontratoservicos_tmpestanl_Internalname ;
      private String lblTextblockcontratoservicos_tmpestanl_Jsonclick ;
      private String lblTextblockcontratoservicos_tmpestexc_Internalname ;
      private String lblTextblockcontratoservicos_tmpestexc_Jsonclick ;
      private String lblTextblockcontratoservicos_tmpestcrr_Internalname ;
      private String lblTextblockcontratoservicos_tmpestcrr_Jsonclick ;
      private String lblTextblockcontratoservicos_statuspagfnc_Internalname ;
      private String lblTextblockcontratoservicos_statuspagfnc_Jsonclick ;
      private String cmbContratoServicos_StatusPagFnc_Internalname ;
      private String cmbContratoServicos_StatusPagFnc_Jsonclick ;
      private String tblTablemergedcontratoservicos_tmpestcrr_Internalname ;
      private String edtContratoServicos_TmpEstCrr_Internalname ;
      private String edtContratoServicos_TmpEstCrr_Jsonclick ;
      private String cellContratoservicos_tmpestcrr_righttext_cell_Internalname ;
      private String lblContratoservicos_tmpestcrr_righttext_Internalname ;
      private String lblContratoservicos_tmpestcrr_righttext_Jsonclick ;
      private String bttBtnreplicar_Internalname ;
      private String bttBtnreplicar_Jsonclick ;
      private String tblTablemergedcontratoservicos_tmpestexc_Internalname ;
      private String edtContratoServicos_TmpEstExc_Internalname ;
      private String edtContratoServicos_TmpEstExc_Jsonclick ;
      private String lblContratoservicos_tmpestexc_righttext_Internalname ;
      private String lblContratoservicos_tmpestexc_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_tmpestanl_Internalname ;
      private String edtContratoServicos_TmpEstAnl_Internalname ;
      private String edtContratoServicos_TmpEstAnl_Jsonclick ;
      private String lblContratoservicos_tmpestanl_righttext_Internalname ;
      private String lblContratoservicos_tmpestanl_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_prazocorrecaotipo_Internalname ;
      private String cmbContratoServicos_PrazoCorrecaoTipo_Internalname ;
      private String cmbContratoServicos_PrazoCorrecaoTipo_Jsonclick ;
      private String lblTextblockcontratoservicos_prazocorrecao_Internalname ;
      private String lblTextblockcontratoservicos_prazocorrecao_Jsonclick ;
      private String edtContratoServicos_PrazoCorrecao_Internalname ;
      private String edtContratoServicos_PrazoCorrecao_Jsonclick ;
      private String cellContratoservicos_prazocorrecao_righttext_cell_Internalname ;
      private String lblContratoservicos_prazocorrecao_righttext_Internalname ;
      private String lblContratoservicos_prazocorrecao_righttext_Caption ;
      private String lblContratoservicos_prazocorrecao_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicos_prazotipo_Internalname ;
      private String cmbContratoServicos_PrazoTipo_Internalname ;
      private String cmbContratoServicos_PrazoTipo_Jsonclick ;
      private String cellContratoservicos_prazodias_cell_Internalname ;
      private String cellContratoservicos_prazodias_cell_Class ;
      private String edtContratoServicos_PrazoDias_Internalname ;
      private String edtContratoServicos_PrazoDias_Jsonclick ;
      private String cellContratoservicos_prazodias_righttext_cell_Internalname ;
      private String cellContratoservicos_prazodias_righttext_cell_Class ;
      private String lblContratoservicos_prazodias_righttext_Internalname ;
      private String lblContratoservicos_prazodias_righttext_Jsonclick ;
      private String tblTblbtnprzexc_Internalname ;
      private String imgUpdprazo_Internalname ;
      private String imgUpdprazo_Tooltiptext ;
      private String imgUpdprazo_Jsonclick ;
      private String tblGruposervico_Internalname ;
      private String lblTextblockservicogrupo_codigo_Internalname ;
      private String lblTextblockservicogrupo_codigo_Jsonclick ;
      private String dynavServicogrupo_codigo_Internalname ;
      private String dynavServicogrupo_codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Caption ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String dynavServico_codigo_Internalname ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblocksubservico_codigo_Internalname ;
      private String lblTextblocksubservico_codigo_Caption ;
      private String lblTextblocksubservico_codigo_Jsonclick ;
      private String dynavSubservico_codigo_Internalname ;
      private String dynavSubservico_codigo_Jsonclick ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String edtavServico_nome_Internalname ;
      private String AV32Servico_Nome ;
      private String edtavServico_nome_Jsonclick ;
      private String lblTextblockalias_Internalname ;
      private String lblTextblockalias_Jsonclick ;
      private String edtavAlias_Internalname ;
      private String AV30Alias ;
      private String edtavAlias_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String lblTextblockcontrato_valorunidadecontratacao_Internalname ;
      private String lblTextblockcontrato_valorunidadecontratacao_Jsonclick ;
      private String edtContrato_ValorUnidadeContratacao_Internalname ;
      private String edtContrato_ValorUnidadeContratacao_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String A1858ContratoServicos_Alias ;
      private String A605Servico_Sigla ;
      private String A608Servico_Nome ;
      private String A826ContratoServicos_ServicoSigla ;
      private String A516Contratada_TipoFabrica ;
      private String A1712ContratoServicos_UndCntSgl ;
      private String A1061Servico_Tela ;
      private String A2132ContratoServicos_UndCntNome ;
      private String AV36Pgmname ;
      private String Gxuitabspanel_tab_Width ;
      private String Gxuitabspanel_tab_Height ;
      private String Gxuitabspanel_tab_Cls ;
      private String Gxuitabspanel_tab_Class ;
      private String Gxuitabspanel_tab_Activetabid ;
      private String Gxuitabspanel_tab_Designtimetabs ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode34 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Gxuitabspanel_tab_Internalname ;
      private String GXt_char1 ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z516Contratada_TipoFabrica ;
      private String Z41Contratada_PessoaNom ;
      private String Z913ContratoServicos_PrazoTipo ;
      private String Z608Servico_Nome ;
      private String Z605Servico_Sigla ;
      private String Z1061Servico_Tela ;
      private String Z2132ContratoServicos_UndCntNome ;
      private String Z1712ContratoServicos_UndCntSgl ;
      private String sGXsfl_312_fel_idx="0001" ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String ROClassString ;
      private String edtContratoServicosRmn_Sequencial_Jsonclick ;
      private String edtContratoServicosRmn_Inicio_Jsonclick ;
      private String edtContratoServicosRmn_Fim_Jsonclick ;
      private String edtContratoServicosRmn_Valor_Jsonclick ;
      private String GXCCtl ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i1454ContratoServicos_PrazoTpDias ;
      private String subGrid_Internalname ;
      private String gxwrpcisep ;
      private bool Z888ContratoServicos_HmlSemCnf ;
      private bool Z1190ContratoServicos_PrazoImediato ;
      private bool Z1217ContratoServicos_EspelhaAceite ;
      private bool Z1397ContratoServicos_NaoRequerAtr ;
      private bool Z638ContratoServicos_Ativo ;
      private bool Z2094ContratoServicos_SolicitaGestorSistema ;
      private bool O638ContratoServicos_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n1266ContratoServicos_Momento ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool wbErr ;
      private bool n2073ContratoServicos_QtdRmn ;
      private bool B638ContratoServicos_Ativo ;
      private bool A638ContratoServicos_Ativo ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool A1217ContratoServicos_EspelhaAceite ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n914ContratoServicos_PrazoDias ;
      private bool n1153ContratoServicos_PrazoResposta ;
      private bool n1181ContratoServicos_PrazoGarantia ;
      private bool n1182ContratoServicos_PrazoAtendeGarantia ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1502ContratoServicos_TmpEstCrr ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private bool n1341ContratoServicos_FatorCnvUndCnt ;
      private bool n555Servico_QtdContratada ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool n1217ContratoServicos_EspelhaAceite ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1723ContratoServicos_CodigoFiscal ;
      private bool n1858ContratoServicos_Alias ;
      private bool n558Servico_Percentual ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool n1190ContratoServicos_PrazoImediato ;
      private bool A1190ContratoServicos_PrazoImediato ;
      private bool n1531ContratoServicos_TipoHierarquia ;
      private bool n1537ContratoServicos_PercTmp ;
      private bool n1538ContratoServicos_PercPgm ;
      private bool AV26CemPorCento ;
      private bool n1712ContratoServicos_UndCntSgl ;
      private bool n911ContratoServicos_Prazos ;
      private bool n1061Servico_Tela ;
      private bool A632Servico_Ativo ;
      private bool n156Servico_Descricao ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool n631Servico_Vinculado ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1377ContratoServicos_Indicadores ;
      private bool Gxuitabspanel_tab_Enabled ;
      private bool Gxuitabspanel_tab_Autowidth ;
      private bool Gxuitabspanel_tab_Autoheight ;
      private bool Gxuitabspanel_tab_Autoscroll ;
      private bool Gxuitabspanel_tab_Visible ;
      private bool returnInSub ;
      private bool AV20TemSubServicos ;
      private bool Z632Servico_Ativo ;
      private bool Z2092Servico_IsOrigemReferencia ;
      private bool Gx_longc ;
      private bool i2094ContratoServicos_SolicitaGestorSistema ;
      private bool i638ContratoServicos_Ativo ;
      private String A156Servico_Descricao ;
      private String Z156Servico_Descricao ;
      private String Z607ServicoContrato_Faturamento ;
      private String A607ServicoContrato_Faturamento ;
      private String A42Contratada_PessoaCNPJ ;
      private String A2107Servico_Identificacao ;
      private String A158ServicoGrupo_Descricao ;
      private String Z42Contratada_PessoaCNPJ ;
      private String Z158ServicoGrupo_Descricao ;
      private String i607ServicoContrato_Faturamento ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private GXCombobox dynavServicogrupo_codigo ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox dynavSubservico_codigo ;
      private GXCombobox cmbContratoServicos_PrazoTpDias ;
      private GXCombobox cmbContratoServicos_PrazoInicio ;
      private GXCombobox cmbContratoServicos_PrazoTipo ;
      private GXCombobox cmbContratoServicos_PrazoCorrecaoTipo ;
      private GXCombobox cmbContratoServicos_StatusPagFnc ;
      private GXCheckbox chkContratoServicos_NaoRequerAtr ;
      private GXCombobox cmbContratoServicos_LocalExec ;
      private GXCombobox dynContratoServicos_UnidadeContratada ;
      private GXCombobox cmbServicoContrato_Faturamento ;
      private GXCombobox cmbContratoServicos_CalculoRmn ;
      private GXCheckbox chkContratoServicos_EspelhaAceite ;
      private GXCombobox cmbContratoServicos_Momento ;
      private GXCombobox cmbContratoServicos_TipoVnc ;
      private GXCheckbox chkContratoServicos_SolicitaGestorSistema ;
      private GXCheckbox chkContratoServicos_Ativo ;
      private IDataStoreProvider pr_default ;
      private short[] T000X7_A911ContratoServicos_Prazos ;
      private bool[] T000X7_n911ContratoServicos_Prazos ;
      private String[] T000X14_A913ContratoServicos_PrazoTipo ;
      private bool[] T000X14_n913ContratoServicos_PrazoTipo ;
      private short[] T000X14_A914ContratoServicos_PrazoDias ;
      private bool[] T000X14_n914ContratoServicos_PrazoDias ;
      private short[] T000X16_A1377ContratoServicos_Indicadores ;
      private bool[] T000X16_n1377ContratoServicos_Indicadores ;
      private short[] T000X18_A2073ContratoServicos_QtdRmn ;
      private bool[] T000X18_n2073ContratoServicos_QtdRmn ;
      private String[] T000X10_A2132ContratoServicos_UndCntNome ;
      private bool[] T000X10_n2132ContratoServicos_UndCntNome ;
      private String[] T000X10_A1712ContratoServicos_UndCntSgl ;
      private bool[] T000X10_n1712ContratoServicos_UndCntSgl ;
      private String[] T000X8_A608Servico_Nome ;
      private String[] T000X8_A605Servico_Sigla ;
      private String[] T000X8_A1061Servico_Tela ;
      private bool[] T000X8_n1061Servico_Tela ;
      private bool[] T000X8_A632Servico_Ativo ;
      private String[] T000X8_A156Servico_Descricao ;
      private bool[] T000X8_n156Servico_Descricao ;
      private bool[] T000X8_A2092Servico_IsOrigemReferencia ;
      private int[] T000X8_A631Servico_Vinculado ;
      private bool[] T000X8_n631Servico_Vinculado ;
      private int[] T000X8_A157ServicoGrupo_Codigo ;
      private String[] T000X11_A158ServicoGrupo_Descricao ;
      private String[] T000X9_A77Contrato_Numero ;
      private String[] T000X9_A78Contrato_NumeroAta ;
      private bool[] T000X9_n78Contrato_NumeroAta ;
      private short[] T000X9_A79Contrato_Ano ;
      private decimal[] T000X9_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] T000X9_A453Contrato_IndiceDivergencia ;
      private int[] T000X9_A39Contratada_Codigo ;
      private int[] T000X9_A75Contrato_AreaTrabalhoCod ;
      private String[] T000X12_A516Contratada_TipoFabrica ;
      private int[] T000X12_A40Contratada_PessoaCod ;
      private String[] T000X13_A41Contratada_PessoaNom ;
      private bool[] T000X13_n41Contratada_PessoaNom ;
      private String[] T000X13_A42Contratada_PessoaCNPJ ;
      private bool[] T000X13_n42Contratada_PessoaCNPJ ;
      private int[] T000X21_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T000X21_A160ContratoServicos_Codigo ;
      private String[] T000X21_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] T000X21_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private String[] T000X21_A77Contrato_Numero ;
      private String[] T000X21_A78Contrato_NumeroAta ;
      private bool[] T000X21_n78Contrato_NumeroAta ;
      private short[] T000X21_A79Contrato_Ano ;
      private decimal[] T000X21_A116Contrato_ValorUnidadeContratacao ;
      private String[] T000X21_A41Contratada_PessoaNom ;
      private bool[] T000X21_n41Contratada_PessoaNom ;
      private String[] T000X21_A42Contratada_PessoaCNPJ ;
      private bool[] T000X21_n42Contratada_PessoaCNPJ ;
      private String[] T000X21_A516Contratada_TipoFabrica ;
      private String[] T000X21_A1858ContratoServicos_Alias ;
      private bool[] T000X21_n1858ContratoServicos_Alias ;
      private String[] T000X21_A608Servico_Nome ;
      private String[] T000X21_A605Servico_Sigla ;
      private String[] T000X21_A1061Servico_Tela ;
      private bool[] T000X21_n1061Servico_Tela ;
      private bool[] T000X21_A632Servico_Ativo ;
      private String[] T000X21_A156Servico_Descricao ;
      private bool[] T000X21_n156Servico_Descricao ;
      private String[] T000X21_A158ServicoGrupo_Descricao ;
      private bool[] T000X21_A2092Servico_IsOrigemReferencia ;
      private String[] T000X21_A2132ContratoServicos_UndCntNome ;
      private bool[] T000X21_n2132ContratoServicos_UndCntNome ;
      private String[] T000X21_A1712ContratoServicos_UndCntSgl ;
      private bool[] T000X21_n1712ContratoServicos_UndCntSgl ;
      private long[] T000X21_A555Servico_QtdContratada ;
      private bool[] T000X21_n555Servico_QtdContratada ;
      private decimal[] T000X21_A557Servico_VlrUnidadeContratada ;
      private decimal[] T000X21_A558Servico_Percentual ;
      private bool[] T000X21_n558Servico_Percentual ;
      private String[] T000X21_A607ServicoContrato_Faturamento ;
      private String[] T000X21_A639ContratoServicos_LocalExec ;
      private String[] T000X21_A868ContratoServicos_TipoVnc ;
      private bool[] T000X21_n868ContratoServicos_TipoVnc ;
      private bool[] T000X21_A888ContratoServicos_HmlSemCnf ;
      private bool[] T000X21_n888ContratoServicos_HmlSemCnf ;
      private String[] T000X21_A1454ContratoServicos_PrazoTpDias ;
      private bool[] T000X21_n1454ContratoServicos_PrazoTpDias ;
      private short[] T000X21_A1152ContratoServicos_PrazoAnalise ;
      private bool[] T000X21_n1152ContratoServicos_PrazoAnalise ;
      private short[] T000X21_A1153ContratoServicos_PrazoResposta ;
      private bool[] T000X21_n1153ContratoServicos_PrazoResposta ;
      private short[] T000X21_A1181ContratoServicos_PrazoGarantia ;
      private bool[] T000X21_n1181ContratoServicos_PrazoGarantia ;
      private short[] T000X21_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] T000X21_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] T000X21_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] T000X21_n1224ContratoServicos_PrazoCorrecao ;
      private short[] T000X21_A1649ContratoServicos_PrazoInicio ;
      private bool[] T000X21_n1649ContratoServicos_PrazoInicio ;
      private bool[] T000X21_A1190ContratoServicos_PrazoImediato ;
      private bool[] T000X21_n1190ContratoServicos_PrazoImediato ;
      private decimal[] T000X21_A1191ContratoServicos_Produtividade ;
      private bool[] T000X21_n1191ContratoServicos_Produtividade ;
      private bool[] T000X21_A1217ContratoServicos_EspelhaAceite ;
      private bool[] T000X21_n1217ContratoServicos_EspelhaAceite ;
      private String[] T000X21_A1266ContratoServicos_Momento ;
      private bool[] T000X21_n1266ContratoServicos_Momento ;
      private String[] T000X21_A1325ContratoServicos_StatusPagFnc ;
      private bool[] T000X21_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] T000X21_A1340ContratoServicos_QntUntCns ;
      private bool[] T000X21_n1340ContratoServicos_QntUntCns ;
      private decimal[] T000X21_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] T000X21_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] T000X21_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] T000X21_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] T000X21_A453Contrato_IndiceDivergencia ;
      private decimal[] T000X21_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] T000X21_n1455ContratoServicos_IndiceDivergencia ;
      private int[] T000X21_A1516ContratoServicos_TmpEstAnl ;
      private bool[] T000X21_n1516ContratoServicos_TmpEstAnl ;
      private int[] T000X21_A1501ContratoServicos_TmpEstExc ;
      private bool[] T000X21_n1501ContratoServicos_TmpEstExc ;
      private int[] T000X21_A1502ContratoServicos_TmpEstCrr ;
      private bool[] T000X21_n1502ContratoServicos_TmpEstCrr ;
      private short[] T000X21_A1531ContratoServicos_TipoHierarquia ;
      private bool[] T000X21_n1531ContratoServicos_TipoHierarquia ;
      private short[] T000X21_A1537ContratoServicos_PercTmp ;
      private bool[] T000X21_n1537ContratoServicos_PercTmp ;
      private short[] T000X21_A1538ContratoServicos_PercPgm ;
      private bool[] T000X21_n1538ContratoServicos_PercPgm ;
      private short[] T000X21_A1539ContratoServicos_PercCnc ;
      private bool[] T000X21_n1539ContratoServicos_PercCnc ;
      private bool[] T000X21_A638ContratoServicos_Ativo ;
      private String[] T000X21_A1723ContratoServicos_CodigoFiscal ;
      private bool[] T000X21_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] T000X21_A1799ContratoServicos_LimiteProposta ;
      private bool[] T000X21_n1799ContratoServicos_LimiteProposta ;
      private short[] T000X21_A2074ContratoServicos_CalculoRmn ;
      private bool[] T000X21_n2074ContratoServicos_CalculoRmn ;
      private bool[] T000X21_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] T000X21_A155Servico_Codigo ;
      private int[] T000X21_A74Contrato_Codigo ;
      private int[] T000X21_A1212ContratoServicos_UnidadeContratada ;
      private int[] T000X21_A631Servico_Vinculado ;
      private bool[] T000X21_n631Servico_Vinculado ;
      private int[] T000X21_A157ServicoGrupo_Codigo ;
      private int[] T000X21_A39Contratada_Codigo ;
      private int[] T000X21_A40Contratada_PessoaCod ;
      private int[] T000X21_A75Contrato_AreaTrabalhoCod ;
      private String[] T000X21_A913ContratoServicos_PrazoTipo ;
      private bool[] T000X21_n913ContratoServicos_PrazoTipo ;
      private short[] T000X21_A914ContratoServicos_PrazoDias ;
      private bool[] T000X21_n914ContratoServicos_PrazoDias ;
      private short[] T000X21_A1377ContratoServicos_Indicadores ;
      private bool[] T000X21_n1377ContratoServicos_Indicadores ;
      private short[] T000X21_A2073ContratoServicos_QtdRmn ;
      private bool[] T000X21_n2073ContratoServicos_QtdRmn ;
      private String[] T000X22_A608Servico_Nome ;
      private String[] T000X22_A605Servico_Sigla ;
      private String[] T000X22_A1061Servico_Tela ;
      private bool[] T000X22_n1061Servico_Tela ;
      private bool[] T000X22_A632Servico_Ativo ;
      private String[] T000X22_A156Servico_Descricao ;
      private bool[] T000X22_n156Servico_Descricao ;
      private bool[] T000X22_A2092Servico_IsOrigemReferencia ;
      private int[] T000X22_A631Servico_Vinculado ;
      private bool[] T000X22_n631Servico_Vinculado ;
      private int[] T000X22_A157ServicoGrupo_Codigo ;
      private String[] T000X23_A158ServicoGrupo_Descricao ;
      private String[] T000X24_A2132ContratoServicos_UndCntNome ;
      private bool[] T000X24_n2132ContratoServicos_UndCntNome ;
      private String[] T000X24_A1712ContratoServicos_UndCntSgl ;
      private bool[] T000X24_n1712ContratoServicos_UndCntSgl ;
      private String[] T000X25_A77Contrato_Numero ;
      private String[] T000X25_A78Contrato_NumeroAta ;
      private bool[] T000X25_n78Contrato_NumeroAta ;
      private short[] T000X25_A79Contrato_Ano ;
      private decimal[] T000X25_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] T000X25_A453Contrato_IndiceDivergencia ;
      private int[] T000X25_A39Contratada_Codigo ;
      private int[] T000X25_A75Contrato_AreaTrabalhoCod ;
      private String[] T000X26_A516Contratada_TipoFabrica ;
      private int[] T000X26_A40Contratada_PessoaCod ;
      private String[] T000X27_A41Contratada_PessoaNom ;
      private bool[] T000X27_n41Contratada_PessoaNom ;
      private String[] T000X27_A42Contratada_PessoaCNPJ ;
      private bool[] T000X27_n42Contratada_PessoaCNPJ ;
      private int[] T000X28_A160ContratoServicos_Codigo ;
      private int[] T000X5_A160ContratoServicos_Codigo ;
      private String[] T000X5_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] T000X5_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private String[] T000X5_A1858ContratoServicos_Alias ;
      private bool[] T000X5_n1858ContratoServicos_Alias ;
      private long[] T000X5_A555Servico_QtdContratada ;
      private bool[] T000X5_n555Servico_QtdContratada ;
      private decimal[] T000X5_A557Servico_VlrUnidadeContratada ;
      private decimal[] T000X5_A558Servico_Percentual ;
      private bool[] T000X5_n558Servico_Percentual ;
      private String[] T000X5_A607ServicoContrato_Faturamento ;
      private String[] T000X5_A639ContratoServicos_LocalExec ;
      private String[] T000X5_A868ContratoServicos_TipoVnc ;
      private bool[] T000X5_n868ContratoServicos_TipoVnc ;
      private bool[] T000X5_A888ContratoServicos_HmlSemCnf ;
      private bool[] T000X5_n888ContratoServicos_HmlSemCnf ;
      private String[] T000X5_A1454ContratoServicos_PrazoTpDias ;
      private bool[] T000X5_n1454ContratoServicos_PrazoTpDias ;
      private short[] T000X5_A1152ContratoServicos_PrazoAnalise ;
      private bool[] T000X5_n1152ContratoServicos_PrazoAnalise ;
      private short[] T000X5_A1153ContratoServicos_PrazoResposta ;
      private bool[] T000X5_n1153ContratoServicos_PrazoResposta ;
      private short[] T000X5_A1181ContratoServicos_PrazoGarantia ;
      private bool[] T000X5_n1181ContratoServicos_PrazoGarantia ;
      private short[] T000X5_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] T000X5_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] T000X5_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] T000X5_n1224ContratoServicos_PrazoCorrecao ;
      private short[] T000X5_A1649ContratoServicos_PrazoInicio ;
      private bool[] T000X5_n1649ContratoServicos_PrazoInicio ;
      private bool[] T000X5_A1190ContratoServicos_PrazoImediato ;
      private bool[] T000X5_n1190ContratoServicos_PrazoImediato ;
      private decimal[] T000X5_A1191ContratoServicos_Produtividade ;
      private bool[] T000X5_n1191ContratoServicos_Produtividade ;
      private bool[] T000X5_A1217ContratoServicos_EspelhaAceite ;
      private bool[] T000X5_n1217ContratoServicos_EspelhaAceite ;
      private String[] T000X5_A1266ContratoServicos_Momento ;
      private bool[] T000X5_n1266ContratoServicos_Momento ;
      private String[] T000X5_A1325ContratoServicos_StatusPagFnc ;
      private bool[] T000X5_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] T000X5_A1340ContratoServicos_QntUntCns ;
      private bool[] T000X5_n1340ContratoServicos_QntUntCns ;
      private decimal[] T000X5_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] T000X5_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] T000X5_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] T000X5_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] T000X5_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] T000X5_n1455ContratoServicos_IndiceDivergencia ;
      private int[] T000X5_A1516ContratoServicos_TmpEstAnl ;
      private bool[] T000X5_n1516ContratoServicos_TmpEstAnl ;
      private int[] T000X5_A1501ContratoServicos_TmpEstExc ;
      private bool[] T000X5_n1501ContratoServicos_TmpEstExc ;
      private int[] T000X5_A1502ContratoServicos_TmpEstCrr ;
      private bool[] T000X5_n1502ContratoServicos_TmpEstCrr ;
      private short[] T000X5_A1531ContratoServicos_TipoHierarquia ;
      private bool[] T000X5_n1531ContratoServicos_TipoHierarquia ;
      private short[] T000X5_A1537ContratoServicos_PercTmp ;
      private bool[] T000X5_n1537ContratoServicos_PercTmp ;
      private short[] T000X5_A1538ContratoServicos_PercPgm ;
      private bool[] T000X5_n1538ContratoServicos_PercPgm ;
      private short[] T000X5_A1539ContratoServicos_PercCnc ;
      private bool[] T000X5_n1539ContratoServicos_PercCnc ;
      private bool[] T000X5_A638ContratoServicos_Ativo ;
      private String[] T000X5_A1723ContratoServicos_CodigoFiscal ;
      private bool[] T000X5_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] T000X5_A1799ContratoServicos_LimiteProposta ;
      private bool[] T000X5_n1799ContratoServicos_LimiteProposta ;
      private short[] T000X5_A2074ContratoServicos_CalculoRmn ;
      private bool[] T000X5_n2074ContratoServicos_CalculoRmn ;
      private bool[] T000X5_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] T000X5_A155Servico_Codigo ;
      private int[] T000X5_A74Contrato_Codigo ;
      private int[] T000X5_A1212ContratoServicos_UnidadeContratada ;
      private int[] T000X29_A160ContratoServicos_Codigo ;
      private int[] T000X30_A160ContratoServicos_Codigo ;
      private int[] T000X4_A160ContratoServicos_Codigo ;
      private String[] T000X4_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] T000X4_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private String[] T000X4_A1858ContratoServicos_Alias ;
      private bool[] T000X4_n1858ContratoServicos_Alias ;
      private long[] T000X4_A555Servico_QtdContratada ;
      private bool[] T000X4_n555Servico_QtdContratada ;
      private decimal[] T000X4_A557Servico_VlrUnidadeContratada ;
      private decimal[] T000X4_A558Servico_Percentual ;
      private bool[] T000X4_n558Servico_Percentual ;
      private String[] T000X4_A607ServicoContrato_Faturamento ;
      private String[] T000X4_A639ContratoServicos_LocalExec ;
      private String[] T000X4_A868ContratoServicos_TipoVnc ;
      private bool[] T000X4_n868ContratoServicos_TipoVnc ;
      private bool[] T000X4_A888ContratoServicos_HmlSemCnf ;
      private bool[] T000X4_n888ContratoServicos_HmlSemCnf ;
      private String[] T000X4_A1454ContratoServicos_PrazoTpDias ;
      private bool[] T000X4_n1454ContratoServicos_PrazoTpDias ;
      private short[] T000X4_A1152ContratoServicos_PrazoAnalise ;
      private bool[] T000X4_n1152ContratoServicos_PrazoAnalise ;
      private short[] T000X4_A1153ContratoServicos_PrazoResposta ;
      private bool[] T000X4_n1153ContratoServicos_PrazoResposta ;
      private short[] T000X4_A1181ContratoServicos_PrazoGarantia ;
      private bool[] T000X4_n1181ContratoServicos_PrazoGarantia ;
      private short[] T000X4_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] T000X4_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] T000X4_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] T000X4_n1224ContratoServicos_PrazoCorrecao ;
      private short[] T000X4_A1649ContratoServicos_PrazoInicio ;
      private bool[] T000X4_n1649ContratoServicos_PrazoInicio ;
      private bool[] T000X4_A1190ContratoServicos_PrazoImediato ;
      private bool[] T000X4_n1190ContratoServicos_PrazoImediato ;
      private decimal[] T000X4_A1191ContratoServicos_Produtividade ;
      private bool[] T000X4_n1191ContratoServicos_Produtividade ;
      private bool[] T000X4_A1217ContratoServicos_EspelhaAceite ;
      private bool[] T000X4_n1217ContratoServicos_EspelhaAceite ;
      private String[] T000X4_A1266ContratoServicos_Momento ;
      private bool[] T000X4_n1266ContratoServicos_Momento ;
      private String[] T000X4_A1325ContratoServicos_StatusPagFnc ;
      private bool[] T000X4_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] T000X4_A1340ContratoServicos_QntUntCns ;
      private bool[] T000X4_n1340ContratoServicos_QntUntCns ;
      private decimal[] T000X4_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] T000X4_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] T000X4_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] T000X4_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] T000X4_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] T000X4_n1455ContratoServicos_IndiceDivergencia ;
      private int[] T000X4_A1516ContratoServicos_TmpEstAnl ;
      private bool[] T000X4_n1516ContratoServicos_TmpEstAnl ;
      private int[] T000X4_A1501ContratoServicos_TmpEstExc ;
      private bool[] T000X4_n1501ContratoServicos_TmpEstExc ;
      private int[] T000X4_A1502ContratoServicos_TmpEstCrr ;
      private bool[] T000X4_n1502ContratoServicos_TmpEstCrr ;
      private short[] T000X4_A1531ContratoServicos_TipoHierarquia ;
      private bool[] T000X4_n1531ContratoServicos_TipoHierarquia ;
      private short[] T000X4_A1537ContratoServicos_PercTmp ;
      private bool[] T000X4_n1537ContratoServicos_PercTmp ;
      private short[] T000X4_A1538ContratoServicos_PercPgm ;
      private bool[] T000X4_n1538ContratoServicos_PercPgm ;
      private short[] T000X4_A1539ContratoServicos_PercCnc ;
      private bool[] T000X4_n1539ContratoServicos_PercCnc ;
      private bool[] T000X4_A638ContratoServicos_Ativo ;
      private String[] T000X4_A1723ContratoServicos_CodigoFiscal ;
      private bool[] T000X4_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] T000X4_A1799ContratoServicos_LimiteProposta ;
      private bool[] T000X4_n1799ContratoServicos_LimiteProposta ;
      private short[] T000X4_A2074ContratoServicos_CalculoRmn ;
      private bool[] T000X4_n2074ContratoServicos_CalculoRmn ;
      private bool[] T000X4_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] T000X4_A155Servico_Codigo ;
      private int[] T000X4_A74Contrato_Codigo ;
      private int[] T000X4_A1212ContratoServicos_UnidadeContratada ;
      private int[] T000X31_A160ContratoServicos_Codigo ;
      private String[] T000X34_A608Servico_Nome ;
      private String[] T000X34_A605Servico_Sigla ;
      private String[] T000X34_A1061Servico_Tela ;
      private bool[] T000X34_n1061Servico_Tela ;
      private bool[] T000X34_A632Servico_Ativo ;
      private String[] T000X34_A156Servico_Descricao ;
      private bool[] T000X34_n156Servico_Descricao ;
      private bool[] T000X34_A2092Servico_IsOrigemReferencia ;
      private int[] T000X34_A631Servico_Vinculado ;
      private bool[] T000X34_n631Servico_Vinculado ;
      private int[] T000X34_A157ServicoGrupo_Codigo ;
      private String[] T000X35_A158ServicoGrupo_Descricao ;
      private String[] T000X36_A2132ContratoServicos_UndCntNome ;
      private bool[] T000X36_n2132ContratoServicos_UndCntNome ;
      private String[] T000X36_A1712ContratoServicos_UndCntSgl ;
      private bool[] T000X36_n1712ContratoServicos_UndCntSgl ;
      private String[] T000X37_A77Contrato_Numero ;
      private String[] T000X37_A78Contrato_NumeroAta ;
      private bool[] T000X37_n78Contrato_NumeroAta ;
      private short[] T000X37_A79Contrato_Ano ;
      private decimal[] T000X37_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] T000X37_A453Contrato_IndiceDivergencia ;
      private int[] T000X37_A39Contratada_Codigo ;
      private int[] T000X37_A75Contrato_AreaTrabalhoCod ;
      private String[] T000X38_A516Contratada_TipoFabrica ;
      private int[] T000X38_A40Contratada_PessoaCod ;
      private String[] T000X39_A41Contratada_PessoaNom ;
      private bool[] T000X39_n41Contratada_PessoaNom ;
      private String[] T000X39_A42Contratada_PessoaCNPJ ;
      private bool[] T000X39_n42Contratada_PessoaCNPJ ;
      private int[] T000X40_A192Contagem_Codigo ;
      private int[] T000X41_A456ContagemResultado_Codigo ;
      private int[] T000X42_A1473ContratoServicosCusto_Codigo ;
      private int[] T000X43_A1336ContratoServicosPrioridade_Codigo ;
      private int[] T000X44_A1269ContratoServicosIndicador_Codigo ;
      private int[] T000X45_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] T000X45_A1184AgendaAtendimento_Data ;
      private int[] T000X45_A1209AgendaAtendimento_CodDmn ;
      private int[] T000X46_A926ContratoServicosTelas_ContratoCod ;
      private short[] T000X46_A938ContratoServicosTelas_Sequencial ;
      private int[] T000X47_A917ContratoSrvVnc_Codigo ;
      private int[] T000X48_A917ContratoSrvVnc_Codigo ;
      private int[] T000X49_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T000X50_A160ContratoServicos_Codigo ;
      private int[] T000X50_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] T000X51_A160ContratoServicos_Codigo ;
      private int[] T000X51_A1749Artefatos_Codigo ;
      private int[] T000X52_A160ContratoServicos_Codigo ;
      private int[] T000X52_A1465ContratoServicosDePara_Codigo ;
      private int[] T000X53_A160ContratoServicos_Codigo ;
      private int[] T000X53_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T000X53_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T000X54_A160ContratoServicos_Codigo ;
      private int[] T000X55_A160ContratoServicos_Codigo ;
      private short[] T000X55_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] T000X55_A2070ContratoServicosRmn_Inicio ;
      private decimal[] T000X55_A2071ContratoServicosRmn_Fim ;
      private decimal[] T000X55_A2072ContratoServicosRmn_Valor ;
      private int[] T000X56_A160ContratoServicos_Codigo ;
      private short[] T000X56_A2069ContratoServicosRmn_Sequencial ;
      private int[] T000X3_A160ContratoServicos_Codigo ;
      private short[] T000X3_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] T000X3_A2070ContratoServicosRmn_Inicio ;
      private decimal[] T000X3_A2071ContratoServicosRmn_Fim ;
      private decimal[] T000X3_A2072ContratoServicosRmn_Valor ;
      private int[] T000X2_A160ContratoServicos_Codigo ;
      private short[] T000X2_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] T000X2_A2070ContratoServicosRmn_Inicio ;
      private decimal[] T000X2_A2071ContratoServicosRmn_Fim ;
      private decimal[] T000X2_A2072ContratoServicosRmn_Valor ;
      private int[] T000X60_A160ContratoServicos_Codigo ;
      private short[] T000X60_A2069ContratoServicosRmn_Sequencial ;
      private int[] T000X61_A157ServicoGrupo_Codigo ;
      private String[] T000X61_A158ServicoGrupo_Descricao ;
      private int[] T000X62_A155Servico_Codigo ;
      private String[] T000X62_A605Servico_Sigla ;
      private int[] T000X62_A157ServicoGrupo_Codigo ;
      private short[] T000X62_A1530Servico_TipoHierarquia ;
      private bool[] T000X62_n1530Servico_TipoHierarquia ;
      private bool[] T000X62_A632Servico_Ativo ;
      private int[] T000X63_A155Servico_Codigo ;
      private String[] T000X63_A608Servico_Nome ;
      private int[] T000X63_A631Servico_Vinculado ;
      private bool[] T000X63_n631Servico_Vinculado ;
      private bool[] T000X63_A632Servico_Ativo ;
      private int[] T000X64_A1204ContratoUnidades_UndMedCod ;
      private String[] T000X64_A1205ContratoUnidades_UndMedNom ;
      private bool[] T000X64_n1205ContratoUnidades_UndMedNom ;
      private int[] T000X64_A1207ContratoUnidades_ContratoCod ;
      private String[] T000X65_A2132ContratoServicos_UndCntNome ;
      private bool[] T000X65_n2132ContratoServicos_UndCntNome ;
      private String[] T000X65_A1712ContratoServicos_UndCntSgl ;
      private bool[] T000X65_n1712ContratoServicos_UndCntSgl ;
      private String[] T000X66_A608Servico_Nome ;
      private String[] T000X66_A605Servico_Sigla ;
      private String[] T000X66_A1061Servico_Tela ;
      private bool[] T000X66_n1061Servico_Tela ;
      private bool[] T000X66_A632Servico_Ativo ;
      private String[] T000X66_A156Servico_Descricao ;
      private bool[] T000X66_n156Servico_Descricao ;
      private bool[] T000X66_A2092Servico_IsOrigemReferencia ;
      private int[] T000X66_A631Servico_Vinculado ;
      private bool[] T000X66_n631Servico_Vinculado ;
      private int[] T000X66_A157ServicoGrupo_Codigo ;
      private String[] T000X67_A158ServicoGrupo_Descricao ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV29AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratoservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new UpdateCursor(def[50])
         ,new UpdateCursor(def[51])
         ,new UpdateCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
         ,new ForEachCursor(def[57])
         ,new ForEachCursor(def[58])
         ,new ForEachCursor(def[59])
         ,new ForEachCursor(def[60])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000X7 ;
          prmT000X7 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X14 ;
          prmT000X14 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X16 ;
          prmT000X16 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X18 ;
          prmT000X18 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X21 ;
          prmT000X21 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000X21 ;
          cmdBufferT000X21=" SELECT T5.[ContratoServicosPrazo_CntSrvCod], TM1.[ContratoServicos_Codigo], TM1.[ContratoServicos_PrazoCorrecaoTipo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T2.[Contrato_ValorUnidadeContratacao], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Contratada_TipoFabrica], TM1.[ContratoServicos_Alias], T8.[Servico_Nome], T8.[Servico_Sigla], T8.[Servico_Tela], T8.[Servico_Ativo], T8.[Servico_Descricao], T9.[ServicoGrupo_Descricao], T8.[Servico_IsOrigemReferencia], T10.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T10.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, TM1.[Servico_QtdContratada], TM1.[Servico_VlrUnidadeContratada], TM1.[Servico_Percentual], TM1.[ServicoContrato_Faturamento], TM1.[ContratoServicos_LocalExec], TM1.[ContratoServicos_TipoVnc], TM1.[ContratoServicos_HmlSemCnf], TM1.[ContratoServicos_PrazoTpDias], TM1.[ContratoServicos_PrazoAnalise], TM1.[ContratoServicos_PrazoResposta], TM1.[ContratoServicos_PrazoGarantia], TM1.[ContratoServicos_PrazoAtendeGarantia], TM1.[ContratoServicos_PrazoCorrecao], TM1.[ContratoServicos_PrazoInicio], TM1.[ContratoServicos_PrazoImediato], TM1.[ContratoServicos_Produtividade], TM1.[ContratoServicos_EspelhaAceite], TM1.[ContratoServicos_Momento], TM1.[ContratoServicos_StatusPagFnc], TM1.[ContratoServicos_QntUntCns], TM1.[ContratoServicos_FatorCnvUndCnt], TM1.[ContratoServicos_NaoRequerAtr], T2.[Contrato_IndiceDivergencia], TM1.[ContratoServicos_IndiceDivergencia], TM1.[ContratoServicos_TmpEstAnl], TM1.[ContratoServicos_TmpEstExc], TM1.[ContratoServicos_TmpEstCrr], TM1.[ContratoServicos_TipoHierarquia], TM1.[ContratoServicos_PercTmp], TM1.[ContratoServicos_PercPgm], TM1.[ContratoServicos_PercCnc], TM1.[ContratoServicos_Ativo], TM1.[ContratoServicos_CodigoFiscal], "
          + " TM1.[ContratoServicos_LimiteProposta], TM1.[ContratoServicos_CalculoRmn], TM1.[ContratoServicos_SolicitaGestorSistema], TM1.[Servico_Codigo], TM1.[Contrato_Codigo], TM1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T8.[Servico_Vinculado], T8.[ServicoGrupo_Codigo], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contrato_AreaTrabalhoCod], COALESCE( T5.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( T5.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( T6.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores, COALESCE( T7.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn FROM ((((((((([ContratoServicos] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicosPrazo] T5 WITH (NOLOCK) ON T5.[ContratoServicosPrazo_CntSrvCod] = TM1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T6 ON T6.[ContratoServicosIndicador_CntSrvCod] = TM1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (NOLOCK) GROUP BY [ContratoServicos_Codigo] ) T7 ON T7.[ContratoServicos_Codigo] = TM1.[ContratoServicos_Codigo]) INNER JOIN [Servico] T8 WITH (NOLOCK) ON T8.[Servico_Codigo] = TM1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T9 WITH (NOLOCK) ON T9.[ServicoGrupo_Codigo]"
          + " = T8.[ServicoGrupo_Codigo]) INNER JOIN [UnidadeMedicao] T10 WITH (NOLOCK) ON T10.[UnidadeMedicao_Codigo] = TM1.[ContratoServicos_UnidadeContratada]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY TM1.[ContratoServicos_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT000X8 ;
          prmT000X8 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X11 ;
          prmT000X11 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X10 ;
          prmT000X10 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X9 ;
          prmT000X9 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X12 ;
          prmT000X12 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X13 ;
          prmT000X13 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X22 ;
          prmT000X22 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X23 ;
          prmT000X23 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X24 ;
          prmT000X24 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X25 ;
          prmT000X25 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X26 ;
          prmT000X26 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X27 ;
          prmT000X27 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X28 ;
          prmT000X28 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X5 ;
          prmT000X5 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X29 ;
          prmT000X29 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X30 ;
          prmT000X30 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X4 ;
          prmT000X4 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X31 ;
          prmT000X31 = new Object[] {
          new Object[] {"@ContratoServicos_PrazoCorrecaoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Servico_Percentual",SqlDbType.Decimal,7,3} ,
          new Object[] {"@ServicoContrato_Faturamento",SqlDbType.VarChar,5,0} ,
          new Object[] {"@ContratoServicos_LocalExec",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_TipoVnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_HmlSemCnf",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoTpDias",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoResposta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAtendeGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoInicio",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoImediato",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_EspelhaAceite",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Momento",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_StatusPagFnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_QntUntCns",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_FatorCnvUndCnt",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_NaoRequerAtr",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_CodigoFiscal",SqlDbType.Char,5,0} ,
          new Object[] {"@ContratoServicos_LimiteProposta",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_CalculoRmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_SolicitaGestorSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000X31 ;
          cmdBufferT000X31=" INSERT INTO [ContratoServicos]([ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_Alias], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoInicio], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_EspelhaAceite], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercPgm], [ContratoServicos_PercCnc], [ContratoServicos_Ativo], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema], [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_UnidadeContratada]) VALUES(@ContratoServicos_PrazoCorrecaoTipo, @ContratoServicos_Alias, @Servico_QtdContratada, @Servico_VlrUnidadeContratada, @Servico_Percentual, @ServicoContrato_Faturamento, @ContratoServicos_LocalExec, @ContratoServicos_TipoVnc, @ContratoServicos_HmlSemCnf, @ContratoServicos_PrazoTpDias, @ContratoServicos_PrazoAnalise, @ContratoServicos_PrazoResposta, @ContratoServicos_PrazoGarantia, @ContratoServicos_PrazoAtendeGarantia, @ContratoServicos_PrazoCorrecao, @ContratoServicos_PrazoInicio, @ContratoServicos_PrazoImediato, "
          + " @ContratoServicos_Produtividade, @ContratoServicos_EspelhaAceite, @ContratoServicos_Momento, @ContratoServicos_StatusPagFnc, @ContratoServicos_QntUntCns, @ContratoServicos_FatorCnvUndCnt, @ContratoServicos_NaoRequerAtr, @ContratoServicos_IndiceDivergencia, @ContratoServicos_TmpEstAnl, @ContratoServicos_TmpEstExc, @ContratoServicos_TmpEstCrr, @ContratoServicos_TipoHierarquia, @ContratoServicos_PercTmp, @ContratoServicos_PercPgm, @ContratoServicos_PercCnc, @ContratoServicos_Ativo, @ContratoServicos_CodigoFiscal, @ContratoServicos_LimiteProposta, @ContratoServicos_CalculoRmn, @ContratoServicos_SolicitaGestorSistema, @Servico_Codigo, @Contrato_Codigo, @ContratoServicos_UnidadeContratada); SELECT SCOPE_IDENTITY()" ;
          Object[] prmT000X32 ;
          prmT000X32 = new Object[] {
          new Object[] {"@ContratoServicos_PrazoCorrecaoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Servico_Percentual",SqlDbType.Decimal,7,3} ,
          new Object[] {"@ServicoContrato_Faturamento",SqlDbType.VarChar,5,0} ,
          new Object[] {"@ContratoServicos_LocalExec",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_TipoVnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_HmlSemCnf",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoTpDias",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoResposta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAtendeGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoInicio",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoImediato",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_EspelhaAceite",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Momento",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_StatusPagFnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_QntUntCns",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_FatorCnvUndCnt",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_NaoRequerAtr",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_CodigoFiscal",SqlDbType.Char,5,0} ,
          new Object[] {"@ContratoServicos_LimiteProposta",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_CalculoRmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_SolicitaGestorSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000X32 ;
          cmdBufferT000X32=" UPDATE [ContratoServicos] SET [ContratoServicos_PrazoCorrecaoTipo]=@ContratoServicos_PrazoCorrecaoTipo, [ContratoServicos_Alias]=@ContratoServicos_Alias, [Servico_QtdContratada]=@Servico_QtdContratada, [Servico_VlrUnidadeContratada]=@Servico_VlrUnidadeContratada, [Servico_Percentual]=@Servico_Percentual, [ServicoContrato_Faturamento]=@ServicoContrato_Faturamento, [ContratoServicos_LocalExec]=@ContratoServicos_LocalExec, [ContratoServicos_TipoVnc]=@ContratoServicos_TipoVnc, [ContratoServicos_HmlSemCnf]=@ContratoServicos_HmlSemCnf, [ContratoServicos_PrazoTpDias]=@ContratoServicos_PrazoTpDias, [ContratoServicos_PrazoAnalise]=@ContratoServicos_PrazoAnalise, [ContratoServicos_PrazoResposta]=@ContratoServicos_PrazoResposta, [ContratoServicos_PrazoGarantia]=@ContratoServicos_PrazoGarantia, [ContratoServicos_PrazoAtendeGarantia]=@ContratoServicos_PrazoAtendeGarantia, [ContratoServicos_PrazoCorrecao]=@ContratoServicos_PrazoCorrecao, [ContratoServicos_PrazoInicio]=@ContratoServicos_PrazoInicio, [ContratoServicos_PrazoImediato]=@ContratoServicos_PrazoImediato, [ContratoServicos_Produtividade]=@ContratoServicos_Produtividade, [ContratoServicos_EspelhaAceite]=@ContratoServicos_EspelhaAceite, [ContratoServicos_Momento]=@ContratoServicos_Momento, [ContratoServicos_StatusPagFnc]=@ContratoServicos_StatusPagFnc, [ContratoServicos_QntUntCns]=@ContratoServicos_QntUntCns, [ContratoServicos_FatorCnvUndCnt]=@ContratoServicos_FatorCnvUndCnt, [ContratoServicos_NaoRequerAtr]=@ContratoServicos_NaoRequerAtr, [ContratoServicos_IndiceDivergencia]=@ContratoServicos_IndiceDivergencia, [ContratoServicos_TmpEstAnl]=@ContratoServicos_TmpEstAnl, [ContratoServicos_TmpEstExc]=@ContratoServicos_TmpEstExc, [ContratoServicos_TmpEstCrr]=@ContratoServicos_TmpEstCrr, [ContratoServicos_TipoHierarquia]=@ContratoServicos_TipoHierarquia, "
          + " [ContratoServicos_PercTmp]=@ContratoServicos_PercTmp, [ContratoServicos_PercPgm]=@ContratoServicos_PercPgm, [ContratoServicos_PercCnc]=@ContratoServicos_PercCnc, [ContratoServicos_Ativo]=@ContratoServicos_Ativo, [ContratoServicos_CodigoFiscal]=@ContratoServicos_CodigoFiscal, [ContratoServicos_LimiteProposta]=@ContratoServicos_LimiteProposta, [ContratoServicos_CalculoRmn]=@ContratoServicos_CalculoRmn, [ContratoServicos_SolicitaGestorSistema]=@ContratoServicos_SolicitaGestorSistema, [Servico_Codigo]=@Servico_Codigo, [Contrato_Codigo]=@Contrato_Codigo, [ContratoServicos_UnidadeContratada]=@ContratoServicos_UnidadeContratada  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo" ;
          Object[] prmT000X33 ;
          prmT000X33 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X34 ;
          prmT000X34 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X35 ;
          prmT000X35 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X36 ;
          prmT000X36 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X37 ;
          prmT000X37 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X38 ;
          prmT000X38 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X39 ;
          prmT000X39 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X40 ;
          prmT000X40 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X41 ;
          prmT000X41 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X42 ;
          prmT000X42 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X43 ;
          prmT000X43 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X44 ;
          prmT000X44 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X45 ;
          prmT000X45 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X46 ;
          prmT000X46 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X47 ;
          prmT000X47 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X48 ;
          prmT000X48 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X49 ;
          prmT000X49 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X50 ;
          prmT000X50 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X51 ;
          prmT000X51 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X52 ;
          prmT000X52 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X53 ;
          prmT000X53 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X54 ;
          prmT000X54 = new Object[] {
          } ;
          Object[] prmT000X55 ;
          prmT000X55 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000X56 ;
          prmT000X56 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000X3 ;
          prmT000X3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000X2 ;
          prmT000X2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000X57 ;
          prmT000X57 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Valor",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmT000X58 ;
          prmT000X58 = new Object[] {
          new Object[] {"@ContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Valor",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000X59 ;
          prmT000X59 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000X60 ;
          prmT000X60 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X61 ;
          prmT000X61 = new Object[] {
          } ;
          Object[] prmT000X62 ;
          prmT000X62 = new Object[] {
          new Object[] {"@AV14ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X63 ;
          prmT000X63 = new Object[] {
          new Object[] {"@AV17Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X64 ;
          prmT000X64 = new Object[] {
          new Object[] {"@AV27Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X65 ;
          prmT000X65 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X66 ;
          prmT000X66 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000X67 ;
          prmT000X67 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000X2", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X2,1,0,true,false )
             ,new CursorDef("T000X3", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X3,1,0,true,false )
             ,new CursorDef("T000X4", "SELECT [ContratoServicos_Codigo], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_Alias], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoInicio], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_EspelhaAceite], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercPgm], [ContratoServicos_PercCnc], [ContratoServicos_Ativo], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema], [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada FROM [ContratoServicos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X4,1,0,true,false )
             ,new CursorDef("T000X5", "SELECT [ContratoServicos_Codigo], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_Alias], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoInicio], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_EspelhaAceite], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercPgm], [ContratoServicos_PercCnc], [ContratoServicos_Ativo], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema], [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X5,1,0,true,false )
             ,new CursorDef("T000X7", "SELECT COALESCE( T1.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos FROM (SELECT COUNT(*) AS ContratoServicos_Prazos FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X7,1,0,true,false )
             ,new CursorDef("T000X8", "SELECT [Servico_Nome], [Servico_Sigla], [Servico_Tela], [Servico_Ativo], [Servico_Descricao], [Servico_IsOrigemReferencia], [Servico_Vinculado], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X8,1,0,true,false )
             ,new CursorDef("T000X9", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_ValorUnidadeContratacao], [Contrato_IndiceDivergencia], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X9,1,0,true,false )
             ,new CursorDef("T000X10", "SELECT [UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X10,1,0,true,false )
             ,new CursorDef("T000X11", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X11,1,0,true,false )
             ,new CursorDef("T000X12", "SELECT [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X12,1,0,true,false )
             ,new CursorDef("T000X13", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X13,1,0,true,false )
             ,new CursorDef("T000X14", "SELECT COALESCE( [ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( [ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X14,1,0,true,false )
             ,new CursorDef("T000X16", "SELECT COALESCE( T1.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores FROM (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T1 WHERE T1.[ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X16,1,0,true,false )
             ,new CursorDef("T000X18", "SELECT COALESCE( T1.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn FROM (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (UPDLOCK) GROUP BY [ContratoServicos_Codigo] ) T1 WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X18,1,0,true,false )
             ,new CursorDef("T000X21", cmdBufferT000X21,true, GxErrorMask.GX_NOMASK, false, this,prmT000X21,100,0,true,false )
             ,new CursorDef("T000X22", "SELECT [Servico_Nome], [Servico_Sigla], [Servico_Tela], [Servico_Ativo], [Servico_Descricao], [Servico_IsOrigemReferencia], [Servico_Vinculado], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X22,1,0,true,false )
             ,new CursorDef("T000X23", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X23,1,0,true,false )
             ,new CursorDef("T000X24", "SELECT [UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X24,1,0,true,false )
             ,new CursorDef("T000X25", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_ValorUnidadeContratacao], [Contrato_IndiceDivergencia], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X25,1,0,true,false )
             ,new CursorDef("T000X26", "SELECT [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X26,1,0,true,false )
             ,new CursorDef("T000X27", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X27,1,0,true,false )
             ,new CursorDef("T000X28", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000X28,1,0,true,false )
             ,new CursorDef("T000X29", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] > @ContratoServicos_Codigo) ORDER BY [ContratoServicos_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000X29,1,0,true,true )
             ,new CursorDef("T000X30", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] < @ContratoServicos_Codigo) ORDER BY [ContratoServicos_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000X30,1,0,true,true )
             ,new CursorDef("T000X31", cmdBufferT000X31, GxErrorMask.GX_NOMASK,prmT000X31)
             ,new CursorDef("T000X32", cmdBufferT000X32, GxErrorMask.GX_NOMASK,prmT000X32)
             ,new CursorDef("T000X33", "DELETE FROM [ContratoServicos]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK,prmT000X33)
             ,new CursorDef("T000X34", "SELECT [Servico_Nome], [Servico_Sigla], [Servico_Tela], [Servico_Ativo], [Servico_Descricao], [Servico_IsOrigemReferencia], [Servico_Vinculado], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X34,1,0,true,false )
             ,new CursorDef("T000X35", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X35,1,0,true,false )
             ,new CursorDef("T000X36", "SELECT [UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X36,1,0,true,false )
             ,new CursorDef("T000X37", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_ValorUnidadeContratacao], [Contrato_IndiceDivergencia], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X37,1,0,true,false )
             ,new CursorDef("T000X38", "SELECT [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X38,1,0,true,false )
             ,new CursorDef("T000X39", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X39,1,0,true,false )
             ,new CursorDef("T000X40", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X40,1,0,true,true )
             ,new CursorDef("T000X41", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X41,1,0,true,true )
             ,new CursorDef("T000X42", "SELECT TOP 1 [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X42,1,0,true,true )
             ,new CursorDef("T000X43", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X43,1,0,true,true )
             ,new CursorDef("T000X44", "SELECT TOP 1 [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X44,1,0,true,true )
             ,new CursorDef("T000X45", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CntSrcCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X45,1,0,true,true )
             ,new CursorDef("T000X46", "SELECT TOP 1 [ContratoServicosTelas_ContratoCod], [ContratoServicosTelas_Sequencial] FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X46,1,0,true,true )
             ,new CursorDef("T000X47", "SELECT TOP 1 [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_SrvVncCntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X47,1,0,true,true )
             ,new CursorDef("T000X48", "SELECT TOP 1 [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X48,1,0,true,true )
             ,new CursorDef("T000X49", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X49,1,0,true,true )
             ,new CursorDef("T000X50", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X50,1,0,true,true )
             ,new CursorDef("T000X51", "SELECT TOP 1 [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X51,1,0,true,true )
             ,new CursorDef("T000X52", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X52,1,0,true,true )
             ,new CursorDef("T000X53", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X53,1,0,true,true )
             ,new CursorDef("T000X54", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) ORDER BY [ContratoServicos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000X54,100,0,true,false )
             ,new CursorDef("T000X55", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X55,11,0,true,false )
             ,new CursorDef("T000X56", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X56,1,0,true,false )
             ,new CursorDef("T000X57", "INSERT INTO [ContratoServicosRmn]([ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor]) VALUES(@ContratoServicos_Codigo, @ContratoServicosRmn_Sequencial, @ContratoServicosRmn_Inicio, @ContratoServicosRmn_Fim, @ContratoServicosRmn_Valor)", GxErrorMask.GX_NOMASK,prmT000X57)
             ,new CursorDef("T000X58", "UPDATE [ContratoServicosRmn] SET [ContratoServicosRmn_Inicio]=@ContratoServicosRmn_Inicio, [ContratoServicosRmn_Fim]=@ContratoServicosRmn_Fim, [ContratoServicosRmn_Valor]=@ContratoServicosRmn_Valor  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial", GxErrorMask.GX_NOMASK,prmT000X58)
             ,new CursorDef("T000X59", "DELETE FROM [ContratoServicosRmn]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial", GxErrorMask.GX_NOMASK,prmT000X59)
             ,new CursorDef("T000X60", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X60,11,0,true,false )
             ,new CursorDef("T000X61", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X61,0,0,true,false )
             ,new CursorDef("T000X62", "SELECT [Servico_Codigo], [Servico_Sigla], [ServicoGrupo_Codigo], [Servico_TipoHierarquia], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Ativo] = 1) AND ([ServicoGrupo_Codigo] = @AV14ServicoGrupo_Codigo) AND ([Servico_TipoHierarquia] = 1) ORDER BY [Servico_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X62,0,0,true,false )
             ,new CursorDef("T000X63", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Vinculado], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Ativo] = 1) AND ([Servico_Vinculado] = @AV17Servico_Codigo) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X63,0,0,true,false )
             ,new CursorDef("T000X64", "SELECT T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_ContratoCod] FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod]) WHERE T1.[ContratoUnidades_ContratoCod] = @AV27Contrato_Codigo ORDER BY T2.[UnidadeMedicao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X64,0,0,true,false )
             ,new CursorDef("T000X65", "SELECT [UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X65,1,0,true,false )
             ,new CursorDef("T000X66", "SELECT [Servico_Nome], [Servico_Sigla], [Servico_Tela], [Servico_Ativo], [Servico_Descricao], [Servico_IsOrigemReferencia], [Servico_Vinculado], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X66,1,0,true,false )
             ,new CursorDef("T000X67", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000X67,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((short[]) buf[28])[0] = rslt.getShort(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((bool[]) buf[34])[0] = rslt.getBool(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((String[]) buf[36])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((String[]) buf[38])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((decimal[]) buf[42])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((bool[]) buf[44])[0] = rslt.getBool(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((short[]) buf[54])[0] = rslt.getShort(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((short[]) buf[56])[0] = rslt.getShort(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((short[]) buf[58])[0] = rslt.getShort(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((short[]) buf[60])[0] = rslt.getShort(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((bool[]) buf[62])[0] = rslt.getBool(34) ;
                ((String[]) buf[63])[0] = rslt.getString(35, 5) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(35);
                ((decimal[]) buf[65])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(36);
                ((short[]) buf[67])[0] = rslt.getShort(37) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(37);
                ((bool[]) buf[69])[0] = rslt.getBool(38) ;
                ((int[]) buf[70])[0] = rslt.getInt(39) ;
                ((int[]) buf[71])[0] = rslt.getInt(40) ;
                ((int[]) buf[72])[0] = rslt.getInt(41) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((short[]) buf[28])[0] = rslt.getShort(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((bool[]) buf[34])[0] = rslt.getBool(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((String[]) buf[36])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((String[]) buf[38])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((decimal[]) buf[42])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((bool[]) buf[44])[0] = rslt.getBool(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((short[]) buf[54])[0] = rslt.getShort(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((short[]) buf[56])[0] = rslt.getShort(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((short[]) buf[58])[0] = rslt.getShort(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((short[]) buf[60])[0] = rslt.getShort(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((bool[]) buf[62])[0] = rslt.getBool(34) ;
                ((String[]) buf[63])[0] = rslt.getString(35, 5) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(35);
                ((decimal[]) buf[65])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(36);
                ((short[]) buf[67])[0] = rslt.getShort(37) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(37);
                ((bool[]) buf[69])[0] = rslt.getBool(38) ;
                ((int[]) buf[70])[0] = rslt.getInt(39) ;
                ((int[]) buf[71])[0] = rslt.getInt(40) ;
                ((int[]) buf[72])[0] = rslt.getInt(41) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((String[]) buf[14])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((String[]) buf[16])[0] = rslt.getString(12, 50) ;
                ((String[]) buf[17])[0] = rslt.getString(13, 15) ;
                ((String[]) buf[18])[0] = rslt.getString(14, 3) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(14);
                ((bool[]) buf[20])[0] = rslt.getBool(15) ;
                ((String[]) buf[21])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((String[]) buf[23])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[24])[0] = rslt.getBool(18) ;
                ((String[]) buf[25])[0] = rslt.getString(19, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(19);
                ((String[]) buf[27])[0] = rslt.getString(20, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(20);
                ((long[]) buf[29])[0] = rslt.getLong(21) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(21);
                ((decimal[]) buf[31])[0] = rslt.getDecimal(22) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(23);
                ((String[]) buf[34])[0] = rslt.getVarchar(24) ;
                ((String[]) buf[35])[0] = rslt.getString(25, 1) ;
                ((String[]) buf[36])[0] = rslt.getString(26, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(26);
                ((bool[]) buf[38])[0] = rslt.getBool(27) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(27);
                ((String[]) buf[40])[0] = rslt.getString(28, 1) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(28);
                ((short[]) buf[42])[0] = rslt.getShort(29) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(29);
                ((short[]) buf[44])[0] = rslt.getShort(30) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(30);
                ((short[]) buf[46])[0] = rslt.getShort(31) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(31);
                ((short[]) buf[48])[0] = rslt.getShort(32) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(32);
                ((short[]) buf[50])[0] = rslt.getShort(33) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(33);
                ((short[]) buf[52])[0] = rslt.getShort(34) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(34);
                ((bool[]) buf[54])[0] = rslt.getBool(35) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(35);
                ((decimal[]) buf[56])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(36);
                ((bool[]) buf[58])[0] = rslt.getBool(37) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(37);
                ((String[]) buf[60])[0] = rslt.getString(38, 1) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(38);
                ((String[]) buf[62])[0] = rslt.getString(39, 1) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(39);
                ((decimal[]) buf[64])[0] = rslt.getDecimal(40) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(40);
                ((decimal[]) buf[66])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(41);
                ((bool[]) buf[68])[0] = rslt.getBool(42) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(42);
                ((decimal[]) buf[70])[0] = rslt.getDecimal(43) ;
                ((decimal[]) buf[71])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(44);
                ((int[]) buf[73])[0] = rslt.getInt(45) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(45);
                ((int[]) buf[75])[0] = rslt.getInt(46) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(46);
                ((int[]) buf[77])[0] = rslt.getInt(47) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(47);
                ((short[]) buf[79])[0] = rslt.getShort(48) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(48);
                ((short[]) buf[81])[0] = rslt.getShort(49) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(49);
                ((short[]) buf[83])[0] = rslt.getShort(50) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(50);
                ((short[]) buf[85])[0] = rslt.getShort(51) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(51);
                ((bool[]) buf[87])[0] = rslt.getBool(52) ;
                ((String[]) buf[88])[0] = rslt.getString(53, 5) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(53);
                ((decimal[]) buf[90])[0] = rslt.getDecimal(54) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(54);
                ((short[]) buf[92])[0] = rslt.getShort(55) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(55);
                ((bool[]) buf[94])[0] = rslt.getBool(56) ;
                ((int[]) buf[95])[0] = rslt.getInt(57) ;
                ((int[]) buf[96])[0] = rslt.getInt(58) ;
                ((int[]) buf[97])[0] = rslt.getInt(59) ;
                ((int[]) buf[98])[0] = rslt.getInt(60) ;
                ((bool[]) buf[99])[0] = rslt.wasNull(60);
                ((int[]) buf[100])[0] = rslt.getInt(61) ;
                ((int[]) buf[101])[0] = rslt.getInt(62) ;
                ((int[]) buf[102])[0] = rslt.getInt(63) ;
                ((int[]) buf[103])[0] = rslt.getInt(64) ;
                ((String[]) buf[104])[0] = rslt.getString(65, 20) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(65);
                ((short[]) buf[106])[0] = rslt.getShort(66) ;
                ((bool[]) buf[107])[0] = rslt.wasNull(66);
                ((short[]) buf[108])[0] = rslt.getShort(67) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(67);
                ((short[]) buf[110])[0] = rslt.getShort(68) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(68);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 54 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 55 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 56 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 57 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 58 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 59 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
       }
       getresults60( cursor, rslt, buf) ;
    }

    public void getresults60( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 60 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[5]);
                }
                stmt.SetParameter(4, (decimal)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                stmt.SetParameter(6, (String)parms[9]);
                stmt.SetParameter(7, (String)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(15, (short)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(16, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(18, (decimal)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(24, (bool)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(29, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(30, (short)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(31, (short)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[60]);
                }
                stmt.SetParameter(33, (bool)parms[61]);
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 34 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(34, (String)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 36 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(36, (short)parms[67]);
                }
                stmt.SetParameter(37, (bool)parms[68]);
                stmt.SetParameter(38, (int)parms[69]);
                stmt.SetParameter(39, (int)parms[70]);
                stmt.SetParameter(40, (int)parms[71]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[5]);
                }
                stmt.SetParameter(4, (decimal)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                stmt.SetParameter(6, (String)parms[9]);
                stmt.SetParameter(7, (String)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(15, (short)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(16, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(18, (decimal)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(24, (bool)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(29, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(30, (short)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(31, (short)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[60]);
                }
                stmt.SetParameter(33, (bool)parms[61]);
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 34 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(34, (String)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 36 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(36, (short)parms[67]);
                }
                stmt.SetParameter(37, (bool)parms[68]);
                stmt.SetParameter(38, (int)parms[69]);
                stmt.SetParameter(39, (int)parms[70]);
                stmt.SetParameter(40, (int)parms[71]);
                stmt.SetParameter(41, (int)parms[72]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 48 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 49 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 50 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                return;
             case 51 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 52 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 55 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 56 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 57 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 58 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 59 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters60( cursor, stmt, parms) ;
    }

    public void setparameters60( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 60 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
