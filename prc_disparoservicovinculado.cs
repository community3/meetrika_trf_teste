/*
               File: PRC_DisparoServicoVinculado
        Description: Disparo Servico Vinculado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:9.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_disparoservicovinculado : GXProcedure
   {
      public prc_disparoservicovinculado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_disparoservicovinculado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_Usuario_Codigo )
      {
         this.AV10ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV13Usuario_Codigo = aP1_Usuario_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_Usuario_Codigo )
      {
         prc_disparoservicovinculado objprc_disparoservicovinculado;
         objprc_disparoservicovinculado = new prc_disparoservicovinculado();
         objprc_disparoservicovinculado.AV10ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_disparoservicovinculado.AV13Usuario_Codigo = aP1_Usuario_Codigo;
         objprc_disparoservicovinculado.context.SetSubmitInitialConfig(context);
         objprc_disparoservicovinculado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_disparoservicovinculado);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_disparoservicovinculado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV71Ok = "ISEARCHOP";
         /* Using cursor P00773 */
         pr_default.execute(0, new Object[] {AV10ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00773_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00773_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00773_n484ContagemResultado_StatusDmn[0];
            A493ContagemResultado_DemandaFM = P00773_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00773_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00773_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00773_n457ContagemResultado_Demanda[0];
            A489ContagemResultado_SistemaCod = P00773_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00773_n489ContagemResultado_SistemaCod[0];
            A1044ContagemResultado_FncUsrCod = P00773_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = P00773_n1044ContagemResultado_FncUsrCod[0];
            A494ContagemResultado_Descricao = P00773_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00773_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = P00773_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = P00773_n514ContagemResultado_Observacao[0];
            A508ContagemResultado_Owner = P00773_A508ContagemResultado_Owner[0];
            A890ContagemResultado_Responsavel = P00773_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00773_n890ContagemResultado_Responsavel[0];
            A1389ContagemResultado_RdmnIssueId = P00773_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = P00773_n1389ContagemResultado_RdmnIssueId[0];
            A1553ContagemResultado_CntSrvCod = P00773_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00773_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00773_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00773_n602ContagemResultado_OSVinculada[0];
            A490ContagemResultado_ContratadaCod = P00773_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00773_n490ContagemResultado_ContratadaCod[0];
            A1326ContagemResultado_ContratadaTipoFab = P00773_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00773_n1326ContagemResultado_ContratadaTipoFab[0];
            A146Modulo_Codigo = P00773_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00773_n146Modulo_Codigo[0];
            A601ContagemResultado_Servico = P00773_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00773_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00773_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00773_n801ContagemResultado_ServicoSigla[0];
            A52Contratada_AreaTrabalhoCod = P00773_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00773_n52Contratada_AreaTrabalhoCod[0];
            A1765ContagemResultado_CodSrvSSVnc = P00773_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00773_n1765ContagemResultado_CodSrvSSVnc[0];
            A465ContagemResultado_Link = P00773_A465ContagemResultado_Link[0];
            n465ContagemResultado_Link = P00773_n465ContagemResultado_Link[0];
            A472ContagemResultado_DataEntrega = P00773_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00773_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00773_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00773_n912ContagemResultado_HoraEntrega[0];
            A684ContagemResultado_PFBFSUltima = P00773_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00773_A531ContagemResultado_StatusUltCnt[0];
            A684ContagemResultado_PFBFSUltima = P00773_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00773_A531ContagemResultado_StatusUltCnt[0];
            A601ContagemResultado_Servico = P00773_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00773_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00773_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00773_n801ContagemResultado_ServicoSigla[0];
            A1765ContagemResultado_CodSrvSSVnc = P00773_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00773_n1765ContagemResultado_CodSrvSSVnc[0];
            A1326ContagemResultado_ContratadaTipoFab = P00773_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00773_n1326ContagemResultado_ContratadaTipoFab[0];
            A52Contratada_AreaTrabalhoCod = P00773_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00773_n52Contratada_AreaTrabalhoCod[0];
            if ( StringUtil.StringSearch( AV71Ok, A484ContagemResultado_StatusDmn, 1) > 0 )
            {
               new prc_setdataprvpgm(context ).execute( ref  A456ContagemResultado_Codigo,  A484ContagemResultado_StatusDmn) ;
            }
            AV14ContagemResultado_Demanda = A493ContagemResultado_DemandaFM;
            AV67Referencia = A457ContagemResultado_Demanda;
            AV15ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
            AV17ContagemResultado_FncUsrCod = A1044ContagemResultado_FncUsrCod;
            AV33ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            AV40ContagemResultado_Observacao = A514ContagemResultado_Observacao;
            AV42ContagemResultado_Owner = A508ContagemResultado_Owner;
            AV51ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            AV47ContagemResultado_RdmnIssueId = A1389ContagemResultado_RdmnIssueId;
            AV48ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            AV53OSVinculada = A602ContagemResultado_OSVinculada;
            AV19ContagemResultado_ContratadaOrigemCod = A490ContagemResultado_ContratadaCod;
            if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
            {
               AV69PFB = A684ContagemResultado_PFBFSUltima;
            }
            else
            {
               AV69PFB = A684ContagemResultado_PFBFSUltima;
            }
            if ( A531ContagemResultado_StatusUltCnt == 5 )
            {
               AV32PFBFSdaOSVnc = AV69PFB;
            }
            AV16Modulo_Codigo = A146Modulo_Codigo;
            AV18Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            AV9StatusDemanda = A484ContagemResultado_StatusDmn;
            AV8Servico_Codigo = A601ContagemResultado_Servico;
            AV23Servico_Sigla = A801ContagemResultado_ServicoSigla;
            AV29AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            AV60CodSrvSSVnc = A1765ContagemResultado_CodSrvSSVnc;
            AV66Link = A465ContagemResultado_Link;
            AV72PrazoAtual = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV72PrazoAtual = DateTimeUtil.TAdd( AV72PrazoAtual, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV72PrazoAtual = DateTimeUtil.TAdd( AV72PrazoAtual, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            /* Using cursor P00774 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A892LogResponsavel_DemandaCod = P00774_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P00774_n892LogResponsavel_DemandaCod[0];
               A1130LogResponsavel_Status = P00774_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = P00774_n1130LogResponsavel_Status[0];
               A1797LogResponsavel_Codigo = P00774_A1797LogResponsavel_Codigo[0];
               AV59DoStatusDemanda = A1130LogResponsavel_Status;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV55ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( AV35WebSession.Get("ProximaEtapa"), "."));
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV55ContratoSrvVnc_Codigo ,
                                              A917ContratoSrvVnc_Codigo ,
                                              A915ContratoSrvVnc_CntSrvCod ,
                                              AV48ContratoServicos_Codigo ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              A1800ContratoSrvVnc_DoStatusDmn ,
                                              AV59DoStatusDemanda ,
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV9StatusDemanda },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         /* Using cursor P00775 */
         pr_default.execute(2, new Object[] {AV59DoStatusDemanda, AV9StatusDemanda, AV55ContratoSrvVnc_Codigo, AV48ContratoServicos_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A921ContratoSrvVnc_ServicoCod = P00775_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00775_n921ContratoSrvVnc_ServicoCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00775_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00775_n923ContratoSrvVnc_ServicoVncCod[0];
            A933ContratoSrvVnc_ContratoCod = P00775_A933ContratoSrvVnc_ContratoCod[0];
            n933ContratoSrvVnc_ContratoCod = P00775_n933ContratoSrvVnc_ContratoCod[0];
            A1453ContratoServicosVnc_Ativo = P00775_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00775_n1453ContratoServicosVnc_Ativo[0];
            A1084ContratoSrvVnc_StatusDmn = P00775_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00775_n1084ContratoSrvVnc_StatusDmn[0];
            A1800ContratoSrvVnc_DoStatusDmn = P00775_A1800ContratoSrvVnc_DoStatusDmn[0];
            n1800ContratoSrvVnc_DoStatusDmn = P00775_n1800ContratoSrvVnc_DoStatusDmn[0];
            A915ContratoSrvVnc_CntSrvCod = P00775_A915ContratoSrvVnc_CntSrvCod[0];
            A917ContratoSrvVnc_Codigo = P00775_A917ContratoSrvVnc_Codigo[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00775_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00775_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00775_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00775_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A1088ContratoSrvVnc_PrestadoraCod = P00775_A1088ContratoSrvVnc_PrestadoraCod[0];
            n1088ContratoSrvVnc_PrestadoraCod = P00775_n1088ContratoSrvVnc_PrestadoraCod[0];
            A1090ContratoSrvVnc_SemCusto = P00775_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00775_n1090ContratoSrvVnc_SemCusto[0];
            A1437ContratoServicosVnc_NaoClonaInfo = P00775_A1437ContratoServicosVnc_NaoClonaInfo[0];
            n1437ContratoServicosVnc_NaoClonaInfo = P00775_n1437ContratoServicosVnc_NaoClonaInfo[0];
            A922ContratoSrvVnc_ServicoSigla = P00775_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00775_n922ContratoSrvVnc_ServicoSigla[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00775_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00775_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1630ContratoSrvVnc_SrvVncTpDias = P00775_A1630ContratoSrvVnc_SrvVncTpDias[0];
            n1630ContratoSrvVnc_SrvVncTpDias = P00775_n1630ContratoSrvVnc_SrvVncTpDias[0];
            A1651ContratoSrvVnc_SrvVncPrzInc = P00775_A1651ContratoSrvVnc_SrvVncPrzInc[0];
            n1651ContratoSrvVnc_SrvVncPrzInc = P00775_n1651ContratoSrvVnc_SrvVncPrzInc[0];
            A1663ContratoSrvVnc_SrvVncStatus = P00775_A1663ContratoSrvVnc_SrvVncStatus[0];
            n1663ContratoSrvVnc_SrvVncStatus = P00775_n1663ContratoSrvVnc_SrvVncStatus[0];
            A1743ContratoSrvVnc_NovoStatusDmn = P00775_A1743ContratoSrvVnc_NovoStatusDmn[0];
            n1743ContratoSrvVnc_NovoStatusDmn = P00775_n1743ContratoSrvVnc_NovoStatusDmn[0];
            A1801ContratoSrvVnc_NovoRspDmn = P00775_A1801ContratoSrvVnc_NovoRspDmn[0];
            n1801ContratoSrvVnc_NovoRspDmn = P00775_n1801ContratoSrvVnc_NovoRspDmn[0];
            A1745ContratoSrvVnc_VincularCom = P00775_A1745ContratoSrvVnc_VincularCom[0];
            n1745ContratoSrvVnc_VincularCom = P00775_n1745ContratoSrvVnc_VincularCom[0];
            A1818ContratoServicosVnc_ClonarLink = P00775_A1818ContratoServicosVnc_ClonarLink[0];
            n1818ContratoServicosVnc_ClonarLink = P00775_n1818ContratoServicosVnc_ClonarLink[0];
            A1821ContratoSrvVnc_SrvVncRef = P00775_A1821ContratoSrvVnc_SrvVncRef[0];
            n1821ContratoSrvVnc_SrvVncRef = P00775_n1821ContratoSrvVnc_SrvVncRef[0];
            A1145ContratoServicosVnc_ClonaSrvOri = P00775_A1145ContratoServicosVnc_ClonaSrvOri[0];
            n1145ContratoServicosVnc_ClonaSrvOri = P00775_n1145ContratoServicosVnc_ClonaSrvOri[0];
            A1438ContratoServicosVnc_Gestor = P00775_A1438ContratoServicosVnc_Gestor[0];
            n1438ContratoServicosVnc_Gestor = P00775_n1438ContratoServicosVnc_Gestor[0];
            A1642ContratoSrvVnc_SrvVncVlrUndCnt = P00775_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            n1642ContratoSrvVnc_SrvVncVlrUndCnt = P00775_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            A116Contrato_ValorUnidadeContratacao = P00775_A116Contrato_ValorUnidadeContratacao[0];
            n116Contrato_ValorUnidadeContratacao = P00775_n116Contrato_ValorUnidadeContratacao[0];
            A1629ContratoSrvVnc_SrvVncTpVnc = P00775_A1629ContratoSrvVnc_SrvVncTpVnc[0];
            n1629ContratoSrvVnc_SrvVncTpVnc = P00775_n1629ContratoSrvVnc_SrvVncTpVnc[0];
            A921ContratoSrvVnc_ServicoCod = P00775_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00775_n921ContratoSrvVnc_ServicoCod[0];
            A933ContratoSrvVnc_ContratoCod = P00775_A933ContratoSrvVnc_ContratoCod[0];
            n933ContratoSrvVnc_ContratoCod = P00775_n933ContratoSrvVnc_ContratoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00775_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00775_n922ContratoSrvVnc_ServicoSigla[0];
            A116Contrato_ValorUnidadeContratacao = P00775_A116Contrato_ValorUnidadeContratacao[0];
            n116Contrato_ValorUnidadeContratacao = P00775_n116Contrato_ValorUnidadeContratacao[0];
            A923ContratoSrvVnc_ServicoVncCod = P00775_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00775_n923ContratoSrvVnc_ServicoVncCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00775_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00775_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A1630ContratoSrvVnc_SrvVncTpDias = P00775_A1630ContratoSrvVnc_SrvVncTpDias[0];
            n1630ContratoSrvVnc_SrvVncTpDias = P00775_n1630ContratoSrvVnc_SrvVncTpDias[0];
            A1651ContratoSrvVnc_SrvVncPrzInc = P00775_A1651ContratoSrvVnc_SrvVncPrzInc[0];
            n1651ContratoSrvVnc_SrvVncPrzInc = P00775_n1651ContratoSrvVnc_SrvVncPrzInc[0];
            A1642ContratoSrvVnc_SrvVncVlrUndCnt = P00775_A1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            n1642ContratoSrvVnc_SrvVncVlrUndCnt = P00775_n1642ContratoSrvVnc_SrvVncVlrUndCnt[0];
            A1629ContratoSrvVnc_SrvVncTpVnc = P00775_A1629ContratoSrvVnc_SrvVncTpVnc[0];
            n1629ContratoSrvVnc_SrvVncTpVnc = P00775_n1629ContratoSrvVnc_SrvVncTpVnc[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00775_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00775_n924ContratoSrvVnc_ServicoVncSigla[0];
            OV49PrazoInicio = AV49PrazoInicio;
            OV56VincularCom = AV56VincularCom;
            AV24ContratoSrvVnc_ServicoVncCod = A1589ContratoSrvVnc_SrvVncCntSrvCod;
            AV45Contrato_Codigo = A1628ContratoSrvVnc_SrvVncCntCod;
            AV25ContratoSrvVnc_PrestadoraCod = A1088ContratoSrvVnc_PrestadoraCod;
            AV26ContratoSrvVnc_SemCusto = A1090ContratoSrvVnc_SemCusto;
            AV39ClonaInfo = (bool)(!A1437ContratoServicosVnc_NaoClonaInfo);
            AV41DescricaoAutomatica = A924ContratoSrvVnc_ServicoVncSigla + " da OS de " + A922ContratoSrvVnc_ServicoSigla;
            AV43TipoDias = A1630ContratoSrvVnc_SrvVncTpDias;
            AV49PrazoInicio = A1651ContratoSrvVnc_SrvVncPrzInc;
            AV54Status = A1663ContratoSrvVnc_SrvVncStatus;
            AV52NovoStatusDmn = A1743ContratoSrvVnc_NovoStatusDmn;
            AV64NovoRspDmn = A1801ContratoSrvVnc_NovoRspDmn;
            AV56VincularCom = A1745ContratoSrvVnc_VincularCom;
            AV65ClonarLink = A1818ContratoServicosVnc_ClonarLink;
            AV68ContratoSrvVnc_SrvVncRef = A1821ContratoSrvVnc_SrvVncRef;
            AV74ContratoServicosVnc_ClonaSrvOri = A1145ContratoServicosVnc_ClonaSrvOri;
            if ( A1438ContratoServicosVnc_Gestor == 700000 )
            {
               AV42ContagemResultado_Owner = AV51ContagemResultado_Responsavel;
            }
            else if ( A1438ContratoServicosVnc_Gestor == 800000 )
            {
            }
            else if ( A1438ContratoServicosVnc_Gestor == 900000 )
            {
               /* Using cursor P00776 */
               pr_default.execute(3, new Object[] {n933ContratoSrvVnc_ContratoCod, A933ContratoSrvVnc_ContratoCod});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P00776_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P00776_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00776_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00776_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00776_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00776_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV42ContagemResultado_Owner = A1079ContratoGestor_UsuarioCod;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
            else if ( A1438ContratoServicosVnc_Gestor > 0 )
            {
               AV42ContagemResultado_Owner = A1438ContratoServicosVnc_Gestor;
            }
            if ( ( StringUtil.StrCmp(A1745ContratoSrvVnc_VincularCom, "O") == 0 ) && (0==AV60CodSrvSSVnc) )
            {
               GXt_int2 = AV53OSVinculada;
               new prc_ssorigem(context ).execute( ref  AV10ContagemResultado_Codigo, out  GXt_int2) ;
               AV53OSVinculada = GXt_int2;
               if ( A1821ContratoSrvVnc_SrvVncRef == 1 )
               {
                  /* Execute user subroutine: 'REFERENCIADAORIGEM' */
                  S191 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     this.cleanup();
                     if (true) return;
                  }
               }
            }
            else
            {
               AV53OSVinculada = AV10ContagemResultado_Codigo;
            }
            if ( ! AV26ContratoSrvVnc_SemCusto )
            {
               if ( ( A1642ContratoSrvVnc_SrvVncVlrUndCnt > Convert.ToDecimal( 0 )) )
               {
                  AV11ValorPF = A1642ContratoSrvVnc_SrvVncVlrUndCnt;
               }
               else
               {
                  AV11ValorPF = A116Contrato_ValorUnidadeContratacao;
               }
            }
            AV44ContagemResultado_TemDpnHmlg = (bool)(((StringUtil.StrCmp(A1629ContratoSrvVnc_SrvVncTpVnc, "C")==0)||(StringUtil.StrCmp(A1629ContratoSrvVnc_SrvVncTpVnc, "A")==0)));
            /* Execute user subroutine: 'CALCULARPRAZOENTREGA' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               this.cleanup();
               if (true) return;
            }
            Gx_msg = "Disparo gerado por " + AV23Servico_Sigla;
            if ( AV25ContratoSrvVnc_PrestadoraCod < 910000 )
            {
               AV83GXLvl115 = 0;
               /* Using cursor P00777 */
               pr_default.execute(4, new Object[] {AV10ContagemResultado_Codigo, AV25ContratoSrvVnc_PrestadoraCod, AV24ContratoSrvVnc_ServicoVncCod});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = P00777_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00777_n1553ContagemResultado_CntSrvCod[0];
                  A490ContagemResultado_ContratadaCod = P00777_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00777_n490ContagemResultado_ContratadaCod[0];
                  A602ContagemResultado_OSVinculada = P00777_A602ContagemResultado_OSVinculada[0];
                  n602ContagemResultado_OSVinculada = P00777_n602ContagemResultado_OSVinculada[0];
                  A456ContagemResultado_Codigo = P00777_A456ContagemResultado_Codigo[0];
                  AV83GXLvl115 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               if ( AV83GXLvl115 == 0 )
               {
                  Gx_msg = Gx_msg + " no status " + gxdomainstatusdemanda.getDescription(context,AV9StatusDemanda) + ".";
                  if ( new prc_temgestor(context).executeUdp( ref  AV45Contrato_Codigo) )
                  {
                     /* Execute user subroutine: 'NEWDEMANDA' */
                     S131 ();
                     if ( returnInSub )
                     {
                        pr_default.close(2);
                        this.cleanup();
                        if (true) return;
                     }
                  }
                  else
                  {
                     Gx_msg = StringUtil.StringReplace( Gx_msg, "Disparo gerado por ", "Disparo N�O GERADO por ");
                     Gx_msg = Gx_msg + " Prestadora sem Preposto/Gestor estabelecido.";
                     new prc_inslogresponsavel(context ).execute( ref  AV20NewCodigo,  0,  "NO",  "D",  AV42ContagemResultado_Owner,  0,  "",  "",  Gx_msg,  AV72PrazoAtual,  false) ;
                  }
               }
               if ( AV64NovoRspDmn > 900000 )
               {
                  /* Execute user subroutine: 'TRANSFERIR' */
                  S171 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     this.cleanup();
                     if (true) return;
                  }
               }
            }
            else
            {
               Gx_msg = Gx_msg + " de " + gxdomainstatusdemanda.getDescription(context,AV59DoStatusDemanda) + " para " + gxdomainstatusdemanda.getDescription(context,AV9StatusDemanda) + ".";
               /* Execute user subroutine: 'TRANSFERIR' */
               S171 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  this.cleanup();
                  if (true) return;
               }
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52NovoStatusDmn)) )
            {
               /* Execute user subroutine: 'ALTERASTATUS' */
               S161 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  this.cleanup();
                  if (true) return;
               }
               if ( AV57SSVinculada > 0 )
               {
                  AV53OSVinculada = AV57SSVinculada;
                  /* Execute user subroutine: 'ALTERASTATUS' */
                  S161 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     this.cleanup();
                     if (true) return;
                  }
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV35WebSession.Remove("Codigos");
         AV35WebSession.Remove("ProximaEtapa");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CALCULARPRAZOENTREGA' Routine */
         GXt_int3 = AV30Dias;
         new prc_diasparaentrega(context ).execute( ref  AV24ContratoSrvVnc_ServicoVncCod,  AV32PFBFSdaOSVnc,  0, out  GXt_int3) ;
         AV30Dias = GXt_int3;
         AV21PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         if ( AV49PrazoInicio == 2 )
         {
            new prc_datadeinicioutil(context ).execute( ref  AV21PrazoEntrega) ;
         }
         GXt_dtime4 = AV21PrazoEntrega;
         new prc_adddiasuteis(context ).execute(  AV21PrazoEntrega,  AV30Dias,  AV43TipoDias, out  GXt_dtime4) ;
         AV21PrazoEntrega = GXt_dtime4;
         /* Execute user subroutine: 'ADDHORASENTREGA' */
         S121 ();
         if (returnInSub) return;
      }

      protected void S121( )
      {
         /* 'ADDHORASENTREGA' Routine */
         new prc_gethorasentrega(context ).execute(  AV29AreaTrabalho_Codigo,  0,  AV24ContratoSrvVnc_ServicoVncCod, out  AV27Hours, out  AV28Minutes) ;
         AV21PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV21PrazoEntrega) ) ;
         AV21PrazoEntrega = DateTimeUtil.TAdd( AV21PrazoEntrega, 3600*(AV27Hours));
         AV21PrazoEntrega = DateTimeUtil.TAdd( AV21PrazoEntrega, 60*(AV28Minutes));
      }

      protected void S131( )
      {
         /* 'NEWDEMANDA' Routine */
         if ( ! ( StringUtil.StrCmp(A1744ContratoSrvVnc_PrestadoraTpFab, "I") == 0 ) )
         {
            AV62Contratada.Load(AV25ContratoSrvVnc_PrestadoraCod);
            AV62Contratada.gxTpr_Contratada_os = (int)(AV62Contratada.gxTpr_Contratada_os+1);
            AV63DemandaFM = StringUtil.Trim( StringUtil.Str( (decimal)(AV62Contratada.gxTpr_Contratada_os), 8, 0));
            AV62Contratada.Save();
         }
         /*
            INSERT RECORD ON TABLE ContagemResultado

         */
         A471ContagemResultado_DataDmn = DateTimeUtil.ServerDate( context, "DEFAULT");
         A484ContagemResultado_StatusDmn = AV54Status;
         n484ContagemResultado_StatusDmn = false;
         A1553ContagemResultado_CntSrvCod = AV24ContratoSrvVnc_ServicoVncCod;
         n1553ContagemResultado_CntSrvCod = false;
         A490ContagemResultado_ContratadaCod = AV25ContratoSrvVnc_PrestadoraCod;
         n490ContagemResultado_ContratadaCod = false;
         A493ContagemResultado_DemandaFM = AV63DemandaFM;
         n493ContagemResultado_DemandaFM = false;
         A512ContagemResultado_ValorPF = AV11ValorPF;
         n512ContagemResultado_ValorPF = false;
         A602ContagemResultado_OSVinculada = AV53OSVinculada;
         n602ContagemResultado_OSVinculada = false;
         if ( AV39ClonaInfo )
         {
            A494ContagemResultado_Descricao = AV33ContagemResultado_Descricao;
            n494ContagemResultado_Descricao = false;
            A514ContagemResultado_Observacao = AV40ContagemResultado_Observacao;
            n514ContagemResultado_Observacao = false;
         }
         else
         {
            A494ContagemResultado_Descricao = AV41DescricaoAutomatica;
            n494ContagemResultado_Descricao = false;
         }
         if ( (0==AV15ContagemResultado_SistemaCod) )
         {
            A489ContagemResultado_SistemaCod = 0;
            n489ContagemResultado_SistemaCod = false;
            n489ContagemResultado_SistemaCod = true;
         }
         else
         {
            A489ContagemResultado_SistemaCod = AV15ContagemResultado_SistemaCod;
            n489ContagemResultado_SistemaCod = false;
         }
         if ( (0==AV16Modulo_Codigo) )
         {
            A146Modulo_Codigo = 0;
            n146Modulo_Codigo = false;
            n146Modulo_Codigo = true;
         }
         else
         {
            A146Modulo_Codigo = AV16Modulo_Codigo;
            n146Modulo_Codigo = false;
         }
         if ( (0==AV17ContagemResultado_FncUsrCod) )
         {
            A1044ContagemResultado_FncUsrCod = 0;
            n1044ContagemResultado_FncUsrCod = false;
            n1044ContagemResultado_FncUsrCod = true;
         }
         else
         {
            A1044ContagemResultado_FncUsrCod = AV17ContagemResultado_FncUsrCod;
            n1044ContagemResultado_FncUsrCod = false;
         }
         A805ContagemResultado_ContratadaOrigemCod = AV19ContagemResultado_ContratadaOrigemCod;
         n805ContagemResultado_ContratadaOrigemCod = false;
         if ( AV13Usuario_Codigo > 1 )
         {
            A454ContagemResultado_ContadorFSCod = AV13Usuario_Codigo;
            n454ContagemResultado_ContadorFSCod = false;
         }
         else
         {
            A454ContagemResultado_ContadorFSCod = 0;
            n454ContagemResultado_ContadorFSCod = false;
            n454ContagemResultado_ContadorFSCod = true;
         }
         if ( AV65ClonarLink )
         {
            A465ContagemResultado_Link = AV66Link;
            n465ContagemResultado_Link = false;
         }
         if ( AV68ContratoSrvVnc_SrvVncRef == 1 )
         {
            A457ContagemResultado_Demanda = AV67Referencia;
            n457ContagemResultado_Demanda = false;
         }
         else
         {
            A457ContagemResultado_Demanda = AV14ContagemResultado_Demanda;
            n457ContagemResultado_Demanda = false;
         }
         A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV21PrazoEntrega);
         n472ContagemResultado_DataEntrega = false;
         A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV21PrazoEntrega);
         n912ContagemResultado_HoraEntrega = false;
         A1351ContagemResultado_DataPrevista = AV21PrazoEntrega;
         n1351ContagemResultado_DataPrevista = false;
         A1227ContagemResultado_PrazoInicialDias = AV30Dias;
         n1227ContagemResultado_PrazoInicialDias = false;
         A1389ContagemResultado_RdmnIssueId = AV47ContagemResultado_RdmnIssueId;
         n1389ContagemResultado_RdmnIssueId = false;
         A1583ContagemResultado_TipoRegistro = 1;
         A1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         A508ContagemResultado_Owner = AV42ContagemResultado_Owner;
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A890ContagemResultado_Responsavel = 0;
         n890ContagemResultado_Responsavel = false;
         n890ContagemResultado_Responsavel = true;
         A1043ContagemResultado_LiqLogCod = 0;
         n1043ContagemResultado_LiqLogCod = false;
         n1043ContagemResultado_LiqLogCod = true;
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         n468ContagemResultado_NaoCnfDmnCod = true;
         A597ContagemResultado_LoteAceiteCod = 0;
         n597ContagemResultado_LoteAceiteCod = false;
         n597ContagemResultado_LoteAceiteCod = true;
         A1636ContagemResultado_ServicoSS = 0;
         n1636ContagemResultado_ServicoSS = false;
         n1636ContagemResultado_ServicoSS = true;
         AV73ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         /* Using cursor P00778 */
         pr_default.execute(5, new Object[] {A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1389ContagemResultado_RdmnIssueId, A1389ContagemResultado_RdmnIssueId, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
         A456ContagemResultado_Codigo = P00778_A456ContagemResultado_Codigo[0];
         pr_default.close(5);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         if ( (pr_default.getStatus(5) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV20NewCodigo = A456ContagemResultado_Codigo;
         AV35WebSession.Set("&NewCodigo", context.localUtil.Format( (decimal)(AV20NewCodigo), "ZZZZZ9"));
         AV35WebSession.Set("&ContagemResultado_OsFsOsFm", StringUtil.Trim( AV73ContagemResultado_OsFsOsFm));
         new prc_setdataprvpgm(context ).execute( ref  AV20NewCodigo,  AV54Status) ;
         if ( AV44ContagemResultado_TemDpnHmlg )
         {
            /* Using cursor P00779 */
            pr_default.execute(6, new Object[] {AV10ContagemResultado_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A456ContagemResultado_Codigo = P00779_A456ContagemResultado_Codigo[0];
               A1457ContagemResultado_TemDpnHmlg = P00779_A1457ContagemResultado_TemDpnHmlg[0];
               n1457ContagemResultado_TemDpnHmlg = P00779_n1457ContagemResultado_TemDpnHmlg[0];
               A1457ContagemResultado_TemDpnHmlg = true;
               n1457ContagemResultado_TemDpnHmlg = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P007710 */
               pr_default.execute(7, new Object[] {n1457ContagemResultado_TemDpnHmlg, A1457ContagemResultado_TemDpnHmlg, A456ContagemResultado_Codigo});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P007711 */
               pr_default.execute(8, new Object[] {n1457ContagemResultado_TemDpnHmlg, A1457ContagemResultado_TemDpnHmlg, A456ContagemResultado_Codigo});
               pr_default.close(8);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
         }
         new prc_inslogresponsavel(context ).execute( ref  AV20NewCodigo,  0,  "M",  "D",  AV42ContagemResultado_Owner,  0,  "",  AV54Status,  Gx_msg,  AV21PrazoEntrega,  true) ;
         if ( AV39ClonaInfo )
         {
            /* Execute user subroutine: 'VINCULARANEXOS' */
            S141 ();
            if (returnInSub) return;
         }
         if ( AV74ContratoServicosVnc_ClonaSrvOri )
         {
            /* Execute user subroutine: 'CLONA.RESULTADO.ORIGEM' */
            S151 ();
            if (returnInSub) return;
         }
      }

      protected void S141( )
      {
         /* 'VINCULARANEXOS' Routine */
         /* Using cursor P007712 */
         pr_default.execute(9, new Object[] {AV10ContagemResultado_Codigo});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A1110AnexoDe_Tabela = P007712_A1110AnexoDe_Tabela[0];
            A1109AnexoDe_Id = P007712_A1109AnexoDe_Id[0];
            A1106Anexo_Codigo = P007712_A1106Anexo_Codigo[0];
            AV34Anexo_Codigo.Add(A1106Anexo_Codigo, 0);
            pr_default.readNext(9);
         }
         pr_default.close(9);
         if ( AV34Anexo_Codigo.Count > 0 )
         {
            AV37Codigos.Add(AV20NewCodigo, 0);
            AV35WebSession.Set("Codigos", AV37Codigos.ToXml(false, true, "Collection", ""));
            AV35WebSession.Set("CodigosAnexos", AV34Anexo_Codigo.ToXml(false, true, "Collection", ""));
            new prc_vincularanexoscompartilhadoscomos(context ).execute( ) ;
         }
      }

      protected void S161( )
      {
         /* 'ALTERASTATUS' Routine */
         /* Using cursor P007713 */
         pr_default.execute(10, new Object[] {AV53OSVinculada});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A456ContagemResultado_Codigo = P007713_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P007713_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P007713_n484ContagemResultado_StatusDmn[0];
            A1636ContagemResultado_ServicoSS = P007713_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = P007713_n1636ContagemResultado_ServicoSS[0];
            A602ContagemResultado_OSVinculada = P007713_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P007713_n602ContagemResultado_OSVinculada[0];
            A484ContagemResultado_StatusDmn = AV52NovoStatusDmn;
            n484ContagemResultado_StatusDmn = false;
            if ( P007713_n1636ContagemResultado_ServicoSS[0] )
            {
               new prc_setdataprvpgm(context ).execute( ref  AV53OSVinculada,  AV52NovoStatusDmn) ;
            }
            AV57SSVinculada = A602ContagemResultado_OSVinculada;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P007714 */
            pr_default.execute(11, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P007715 */
            pr_default.execute(12, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(10);
      }

      protected void S171( )
      {
         /* 'TRANSFERIR' Routine */
         /* Using cursor P007716 */
         pr_default.execute(13, new Object[] {AV53OSVinculada});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A456ContagemResultado_Codigo = P007716_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P007716_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P007716_n484ContagemResultado_StatusDmn[0];
            A602ContagemResultado_OSVinculada = P007716_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P007716_n602ContagemResultado_OSVinculada[0];
            A890ContagemResultado_Responsavel = P007716_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P007716_n890ContagemResultado_Responsavel[0];
            A508ContagemResultado_Owner = P007716_A508ContagemResultado_Owner[0];
            A484ContagemResultado_StatusDmn = AV52NovoStatusDmn;
            n484ContagemResultado_StatusDmn = false;
            if ( ( AV25ContratoSrvVnc_PrestadoraCod == 910000 ) || ( AV64NovoRspDmn == 910000 ) )
            {
               AV57SSVinculada = A602ContagemResultado_OSVinculada;
               /* Execute user subroutine: 'CRIADORSS' */
               S1811 ();
               if ( returnInSub )
               {
                  pr_default.close(13);
                  returnInSub = true;
                  if (true) return;
               }
               A890ContagemResultado_Responsavel = AV51ContagemResultado_Responsavel;
               n890ContagemResultado_Responsavel = false;
            }
            else if ( ( AV25ContratoSrvVnc_PrestadoraCod == 920000 ) || ( AV64NovoRspDmn == 920000 ) )
            {
               AV51ContagemResultado_Responsavel = A508ContagemResultado_Owner;
               A890ContagemResultado_Responsavel = AV51ContagemResultado_Responsavel;
               n890ContagemResultado_Responsavel = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P007717 */
            pr_default.execute(14, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P007718 */
            pr_default.execute(15, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(15);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(13);
         new prc_inslogresponsavel(context ).execute( ref  AV53OSVinculada,  AV51ContagemResultado_Responsavel,  "M",  "D",  AV42ContagemResultado_Owner,  0,  AV9StatusDemanda,  AV52NovoStatusDmn,  Gx_msg,  AV21PrazoEntrega,  true) ;
      }

      protected void S1811( )
      {
         /* 'CRIADORSS' Routine */
         AV61ContagemResultado.Load(AV57SSVinculada);
         AV51ContagemResultado_Responsavel = AV61ContagemResultado.gxTpr_Contagemresultado_owner;
      }

      protected void S191( )
      {
         /* 'REFERENCIADAORIGEM' Routine */
         /* Using cursor P007719 */
         pr_default.execute(16, new Object[] {AV53OSVinculada});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A456ContagemResultado_Codigo = P007719_A456ContagemResultado_Codigo[0];
            A457ContagemResultado_Demanda = P007719_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P007719_n457ContagemResultado_Demanda[0];
            AV67Referencia = A457ContagemResultado_Demanda;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(16);
      }

      protected void S151( )
      {
         /* 'CLONA.RESULTADO.ORIGEM' Routine */
         /* Using cursor P007720 */
         pr_default.execute(17, new Object[] {AV10ContagemResultado_Codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A456ContagemResultado_Codigo = P007720_A456ContagemResultado_Codigo[0];
            A1756ContagemResultado_NvlCnt = P007720_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = P007720_n1756ContagemResultado_NvlCnt[0];
            A901ContagemResultadoContagens_Prazo = P007720_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = P007720_n901ContagemResultadoContagens_Prazo[0];
            A854ContagemResultado_TipoPla = P007720_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = P007720_n854ContagemResultado_TipoPla[0];
            A853ContagemResultado_NomePla = P007720_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = P007720_n853ContagemResultado_NomePla[0];
            A833ContagemResultado_CstUntPrd = P007720_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = P007720_n833ContagemResultado_CstUntPrd[0];
            A800ContagemResultado_Deflator = P007720_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P007720_n800ContagemResultado_Deflator[0];
            A517ContagemResultado_Ultima = P007720_A517ContagemResultado_Ultima[0];
            A482ContagemResultadoContagens_Esforco = P007720_A482ContagemResultadoContagens_Esforco[0];
            A483ContagemResultado_StatusCnt = P007720_A483ContagemResultado_StatusCnt[0];
            A469ContagemResultado_NaoCnfCntCod = P007720_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = P007720_n469ContagemResultado_NaoCnfCntCod[0];
            A470ContagemResultado_ContadorFMCod = P007720_A470ContagemResultado_ContadorFMCod[0];
            A463ContagemResultado_ParecerTcn = P007720_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = P007720_n463ContagemResultado_ParecerTcn[0];
            A462ContagemResultado_Divergencia = P007720_A462ContagemResultado_Divergencia[0];
            A461ContagemResultado_PFLFM = P007720_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P007720_n461ContagemResultado_PFLFM[0];
            A460ContagemResultado_PFBFM = P007720_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P007720_n460ContagemResultado_PFBFM[0];
            A459ContagemResultado_PFLFS = P007720_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P007720_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P007720_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P007720_n458ContagemResultado_PFBFS[0];
            A481ContagemResultado_TimeCnt = P007720_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = P007720_n481ContagemResultado_TimeCnt[0];
            A511ContagemResultado_HoraCnt = P007720_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P007720_A473ContagemResultado_DataCnt[0];
            A852ContagemResultado_Planilha = P007720_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = P007720_n852ContagemResultado_Planilha[0];
            /*
               INSERT RECORD ON TABLE ContagemResultadoContagens

            */
            W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            W481ContagemResultado_TimeCnt = A481ContagemResultado_TimeCnt;
            n481ContagemResultado_TimeCnt = false;
            W901ContagemResultadoContagens_Prazo = A901ContagemResultadoContagens_Prazo;
            n901ContagemResultadoContagens_Prazo = false;
            W458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            W459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            W460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            W461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            W462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            W463ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            W470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            W483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            W482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
            W517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
            W800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            W833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
            n833ContagemResultado_CstUntPrd = false;
            W852ContagemResultado_Planilha = A852ContagemResultado_Planilha;
            n852ContagemResultado_Planilha = false;
            W853ContagemResultado_NomePla = A853ContagemResultado_NomePla;
            n853ContagemResultado_NomePla = false;
            W854ContagemResultado_TipoPla = A854ContagemResultado_TipoPla;
            n854ContagemResultado_TipoPla = false;
            W1756ContagemResultado_NvlCnt = A1756ContagemResultado_NvlCnt;
            n1756ContagemResultado_NvlCnt = false;
            A456ContagemResultado_Codigo = AV20NewCodigo;
            n481ContagemResultado_TimeCnt = false;
            n901ContagemResultadoContagens_Prazo = false;
            n458ContagemResultado_PFBFS = false;
            n459ContagemResultado_PFLFS = false;
            n460ContagemResultado_PFBFM = false;
            n461ContagemResultado_PFLFM = false;
            n463ContagemResultado_ParecerTcn = false;
            n800ContagemResultado_Deflator = false;
            n833ContagemResultado_CstUntPrd = false;
            n852ContagemResultado_Planilha = false;
            n853ContagemResultado_NomePla = false;
            n854ContagemResultado_TipoPla = false;
            n1756ContagemResultado_NvlCnt = false;
            /* Using cursor P007721 */
            A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
            n854ContagemResultado_TipoPla = false;
            pr_default.execute(18, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt});
            pr_default.close(18);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if ( (pr_default.getStatus(18) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
            A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
            A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
            A481ContagemResultado_TimeCnt = W481ContagemResultado_TimeCnt;
            n481ContagemResultado_TimeCnt = false;
            A901ContagemResultadoContagens_Prazo = W901ContagemResultadoContagens_Prazo;
            n901ContagemResultadoContagens_Prazo = false;
            A458ContagemResultado_PFBFS = W458ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            A459ContagemResultado_PFLFS = W459ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            A460ContagemResultado_PFBFM = W460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A461ContagemResultado_PFLFM = W461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            A462ContagemResultado_Divergencia = W462ContagemResultado_Divergencia;
            A463ContagemResultado_ParecerTcn = W463ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            A470ContagemResultado_ContadorFMCod = W470ContagemResultado_ContadorFMCod;
            A483ContagemResultado_StatusCnt = W483ContagemResultado_StatusCnt;
            A482ContagemResultadoContagens_Esforco = W482ContagemResultadoContagens_Esforco;
            A517ContagemResultado_Ultima = W517ContagemResultado_Ultima;
            A800ContagemResultado_Deflator = W800ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            A833ContagemResultado_CstUntPrd = W833ContagemResultado_CstUntPrd;
            n833ContagemResultado_CstUntPrd = false;
            A852ContagemResultado_Planilha = W852ContagemResultado_Planilha;
            n852ContagemResultado_Planilha = false;
            A853ContagemResultado_NomePla = W853ContagemResultado_NomePla;
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = W854ContagemResultado_TipoPla;
            n854ContagemResultado_TipoPla = false;
            A1756ContagemResultado_NvlCnt = W1756ContagemResultado_NvlCnt;
            n1756ContagemResultado_NvlCnt = false;
            /* End Insert */
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DisparoServicoVinculado");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV71Ok = "";
         scmdbuf = "";
         P00773_A456ContagemResultado_Codigo = new int[1] ;
         P00773_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00773_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00773_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00773_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00773_A457ContagemResultado_Demanda = new String[] {""} ;
         P00773_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00773_A489ContagemResultado_SistemaCod = new int[1] ;
         P00773_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00773_A1044ContagemResultado_FncUsrCod = new int[1] ;
         P00773_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         P00773_A494ContagemResultado_Descricao = new String[] {""} ;
         P00773_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00773_A514ContagemResultado_Observacao = new String[] {""} ;
         P00773_n514ContagemResultado_Observacao = new bool[] {false} ;
         P00773_A508ContagemResultado_Owner = new int[1] ;
         P00773_A890ContagemResultado_Responsavel = new int[1] ;
         P00773_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00773_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         P00773_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         P00773_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00773_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00773_A602ContagemResultado_OSVinculada = new int[1] ;
         P00773_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00773_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00773_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00773_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00773_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00773_A146Modulo_Codigo = new int[1] ;
         P00773_n146Modulo_Codigo = new bool[] {false} ;
         P00773_A601ContagemResultado_Servico = new int[1] ;
         P00773_n601ContagemResultado_Servico = new bool[] {false} ;
         P00773_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00773_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00773_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00773_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00773_A1765ContagemResultado_CodSrvSSVnc = new int[1] ;
         P00773_n1765ContagemResultado_CodSrvSSVnc = new bool[] {false} ;
         P00773_A465ContagemResultado_Link = new String[] {""} ;
         P00773_n465ContagemResultado_Link = new bool[] {false} ;
         P00773_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00773_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00773_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00773_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00773_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00773_A531ContagemResultado_StatusUltCnt = new short[1] ;
         A484ContagemResultado_StatusDmn = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A494ContagemResultado_Descricao = "";
         A514ContagemResultado_Observacao = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A801ContagemResultado_ServicoSigla = "";
         A465ContagemResultado_Link = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV14ContagemResultado_Demanda = "";
         AV67Referencia = "";
         AV33ContagemResultado_Descricao = "";
         AV40ContagemResultado_Observacao = "";
         AV9StatusDemanda = "";
         AV23Servico_Sigla = "";
         AV66Link = "";
         AV72PrazoAtual = (DateTime)(DateTime.MinValue);
         P00774_A892LogResponsavel_DemandaCod = new int[1] ;
         P00774_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00774_A1130LogResponsavel_Status = new String[] {""} ;
         P00774_n1130LogResponsavel_Status = new bool[] {false} ;
         P00774_A1797LogResponsavel_Codigo = new long[1] ;
         A1130LogResponsavel_Status = "";
         AV59DoStatusDemanda = "";
         AV35WebSession = context.GetSession();
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         P00775_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00775_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00775_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00775_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00775_A933ContratoSrvVnc_ContratoCod = new int[1] ;
         P00775_n933ContratoSrvVnc_ContratoCod = new bool[] {false} ;
         P00775_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00775_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00775_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00775_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00775_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         P00775_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         P00775_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00775_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00775_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00775_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00775_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00775_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00775_A1088ContratoSrvVnc_PrestadoraCod = new int[1] ;
         P00775_n1088ContratoSrvVnc_PrestadoraCod = new bool[] {false} ;
         P00775_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00775_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00775_A1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         P00775_n1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         P00775_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00775_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00775_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00775_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00775_A1630ContratoSrvVnc_SrvVncTpDias = new String[] {""} ;
         P00775_n1630ContratoSrvVnc_SrvVncTpDias = new bool[] {false} ;
         P00775_A1651ContratoSrvVnc_SrvVncPrzInc = new short[1] ;
         P00775_n1651ContratoSrvVnc_SrvVncPrzInc = new bool[] {false} ;
         P00775_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         P00775_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         P00775_A1743ContratoSrvVnc_NovoStatusDmn = new String[] {""} ;
         P00775_n1743ContratoSrvVnc_NovoStatusDmn = new bool[] {false} ;
         P00775_A1801ContratoSrvVnc_NovoRspDmn = new int[1] ;
         P00775_n1801ContratoSrvVnc_NovoRspDmn = new bool[] {false} ;
         P00775_A1745ContratoSrvVnc_VincularCom = new String[] {""} ;
         P00775_n1745ContratoSrvVnc_VincularCom = new bool[] {false} ;
         P00775_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00775_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00775_A1821ContratoSrvVnc_SrvVncRef = new short[1] ;
         P00775_n1821ContratoSrvVnc_SrvVncRef = new bool[] {false} ;
         P00775_A1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         P00775_n1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         P00775_A1438ContratoServicosVnc_Gestor = new int[1] ;
         P00775_n1438ContratoServicosVnc_Gestor = new bool[] {false} ;
         P00775_A1642ContratoSrvVnc_SrvVncVlrUndCnt = new decimal[1] ;
         P00775_n1642ContratoSrvVnc_SrvVncVlrUndCnt = new bool[] {false} ;
         P00775_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00775_n116Contrato_ValorUnidadeContratacao = new bool[] {false} ;
         P00775_A1629ContratoSrvVnc_SrvVncTpVnc = new String[] {""} ;
         P00775_n1629ContratoSrvVnc_SrvVncTpVnc = new bool[] {false} ;
         A922ContratoSrvVnc_ServicoSigla = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         A1630ContratoSrvVnc_SrvVncTpDias = "";
         A1663ContratoSrvVnc_SrvVncStatus = "";
         A1743ContratoSrvVnc_NovoStatusDmn = "";
         A1745ContratoSrvVnc_VincularCom = "";
         A1629ContratoSrvVnc_SrvVncTpVnc = "";
         AV49PrazoInicio = 1;
         OV56VincularCom = "";
         AV56VincularCom = "D";
         AV41DescricaoAutomatica = "";
         AV43TipoDias = "";
         AV54Status = "";
         AV52NovoStatusDmn = "";
         P00776_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00776_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00776_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00776_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         Gx_msg = "";
         P00777_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00777_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00777_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00777_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00777_A602ContagemResultado_OSVinculada = new int[1] ;
         P00777_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00777_A456ContagemResultado_Codigo = new int[1] ;
         AV21PrazoEntrega = (DateTime)(DateTime.MinValue);
         GXt_dtime4 = (DateTime)(DateTime.MinValue);
         A1744ContratoSrvVnc_PrestadoraTpFab = "";
         AV62Contratada = new SdtContratada(context);
         AV63DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         AV73ContagemResultado_OsFsOsFm = "";
         P00778_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         P00779_A456ContagemResultado_Codigo = new int[1] ;
         P00779_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00779_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P007712_A1110AnexoDe_Tabela = new int[1] ;
         P007712_A1109AnexoDe_Id = new int[1] ;
         P007712_A1106Anexo_Codigo = new int[1] ;
         AV34Anexo_Codigo = new GxSimpleCollection();
         AV37Codigos = new GxSimpleCollection();
         P007713_A456ContagemResultado_Codigo = new int[1] ;
         P007713_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P007713_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P007713_A1636ContagemResultado_ServicoSS = new int[1] ;
         P007713_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P007713_A602ContagemResultado_OSVinculada = new int[1] ;
         P007713_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P007716_A456ContagemResultado_Codigo = new int[1] ;
         P007716_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P007716_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P007716_A602ContagemResultado_OSVinculada = new int[1] ;
         P007716_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P007716_A890ContagemResultado_Responsavel = new int[1] ;
         P007716_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P007716_A508ContagemResultado_Owner = new int[1] ;
         AV61ContagemResultado = new SdtContagemResultado(context);
         P007719_A456ContagemResultado_Codigo = new int[1] ;
         P007719_A457ContagemResultado_Demanda = new String[] {""} ;
         P007719_n457ContagemResultado_Demanda = new bool[] {false} ;
         P007720_A456ContagemResultado_Codigo = new int[1] ;
         P007720_A1756ContagemResultado_NvlCnt = new short[1] ;
         P007720_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         P007720_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         P007720_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         P007720_A854ContagemResultado_TipoPla = new String[] {""} ;
         P007720_n854ContagemResultado_TipoPla = new bool[] {false} ;
         P007720_A853ContagemResultado_NomePla = new String[] {""} ;
         P007720_n853ContagemResultado_NomePla = new bool[] {false} ;
         P007720_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P007720_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         P007720_A800ContagemResultado_Deflator = new decimal[1] ;
         P007720_n800ContagemResultado_Deflator = new bool[] {false} ;
         P007720_A517ContagemResultado_Ultima = new bool[] {false} ;
         P007720_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P007720_A483ContagemResultado_StatusCnt = new short[1] ;
         P007720_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P007720_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P007720_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P007720_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P007720_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P007720_A462ContagemResultado_Divergencia = new decimal[1] ;
         P007720_A461ContagemResultado_PFLFM = new decimal[1] ;
         P007720_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P007720_A460ContagemResultado_PFBFM = new decimal[1] ;
         P007720_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P007720_A459ContagemResultado_PFLFS = new decimal[1] ;
         P007720_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P007720_A458ContagemResultado_PFBFS = new decimal[1] ;
         P007720_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P007720_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         P007720_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         P007720_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P007720_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P007720_A852ContagemResultado_Planilha = new String[] {""} ;
         P007720_n852ContagemResultado_Planilha = new bool[] {false} ;
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         A854ContagemResultado_TipoPla = "";
         A853ContagemResultado_NomePla = "";
         A463ContagemResultado_ParecerTcn = "";
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A852ContagemResultado_Planilha = "";
         W473ContagemResultado_DataCnt = DateTime.MinValue;
         W511ContagemResultado_HoraCnt = "";
         W481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         W901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         W463ContagemResultado_ParecerTcn = "";
         W852ContagemResultado_Planilha = "";
         W853ContagemResultado_NomePla = "";
         W854ContagemResultado_TipoPla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_disparoservicovinculado__default(),
            new Object[][] {
                new Object[] {
               P00773_A456ContagemResultado_Codigo, P00773_A484ContagemResultado_StatusDmn, P00773_n484ContagemResultado_StatusDmn, P00773_A493ContagemResultado_DemandaFM, P00773_n493ContagemResultado_DemandaFM, P00773_A457ContagemResultado_Demanda, P00773_n457ContagemResultado_Demanda, P00773_A489ContagemResultado_SistemaCod, P00773_n489ContagemResultado_SistemaCod, P00773_A1044ContagemResultado_FncUsrCod,
               P00773_n1044ContagemResultado_FncUsrCod, P00773_A494ContagemResultado_Descricao, P00773_n494ContagemResultado_Descricao, P00773_A514ContagemResultado_Observacao, P00773_n514ContagemResultado_Observacao, P00773_A508ContagemResultado_Owner, P00773_A890ContagemResultado_Responsavel, P00773_n890ContagemResultado_Responsavel, P00773_A1389ContagemResultado_RdmnIssueId, P00773_n1389ContagemResultado_RdmnIssueId,
               P00773_A1553ContagemResultado_CntSrvCod, P00773_n1553ContagemResultado_CntSrvCod, P00773_A602ContagemResultado_OSVinculada, P00773_n602ContagemResultado_OSVinculada, P00773_A490ContagemResultado_ContratadaCod, P00773_n490ContagemResultado_ContratadaCod, P00773_A1326ContagemResultado_ContratadaTipoFab, P00773_n1326ContagemResultado_ContratadaTipoFab, P00773_A146Modulo_Codigo, P00773_n146Modulo_Codigo,
               P00773_A601ContagemResultado_Servico, P00773_n601ContagemResultado_Servico, P00773_A801ContagemResultado_ServicoSigla, P00773_n801ContagemResultado_ServicoSigla, P00773_A52Contratada_AreaTrabalhoCod, P00773_n52Contratada_AreaTrabalhoCod, P00773_A1765ContagemResultado_CodSrvSSVnc, P00773_n1765ContagemResultado_CodSrvSSVnc, P00773_A465ContagemResultado_Link, P00773_n465ContagemResultado_Link,
               P00773_A472ContagemResultado_DataEntrega, P00773_n472ContagemResultado_DataEntrega, P00773_A912ContagemResultado_HoraEntrega, P00773_n912ContagemResultado_HoraEntrega, P00773_A684ContagemResultado_PFBFSUltima, P00773_A531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               P00774_A892LogResponsavel_DemandaCod, P00774_n892LogResponsavel_DemandaCod, P00774_A1130LogResponsavel_Status, P00774_n1130LogResponsavel_Status, P00774_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P00775_A921ContratoSrvVnc_ServicoCod, P00775_n921ContratoSrvVnc_ServicoCod, P00775_A923ContratoSrvVnc_ServicoVncCod, P00775_n923ContratoSrvVnc_ServicoVncCod, P00775_A933ContratoSrvVnc_ContratoCod, P00775_n933ContratoSrvVnc_ContratoCod, P00775_A1453ContratoServicosVnc_Ativo, P00775_n1453ContratoServicosVnc_Ativo, P00775_A1084ContratoSrvVnc_StatusDmn, P00775_n1084ContratoSrvVnc_StatusDmn,
               P00775_A1800ContratoSrvVnc_DoStatusDmn, P00775_n1800ContratoSrvVnc_DoStatusDmn, P00775_A915ContratoSrvVnc_CntSrvCod, P00775_A917ContratoSrvVnc_Codigo, P00775_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00775_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00775_A1628ContratoSrvVnc_SrvVncCntCod, P00775_n1628ContratoSrvVnc_SrvVncCntCod, P00775_A1088ContratoSrvVnc_PrestadoraCod, P00775_n1088ContratoSrvVnc_PrestadoraCod,
               P00775_A1090ContratoSrvVnc_SemCusto, P00775_n1090ContratoSrvVnc_SemCusto, P00775_A1437ContratoServicosVnc_NaoClonaInfo, P00775_n1437ContratoServicosVnc_NaoClonaInfo, P00775_A922ContratoSrvVnc_ServicoSigla, P00775_n922ContratoSrvVnc_ServicoSigla, P00775_A924ContratoSrvVnc_ServicoVncSigla, P00775_n924ContratoSrvVnc_ServicoVncSigla, P00775_A1630ContratoSrvVnc_SrvVncTpDias, P00775_n1630ContratoSrvVnc_SrvVncTpDias,
               P00775_A1651ContratoSrvVnc_SrvVncPrzInc, P00775_n1651ContratoSrvVnc_SrvVncPrzInc, P00775_A1663ContratoSrvVnc_SrvVncStatus, P00775_n1663ContratoSrvVnc_SrvVncStatus, P00775_A1743ContratoSrvVnc_NovoStatusDmn, P00775_n1743ContratoSrvVnc_NovoStatusDmn, P00775_A1801ContratoSrvVnc_NovoRspDmn, P00775_n1801ContratoSrvVnc_NovoRspDmn, P00775_A1745ContratoSrvVnc_VincularCom, P00775_n1745ContratoSrvVnc_VincularCom,
               P00775_A1818ContratoServicosVnc_ClonarLink, P00775_n1818ContratoServicosVnc_ClonarLink, P00775_A1821ContratoSrvVnc_SrvVncRef, P00775_n1821ContratoSrvVnc_SrvVncRef, P00775_A1145ContratoServicosVnc_ClonaSrvOri, P00775_n1145ContratoServicosVnc_ClonaSrvOri, P00775_A1438ContratoServicosVnc_Gestor, P00775_n1438ContratoServicosVnc_Gestor, P00775_A1642ContratoSrvVnc_SrvVncVlrUndCnt, P00775_n1642ContratoSrvVnc_SrvVncVlrUndCnt,
               P00775_A116Contrato_ValorUnidadeContratacao, P00775_n116Contrato_ValorUnidadeContratacao, P00775_A1629ContratoSrvVnc_SrvVncTpVnc, P00775_n1629ContratoSrvVnc_SrvVncTpVnc
               }
               , new Object[] {
               P00776_A1078ContratoGestor_ContratoCod, P00776_A1079ContratoGestor_UsuarioCod, P00776_A1446ContratoGestor_ContratadaAreaCod, P00776_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00777_A1553ContagemResultado_CntSrvCod, P00777_n1553ContagemResultado_CntSrvCod, P00777_A490ContagemResultado_ContratadaCod, P00777_n490ContagemResultado_ContratadaCod, P00777_A602ContagemResultado_OSVinculada, P00777_n602ContagemResultado_OSVinculada, P00777_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00778_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00779_A456ContagemResultado_Codigo, P00779_A1457ContagemResultado_TemDpnHmlg, P00779_n1457ContagemResultado_TemDpnHmlg
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007712_A1110AnexoDe_Tabela, P007712_A1109AnexoDe_Id, P007712_A1106Anexo_Codigo
               }
               , new Object[] {
               P007713_A456ContagemResultado_Codigo, P007713_A484ContagemResultado_StatusDmn, P007713_n484ContagemResultado_StatusDmn, P007713_A1636ContagemResultado_ServicoSS, P007713_n1636ContagemResultado_ServicoSS, P007713_A602ContagemResultado_OSVinculada, P007713_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007716_A456ContagemResultado_Codigo, P007716_A484ContagemResultado_StatusDmn, P007716_n484ContagemResultado_StatusDmn, P007716_A602ContagemResultado_OSVinculada, P007716_n602ContagemResultado_OSVinculada, P007716_A890ContagemResultado_Responsavel, P007716_n890ContagemResultado_Responsavel, P007716_A508ContagemResultado_Owner
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P007719_A456ContagemResultado_Codigo, P007719_A457ContagemResultado_Demanda, P007719_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P007720_A456ContagemResultado_Codigo, P007720_A1756ContagemResultado_NvlCnt, P007720_n1756ContagemResultado_NvlCnt, P007720_A901ContagemResultadoContagens_Prazo, P007720_n901ContagemResultadoContagens_Prazo, P007720_A854ContagemResultado_TipoPla, P007720_n854ContagemResultado_TipoPla, P007720_A853ContagemResultado_NomePla, P007720_n853ContagemResultado_NomePla, P007720_A833ContagemResultado_CstUntPrd,
               P007720_n833ContagemResultado_CstUntPrd, P007720_A800ContagemResultado_Deflator, P007720_n800ContagemResultado_Deflator, P007720_A517ContagemResultado_Ultima, P007720_A482ContagemResultadoContagens_Esforco, P007720_A483ContagemResultado_StatusCnt, P007720_A469ContagemResultado_NaoCnfCntCod, P007720_n469ContagemResultado_NaoCnfCntCod, P007720_A470ContagemResultado_ContadorFMCod, P007720_A463ContagemResultado_ParecerTcn,
               P007720_n463ContagemResultado_ParecerTcn, P007720_A462ContagemResultado_Divergencia, P007720_A461ContagemResultado_PFLFM, P007720_n461ContagemResultado_PFLFM, P007720_A460ContagemResultado_PFBFM, P007720_n460ContagemResultado_PFBFM, P007720_A459ContagemResultado_PFLFS, P007720_n459ContagemResultado_PFLFS, P007720_A458ContagemResultado_PFBFS, P007720_n458ContagemResultado_PFBFS,
               P007720_A481ContagemResultado_TimeCnt, P007720_n481ContagemResultado_TimeCnt, P007720_A511ContagemResultado_HoraCnt, P007720_A473ContagemResultado_DataCnt, P007720_A852ContagemResultado_Planilha, P007720_n852ContagemResultado_Planilha
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A531ContagemResultado_StatusUltCnt ;
      private short A1651ContratoSrvVnc_SrvVncPrzInc ;
      private short A1821ContratoSrvVnc_SrvVncRef ;
      private short OV49PrazoInicio ;
      private short AV49PrazoInicio ;
      private short AV68ContratoSrvVnc_SrvVncRef ;
      private short AV83GXLvl115 ;
      private short AV30Dias ;
      private short GXt_int3 ;
      private short AV27Hours ;
      private short AV28Minutes ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short A1515ContagemResultado_Evento ;
      private short A1756ContagemResultado_NvlCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short W483ContagemResultado_StatusCnt ;
      private short W482ContagemResultadoContagens_Esforco ;
      private short W1756ContagemResultado_NvlCnt ;
      private int AV10ContagemResultado_Codigo ;
      private int AV13Usuario_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A508ContagemResultado_Owner ;
      private int A890ContagemResultado_Responsavel ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A146Modulo_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1765ContagemResultado_CodSrvSSVnc ;
      private int AV15ContagemResultado_SistemaCod ;
      private int AV17ContagemResultado_FncUsrCod ;
      private int AV42ContagemResultado_Owner ;
      private int AV51ContagemResultado_Responsavel ;
      private int AV47ContagemResultado_RdmnIssueId ;
      private int AV48ContratoServicos_Codigo ;
      private int AV53OSVinculada ;
      private int AV19ContagemResultado_ContratadaOrigemCod ;
      private int AV16Modulo_Codigo ;
      private int AV18Contratada_Codigo ;
      private int AV8Servico_Codigo ;
      private int AV29AreaTrabalho_Codigo ;
      private int AV60CodSrvSSVnc ;
      private int A892LogResponsavel_DemandaCod ;
      private int AV55ContratoSrvVnc_Codigo ;
      private int A917ContratoSrvVnc_Codigo ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int A933ContratoSrvVnc_ContratoCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private int A1088ContratoSrvVnc_PrestadoraCod ;
      private int A1801ContratoSrvVnc_NovoRspDmn ;
      private int A1438ContratoServicosVnc_Gestor ;
      private int AV24ContratoSrvVnc_ServicoVncCod ;
      private int AV45Contrato_Codigo ;
      private int AV25ContratoSrvVnc_PrestadoraCod ;
      private int AV64NovoRspDmn ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int GXt_int2 ;
      private int AV20NewCodigo ;
      private int AV57SSVinculada ;
      private int GX_INS69 ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1452ContagemResultado_SS ;
      private int A1110AnexoDe_Tabela ;
      private int A1109AnexoDe_Id ;
      private int A1106Anexo_Codigo ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int GX_INS72 ;
      private int W456ContagemResultado_Codigo ;
      private int W470ContagemResultado_ContadorFMCod ;
      private long A1797LogResponsavel_Codigo ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV69PFB ;
      private decimal AV32PFBFSdaOSVnc ;
      private decimal A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal AV11ValorPF ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal W458ContagemResultado_PFBFS ;
      private decimal W459ContagemResultado_PFLFS ;
      private decimal W460ContagemResultado_PFBFM ;
      private decimal W461ContagemResultado_PFLFM ;
      private decimal W462ContagemResultado_Divergencia ;
      private decimal W800ContagemResultado_Deflator ;
      private decimal W833ContagemResultado_CstUntPrd ;
      private String AV71Ok ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A801ContagemResultado_ServicoSigla ;
      private String AV9StatusDemanda ;
      private String AV23Servico_Sigla ;
      private String A1130LogResponsavel_Status ;
      private String AV59DoStatusDemanda ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String A1630ContratoSrvVnc_SrvVncTpDias ;
      private String A1663ContratoSrvVnc_SrvVncStatus ;
      private String A1743ContratoSrvVnc_NovoStatusDmn ;
      private String A1745ContratoSrvVnc_VincularCom ;
      private String A1629ContratoSrvVnc_SrvVncTpVnc ;
      private String OV56VincularCom ;
      private String AV56VincularCom ;
      private String AV43TipoDias ;
      private String AV54Status ;
      private String AV52NovoStatusDmn ;
      private String Gx_msg ;
      private String A1744ContratoSrvVnc_PrestadoraTpFab ;
      private String Gx_emsg ;
      private String A854ContagemResultado_TipoPla ;
      private String A853ContagemResultado_NomePla ;
      private String A511ContagemResultado_HoraCnt ;
      private String W511ContagemResultado_HoraCnt ;
      private String W853ContagemResultado_NomePla ;
      private String W854ContagemResultado_TipoPla ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV72PrazoAtual ;
      private DateTime AV21PrazoEntrega ;
      private DateTime GXt_dtime4 ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A901ContagemResultadoContagens_Prazo ;
      private DateTime A481ContagemResultado_TimeCnt ;
      private DateTime W481ContagemResultado_TimeCnt ;
      private DateTime W901ContagemResultadoContagens_Prazo ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime W473ContagemResultado_DataCnt ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n146Modulo_Codigo ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1765ContagemResultado_CodSrvSSVnc ;
      private bool n465ContagemResultado_Link ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1130LogResponsavel_Status ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool n933ContratoSrvVnc_ContratoCod ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool n1088ContratoSrvVnc_PrestadoraCod ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool n1437ContratoServicosVnc_NaoClonaInfo ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1630ContratoSrvVnc_SrvVncTpDias ;
      private bool n1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool n1663ContratoSrvVnc_SrvVncStatus ;
      private bool n1743ContratoSrvVnc_NovoStatusDmn ;
      private bool n1801ContratoSrvVnc_NovoRspDmn ;
      private bool n1745ContratoSrvVnc_VincularCom ;
      private bool A1818ContratoServicosVnc_ClonarLink ;
      private bool n1818ContratoServicosVnc_ClonarLink ;
      private bool n1821ContratoSrvVnc_SrvVncRef ;
      private bool A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool n1438ContratoServicosVnc_Gestor ;
      private bool n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool n116Contrato_ValorUnidadeContratacao ;
      private bool n1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool AV26ContratoSrvVnc_SemCusto ;
      private bool AV39ClonaInfo ;
      private bool AV65ClonarLink ;
      private bool AV74ContratoServicosVnc_ClonaSrvOri ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean1 ;
      private bool returnInSub ;
      private bool AV44ContagemResultado_TemDpnHmlg ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1452ContagemResultado_SS ;
      private bool A1457ContagemResultado_TemDpnHmlg ;
      private bool n1457ContagemResultado_TemDpnHmlg ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n901ContagemResultadoContagens_Prazo ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n800ContagemResultado_Deflator ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n481ContagemResultado_TimeCnt ;
      private bool n852ContagemResultado_Planilha ;
      private bool W517ContagemResultado_Ultima ;
      private String A514ContagemResultado_Observacao ;
      private String A465ContagemResultado_Link ;
      private String AV40ContagemResultado_Observacao ;
      private String AV66Link ;
      private String A463ContagemResultado_ParecerTcn ;
      private String W463ContagemResultado_ParecerTcn ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String AV14ContagemResultado_Demanda ;
      private String AV67Referencia ;
      private String AV33ContagemResultado_Descricao ;
      private String AV41DescricaoAutomatica ;
      private String AV63DemandaFM ;
      private String AV73ContagemResultado_OsFsOsFm ;
      private String A852ContagemResultado_Planilha ;
      private String W852ContagemResultado_Planilha ;
      private IGxSession AV35WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00773_A456ContagemResultado_Codigo ;
      private String[] P00773_A484ContagemResultado_StatusDmn ;
      private bool[] P00773_n484ContagemResultado_StatusDmn ;
      private String[] P00773_A493ContagemResultado_DemandaFM ;
      private bool[] P00773_n493ContagemResultado_DemandaFM ;
      private String[] P00773_A457ContagemResultado_Demanda ;
      private bool[] P00773_n457ContagemResultado_Demanda ;
      private int[] P00773_A489ContagemResultado_SistemaCod ;
      private bool[] P00773_n489ContagemResultado_SistemaCod ;
      private int[] P00773_A1044ContagemResultado_FncUsrCod ;
      private bool[] P00773_n1044ContagemResultado_FncUsrCod ;
      private String[] P00773_A494ContagemResultado_Descricao ;
      private bool[] P00773_n494ContagemResultado_Descricao ;
      private String[] P00773_A514ContagemResultado_Observacao ;
      private bool[] P00773_n514ContagemResultado_Observacao ;
      private int[] P00773_A508ContagemResultado_Owner ;
      private int[] P00773_A890ContagemResultado_Responsavel ;
      private bool[] P00773_n890ContagemResultado_Responsavel ;
      private int[] P00773_A1389ContagemResultado_RdmnIssueId ;
      private bool[] P00773_n1389ContagemResultado_RdmnIssueId ;
      private int[] P00773_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00773_n1553ContagemResultado_CntSrvCod ;
      private int[] P00773_A602ContagemResultado_OSVinculada ;
      private bool[] P00773_n602ContagemResultado_OSVinculada ;
      private int[] P00773_A490ContagemResultado_ContratadaCod ;
      private bool[] P00773_n490ContagemResultado_ContratadaCod ;
      private String[] P00773_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00773_n1326ContagemResultado_ContratadaTipoFab ;
      private int[] P00773_A146Modulo_Codigo ;
      private bool[] P00773_n146Modulo_Codigo ;
      private int[] P00773_A601ContagemResultado_Servico ;
      private bool[] P00773_n601ContagemResultado_Servico ;
      private String[] P00773_A801ContagemResultado_ServicoSigla ;
      private bool[] P00773_n801ContagemResultado_ServicoSigla ;
      private int[] P00773_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00773_n52Contratada_AreaTrabalhoCod ;
      private int[] P00773_A1765ContagemResultado_CodSrvSSVnc ;
      private bool[] P00773_n1765ContagemResultado_CodSrvSSVnc ;
      private String[] P00773_A465ContagemResultado_Link ;
      private bool[] P00773_n465ContagemResultado_Link ;
      private DateTime[] P00773_A472ContagemResultado_DataEntrega ;
      private bool[] P00773_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00773_A912ContagemResultado_HoraEntrega ;
      private bool[] P00773_n912ContagemResultado_HoraEntrega ;
      private decimal[] P00773_A684ContagemResultado_PFBFSUltima ;
      private short[] P00773_A531ContagemResultado_StatusUltCnt ;
      private int[] P00774_A892LogResponsavel_DemandaCod ;
      private bool[] P00774_n892LogResponsavel_DemandaCod ;
      private String[] P00774_A1130LogResponsavel_Status ;
      private bool[] P00774_n1130LogResponsavel_Status ;
      private long[] P00774_A1797LogResponsavel_Codigo ;
      private int[] P00775_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00775_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00775_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00775_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00775_A933ContratoSrvVnc_ContratoCod ;
      private bool[] P00775_n933ContratoSrvVnc_ContratoCod ;
      private bool[] P00775_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00775_n1453ContratoServicosVnc_Ativo ;
      private String[] P00775_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00775_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00775_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] P00775_n1800ContratoSrvVnc_DoStatusDmn ;
      private int[] P00775_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00775_A917ContratoSrvVnc_Codigo ;
      private int[] P00775_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00775_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00775_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00775_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00775_A1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] P00775_n1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] P00775_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00775_n1090ContratoSrvVnc_SemCusto ;
      private bool[] P00775_A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] P00775_n1437ContratoServicosVnc_NaoClonaInfo ;
      private String[] P00775_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00775_n922ContratoSrvVnc_ServicoSigla ;
      private String[] P00775_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00775_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00775_A1630ContratoSrvVnc_SrvVncTpDias ;
      private bool[] P00775_n1630ContratoSrvVnc_SrvVncTpDias ;
      private short[] P00775_A1651ContratoSrvVnc_SrvVncPrzInc ;
      private bool[] P00775_n1651ContratoSrvVnc_SrvVncPrzInc ;
      private String[] P00775_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] P00775_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] P00775_A1743ContratoSrvVnc_NovoStatusDmn ;
      private bool[] P00775_n1743ContratoSrvVnc_NovoStatusDmn ;
      private int[] P00775_A1801ContratoSrvVnc_NovoRspDmn ;
      private bool[] P00775_n1801ContratoSrvVnc_NovoRspDmn ;
      private String[] P00775_A1745ContratoSrvVnc_VincularCom ;
      private bool[] P00775_n1745ContratoSrvVnc_VincularCom ;
      private bool[] P00775_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P00775_n1818ContratoServicosVnc_ClonarLink ;
      private short[] P00775_A1821ContratoSrvVnc_SrvVncRef ;
      private bool[] P00775_n1821ContratoSrvVnc_SrvVncRef ;
      private bool[] P00775_A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] P00775_n1145ContratoServicosVnc_ClonaSrvOri ;
      private int[] P00775_A1438ContratoServicosVnc_Gestor ;
      private bool[] P00775_n1438ContratoServicosVnc_Gestor ;
      private decimal[] P00775_A1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private bool[] P00775_n1642ContratoSrvVnc_SrvVncVlrUndCnt ;
      private decimal[] P00775_A116Contrato_ValorUnidadeContratacao ;
      private bool[] P00775_n116Contrato_ValorUnidadeContratacao ;
      private String[] P00775_A1629ContratoSrvVnc_SrvVncTpVnc ;
      private bool[] P00775_n1629ContratoSrvVnc_SrvVncTpVnc ;
      private int[] P00776_A1078ContratoGestor_ContratoCod ;
      private int[] P00776_A1079ContratoGestor_UsuarioCod ;
      private int[] P00776_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00776_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00777_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00777_n1553ContagemResultado_CntSrvCod ;
      private int[] P00777_A490ContagemResultado_ContratadaCod ;
      private bool[] P00777_n490ContagemResultado_ContratadaCod ;
      private int[] P00777_A602ContagemResultado_OSVinculada ;
      private bool[] P00777_n602ContagemResultado_OSVinculada ;
      private int[] P00777_A456ContagemResultado_Codigo ;
      private int[] P00778_A456ContagemResultado_Codigo ;
      private int[] P00779_A456ContagemResultado_Codigo ;
      private bool[] P00779_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P00779_n1457ContagemResultado_TemDpnHmlg ;
      private int[] P007712_A1110AnexoDe_Tabela ;
      private int[] P007712_A1109AnexoDe_Id ;
      private int[] P007712_A1106Anexo_Codigo ;
      private int[] P007713_A456ContagemResultado_Codigo ;
      private String[] P007713_A484ContagemResultado_StatusDmn ;
      private bool[] P007713_n484ContagemResultado_StatusDmn ;
      private int[] P007713_A1636ContagemResultado_ServicoSS ;
      private bool[] P007713_n1636ContagemResultado_ServicoSS ;
      private int[] P007713_A602ContagemResultado_OSVinculada ;
      private bool[] P007713_n602ContagemResultado_OSVinculada ;
      private int[] P007716_A456ContagemResultado_Codigo ;
      private String[] P007716_A484ContagemResultado_StatusDmn ;
      private bool[] P007716_n484ContagemResultado_StatusDmn ;
      private int[] P007716_A602ContagemResultado_OSVinculada ;
      private bool[] P007716_n602ContagemResultado_OSVinculada ;
      private int[] P007716_A890ContagemResultado_Responsavel ;
      private bool[] P007716_n890ContagemResultado_Responsavel ;
      private int[] P007716_A508ContagemResultado_Owner ;
      private int[] P007719_A456ContagemResultado_Codigo ;
      private String[] P007719_A457ContagemResultado_Demanda ;
      private bool[] P007719_n457ContagemResultado_Demanda ;
      private int[] P007720_A456ContagemResultado_Codigo ;
      private short[] P007720_A1756ContagemResultado_NvlCnt ;
      private bool[] P007720_n1756ContagemResultado_NvlCnt ;
      private DateTime[] P007720_A901ContagemResultadoContagens_Prazo ;
      private bool[] P007720_n901ContagemResultadoContagens_Prazo ;
      private String[] P007720_A854ContagemResultado_TipoPla ;
      private bool[] P007720_n854ContagemResultado_TipoPla ;
      private String[] P007720_A853ContagemResultado_NomePla ;
      private bool[] P007720_n853ContagemResultado_NomePla ;
      private decimal[] P007720_A833ContagemResultado_CstUntPrd ;
      private bool[] P007720_n833ContagemResultado_CstUntPrd ;
      private decimal[] P007720_A800ContagemResultado_Deflator ;
      private bool[] P007720_n800ContagemResultado_Deflator ;
      private bool[] P007720_A517ContagemResultado_Ultima ;
      private short[] P007720_A482ContagemResultadoContagens_Esforco ;
      private short[] P007720_A483ContagemResultado_StatusCnt ;
      private int[] P007720_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P007720_n469ContagemResultado_NaoCnfCntCod ;
      private int[] P007720_A470ContagemResultado_ContadorFMCod ;
      private String[] P007720_A463ContagemResultado_ParecerTcn ;
      private bool[] P007720_n463ContagemResultado_ParecerTcn ;
      private decimal[] P007720_A462ContagemResultado_Divergencia ;
      private decimal[] P007720_A461ContagemResultado_PFLFM ;
      private bool[] P007720_n461ContagemResultado_PFLFM ;
      private decimal[] P007720_A460ContagemResultado_PFBFM ;
      private bool[] P007720_n460ContagemResultado_PFBFM ;
      private decimal[] P007720_A459ContagemResultado_PFLFS ;
      private bool[] P007720_n459ContagemResultado_PFLFS ;
      private decimal[] P007720_A458ContagemResultado_PFBFS ;
      private bool[] P007720_n458ContagemResultado_PFBFS ;
      private DateTime[] P007720_A481ContagemResultado_TimeCnt ;
      private bool[] P007720_n481ContagemResultado_TimeCnt ;
      private String[] P007720_A511ContagemResultado_HoraCnt ;
      private DateTime[] P007720_A473ContagemResultado_DataCnt ;
      private String[] P007720_A852ContagemResultado_Planilha ;
      private bool[] P007720_n852ContagemResultado_Planilha ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV34Anexo_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV37Codigos ;
      private SdtContagemResultado AV61ContagemResultado ;
      private SdtContratada AV62Contratada ;
   }

   public class prc_disparoservicovinculado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00775( IGxContext context ,
                                             int AV55ContratoSrvVnc_Codigo ,
                                             int A917ContratoSrvVnc_Codigo ,
                                             int A915ContratoSrvVnc_CntSrvCod ,
                                             int AV48ContratoServicos_Codigo ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             String A1800ContratoSrvVnc_DoStatusDmn ,
                                             String AV59DoStatusDemanda ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             String AV9StatusDemanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [4] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T5.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_ContratoCod, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T1.[ContratoSrvVnc_Codigo], T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T5.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T1.[ContratoSrvVnc_PrestadoraCod], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_NaoClonaInfo], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T5.[ContratoServicos_PrazoTpDias] AS ContratoSrvVnc_SrvVncTpDias, T5.[ContratoServicos_PrazoInicio] AS ContratoSrvVnc_SrvVncPrzInc, T1.[ContratoSrvVnc_SrvVncStatus], T1.[ContratoSrvVnc_NovoStatusDmn], T1.[ContratoSrvVnc_NovoRspDmn], T1.[ContratoSrvVnc_VincularCom], T1.[ContratoServicosVnc_ClonarLink], T1.[ContratoSrvVnc_SrvVncRef], T1.[ContratoServicosVnc_ClonaSrvOri], T1.[ContratoServicosVnc_Gestor], T5.[Servico_VlrUnidadeContratada] AS ContratoSrvVnc_SrvVncVlrUndCnt, T4.[Contrato_ValorUnidadeContratacao], T5.[ContratoServicos_TipoVnc] AS ContratoSrvVnc_SrvVncTpVnc FROM ((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T5.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoSrvVnc_DoStatusDmn] IS NULL or T1.[ContratoSrvVnc_DoStatusDmn] = @AV59DoStatusDemanda)";
         scmdbuf = scmdbuf + " and (T1.[ContratoSrvVnc_StatusDmn] = @AV9StatusDemanda)";
         if ( AV55ContratoSrvVnc_Codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_Codigo] = @AV55ContratoSrvVnc_Codigo)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( (0==AV55ContratoSrvVnc_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_CntSrvCod] = @AV48ContratoServicos_Codigo)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( (0==AV55ContratoSrvVnc_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoSrvVnc_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_P00775(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new UpdateCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00773 ;
          prmP00773 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00773 ;
          cmdBufferP00773=" SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_FncUsrCod], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Observacao], T1.[ContagemResultado_Owner], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_RdmnIssueId], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[Modulo_Codigo], T3.[Servico_Codigo] AS ContagemResultado_Servico, T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Contratada_AreaTrabalhoCod], T5.[ContagemResultado_ServicoSS] AS ContagemResultado_CodSrvSSVnc, T1.[ContagemResultado_Link], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T5 WITH "
          + " (NOLOCK) ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV10ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmP00774 ;
          prmP00774 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00776 ;
          prmP00776 = new Object[] {
          new Object[] {"@ContratoSrvVnc_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00777 ;
          prmP00777 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContratoSrvVnc_PrestadoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24ContratoSrvVnc_ServicoVncCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00778 ;
          prmP00778 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_RdmnIssueId",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00778 ;
          cmdBufferP00778=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_LiqLogCod], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_ServicoSS], [ContagemResultado_Evidencia], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], "
          + " [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_DataEntrega, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_HoraEntrega, @ContagemResultado_LiqLogCod, @ContagemResultado_FncUsrCod, @ContagemResultado_PrazoInicialDias, @ContagemResultado_DataCadastro, @ContagemResultado_DataPrevista, @ContagemResultado_RdmnIssueId, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, @ContagemResultado_ServicoSS, '', convert(int, 0), convert(int, 0), '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit,"
          + " 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP00779 ;
          prmP00779 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007710 ;
          prmP007710 = new Object[] {
          new Object[] {"@ContagemResultado_TemDpnHmlg",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007711 ;
          prmP007711 = new Object[] {
          new Object[] {"@ContagemResultado_TemDpnHmlg",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007712 ;
          prmP007712 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007713 ;
          prmP007713 = new Object[] {
          new Object[] {"@AV53OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007714 ;
          prmP007714 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007715 ;
          prmP007715 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007716 ;
          prmP007716 = new Object[] {
          new Object[] {"@AV53OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007717 ;
          prmP007717 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007718 ;
          prmP007718 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007719 ;
          prmP007719 = new Object[] {
          new Object[] {"@AV53OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007720 ;
          prmP007720 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007721 ;
          prmP007721 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00775 ;
          prmP00775 = new Object[] {
          new Object[] {"@AV59DoStatusDemanda",SqlDbType.Char,1,0} ,
          new Object[] {"@AV9StatusDemanda",SqlDbType.Char,1,0} ,
          new Object[] {"@AV55ContratoSrvVnc_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00773", cmdBufferP00773,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00773,1,0,true,true )
             ,new CursorDef("P00774", "SELECT TOP 1 [LogResponsavel_DemandaCod], [LogResponsavel_Status], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00774,1,0,false,true )
             ,new CursorDef("P00775", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00775,100,0,true,false )
             ,new CursorDef("P00776", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @ContratoSrvVnc_ContratoCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00776,100,0,true,false )
             ,new CursorDef("P00777", "SELECT TOP 1 [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_OSVinculada] = @AV10ContagemResultado_Codigo) AND ([ContagemResultado_ContratadaCod] = @AV25ContratoSrvVnc_PrestadoraCod) AND ([ContagemResultado_CntSrvCod] = @AV24ContratoSrvVnc_ServicoVncCod) ORDER BY [ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00777,1,0,false,true )
             ,new CursorDef("P00778", cmdBufferP00778, GxErrorMask.GX_NOMASK,prmP00778)
             ,new CursorDef("P00779", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_TemDpnHmlg] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV10ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00779,1,0,true,true )
             ,new CursorDef("P007710", "UPDATE [ContagemResultado] SET [ContagemResultado_TemDpnHmlg]=@ContagemResultado_TemDpnHmlg  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007710)
             ,new CursorDef("P007711", "UPDATE [ContagemResultado] SET [ContagemResultado_TemDpnHmlg]=@ContagemResultado_TemDpnHmlg  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007711)
             ,new CursorDef("P007712", "SELECT [AnexoDe_Tabela], [AnexoDe_Id], [Anexo_Codigo] FROM [AnexosDe] WITH (NOLOCK) WHERE ([AnexoDe_Id] = @AV10ContagemResultado_Codigo) AND ([AnexoDe_Tabela] = 1) ORDER BY [AnexoDe_Id] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007712,100,0,false,false )
             ,new CursorDef("P007713", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_ServicoSS], [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV53OSVinculada ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007713,1,0,true,true )
             ,new CursorDef("P007714", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007714)
             ,new CursorDef("P007715", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007715)
             ,new CursorDef("P007716", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, [ContagemResultado_Responsavel], [ContagemResultado_Owner] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV53OSVinculada ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007716,1,0,true,true )
             ,new CursorDef("P007717", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007717)
             ,new CursorDef("P007718", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007718)
             ,new CursorDef("P007719", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV53OSVinculada ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007719,1,0,false,true )
             ,new CursorDef("P007720", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_NvlCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_CstUntPrd], [ContagemResultado_Deflator], [ContagemResultado_Ultima], [ContagemResultadoContagens_Esforco], [ContagemResultado_StatusCnt], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_ContadorFMCod], [ContagemResultado_ParecerTcn], [ContagemResultado_Divergencia], [ContagemResultado_PFLFM], [ContagemResultado_PFBFM], [ContagemResultado_PFLFS], [ContagemResultado_PFBFS], [ContagemResultado_TimeCnt], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt], [ContagemResultado_Planilha] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @AV10ContagemResultado_Codigo) AND ([ContagemResultado_StatusCnt] = 5) ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007720,1,0,true,true )
             ,new CursorDef("P007721", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_TimeCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, @ContagemResultadoContagens_Prazo, @ContagemResultado_NvlCnt)", GxErrorMask.GX_NOMASK,prmP007721)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 15) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getLongVarchar(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[40])[0] = rslt.getGXDate(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((DateTime[]) buf[42])[0] = rslt.getGXDateTime(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(24) ;
                ((short[]) buf[45])[0] = rslt.getShort(25) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((bool[]) buf[20])[0] = rslt.getBool(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((bool[]) buf[22])[0] = rslt.getBool(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((short[]) buf[30])[0] = rslt.getShort(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 1) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getString(21, 20) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((bool[]) buf[40])[0] = rslt.getBool(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((short[]) buf[42])[0] = rslt.getShort(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((bool[]) buf[44])[0] = rslt.getBool(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((decimal[]) buf[48])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((decimal[]) buf[50])[0] = rslt.getDecimal(27) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(27);
                ((String[]) buf[52])[0] = rslt.getString(28, 1) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(28);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(14) ;
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[30])[0] = rslt.getGXDateTime(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((String[]) buf[32])[0] = rslt.getString(20, 5) ;
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(21) ;
                ((String[]) buf[34])[0] = rslt.getBLOBFile(22, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[26]);
                }
                stmt.SetParameter(15, (int)parms[27]);
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(22, (DateTime)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(25, (short)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(26, (DateTime)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(27, (DateTime)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 30 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(30, (short)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[59]);
                }
                stmt.SetParameter(32, (short)parms[60]);
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[12]);
                }
                stmt.SetParameter(9, (decimal)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                stmt.SetParameter(11, (int)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[18]);
                }
                stmt.SetParameter(13, (short)parms[19]);
                stmt.SetParameter(14, (short)parms[20]);
                stmt.SetParameter(15, (bool)parms[21]);
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(17, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(21, (DateTime)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(22, (short)parms[35]);
                }
                return;
       }
    }

 }

}
