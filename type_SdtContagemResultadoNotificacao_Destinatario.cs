/*
               File: type_SdtContagemResultadoNotificacao_Destinatario
        Description: Notificações da Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:52.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoNotificacao.Destinatario" )]
   [XmlType(TypeName =  "ContagemResultadoNotificacao.Destinatario" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemResultadoNotificacao_Destinatario : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtContagemResultadoNotificacao_Destinatario( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z = "";
      }

      public SdtContagemResultadoNotificacao_Destinatario( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Destinatario");
         metadata.Set("BT", "ContagemResultadoNotificacaoDestinatario");
         metadata.Set("PK", "[ \"ContagemResultadoNotificacao_DestCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoNotificacao_DestCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultadoNotificacao_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultadoNotificacao_DestCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destpescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destnome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destemail_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destpescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destnome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destemail_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoNotificacao_Destinatario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoNotificacao_Destinatario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoNotificacao_Destinatario obj ;
         obj = this;
         obj.gxTpr_Contagemresultadonotificacao_destcod = deserialized.gxTpr_Contagemresultadonotificacao_destcod;
         obj.gxTpr_Contagemresultadonotificacao_destpescod = deserialized.gxTpr_Contagemresultadonotificacao_destpescod;
         obj.gxTpr_Contagemresultadonotificacao_destnome = deserialized.gxTpr_Contagemresultadonotificacao_destnome;
         obj.gxTpr_Contagemresultadonotificacao_destemail = deserialized.gxTpr_Contagemresultadonotificacao_destemail;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadonotificacao_destcod_Z = deserialized.gxTpr_Contagemresultadonotificacao_destcod_Z;
         obj.gxTpr_Contagemresultadonotificacao_destpescod_Z = deserialized.gxTpr_Contagemresultadonotificacao_destpescod_Z;
         obj.gxTpr_Contagemresultadonotificacao_destnome_Z = deserialized.gxTpr_Contagemresultadonotificacao_destnome_Z;
         obj.gxTpr_Contagemresultadonotificacao_destemail_Z = deserialized.gxTpr_Contagemresultadonotificacao_destemail_Z;
         obj.gxTpr_Contagemresultadonotificacao_destpescod_N = deserialized.gxTpr_Contagemresultadonotificacao_destpescod_N;
         obj.gxTpr_Contagemresultadonotificacao_destnome_N = deserialized.gxTpr_Contagemresultadonotificacao_destnome_N;
         obj.gxTpr_Contagemresultadonotificacao_destemail_N = deserialized.gxTpr_Contagemresultadonotificacao_destemail_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestCod") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestPesCod") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestNome") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestEmail") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestPesCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestNome_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestEmail_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestPesCod_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestNome_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DestEmail_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoNotificacao.Destinatario";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_DestCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_DestPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_DestNome", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_DestEmail", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestNome_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestEmail_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DestEmail_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoNotificacao_DestCod", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod, false);
         AddObjectProperty("ContagemResultadoNotificacao_DestPesCod", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod, false);
         AddObjectProperty("ContagemResultadoNotificacao_DestNome", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome, false);
         AddObjectProperty("ContagemResultadoNotificacao_DestEmail", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestCod_Z", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestPesCod_Z", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestNome_Z", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestEmail_Z", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestPesCod_N", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestNome_N", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_DestEmail_N", gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestCod"   )]
      public int gxTpr_Contagemresultadonotificacao_destcod
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestPesCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestPesCod"   )]
      public int gxTpr_Contagemresultadonotificacao_destpescod
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestNome" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestNome"   )]
      public String gxTpr_Contagemresultadonotificacao_destnome
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestEmail" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestEmail"   )]
      public String gxTpr_Contagemresultadonotificacao_destemail
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestCod_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_destcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestPesCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestPesCod_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_destpescod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z = (int)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestNome_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestNome_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_destnome_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestEmail_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestEmail_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_destemail_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z = (String)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestPesCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestPesCod_N"   )]
      public short gxTpr_Contagemresultadonotificacao_destpescod_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestNome_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestNome_N"   )]
      public short gxTpr_Contagemresultadonotificacao_destnome_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DestEmail_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DestEmail_N"   )]
      public short gxTpr_Contagemresultadonotificacao_destemail_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N = (short)(value);
            gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified = 1;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z = "";
         sTagName = "";
         return  ;
      }

      private short gxTv_SdtContagemResultadoNotificacao_Destinatario_Modified ;
      private short gxTv_SdtContagemResultadoNotificacao_Destinatario_Initialized ;
      private short gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod ;
      private int gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod ;
      private int gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destcod_Z ;
      private int gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destpescod_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome ;
      private String gxTv_SdtContagemResultadoNotificacao_Destinatario_Mode ;
      private String gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destnome_Z ;
      private String sTagName ;
      private String gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail ;
      private String gxTv_SdtContagemResultadoNotificacao_Destinatario_Contagemresultadonotificacao_destemail_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoNotificacao.Destinatario", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemResultadoNotificacao_Destinatario_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoNotificacao_Destinatario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoNotificacao_Destinatario_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoNotificacao_Destinatario_RESTInterface( SdtContagemResultadoNotificacao_Destinatario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoNotificacao_DestCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_destcod
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_destcod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_destcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_DestPesCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_destpescod
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_destpescod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_destpescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_DestNome" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_destnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadonotificacao_destnome) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_destnome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_DestEmail" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_destemail
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_destemail ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_destemail = (String)(value);
         }

      }

      public SdtContagemResultadoNotificacao_Destinatario sdt
      {
         get {
            return (SdtContagemResultadoNotificacao_Destinatario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoNotificacao_Destinatario() ;
         }
      }

   }

}
