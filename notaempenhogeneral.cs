/*
               File: NotaEmpenhoGeneral
        Description: Nota Empenho General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:18.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class notaempenhogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public notaempenhogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public notaempenhogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_NotaEmpenho_Codigo )
      {
         this.A1560NotaEmpenho_Codigo = aP0_NotaEmpenho_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkNotaEmpenho_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1560NotaEmpenho_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAM82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "NotaEmpenhoGeneral";
               context.Gx_err = 0;
               WSM82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Nota Empenho General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428238193");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("notaempenhogeneral.aspx") + "?" + UrlEncode("" +A1560NotaEmpenho_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1560NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_ITENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_DEMISSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_QTD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_SALDOANT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_SALDOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_ATIVO", GetSecureSignedToken( sPrefix, A1570NotaEmpenho_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormM82( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("notaempenhogeneral.js", "?2020428238195");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "NotaEmpenhoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nota Empenho General" ;
      }

      protected void WBM80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "notaempenhogeneral.aspx");
            }
            wb_table1_2_M82( true) ;
         }
         else
         {
            wb_table1_2_M82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_M82e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTM82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Nota Empenho General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPM80( ) ;
            }
         }
      }

      protected void WSM82( )
      {
         STARTM82( ) ;
         EVTM82( ) ;
      }

      protected void EVTM82( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11M82 */
                                    E11M82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12M82 */
                                    E12M82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13M82 */
                                    E13M82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14M82 */
                                    E14M82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEM82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormM82( ) ;
            }
         }
      }

      protected void PAM82( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            chkNotaEmpenho_Ativo.Name = "NOTAEMPENHO_ATIVO";
            chkNotaEmpenho_Ativo.WebTags = "";
            chkNotaEmpenho_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkNotaEmpenho_Ativo_Internalname, "TitleCaption", chkNotaEmpenho_Ativo.Caption);
            chkNotaEmpenho_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFM82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "NotaEmpenhoGeneral";
         context.Gx_err = 0;
      }

      protected void RFM82( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00M82 */
            pr_default.execute(0, new Object[] {A1560NotaEmpenho_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = H00M82_A74Contrato_Codigo[0];
               A1570NotaEmpenho_Ativo = H00M82_A1570NotaEmpenho_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_ATIVO", GetSecureSignedToken( sPrefix, A1570NotaEmpenho_Ativo));
               n1570NotaEmpenho_Ativo = H00M82_n1570NotaEmpenho_Ativo[0];
               A1569NotaEmpenho_SaldoPos = H00M82_A1569NotaEmpenho_SaldoPos[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1569NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( A1569NotaEmpenho_SaldoPos, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_SALDOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1569NotaEmpenho_SaldoPos = H00M82_n1569NotaEmpenho_SaldoPos[0];
               A1568NotaEmpenho_SaldoAnt = H00M82_A1568NotaEmpenho_SaldoAnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1568NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( A1568NotaEmpenho_SaldoAnt, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_SALDOANT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1568NotaEmpenho_SaldoAnt = H00M82_n1568NotaEmpenho_SaldoAnt[0];
               A1567NotaEmpenho_Qtd = H00M82_A1567NotaEmpenho_Qtd[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_QTD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
               n1567NotaEmpenho_Qtd = H00M82_n1567NotaEmpenho_Qtd[0];
               A1566NotaEmpenho_Valor = H00M82_A1566NotaEmpenho_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1566NotaEmpenho_Valor = H00M82_n1566NotaEmpenho_Valor[0];
               A1565NotaEmpenho_DEmissao = H00M82_A1565NotaEmpenho_DEmissao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_DEMISSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
               n1565NotaEmpenho_DEmissao = H00M82_n1565NotaEmpenho_DEmissao[0];
               A1564NotaEmpenho_Itentificador = H00M82_A1564NotaEmpenho_Itentificador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_ITENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
               n1564NotaEmpenho_Itentificador = H00M82_n1564NotaEmpenho_Itentificador[0];
               A1561SaldoContrato_Codigo = H00M82_A1561SaldoContrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SALDOCONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = H00M82_A74Contrato_Codigo[0];
               /* Execute user event: E12M82 */
               E12M82 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBM80( ) ;
         }
      }

      protected void STRUPM80( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "NotaEmpenhoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11M82 */
         E11M82 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSaldoContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SALDOCONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
            A1564NotaEmpenho_Itentificador = cgiGet( edtNotaEmpenho_Itentificador_Internalname);
            n1564NotaEmpenho_Itentificador = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_ITENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
            A1565NotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtNotaEmpenho_DEmissao_Internalname), 0);
            n1565NotaEmpenho_DEmissao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_DEMISSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
            A1566NotaEmpenho_Valor = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Valor_Internalname), ",", ".");
            n1566NotaEmpenho_Valor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1567NotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Qtd_Internalname), ",", ".");
            n1567NotaEmpenho_Qtd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_QTD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
            A1568NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( edtNotaEmpenho_SaldoAnt_Internalname), ",", ".");
            n1568NotaEmpenho_SaldoAnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1568NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( A1568NotaEmpenho_SaldoAnt, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_SALDOANT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1569NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( edtNotaEmpenho_SaldoPos_Internalname), ",", ".");
            n1569NotaEmpenho_SaldoPos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1569NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( A1569NotaEmpenho_SaldoPos, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_SALDOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1570NotaEmpenho_Ativo = StringUtil.StrToBool( cgiGet( chkNotaEmpenho_Ativo_Internalname));
            n1570NotaEmpenho_Ativo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NOTAEMPENHO_ATIVO", GetSecureSignedToken( sPrefix, A1570NotaEmpenho_Ativo));
            /* Read saved values. */
            wcpOA1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1560NotaEmpenho_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11M82 */
         E11M82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11M82( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12M82( )
      {
         /* Load Routine */
      }

      protected void E13M82( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("notaempenho.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1560NotaEmpenho_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14M82( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("notaempenho.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1560NotaEmpenho_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "NotaEmpenho";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "NotaEmpenho_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7NotaEmpenho_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_M82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_M82( true) ;
         }
         else
         {
            wb_table2_8_M82( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_M82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_M82( true) ;
         }
         else
         {
            wb_table3_56_M82( false) ;
         }
         return  ;
      }

      protected void wb_table3_56_M82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_M82e( true) ;
         }
         else
         {
            wb_table1_2_M82e( false) ;
         }
      }

      protected void wb_table3_56_M82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_M82e( true) ;
         }
         else
         {
            wb_table3_56_M82e( false) ;
         }
      }

      protected void wb_table2_8_M82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_codigo_Internalname, "C�digo", "", "", lblTextblocknotaempenho_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_codigo_Internalname, "Saldo Contrato", "", "", lblTextblocksaldocontrato_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_itentificador_Internalname, "Identificador", "", "", lblTextblocknotaempenho_itentificador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Itentificador_Internalname, StringUtil.RTrim( A1564NotaEmpenho_Itentificador), StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Itentificador_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_demissao_Internalname, "Emiss�o", "", "", lblTextblocknotaempenho_demissao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtNotaEmpenho_DEmissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_DEmissao_Internalname, context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_DEmissao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtNotaEmpenho_DEmissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_valor_Internalname, "Valor", "", "", lblTextblocknotaempenho_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ",", "")), context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Valor_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_qtd_Internalname, "Quantidade", "", "", lblTextblocknotaempenho_qtd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Qtd_Internalname, StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ",", "")), context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Qtd_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_saldoant_Internalname, "Anterior", "", "", lblTextblocknotaempenho_saldoant_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_SaldoAnt_Internalname, StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ",", "")), context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_SaldoAnt_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_saldopos_Internalname, "Posterior", "", "", lblTextblocknotaempenho_saldopos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_SaldoPos_Internalname, StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ",", "")), context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_SaldoPos_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_ativo_Internalname, "Ativo", "", "", lblTextblocknotaempenho_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenhoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkNotaEmpenho_Ativo_Internalname, StringUtil.BoolToStr( A1570NotaEmpenho_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_M82e( true) ;
         }
         else
         {
            wb_table2_8_M82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1560NotaEmpenho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAM82( ) ;
         WSM82( ) ;
         WEM82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1560NotaEmpenho_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAM82( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "notaempenhogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAM82( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1560NotaEmpenho_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
         wcpOA1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1560NotaEmpenho_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1560NotaEmpenho_Codigo != wcpOA1560NotaEmpenho_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1560NotaEmpenho_Codigo = A1560NotaEmpenho_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1560NotaEmpenho_Codigo = cgiGet( sPrefix+"A1560NotaEmpenho_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1560NotaEmpenho_Codigo) > 0 )
         {
            A1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1560NotaEmpenho_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
         else
         {
            A1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1560NotaEmpenho_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAM82( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSM82( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSM82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1560NotaEmpenho_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1560NotaEmpenho_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1560NotaEmpenho_Codigo_CTRL", StringUtil.RTrim( sCtrlA1560NotaEmpenho_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEM82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282381941");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("notaempenhogeneral.js", "?20204282381941");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocknotaempenho_codigo_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_CODIGO";
         edtNotaEmpenho_Codigo_Internalname = sPrefix+"NOTAEMPENHO_CODIGO";
         lblTextblocksaldocontrato_codigo_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_CODIGO";
         edtSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO";
         lblTextblocknotaempenho_itentificador_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_ITENTIFICADOR";
         edtNotaEmpenho_Itentificador_Internalname = sPrefix+"NOTAEMPENHO_ITENTIFICADOR";
         lblTextblocknotaempenho_demissao_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_DEMISSAO";
         edtNotaEmpenho_DEmissao_Internalname = sPrefix+"NOTAEMPENHO_DEMISSAO";
         lblTextblocknotaempenho_valor_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_VALOR";
         edtNotaEmpenho_Valor_Internalname = sPrefix+"NOTAEMPENHO_VALOR";
         lblTextblocknotaempenho_qtd_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_QTD";
         edtNotaEmpenho_Qtd_Internalname = sPrefix+"NOTAEMPENHO_QTD";
         lblTextblocknotaempenho_saldoant_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_SALDOANT";
         edtNotaEmpenho_SaldoAnt_Internalname = sPrefix+"NOTAEMPENHO_SALDOANT";
         lblTextblocknotaempenho_saldopos_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_SALDOPOS";
         edtNotaEmpenho_SaldoPos_Internalname = sPrefix+"NOTAEMPENHO_SALDOPOS";
         lblTextblocknotaempenho_ativo_Internalname = sPrefix+"TEXTBLOCKNOTAEMPENHO_ATIVO";
         chkNotaEmpenho_Ativo_Internalname = sPrefix+"NOTAEMPENHO_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtNotaEmpenho_SaldoPos_Jsonclick = "";
         edtNotaEmpenho_SaldoAnt_Jsonclick = "";
         edtNotaEmpenho_Qtd_Jsonclick = "";
         edtNotaEmpenho_Valor_Jsonclick = "";
         edtNotaEmpenho_DEmissao_Jsonclick = "";
         edtNotaEmpenho_Itentificador_Jsonclick = "";
         edtSaldoContrato_Codigo_Jsonclick = "";
         edtNotaEmpenho_Codigo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         chkNotaEmpenho_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13M82',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14M82',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00M82_A1560NotaEmpenho_Codigo = new int[1] ;
         H00M82_A74Contrato_Codigo = new int[1] ;
         H00M82_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         H00M82_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         H00M82_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         H00M82_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         H00M82_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         H00M82_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         H00M82_A1567NotaEmpenho_Qtd = new decimal[1] ;
         H00M82_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         H00M82_A1566NotaEmpenho_Valor = new decimal[1] ;
         H00M82_n1566NotaEmpenho_Valor = new bool[] {false} ;
         H00M82_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         H00M82_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         H00M82_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         H00M82_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         H00M82_A1561SaldoContrato_Codigo = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocknotaempenho_codigo_Jsonclick = "";
         lblTextblocksaldocontrato_codigo_Jsonclick = "";
         lblTextblocknotaempenho_itentificador_Jsonclick = "";
         lblTextblocknotaempenho_demissao_Jsonclick = "";
         lblTextblocknotaempenho_valor_Jsonclick = "";
         lblTextblocknotaempenho_qtd_Jsonclick = "";
         lblTextblocknotaempenho_saldoant_Jsonclick = "";
         lblTextblocknotaempenho_saldopos_Jsonclick = "";
         lblTextblocknotaempenho_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1560NotaEmpenho_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.notaempenhogeneral__default(),
            new Object[][] {
                new Object[] {
               H00M82_A1560NotaEmpenho_Codigo, H00M82_A74Contrato_Codigo, H00M82_A1570NotaEmpenho_Ativo, H00M82_n1570NotaEmpenho_Ativo, H00M82_A1569NotaEmpenho_SaldoPos, H00M82_n1569NotaEmpenho_SaldoPos, H00M82_A1568NotaEmpenho_SaldoAnt, H00M82_n1568NotaEmpenho_SaldoAnt, H00M82_A1567NotaEmpenho_Qtd, H00M82_n1567NotaEmpenho_Qtd,
               H00M82_A1566NotaEmpenho_Valor, H00M82_n1566NotaEmpenho_Valor, H00M82_A1565NotaEmpenho_DEmissao, H00M82_n1565NotaEmpenho_DEmissao, H00M82_A1564NotaEmpenho_Itentificador, H00M82_n1564NotaEmpenho_Itentificador, H00M82_A1561SaldoContrato_Codigo
               }
            }
         );
         AV14Pgmname = "NotaEmpenhoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "NotaEmpenhoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1560NotaEmpenho_Codigo ;
      private int wcpOA1560NotaEmpenho_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7NotaEmpenho_Codigo ;
      private int idxLst ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1564NotaEmpenho_Itentificador ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkNotaEmpenho_Ativo_Internalname ;
      private String scmdbuf ;
      private String edtSaldoContrato_Codigo_Internalname ;
      private String edtNotaEmpenho_Itentificador_Internalname ;
      private String edtNotaEmpenho_DEmissao_Internalname ;
      private String edtNotaEmpenho_Valor_Internalname ;
      private String edtNotaEmpenho_Qtd_Internalname ;
      private String edtNotaEmpenho_SaldoAnt_Internalname ;
      private String edtNotaEmpenho_SaldoPos_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocknotaempenho_codigo_Internalname ;
      private String lblTextblocknotaempenho_codigo_Jsonclick ;
      private String edtNotaEmpenho_Codigo_Internalname ;
      private String edtNotaEmpenho_Codigo_Jsonclick ;
      private String lblTextblocksaldocontrato_codigo_Internalname ;
      private String lblTextblocksaldocontrato_codigo_Jsonclick ;
      private String edtSaldoContrato_Codigo_Jsonclick ;
      private String lblTextblocknotaempenho_itentificador_Internalname ;
      private String lblTextblocknotaempenho_itentificador_Jsonclick ;
      private String edtNotaEmpenho_Itentificador_Jsonclick ;
      private String lblTextblocknotaempenho_demissao_Internalname ;
      private String lblTextblocknotaempenho_demissao_Jsonclick ;
      private String edtNotaEmpenho_DEmissao_Jsonclick ;
      private String lblTextblocknotaempenho_valor_Internalname ;
      private String lblTextblocknotaempenho_valor_Jsonclick ;
      private String edtNotaEmpenho_Valor_Jsonclick ;
      private String lblTextblocknotaempenho_qtd_Internalname ;
      private String lblTextblocknotaempenho_qtd_Jsonclick ;
      private String edtNotaEmpenho_Qtd_Jsonclick ;
      private String lblTextblocknotaempenho_saldoant_Internalname ;
      private String lblTextblocknotaempenho_saldoant_Jsonclick ;
      private String edtNotaEmpenho_SaldoAnt_Jsonclick ;
      private String lblTextblocknotaempenho_saldopos_Internalname ;
      private String lblTextblocknotaempenho_saldopos_Jsonclick ;
      private String edtNotaEmpenho_SaldoPos_Jsonclick ;
      private String lblTextblocknotaempenho_ativo_Internalname ;
      private String lblTextblocknotaempenho_ativo_Jsonclick ;
      private String sCtrlA1560NotaEmpenho_Codigo ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1566NotaEmpenho_Valor ;
      private bool n1565NotaEmpenho_DEmissao ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkNotaEmpenho_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00M82_A1560NotaEmpenho_Codigo ;
      private int[] H00M82_A74Contrato_Codigo ;
      private bool[] H00M82_A1570NotaEmpenho_Ativo ;
      private bool[] H00M82_n1570NotaEmpenho_Ativo ;
      private decimal[] H00M82_A1569NotaEmpenho_SaldoPos ;
      private bool[] H00M82_n1569NotaEmpenho_SaldoPos ;
      private decimal[] H00M82_A1568NotaEmpenho_SaldoAnt ;
      private bool[] H00M82_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] H00M82_A1567NotaEmpenho_Qtd ;
      private bool[] H00M82_n1567NotaEmpenho_Qtd ;
      private decimal[] H00M82_A1566NotaEmpenho_Valor ;
      private bool[] H00M82_n1566NotaEmpenho_Valor ;
      private DateTime[] H00M82_A1565NotaEmpenho_DEmissao ;
      private bool[] H00M82_n1565NotaEmpenho_DEmissao ;
      private String[] H00M82_A1564NotaEmpenho_Itentificador ;
      private bool[] H00M82_n1564NotaEmpenho_Itentificador ;
      private int[] H00M82_A1561SaldoContrato_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class notaempenhogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00M82 ;
          prmH00M82 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00M82", "SELECT T1.[NotaEmpenho_Codigo], T2.[Contrato_Codigo], T1.[NotaEmpenho_Ativo], T1.[NotaEmpenho_SaldoPos], T1.[NotaEmpenho_SaldoAnt], T1.[NotaEmpenho_Qtd], T1.[NotaEmpenho_Valor], T1.[NotaEmpenho_DEmissao], T1.[NotaEmpenho_Itentificador], T1.[SaldoContrato_Codigo] FROM ([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) WHERE T1.[NotaEmpenho_Codigo] = @NotaEmpenho_Codigo ORDER BY T1.[NotaEmpenho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M82,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
