/*
               File: WP_ConsultaContadores
        Description: Consulta de Contadores
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:21:22.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultacontadores : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultacontadores( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultacontadores( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavAreatrabalho_codigo = new GXCombobox();
         dynavFiltro_contadorfmcod = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         cmbavFiltro_ano = new GXCombobox();
         cmbavFiltro_mes = new GXCombobox();
         cmbavMes = new GXCombobox();
         cmbavAno = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vAREATRABALHO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvAREATRABALHO_CODIGOBU2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFILTRO_CONTADORFMCOD") == 0 )
            {
               AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
               AV100Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100Contratada_PessoaCod), 6, 0)));
               AV83Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvFILTRO_CONTADORFMCODBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
               AV83Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDSVvSERVICO_CODIGOBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_36 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_36_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_36_idx = GetNextPar( );
               edtavQtdedmn_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               edtavQtdedmn_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV82Contratadas);
               AV30Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV68SDT_FiltroConsContadorFM);
               A474ContagemResultado_ContadorFMNom = GetNextPar( );
               n474ContagemResultado_ContadorFMNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
               A470ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A470ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0)));
               AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0)));
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV19WWPContext);
               A999ContagemResultado_CrFMEhContratada = (bool)(BooleanUtil.Val(GetNextPar( )));
               n999ContagemResultado_CrFMEhContratada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A999ContagemResultado_CrFMEhContratada", A999ContagemResultado_CrFMEhContratada);
               A1000ContagemResultado_CrFMEhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
               n1000ContagemResultado_CrFMEhContratante = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1000ContagemResultado_CrFMEhContratante", A1000ContagemResultado_CrFMEhContratante);
               AV73ContagemResultado_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ContagemResultado_Data", context.localUtil.Format(AV73ContagemResultado_Data, "99/99/99"));
               A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               AV29Filtro_Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)));
               A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n601ContagemResultado_Servico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
               AV75Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0)));
               A1583ContagemResultado_TipoRegistro = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
               A517ContagemResultado_Ultima = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
               A1616ContagemResultado_CntVlrUndCnt = NumberUtil.Val( GetNextPar( ), ".");
               n1616ContagemResultado_CntVlrUndCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1616ContagemResultado_CntVlrUndCnt", StringUtil.LTrim( StringUtil.Str( A1616ContagemResultado_CntVlrUndCnt, 18, 5)));
               A1615ContagemResultado_CntIndDvr = NumberUtil.Val( GetNextPar( ), ".");
               n1615ContagemResultado_CntIndDvr = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1615ContagemResultado_CntIndDvr", StringUtil.LTrim( StringUtil.Str( A1615ContagemResultado_CntIndDvr, 6, 2)));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A458ContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
               n458ContagemResultado_PFBFS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
               A460ContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
               n460ContagemResultado_PFBFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
               A833ContagemResultado_CstUntPrd = NumberUtil.Val( GetNextPar( ), ".");
               n833ContagemResultado_CstUntPrd = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A833ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( A833ContagemResultado_CstUntPrd, 18, 5)));
               A574ContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               A512ContagemResultado_ValorPF = NumberUtil.Val( GetNextPar( ), ".");
               n512ContagemResultado_ValorPF = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A512ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( A512ContagemResultado_ValorPF, 18, 5)));
               A1043ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1043ContagemResultado_LiqLogCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV97Codigo);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV96SDT_Codigos);
               AV45tQtdeDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdeDmn), 8, 0)));
               AV44tPFBFS = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tPFBFS", StringUtil.LTrim( StringUtil.Str( AV44tPFBFS, 14, 5)));
               AV43tPFBFM = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43tPFBFM", StringUtil.LTrim( StringUtil.Str( AV43tPFBFM, 14, 5)));
               AV70tPFFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70tPFFinal", StringUtil.LTrim( StringUtil.Str( AV70tPFFinal, 14, 5)));
               AV42tCTPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
               AV46tRTPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tRTPF", StringUtil.LTrim( StringUtil.Str( AV46tRTPF, 18, 5)));
               AV65tErros = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65tErros), 8, 0)));
               AV61tPendencias = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61tPendencias), 8, 0)));
               AV62tCanceladas = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62tCanceladas), 8, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( AV82Contratadas, AV30Filtro_Ano, AV68SDT_FiltroConsContadorFM, A474ContagemResultado_ContadorFMNom, A470ContagemResultado_ContadorFMCod, AV31Filtro_ContadorFMCod, A490ContagemResultado_ContratadaCod, AV19WWPContext, A999ContagemResultado_CrFMEhContratada, A1000ContagemResultado_CrFMEhContratante, AV73ContagemResultado_Data, A473ContagemResultado_DataCnt, AV29Filtro_Mes, A601ContagemResultado_Servico, AV75Servico_Codigo, A1583ContagemResultado_TipoRegistro, A517ContagemResultado_Ultima, A1616ContagemResultado_CntVlrUndCnt, A1615ContagemResultado_CntIndDvr, A484ContagemResultado_StatusDmn, A458ContagemResultado_PFBFS, A460ContagemResultado_PFBFM, A833ContagemResultado_CstUntPrd, A574ContagemResultado_PFFinal, A512ContagemResultado_ValorPF, A1043ContagemResultado_LiqLogCod, A456ContagemResultado_Codigo, AV97Codigo, AV96SDT_Codigos, AV45tQtdeDmn, AV44tPFBFS, AV43tPFBFM, AV70tPFFinal, AV42tCTPF, AV46tRTPF, AV65tErros, AV61tPendencias, AV62tCanceladas) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABU2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBU2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216212277");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultacontadores.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_36", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_36), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINNEWWINDOWTARGETS", AV98InNewWindowTargets);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINNEWWINDOWTARGETS", AV98InNewWindowTargets);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAS", AV82Contratadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAS", AV82Contratadas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_FILTROCONSCONTADORFM", AV68SDT_FiltroConsContadorFM);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_FILTROCONSCONTADORFM", AV68SDT_FiltroConsContadorFM);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMNOM", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV19WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV19WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_CRFMEHCONTRATADA", A999ContagemResultado_CrFMEhContratada);
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_CRFMEHCONTRATANTE", A1000ContagemResultado_CrFMEhContratante);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DATA", context.localUtil.DToC( AV73ContagemResultado_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( A473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_TIPOREGISTRO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_ULTIMA", A517ContagemResultado_Ultima);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTVLRUNDCNT", StringUtil.LTrim( StringUtil.NToC( A1616ContagemResultado_CntVlrUndCnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTINDDVR", StringUtil.LTrim( StringUtil.NToC( A1615ContagemResultado_CntIndDvr, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CSTUNTPRD", StringUtil.LTrim( StringUtil.NToC( A833ContagemResultado_CstUntPrd, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORPF", StringUtil.LTrim( StringUtil.NToC( A512ContagemResultado_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_LIQLOGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGO", AV97Codigo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGO", AV97Codigo);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CODIGOS", AV96SDT_Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CODIGOS", AV96SDT_Codigos);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTARGET", AV99Target);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTARGET", AV99Target);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, "vQTDEDMN_Tooltiptext", StringUtil.RTrim( edtavQtdedmn_Tooltiptext));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBU2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBU2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultacontadores.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaContadores" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta de Contadores" ;
      }

      protected void WBBU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_BU2( true) ;
         }
         else
         {
            wb_table1_2_BU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83Contratada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83Contratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratada_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ConsultaContadores.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV100Contratada_PessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV100Contratada_PessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavContratada_pessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJava_Internalname, lblJava_Caption, "", "", lblJava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 1, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</p>") ;
         }
         wbLoad = true;
      }

      protected void STARTBU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta de Contadores", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBU0( ) ;
      }

      protected void WSBU2( )
      {
         STARTBU2( ) ;
         EVTBU2( ) ;
      }

      protected void EVTBU2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11BU2 */
                              E11BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOGRAFICOSDMN'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12BU2 */
                              E12BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOGRAFICOSPFB'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13BU2 */
                              E13BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOGRAFICOSPRD'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14BU2 */
                              E14BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXPORT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15BU2 */
                              E15BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCFPS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16BU2 */
                              E16BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFILTRO_MES.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17BU2 */
                              E17BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFILTRO_DIA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18BU2 */
                              E18BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFILTRO_CONTADORFMCOD.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19BU2 */
                              E19BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20BU2 */
                              E20BU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "GRID1.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VQTDEDMN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "VQTDESEMLIQ.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VLIQUIDAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VQTDEDMN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "VQTDESEMLIQ.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VLIQUIDAR.CLICK") == 0 ) )
                           {
                              nGXsfl_36_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
                              SubsflControlProps_362( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_CONTADORFMCOD");
                                 GX_FocusControl = edtavContagemresultado_contadorfmcod_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV67ContagemResultado_ContadorFMCod = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ContadorFMCod), 6, 0)));
                              }
                              else
                              {
                                 AV67ContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ContadorFMCod), 6, 0)));
                              }
                              AV20ContagemResultado_ContadorFMNom = StringUtil.Upper( cgiGet( edtavContagemresultado_contadorfmnom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmnom_Internalname, AV20ContagemResultado_ContadorFMNom);
                              cmbavMes.Name = cmbavMes_Internalname;
                              cmbavMes.CurrentValue = cgiGet( cmbavMes_Internalname);
                              AV17Mes = (long)(NumberUtil.Val( cgiGet( cmbavMes_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Mes), 10, 0)));
                              cmbavAno.Name = cmbavAno_Internalname;
                              cmbavAno.CurrentValue = cgiGet( cmbavAno_Internalname);
                              AV16Ano = (short)(NumberUtil.Val( cgiGet( cmbavAno_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Ano), 4, 0)));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFS");
                                 GX_FocusControl = edtavPfbfs_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV24PFBFS = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV24PFBFS, 14, 5)));
                              }
                              else
                              {
                                 AV24PFBFS = context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV24PFBFS, 14, 5)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFM");
                                 GX_FocusControl = edtavPfbfm_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV22PFBFM = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22PFBFM, 14, 5)));
                              }
                              else
                              {
                                 AV22PFBFM = context.localUtil.CToN( cgiGet( edtavPfbfm_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22PFBFM, 14, 5)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFFINAL");
                                 GX_FocusControl = edtavPffinal_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV71PFFinal = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV71PFFinal, 14, 5)));
                              }
                              else
                              {
                                 AV71PFFinal = context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV71PFFinal, 14, 5)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDEDMN");
                                 GX_FocusControl = edtavQtdedmn_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV21QtdeDmn = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21QtdeDmn), 8, 0)));
                              }
                              else
                              {
                                 AV21QtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21QtdeDmn), 8, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdesemliq_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdesemliq_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDESEMLIQ");
                                 GX_FocusControl = edtavQtdesemliq_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV76QtdeSemLiq = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdesemliq_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV76QtdeSemLiq), 8, 0)));
                              }
                              else
                              {
                                 AV76QtdeSemLiq = (int)(context.localUtil.CToN( cgiGet( edtavQtdesemliq_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdesemliq_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV76QtdeSemLiq), 8, 0)));
                              }
                              AV79Liquidar = cgiGet( edtavLiquidar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLiquidar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV79Liquidar)) ? AV105Liquidar_GXI : context.convertURL( context.PathToRelativeUrl( AV79Liquidar))));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavErros_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavErros_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vERROS");
                                 GX_FocusControl = edtavErros_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV57Erros = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavErros_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Erros), 8, 0)));
                              }
                              else
                              {
                                 AV57Erros = (int)(context.localUtil.CToN( cgiGet( edtavErros_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavErros_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Erros), 8, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPendencias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPendencias_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPENDENCIAS");
                                 GX_FocusControl = edtavPendencias_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV55Pendencias = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPendencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV55Pendencias), 8, 0)));
                              }
                              else
                              {
                                 AV55Pendencias = (int)(context.localUtil.CToN( cgiGet( edtavPendencias_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPendencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV55Pendencias), 8, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavCanceladas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCanceladas_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCANCELADAS");
                                 GX_FocusControl = edtavCanceladas_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV56Canceladas = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCanceladas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV56Canceladas), 8, 0)));
                              }
                              else
                              {
                                 AV56Canceladas = (int)(context.localUtil.CToN( cgiGet( edtavCanceladas_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCanceladas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV56Canceladas), 8, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavCtpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCTPF");
                                 GX_FocusControl = edtavCtpf_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV27CTPF = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV27CTPF, 18, 5)));
                              }
                              else
                              {
                                 AV27CTPF = context.localUtil.CToN( cgiGet( edtavCtpf_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV27CTPF, 18, 5)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavRtpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavRtpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vRTPF");
                                 GX_FocusControl = edtavRtpf_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV28RTPF = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV28RTPF, 18, 5)));
                              }
                              else
                              {
                                 AV28RTPF = context.localUtil.CToN( cgiGet( edtavRtpf_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV28RTPF, 18, 5)));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21BU2 */
                                    E21BU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20BU2 */
                                    E20BU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22BU2 */
                                    E22BU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VQTDEDMN.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23BU2 */
                                    E23BU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VQTDESEMLIQ.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24BU2 */
                                    E24BU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VLIQUIDAR.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25BU2 */
                                    E25BU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavAreatrabalho_codigo.Name = "vAREATRABALHO_CODIGO";
            dynavAreatrabalho_codigo.WebTags = "";
            dynavFiltro_contadorfmcod.Name = "vFILTRO_CONTADORFMCOD";
            dynavFiltro_contadorfmcod.WebTags = "";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavFiltro_ano.Name = "vFILTRO_ANO";
            cmbavFiltro_ano.WebTags = "";
            cmbavFiltro_ano.addItem("2014", "2014", 0);
            cmbavFiltro_ano.addItem("2015", "2015", 0);
            if ( cmbavFiltro_ano.ItemCount > 0 )
            {
               AV30Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0)));
            }
            cmbavFiltro_mes.Name = "vFILTRO_MES";
            cmbavFiltro_mes.WebTags = "";
            cmbavFiltro_mes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavFiltro_mes.addItem("1", "Janeiro", 0);
            cmbavFiltro_mes.addItem("2", "Fevereiro", 0);
            cmbavFiltro_mes.addItem("3", "Mar�o", 0);
            cmbavFiltro_mes.addItem("4", "Abril", 0);
            cmbavFiltro_mes.addItem("5", "Maio", 0);
            cmbavFiltro_mes.addItem("6", "Junho", 0);
            cmbavFiltro_mes.addItem("7", "Julho", 0);
            cmbavFiltro_mes.addItem("8", "Agosto", 0);
            cmbavFiltro_mes.addItem("9", "Setembro", 0);
            cmbavFiltro_mes.addItem("10", "Outubro", 0);
            cmbavFiltro_mes.addItem("11", "Novembro", 0);
            cmbavFiltro_mes.addItem("12", "Dezembro", 0);
            if ( cmbavFiltro_mes.ItemCount > 0 )
            {
               AV29Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)));
            }
            GXCCtl = "vMES_" + sGXsfl_36_idx;
            cmbavMes.Name = GXCCtl;
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV17Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Mes), 10, 0)));
            }
            GXCCtl = "vANO_" + sGXsfl_36_idx;
            cmbavAno.Name = GXCCtl;
            cmbavAno.WebTags = "";
            cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "", 0);
            cmbavAno.addItem("2014", "2014", 0);
            cmbavAno.addItem("2015", "2015", 0);
            if ( cmbavAno.ItemCount > 0 )
            {
               AV16Ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Ano), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvSERVICO_CODIGO_htmlBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
         GXVvSERVICO_CODIGO_htmlBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
         GXVvFILTRO_CONTADORFMCOD_htmlBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         GXVvFILTRO_CONTADORFMCOD_htmlBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         GXVvFILTRO_CONTADORFMCOD_htmlBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvAREATRABALHO_CODIGOBU2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAREATRABALHO_CODIGO_dataBU2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAREATRABALHO_CODIGO_htmlBU2( )
      {
         int gxdynajaxvalue ;
         GXDLVvAREATRABALHO_CODIGO_dataBU2( ) ;
         gxdynajaxindex = 1;
         dynavAreatrabalho_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvAREATRABALHO_CODIGO_dataBU2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         /* Using cursor H00BU2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BU2_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00BU2_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvFILTRO_CONTADORFMCODBU2( int AV18AreaTrabalho_Codigo ,
                                                    int AV100Contratada_PessoaCod ,
                                                    int AV83Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFILTRO_CONTADORFMCOD_dataBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFILTRO_CONTADORFMCOD_htmlBU2( int AV18AreaTrabalho_Codigo ,
                                                       int AV100Contratada_PessoaCod ,
                                                       int AV83Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvFILTRO_CONTADORFMCOD_dataBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavFiltro_contadorfmcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFiltro_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavFiltro_contadorfmcod.ItemCount > 0 )
         {
            AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0)));
         }
      }

      protected void GXDLVvFILTRO_CONTADORFMCOD_dataBU2( int AV18AreaTrabalho_Codigo ,
                                                         int AV100Contratada_PessoaCod ,
                                                         int AV83Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* Using cursor H00BU3 */
         pr_default.execute(1, new Object[] {AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00BU3_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00BU3_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDSVvSERVICO_CODIGOBU2( int AV18AreaTrabalho_Codigo ,
                                              int AV83Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvSERVICO_CODIGO_dataBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlBU2( int AV18AreaTrabalho_Codigo ,
                                                 int AV83Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDSVvSERVICO_CODIGO_dataBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV75Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDSVvSERVICO_CODIGO_dataBU2( int AV18AreaTrabalho_Codigo ,
                                                   int AV83Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         IGxCollection gxcolvSERVICO_CODIGO ;
         SdtSDT_Codigos gxcolitemvSERVICO_CODIGO ;
         new dp_servicoscontratados(context ).execute(  AV18AreaTrabalho_Codigo,  AV83Contratada_Codigo, out  gxcolvSERVICO_CODIGO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
         gxcolvSERVICO_CODIGO.Sort("Descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolvSERVICO_CODIGO.Count )
         {
            gxcolitemvSERVICO_CODIGO = ((SdtSDT_Codigos)gxcolvSERVICO_CODIGO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvSERVICO_CODIGO.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvSERVICO_CODIGO.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_362( ) ;
         while ( nGXsfl_36_idx <= nRC_GXsfl_36 )
         {
            sendrow_362( ) ;
            nGXsfl_36_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_36_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_36_idx+1));
            sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
            SubsflControlProps_362( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( IGxCollection AV82Contratadas ,
                                        short AV30Filtro_Ano ,
                                        SdtSDT_FiltroConsContadorFM AV68SDT_FiltroConsContadorFM ,
                                        String A474ContagemResultado_ContadorFMNom ,
                                        int A470ContagemResultado_ContadorFMCod ,
                                        int AV31Filtro_ContadorFMCod ,
                                        int A490ContagemResultado_ContratadaCod ,
                                        wwpbaseobjects.SdtWWPContext AV19WWPContext ,
                                        bool A999ContagemResultado_CrFMEhContratada ,
                                        bool A1000ContagemResultado_CrFMEhContratante ,
                                        DateTime AV73ContagemResultado_Data ,
                                        DateTime A473ContagemResultado_DataCnt ,
                                        long AV29Filtro_Mes ,
                                        int A601ContagemResultado_Servico ,
                                        int AV75Servico_Codigo ,
                                        short A1583ContagemResultado_TipoRegistro ,
                                        bool A517ContagemResultado_Ultima ,
                                        decimal A1616ContagemResultado_CntVlrUndCnt ,
                                        decimal A1615ContagemResultado_CntIndDvr ,
                                        String A484ContagemResultado_StatusDmn ,
                                        decimal A458ContagemResultado_PFBFS ,
                                        decimal A460ContagemResultado_PFBFM ,
                                        decimal A833ContagemResultado_CstUntPrd ,
                                        decimal A574ContagemResultado_PFFinal ,
                                        decimal A512ContagemResultado_ValorPF ,
                                        int A1043ContagemResultado_LiqLogCod ,
                                        int A456ContagemResultado_Codigo ,
                                        SdtSDT_Codigos AV97Codigo ,
                                        IGxCollection AV96SDT_Codigos ,
                                        int AV45tQtdeDmn ,
                                        decimal AV44tPFBFS ,
                                        decimal AV43tPFBFM ,
                                        decimal AV70tPFFinal ,
                                        decimal AV42tCTPF ,
                                        decimal AV46tRTPF ,
                                        int AV65tErros ,
                                        int AV61tPendencias ,
                                        int AV62tCanceladas )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFBU2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
         }
         if ( dynavFiltro_contadorfmcod.ItemCount > 0 )
         {
            AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV75Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0)));
         }
         if ( cmbavFiltro_ano.ItemCount > 0 )
         {
            AV30Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0)));
         }
         if ( cmbavFiltro_mes.ItemCount > 0 )
         {
            AV29Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0)));
         edtavContagemresultado_contadorfmnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmnom_Enabled), 5, 0)));
         cmbavMes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMes.Enabled), 5, 0)));
         cmbavAno.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAno.Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         edtavQtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdedmn_Enabled), 5, 0)));
         edtavQtdesemliq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdesemliq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdesemliq_Enabled), 5, 0)));
         edtavErros_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErros_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErros_Enabled), 5, 0)));
         edtavPendencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPendencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPendencias_Enabled), 5, 0)));
         edtavCanceladas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCanceladas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCanceladas_Enabled), 5, 0)));
         edtavCtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtpf_Enabled), 5, 0)));
         edtavRtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRtpf_Enabled), 5, 0)));
         edtavTpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfs_Enabled), 5, 0)));
         edtavTpfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfm_Enabled), 5, 0)));
         edtavTpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpffinal_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTerros_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTerros_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTerros_Enabled), 5, 0)));
         edtavTpendencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpendencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpendencias_Enabled), 5, 0)));
         edtavTcanceladas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTcanceladas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTcanceladas_Enabled), 5, 0)));
         edtavTctpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTctpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTctpf_Enabled), 5, 0)));
         edtavTrtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTrtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTrtpf_Enabled), 5, 0)));
      }

      protected void RFBU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 36;
         /* Execute user event: E20BU2 */
         E20BU2 ();
         nGXsfl_36_idx = 1;
         sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
         SubsflControlProps_362( ) ;
         nGXsfl_36_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "WorkWith");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
         Grid1Container.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Sortable), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_362( ) ;
            /* Execute user event: E22BU2 */
            E22BU2 ();
            wbEnd = 36;
            WBBU0( ) ;
         }
         nGXsfl_36_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPBU0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0)));
         edtavContagemresultado_contadorfmnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmnom_Enabled), 5, 0)));
         cmbavMes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMes.Enabled), 5, 0)));
         cmbavAno.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAno.Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         edtavQtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdedmn_Enabled), 5, 0)));
         edtavQtdesemliq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdesemliq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdesemliq_Enabled), 5, 0)));
         edtavErros_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavErros_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavErros_Enabled), 5, 0)));
         edtavPendencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPendencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPendencias_Enabled), 5, 0)));
         edtavCanceladas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCanceladas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCanceladas_Enabled), 5, 0)));
         edtavCtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtpf_Enabled), 5, 0)));
         edtavRtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRtpf_Enabled), 5, 0)));
         edtavTpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfs_Enabled), 5, 0)));
         edtavTpfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfm_Enabled), 5, 0)));
         edtavTpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpffinal_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTerros_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTerros_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTerros_Enabled), 5, 0)));
         edtavTpendencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpendencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpendencias_Enabled), 5, 0)));
         edtavTcanceladas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTcanceladas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTcanceladas_Enabled), 5, 0)));
         edtavTctpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTctpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTctpf_Enabled), 5, 0)));
         edtavTrtpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTrtpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTrtpf_Enabled), 5, 0)));
         GXVvAREATRABALHO_CODIGO_htmlBU2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21BU2 */
         E21BU2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vINNEWWINDOWTARGETS"), AV98InNewWindowTargets);
            /* Read variables values. */
            dynavAreatrabalho_codigo.Name = dynavAreatrabalho_codigo_Internalname;
            dynavAreatrabalho_codigo.CurrentValue = cgiGet( dynavAreatrabalho_codigo_Internalname);
            AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
            dynavFiltro_contadorfmcod.Name = dynavFiltro_contadorfmcod_Internalname;
            dynavFiltro_contadorfmcod.CurrentValue = cgiGet( dynavFiltro_contadorfmcod_Internalname);
            AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( dynavFiltro_contadorfmcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0)));
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV75Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0)));
            cmbavFiltro_ano.Name = cmbavFiltro_ano_Internalname;
            cmbavFiltro_ano.CurrentValue = cgiGet( cmbavFiltro_ano_Internalname);
            AV30Filtro_Ano = (short)(NumberUtil.Val( cgiGet( cmbavFiltro_ano_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0)));
            cmbavFiltro_mes.Name = cmbavFiltro_mes_Internalname;
            cmbavFiltro_mes.CurrentValue = cgiGet( cmbavFiltro_mes_Internalname);
            AV29Filtro_Mes = (long)(NumberUtil.Val( cgiGet( cmbavFiltro_mes_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFiltro_dia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFiltro_dia_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFILTRO_DIA");
               GX_FocusControl = edtavFiltro_dia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72Filtro_Dia = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72Filtro_Dia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72Filtro_Dia), 2, 0)));
            }
            else
            {
               AV72Filtro_Dia = (short)(context.localUtil.CToN( cgiGet( edtavFiltro_dia_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72Filtro_Dia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72Filtro_Dia), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFBFS");
               GX_FocusControl = edtavTpfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44tPFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tPFBFS", StringUtil.LTrim( StringUtil.Str( AV44tPFBFS, 14, 5)));
            }
            else
            {
               AV44tPFBFS = context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tPFBFS", StringUtil.LTrim( StringUtil.Str( AV44tPFBFS, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFBFM");
               GX_FocusControl = edtavTpfbfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43tPFBFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43tPFBFM", StringUtil.LTrim( StringUtil.Str( AV43tPFBFM, 14, 5)));
            }
            else
            {
               AV43tPFBFM = context.localUtil.CToN( cgiGet( edtavTpfbfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43tPFBFM", StringUtil.LTrim( StringUtil.Str( AV43tPFBFM, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFFINAL");
               GX_FocusControl = edtavTpffinal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70tPFFinal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70tPFFinal", StringUtil.LTrim( StringUtil.Str( AV70tPFFinal, 14, 5)));
            }
            else
            {
               AV70tPFFinal = context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70tPFFinal", StringUtil.LTrim( StringUtil.Str( AV70tPFFinal, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTQTDEDMN");
               GX_FocusControl = edtavTqtdedmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45tQtdeDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdeDmn), 8, 0)));
            }
            else
            {
               AV45tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdeDmn), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTerros_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTerros_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTERROS");
               GX_FocusControl = edtavTerros_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65tErros = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65tErros), 8, 0)));
            }
            else
            {
               AV65tErros = (int)(context.localUtil.CToN( cgiGet( edtavTerros_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65tErros), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpendencias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpendencias_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPENDENCIAS");
               GX_FocusControl = edtavTpendencias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61tPendencias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61tPendencias), 8, 0)));
            }
            else
            {
               AV61tPendencias = (int)(context.localUtil.CToN( cgiGet( edtavTpendencias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61tPendencias), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTcanceladas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTcanceladas_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTCANCELADAS");
               GX_FocusControl = edtavTcanceladas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62tCanceladas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62tCanceladas), 8, 0)));
            }
            else
            {
               AV62tCanceladas = (int)(context.localUtil.CToN( cgiGet( edtavTcanceladas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62tCanceladas), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTCTPF");
               GX_FocusControl = edtavTctpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42tCTPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
            }
            else
            {
               AV42tCTPF = context.localUtil.CToN( cgiGet( edtavTctpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTRTPF");
               GX_FocusControl = edtavTrtpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46tRTPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tRTPF", StringUtil.LTrim( StringUtil.Str( AV46tRTPF, 18, 5)));
            }
            else
            {
               AV46tRTPF = context.localUtil.CToN( cgiGet( edtavTrtpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tRTPF", StringUtil.LTrim( StringUtil.Str( AV46tRTPF, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_CODIGO");
               GX_FocusControl = edtavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83Contratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
            }
            else
            {
               AV83Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_pessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_pessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_PESSOACOD");
               GX_FocusControl = edtavContratada_pessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV100Contratada_PessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100Contratada_PessoaCod), 6, 0)));
            }
            else
            {
               AV100Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_pessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100Contratada_PessoaCod), 6, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_36 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_36"), ",", "."));
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21BU2 */
         E21BU2 ();
         if (returnInSub) return;
      }

      protected void E21BU2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV19WWPContext) ;
         AV18AreaTrabalho_Codigo = AV19WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
         AV83Contratada_Codigo = AV19WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
         AV100Contratada_PessoaCod = AV19WWPContext.gxTpr_Contratada_pessoacod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100Contratada_PessoaCod), 6, 0)));
         AV82Contratadas.Add(AV83Contratada_Codigo, 0);
         AV30Filtro_Ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0)));
         AV29Filtro_Mes = DateTimeUtil.Month( Gx_date);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)));
         if ( DateTimeUtil.Year( Gx_date) > 2015 )
         {
            AV34i = 2016;
            while ( AV34i <= DateTimeUtil.Year( Gx_date) )
            {
               cmbavFiltro_ano.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV34i), 4, 0)), StringUtil.Str( (decimal)(AV34i), 4, 0), 0);
               AV34i = (short)(AV34i+1);
            }
         }
         imgExportreport_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportreport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportreport_Visible), 5, 0)));
         edtavQtdedmn_Tooltiptext = "Visualizar demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
         lblJava_Caption = "<body onclick=\"myFunction(event)\">"+StringUtil.NewLine( )+"<script type=\"text/javascript\" language=\"javascript\">"+StringUtil.NewLine( )+"function myFunction(event) {var x = event.target;"+StringUtil.NewLine( )+"if(x && x == 'BODY'){"+"gx.evt.execEvt('E\\'CalculaDivergencia\\'.',this);"+"}"+StringUtil.NewLine( )+"</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJava_Internalname, "Caption", lblJava_Caption);
         imgExportreport_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportreport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportreport_Visible), 5, 0)));
         imgExportgraphicdmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportgraphicdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportgraphicdmn_Visible), 5, 0)));
         imgExportgraphicpfb_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportgraphicpfb_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportgraphicpfb_Visible), 5, 0)));
         imgExportgraphicprd_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportgraphicprd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportgraphicprd_Visible), 5, 0)));
         edtavContratada_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Visible), 5, 0)));
         edtavContratada_pessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacod_Visible), 5, 0)));
      }

      protected void E20BU2( )
      {
         /* Grid1_Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV19WWPContext) ;
         AV83Contratada_Codigo = AV19WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Contratada_Codigo), 6, 0)));
         AV100Contratada_PessoaCod = AV19WWPContext.gxTpr_Contratada_pessoacod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100Contratada_PessoaCod), 6, 0)));
         if ( (0==AV83Contratada_Codigo) )
         {
            GX_msglist.addItem("O usu�rio logado n�o esta associado a nenhuma Contratada, necess�rio para filtrar os Colaboradores!");
         }
         else
         {
            if ( AV82Contratadas.IndexOf(AV83Contratada_Codigo) == 0 )
            {
               AV82Contratadas.Add(AV83Contratada_Codigo, 0);
            }
         }
         AV16Ano = AV30Filtro_Ano;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Ano), 4, 0)));
         AV45tQtdeDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdeDmn), 8, 0)));
         AV44tPFBFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tPFBFS", StringUtil.LTrim( StringUtil.Str( AV44tPFBFS, 14, 5)));
         AV43tPFBFM = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43tPFBFM", StringUtil.LTrim( StringUtil.Str( AV43tPFBFM, 14, 5)));
         AV70tPFFinal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70tPFFinal", StringUtil.LTrim( StringUtil.Str( AV70tPFFinal, 14, 5)));
         AV42tCTPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
         AV46tRTPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tRTPF", StringUtil.LTrim( StringUtil.Str( AV46tRTPF, 18, 5)));
         AV61tPendencias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61tPendencias), 8, 0)));
         AV62tCanceladas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62tCanceladas), 8, 0)));
         AV65tErros = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65tErros), 8, 0)));
         AV96SDT_Codigos.Clear();
         if ( AV19WWPContext.gxTpr_Userehcontratante )
         {
            edtavQtdesemliq_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdesemliq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdesemliq_Visible), 5, 0)));
            edtavLiquidar_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLiquidar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLiquidar_Visible), 5, 0)));
            edtavLiquidar_Title = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLiquidar_Internalname, "Title", edtavLiquidar_Title);
            dynavAreatrabalho_codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo.Enabled), 5, 0)));
         }
         else
         {
            edtavQtdesemliq_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdesemliq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdesemliq_Visible), 5, 0)));
            edtavLiquidar_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLiquidar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLiquidar_Visible), 5, 0)));
            edtavLiquidar_Title = "Liq";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLiquidar_Internalname, "Title", edtavLiquidar_Title);
            dynavAreatrabalho_codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo.Enabled), 5, 0)));
         }
         AV68SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = 0;
         AV68SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = 0;
         AV68SDT_FiltroConsContadorFM.gxTpr_Mes = 0;
         AV68SDT_FiltroConsContadorFM.gxTpr_Ano = 0;
         AV68SDT_FiltroConsContadorFM.gxTpr_Comerro = false;
         AV68SDT_FiltroConsContadorFM.gxTpr_Abertas = false;
         AV68SDT_FiltroConsContadorFM.gxTpr_Solicitadas = false;
         AV68SDT_FiltroConsContadorFM.gxTpr_Servico = 0;
         AV68SDT_FiltroConsContadorFM.gxTpr_Nome = "";
         AV68SDT_FiltroConsContadorFM.gxTpr_Semliquidar = false;
         if ( dynavFiltro_contadorfmcod.ItemCount == 1 )
         {
            dynavFiltro_contadorfmcod.removeAllItems();
            dynavFiltro_contadorfmcod.addItem("0", "(Nenhum)", 0);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19WWPContext", AV19WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Contratadas", AV82Contratadas);
         cmbavAno.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Ano), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Values", cmbavAno.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96SDT_Codigos", AV96SDT_Codigos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68SDT_FiltroConsContadorFM", AV68SDT_FiltroConsContadorFM);
         dynavFiltro_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFiltro_contadorfmcod_Internalname, "Values", dynavFiltro_contadorfmcod.ToJavascriptSource());
      }

      private void E22BU2( )
      {
         /* Grid1_Load Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV82Contratadas ,
                                              AV31Filtro_ContadorFMCod ,
                                              AV19WWPContext.gxTpr_Userehcontratante ,
                                              AV73ContagemResultado_Data ,
                                              AV29Filtro_Mes ,
                                              AV75Servico_Codigo ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              A999ContagemResultado_CrFMEhContratada ,
                                              A1000ContagemResultado_CrFMEhContratante ,
                                              A473ContagemResultado_DataCnt ,
                                              AV30Filtro_Ano ,
                                              A601ContagemResultado_Servico ,
                                              A517ContagemResultado_Ultima ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.LONG, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00BU4 */
         pr_default.execute(2, new Object[] {AV31Filtro_ContadorFMCod, AV30Filtro_Ano, AV29Filtro_Mes, AV73ContagemResultado_Data, AV75Servico_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKBU3 = false;
            A1553ContagemResultado_CntSrvCod = H00BU4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00BU4_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = H00BU4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00BU4_n1603ContagemResultado_CntCod[0];
            A479ContagemResultado_CrFMPessoaCod = H00BU4_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00BU4_n479ContagemResultado_CrFMPessoaCod[0];
            A1583ContagemResultado_TipoRegistro = H00BU4_A1583ContagemResultado_TipoRegistro[0];
            A474ContagemResultado_ContadorFMNom = H00BU4_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00BU4_n474ContagemResultado_ContadorFMNom[0];
            A473ContagemResultado_DataCnt = H00BU4_A473ContagemResultado_DataCnt[0];
            A458ContagemResultado_PFBFS = H00BU4_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = H00BU4_n458ContagemResultado_PFBFS[0];
            A460ContagemResultado_PFBFM = H00BU4_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = H00BU4_n460ContagemResultado_PFBFM[0];
            A833ContagemResultado_CstUntPrd = H00BU4_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = H00BU4_n833ContagemResultado_CstUntPrd[0];
            A512ContagemResultado_ValorPF = H00BU4_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00BU4_n512ContagemResultado_ValorPF[0];
            A1043ContagemResultado_LiqLogCod = H00BU4_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = H00BU4_n1043ContagemResultado_LiqLogCod[0];
            A490ContagemResultado_ContratadaCod = H00BU4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00BU4_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00BU4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00BU4_n484ContagemResultado_StatusDmn[0];
            A517ContagemResultado_Ultima = H00BU4_A517ContagemResultado_Ultima[0];
            A601ContagemResultado_Servico = H00BU4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00BU4_n601ContagemResultado_Servico[0];
            A1000ContagemResultado_CrFMEhContratante = H00BU4_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = H00BU4_n1000ContagemResultado_CrFMEhContratante[0];
            A999ContagemResultado_CrFMEhContratada = H00BU4_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = H00BU4_n999ContagemResultado_CrFMEhContratada[0];
            A470ContagemResultado_ContadorFMCod = H00BU4_A470ContagemResultado_ContadorFMCod[0];
            A1616ContagemResultado_CntVlrUndCnt = H00BU4_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = H00BU4_n1616ContagemResultado_CntVlrUndCnt[0];
            A1615ContagemResultado_CntIndDvr = H00BU4_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = H00BU4_n1615ContagemResultado_CntIndDvr[0];
            A456ContagemResultado_Codigo = H00BU4_A456ContagemResultado_Codigo[0];
            A479ContagemResultado_CrFMPessoaCod = H00BU4_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00BU4_n479ContagemResultado_CrFMPessoaCod[0];
            A1000ContagemResultado_CrFMEhContratante = H00BU4_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = H00BU4_n1000ContagemResultado_CrFMEhContratante[0];
            A999ContagemResultado_CrFMEhContratada = H00BU4_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = H00BU4_n999ContagemResultado_CrFMEhContratada[0];
            A474ContagemResultado_ContadorFMNom = H00BU4_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00BU4_n474ContagemResultado_ContadorFMNom[0];
            A1553ContagemResultado_CntSrvCod = H00BU4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00BU4_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = H00BU4_A1583ContagemResultado_TipoRegistro[0];
            A512ContagemResultado_ValorPF = H00BU4_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00BU4_n512ContagemResultado_ValorPF[0];
            A1043ContagemResultado_LiqLogCod = H00BU4_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = H00BU4_n1043ContagemResultado_LiqLogCod[0];
            A490ContagemResultado_ContratadaCod = H00BU4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00BU4_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00BU4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00BU4_n484ContagemResultado_StatusDmn[0];
            A1603ContagemResultado_CntCod = H00BU4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00BU4_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = H00BU4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00BU4_n601ContagemResultado_Servico[0];
            A1616ContagemResultado_CntVlrUndCnt = H00BU4_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = H00BU4_n1616ContagemResultado_CntVlrUndCnt[0];
            A1615ContagemResultado_CntIndDvr = H00BU4_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = H00BU4_n1615ContagemResultado_CntIndDvr[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV79Liquidar = context.GetImagePath( "6235c4eb-f913-43b6-a04a-91a52d975b13", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLiquidar_Internalname, AV79Liquidar);
            AV105Liquidar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6235c4eb-f913-43b6-a04a-91a52d975b13", "", context.GetTheme( )));
            AV67ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ContadorFMCod), 6, 0)));
            AV20ContagemResultado_ContadorFMNom = A474ContagemResultado_ContadorFMNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmnom_Internalname, AV20ContagemResultado_ContadorFMNom);
            AV26CUPF = A1616ContagemResultado_CntVlrUndCnt;
            AV59Contrato_IndiceDivergencia = A1615ContagemResultado_CntIndDvr;
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV35mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV78mQtdeSemLiq[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV37mPFBFM[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV36mPFBFS[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV69mPFFinal[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV38mCTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV39mRTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV64mErros[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV69mPFFinal[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV58mPendencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV60mCanceladas[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(H00BU4_A474ContagemResultado_ContadorFMNom[0], A474ContagemResultado_ContadorFMNom) == 0 ) )
            {
               BRKBU3 = false;
               A479ContagemResultado_CrFMPessoaCod = H00BU4_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BU4_n479ContagemResultado_CrFMPessoaCod[0];
               A473ContagemResultado_DataCnt = H00BU4_A473ContagemResultado_DataCnt[0];
               A458ContagemResultado_PFBFS = H00BU4_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00BU4_n458ContagemResultado_PFBFS[0];
               A460ContagemResultado_PFBFM = H00BU4_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00BU4_n460ContagemResultado_PFBFM[0];
               A833ContagemResultado_CstUntPrd = H00BU4_A833ContagemResultado_CstUntPrd[0];
               n833ContagemResultado_CstUntPrd = H00BU4_n833ContagemResultado_CstUntPrd[0];
               A512ContagemResultado_ValorPF = H00BU4_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = H00BU4_n512ContagemResultado_ValorPF[0];
               A1043ContagemResultado_LiqLogCod = H00BU4_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = H00BU4_n1043ContagemResultado_LiqLogCod[0];
               A490ContagemResultado_ContratadaCod = H00BU4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00BU4_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = H00BU4_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00BU4_n484ContagemResultado_StatusDmn[0];
               A517ContagemResultado_Ultima = H00BU4_A517ContagemResultado_Ultima[0];
               A470ContagemResultado_ContadorFMCod = H00BU4_A470ContagemResultado_ContadorFMCod[0];
               A456ContagemResultado_Codigo = H00BU4_A456ContagemResultado_Codigo[0];
               A479ContagemResultado_CrFMPessoaCod = H00BU4_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BU4_n479ContagemResultado_CrFMPessoaCod[0];
               A512ContagemResultado_ValorPF = H00BU4_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = H00BU4_n512ContagemResultado_ValorPF[0];
               A1043ContagemResultado_LiqLogCod = H00BU4_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = H00BU4_n1043ContagemResultado_LiqLogCod[0];
               A490ContagemResultado_ContratadaCod = H00BU4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00BU4_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = H00BU4_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00BU4_n484ContagemResultado_StatusDmn[0];
               if ( (AV82Contratadas.IndexOf(A490ContagemResultado_ContratadaCod)>0) )
               {
                  /* Using cursor H00BU6 */
                  pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
                  if ( (pr_default.getStatus(3) != 101) )
                  {
                     A40000ContagemResultadoErro_Tipo = H00BU6_A40000ContagemResultadoErro_Tipo[0];
                     n40000ContagemResultadoErro_Tipo = H00BU6_n40000ContagemResultadoErro_Tipo[0];
                  }
                  else
                  {
                     A40000ContagemResultadoErro_Tipo = "";
                     n40000ContagemResultadoErro_Tipo = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40000ContagemResultadoErro_Tipo", A40000ContagemResultadoErro_Tipo);
                  }
                  pr_default.close(3);
                  GXt_decimal1 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A574ContagemResultado_PFFinal = GXt_decimal1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  AV34i = (short)(DateTimeUtil.Month( A473ContagemResultado_DataCnt));
                  AV35mQtdeDmn[AV34i-1] = (int)(AV35mQtdeDmn[AV34i-1]+1);
                  if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                  {
                     AV60mCanceladas[AV34i-1] = (int)(AV60mCanceladas[AV34i-1]+1);
                  }
                  else
                  {
                     AV36mPFBFS[AV34i-1] = (decimal)(AV36mPFBFS[AV34i-1]+A458ContagemResultado_PFBFS);
                     AV37mPFBFM[AV34i-1] = (decimal)(AV37mPFBFM[AV34i-1]+A460ContagemResultado_PFBFM);
                     AV38mCTPF[AV34i-1] = (decimal)(AV38mCTPF[AV34i-1]+(A460ContagemResultado_PFBFM*A833ContagemResultado_CstUntPrd));
                     AV39mRTPF[AV34i-1] = (decimal)(AV39mRTPF[AV34i-1]+(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF));
                     AV69mPFFinal[AV34i-1] = (decimal)(AV69mPFFinal[AV34i-1]+(((A458ContagemResultado_PFBFS>Convert.ToDecimal(0))&&(A458ContagemResultado_PFBFS<A460ContagemResultado_PFBFM) ? A458ContagemResultado_PFBFS : A460ContagemResultado_PFBFM)));
                     if ( StringUtil.StrCmp(A40000ContagemResultadoErro_Tipo, "") != 0 )
                     {
                        AV64mErros[AV34i-1] = (int)(AV64mErros[AV34i-1]+1);
                     }
                     if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 )
                     {
                        AV58mPendencias[AV34i-1] = (int)(AV58mPendencias[AV34i-1]+1);
                     }
                     if ( H00BU4_n1043ContagemResultado_LiqLogCod[0] && ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) ) )
                     {
                        AV78mQtdeSemLiq[AV34i-1] = (int)(AV78mQtdeSemLiq[AV34i-1]+1);
                     }
                     AV97Codigo.gxTpr_Codigo = A456ContagemResultado_Codigo;
                     AV96SDT_Codigos.Add(AV97Codigo, 0);
                     AV97Codigo = new SdtSDT_Codigos(context);
                  }
               }
               BRKBU3 = true;
               pr_default.readNext(2);
            }
            AV80Ini = 1;
            AV81Fim = 12;
            if ( ! (0==AV29Filtro_Mes) )
            {
               AV80Ini = (short)(AV29Filtro_Mes);
               AV81Fim = (short)(AV29Filtro_Mes);
            }
            AV34i = AV80Ini;
            while ( AV34i <= AV81Fim )
            {
               if ( AV35mQtdeDmn[AV34i-1] > 0 )
               {
                  AV17Mes = AV34i;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Mes), 10, 0)));
                  AV21QtdeDmn = AV35mQtdeDmn[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21QtdeDmn), 8, 0)));
                  AV76QtdeSemLiq = AV78mQtdeSemLiq[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdesemliq_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV76QtdeSemLiq), 8, 0)));
                  AV24PFBFS = AV36mPFBFS[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV24PFBFS, 14, 5)));
                  AV22PFBFM = AV37mPFBFM[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfm_Internalname, StringUtil.LTrim( StringUtil.Str( AV22PFBFM, 14, 5)));
                  AV71PFFinal = AV69mPFFinal[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV71PFFinal, 14, 5)));
                  AV27CTPF = AV38mCTPF[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV27CTPF, 18, 5)));
                  AV28RTPF = AV39mRTPF[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRtpf_Internalname, StringUtil.LTrim( StringUtil.Str( AV28RTPF, 18, 5)));
                  AV57Erros = AV64mErros[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavErros_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Erros), 8, 0)));
                  AV55Pendencias = AV58mPendencias[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPendencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV55Pendencias), 8, 0)));
                  AV56Canceladas = AV60mCanceladas[AV34i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCanceladas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV56Canceladas), 8, 0)));
                  edtavLiquidar_Visible = (((AV76QtdeSemLiq>0)) ? 1 : 0);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 36;
                  }
                  sendrow_362( ) ;
                  if ( isFullAjaxMode( ) && ( nGXsfl_36_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(36, Grid1Row);
                  }
                  AV20ContagemResultado_ContadorFMNom = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmnom_Internalname, AV20ContagemResultado_ContadorFMNom);
                  AV45tQtdeDmn = (int)(AV45tQtdeDmn+(AV35mQtdeDmn[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdeDmn), 8, 0)));
                  AV44tPFBFS = (decimal)(AV44tPFBFS+(AV36mPFBFS[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44tPFBFS", StringUtil.LTrim( StringUtil.Str( AV44tPFBFS, 14, 5)));
                  AV43tPFBFM = (decimal)(AV43tPFBFM+(AV37mPFBFM[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43tPFBFM", StringUtil.LTrim( StringUtil.Str( AV43tPFBFM, 14, 5)));
                  AV70tPFFinal = (decimal)(AV70tPFFinal+(AV69mPFFinal[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70tPFFinal", StringUtil.LTrim( StringUtil.Str( AV70tPFFinal, 14, 5)));
                  AV42tCTPF = (decimal)(AV42tCTPF+(AV38mCTPF[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42tCTPF", StringUtil.LTrim( StringUtil.Str( AV42tCTPF, 18, 5)));
                  AV46tRTPF = (decimal)(AV46tRTPF+(AV39mRTPF[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46tRTPF", StringUtil.LTrim( StringUtil.Str( AV46tRTPF, 18, 5)));
                  AV65tErros = (int)(AV65tErros+(AV64mErros[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65tErros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65tErros), 8, 0)));
                  AV61tPendencias = (int)(AV61tPendencias+(AV58mPendencias[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61tPendencias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61tPendencias), 8, 0)));
                  AV62tCanceladas = (int)(AV62tCanceladas+(AV60mCanceladas[AV34i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62tCanceladas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62tCanceladas), 8, 0)));
               }
               AV34i = (short)(AV34i+1);
            }
            if ( ! BRKBU3 )
            {
               BRKBU3 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV97Codigo", AV97Codigo);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV96SDT_Codigos", AV96SDT_Codigos);
         cmbavMes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Mes), 10, 0));
      }

      protected void E11BU2( )
      {
         /* Areatrabalho_codigo_Click Routine */
         AV82Contratadas.Clear();
         if ( (0==AV83Contratada_Codigo) )
         {
            GX_msglist.addItem("O usu�rio logado n�o esta associado a nenhuma Contratada, necess�ria para filtrar os Colaboradores!");
         }
         else
         {
            if ( AV18AreaTrabalho_Codigo == AV19WWPContext.gxTpr_Areatrabalho_codigo )
            {
               if ( AV82Contratadas.IndexOf(AV83Contratada_Codigo) == 0 )
               {
                  AV82Contratadas.Add(AV83Contratada_Codigo, 0);
               }
            }
            else
            {
               if ( AV18AreaTrabalho_Codigo > 0 )
               {
                  /* Using cursor H00BU7 */
                  pr_default.execute(4, new Object[] {AV100Contratada_PessoaCod, AV18AreaTrabalho_Codigo});
                  while ( (pr_default.getStatus(4) != 101) )
                  {
                     A40Contratada_PessoaCod = H00BU7_A40Contratada_PessoaCod[0];
                     A52Contratada_AreaTrabalhoCod = H00BU7_A52Contratada_AreaTrabalhoCod[0];
                     A39Contratada_Codigo = H00BU7_A39Contratada_Codigo[0];
                     AV82Contratadas.Add(A39Contratada_Codigo, 0);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(4);
                  }
                  pr_default.close(4);
               }
               else
               {
                  /* Using cursor H00BU8 */
                  pr_default.execute(5, new Object[] {AV100Contratada_PessoaCod});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A40Contratada_PessoaCod = H00BU8_A40Contratada_PessoaCod[0];
                     A39Contratada_Codigo = H00BU8_A39Contratada_Codigo[0];
                     AV82Contratadas.Add(A39Contratada_Codigo, 0);
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
               }
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Contratadas", AV82Contratadas);
      }

      protected void E17BU2( )
      {
         /* Filtro_mes_Click Routine */
      }

      protected void E18BU2( )
      {
         /* Filtro_dia_Isvalid Routine */
         if ( (0==AV72Filtro_Dia) || (0==AV29Filtro_Mes) || (0==AV30Filtro_Ano) )
         {
            AV73ContagemResultado_Data = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ContagemResultado_Data", context.localUtil.Format(AV73ContagemResultado_Data, "99/99/99"));
         }
         else
         {
            Gx_msg = "01/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)), 2, "0") + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0));
            AV73ContagemResultado_Data = context.localUtil.CToD( Gx_msg, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ContagemResultado_Data", context.localUtil.Format(AV73ContagemResultado_Data, "99/99/99"));
            if ( AV72Filtro_Dia > DateTimeUtil.Day( DateTimeUtil.DateEndOfMonth( AV73ContagemResultado_Data)) )
            {
               AV72Filtro_Dia = (short)(DateTimeUtil.Day( DateTimeUtil.DateEndOfMonth( AV73ContagemResultado_Data)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72Filtro_Dia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72Filtro_Dia), 2, 0)));
            }
            Gx_msg = StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV72Filtro_Dia), 2, 0)), 2, "0") + "/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)), 2, "0") + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0));
            AV73ContagemResultado_Data = context.localUtil.CToD( Gx_msg, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ContagemResultado_Data", context.localUtil.Format(AV73ContagemResultado_Data, "99/99/99"));
         }
      }

      protected void E19BU2( )
      {
         /* Filtro_contadorfmcod_Click Routine */
      }

      protected void E12BU2( )
      {
         /* 'DoGraficosDMN' Routine */
         AV66WebSession.Set("Contratadas", AV82Contratadas.ToXml(false, true, "Collection", ""));
         AV63Window.Height = 600;
         AV63Window.Width = 1000;
         AV63Window.Url = formatLink("wp_consultacontadoresgrf.aspx") + "?" + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV31Filtro_ContadorFMCod) + "," + UrlEncode("" +AV30Filtro_Ano) + "," + UrlEncode("" +AV29Filtro_Mes) + "," + UrlEncode(StringUtil.RTrim("DMN")) + "," + UrlEncode("" +AV75Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV19WWPContext.gxTpr_Userehcontratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV19WWPContext.gxTpr_Userehcontratante)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV73ContagemResultado_Data)) + "," + UrlEncode("" +AV19WWPContext.gxTpr_Contratada_pessoacod);
         AV63Window.SetReturnParms(new Object[] {"","","AV73ContagemResultado_Data","",});
         context.NewWindow(AV63Window);
      }

      protected void E13BU2( )
      {
         /* 'DoGraficosPFB' Routine */
         AV66WebSession.Set("Contratadas", AV82Contratadas.ToXml(false, true, "Collection", ""));
         AV63Window.Height = 600;
         AV63Window.Width = 1000;
         AV63Window.Url = formatLink("wp_consultacontadoresgrf.aspx") + "?" + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV31Filtro_ContadorFMCod) + "," + UrlEncode("" +AV30Filtro_Ano) + "," + UrlEncode("" +AV29Filtro_Mes) + "," + UrlEncode(StringUtil.RTrim("PFB")) + "," + UrlEncode("" +AV75Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV19WWPContext.gxTpr_Userehcontratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV19WWPContext.gxTpr_Userehcontratante)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV73ContagemResultado_Data)) + "," + UrlEncode("" +AV19WWPContext.gxTpr_Contratada_pessoacod);
         AV63Window.SetReturnParms(new Object[] {"","","AV73ContagemResultado_Data","",});
         context.NewWindow(AV63Window);
      }

      protected void E14BU2( )
      {
         /* 'DoGraficosPRD' Routine */
         AV66WebSession.Set("Contratadas", AV82Contratadas.ToXml(false, true, "Collection", ""));
         AV63Window.Height = 600;
         AV63Window.Width = 1000;
         AV63Window.Url = formatLink("wp_consultaareasgrf.aspx") + "?" + UrlEncode("" +AV31Filtro_ContadorFMCod) + "," + UrlEncode("" +AV75Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV19WWPContext.gxTpr_Userehcontratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV19WWPContext.gxTpr_Userehcontratante));
         AV63Window.SetReturnParms(new Object[] {"AV31Filtro_ContadorFMCod","","",});
         context.NewWindow(AV63Window);
      }

      protected void E23BU2( )
      {
         /* Qtdedmn_Click Routine */
         AV68SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = AV18AreaTrabalho_Codigo;
         AV68SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = AV67ContagemResultado_ContadorFMCod;
         AV68SDT_FiltroConsContadorFM.gxTpr_Mes = AV17Mes;
         AV68SDT_FiltroConsContadorFM.gxTpr_Ano = AV16Ano;
         AV68SDT_FiltroConsContadorFM.gxTpr_Servico = AV75Servico_Codigo;
         AV68SDT_FiltroConsContadorFM.gxTpr_Nome = AV20ContagemResultado_ContadorFMNom;
         AV66WebSession.Set("FiltroConsultaContador", AV68SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         Innewwindow_Target = formatLink("wwcontagemresultado.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68SDT_FiltroConsContadorFM", AV68SDT_FiltroConsContadorFM);
      }

      protected void E24BU2( )
      {
         /* Qtdesemliq_Click Routine */
         if ( AV76QtdeSemLiq > 0 )
         {
            AV68SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = AV18AreaTrabalho_Codigo;
            AV68SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = AV67ContagemResultado_ContadorFMCod;
            AV68SDT_FiltroConsContadorFM.gxTpr_Mes = AV29Filtro_Mes;
            AV68SDT_FiltroConsContadorFM.gxTpr_Ano = AV30Filtro_Ano;
            AV68SDT_FiltroConsContadorFM.gxTpr_Servico = AV75Servico_Codigo;
            AV68SDT_FiltroConsContadorFM.gxTpr_Nome = AV20ContagemResultado_ContadorFMNom+" - Sem liquidar";
            AV68SDT_FiltroConsContadorFM.gxTpr_Semliquidar = true;
            AV66WebSession.Set("FiltroConsultaContador", AV68SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
            Innewwindow_Target = formatLink("wwcontagemresultado.aspx") ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
            this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68SDT_FiltroConsContadorFM", AV68SDT_FiltroConsContadorFM);
      }

      protected void E25BU2( )
      {
         /* Liquidar_Click Routine */
         AV68SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = AV18AreaTrabalho_Codigo;
         AV68SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = AV67ContagemResultado_ContadorFMCod;
         AV68SDT_FiltroConsContadorFM.gxTpr_Mes = AV29Filtro_Mes;
         AV68SDT_FiltroConsContadorFM.gxTpr_Ano = AV30Filtro_Ano;
         AV68SDT_FiltroConsContadorFM.gxTpr_Servico = AV75Servico_Codigo;
         AV66WebSession.Set("FiltroConsultaContador", AV68SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         GXt_int2 = AV19WWPContext.gxTpr_Userid;
         new prc_liquidardemandas(context ).execute( ref  GXt_int2) ;
         AV19WWPContext.gxTpr_Userid = (short)(GXt_int2);
         gxgrGrid1_refresh( AV82Contratadas, AV30Filtro_Ano, AV68SDT_FiltroConsContadorFM, A474ContagemResultado_ContadorFMNom, A470ContagemResultado_ContadorFMCod, AV31Filtro_ContadorFMCod, A490ContagemResultado_ContratadaCod, AV19WWPContext, A999ContagemResultado_CrFMEhContratada, A1000ContagemResultado_CrFMEhContratante, AV73ContagemResultado_Data, A473ContagemResultado_DataCnt, AV29Filtro_Mes, A601ContagemResultado_Servico, AV75Servico_Codigo, A1583ContagemResultado_TipoRegistro, A517ContagemResultado_Ultima, A1616ContagemResultado_CntVlrUndCnt, A1615ContagemResultado_CntIndDvr, A484ContagemResultado_StatusDmn, A458ContagemResultado_PFBFS, A460ContagemResultado_PFBFM, A833ContagemResultado_CstUntPrd, A574ContagemResultado_PFFinal, A512ContagemResultado_ValorPF, A1043ContagemResultado_LiqLogCod, A456ContagemResultado_Codigo, AV97Codigo, AV96SDT_Codigos, AV45tQtdeDmn, AV44tPFBFS, AV43tPFBFM, AV70tPFFinal, AV42tCTPF, AV46tRTPF, AV65tErros, AV61tPendencias, AV62tCanceladas) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68SDT_FiltroConsContadorFM", AV68SDT_FiltroConsContadorFM);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19WWPContext", AV19WWPContext);
      }

      protected void E15BU2( )
      {
         /* 'DoExport' Routine */
         AV66WebSession.Set("Codigos", AV96SDT_Codigos.ToXml(false, true, "SDT_CodigosCollection", "GxEv3Up14_MeetrikaVs3"));
         Innewwindow_Target = formatLink("wp_summaryadd.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
      }

      protected void E16BU2( )
      {
         /* 'DoCFPS' Routine */
         if ( (0==AV31Filtro_ContadorFMCod) )
         {
            GX_msglist.addItem("Para este relat�rio deve selecionar um colaborador!");
         }
         else
         {
            /* Execute user subroutine: 'LIMITARIFPUG' */
            S112 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV99Target", AV99Target);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV98InNewWindowTargets", AV98InNewWindowTargets);
      }

      protected void S112( )
      {
         /* 'LIMITARIFPUG' Routine */
         AV66WebSession.Set("Codigos", AV96SDT_Codigos.ToXml(false, true, "SDT_CodigosCollection", "GxEv3Up14_MeetrikaVs3"));
         GXt_decimal1 = (decimal)(6000);
         GXt_int3 = 10;
         new prc_limitarifpug(context ).execute( ref  GXt_decimal1, ref  GXt_int3) ;
         /* Execute user subroutine: 'CFPS' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'CFPS' Routine */
         AV99Target.gxTpr_Target = formatLink("arel_ifpug.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV19WWPContext.gxTpr_Username)) + "," + UrlEncode("" +AV19WWPContext.gxTpr_Contratada_codigo) + "," + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV31Filtro_ContadorFMCod) + "," + UrlEncode("" +AV75Servico_Codigo) + "," + UrlEncode("" +AV30Filtro_Ano) + "," + UrlEncode("" +AV29Filtro_Mes);
         AV98InNewWindowTargets.Add(AV99Target, 0);
         AV99Target = new SdtInNewWindowTargets_InNewWindowTargetsItem(context);
         AV99Target.gxTpr_Target = formatLink("arel_semplanilha.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV19WWPContext.gxTpr_Username)) + "," + UrlEncode("" +AV19WWPContext.gxTpr_Contratada_codigo) + "," + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV31Filtro_ContadorFMCod) + "," + UrlEncode("" +AV75Servico_Codigo) + "," + UrlEncode("" +AV30Filtro_Ano) + "," + UrlEncode("" +AV29Filtro_Mes);
         AV98InNewWindowTargets.Add(AV99Target, 0);
         AV99Target = new SdtInNewWindowTargets_InNewWindowTargetsItem(context);
         AV99Target.gxTpr_Target = formatLink("arel_ifpugsummary.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV19WWPContext.gxTpr_Username)) + "," + UrlEncode("" +AV19WWPContext.gxTpr_Contratada_codigo) + "," + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV31Filtro_ContadorFMCod) + "," + UrlEncode("" +AV75Servico_Codigo) + "," + UrlEncode("" +AV30Filtro_Ano) + "," + UrlEncode("" +AV29Filtro_Mes);
         AV98InNewWindowTargets.Add(AV99Target, 0);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
      }

      protected void wb_table1_2_BU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class=''>") ;
            wb_table2_8_BU2( true) ;
         }
         else
         {
            wb_table2_8_BU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_BU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "�rea de Trabalho:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAreatrabalho_codigo, dynavAreatrabalho_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)), 1, dynavAreatrabalho_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVAREATRABALHO_CODIGO.CLICK."+"'", "int", "", 1, dynavAreatrabalho_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_ConsultaContadores.htm");
            dynavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Values", (String)(dynavAreatrabalho_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Colaborador:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavFiltro_contadorfmcod, dynavFiltro_contadorfmcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0)), 1, dynavFiltro_contadorfmcod_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVFILTRO_CONTADORFMCOD.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_WP_ConsultaContadores.htm");
            dynavFiltro_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFiltro_contadorfmcod_Internalname, "Values", (String)(dynavFiltro_contadorfmcod.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Servi�o:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_WP_ConsultaContadores.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Ano:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_ano, cmbavFiltro_ano_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0)), 1, cmbavFiltro_ano_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_ConsultaContadores.htm");
            cmbavFiltro_ano.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30Filtro_Ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_ano_Internalname, "Values", (String)(cmbavFiltro_ano.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Mes:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_mes, cmbavFiltro_mes_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0)), 1, cmbavFiltro_mes_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVFILTRO_MES.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_WP_ConsultaContadores.htm");
            cmbavFiltro_mes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29Filtro_Mes), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_mes_Internalname, "Values", (String)(cmbavFiltro_mes.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Dia:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_dia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72Filtro_Dia), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV72Filtro_Dia), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_dia_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(36), 2, 0)+","+"null"+");", "Atualizar", bttButton1_Jsonclick, 5, "Atualizar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"36\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Mes") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Ano") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFBFS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFBFM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF Final") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtde Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavQtdesemliq_Visible==0) ? "display:none;" : "")+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Sem Liq") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(20), 4, 0))+"px"+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((edtavLiquidar_Visible==0) ? "display:none;" : "")+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( edtavLiquidar_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Erros") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Pend") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Canc") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "CT/PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "RT/PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "WorkWith");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Sortable), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV20ContagemResultado_ContadorFMNom));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contadorfmnom_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Mes), 10, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavMes.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Ano), 4, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavAno.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV24PFBFS, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfs_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV22PFBFM, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfm_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV71PFFinal, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPffinal_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21QtdeDmn), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdedmn_Enabled), 5, 0, ".", "")));
               Grid1Column.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavQtdedmn_Tooltiptext));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76QtdeSemLiq), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdesemliq_Enabled), 5, 0, ".", "")));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdesemliq_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV79Liquidar));
               Grid1Column.AddObjectProperty("Title", StringUtil.RTrim( edtavLiquidar_Title));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLiquidar_Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57Erros), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavErros_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55Pendencias), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPendencias_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56Canceladas), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCanceladas_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV27CTPF, 18, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtpf_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV28RTPF, 18, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRtpf_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 36 )
         {
            wbEnd = 0;
            nRC_GXsfl_36 = (short)(nGXsfl_36_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table3_54_BU2( true) ;
         }
         else
         {
            wb_table3_54_BU2( false) ;
         }
         return  ;
      }

      protected void wb_table3_54_BU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BU2e( true) ;
         }
         else
         {
            wb_table1_2_BU2e( false) ;
         }
      }

      protected void wb_table3_54_BU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "PFB FS: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV44tPFBFS, 14, 5, ",", "")), ((edtavTpfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV44tPFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV44tPFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfbfs_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfbfs_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal_Internalname, " - PFB FM: ", "", "", lblTextblockcontagemresultado_pfbfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfbfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV43tPFBFM, 14, 5, ",", "")), ((edtavTpfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV43tPFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV43tPFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfbfm_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfbfm_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal2_Internalname, " - PF Final: ", "", "", lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpffinal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV70tPFFinal, 14, 5, ",", "")), ((edtavTpffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV70tPFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV70tPFFinal, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpffinal_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpffinal_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal_Internalname, " - Dmn: ", "", "", lblTextblockcontagemresultado_pflfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTqtdedmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45tQtdeDmn), 8, 0, ",", "")), ((edtavTqtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45tQtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV45tQtdeDmn), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTqtdedmn_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTqtdedmn_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal2_Internalname, " - Erros: ", "", "", lblTextblockcontagemresultado_pflfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTerros_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65tErros), 8, 0, ",", "")), ((edtavTerros_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65tErros), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV65tErros), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTerros_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTerros_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pftotal2_Internalname, " - Pend: ", "", "", lblTextblockcontagemresultado_pftotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpendencias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61tPendencias), 8, 0, ",", "")), ((edtavTpendencias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61tPendencias), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV61tPendencias), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpendencias_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpendencias_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pftotal3_Internalname, " - Canc: ", "", "", lblTextblockcontagemresultado_pftotal3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTcanceladas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62tCanceladas), 8, 0, ",", "")), ((edtavTcanceladas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62tCanceladas), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV62tCanceladas), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTcanceladas_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTcanceladas_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pftotal_Internalname, " - R$: ", "", "", lblTextblockcontagemresultado_pftotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTctpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV42tCTPF, 18, 5, ",", "")), ((edtavTctpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV42tCTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTctpf_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTctpf_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pftotalpagar_Internalname, "R$: ", "", "", lblTextblockcontagemresultado_pftotalpagar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTrtpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV46tRTPF, 18, 5, ",", "")), ((edtavTrtpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV46tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV46tRTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTrtpf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTrtpf_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_54_BU2e( true) ;
         }
         else
         {
            wb_table3_54_BU2e( false) ;
         }
      }

      protected void wb_table2_8_BU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 10, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Consulta de Contadores", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExport_Internalname, context.GetImagePath( "da69a816-fd11-445b-8aaf-1a2f7f1acc93", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Exportar Excel!", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExport_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOEXPORT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCfps_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Relat�rios para CFPS", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCfps_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCFPS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportgraphicprd_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgExportgraphicprd_Visible, 1, "", "Grafico de Produtividade", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExportgraphicprd_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOGRAFICOSPRD\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportgraphicpfb_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgExportgraphicpfb_Visible, 1, "", "Grafico mensual de PF Bruto", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExportgraphicpfb_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOGRAFICOSPFB\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportgraphicdmn_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgExportgraphicdmn_Visible, 1, "", "Grafico mensual de Demandas", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExportgraphicdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOGRAFICOSDMN\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContadores.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportreport_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgExportreport_Visible, 1, "", "Oculto", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgExportreport_Jsonclick, "'"+""+"'"+",false,"+"'"+"e26bu1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_ConsultaContadores.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_BU2e( true) ;
         }
         else
         {
            wb_table2_8_BU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABU2( ) ;
         WSBU2( ) ;
         WEBU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216212499");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_consultacontadores.js", "?20206216212499");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_362( )
      {
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_36_idx;
         edtavContagemresultado_contadorfmnom_Internalname = "vCONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_36_idx;
         cmbavMes_Internalname = "vMES_"+sGXsfl_36_idx;
         cmbavAno_Internalname = "vANO_"+sGXsfl_36_idx;
         edtavPfbfs_Internalname = "vPFBFS_"+sGXsfl_36_idx;
         edtavPfbfm_Internalname = "vPFBFM_"+sGXsfl_36_idx;
         edtavPffinal_Internalname = "vPFFINAL_"+sGXsfl_36_idx;
         edtavQtdedmn_Internalname = "vQTDEDMN_"+sGXsfl_36_idx;
         edtavQtdesemliq_Internalname = "vQTDESEMLIQ_"+sGXsfl_36_idx;
         edtavLiquidar_Internalname = "vLIQUIDAR_"+sGXsfl_36_idx;
         edtavErros_Internalname = "vERROS_"+sGXsfl_36_idx;
         edtavPendencias_Internalname = "vPENDENCIAS_"+sGXsfl_36_idx;
         edtavCanceladas_Internalname = "vCANCELADAS_"+sGXsfl_36_idx;
         edtavCtpf_Internalname = "vCTPF_"+sGXsfl_36_idx;
         edtavRtpf_Internalname = "vRTPF_"+sGXsfl_36_idx;
      }

      protected void SubsflControlProps_fel_362( )
      {
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_36_fel_idx;
         edtavContagemresultado_contadorfmnom_Internalname = "vCONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_36_fel_idx;
         cmbavMes_Internalname = "vMES_"+sGXsfl_36_fel_idx;
         cmbavAno_Internalname = "vANO_"+sGXsfl_36_fel_idx;
         edtavPfbfs_Internalname = "vPFBFS_"+sGXsfl_36_fel_idx;
         edtavPfbfm_Internalname = "vPFBFM_"+sGXsfl_36_fel_idx;
         edtavPffinal_Internalname = "vPFFINAL_"+sGXsfl_36_fel_idx;
         edtavQtdedmn_Internalname = "vQTDEDMN_"+sGXsfl_36_fel_idx;
         edtavQtdesemliq_Internalname = "vQTDESEMLIQ_"+sGXsfl_36_fel_idx;
         edtavLiquidar_Internalname = "vLIQUIDAR_"+sGXsfl_36_fel_idx;
         edtavErros_Internalname = "vERROS_"+sGXsfl_36_fel_idx;
         edtavPendencias_Internalname = "vPENDENCIAS_"+sGXsfl_36_fel_idx;
         edtavCanceladas_Internalname = "vCANCELADAS_"+sGXsfl_36_fel_idx;
         edtavCtpf_Internalname = "vCTPF_"+sGXsfl_36_fel_idx;
         edtavRtpf_Internalname = "vRTPF_"+sGXsfl_36_fel_idx;
      }

      protected void sendrow_362( )
      {
         SubsflControlProps_362( ) ;
         WBBU0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_36_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_36_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 37,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contadorfmcod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ContagemResultado_ContadorFMCod), 6, 0, ",", "")),((edtavContagemresultado_contadorfmcod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV67ContagemResultado_ContadorFMCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV67ContagemResultado_ContadorFMCod), "ZZZZZ9")),TempTags+((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contadorfmcod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_contadorfmcod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultado_contadorfmnom_Enabled!=0)&&(edtavContagemresultado_contadorfmnom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 38,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contadorfmnom_Internalname,StringUtil.RTrim( AV20ContagemResultado_ContadorFMNom),StringUtil.RTrim( context.localUtil.Format( AV20ContagemResultado_ContadorFMNom, "@!")),TempTags+((edtavContagemresultado_contadorfmnom_Enabled!=0)&&(edtavContagemresultado_contadorfmnom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultado_contadorfmnom_Enabled!=0)&&(edtavContagemresultado_contadorfmnom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contadorfmnom_Jsonclick,(short)0,(String)"BootstrapAttribute100",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_contadorfmnom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 39,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         if ( ( nGXsfl_36_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vMES_" + sGXsfl_36_idx;
            cmbavMes.Name = GXCCtl;
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV17Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Mes), 10, 0)));
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavMes,(String)cmbavMes_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV17Mes), 10, 0)),(short)1,(String)cmbavMes_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavMes.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,39);\"" : " "),(String)"",(bool)true});
         cmbavMes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Mes), 10, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Values", (String)(cmbavMes.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavAno.Enabled!=0)&&(cmbavAno.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 40,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         if ( ( nGXsfl_36_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vANO_" + sGXsfl_36_idx;
            cmbavAno.Name = GXCCtl;
            cmbavAno.WebTags = "";
            cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "", 0);
            cmbavAno.addItem("2014", "2014", 0);
            cmbavAno.addItem("2015", "2015", 0);
            if ( cmbavAno.ItemCount > 0 )
            {
               AV16Ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Ano), 4, 0)));
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavAno,(String)cmbavAno_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV16Ano), 4, 0)),(short)1,(String)cmbavAno_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavAno.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavAno.Enabled!=0)&&(cmbavAno.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavAno.Enabled!=0)&&(cmbavAno.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,40);\"" : " "),(String)"",(bool)true});
         cmbavAno.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Ano), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Values", (String)(cmbavAno.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 41,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfs_Internalname,StringUtil.LTrim( StringUtil.NToC( AV24PFBFS, 14, 5, ",", "")),((edtavPfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV24PFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV24PFBFS, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 42,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfm_Internalname,StringUtil.LTrim( StringUtil.NToC( AV22PFBFM, 14, 5, ",", "")),((edtavPfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV22PFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV22PFBFM, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,42);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 43,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPffinal_Internalname,StringUtil.LTrim( StringUtil.NToC( AV71PFFinal, 14, 5, ",", "")),((edtavPffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV71PFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV71PFFinal, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,43);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPffinal_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 44,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdedmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21QtdeDmn), 8, 0, ",", "")),((edtavQtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21QtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV21QtdeDmn), "ZZZZZZZ9")),TempTags+((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"" : " "),"'"+""+"'"+",false,"+"'"+"EVQTDEDMN.CLICK."+sGXsfl_36_idx+"'",(String)"",(String)"",(String)edtavQtdedmn_Tooltiptext,(String)"",(String)edtavQtdedmn_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtdedmn_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtavQtdesemliq_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavQtdesemliq_Enabled!=0)&&(edtavQtdesemliq_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 45,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdesemliq_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76QtdeSemLiq), 8, 0, ",", "")),((edtavQtdesemliq_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76QtdeSemLiq), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV76QtdeSemLiq), "ZZZZZZZ9")),TempTags+((edtavQtdesemliq_Enabled!=0)&&(edtavQtdesemliq_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavQtdesemliq_Enabled!=0)&&(edtavQtdesemliq_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,45);\"" : " "),"'"+""+"'"+",false,"+"'"+"EVQTDESEMLIQ.CLICK."+sGXsfl_36_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtdesemliq_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(int)edtavQtdesemliq_Visible,(int)edtavQtdesemliq_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavLiquidar_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavLiquidar_Enabled!=0)&&(edtavLiquidar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 46,'',false,'',36)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV79Liquidar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV79Liquidar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV105Liquidar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV79Liquidar)));
         Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLiquidar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV79Liquidar)) ? AV105Liquidar_GXI : context.PathToRelativeUrl( AV79Liquidar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavLiquidar_Visible,(short)1,(String)"",(String)"Liquidar as demandas sem liquidar",(short)0,(short)1,(short)20,(String)"px",(short)0,(String)"px",(short)0,(short)10,(short)5,(String)edtavLiquidar_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVLIQUIDAR.CLICK."+sGXsfl_36_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV79Liquidar_IsBlob,(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavErros_Enabled!=0)&&(edtavErros_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 47,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavErros_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57Erros), 8, 0, ",", "")),((edtavErros_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57Erros), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV57Erros), "ZZZZZZZ9")),TempTags+((edtavErros_Enabled!=0)&&(edtavErros_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavErros_Enabled!=0)&&(edtavErros_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavErros_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavErros_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPendencias_Enabled!=0)&&(edtavPendencias_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 48,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPendencias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55Pendencias), 8, 0, ",", "")),((edtavPendencias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55Pendencias), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV55Pendencias), "ZZZZZZZ9")),TempTags+((edtavPendencias_Enabled!=0)&&(edtavPendencias_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPendencias_Enabled!=0)&&(edtavPendencias_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPendencias_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPendencias_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCanceladas_Enabled!=0)&&(edtavCanceladas_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 49,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCanceladas_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56Canceladas), 8, 0, ",", "")),((edtavCanceladas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56Canceladas), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV56Canceladas), "ZZZZZZZ9")),TempTags+((edtavCanceladas_Enabled!=0)&&(edtavCanceladas_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCanceladas_Enabled!=0)&&(edtavCanceladas_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCanceladas_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCanceladas_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtpf_Enabled!=0)&&(edtavCtpf_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 50,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtpf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV27CTPF, 18, 5, ",", "")),((edtavCtpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV27CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV27CTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavCtpf_Enabled!=0)&&(edtavCtpf_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtpf_Enabled!=0)&&(edtavCtpf_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Custo Total Ponto de Fun��o",(String)"",(String)edtavCtpf_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtpf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavRtpf_Enabled!=0)&&(edtavRtpf_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 51,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRtpf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV28RTPF, 18, 5, ",", "")),((edtavRtpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV28RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV28RTPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavRtpf_Enabled!=0)&&(edtavRtpf_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavRtpf_Enabled!=0)&&(edtavRtpf_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,51);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"Receita Total Ponto de Fun��o",(String)"",(String)edtavRtpf_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavRtpf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_36_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_36_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_36_idx+1));
         sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
         SubsflControlProps_362( ) ;
         /* End function sendrow_362 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         imgExport_Internalname = "EXPORT";
         imgCfps_Internalname = "CFPS";
         imgExportgraphicprd_Internalname = "EXPORTGRAPHICPRD";
         imgExportgraphicpfb_Internalname = "EXPORTGRAPHICPFB";
         imgExportgraphicdmn_Internalname = "EXPORTGRAPHICDMN";
         imgExportreport_Internalname = "EXPORTREPORT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         dynavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         dynavFiltro_contadorfmcod_Internalname = "vFILTRO_CONTADORFMCOD";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         cmbavFiltro_ano_Internalname = "vFILTRO_ANO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavFiltro_mes_Internalname = "vFILTRO_MES";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavFiltro_dia_Internalname = "vFILTRO_DIA";
         bttButton1_Internalname = "BUTTON1";
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         edtavContagemresultado_contadorfmnom_Internalname = "vCONTAGEMRESULTADO_CONTADORFMNOM";
         cmbavMes_Internalname = "vMES";
         cmbavAno_Internalname = "vANO";
         edtavPfbfs_Internalname = "vPFBFS";
         edtavPfbfm_Internalname = "vPFBFM";
         edtavPffinal_Internalname = "vPFFINAL";
         edtavQtdedmn_Internalname = "vQTDEDMN";
         edtavQtdesemliq_Internalname = "vQTDESEMLIQ";
         edtavLiquidar_Internalname = "vLIQUIDAR";
         edtavErros_Internalname = "vERROS";
         edtavPendencias_Internalname = "vPENDENCIAS";
         edtavCanceladas_Internalname = "vCANCELADAS";
         edtavCtpf_Internalname = "vCTPF";
         edtavRtpf_Internalname = "vRTPF";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavTpfbfs_Internalname = "vTPFBFS";
         lblTextblockcontagemresultado_pfbfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL";
         edtavTpfbfm_Internalname = "vTPFBFM";
         lblTextblockcontagemresultado_pfbfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL2";
         edtavTpffinal_Internalname = "vTPFFINAL";
         lblTextblockcontagemresultado_pflfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL";
         edtavTqtdedmn_Internalname = "vTQTDEDMN";
         lblTextblockcontagemresultado_pflfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL2";
         edtavTerros_Internalname = "vTERROS";
         lblTextblockcontagemresultado_pftotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFTOTAL2";
         edtavTpendencias_Internalname = "vTPENDENCIAS";
         lblTextblockcontagemresultado_pftotal3_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFTOTAL3";
         edtavTcanceladas_Internalname = "vTCANCELADAS";
         lblTextblockcontagemresultado_pftotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFTOTAL";
         edtavTctpf_Internalname = "vTCTPF";
         lblTextblockcontagemresultado_pftotalpagar_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFTOTALPAGAR";
         edtavTrtpf_Internalname = "vTRTPF";
         tblTable3_Internalname = "TABLE3";
         tblTable4_Internalname = "TABLE4";
         Innewwindow_Internalname = "INNEWWINDOW";
         edtavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         edtavContratada_pessoacod_Internalname = "vCONTRATADA_PESSOACOD";
         lblJava_Internalname = "JAVA";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavRtpf_Jsonclick = "";
         edtavRtpf_Visible = -1;
         edtavCtpf_Jsonclick = "";
         edtavCtpf_Visible = -1;
         edtavCanceladas_Jsonclick = "";
         edtavCanceladas_Visible = -1;
         edtavPendencias_Jsonclick = "";
         edtavPendencias_Visible = -1;
         edtavErros_Jsonclick = "";
         edtavErros_Visible = -1;
         edtavLiquidar_Jsonclick = "";
         edtavLiquidar_Enabled = 1;
         edtavQtdesemliq_Jsonclick = "";
         edtavQtdedmn_Jsonclick = "";
         edtavQtdedmn_Visible = -1;
         edtavPffinal_Jsonclick = "";
         edtavPffinal_Visible = -1;
         edtavPfbfm_Jsonclick = "";
         edtavPfbfm_Visible = -1;
         edtavPfbfs_Jsonclick = "";
         edtavPfbfs_Visible = -1;
         cmbavAno_Jsonclick = "";
         cmbavAno.Visible = -1;
         cmbavMes_Jsonclick = "";
         cmbavMes.Visible = -1;
         edtavContagemresultado_contadorfmnom_Jsonclick = "";
         edtavContagemresultado_contadorfmnom_Visible = -1;
         edtavContagemresultado_contadorfmcod_Jsonclick = "";
         edtavContagemresultado_contadorfmcod_Visible = 0;
         imgExportreport_Visible = 1;
         imgExportgraphicdmn_Visible = 1;
         imgExportgraphicpfb_Visible = 1;
         imgExportgraphicprd_Visible = 1;
         edtavTrtpf_Jsonclick = "";
         edtavTrtpf_Enabled = 1;
         edtavTctpf_Jsonclick = "";
         edtavTctpf_Enabled = 1;
         edtavTcanceladas_Jsonclick = "";
         edtavTcanceladas_Enabled = 1;
         edtavTpendencias_Jsonclick = "";
         edtavTpendencias_Enabled = 1;
         edtavTerros_Jsonclick = "";
         edtavTerros_Enabled = 1;
         edtavTqtdedmn_Jsonclick = "";
         edtavTqtdedmn_Enabled = 1;
         edtavTpffinal_Jsonclick = "";
         edtavTpffinal_Enabled = 1;
         edtavTpfbfm_Jsonclick = "";
         edtavTpfbfm_Enabled = 1;
         edtavTpfbfs_Jsonclick = "";
         edtavTpfbfs_Enabled = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavRtpf_Enabled = 1;
         edtavCtpf_Enabled = 1;
         edtavCanceladas_Enabled = 1;
         edtavPendencias_Enabled = 1;
         edtavErros_Enabled = 1;
         edtavQtdesemliq_Enabled = 1;
         edtavQtdedmn_Enabled = 1;
         edtavPffinal_Enabled = 1;
         edtavPfbfm_Enabled = 1;
         edtavPfbfs_Enabled = 1;
         cmbavAno.Enabled = 1;
         cmbavMes.Enabled = 1;
         edtavContagemresultado_contadorfmnom_Enabled = 1;
         edtavContagemresultado_contadorfmcod_Enabled = 1;
         subGrid1_Class = "WorkWith";
         edtavFiltro_dia_Jsonclick = "";
         cmbavFiltro_mes_Jsonclick = "";
         cmbavFiltro_ano_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         dynavFiltro_contadorfmcod_Jsonclick = "";
         dynavAreatrabalho_codigo_Jsonclick = "";
         dynavAreatrabalho_codigo.Enabled = 1;
         edtavLiquidar_Title = "Liq";
         edtavLiquidar_Visible = -1;
         edtavQtdesemliq_Visible = -1;
         subGrid1_Sortable = 0;
         subGrid1_Titleforecolor = (int)(0x000000);
         subGrid1_Backcolorstyle = 3;
         lblJava_Jsonclick = "";
         lblJava_Caption = "Java";
         edtavContratada_pessoacod_Jsonclick = "";
         edtavContratada_pessoacod_Visible = 1;
         edtavContratada_codigo_Jsonclick = "";
         edtavContratada_codigo_Visible = 1;
         Innewwindow_Target = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta de Contadores";
         edtavQtdedmn_Tooltiptext = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Areatrabalho_codigo( GXCombobox dynGX_Parm1 ,
                                              int GX_Parm2 ,
                                              int GX_Parm3 ,
                                              GXCombobox dynGX_Parm4 ,
                                              GXCombobox dynGX_Parm5 )
      {
         dynavAreatrabalho_codigo = dynGX_Parm1;
         AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.CurrentValue, "."));
         AV100Contratada_PessoaCod = GX_Parm2;
         AV83Contratada_Codigo = GX_Parm3;
         dynavFiltro_contadorfmcod = dynGX_Parm4;
         AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm5;
         AV75Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         GXVvFILTRO_CONTADORFMCOD_htmlBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         GXVvSERVICO_CODIGO_htmlBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavFiltro_contadorfmcod.ItemCount > 0 )
         {
            AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0))), "."));
         }
         dynavFiltro_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynavFiltro_contadorfmcod);
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV75Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contratada_codigo( GXCombobox dynGX_Parm1 ,
                                            int GX_Parm2 ,
                                            int GX_Parm3 ,
                                            GXCombobox dynGX_Parm4 ,
                                            GXCombobox dynGX_Parm5 )
      {
         dynavAreatrabalho_codigo = dynGX_Parm1;
         AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.CurrentValue, "."));
         AV100Contratada_PessoaCod = GX_Parm2;
         AV83Contratada_Codigo = GX_Parm3;
         dynavFiltro_contadorfmcod = dynGX_Parm4;
         AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm5;
         AV75Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         GXVvFILTRO_CONTADORFMCOD_htmlBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         GXVvSERVICO_CODIGO_htmlBU2( AV18AreaTrabalho_Codigo, AV83Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavFiltro_contadorfmcod.ItemCount > 0 )
         {
            AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0))), "."));
         }
         dynavFiltro_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynavFiltro_contadorfmcod);
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV75Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV75Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contratada_pessoacod( GXCombobox dynGX_Parm1 ,
                                               int GX_Parm2 ,
                                               int GX_Parm3 ,
                                               GXCombobox dynGX_Parm4 )
      {
         dynavAreatrabalho_codigo = dynGX_Parm1;
         AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.CurrentValue, "."));
         AV100Contratada_PessoaCod = GX_Parm2;
         AV83Contratada_Codigo = GX_Parm3;
         dynavFiltro_contadorfmcod = dynGX_Parm4;
         AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.CurrentValue, "."));
         GXVvFILTRO_CONTADORFMCOD_htmlBU2( AV18AreaTrabalho_Codigo, AV100Contratada_PessoaCod, AV83Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavFiltro_contadorfmcod.ItemCount > 0 )
         {
            AV31Filtro_ContadorFMCod = (int)(NumberUtil.Val( dynavFiltro_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0))), "."));
         }
         dynavFiltro_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31Filtro_ContadorFMCod), 6, 0));
         isValidOutput.Add(dynavFiltro_contadorfmcod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'edtavQtdedmn_Tooltiptext',ctrl:'vQTDEDMN',prop:'Tooltiptext'},{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''},{av:'A470ContagemResultado_ContadorFMCod',fld:'CONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A999ContagemResultado_CrFMEhContratada',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATADA',pic:'',nv:false},{av:'A1000ContagemResultado_CrFMEhContratante',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATANTE',pic:'',nv:false},{av:'AV73ContagemResultado_Data',fld:'vCONTAGEMRESULTADO_DATA',pic:'',nv:''},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A1616ContagemResultado_CntVlrUndCnt',fld:'CONTAGEMRESULTADO_CNTVLRUNDCNT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1615ContagemResultado_CntIndDvr',fld:'CONTAGEMRESULTADO_CNTINDDVR',pic:'ZZ9.99',nv:0.0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A833ContagemResultado_CstUntPrd',fld:'CONTAGEMRESULTADO_CSTUNTPRD',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Codigo',fld:'vCODIGO',pic:'',nv:null},{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null},{av:'AV45tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV44tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV61tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV62tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("GRID1.REFRESH","{handler:'E20BU2',iparms:[{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null}],oparms:[{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV83Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV100Contratada_PessoaCod',fld:'vCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV16Ano',fld:'vANO',pic:'ZZZ9',nv:0},{av:'AV45tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV44tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV62tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0},{av:'AV65tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null},{av:'edtavQtdesemliq_Visible',ctrl:'vQTDESEMLIQ',prop:'Visible'},{av:'edtavLiquidar_Visible',ctrl:'vLIQUIDAR',prop:'Visible'},{av:'edtavLiquidar_Title',ctrl:'vLIQUIDAR',prop:'Title'},{av:'dynavAreatrabalho_codigo'},{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID1.LOAD","{handler:'E22BU2',iparms:[{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''},{av:'A470ContagemResultado_ContadorFMCod',fld:'CONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A999ContagemResultado_CrFMEhContratada',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATADA',pic:'',nv:false},{av:'A1000ContagemResultado_CrFMEhContratante',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATANTE',pic:'',nv:false},{av:'AV73ContagemResultado_Data',fld:'vCONTAGEMRESULTADO_DATA',pic:'',nv:''},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A1616ContagemResultado_CntVlrUndCnt',fld:'CONTAGEMRESULTADO_CNTVLRUNDCNT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1615ContagemResultado_CntIndDvr',fld:'CONTAGEMRESULTADO_CNTINDDVR',pic:'ZZ9.99',nv:0.0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A833ContagemResultado_CstUntPrd',fld:'CONTAGEMRESULTADO_CSTUNTPRD',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Codigo',fld:'vCODIGO',pic:'',nv:null},{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null},{av:'AV45tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV44tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV61tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV62tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0}],oparms:[{av:'AV79Liquidar',fld:'vLIQUIDAR',pic:'',nv:''},{av:'AV67ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_ContadorFMNom',fld:'vCONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''},{av:'AV97Codigo',fld:'vCODIGO',pic:'',nv:null},{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null},{av:'AV17Mes',fld:'vMES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV21QtdeDmn',fld:'vQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV76QtdeSemLiq',fld:'vQTDESEMLIQ',pic:'ZZZZZZZ9',nv:0},{av:'AV24PFBFS',fld:'vPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22PFBFM',fld:'vPFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71PFFinal',fld:'vPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV27CTPF',fld:'vCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28RTPF',fld:'vRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV57Erros',fld:'vERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV55Pendencias',fld:'vPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV56Canceladas',fld:'vCANCELADAS',pic:'ZZZZZZZ9',nv:0},{av:'edtavLiquidar_Visible',ctrl:'vLIQUIDAR',prop:'Visible'},{av:'AV45tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV44tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV61tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV62tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0}]}");
         setEventMetadata("VAREATRABALHO_CODIGO.CLICK","{handler:'E11BU2',iparms:[{av:'AV83Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV100Contratada_PessoaCod',fld:'vCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null}]}");
         setEventMetadata("VFILTRO_MES.CLICK","{handler:'E17BU2',iparms:[],oparms:[]}");
         setEventMetadata("VFILTRO_DIA.ISVALID","{handler:'E18BU2',iparms:[{av:'AV72Filtro_Dia',fld:'vFILTRO_DIA',pic:'Z9',nv:0},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV72Filtro_Dia',fld:'vFILTRO_DIA',pic:'Z9',nv:0},{av:'AV73ContagemResultado_Data',fld:'vCONTAGEMRESULTADO_DATA',pic:'',nv:''}]}");
         setEventMetadata("VFILTRO_CONTADORFMCOD.CLICK","{handler:'E19BU2',iparms:[],oparms:[]}");
         setEventMetadata("'DOGRAFICOSDMN'","{handler:'E12BU2',iparms:[{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73ContagemResultado_Data',fld:'vCONTAGEMRESULTADO_DATA',pic:'',nv:''},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOGRAFICOSPFB'","{handler:'E13BU2',iparms:[{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73ContagemResultado_Data',fld:'vCONTAGEMRESULTADO_DATA',pic:'',nv:''},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOGRAFICOSPRD'","{handler:'E14BU2',iparms:[{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("VQTDEDMN.CLICK","{handler:'E23BU2',iparms:[{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV67ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Mes',fld:'vMES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV16Ano',fld:'vANO',pic:'ZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_ContadorFMNom',fld:'vCONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''}],oparms:[{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("VQTDESEMLIQ.CLICK","{handler:'E24BU2',iparms:[{av:'AV76QtdeSemLiq',fld:'vQTDESEMLIQ',pic:'ZZZZZZZ9',nv:0},{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV67ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ContagemResultado_ContadorFMNom',fld:'vCONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''}],oparms:[{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("VLIQUIDAR.CLICK","{handler:'E25BU2',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'edtavQtdedmn_Tooltiptext',ctrl:'vQTDEDMN',prop:'Tooltiptext'},{av:'AV82Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''},{av:'A470ContagemResultado_ContadorFMCod',fld:'CONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A999ContagemResultado_CrFMEhContratada',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATADA',pic:'',nv:false},{av:'A1000ContagemResultado_CrFMEhContratante',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATANTE',pic:'',nv:false},{av:'AV73ContagemResultado_Data',fld:'vCONTAGEMRESULTADO_DATA',pic:'',nv:''},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A1616ContagemResultado_CntVlrUndCnt',fld:'CONTAGEMRESULTADO_CNTVLRUNDCNT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1615ContagemResultado_CntIndDvr',fld:'CONTAGEMRESULTADO_CNTINDDVR',pic:'ZZ9.99',nv:0.0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A833ContagemResultado_CstUntPrd',fld:'CONTAGEMRESULTADO_CSTUNTPRD',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Codigo',fld:'vCODIGO',pic:'',nv:null},{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null},{av:'AV45tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV44tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43tPFBFM',fld:'vTPFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV70tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV42tCTPF',fld:'vTCTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46tRTPF',fld:'vTRTPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65tErros',fld:'vTERROS',pic:'ZZZZZZZ9',nv:0},{av:'AV61tPendencias',fld:'vTPENDENCIAS',pic:'ZZZZZZZ9',nv:0},{av:'AV62tCanceladas',fld:'vTCANCELADAS',pic:'ZZZZZZZ9',nv:0},{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV67ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV68SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("'DOEXPORT'","{handler:'E15BU2',iparms:[{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("'DOEXPORTREPORT'","{handler:'E26BU1',iparms:[],oparms:[]}");
         setEventMetadata("'DOCFPS'","{handler:'E16BU2',iparms:[{av:'AV31Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV96SDT_Codigos',fld:'vSDT_CODIGOS',pic:'',nv:null},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV18AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV75Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV30Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV29Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV99Target',fld:'vTARGET',pic:'',nv:null},{av:'AV98InNewWindowTargets',fld:'vINNEWWINDOWTARGETS',pic:'',nv:null}],oparms:[{av:'AV99Target',fld:'vTARGET',pic:'',nv:null},{av:'AV98InNewWindowTargets',fld:'vINNEWWINDOWTARGETS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(3);
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV82Contratadas = new GxSimpleCollection();
         AV68SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         A474ContagemResultado_ContadorFMNom = "";
         AV19WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV73ContagemResultado_Data = DateTime.MinValue;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         AV97Codigo = new SdtSDT_Codigos(context);
         AV96SDT_Codigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV98InNewWindowTargets = new GxObjectCollection( context, "InNewWindowTargets.InNewWindowTargetsItem", "GxEv3Up14_MeetrikaVs3", "SdtInNewWindowTargets_InNewWindowTargetsItem", "GeneXus.Programs");
         AV99Target = new SdtInNewWindowTargets_InNewWindowTargetsItem(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV20ContagemResultado_ContadorFMNom = "";
         AV79Liquidar = "";
         AV105Liquidar_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00BU2_A5AreaTrabalho_Codigo = new int[1] ;
         H00BU2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00BU3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00BU3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00BU3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00BU3_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00BU3_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00BU3_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H00BU3_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H00BU3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         Grid1Container = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         H00BU4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00BU4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00BU4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00BU4_A1603ContagemResultado_CntCod = new int[1] ;
         H00BU4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00BU4_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00BU4_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00BU4_A1583ContagemResultado_TipoRegistro = new short[1] ;
         H00BU4_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00BU4_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00BU4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00BU4_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00BU4_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00BU4_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00BU4_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00BU4_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         H00BU4_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         H00BU4_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00BU4_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00BU4_A1043ContagemResultado_LiqLogCod = new int[1] ;
         H00BU4_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         H00BU4_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00BU4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00BU4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00BU4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00BU4_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00BU4_A601ContagemResultado_Servico = new int[1] ;
         H00BU4_n601ContagemResultado_Servico = new bool[] {false} ;
         H00BU4_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00BU4_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00BU4_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         H00BU4_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         H00BU4_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00BU4_A1616ContagemResultado_CntVlrUndCnt = new decimal[1] ;
         H00BU4_n1616ContagemResultado_CntVlrUndCnt = new bool[] {false} ;
         H00BU4_A1615ContagemResultado_CntIndDvr = new decimal[1] ;
         H00BU4_n1615ContagemResultado_CntIndDvr = new bool[] {false} ;
         H00BU4_A456ContagemResultado_Codigo = new int[1] ;
         AV59Contrato_IndiceDivergencia = (decimal)(10);
         AV35mQtdeDmn = new int [12] ;
         AV78mQtdeSemLiq = new int [12] ;
         AV37mPFBFM = new decimal [12] ;
         AV36mPFBFS = new decimal [12] ;
         AV69mPFFinal = new decimal [12] ;
         AV38mCTPF = new decimal [12] ;
         AV39mRTPF = new decimal [12] ;
         AV64mErros = new int [12] ;
         AV58mPendencias = new int [12] ;
         AV60mCanceladas = new int [12] ;
         H00BU6_A40000ContagemResultadoErro_Tipo = new String[] {""} ;
         H00BU6_n40000ContagemResultadoErro_Tipo = new bool[] {false} ;
         A40000ContagemResultadoErro_Tipo = "";
         Grid1Row = new GXWebRow();
         H00BU7_A40Contratada_PessoaCod = new int[1] ;
         H00BU7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00BU7_A39Contratada_Codigo = new int[1] ;
         H00BU8_A40Contratada_PessoaCod = new int[1] ;
         H00BU8_A39Contratada_Codigo = new int[1] ;
         Gx_msg = "";
         AV66WebSession = context.GetSession();
         AV63Window = new GXWindow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         bttButton1_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pftotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pftotal3_Jsonclick = "";
         lblTextblockcontagemresultado_pftotal_Jsonclick = "";
         lblTextblockcontagemresultado_pftotalpagar_Jsonclick = "";
         lblContagemresultadotitle_Jsonclick = "";
         imgExport_Jsonclick = "";
         imgCfps_Jsonclick = "";
         imgExportgraphicprd_Jsonclick = "";
         imgExportgraphicpfb_Jsonclick = "";
         imgExportgraphicdmn_Jsonclick = "";
         imgExportreport_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultacontadores__default(),
            new Object[][] {
                new Object[] {
               H00BU2_A5AreaTrabalho_Codigo, H00BU2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00BU3_A70ContratadaUsuario_UsuarioPessoaCod, H00BU3_n70ContratadaUsuario_UsuarioPessoaCod, H00BU3_A69ContratadaUsuario_UsuarioCod, H00BU3_A71ContratadaUsuario_UsuarioPessoaNom, H00BU3_n71ContratadaUsuario_UsuarioPessoaNom, H00BU3_A67ContratadaUsuario_ContratadaPessoaCod, H00BU3_n67ContratadaUsuario_ContratadaPessoaCod, H00BU3_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00BU4_A511ContagemResultado_HoraCnt, H00BU4_A1553ContagemResultado_CntSrvCod, H00BU4_n1553ContagemResultado_CntSrvCod, H00BU4_A1603ContagemResultado_CntCod, H00BU4_n1603ContagemResultado_CntCod, H00BU4_A479ContagemResultado_CrFMPessoaCod, H00BU4_n479ContagemResultado_CrFMPessoaCod, H00BU4_A1583ContagemResultado_TipoRegistro, H00BU4_A474ContagemResultado_ContadorFMNom, H00BU4_n474ContagemResultado_ContadorFMNom,
               H00BU4_A473ContagemResultado_DataCnt, H00BU4_A458ContagemResultado_PFBFS, H00BU4_n458ContagemResultado_PFBFS, H00BU4_A460ContagemResultado_PFBFM, H00BU4_n460ContagemResultado_PFBFM, H00BU4_A833ContagemResultado_CstUntPrd, H00BU4_n833ContagemResultado_CstUntPrd, H00BU4_A512ContagemResultado_ValorPF, H00BU4_n512ContagemResultado_ValorPF, H00BU4_A1043ContagemResultado_LiqLogCod,
               H00BU4_n1043ContagemResultado_LiqLogCod, H00BU4_A490ContagemResultado_ContratadaCod, H00BU4_n490ContagemResultado_ContratadaCod, H00BU4_A484ContagemResultado_StatusDmn, H00BU4_n484ContagemResultado_StatusDmn, H00BU4_A517ContagemResultado_Ultima, H00BU4_A601ContagemResultado_Servico, H00BU4_n601ContagemResultado_Servico, H00BU4_A1000ContagemResultado_CrFMEhContratante, H00BU4_n1000ContagemResultado_CrFMEhContratante,
               H00BU4_A999ContagemResultado_CrFMEhContratada, H00BU4_n999ContagemResultado_CrFMEhContratada, H00BU4_A470ContagemResultado_ContadorFMCod, H00BU4_A1616ContagemResultado_CntVlrUndCnt, H00BU4_n1616ContagemResultado_CntVlrUndCnt, H00BU4_A1615ContagemResultado_CntIndDvr, H00BU4_n1615ContagemResultado_CntIndDvr, H00BU4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00BU6_A40000ContagemResultadoErro_Tipo, H00BU6_n40000ContagemResultadoErro_Tipo
               }
               , new Object[] {
               H00BU7_A40Contratada_PessoaCod, H00BU7_A52Contratada_AreaTrabalhoCod, H00BU7_A39Contratada_Codigo
               }
               , new Object[] {
               H00BU8_A40Contratada_PessoaCod, H00BU8_A39Contratada_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         edtavContagemresultado_contadorfmnom_Enabled = 0;
         cmbavMes.Enabled = 0;
         cmbavAno.Enabled = 0;
         edtavPfbfs_Enabled = 0;
         edtavPfbfm_Enabled = 0;
         edtavPffinal_Enabled = 0;
         edtavQtdedmn_Enabled = 0;
         edtavQtdesemliq_Enabled = 0;
         edtavErros_Enabled = 0;
         edtavPendencias_Enabled = 0;
         edtavCanceladas_Enabled = 0;
         edtavCtpf_Enabled = 0;
         edtavRtpf_Enabled = 0;
         edtavTpfbfs_Enabled = 0;
         edtavTpfbfm_Enabled = 0;
         edtavTpffinal_Enabled = 0;
         edtavTqtdedmn_Enabled = 0;
         edtavTerros_Enabled = 0;
         edtavTpendencias_Enabled = 0;
         edtavTcanceladas_Enabled = 0;
         edtavTctpf_Enabled = 0;
         edtavTrtpf_Enabled = 0;
      }

      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_36 ;
      private short nGXsfl_36_idx=1 ;
      private short AV30Filtro_Ano ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV16Ano ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_36_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Sortable ;
      private short AV72Filtro_Dia ;
      private short AV34i ;
      private short AV80Ini ;
      private short AV81Fim ;
      private short GXt_int3 ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private short wbTemp ;
      private short GRID1_nEOF ;
      private int AV18AreaTrabalho_Codigo ;
      private int AV100Contratada_PessoaCod ;
      private int AV83Contratada_Codigo ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int AV31Filtro_ContadorFMCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV75Servico_Codigo ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV45tQtdeDmn ;
      private int AV65tErros ;
      private int AV61tPendencias ;
      private int AV62tCanceladas ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int edtavContratada_codigo_Visible ;
      private int edtavContratada_pessoacod_Visible ;
      private int AV67ContagemResultado_ContadorFMCod ;
      private int AV21QtdeDmn ;
      private int AV76QtdeSemLiq ;
      private int AV57Erros ;
      private int AV55Pendencias ;
      private int AV56Canceladas ;
      private int gxdynajaxindex ;
      private int subGrid1_Islastpage ;
      private int edtavContagemresultado_contadorfmcod_Enabled ;
      private int edtavContagemresultado_contadorfmnom_Enabled ;
      private int edtavPfbfs_Enabled ;
      private int edtavPfbfm_Enabled ;
      private int edtavPffinal_Enabled ;
      private int edtavQtdedmn_Enabled ;
      private int edtavQtdesemliq_Enabled ;
      private int edtavErros_Enabled ;
      private int edtavPendencias_Enabled ;
      private int edtavCanceladas_Enabled ;
      private int edtavCtpf_Enabled ;
      private int edtavRtpf_Enabled ;
      private int edtavTpfbfs_Enabled ;
      private int edtavTpfbfm_Enabled ;
      private int edtavTpffinal_Enabled ;
      private int edtavTqtdedmn_Enabled ;
      private int edtavTerros_Enabled ;
      private int edtavTpendencias_Enabled ;
      private int edtavTcanceladas_Enabled ;
      private int edtavTctpf_Enabled ;
      private int edtavTrtpf_Enabled ;
      private int subGrid1_Titleforecolor ;
      private int imgExportreport_Visible ;
      private int imgExportgraphicdmn_Visible ;
      private int imgExportgraphicpfb_Visible ;
      private int imgExportgraphicprd_Visible ;
      private int edtavQtdesemliq_Visible ;
      private int edtavLiquidar_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int GX_I ;
      private int [] AV35mQtdeDmn ;
      private int [] AV78mQtdeSemLiq ;
      private int [] AV64mErros ;
      private int [] AV58mPendencias ;
      private int [] AV60mCanceladas ;
      private int GXt_int2 ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavContagemresultado_contadorfmcod_Visible ;
      private int edtavContagemresultado_contadorfmnom_Visible ;
      private int edtavPfbfs_Visible ;
      private int edtavPfbfm_Visible ;
      private int edtavPffinal_Visible ;
      private int edtavQtdedmn_Visible ;
      private int edtavLiquidar_Enabled ;
      private int edtavErros_Visible ;
      private int edtavPendencias_Visible ;
      private int edtavCanceladas_Visible ;
      private int edtavCtpf_Visible ;
      private int edtavRtpf_Visible ;
      private long AV29Filtro_Mes ;
      private long AV17Mes ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private decimal A1616ContagemResultado_CntVlrUndCnt ;
      private decimal A1615ContagemResultado_CntIndDvr ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal AV44tPFBFS ;
      private decimal AV43tPFBFM ;
      private decimal AV70tPFFinal ;
      private decimal AV42tCTPF ;
      private decimal AV46tRTPF ;
      private decimal AV24PFBFS ;
      private decimal AV22PFBFM ;
      private decimal AV71PFFinal ;
      private decimal AV27CTPF ;
      private decimal AV28RTPF ;
      private decimal AV26CUPF ;
      private decimal AV59Contrato_IndiceDivergencia ;
      private decimal [] AV37mPFBFM ;
      private decimal [] AV36mPFBFS ;
      private decimal [] AV69mPFFinal ;
      private decimal [] AV38mCTPF ;
      private decimal [] AV39mRTPF ;
      private decimal GXt_decimal1 ;
      private String edtavQtdedmn_Tooltiptext ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_36_idx="0001" ;
      private String edtavQtdedmn_Internalname ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavContratada_codigo_Internalname ;
      private String edtavContratada_codigo_Jsonclick ;
      private String edtavContratada_pessoacod_Internalname ;
      private String edtavContratada_pessoacod_Jsonclick ;
      private String lblJava_Internalname ;
      private String lblJava_Caption ;
      private String lblJava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavContagemresultado_contadorfmcod_Internalname ;
      private String AV20ContagemResultado_ContadorFMNom ;
      private String edtavContagemresultado_contadorfmnom_Internalname ;
      private String cmbavMes_Internalname ;
      private String cmbavAno_Internalname ;
      private String edtavPfbfs_Internalname ;
      private String edtavPfbfm_Internalname ;
      private String edtavPffinal_Internalname ;
      private String edtavQtdesemliq_Internalname ;
      private String edtavLiquidar_Internalname ;
      private String edtavErros_Internalname ;
      private String edtavPendencias_Internalname ;
      private String edtavCanceladas_Internalname ;
      private String edtavCtpf_Internalname ;
      private String edtavRtpf_Internalname ;
      private String GXCCtl ;
      private String dynavAreatrabalho_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavTpfbfs_Internalname ;
      private String edtavTpfbfm_Internalname ;
      private String edtavTpffinal_Internalname ;
      private String edtavTqtdedmn_Internalname ;
      private String edtavTerros_Internalname ;
      private String edtavTpendencias_Internalname ;
      private String edtavTcanceladas_Internalname ;
      private String edtavTctpf_Internalname ;
      private String edtavTrtpf_Internalname ;
      private String dynavFiltro_contadorfmcod_Internalname ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavFiltro_ano_Internalname ;
      private String cmbavFiltro_mes_Internalname ;
      private String edtavFiltro_dia_Internalname ;
      private String imgExportreport_Internalname ;
      private String imgExportgraphicdmn_Internalname ;
      private String imgExportgraphicpfb_Internalname ;
      private String imgExportgraphicprd_Internalname ;
      private String edtavLiquidar_Title ;
      private String A40000ContagemResultadoErro_Tipo ;
      private String Gx_msg ;
      private String Innewwindow_Internalname ;
      private String sStyleString ;
      private String tblTable4_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String dynavAreatrabalho_codigo_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String dynavFiltro_contadorfmcod_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String cmbavFiltro_ano_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavFiltro_mes_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavFiltro_dia_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String tblTable3_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String edtavTpfbfs_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal_Jsonclick ;
      private String edtavTpfbfm_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick ;
      private String edtavTpffinal_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal_Jsonclick ;
      private String edtavTqtdedmn_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal2_Jsonclick ;
      private String edtavTerros_Jsonclick ;
      private String lblTextblockcontagemresultado_pftotal2_Internalname ;
      private String lblTextblockcontagemresultado_pftotal2_Jsonclick ;
      private String edtavTpendencias_Jsonclick ;
      private String lblTextblockcontagemresultado_pftotal3_Internalname ;
      private String lblTextblockcontagemresultado_pftotal3_Jsonclick ;
      private String edtavTcanceladas_Jsonclick ;
      private String lblTextblockcontagemresultado_pftotal_Internalname ;
      private String lblTextblockcontagemresultado_pftotal_Jsonclick ;
      private String edtavTctpf_Jsonclick ;
      private String lblTextblockcontagemresultado_pftotalpagar_Internalname ;
      private String lblTextblockcontagemresultado_pftotalpagar_Jsonclick ;
      private String edtavTrtpf_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String imgExport_Internalname ;
      private String imgExport_Jsonclick ;
      private String imgCfps_Internalname ;
      private String imgCfps_Jsonclick ;
      private String imgExportgraphicprd_Jsonclick ;
      private String imgExportgraphicpfb_Jsonclick ;
      private String imgExportgraphicdmn_Jsonclick ;
      private String imgExportreport_Jsonclick ;
      private String sGXsfl_36_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultado_contadorfmcod_Jsonclick ;
      private String edtavContagemresultado_contadorfmnom_Jsonclick ;
      private String cmbavMes_Jsonclick ;
      private String cmbavAno_Jsonclick ;
      private String edtavPfbfs_Jsonclick ;
      private String edtavPfbfm_Jsonclick ;
      private String edtavPffinal_Jsonclick ;
      private String edtavQtdedmn_Jsonclick ;
      private String edtavQtdesemliq_Jsonclick ;
      private String edtavLiquidar_Jsonclick ;
      private String edtavErros_Jsonclick ;
      private String edtavPendencias_Jsonclick ;
      private String edtavCanceladas_Jsonclick ;
      private String edtavCtpf_Jsonclick ;
      private String edtavRtpf_Jsonclick ;
      private DateTime AV73ContagemResultado_Data ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool A999ContagemResultado_CrFMEhContratada ;
      private bool n999ContagemResultado_CrFMEhContratada ;
      private bool A1000ContagemResultado_CrFMEhContratante ;
      private bool n1000ContagemResultado_CrFMEhContratante ;
      private bool n601ContagemResultado_Servico ;
      private bool A517ContagemResultado_Ultima ;
      private bool n1616ContagemResultado_CntVlrUndCnt ;
      private bool n1615ContagemResultado_CntIndDvr ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV19WWPContext_gxTpr_Userehcontratante ;
      private bool BRKBU3 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n40000ContagemResultadoErro_Tipo ;
      private bool AV79Liquidar_IsBlob ;
      private String AV105Liquidar_GXI ;
      private String AV79Liquidar ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavAreatrabalho_codigo ;
      private GXCombobox dynavFiltro_contadorfmcod ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavFiltro_ano ;
      private GXCombobox cmbavFiltro_mes ;
      private GXCombobox cmbavMes ;
      private GXCombobox cmbavAno ;
      private IDataStoreProvider pr_default ;
      private int[] H00BU2_A5AreaTrabalho_Codigo ;
      private String[] H00BU2_A6AreaTrabalho_Descricao ;
      private int[] H00BU3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00BU3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00BU3_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00BU3_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00BU3_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00BU3_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H00BU3_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] H00BU3_A66ContratadaUsuario_ContratadaCod ;
      private String[] H00BU4_A511ContagemResultado_HoraCnt ;
      private int[] H00BU4_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00BU4_n1553ContagemResultado_CntSrvCod ;
      private int[] H00BU4_A1603ContagemResultado_CntCod ;
      private bool[] H00BU4_n1603ContagemResultado_CntCod ;
      private int[] H00BU4_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00BU4_n479ContagemResultado_CrFMPessoaCod ;
      private short[] H00BU4_A1583ContagemResultado_TipoRegistro ;
      private String[] H00BU4_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00BU4_n474ContagemResultado_ContadorFMNom ;
      private DateTime[] H00BU4_A473ContagemResultado_DataCnt ;
      private decimal[] H00BU4_A458ContagemResultado_PFBFS ;
      private bool[] H00BU4_n458ContagemResultado_PFBFS ;
      private decimal[] H00BU4_A460ContagemResultado_PFBFM ;
      private bool[] H00BU4_n460ContagemResultado_PFBFM ;
      private decimal[] H00BU4_A833ContagemResultado_CstUntPrd ;
      private bool[] H00BU4_n833ContagemResultado_CstUntPrd ;
      private decimal[] H00BU4_A512ContagemResultado_ValorPF ;
      private bool[] H00BU4_n512ContagemResultado_ValorPF ;
      private int[] H00BU4_A1043ContagemResultado_LiqLogCod ;
      private bool[] H00BU4_n1043ContagemResultado_LiqLogCod ;
      private int[] H00BU4_A490ContagemResultado_ContratadaCod ;
      private bool[] H00BU4_n490ContagemResultado_ContratadaCod ;
      private String[] H00BU4_A484ContagemResultado_StatusDmn ;
      private bool[] H00BU4_n484ContagemResultado_StatusDmn ;
      private bool[] H00BU4_A517ContagemResultado_Ultima ;
      private int[] H00BU4_A601ContagemResultado_Servico ;
      private bool[] H00BU4_n601ContagemResultado_Servico ;
      private bool[] H00BU4_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00BU4_n1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00BU4_A999ContagemResultado_CrFMEhContratada ;
      private bool[] H00BU4_n999ContagemResultado_CrFMEhContratada ;
      private int[] H00BU4_A470ContagemResultado_ContadorFMCod ;
      private decimal[] H00BU4_A1616ContagemResultado_CntVlrUndCnt ;
      private bool[] H00BU4_n1616ContagemResultado_CntVlrUndCnt ;
      private decimal[] H00BU4_A1615ContagemResultado_CntIndDvr ;
      private bool[] H00BU4_n1615ContagemResultado_CntIndDvr ;
      private int[] H00BU4_A456ContagemResultado_Codigo ;
      private String[] H00BU6_A40000ContagemResultadoErro_Tipo ;
      private bool[] H00BU6_n40000ContagemResultadoErro_Tipo ;
      private int[] H00BU7_A40Contratada_PessoaCod ;
      private int[] H00BU7_A52Contratada_AreaTrabalhoCod ;
      private int[] H00BU7_A39Contratada_Codigo ;
      private int[] H00BU8_A40Contratada_PessoaCod ;
      private int[] H00BU8_A39Contratada_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV66WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV82Contratadas ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV96SDT_Codigos ;
      [ObjectCollection(ItemType=typeof( SdtInNewWindowTargets_InNewWindowTargetsItem ))]
      private IGxCollection AV98InNewWindowTargets ;
      private GXWebForm Form ;
      private GXWindow AV63Window ;
      private SdtSDT_Codigos AV97Codigo ;
      private SdtSDT_FiltroConsContadorFM AV68SDT_FiltroConsContadorFM ;
      private SdtInNewWindowTargets_InNewWindowTargetsItem AV99Target ;
      private wwpbaseobjects.SdtWWPContext AV19WWPContext ;
   }

   public class wp_consultacontadores__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BU4( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV82Contratadas ,
                                             int AV31Filtro_ContadorFMCod ,
                                             bool AV19WWPContext_gxTpr_Userehcontratante ,
                                             DateTime AV73ContagemResultado_Data ,
                                             long AV29Filtro_Mes ,
                                             int AV75Servico_Codigo ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             bool A999ContagemResultado_CrFMEhContratada ,
                                             bool A1000ContagemResultado_CrFMEhContratante ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             short AV30Filtro_Ano ,
                                             int A601ContagemResultado_Servico ,
                                             bool A517ContagemResultado_Ultima ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_HoraCnt], T4.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T4.[ContagemResultado_TipoRegistro], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_CstUntPrd], T4.[ContagemResultado_ValorPF], T4.[ContagemResultado_LiqLogCod], T4.[ContagemResultado_ContratadaCod], T4.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Ultima], T5.[Servico_Codigo] AS ContagemResultado_Servico, T2.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T2.[Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T6.[Contrato_ValorUnidadeContratacao] AS ContagemResultado_CntVlrUndCnt, T6.[Contrato_IndiceDivergencia] AS ContagemResultado_CntIndDvr, T1.[ContagemResultado_Codigo] FROM ((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T4.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T6 WITH (NOLOCK) ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV82Contratadas, "T4.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + " and (T4.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV31Filtro_ContadorFMCod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContadorFMCod] = @AV31Filtro_ContadorFMCod)";
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! AV19WWPContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_EhContratada] = 1)";
         }
         if ( AV19WWPContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_EhContratante] = 1)";
         }
         if ( (DateTime.MinValue==AV73ContagemResultado_Data) )
         {
            sWhereString = sWhereString + " and (YEAR(T1.[ContagemResultado_DataCnt]) = @AV30Filtro_Ano)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV29Filtro_Mes) && (DateTime.MinValue==AV73ContagemResultado_Data) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataCnt]) = @AV29Filtro_Mes)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV73ContagemResultado_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] = @AV73ContagemResultado_Data)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV75Servico_Codigo > 0 )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Codigo] = @AV75Servico_Codigo)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome], T4.[ContagemResultado_StatusDmn], T4.[ContagemResultado_ContratadaCod]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00BU4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (DateTime)dynConstraints[4] , (long)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (bool)dynConstraints[8] , (bool)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (bool)dynConstraints[13] , (short)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BU2 ;
          prmH00BU2 = new Object[] {
          } ;
          Object[] prmH00BU3 ;
          prmH00BU3 = new Object[] {
          new Object[] {"@AV18AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100Contratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV83Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BU6 ;
          prmH00BU6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BU7 ;
          prmH00BU7 = new Object[] {
          new Object[] {"@AV100Contratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BU8 ;
          prmH00BU8 = new Object[] {
          new Object[] {"@AV100Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BU4 ;
          prmH00BU4 = new Object[] {
          new Object[] {"@AV31Filtro_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV30Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV29Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73ContagemResultado_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BU2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BU2,0,0,true,false )
             ,new CursorDef("H00BU3", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE ( (@AV18AreaTrabalho_Codigo = convert(int, 0)) and T2.[Contratada_PessoaCod] = @AV100Contratada_PessoaCod) or ( @AV18AreaTrabalho_Codigo > 0 and T1.[ContratadaUsuario_ContratadaCod] = @AV83Contratada_Codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BU3,0,0,true,false )
             ,new CursorDef("H00BU4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BU4,100,0,true,false )
             ,new CursorDef("H00BU6", "SELECT COALESCE( T1.[ContagemResultadoErro_Tipo], '') AS ContagemResultadoErro_Tipo FROM (SELECT MIN([ContagemResultadoErro_Tipo]) AS ContagemResultadoErro_Tipo, [ContagemResultado_Codigo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultadoErro_Status] = 'P' GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BU6,1,0,true,false )
             ,new CursorDef("H00BU7", "SELECT TOP 1 [Contratada_PessoaCod], [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE ([Contratada_PessoaCod] = @AV100Contratada_PessoaCod) AND ([Contratada_AreaTrabalhoCod] = @AV18AreaTrabalho_Codigo) ORDER BY [Contratada_PessoaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BU7,1,0,false,true )
             ,new CursorDef("H00BU8", "SELECT [Contratada_PessoaCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_PessoaCod] = @AV100Contratada_PessoaCod ORDER BY [Contratada_PessoaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BU8,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((bool[]) buf[28])[0] = rslt.getBool(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
