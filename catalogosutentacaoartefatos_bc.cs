/*
               File: CatalogoSutentacaoArtefatos_BC
        Description: Catalogo Sutentacao Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:59.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class catalogosutentacaoartefatos_bc : GXHttpHandler, IGxSilentTrn
   {
      public catalogosutentacaoartefatos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public catalogosutentacaoartefatos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4D193( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4D193( ) ;
         standaloneModal( ) ;
         AddRow4D193( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
               Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4D0( )
      {
         BeforeValidate4D193( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4D193( ) ;
            }
            else
            {
               CheckExtendedTable4D193( ) ;
               if ( AnyError == 0 )
               {
                  ZM4D193( 2) ;
                  ZM4D193( 3) ;
               }
               CloseExtendedTableCursors4D193( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4D193( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4D193( )
      {
         /* Using cursor BC004D6 */
         pr_default.execute(4, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound193 = 1;
            ZM4D193( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4D193( ) ;
      }

      protected void OnLoadActions4D193( )
      {
      }

      protected void CheckExtendedTable4D193( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004D4 */
         pr_default.execute(2, new Object[] {A1750CatalogoSutentacao_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Catalogo Sutentacao'.", "ForeignKeyNotFound", 1, "CATALOGOSUTENTACAO_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004D5 */
         pr_default.execute(3, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4D193( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4D193( )
      {
         /* Using cursor BC004D7 */
         pr_default.execute(5, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound193 = 1;
         }
         else
         {
            RcdFound193 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004D3 */
         pr_default.execute(1, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4D193( 1) ;
            RcdFound193 = 1;
            A1750CatalogoSutentacao_Codigo = BC004D3_A1750CatalogoSutentacao_Codigo[0];
            A1749Artefatos_Codigo = BC004D3_A1749Artefatos_Codigo[0];
            Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            sMode193 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4D193( ) ;
            if ( AnyError == 1 )
            {
               RcdFound193 = 0;
               InitializeNonKey4D193( ) ;
            }
            Gx_mode = sMode193;
         }
         else
         {
            RcdFound193 = 0;
            InitializeNonKey4D193( ) ;
            sMode193 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode193;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4D193( ) ;
         if ( RcdFound193 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4D0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4D193( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004D2 */
            pr_default.execute(0, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CatalogoSutentacaoArtefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CatalogoSutentacaoArtefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4D193( )
      {
         BeforeValidate4D193( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4D193( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4D193( 0) ;
            CheckOptimisticConcurrency4D193( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4D193( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4D193( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004D8 */
                     pr_default.execute(6, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("CatalogoSutentacaoArtefatos") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4D193( ) ;
            }
            EndLevel4D193( ) ;
         }
         CloseExtendedTableCursors4D193( ) ;
      }

      protected void Update4D193( )
      {
         BeforeValidate4D193( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4D193( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4D193( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4D193( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4D193( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [CatalogoSutentacaoArtefatos] */
                     DeferredUpdate4D193( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4D193( ) ;
         }
         CloseExtendedTableCursors4D193( ) ;
      }

      protected void DeferredUpdate4D193( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4D193( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4D193( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4D193( ) ;
            AfterConfirm4D193( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4D193( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004D9 */
                  pr_default.execute(7, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("CatalogoSutentacaoArtefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode193 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4D193( ) ;
         Gx_mode = sMode193;
      }

      protected void OnDeleteControls4D193( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4D193( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4D193( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4D193( )
      {
         /* Using cursor BC004D10 */
         pr_default.execute(8, new Object[] {A1750CatalogoSutentacao_Codigo, A1749Artefatos_Codigo});
         RcdFound193 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound193 = 1;
            A1750CatalogoSutentacao_Codigo = BC004D10_A1750CatalogoSutentacao_Codigo[0];
            A1749Artefatos_Codigo = BC004D10_A1749Artefatos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4D193( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound193 = 0;
         ScanKeyLoad4D193( ) ;
      }

      protected void ScanKeyLoad4D193( )
      {
         sMode193 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound193 = 1;
            A1750CatalogoSutentacao_Codigo = BC004D10_A1750CatalogoSutentacao_Codigo[0];
            A1749Artefatos_Codigo = BC004D10_A1749Artefatos_Codigo[0];
         }
         Gx_mode = sMode193;
      }

      protected void ScanKeyEnd4D193( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm4D193( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4D193( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4D193( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4D193( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4D193( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4D193( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4D193( )
      {
      }

      protected void AddRow4D193( )
      {
         VarsToRow193( bcCatalogoSutentacaoArtefatos) ;
      }

      protected void ReadRow4D193( )
      {
         RowToVars193( bcCatalogoSutentacaoArtefatos, 1) ;
      }

      protected void InitializeNonKey4D193( )
      {
      }

      protected void InitAll4D193( )
      {
         A1750CatalogoSutentacao_Codigo = 0;
         A1749Artefatos_Codigo = 0;
         InitializeNonKey4D193( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow193( SdtCatalogoSutentacaoArtefatos obj193 )
      {
         obj193.gxTpr_Mode = Gx_mode;
         obj193.gxTpr_Catalogosutentacao_codigo = A1750CatalogoSutentacao_Codigo;
         obj193.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
         obj193.gxTpr_Catalogosutentacao_codigo_Z = Z1750CatalogoSutentacao_Codigo;
         obj193.gxTpr_Artefatos_codigo_Z = Z1749Artefatos_Codigo;
         obj193.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow193( SdtCatalogoSutentacaoArtefatos obj193 )
      {
         obj193.gxTpr_Catalogosutentacao_codigo = A1750CatalogoSutentacao_Codigo;
         obj193.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
         return  ;
      }

      public void RowToVars193( SdtCatalogoSutentacaoArtefatos obj193 ,
                                int forceLoad )
      {
         Gx_mode = obj193.gxTpr_Mode;
         A1750CatalogoSutentacao_Codigo = obj193.gxTpr_Catalogosutentacao_codigo;
         A1749Artefatos_Codigo = obj193.gxTpr_Artefatos_codigo;
         Z1750CatalogoSutentacao_Codigo = obj193.gxTpr_Catalogosutentacao_codigo_Z;
         Z1749Artefatos_Codigo = obj193.gxTpr_Artefatos_codigo_Z;
         Gx_mode = obj193.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1750CatalogoSutentacao_Codigo = (int)getParm(obj,0);
         A1749Artefatos_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4D193( ) ;
         ScanKeyStart4D193( ) ;
         if ( RcdFound193 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004D11 */
            pr_default.execute(9, new Object[] {A1750CatalogoSutentacao_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Catalogo Sutentacao'.", "ForeignKeyNotFound", 1, "CATALOGOSUTENTACAO_CODIGO");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004D12 */
            pr_default.execute(10, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
         }
         ZM4D193( -1) ;
         OnLoadActions4D193( ) ;
         AddRow4D193( ) ;
         ScanKeyEnd4D193( ) ;
         if ( RcdFound193 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars193( bcCatalogoSutentacaoArtefatos, 0) ;
         ScanKeyStart4D193( ) ;
         if ( RcdFound193 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004D11 */
            pr_default.execute(9, new Object[] {A1750CatalogoSutentacao_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Catalogo Sutentacao'.", "ForeignKeyNotFound", 1, "CATALOGOSUTENTACAO_CODIGO");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004D12 */
            pr_default.execute(10, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
         }
         ZM4D193( -1) ;
         OnLoadActions4D193( ) ;
         AddRow4D193( ) ;
         ScanKeyEnd4D193( ) ;
         if ( RcdFound193 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars193( bcCatalogoSutentacaoArtefatos, 0) ;
         nKeyPressed = 1;
         GetKey4D193( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4D193( ) ;
         }
         else
         {
            if ( RcdFound193 == 1 )
            {
               if ( ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
               {
                  A1750CatalogoSutentacao_Codigo = Z1750CatalogoSutentacao_Codigo;
                  A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4D193( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4D193( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4D193( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow193( bcCatalogoSutentacaoArtefatos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars193( bcCatalogoSutentacaoArtefatos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4D193( ) ;
         if ( RcdFound193 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
            {
               A1750CatalogoSutentacao_Codigo = Z1750CatalogoSutentacao_Codigo;
               A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "CatalogoSutentacaoArtefatos_BC");
         VarsToRow193( bcCatalogoSutentacaoArtefatos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcCatalogoSutentacaoArtefatos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcCatalogoSutentacaoArtefatos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcCatalogoSutentacaoArtefatos )
         {
            bcCatalogoSutentacaoArtefatos = (SdtCatalogoSutentacaoArtefatos)(sdt);
            if ( StringUtil.StrCmp(bcCatalogoSutentacaoArtefatos.gxTpr_Mode, "") == 0 )
            {
               bcCatalogoSutentacaoArtefatos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow193( bcCatalogoSutentacaoArtefatos) ;
            }
            else
            {
               RowToVars193( bcCatalogoSutentacaoArtefatos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcCatalogoSutentacaoArtefatos.gxTpr_Mode, "") == 0 )
            {
               bcCatalogoSutentacaoArtefatos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars193( bcCatalogoSutentacaoArtefatos, 1) ;
         return  ;
      }

      public SdtCatalogoSutentacaoArtefatos CatalogoSutentacaoArtefatos_BC
      {
         get {
            return bcCatalogoSutentacaoArtefatos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC004D6_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D6_A1749Artefatos_Codigo = new int[1] ;
         BC004D4_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D5_A1749Artefatos_Codigo = new int[1] ;
         BC004D7_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D7_A1749Artefatos_Codigo = new int[1] ;
         BC004D3_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D3_A1749Artefatos_Codigo = new int[1] ;
         sMode193 = "";
         BC004D2_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D2_A1749Artefatos_Codigo = new int[1] ;
         BC004D10_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D10_A1749Artefatos_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004D11_A1750CatalogoSutentacao_Codigo = new int[1] ;
         BC004D12_A1749Artefatos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.catalogosutentacaoartefatos_bc__default(),
            new Object[][] {
                new Object[] {
               BC004D2_A1750CatalogoSutentacao_Codigo, BC004D2_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004D3_A1750CatalogoSutentacao_Codigo, BC004D3_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004D4_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               BC004D5_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004D6_A1750CatalogoSutentacao_Codigo, BC004D6_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004D7_A1750CatalogoSutentacao_Codigo, BC004D7_A1749Artefatos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004D10_A1750CatalogoSutentacao_Codigo, BC004D10_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004D11_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               BC004D12_A1749Artefatos_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound193 ;
      private int trnEnded ;
      private int Z1750CatalogoSutentacao_Codigo ;
      private int A1750CatalogoSutentacao_Codigo ;
      private int Z1749Artefatos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode193 ;
      private SdtCatalogoSutentacaoArtefatos bcCatalogoSutentacaoArtefatos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004D6_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D6_A1749Artefatos_Codigo ;
      private int[] BC004D4_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D5_A1749Artefatos_Codigo ;
      private int[] BC004D7_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D7_A1749Artefatos_Codigo ;
      private int[] BC004D3_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D3_A1749Artefatos_Codigo ;
      private int[] BC004D2_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D2_A1749Artefatos_Codigo ;
      private int[] BC004D10_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D10_A1749Artefatos_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC004D11_A1750CatalogoSutentacao_Codigo ;
      private int[] BC004D12_A1749Artefatos_Codigo ;
   }

   public class catalogosutentacaoartefatos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004D6 ;
          prmBC004D6 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D4 ;
          prmBC004D4 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D5 ;
          prmBC004D5 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D7 ;
          prmBC004D7 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D3 ;
          prmBC004D3 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D2 ;
          prmBC004D2 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D8 ;
          prmBC004D8 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D9 ;
          prmBC004D9 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D10 ;
          prmBC004D10 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D11 ;
          prmBC004D11 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004D12 ;
          prmBC004D12 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004D2", "SELECT [CatalogoSutentacao_Codigo], [Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (UPDLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D2,1,0,true,false )
             ,new CursorDef("BC004D3", "SELECT [CatalogoSutentacao_Codigo], [Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D3,1,0,true,false )
             ,new CursorDef("BC004D4", "SELECT [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D4,1,0,true,false )
             ,new CursorDef("BC004D5", "SELECT [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D5,1,0,true,false )
             ,new CursorDef("BC004D6", "SELECT TM1.[CatalogoSutentacao_Codigo], TM1.[Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] TM1 WITH (NOLOCK) WHERE TM1.[CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo and TM1.[Artefatos_Codigo] = @Artefatos_Codigo ORDER BY TM1.[CatalogoSutentacao_Codigo], TM1.[Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D6,100,0,true,false )
             ,new CursorDef("BC004D7", "SELECT [CatalogoSutentacao_Codigo], [Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D7,1,0,true,false )
             ,new CursorDef("BC004D8", "INSERT INTO [CatalogoSutentacaoArtefatos]([CatalogoSutentacao_Codigo], [Artefatos_Codigo]) VALUES(@CatalogoSutentacao_Codigo, @Artefatos_Codigo)", GxErrorMask.GX_NOMASK,prmBC004D8)
             ,new CursorDef("BC004D9", "DELETE FROM [CatalogoSutentacaoArtefatos]  WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmBC004D9)
             ,new CursorDef("BC004D10", "SELECT TM1.[CatalogoSutentacao_Codigo], TM1.[Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] TM1 WITH (NOLOCK) WHERE TM1.[CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo and TM1.[Artefatos_Codigo] = @Artefatos_Codigo ORDER BY TM1.[CatalogoSutentacao_Codigo], TM1.[Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D10,100,0,true,false )
             ,new CursorDef("BC004D11", "SELECT [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D11,1,0,true,false )
             ,new CursorDef("BC004D12", "SELECT [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004D12,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
