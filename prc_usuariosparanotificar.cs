/*
               File: PRC_UsuariosParaNotificar
        Description: Usuarios Para Notificar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:43:0.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuariosparanotificar : GXProcedure
   {
      public prc_usuariosparanotificar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuariosparanotificar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           String aP1_Status )
      {
         this.AV12Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV18Status = aP1_Status;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 String aP1_Status )
      {
         prc_usuariosparanotificar objprc_usuariosparanotificar;
         objprc_usuariosparanotificar = new prc_usuariosparanotificar();
         objprc_usuariosparanotificar.AV12Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_usuariosparanotificar.AV18Status = aP1_Status;
         objprc_usuariosparanotificar.context.SetSubmitInitialConfig(context);
         objprc_usuariosparanotificar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuariosparanotificar);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuariosparanotificar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Usuarios.FromXml(AV8WebSession.Get("Usuarios"), "Collection");
         /* Using cursor P009M2 */
         pr_default.execute(0, new Object[] {AV12Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P009M2_A74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P009M2_A75Contrato_AreaTrabalhoCod[0];
            A1013Contrato_PrepostoCod = P009M2_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P009M2_n1013Contrato_PrepostoCod[0];
            if ( ( A1013Contrato_PrepostoCod > 0 ) && (0==AV9Usuarios.IndexOf(A1013Contrato_PrepostoCod)) && new prc_usuarionotificanostatus(context).executeUdp( ref  A1013Contrato_PrepostoCod, ref  A75Contrato_AreaTrabalhoCod,  AV18Status) )
            {
               AV9Usuarios.Add(A1013Contrato_PrepostoCod, 0);
            }
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A1079ContratoGestor_UsuarioCod ,
                                                 AV9Usuarios ,
                                                 A74Contrato_Codigo ,
                                                 A1078ContratoGestor_ContratoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor P009M3 */
            pr_default.execute(1, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P009M3_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P009M3_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P009M3_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P009M3_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P009M3_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P009M3_n1446ContratoGestor_ContratadaAreaCod[0];
               if ( new prc_usuarionotificanostatus(context).executeUdp( ref  A1079ContratoGestor_UsuarioCod, ref  A1446ContratoGestor_ContratadaAreaCod,  AV18Status) )
               {
                  AV9Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV8WebSession.Set("Usuarios", AV9Usuarios.ToXml(false, true, "Collection", ""));
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9Usuarios = new GxSimpleCollection();
         AV8WebSession = context.GetSession();
         scmdbuf = "";
         P009M2_A74Contrato_Codigo = new int[1] ;
         P009M2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P009M2_A1013Contrato_PrepostoCod = new int[1] ;
         P009M2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P009M3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P009M3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P009M3_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P009M3_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuariosparanotificar__default(),
            new Object[][] {
                new Object[] {
               P009M2_A74Contrato_Codigo, P009M2_A75Contrato_AreaTrabalhoCod, P009M2_A1013Contrato_PrepostoCod, P009M2_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P009M3_A1078ContratoGestor_ContratoCod, P009M3_A1079ContratoGestor_UsuarioCod, P009M3_A1446ContratoGestor_ContratadaAreaCod, P009M3_n1446ContratoGestor_ContratadaAreaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV12Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private String AV18Status ;
      private String scmdbuf ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private IGxSession AV8WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P009M2_A74Contrato_Codigo ;
      private int[] P009M2_A75Contrato_AreaTrabalhoCod ;
      private int[] P009M2_A1013Contrato_PrepostoCod ;
      private bool[] P009M2_n1013Contrato_PrepostoCod ;
      private int[] P009M3_A1078ContratoGestor_ContratoCod ;
      private int[] P009M3_A1079ContratoGestor_UsuarioCod ;
      private int[] P009M3_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P009M3_n1446ContratoGestor_ContratadaAreaCod ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV9Usuarios ;
   }

   public class prc_usuariosparanotificar__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P009M3( IGxContext context ,
                                             int A1079ContratoGestor_UsuarioCod ,
                                             IGxCollection AV9Usuarios ,
                                             int A74Contrato_Codigo ,
                                             int A1078ContratoGestor_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Usuarios, "T1.[ContratoGestor_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_ContratoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P009M3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009M2 ;
          prmP009M2 = new Object[] {
          new Object[] {"@AV12Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009M3 ;
          prmP009M3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009M2", "SELECT TOP 1 [Contrato_Codigo], [Contrato_AreaTrabalhoCod], [Contrato_PrepostoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV12Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009M2,1,0,true,true )
             ,new CursorDef("P009M3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009M3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
