/*
               File: GetWWServicoFilterData
        Description: Get WWServico Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:28.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwservicofilterdata : GXProcedure
   {
      public getwwservicofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwservicofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwservicofilterdata objgetwwservicofilterdata;
         objgetwwservicofilterdata = new getwwservicofilterdata();
         objgetwwservicofilterdata.AV20DDOName = aP0_DDOName;
         objgetwwservicofilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwservicofilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwservicofilterdata.AV24OptionsJson = "" ;
         objgetwwservicofilterdata.AV27OptionsDescJson = "" ;
         objgetwwservicofilterdata.AV29OptionIndexesJson = "" ;
         objgetwwservicofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwservicofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwservicofilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwservicofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_SERVICO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_SERVICOGRUPO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWServicoGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWServicoGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWServicoGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV10TFServico_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV11TFServico_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA") == 0 )
            {
               AV12TFServico_Sigla = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA_SEL") == 0 )
            {
               AV13TFServico_Sigla_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV14TFServicoGrupo_Descricao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO_SEL") == 0 )
            {
               AV15TFServicoGrupo_Descricao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFSERVICO_OBJETOCONTROLE_SEL") == 0 )
            {
               AV16TFServico_ObjetoControle_SelsJson = AV34GridStateFilterValue.gxTpr_Value;
               AV17TFServico_ObjetoControle_Sels.FromJSonString(AV16TFServico_ObjetoControle_SelsJson);
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 )
            {
               AV37Servico_Sigla1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV38Servico_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
            {
               AV39ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( AV35GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 )
               {
                  AV42Servico_Sigla2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV43Servico_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 )
               {
                  AV44ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( AV35GridStateDynamicFilter.gxTpr_Value, "."));
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV10TFServico_Nome = AV18SearchTxt;
         AV11TFServico_Nome_Sel = "";
         AV49WWServicoDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV50WWServicoDS_2_Servico_sigla1 = AV37Servico_Sigla1;
         AV51WWServicoDS_3_Servico_nome1 = AV38Servico_Nome1;
         AV52WWServicoDS_4_Servicogrupo_codigo1 = AV39ServicoGrupo_Codigo1;
         AV53WWServicoDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV54WWServicoDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV55WWServicoDS_7_Servico_sigla2 = AV42Servico_Sigla2;
         AV56WWServicoDS_8_Servico_nome2 = AV43Servico_Nome2;
         AV57WWServicoDS_9_Servicogrupo_codigo2 = AV44ServicoGrupo_Codigo2;
         AV58WWServicoDS_10_Tfservico_nome = AV10TFServico_Nome;
         AV59WWServicoDS_11_Tfservico_nome_sel = AV11TFServico_Nome_Sel;
         AV60WWServicoDS_12_Tfservico_sigla = AV12TFServico_Sigla;
         AV61WWServicoDS_13_Tfservico_sigla_sel = AV13TFServico_Sigla_Sel;
         AV62WWServicoDS_14_Tfservicogrupo_descricao = AV14TFServicoGrupo_Descricao;
         AV63WWServicoDS_15_Tfservicogrupo_descricao_sel = AV15TFServicoGrupo_Descricao_Sel;
         AV64WWServicoDS_16_Tfservico_objetocontrole_sels = AV17TFServico_ObjetoControle_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1436Servico_ObjetoControle ,
                                              AV64WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                              AV49WWServicoDS_1_Dynamicfiltersselector1 ,
                                              AV50WWServicoDS_2_Servico_sigla1 ,
                                              AV51WWServicoDS_3_Servico_nome1 ,
                                              AV52WWServicoDS_4_Servicogrupo_codigo1 ,
                                              AV53WWServicoDS_5_Dynamicfiltersenabled2 ,
                                              AV54WWServicoDS_6_Dynamicfiltersselector2 ,
                                              AV55WWServicoDS_7_Servico_sigla2 ,
                                              AV56WWServicoDS_8_Servico_nome2 ,
                                              AV57WWServicoDS_9_Servicogrupo_codigo2 ,
                                              AV59WWServicoDS_11_Tfservico_nome_sel ,
                                              AV58WWServicoDS_10_Tfservico_nome ,
                                              AV61WWServicoDS_13_Tfservico_sigla_sel ,
                                              AV60WWServicoDS_12_Tfservico_sigla ,
                                              AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                              AV62WWServicoDS_14_Tfservicogrupo_descricao ,
                                              AV64WWServicoDS_16_Tfservico_objetocontrole_sels.Count ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV51WWServicoDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWServicoDS_3_Servico_nome1), 50, "%");
         lV56WWServicoDS_8_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWServicoDS_8_Servico_nome2), 50, "%");
         lV58WWServicoDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWServicoDS_10_Tfservico_nome), 50, "%");
         lV60WWServicoDS_12_Tfservico_sigla = StringUtil.PadR( StringUtil.RTrim( AV60WWServicoDS_12_Tfservico_sigla), 15, "%");
         lV62WWServicoDS_14_Tfservicogrupo_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWServicoDS_14_Tfservicogrupo_descricao), "%", "");
         /* Using cursor P00JM2 */
         pr_default.execute(0, new Object[] {AV50WWServicoDS_2_Servico_sigla1, lV51WWServicoDS_3_Servico_nome1, AV52WWServicoDS_4_Servicogrupo_codigo1, AV55WWServicoDS_7_Servico_sigla2, lV56WWServicoDS_8_Servico_nome2, AV57WWServicoDS_9_Servicogrupo_codigo2, lV58WWServicoDS_10_Tfservico_nome, AV59WWServicoDS_11_Tfservico_nome_sel, lV60WWServicoDS_12_Tfservico_sigla, AV61WWServicoDS_13_Tfservico_sigla_sel, lV62WWServicoDS_14_Tfservicogrupo_descricao, AV63WWServicoDS_15_Tfservicogrupo_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJM2 = false;
            A608Servico_Nome = P00JM2_A608Servico_Nome[0];
            A1436Servico_ObjetoControle = P00JM2_A1436Servico_ObjetoControle[0];
            A158ServicoGrupo_Descricao = P00JM2_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JM2_A159ServicoGrupo_Ativo[0];
            A157ServicoGrupo_Codigo = P00JM2_A157ServicoGrupo_Codigo[0];
            A605Servico_Sigla = P00JM2_A605Servico_Sigla[0];
            A155Servico_Codigo = P00JM2_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00JM2_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JM2_A159ServicoGrupo_Ativo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JM2_A608Servico_Nome[0], A608Servico_Nome) == 0 ) )
            {
               BRKJM2 = false;
               A155Servico_Codigo = P00JM2_A155Servico_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKJM2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV22Option = A608Servico_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJM2 )
            {
               BRKJM2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICO_SIGLAOPTIONS' Routine */
         AV12TFServico_Sigla = AV18SearchTxt;
         AV13TFServico_Sigla_Sel = "";
         AV49WWServicoDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV50WWServicoDS_2_Servico_sigla1 = AV37Servico_Sigla1;
         AV51WWServicoDS_3_Servico_nome1 = AV38Servico_Nome1;
         AV52WWServicoDS_4_Servicogrupo_codigo1 = AV39ServicoGrupo_Codigo1;
         AV53WWServicoDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV54WWServicoDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV55WWServicoDS_7_Servico_sigla2 = AV42Servico_Sigla2;
         AV56WWServicoDS_8_Servico_nome2 = AV43Servico_Nome2;
         AV57WWServicoDS_9_Servicogrupo_codigo2 = AV44ServicoGrupo_Codigo2;
         AV58WWServicoDS_10_Tfservico_nome = AV10TFServico_Nome;
         AV59WWServicoDS_11_Tfservico_nome_sel = AV11TFServico_Nome_Sel;
         AV60WWServicoDS_12_Tfservico_sigla = AV12TFServico_Sigla;
         AV61WWServicoDS_13_Tfservico_sigla_sel = AV13TFServico_Sigla_Sel;
         AV62WWServicoDS_14_Tfservicogrupo_descricao = AV14TFServicoGrupo_Descricao;
         AV63WWServicoDS_15_Tfservicogrupo_descricao_sel = AV15TFServicoGrupo_Descricao_Sel;
         AV64WWServicoDS_16_Tfservico_objetocontrole_sels = AV17TFServico_ObjetoControle_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1436Servico_ObjetoControle ,
                                              AV64WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                              AV49WWServicoDS_1_Dynamicfiltersselector1 ,
                                              AV50WWServicoDS_2_Servico_sigla1 ,
                                              AV51WWServicoDS_3_Servico_nome1 ,
                                              AV52WWServicoDS_4_Servicogrupo_codigo1 ,
                                              AV53WWServicoDS_5_Dynamicfiltersenabled2 ,
                                              AV54WWServicoDS_6_Dynamicfiltersselector2 ,
                                              AV55WWServicoDS_7_Servico_sigla2 ,
                                              AV56WWServicoDS_8_Servico_nome2 ,
                                              AV57WWServicoDS_9_Servicogrupo_codigo2 ,
                                              AV59WWServicoDS_11_Tfservico_nome_sel ,
                                              AV58WWServicoDS_10_Tfservico_nome ,
                                              AV61WWServicoDS_13_Tfservico_sigla_sel ,
                                              AV60WWServicoDS_12_Tfservico_sigla ,
                                              AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                              AV62WWServicoDS_14_Tfservicogrupo_descricao ,
                                              AV64WWServicoDS_16_Tfservico_objetocontrole_sels.Count ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV51WWServicoDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWServicoDS_3_Servico_nome1), 50, "%");
         lV56WWServicoDS_8_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWServicoDS_8_Servico_nome2), 50, "%");
         lV58WWServicoDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWServicoDS_10_Tfservico_nome), 50, "%");
         lV60WWServicoDS_12_Tfservico_sigla = StringUtil.PadR( StringUtil.RTrim( AV60WWServicoDS_12_Tfservico_sigla), 15, "%");
         lV62WWServicoDS_14_Tfservicogrupo_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWServicoDS_14_Tfservicogrupo_descricao), "%", "");
         /* Using cursor P00JM3 */
         pr_default.execute(1, new Object[] {AV50WWServicoDS_2_Servico_sigla1, lV51WWServicoDS_3_Servico_nome1, AV52WWServicoDS_4_Servicogrupo_codigo1, AV55WWServicoDS_7_Servico_sigla2, lV56WWServicoDS_8_Servico_nome2, AV57WWServicoDS_9_Servicogrupo_codigo2, lV58WWServicoDS_10_Tfservico_nome, AV59WWServicoDS_11_Tfservico_nome_sel, lV60WWServicoDS_12_Tfservico_sigla, AV61WWServicoDS_13_Tfservico_sigla_sel, lV62WWServicoDS_14_Tfservicogrupo_descricao, AV63WWServicoDS_15_Tfservicogrupo_descricao_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJM4 = false;
            A605Servico_Sigla = P00JM3_A605Servico_Sigla[0];
            A1436Servico_ObjetoControle = P00JM3_A1436Servico_ObjetoControle[0];
            A158ServicoGrupo_Descricao = P00JM3_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JM3_A159ServicoGrupo_Ativo[0];
            A157ServicoGrupo_Codigo = P00JM3_A157ServicoGrupo_Codigo[0];
            A608Servico_Nome = P00JM3_A608Servico_Nome[0];
            A155Servico_Codigo = P00JM3_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00JM3_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JM3_A159ServicoGrupo_Ativo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00JM3_A605Servico_Sigla[0], A605Servico_Sigla) == 0 ) )
            {
               BRKJM4 = false;
               A155Servico_Codigo = P00JM3_A155Servico_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKJM4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
            {
               AV22Option = A605Servico_Sigla;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJM4 )
            {
               BRKJM4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' Routine */
         AV14TFServicoGrupo_Descricao = AV18SearchTxt;
         AV15TFServicoGrupo_Descricao_Sel = "";
         AV49WWServicoDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV50WWServicoDS_2_Servico_sigla1 = AV37Servico_Sigla1;
         AV51WWServicoDS_3_Servico_nome1 = AV38Servico_Nome1;
         AV52WWServicoDS_4_Servicogrupo_codigo1 = AV39ServicoGrupo_Codigo1;
         AV53WWServicoDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV54WWServicoDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV55WWServicoDS_7_Servico_sigla2 = AV42Servico_Sigla2;
         AV56WWServicoDS_8_Servico_nome2 = AV43Servico_Nome2;
         AV57WWServicoDS_9_Servicogrupo_codigo2 = AV44ServicoGrupo_Codigo2;
         AV58WWServicoDS_10_Tfservico_nome = AV10TFServico_Nome;
         AV59WWServicoDS_11_Tfservico_nome_sel = AV11TFServico_Nome_Sel;
         AV60WWServicoDS_12_Tfservico_sigla = AV12TFServico_Sigla;
         AV61WWServicoDS_13_Tfservico_sigla_sel = AV13TFServico_Sigla_Sel;
         AV62WWServicoDS_14_Tfservicogrupo_descricao = AV14TFServicoGrupo_Descricao;
         AV63WWServicoDS_15_Tfservicogrupo_descricao_sel = AV15TFServicoGrupo_Descricao_Sel;
         AV64WWServicoDS_16_Tfservico_objetocontrole_sels = AV17TFServico_ObjetoControle_Sels;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1436Servico_ObjetoControle ,
                                              AV64WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                              AV49WWServicoDS_1_Dynamicfiltersselector1 ,
                                              AV50WWServicoDS_2_Servico_sigla1 ,
                                              AV51WWServicoDS_3_Servico_nome1 ,
                                              AV52WWServicoDS_4_Servicogrupo_codigo1 ,
                                              AV53WWServicoDS_5_Dynamicfiltersenabled2 ,
                                              AV54WWServicoDS_6_Dynamicfiltersselector2 ,
                                              AV55WWServicoDS_7_Servico_sigla2 ,
                                              AV56WWServicoDS_8_Servico_nome2 ,
                                              AV57WWServicoDS_9_Servicogrupo_codigo2 ,
                                              AV59WWServicoDS_11_Tfservico_nome_sel ,
                                              AV58WWServicoDS_10_Tfservico_nome ,
                                              AV61WWServicoDS_13_Tfservico_sigla_sel ,
                                              AV60WWServicoDS_12_Tfservico_sigla ,
                                              AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                              AV62WWServicoDS_14_Tfservicogrupo_descricao ,
                                              AV64WWServicoDS_16_Tfservico_objetocontrole_sels.Count ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV51WWServicoDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWServicoDS_3_Servico_nome1), 50, "%");
         lV56WWServicoDS_8_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV56WWServicoDS_8_Servico_nome2), 50, "%");
         lV58WWServicoDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV58WWServicoDS_10_Tfservico_nome), 50, "%");
         lV60WWServicoDS_12_Tfservico_sigla = StringUtil.PadR( StringUtil.RTrim( AV60WWServicoDS_12_Tfservico_sigla), 15, "%");
         lV62WWServicoDS_14_Tfservicogrupo_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWServicoDS_14_Tfservicogrupo_descricao), "%", "");
         /* Using cursor P00JM4 */
         pr_default.execute(2, new Object[] {AV50WWServicoDS_2_Servico_sigla1, lV51WWServicoDS_3_Servico_nome1, AV52WWServicoDS_4_Servicogrupo_codigo1, AV55WWServicoDS_7_Servico_sigla2, lV56WWServicoDS_8_Servico_nome2, AV57WWServicoDS_9_Servicogrupo_codigo2, lV58WWServicoDS_10_Tfservico_nome, AV59WWServicoDS_11_Tfservico_nome_sel, lV60WWServicoDS_12_Tfservico_sigla, AV61WWServicoDS_13_Tfservico_sigla_sel, lV62WWServicoDS_14_Tfservicogrupo_descricao, AV63WWServicoDS_15_Tfservicogrupo_descricao_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJM6 = false;
            A157ServicoGrupo_Codigo = P00JM4_A157ServicoGrupo_Codigo[0];
            A1436Servico_ObjetoControle = P00JM4_A1436Servico_ObjetoControle[0];
            A158ServicoGrupo_Descricao = P00JM4_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JM4_A159ServicoGrupo_Ativo[0];
            A608Servico_Nome = P00JM4_A608Servico_Nome[0];
            A605Servico_Sigla = P00JM4_A605Servico_Sigla[0];
            A155Servico_Codigo = P00JM4_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00JM4_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JM4_A159ServicoGrupo_Ativo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00JM4_A157ServicoGrupo_Codigo[0] == A157ServicoGrupo_Codigo ) )
            {
               BRKJM6 = false;
               A155Servico_Codigo = P00JM4_A155Servico_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKJM6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A158ServicoGrupo_Descricao)) )
            {
               AV22Option = A158ServicoGrupo_Descricao;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJM6 )
            {
               BRKJM6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFServico_Nome = "";
         AV11TFServico_Nome_Sel = "";
         AV12TFServico_Sigla = "";
         AV13TFServico_Sigla_Sel = "";
         AV14TFServicoGrupo_Descricao = "";
         AV15TFServicoGrupo_Descricao_Sel = "";
         AV16TFServico_ObjetoControle_SelsJson = "";
         AV17TFServico_ObjetoControle_Sels = new GxSimpleCollection();
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37Servico_Sigla1 = "";
         AV38Servico_Nome1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42Servico_Sigla2 = "";
         AV43Servico_Nome2 = "";
         AV49WWServicoDS_1_Dynamicfiltersselector1 = "";
         AV50WWServicoDS_2_Servico_sigla1 = "";
         AV51WWServicoDS_3_Servico_nome1 = "";
         AV54WWServicoDS_6_Dynamicfiltersselector2 = "";
         AV55WWServicoDS_7_Servico_sigla2 = "";
         AV56WWServicoDS_8_Servico_nome2 = "";
         AV58WWServicoDS_10_Tfservico_nome = "";
         AV59WWServicoDS_11_Tfservico_nome_sel = "";
         AV60WWServicoDS_12_Tfservico_sigla = "";
         AV61WWServicoDS_13_Tfservico_sigla_sel = "";
         AV62WWServicoDS_14_Tfservicogrupo_descricao = "";
         AV63WWServicoDS_15_Tfservicogrupo_descricao_sel = "";
         AV64WWServicoDS_16_Tfservico_objetocontrole_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV51WWServicoDS_3_Servico_nome1 = "";
         lV56WWServicoDS_8_Servico_nome2 = "";
         lV58WWServicoDS_10_Tfservico_nome = "";
         lV60WWServicoDS_12_Tfservico_sigla = "";
         lV62WWServicoDS_14_Tfservicogrupo_descricao = "";
         A1436Servico_ObjetoControle = "";
         A605Servico_Sigla = "";
         A608Servico_Nome = "";
         A158ServicoGrupo_Descricao = "";
         P00JM2_A608Servico_Nome = new String[] {""} ;
         P00JM2_A1436Servico_ObjetoControle = new String[] {""} ;
         P00JM2_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JM2_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JM2_A157ServicoGrupo_Codigo = new int[1] ;
         P00JM2_A605Servico_Sigla = new String[] {""} ;
         P00JM2_A155Servico_Codigo = new int[1] ;
         AV22Option = "";
         P00JM3_A605Servico_Sigla = new String[] {""} ;
         P00JM3_A1436Servico_ObjetoControle = new String[] {""} ;
         P00JM3_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JM3_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JM3_A157ServicoGrupo_Codigo = new int[1] ;
         P00JM3_A608Servico_Nome = new String[] {""} ;
         P00JM3_A155Servico_Codigo = new int[1] ;
         P00JM4_A157ServicoGrupo_Codigo = new int[1] ;
         P00JM4_A1436Servico_ObjetoControle = new String[] {""} ;
         P00JM4_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JM4_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JM4_A608Servico_Nome = new String[] {""} ;
         P00JM4_A605Servico_Sigla = new String[] {""} ;
         P00JM4_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwservicofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JM2_A608Servico_Nome, P00JM2_A1436Servico_ObjetoControle, P00JM2_A158ServicoGrupo_Descricao, P00JM2_A159ServicoGrupo_Ativo, P00JM2_A157ServicoGrupo_Codigo, P00JM2_A605Servico_Sigla, P00JM2_A155Servico_Codigo
               }
               , new Object[] {
               P00JM3_A605Servico_Sigla, P00JM3_A1436Servico_ObjetoControle, P00JM3_A158ServicoGrupo_Descricao, P00JM3_A159ServicoGrupo_Ativo, P00JM3_A157ServicoGrupo_Codigo, P00JM3_A608Servico_Nome, P00JM3_A155Servico_Codigo
               }
               , new Object[] {
               P00JM4_A157ServicoGrupo_Codigo, P00JM4_A1436Servico_ObjetoControle, P00JM4_A158ServicoGrupo_Descricao, P00JM4_A159ServicoGrupo_Ativo, P00JM4_A608Servico_Nome, P00JM4_A605Servico_Sigla, P00JM4_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV47GXV1 ;
      private int AV39ServicoGrupo_Codigo1 ;
      private int AV44ServicoGrupo_Codigo2 ;
      private int AV52WWServicoDS_4_Servicogrupo_codigo1 ;
      private int AV57WWServicoDS_9_Servicogrupo_codigo2 ;
      private int AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count ;
      private int A157ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String AV10TFServico_Nome ;
      private String AV11TFServico_Nome_Sel ;
      private String AV12TFServico_Sigla ;
      private String AV13TFServico_Sigla_Sel ;
      private String AV37Servico_Sigla1 ;
      private String AV38Servico_Nome1 ;
      private String AV42Servico_Sigla2 ;
      private String AV43Servico_Nome2 ;
      private String AV50WWServicoDS_2_Servico_sigla1 ;
      private String AV51WWServicoDS_3_Servico_nome1 ;
      private String AV55WWServicoDS_7_Servico_sigla2 ;
      private String AV56WWServicoDS_8_Servico_nome2 ;
      private String AV58WWServicoDS_10_Tfservico_nome ;
      private String AV59WWServicoDS_11_Tfservico_nome_sel ;
      private String AV60WWServicoDS_12_Tfservico_sigla ;
      private String AV61WWServicoDS_13_Tfservico_sigla_sel ;
      private String scmdbuf ;
      private String lV51WWServicoDS_3_Servico_nome1 ;
      private String lV56WWServicoDS_8_Servico_nome2 ;
      private String lV58WWServicoDS_10_Tfservico_nome ;
      private String lV60WWServicoDS_12_Tfservico_sigla ;
      private String A1436Servico_ObjetoControle ;
      private String A605Servico_Sigla ;
      private String A608Servico_Nome ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV53WWServicoDS_5_Dynamicfiltersenabled2 ;
      private bool A159ServicoGrupo_Ativo ;
      private bool BRKJM2 ;
      private bool BRKJM4 ;
      private bool BRKJM6 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV16TFServico_ObjetoControle_SelsJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV14TFServicoGrupo_Descricao ;
      private String AV15TFServicoGrupo_Descricao_Sel ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV49WWServicoDS_1_Dynamicfiltersselector1 ;
      private String AV54WWServicoDS_6_Dynamicfiltersselector2 ;
      private String AV62WWServicoDS_14_Tfservicogrupo_descricao ;
      private String AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ;
      private String lV62WWServicoDS_14_Tfservicogrupo_descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00JM2_A608Servico_Nome ;
      private String[] P00JM2_A1436Servico_ObjetoControle ;
      private String[] P00JM2_A158ServicoGrupo_Descricao ;
      private bool[] P00JM2_A159ServicoGrupo_Ativo ;
      private int[] P00JM2_A157ServicoGrupo_Codigo ;
      private String[] P00JM2_A605Servico_Sigla ;
      private int[] P00JM2_A155Servico_Codigo ;
      private String[] P00JM3_A605Servico_Sigla ;
      private String[] P00JM3_A1436Servico_ObjetoControle ;
      private String[] P00JM3_A158ServicoGrupo_Descricao ;
      private bool[] P00JM3_A159ServicoGrupo_Ativo ;
      private int[] P00JM3_A157ServicoGrupo_Codigo ;
      private String[] P00JM3_A608Servico_Nome ;
      private int[] P00JM3_A155Servico_Codigo ;
      private int[] P00JM4_A157ServicoGrupo_Codigo ;
      private String[] P00JM4_A1436Servico_ObjetoControle ;
      private String[] P00JM4_A158ServicoGrupo_Descricao ;
      private bool[] P00JM4_A159ServicoGrupo_Ativo ;
      private String[] P00JM4_A608Servico_Nome ;
      private String[] P00JM4_A605Servico_Sigla ;
      private int[] P00JM4_A155Servico_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17TFServico_ObjetoControle_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV64WWServicoDS_16_Tfservico_objetocontrole_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwservicofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JM2( IGxContext context ,
                                             String A1436Servico_ObjetoControle ,
                                             IGxCollection AV64WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                             String AV49WWServicoDS_1_Dynamicfiltersselector1 ,
                                             String AV50WWServicoDS_2_Servico_sigla1 ,
                                             String AV51WWServicoDS_3_Servico_nome1 ,
                                             int AV52WWServicoDS_4_Servicogrupo_codigo1 ,
                                             bool AV53WWServicoDS_5_Dynamicfiltersenabled2 ,
                                             String AV54WWServicoDS_6_Dynamicfiltersselector2 ,
                                             String AV55WWServicoDS_7_Servico_sigla2 ,
                                             String AV56WWServicoDS_8_Servico_nome2 ,
                                             int AV57WWServicoDS_9_Servicogrupo_codigo2 ,
                                             String AV59WWServicoDS_11_Tfservico_nome_sel ,
                                             String AV58WWServicoDS_10_Tfservico_nome ,
                                             String AV61WWServicoDS_13_Tfservico_sigla_sel ,
                                             String AV60WWServicoDS_12_Tfservico_sigla ,
                                             String AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                             String AV62WWServicoDS_14_Tfservicogrupo_descricao ,
                                             int AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Nome], T1.[Servico_ObjetoControle], T2.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Ativo], T1.[ServicoGrupo_Codigo], T1.[Servico_Sigla], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWServicoDS_2_Servico_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV50WWServicoDS_2_Servico_sigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV50WWServicoDS_2_Servico_sigla1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV51WWServicoDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV51WWServicoDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV52WWServicoDS_4_Servicogrupo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV52WWServicoDS_4_Servicogrupo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV52WWServicoDS_4_Servicogrupo_codigo1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWServicoDS_7_Servico_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV55WWServicoDS_7_Servico_sigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV55WWServicoDS_7_Servico_sigla2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoDS_8_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV56WWServicoDS_8_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV56WWServicoDS_8_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV57WWServicoDS_9_Servicogrupo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV57WWServicoDS_9_Servicogrupo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV57WWServicoDS_9_Servicogrupo_codigo2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWServicoDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV58WWServicoDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like @lV58WWServicoDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWServicoDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV59WWServicoDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] = @AV59WWServicoDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoDS_13_Tfservico_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWServicoDS_12_Tfservico_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV60WWServicoDS_12_Tfservico_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] like @lV60WWServicoDS_12_Tfservico_sigla)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoDS_13_Tfservico_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV61WWServicoDS_13_Tfservico_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV61WWServicoDS_13_Tfservico_sigla_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWServicoDS_14_Tfservicogrupo_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV62WWServicoDS_14_Tfservicogrupo_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] like @lV62WWServicoDS_14_Tfservicogrupo_descricao)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] = @AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JM3( IGxContext context ,
                                             String A1436Servico_ObjetoControle ,
                                             IGxCollection AV64WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                             String AV49WWServicoDS_1_Dynamicfiltersselector1 ,
                                             String AV50WWServicoDS_2_Servico_sigla1 ,
                                             String AV51WWServicoDS_3_Servico_nome1 ,
                                             int AV52WWServicoDS_4_Servicogrupo_codigo1 ,
                                             bool AV53WWServicoDS_5_Dynamicfiltersenabled2 ,
                                             String AV54WWServicoDS_6_Dynamicfiltersselector2 ,
                                             String AV55WWServicoDS_7_Servico_sigla2 ,
                                             String AV56WWServicoDS_8_Servico_nome2 ,
                                             int AV57WWServicoDS_9_Servicogrupo_codigo2 ,
                                             String AV59WWServicoDS_11_Tfservico_nome_sel ,
                                             String AV58WWServicoDS_10_Tfservico_nome ,
                                             String AV61WWServicoDS_13_Tfservico_sigla_sel ,
                                             String AV60WWServicoDS_12_Tfservico_sigla ,
                                             String AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                             String AV62WWServicoDS_14_Tfservicogrupo_descricao ,
                                             int AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Sigla], T1.[Servico_ObjetoControle], T2.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Ativo], T1.[ServicoGrupo_Codigo], T1.[Servico_Nome], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWServicoDS_2_Servico_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV50WWServicoDS_2_Servico_sigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV50WWServicoDS_2_Servico_sigla1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV51WWServicoDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV51WWServicoDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV52WWServicoDS_4_Servicogrupo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV52WWServicoDS_4_Servicogrupo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV52WWServicoDS_4_Servicogrupo_codigo1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWServicoDS_7_Servico_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV55WWServicoDS_7_Servico_sigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV55WWServicoDS_7_Servico_sigla2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoDS_8_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV56WWServicoDS_8_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV56WWServicoDS_8_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV57WWServicoDS_9_Servicogrupo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV57WWServicoDS_9_Servicogrupo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV57WWServicoDS_9_Servicogrupo_codigo2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWServicoDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV58WWServicoDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like @lV58WWServicoDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWServicoDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV59WWServicoDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] = @AV59WWServicoDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoDS_13_Tfservico_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWServicoDS_12_Tfservico_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV60WWServicoDS_12_Tfservico_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] like @lV60WWServicoDS_12_Tfservico_sigla)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoDS_13_Tfservico_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV61WWServicoDS_13_Tfservico_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV61WWServicoDS_13_Tfservico_sigla_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWServicoDS_14_Tfservicogrupo_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV62WWServicoDS_14_Tfservicogrupo_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] like @lV62WWServicoDS_14_Tfservicogrupo_descricao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] = @AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00JM4( IGxContext context ,
                                             String A1436Servico_ObjetoControle ,
                                             IGxCollection AV64WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                             String AV49WWServicoDS_1_Dynamicfiltersselector1 ,
                                             String AV50WWServicoDS_2_Servico_sigla1 ,
                                             String AV51WWServicoDS_3_Servico_nome1 ,
                                             int AV52WWServicoDS_4_Servicogrupo_codigo1 ,
                                             bool AV53WWServicoDS_5_Dynamicfiltersenabled2 ,
                                             String AV54WWServicoDS_6_Dynamicfiltersselector2 ,
                                             String AV55WWServicoDS_7_Servico_sigla2 ,
                                             String AV56WWServicoDS_8_Servico_nome2 ,
                                             int AV57WWServicoDS_9_Servicogrupo_codigo2 ,
                                             String AV59WWServicoDS_11_Tfservico_nome_sel ,
                                             String AV58WWServicoDS_10_Tfservico_nome ,
                                             String AV61WWServicoDS_13_Tfservico_sigla_sel ,
                                             String AV60WWServicoDS_12_Tfservico_sigla ,
                                             String AV63WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                             String AV62WWServicoDS_14_Tfservicogrupo_descricao ,
                                             int AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ServicoGrupo_Codigo], T1.[Servico_ObjetoControle], T2.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Ativo], T1.[Servico_Nome], T1.[Servico_Sigla], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWServicoDS_2_Servico_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV50WWServicoDS_2_Servico_sigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV50WWServicoDS_2_Servico_sigla1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV51WWServicoDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV51WWServicoDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV52WWServicoDS_4_Servicogrupo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV52WWServicoDS_4_Servicogrupo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV52WWServicoDS_4_Servicogrupo_codigo1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( StringUtil.StrCmp(AV49WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWServicoDS_7_Servico_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV55WWServicoDS_7_Servico_sigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV55WWServicoDS_7_Servico_sigla2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWServicoDS_8_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV56WWServicoDS_8_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV56WWServicoDS_8_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV57WWServicoDS_9_Servicogrupo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV57WWServicoDS_9_Servicogrupo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV57WWServicoDS_9_Servicogrupo_codigo2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV53WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWServicoDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWServicoDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV58WWServicoDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like @lV58WWServicoDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWServicoDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV59WWServicoDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] = @AV59WWServicoDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoDS_13_Tfservico_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWServicoDS_12_Tfservico_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV60WWServicoDS_12_Tfservico_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] like @lV60WWServicoDS_12_Tfservico_sigla)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoDS_13_Tfservico_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV61WWServicoDS_13_Tfservico_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV61WWServicoDS_13_Tfservico_sigla_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWServicoDS_14_Tfservicogrupo_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV62WWServicoDS_14_Tfservicogrupo_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] like @lV62WWServicoDS_14_Tfservicogrupo_descricao)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] = @AV63WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV64WWServicoDS_16_Tfservico_objetocontrole_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ServicoGrupo_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JM2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] );
               case 1 :
                     return conditional_P00JM3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] );
               case 2 :
                     return conditional_P00JM4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JM2 ;
          prmP00JM2 = new Object[] {
          new Object[] {"@AV50WWServicoDS_2_Servico_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV51WWServicoDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV52WWServicoDS_4_Servicogrupo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55WWServicoDS_7_Servico_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWServicoDS_8_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWServicoDS_9_Servicogrupo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV58WWServicoDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWServicoDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWServicoDS_12_Tfservico_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV61WWServicoDS_13_Tfservico_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV62WWServicoDS_14_Tfservicogrupo_descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV63WWServicoDS_15_Tfservicogrupo_descricao_sel",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmP00JM3 ;
          prmP00JM3 = new Object[] {
          new Object[] {"@AV50WWServicoDS_2_Servico_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV51WWServicoDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV52WWServicoDS_4_Servicogrupo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55WWServicoDS_7_Servico_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWServicoDS_8_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWServicoDS_9_Servicogrupo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV58WWServicoDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWServicoDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWServicoDS_12_Tfservico_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV61WWServicoDS_13_Tfservico_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV62WWServicoDS_14_Tfservicogrupo_descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV63WWServicoDS_15_Tfservicogrupo_descricao_sel",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmP00JM4 ;
          prmP00JM4 = new Object[] {
          new Object[] {"@AV50WWServicoDS_2_Servico_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV51WWServicoDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV52WWServicoDS_4_Servicogrupo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55WWServicoDS_7_Servico_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWServicoDS_8_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWServicoDS_9_Servicogrupo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV58WWServicoDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV59WWServicoDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWServicoDS_12_Tfservico_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV61WWServicoDS_13_Tfservico_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV62WWServicoDS_14_Tfservicogrupo_descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV63WWServicoDS_15_Tfservicogrupo_descricao_sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JM2,100,0,true,false )
             ,new CursorDef("P00JM3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JM3,100,0,true,false )
             ,new CursorDef("P00JM4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JM4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwservicofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwservicofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwservicofilterdata") )
          {
             return  ;
          }
          getwwservicofilterdata worker = new getwwservicofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
