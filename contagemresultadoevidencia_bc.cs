/*
               File: ContagemResultadoEvidencia_BC
        Description: Anexar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:19.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoevidencia_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadoevidencia_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoevidencia_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2076( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2076( ) ;
         standaloneModal( ) ;
         AddRow2076( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11202 */
            E11202 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_200( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2076( ) ;
            }
            else
            {
               CheckExtendedTable2076( ) ;
               if ( AnyError == 0 )
               {
                  ZM2076( 10) ;
                  ZM2076( 11) ;
                  ZM2076( 12) ;
               }
               CloseExtendedTableCursors2076( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12202( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            while ( AV16GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemResultado_Codigo") == 0 )
               {
                  AV12Insert_ContagemResultado_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "TipoDocumento_Codigo") == 0 )
               {
                  AV14Insert_TipoDocumento_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
            }
         }
         A588ContagemResultadoEvidencia_Arquivo_Display = 1;
      }

      protected void E11202( )
      {
         /* After Trn Routine */
         if ( false )
         {
         }
      }

      protected void ZM2076( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z591ContagemResultadoEvidencia_Data = A591ContagemResultadoEvidencia_Data;
            Z1393ContagemResultadoEvidencia_RdmnCreated = A1393ContagemResultadoEvidencia_RdmnCreated;
            Z1493ContagemResultadoEvidencia_Owner = A1493ContagemResultadoEvidencia_Owner;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
            Z1494ContagemResultadoEvidencia_Entidade = A1494ContagemResultadoEvidencia_Entidade;
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
            Z1494ContagemResultadoEvidencia_Entidade = A1494ContagemResultadoEvidencia_Entidade;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
            Z1494ContagemResultadoEvidencia_Entidade = A1494ContagemResultadoEvidencia_Entidade;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
            Z1494ContagemResultadoEvidencia_Entidade = A1494ContagemResultadoEvidencia_Entidade;
         }
         if ( GX_JID == -9 )
         {
            Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
            Z1449ContagemResultadoEvidencia_Link = A1449ContagemResultadoEvidencia_Link;
            Z588ContagemResultadoEvidencia_Arquivo = A588ContagemResultadoEvidencia_Arquivo;
            Z591ContagemResultadoEvidencia_Data = A591ContagemResultadoEvidencia_Data;
            Z587ContagemResultadoEvidencia_Descricao = A587ContagemResultadoEvidencia_Descricao;
            Z1393ContagemResultadoEvidencia_RdmnCreated = A1393ContagemResultadoEvidencia_RdmnCreated;
            Z1493ContagemResultadoEvidencia_Owner = A1493ContagemResultadoEvidencia_Owner;
            Z590ContagemResultadoEvidencia_TipoArq = A590ContagemResultadoEvidencia_TipoArq;
            Z589ContagemResultadoEvidencia_NomeArq = A589ContagemResultadoEvidencia_NomeArq;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV15Pgmname = "ContagemResultadoEvidencia_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A591ContagemResultadoEvidencia_Data) && ( Gx_BScreen == 0 ) )
         {
            A591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            n591ContagemResultadoEvidencia_Data = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load2076( )
      {
         /* Using cursor BC00207 */
         pr_default.execute(5, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound76 = 1;
            A490ContagemResultado_ContratadaCod = BC00207_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC00207_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = BC00207_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC00207_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC00207_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC00207_n493ContagemResultado_DemandaFM[0];
            A1449ContagemResultadoEvidencia_Link = BC00207_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = BC00207_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = BC00207_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = BC00207_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = BC00207_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = BC00207_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = BC00207_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = BC00207_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A646TipoDocumento_Nome = BC00207_A646TipoDocumento_Nome[0];
            A1493ContagemResultadoEvidencia_Owner = BC00207_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = BC00207_n1493ContagemResultadoEvidencia_Owner[0];
            A590ContagemResultadoEvidencia_TipoArq = BC00207_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = BC00207_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A589ContagemResultadoEvidencia_NomeArq = BC00207_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = BC00207_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A456ContagemResultado_Codigo = BC00207_A456ContagemResultado_Codigo[0];
            A645TipoDocumento_Codigo = BC00207_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC00207_n645TipoDocumento_Codigo[0];
            A52Contratada_AreaTrabalhoCod = BC00207_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC00207_n52Contratada_AreaTrabalhoCod[0];
            A588ContagemResultadoEvidencia_Arquivo = BC00207_A588ContagemResultadoEvidencia_Arquivo[0];
            n588ContagemResultadoEvidencia_Arquivo = BC00207_n588ContagemResultadoEvidencia_Arquivo[0];
            ZM2076( -9) ;
         }
         pr_default.close(5);
         OnLoadActions2076( ) ;
      }

      protected void OnLoadActions2076( )
      {
         GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
         new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
         A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         }
      }

      protected void CheckExtendedTable2076( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00204 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
         }
         A490ContagemResultado_ContratadaCod = BC00204_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = BC00204_n490ContagemResultado_ContratadaCod[0];
         A457ContagemResultado_Demanda = BC00204_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = BC00204_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = BC00204_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = BC00204_n493ContagemResultado_DemandaFM[0];
         pr_default.close(2);
         /* Using cursor BC00206 */
         pr_default.execute(4, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = BC00206_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = BC00206_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(4);
         GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
         new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
         A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         }
         if ( ! ( (DateTime.MinValue==A591ContagemResultadoEvidencia_Data) || ( A591ContagemResultadoEvidencia_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A1393ContagemResultadoEvidencia_RdmnCreated) || ( A1393ContagemResultadoEvidencia_RdmnCreated >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Anexado no Redmine fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00205 */
         pr_default.execute(3, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = BC00205_A646TipoDocumento_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2076( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2076( )
      {
         /* Using cursor BC00208 */
         pr_default.execute(6, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound76 = 1;
         }
         else
         {
            RcdFound76 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00203 */
         pr_default.execute(1, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2076( 9) ;
            RcdFound76 = 1;
            A586ContagemResultadoEvidencia_Codigo = BC00203_A586ContagemResultadoEvidencia_Codigo[0];
            A1449ContagemResultadoEvidencia_Link = BC00203_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = BC00203_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = BC00203_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = BC00203_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = BC00203_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = BC00203_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = BC00203_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = BC00203_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A1493ContagemResultadoEvidencia_Owner = BC00203_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = BC00203_n1493ContagemResultadoEvidencia_Owner[0];
            A590ContagemResultadoEvidencia_TipoArq = BC00203_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = BC00203_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A589ContagemResultadoEvidencia_NomeArq = BC00203_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = BC00203_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A456ContagemResultado_Codigo = BC00203_A456ContagemResultado_Codigo[0];
            A645TipoDocumento_Codigo = BC00203_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC00203_n645TipoDocumento_Codigo[0];
            A588ContagemResultadoEvidencia_Arquivo = BC00203_A588ContagemResultadoEvidencia_Arquivo[0];
            n588ContagemResultadoEvidencia_Arquivo = BC00203_n588ContagemResultadoEvidencia_Arquivo[0];
            Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
            sMode76 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2076( ) ;
            if ( AnyError == 1 )
            {
               RcdFound76 = 0;
               InitializeNonKey2076( ) ;
            }
            Gx_mode = sMode76;
         }
         else
         {
            RcdFound76 = 0;
            InitializeNonKey2076( ) ;
            sMode76 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode76;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2076( ) ;
         if ( RcdFound76 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_200( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2076( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00202 */
            pr_default.execute(0, new Object[] {A586ContagemResultadoEvidencia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoEvidencia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z591ContagemResultadoEvidencia_Data != BC00202_A591ContagemResultadoEvidencia_Data[0] ) || ( Z1393ContagemResultadoEvidencia_RdmnCreated != BC00202_A1393ContagemResultadoEvidencia_RdmnCreated[0] ) || ( Z1493ContagemResultadoEvidencia_Owner != BC00202_A1493ContagemResultadoEvidencia_Owner[0] ) || ( Z456ContagemResultado_Codigo != BC00202_A456ContagemResultado_Codigo[0] ) || ( Z645TipoDocumento_Codigo != BC00202_A645TipoDocumento_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoEvidencia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2076( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2076( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2076( 0) ;
            CheckOptimisticConcurrency2076( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2076( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2076( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00209 */
                     pr_default.execute(7, new Object[] {n1449ContagemResultadoEvidencia_Link, A1449ContagemResultadoEvidencia_Link, n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, n591ContagemResultadoEvidencia_Data, A591ContagemResultadoEvidencia_Data, n587ContagemResultadoEvidencia_Descricao, A587ContagemResultadoEvidencia_Descricao, n1393ContagemResultadoEvidencia_RdmnCreated, A1393ContagemResultadoEvidencia_RdmnCreated, n1493ContagemResultadoEvidencia_Owner, A1493ContagemResultadoEvidencia_Owner, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A456ContagemResultado_Codigo, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     A586ContagemResultadoEvidencia_Codigo = BC00209_A586ContagemResultadoEvidencia_Codigo[0];
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2076( ) ;
            }
            EndLevel2076( ) ;
         }
         CloseExtendedTableCursors2076( ) ;
      }

      protected void Update2076( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2076( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2076( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2076( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2076( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002010 */
                     pr_default.execute(8, new Object[] {n1449ContagemResultadoEvidencia_Link, A1449ContagemResultadoEvidencia_Link, n591ContagemResultadoEvidencia_Data, A591ContagemResultadoEvidencia_Data, n587ContagemResultadoEvidencia_Descricao, A587ContagemResultadoEvidencia_Descricao, n1393ContagemResultadoEvidencia_RdmnCreated, A1393ContagemResultadoEvidencia_RdmnCreated, n1493ContagemResultadoEvidencia_Owner, A1493ContagemResultadoEvidencia_Owner, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A456ContagemResultado_Codigo, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A586ContagemResultadoEvidencia_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoEvidencia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2076( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2076( ) ;
         }
         CloseExtendedTableCursors2076( ) ;
      }

      protected void DeferredUpdate2076( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC002011 */
            pr_default.execute(9, new Object[] {n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, A586ContagemResultadoEvidencia_Codigo});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2076( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2076( ) ;
            AfterConfirm2076( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2076( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002012 */
                  pr_default.execute(10, new Object[] {A586ContagemResultadoEvidencia_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode76 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2076( ) ;
         Gx_mode = sMode76;
      }

      protected void OnDeleteControls2076( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002013 */
            pr_default.execute(11, new Object[] {A456ContagemResultado_Codigo});
            A490ContagemResultado_ContratadaCod = BC002013_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC002013_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = BC002013_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC002013_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC002013_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC002013_n493ContagemResultado_DemandaFM[0];
            pr_default.close(11);
            /* Using cursor BC002014 */
            pr_default.execute(12, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = BC002014_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC002014_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(12);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            }
            /* Using cursor BC002015 */
            pr_default.execute(13, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = BC002015_A646TipoDocumento_Nome[0];
            pr_default.close(13);
            GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
            new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
            A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC002016 */
            pr_default.execute(14, new Object[] {A586ContagemResultadoEvidencia_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel2076( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2076( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2076( )
      {
         /* Scan By routine */
         /* Using cursor BC002017 */
         pr_default.execute(15, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         RcdFound76 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound76 = 1;
            A490ContagemResultado_ContratadaCod = BC002017_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC002017_n490ContagemResultado_ContratadaCod[0];
            A586ContagemResultadoEvidencia_Codigo = BC002017_A586ContagemResultadoEvidencia_Codigo[0];
            A457ContagemResultado_Demanda = BC002017_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC002017_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC002017_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC002017_n493ContagemResultado_DemandaFM[0];
            A1449ContagemResultadoEvidencia_Link = BC002017_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = BC002017_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = BC002017_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = BC002017_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = BC002017_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = BC002017_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = BC002017_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = BC002017_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A646TipoDocumento_Nome = BC002017_A646TipoDocumento_Nome[0];
            A1493ContagemResultadoEvidencia_Owner = BC002017_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = BC002017_n1493ContagemResultadoEvidencia_Owner[0];
            A590ContagemResultadoEvidencia_TipoArq = BC002017_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = BC002017_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A589ContagemResultadoEvidencia_NomeArq = BC002017_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = BC002017_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A456ContagemResultado_Codigo = BC002017_A456ContagemResultado_Codigo[0];
            A645TipoDocumento_Codigo = BC002017_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002017_n645TipoDocumento_Codigo[0];
            A52Contratada_AreaTrabalhoCod = BC002017_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC002017_n52Contratada_AreaTrabalhoCod[0];
            A588ContagemResultadoEvidencia_Arquivo = BC002017_A588ContagemResultadoEvidencia_Arquivo[0];
            n588ContagemResultadoEvidencia_Arquivo = BC002017_n588ContagemResultadoEvidencia_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2076( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound76 = 0;
         ScanKeyLoad2076( ) ;
      }

      protected void ScanKeyLoad2076( )
      {
         sMode76 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound76 = 1;
            A490ContagemResultado_ContratadaCod = BC002017_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC002017_n490ContagemResultado_ContratadaCod[0];
            A586ContagemResultadoEvidencia_Codigo = BC002017_A586ContagemResultadoEvidencia_Codigo[0];
            A457ContagemResultado_Demanda = BC002017_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC002017_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = BC002017_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC002017_n493ContagemResultado_DemandaFM[0];
            A1449ContagemResultadoEvidencia_Link = BC002017_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = BC002017_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = BC002017_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = BC002017_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = BC002017_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = BC002017_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = BC002017_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = BC002017_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A646TipoDocumento_Nome = BC002017_A646TipoDocumento_Nome[0];
            A1493ContagemResultadoEvidencia_Owner = BC002017_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = BC002017_n1493ContagemResultadoEvidencia_Owner[0];
            A590ContagemResultadoEvidencia_TipoArq = BC002017_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = BC002017_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A589ContagemResultadoEvidencia_NomeArq = BC002017_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = BC002017_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A456ContagemResultado_Codigo = BC002017_A456ContagemResultado_Codigo[0];
            A645TipoDocumento_Codigo = BC002017_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002017_n645TipoDocumento_Codigo[0];
            A52Contratada_AreaTrabalhoCod = BC002017_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC002017_n52Contratada_AreaTrabalhoCod[0];
            A588ContagemResultadoEvidencia_Arquivo = BC002017_A588ContagemResultadoEvidencia_Arquivo[0];
            n588ContagemResultadoEvidencia_Arquivo = BC002017_n588ContagemResultadoEvidencia_Arquivo[0];
         }
         Gx_mode = sMode76;
      }

      protected void ScanKeyEnd2076( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm2076( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2076( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2076( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2076( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2076( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2076( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2076( )
      {
      }

      protected void AddRow2076( )
      {
         VarsToRow76( bcContagemResultadoEvidencia) ;
      }

      protected void ReadRow2076( )
      {
         RowToVars76( bcContagemResultadoEvidencia, 1) ;
      }

      protected void InitializeNonKey2076( )
      {
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         AV7ContagemResultado_Codigo = 0;
         A1494ContagemResultadoEvidencia_Entidade = "";
         A501ContagemResultado_OsFsOsFm = "";
         A456ContagemResultado_Codigo = 0;
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         A52Contratada_AreaTrabalhoCod = 0;
         n52Contratada_AreaTrabalhoCod = false;
         A1449ContagemResultadoEvidencia_Link = "";
         n1449ContagemResultadoEvidencia_Link = false;
         A588ContagemResultadoEvidencia_Arquivo = "";
         n588ContagemResultadoEvidencia_Arquivo = false;
         A587ContagemResultadoEvidencia_Descricao = "";
         n587ContagemResultadoEvidencia_Descricao = false;
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         n1393ContagemResultadoEvidencia_RdmnCreated = false;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         A1493ContagemResultadoEvidencia_Owner = 0;
         n1493ContagemResultadoEvidencia_Owner = false;
         A590ContagemResultadoEvidencia_TipoArq = "";
         n590ContagemResultadoEvidencia_TipoArq = false;
         A589ContagemResultadoEvidencia_NomeArq = "";
         n589ContagemResultadoEvidencia_NomeArq = false;
         A591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         Z591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         Z1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         Z1493ContagemResultadoEvidencia_Owner = 0;
         Z456ContagemResultado_Codigo = 0;
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll2076( )
      {
         A586ContagemResultadoEvidencia_Codigo = 0;
         InitializeNonKey2076( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A591ContagemResultadoEvidencia_Data = i591ContagemResultadoEvidencia_Data;
         n591ContagemResultadoEvidencia_Data = false;
      }

      public void VarsToRow76( SdtContagemResultadoEvidencia obj76 )
      {
         obj76.gxTpr_Mode = Gx_mode;
         obj76.gxTpr_Contagemresultadoevidencia_entidade = A1494ContagemResultadoEvidencia_Entidade;
         obj76.gxTpr_Contagemresultado_osfsosfm = A501ContagemResultado_OsFsOsFm;
         obj76.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         obj76.gxTpr_Contagemresultado_demanda = A457ContagemResultado_Demanda;
         obj76.gxTpr_Contagemresultado_demandafm = A493ContagemResultado_DemandaFM;
         obj76.gxTpr_Contratada_areatrabalhocod = A52Contratada_AreaTrabalhoCod;
         obj76.gxTpr_Contagemresultadoevidencia_link = A1449ContagemResultadoEvidencia_Link;
         obj76.gxTpr_Contagemresultadoevidencia_arquivo = A588ContagemResultadoEvidencia_Arquivo;
         obj76.gxTpr_Contagemresultadoevidencia_descricao = A587ContagemResultadoEvidencia_Descricao;
         obj76.gxTpr_Contagemresultadoevidencia_rdmncreated = A1393ContagemResultadoEvidencia_RdmnCreated;
         obj76.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
         obj76.gxTpr_Tipodocumento_nome = A646TipoDocumento_Nome;
         obj76.gxTpr_Contagemresultadoevidencia_owner = A1493ContagemResultadoEvidencia_Owner;
         obj76.gxTpr_Contagemresultadoevidencia_tipoarq = A590ContagemResultadoEvidencia_TipoArq;
         obj76.gxTpr_Contagemresultadoevidencia_nomearq = A589ContagemResultadoEvidencia_NomeArq;
         obj76.gxTpr_Contagemresultadoevidencia_data = A591ContagemResultadoEvidencia_Data;
         obj76.gxTpr_Contagemresultadoevidencia_codigo = A586ContagemResultadoEvidencia_Codigo;
         obj76.gxTpr_Contagemresultadoevidencia_codigo_Z = Z586ContagemResultadoEvidencia_Codigo;
         obj76.gxTpr_Contagemresultado_codigo_Z = Z456ContagemResultado_Codigo;
         obj76.gxTpr_Contagemresultado_demanda_Z = Z457ContagemResultado_Demanda;
         obj76.gxTpr_Contagemresultado_demandafm_Z = Z493ContagemResultado_DemandaFM;
         obj76.gxTpr_Contratada_areatrabalhocod_Z = Z52Contratada_AreaTrabalhoCod;
         obj76.gxTpr_Contagemresultado_osfsosfm_Z = Z501ContagemResultado_OsFsOsFm;
         obj76.gxTpr_Contagemresultadoevidencia_nomearq_Z = Z589ContagemResultadoEvidencia_NomeArq;
         obj76.gxTpr_Contagemresultadoevidencia_tipoarq_Z = Z590ContagemResultadoEvidencia_TipoArq;
         obj76.gxTpr_Contagemresultadoevidencia_data_Z = Z591ContagemResultadoEvidencia_Data;
         obj76.gxTpr_Contagemresultadoevidencia_rdmncreated_Z = Z1393ContagemResultadoEvidencia_RdmnCreated;
         obj76.gxTpr_Tipodocumento_codigo_Z = Z645TipoDocumento_Codigo;
         obj76.gxTpr_Tipodocumento_nome_Z = Z646TipoDocumento_Nome;
         obj76.gxTpr_Contagemresultadoevidencia_owner_Z = Z1493ContagemResultadoEvidencia_Owner;
         obj76.gxTpr_Contagemresultadoevidencia_entidade_Z = Z1494ContagemResultadoEvidencia_Entidade;
         obj76.gxTpr_Contagemresultado_demanda_N = (short)(Convert.ToInt16(n457ContagemResultado_Demanda));
         obj76.gxTpr_Contagemresultado_demandafm_N = (short)(Convert.ToInt16(n493ContagemResultado_DemandaFM));
         obj76.gxTpr_Contratada_areatrabalhocod_N = (short)(Convert.ToInt16(n52Contratada_AreaTrabalhoCod));
         obj76.gxTpr_Contagemresultadoevidencia_link_N = (short)(Convert.ToInt16(n1449ContagemResultadoEvidencia_Link));
         obj76.gxTpr_Contagemresultadoevidencia_arquivo_N = (short)(Convert.ToInt16(n588ContagemResultadoEvidencia_Arquivo));
         obj76.gxTpr_Contagemresultadoevidencia_nomearq_N = (short)(Convert.ToInt16(n589ContagemResultadoEvidencia_NomeArq));
         obj76.gxTpr_Contagemresultadoevidencia_tipoarq_N = (short)(Convert.ToInt16(n590ContagemResultadoEvidencia_TipoArq));
         obj76.gxTpr_Contagemresultadoevidencia_data_N = (short)(Convert.ToInt16(n591ContagemResultadoEvidencia_Data));
         obj76.gxTpr_Contagemresultadoevidencia_descricao_N = (short)(Convert.ToInt16(n587ContagemResultadoEvidencia_Descricao));
         obj76.gxTpr_Contagemresultadoevidencia_rdmncreated_N = (short)(Convert.ToInt16(n1393ContagemResultadoEvidencia_RdmnCreated));
         obj76.gxTpr_Tipodocumento_codigo_N = (short)(Convert.ToInt16(n645TipoDocumento_Codigo));
         obj76.gxTpr_Contagemresultadoevidencia_owner_N = (short)(Convert.ToInt16(n1493ContagemResultadoEvidencia_Owner));
         obj76.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow76( SdtContagemResultadoEvidencia obj76 )
      {
         obj76.gxTpr_Contagemresultadoevidencia_codigo = A586ContagemResultadoEvidencia_Codigo;
         return  ;
      }

      public void RowToVars76( SdtContagemResultadoEvidencia obj76 ,
                               int forceLoad )
      {
         Gx_mode = obj76.gxTpr_Mode;
         A1494ContagemResultadoEvidencia_Entidade = obj76.gxTpr_Contagemresultadoevidencia_entidade;
         A501ContagemResultado_OsFsOsFm = obj76.gxTpr_Contagemresultado_osfsosfm;
         A456ContagemResultado_Codigo = obj76.gxTpr_Contagemresultado_codigo;
         A457ContagemResultado_Demanda = obj76.gxTpr_Contagemresultado_demanda;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = obj76.gxTpr_Contagemresultado_demandafm;
         n493ContagemResultado_DemandaFM = false;
         A52Contratada_AreaTrabalhoCod = obj76.gxTpr_Contratada_areatrabalhocod;
         n52Contratada_AreaTrabalhoCod = false;
         A1449ContagemResultadoEvidencia_Link = obj76.gxTpr_Contagemresultadoevidencia_link;
         n1449ContagemResultadoEvidencia_Link = false;
         A588ContagemResultadoEvidencia_Arquivo = obj76.gxTpr_Contagemresultadoevidencia_arquivo;
         n588ContagemResultadoEvidencia_Arquivo = false;
         A587ContagemResultadoEvidencia_Descricao = obj76.gxTpr_Contagemresultadoevidencia_descricao;
         n587ContagemResultadoEvidencia_Descricao = false;
         A1393ContagemResultadoEvidencia_RdmnCreated = obj76.gxTpr_Contagemresultadoevidencia_rdmncreated;
         n1393ContagemResultadoEvidencia_RdmnCreated = false;
         A645TipoDocumento_Codigo = obj76.gxTpr_Tipodocumento_codigo;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = obj76.gxTpr_Tipodocumento_nome;
         A1493ContagemResultadoEvidencia_Owner = obj76.gxTpr_Contagemresultadoevidencia_owner;
         n1493ContagemResultadoEvidencia_Owner = false;
         A590ContagemResultadoEvidencia_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj76.gxTpr_Contagemresultadoevidencia_tipoarq)) ? FileUtil.GetFileType( A588ContagemResultadoEvidencia_Arquivo) : obj76.gxTpr_Contagemresultadoevidencia_tipoarq);
         n590ContagemResultadoEvidencia_TipoArq = false;
         A589ContagemResultadoEvidencia_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj76.gxTpr_Contagemresultadoevidencia_nomearq)) ? FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo) : obj76.gxTpr_Contagemresultadoevidencia_nomearq);
         n589ContagemResultadoEvidencia_NomeArq = false;
         A591ContagemResultadoEvidencia_Data = obj76.gxTpr_Contagemresultadoevidencia_data;
         n591ContagemResultadoEvidencia_Data = false;
         A586ContagemResultadoEvidencia_Codigo = obj76.gxTpr_Contagemresultadoevidencia_codigo;
         Z586ContagemResultadoEvidencia_Codigo = obj76.gxTpr_Contagemresultadoevidencia_codigo_Z;
         Z456ContagemResultado_Codigo = obj76.gxTpr_Contagemresultado_codigo_Z;
         Z457ContagemResultado_Demanda = obj76.gxTpr_Contagemresultado_demanda_Z;
         Z493ContagemResultado_DemandaFM = obj76.gxTpr_Contagemresultado_demandafm_Z;
         Z52Contratada_AreaTrabalhoCod = obj76.gxTpr_Contratada_areatrabalhocod_Z;
         Z501ContagemResultado_OsFsOsFm = obj76.gxTpr_Contagemresultado_osfsosfm_Z;
         Z589ContagemResultadoEvidencia_NomeArq = obj76.gxTpr_Contagemresultadoevidencia_nomearq_Z;
         Z590ContagemResultadoEvidencia_TipoArq = obj76.gxTpr_Contagemresultadoevidencia_tipoarq_Z;
         Z591ContagemResultadoEvidencia_Data = obj76.gxTpr_Contagemresultadoevidencia_data_Z;
         Z1393ContagemResultadoEvidencia_RdmnCreated = obj76.gxTpr_Contagemresultadoevidencia_rdmncreated_Z;
         Z645TipoDocumento_Codigo = obj76.gxTpr_Tipodocumento_codigo_Z;
         Z646TipoDocumento_Nome = obj76.gxTpr_Tipodocumento_nome_Z;
         Z1493ContagemResultadoEvidencia_Owner = obj76.gxTpr_Contagemresultadoevidencia_owner_Z;
         Z1494ContagemResultadoEvidencia_Entidade = obj76.gxTpr_Contagemresultadoevidencia_entidade_Z;
         n457ContagemResultado_Demanda = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultado_demanda_N));
         n493ContagemResultado_DemandaFM = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultado_demandafm_N));
         n52Contratada_AreaTrabalhoCod = (bool)(Convert.ToBoolean(obj76.gxTpr_Contratada_areatrabalhocod_N));
         n1449ContagemResultadoEvidencia_Link = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_link_N));
         n588ContagemResultadoEvidencia_Arquivo = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_arquivo_N));
         n589ContagemResultadoEvidencia_NomeArq = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_nomearq_N));
         n590ContagemResultadoEvidencia_TipoArq = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_tipoarq_N));
         n591ContagemResultadoEvidencia_Data = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_data_N));
         n587ContagemResultadoEvidencia_Descricao = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_descricao_N));
         n1393ContagemResultadoEvidencia_RdmnCreated = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_rdmncreated_N));
         n645TipoDocumento_Codigo = (bool)(Convert.ToBoolean(obj76.gxTpr_Tipodocumento_codigo_N));
         n1493ContagemResultadoEvidencia_Owner = (bool)(Convert.ToBoolean(obj76.gxTpr_Contagemresultadoevidencia_owner_N));
         Gx_mode = obj76.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A586ContagemResultadoEvidencia_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2076( ) ;
         ScanKeyStart2076( ) ;
         if ( RcdFound76 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
         }
         ZM2076( -9) ;
         OnLoadActions2076( ) ;
         AddRow2076( ) ;
         ScanKeyEnd2076( ) ;
         if ( RcdFound76 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars76( bcContagemResultadoEvidencia, 0) ;
         ScanKeyStart2076( ) ;
         if ( RcdFound76 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
         }
         ZM2076( -9) ;
         OnLoadActions2076( ) ;
         AddRow2076( ) ;
         ScanKeyEnd2076( ) ;
         if ( RcdFound76 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars76( bcContagemResultadoEvidencia, 0) ;
         nKeyPressed = 1;
         GetKey2076( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2076( ) ;
         }
         else
         {
            if ( RcdFound76 == 1 )
            {
               if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
               {
                  A586ContagemResultadoEvidencia_Codigo = Z586ContagemResultadoEvidencia_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2076( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2076( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2076( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow76( bcContagemResultadoEvidencia) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars76( bcContagemResultadoEvidencia, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2076( ) ;
         if ( RcdFound76 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
            {
               A586ContagemResultadoEvidencia_Codigo = Z586ContagemResultadoEvidencia_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(13);
         pr_default.close(12);
         context.RollbackDataStores( "ContagemResultadoEvidencia_BC");
         VarsToRow76( bcContagemResultadoEvidencia) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoEvidencia.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoEvidencia.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoEvidencia )
         {
            bcContagemResultadoEvidencia = (SdtContagemResultadoEvidencia)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoEvidencia.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoEvidencia.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow76( bcContagemResultadoEvidencia) ;
            }
            else
            {
               RowToVars76( bcContagemResultadoEvidencia, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoEvidencia.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoEvidencia.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars76( bcContagemResultadoEvidencia, 1) ;
         return  ;
      }

      public SdtContagemResultadoEvidencia ContagemResultadoEvidencia_BC
      {
         get {
            return bcContagemResultadoEvidencia ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(13);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV15Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         Z1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         Z501ContagemResultado_OsFsOsFm = "";
         A501ContagemResultado_OsFsOsFm = "";
         Z1494ContagemResultadoEvidencia_Entidade = "";
         A1494ContagemResultadoEvidencia_Entidade = "";
         Z457ContagemResultado_Demanda = "";
         A457ContagemResultado_Demanda = "";
         Z493ContagemResultado_DemandaFM = "";
         A493ContagemResultado_DemandaFM = "";
         Z646TipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         Z1449ContagemResultadoEvidencia_Link = "";
         A1449ContagemResultadoEvidencia_Link = "";
         Z588ContagemResultadoEvidencia_Arquivo = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         Z587ContagemResultadoEvidencia_Descricao = "";
         A587ContagemResultadoEvidencia_Descricao = "";
         Z590ContagemResultadoEvidencia_TipoArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         Z589ContagemResultadoEvidencia_NomeArq = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         BC00207_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC00207_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC00207_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC00207_A457ContagemResultado_Demanda = new String[] {""} ;
         BC00207_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC00207_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC00207_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC00207_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         BC00207_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         BC00207_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         BC00207_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         BC00207_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         BC00207_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         BC00207_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         BC00207_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         BC00207_A646TipoDocumento_Nome = new String[] {""} ;
         BC00207_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         BC00207_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         BC00207_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         BC00207_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         BC00207_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         BC00207_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         BC00207_A456ContagemResultado_Codigo = new int[1] ;
         BC00207_A645TipoDocumento_Codigo = new int[1] ;
         BC00207_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00207_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC00207_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC00207_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         BC00207_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A588ContagemResultadoEvidencia_Arquivo_Filetype = "";
         A588ContagemResultadoEvidencia_Arquivo_Filename = "";
         BC00204_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC00204_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC00204_A457ContagemResultado_Demanda = new String[] {""} ;
         BC00204_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC00204_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC00204_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC00206_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC00206_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC00205_A646TipoDocumento_Nome = new String[] {""} ;
         BC00208_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC00203_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC00203_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         BC00203_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         BC00203_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         BC00203_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         BC00203_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         BC00203_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         BC00203_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         BC00203_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         BC00203_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         BC00203_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         BC00203_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         BC00203_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         BC00203_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         BC00203_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         BC00203_A456ContagemResultado_Codigo = new int[1] ;
         BC00203_A645TipoDocumento_Codigo = new int[1] ;
         BC00203_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00203_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         BC00203_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         sMode76 = "";
         BC00202_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC00202_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         BC00202_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         BC00202_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         BC00202_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         BC00202_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         BC00202_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         BC00202_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         BC00202_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         BC00202_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         BC00202_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         BC00202_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         BC00202_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         BC00202_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         BC00202_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         BC00202_A456ContagemResultado_Codigo = new int[1] ;
         BC00202_A645TipoDocumento_Codigo = new int[1] ;
         BC00202_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00202_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         BC00202_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         BC00209_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC002013_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC002013_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC002013_A457ContagemResultado_Demanda = new String[] {""} ;
         BC002013_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC002013_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC002013_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC002014_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC002014_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC002015_A646TipoDocumento_Nome = new String[] {""} ;
         GXt_char1 = "";
         BC002016_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC002017_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC002017_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC002017_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC002017_A457ContagemResultado_Demanda = new String[] {""} ;
         BC002017_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC002017_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC002017_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC002017_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         BC002017_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         BC002017_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         BC002017_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         BC002017_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         BC002017_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         BC002017_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         BC002017_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         BC002017_A646TipoDocumento_Nome = new String[] {""} ;
         BC002017_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         BC002017_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         BC002017_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         BC002017_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         BC002017_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         BC002017_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         BC002017_A456ContagemResultado_Codigo = new int[1] ;
         BC002017_A645TipoDocumento_Codigo = new int[1] ;
         BC002017_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002017_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC002017_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC002017_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         BC002017_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         i591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoevidencia_bc__default(),
            new Object[][] {
                new Object[] {
               BC00202_A586ContagemResultadoEvidencia_Codigo, BC00202_A1449ContagemResultadoEvidencia_Link, BC00202_n1449ContagemResultadoEvidencia_Link, BC00202_A591ContagemResultadoEvidencia_Data, BC00202_n591ContagemResultadoEvidencia_Data, BC00202_A587ContagemResultadoEvidencia_Descricao, BC00202_n587ContagemResultadoEvidencia_Descricao, BC00202_A1393ContagemResultadoEvidencia_RdmnCreated, BC00202_n1393ContagemResultadoEvidencia_RdmnCreated, BC00202_A1493ContagemResultadoEvidencia_Owner,
               BC00202_n1493ContagemResultadoEvidencia_Owner, BC00202_A590ContagemResultadoEvidencia_TipoArq, BC00202_n590ContagemResultadoEvidencia_TipoArq, BC00202_A589ContagemResultadoEvidencia_NomeArq, BC00202_n589ContagemResultadoEvidencia_NomeArq, BC00202_A456ContagemResultado_Codigo, BC00202_A645TipoDocumento_Codigo, BC00202_n645TipoDocumento_Codigo, BC00202_A588ContagemResultadoEvidencia_Arquivo, BC00202_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               BC00203_A586ContagemResultadoEvidencia_Codigo, BC00203_A1449ContagemResultadoEvidencia_Link, BC00203_n1449ContagemResultadoEvidencia_Link, BC00203_A591ContagemResultadoEvidencia_Data, BC00203_n591ContagemResultadoEvidencia_Data, BC00203_A587ContagemResultadoEvidencia_Descricao, BC00203_n587ContagemResultadoEvidencia_Descricao, BC00203_A1393ContagemResultadoEvidencia_RdmnCreated, BC00203_n1393ContagemResultadoEvidencia_RdmnCreated, BC00203_A1493ContagemResultadoEvidencia_Owner,
               BC00203_n1493ContagemResultadoEvidencia_Owner, BC00203_A590ContagemResultadoEvidencia_TipoArq, BC00203_n590ContagemResultadoEvidencia_TipoArq, BC00203_A589ContagemResultadoEvidencia_NomeArq, BC00203_n589ContagemResultadoEvidencia_NomeArq, BC00203_A456ContagemResultado_Codigo, BC00203_A645TipoDocumento_Codigo, BC00203_n645TipoDocumento_Codigo, BC00203_A588ContagemResultadoEvidencia_Arquivo, BC00203_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               BC00204_A490ContagemResultado_ContratadaCod, BC00204_n490ContagemResultado_ContratadaCod, BC00204_A457ContagemResultado_Demanda, BC00204_n457ContagemResultado_Demanda, BC00204_A493ContagemResultado_DemandaFM, BC00204_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               BC00205_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC00206_A52Contratada_AreaTrabalhoCod, BC00206_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               BC00207_A490ContagemResultado_ContratadaCod, BC00207_n490ContagemResultado_ContratadaCod, BC00207_A586ContagemResultadoEvidencia_Codigo, BC00207_A457ContagemResultado_Demanda, BC00207_n457ContagemResultado_Demanda, BC00207_A493ContagemResultado_DemandaFM, BC00207_n493ContagemResultado_DemandaFM, BC00207_A1449ContagemResultadoEvidencia_Link, BC00207_n1449ContagemResultadoEvidencia_Link, BC00207_A591ContagemResultadoEvidencia_Data,
               BC00207_n591ContagemResultadoEvidencia_Data, BC00207_A587ContagemResultadoEvidencia_Descricao, BC00207_n587ContagemResultadoEvidencia_Descricao, BC00207_A1393ContagemResultadoEvidencia_RdmnCreated, BC00207_n1393ContagemResultadoEvidencia_RdmnCreated, BC00207_A646TipoDocumento_Nome, BC00207_A1493ContagemResultadoEvidencia_Owner, BC00207_n1493ContagemResultadoEvidencia_Owner, BC00207_A590ContagemResultadoEvidencia_TipoArq, BC00207_n590ContagemResultadoEvidencia_TipoArq,
               BC00207_A589ContagemResultadoEvidencia_NomeArq, BC00207_n589ContagemResultadoEvidencia_NomeArq, BC00207_A456ContagemResultado_Codigo, BC00207_A645TipoDocumento_Codigo, BC00207_n645TipoDocumento_Codigo, BC00207_A52Contratada_AreaTrabalhoCod, BC00207_n52Contratada_AreaTrabalhoCod, BC00207_A588ContagemResultadoEvidencia_Arquivo, BC00207_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               BC00208_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               BC00209_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002013_A490ContagemResultado_ContratadaCod, BC002013_n490ContagemResultado_ContratadaCod, BC002013_A457ContagemResultado_Demanda, BC002013_n457ContagemResultado_Demanda, BC002013_A493ContagemResultado_DemandaFM, BC002013_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               BC002014_A52Contratada_AreaTrabalhoCod, BC002014_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               BC002015_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC002016_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               BC002017_A490ContagemResultado_ContratadaCod, BC002017_n490ContagemResultado_ContratadaCod, BC002017_A586ContagemResultadoEvidencia_Codigo, BC002017_A457ContagemResultado_Demanda, BC002017_n457ContagemResultado_Demanda, BC002017_A493ContagemResultado_DemandaFM, BC002017_n493ContagemResultado_DemandaFM, BC002017_A1449ContagemResultadoEvidencia_Link, BC002017_n1449ContagemResultadoEvidencia_Link, BC002017_A591ContagemResultadoEvidencia_Data,
               BC002017_n591ContagemResultadoEvidencia_Data, BC002017_A587ContagemResultadoEvidencia_Descricao, BC002017_n587ContagemResultadoEvidencia_Descricao, BC002017_A1393ContagemResultadoEvidencia_RdmnCreated, BC002017_n1393ContagemResultadoEvidencia_RdmnCreated, BC002017_A646TipoDocumento_Nome, BC002017_A1493ContagemResultadoEvidencia_Owner, BC002017_n1493ContagemResultadoEvidencia_Owner, BC002017_A590ContagemResultadoEvidencia_TipoArq, BC002017_n590ContagemResultadoEvidencia_TipoArq,
               BC002017_A589ContagemResultadoEvidencia_NomeArq, BC002017_n589ContagemResultadoEvidencia_NomeArq, BC002017_A456ContagemResultado_Codigo, BC002017_A645TipoDocumento_Codigo, BC002017_n645TipoDocumento_Codigo, BC002017_A52Contratada_AreaTrabalhoCod, BC002017_n52Contratada_AreaTrabalhoCod, BC002017_A588ContagemResultadoEvidencia_Arquivo, BC002017_n588ContagemResultadoEvidencia_Arquivo
               }
            }
         );
         Z591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         A591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         i591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         AV15Pgmname = "ContagemResultadoEvidencia_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12202 */
         E12202 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short A588ContagemResultadoEvidencia_Arquivo_Display ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound76 ;
      private int trnEnded ;
      private int Z586ContagemResultadoEvidencia_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int AV16GXV1 ;
      private int AV12Insert_ContagemResultado_Codigo ;
      private int AV14Insert_TipoDocumento_Codigo ;
      private int Z1493ContagemResultadoEvidencia_Owner ;
      private int A1493ContagemResultadoEvidencia_Owner ;
      private int Z456ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV7ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV15Pgmname ;
      private String Z1494ContagemResultadoEvidencia_Entidade ;
      private String A1494ContagemResultadoEvidencia_Entidade ;
      private String Z646TipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private String Z590ContagemResultadoEvidencia_TipoArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String Z589ContagemResultadoEvidencia_NomeArq ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filetype ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filename ;
      private String sMode76 ;
      private String GXt_char1 ;
      private DateTime Z591ContagemResultadoEvidencia_Data ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime Z1393ContagemResultadoEvidencia_RdmnCreated ;
      private DateTime A1393ContagemResultadoEvidencia_RdmnCreated ;
      private DateTime i591ContagemResultadoEvidencia_Data ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1449ContagemResultadoEvidencia_Link ;
      private bool n587ContagemResultadoEvidencia_Descricao ;
      private bool n1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool n1493ContagemResultadoEvidencia_Owner ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n645TipoDocumento_Codigo ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private String Z1449ContagemResultadoEvidencia_Link ;
      private String A1449ContagemResultadoEvidencia_Link ;
      private String Z587ContagemResultadoEvidencia_Descricao ;
      private String A587ContagemResultadoEvidencia_Descricao ;
      private String Z501ContagemResultado_OsFsOsFm ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String Z457ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private String A493ContagemResultado_DemandaFM ;
      private String Z588ContagemResultadoEvidencia_Arquivo ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private IGxSession AV11WebSession ;
      private SdtContagemResultadoEvidencia bcContagemResultadoEvidencia ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00207_A490ContagemResultado_ContratadaCod ;
      private bool[] BC00207_n490ContagemResultado_ContratadaCod ;
      private int[] BC00207_A586ContagemResultadoEvidencia_Codigo ;
      private String[] BC00207_A457ContagemResultado_Demanda ;
      private bool[] BC00207_n457ContagemResultado_Demanda ;
      private String[] BC00207_A493ContagemResultado_DemandaFM ;
      private bool[] BC00207_n493ContagemResultado_DemandaFM ;
      private String[] BC00207_A1449ContagemResultadoEvidencia_Link ;
      private bool[] BC00207_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] BC00207_A591ContagemResultadoEvidencia_Data ;
      private bool[] BC00207_n591ContagemResultadoEvidencia_Data ;
      private String[] BC00207_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] BC00207_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] BC00207_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] BC00207_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private String[] BC00207_A646TipoDocumento_Nome ;
      private int[] BC00207_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] BC00207_n1493ContagemResultadoEvidencia_Owner ;
      private String[] BC00207_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] BC00207_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] BC00207_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] BC00207_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] BC00207_A456ContagemResultado_Codigo ;
      private int[] BC00207_A645TipoDocumento_Codigo ;
      private bool[] BC00207_n645TipoDocumento_Codigo ;
      private int[] BC00207_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC00207_n52Contratada_AreaTrabalhoCod ;
      private String[] BC00207_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] BC00207_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] BC00204_A490ContagemResultado_ContratadaCod ;
      private bool[] BC00204_n490ContagemResultado_ContratadaCod ;
      private String[] BC00204_A457ContagemResultado_Demanda ;
      private bool[] BC00204_n457ContagemResultado_Demanda ;
      private String[] BC00204_A493ContagemResultado_DemandaFM ;
      private bool[] BC00204_n493ContagemResultado_DemandaFM ;
      private int[] BC00206_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC00206_n52Contratada_AreaTrabalhoCod ;
      private String[] BC00205_A646TipoDocumento_Nome ;
      private int[] BC00208_A586ContagemResultadoEvidencia_Codigo ;
      private int[] BC00203_A586ContagemResultadoEvidencia_Codigo ;
      private String[] BC00203_A1449ContagemResultadoEvidencia_Link ;
      private bool[] BC00203_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] BC00203_A591ContagemResultadoEvidencia_Data ;
      private bool[] BC00203_n591ContagemResultadoEvidencia_Data ;
      private String[] BC00203_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] BC00203_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] BC00203_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] BC00203_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private int[] BC00203_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] BC00203_n1493ContagemResultadoEvidencia_Owner ;
      private String[] BC00203_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] BC00203_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] BC00203_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] BC00203_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] BC00203_A456ContagemResultado_Codigo ;
      private int[] BC00203_A645TipoDocumento_Codigo ;
      private bool[] BC00203_n645TipoDocumento_Codigo ;
      private String[] BC00203_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] BC00203_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] BC00202_A586ContagemResultadoEvidencia_Codigo ;
      private String[] BC00202_A1449ContagemResultadoEvidencia_Link ;
      private bool[] BC00202_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] BC00202_A591ContagemResultadoEvidencia_Data ;
      private bool[] BC00202_n591ContagemResultadoEvidencia_Data ;
      private String[] BC00202_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] BC00202_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] BC00202_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] BC00202_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private int[] BC00202_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] BC00202_n1493ContagemResultadoEvidencia_Owner ;
      private String[] BC00202_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] BC00202_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] BC00202_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] BC00202_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] BC00202_A456ContagemResultado_Codigo ;
      private int[] BC00202_A645TipoDocumento_Codigo ;
      private bool[] BC00202_n645TipoDocumento_Codigo ;
      private String[] BC00202_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] BC00202_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] BC00209_A586ContagemResultadoEvidencia_Codigo ;
      private int[] BC002013_A490ContagemResultado_ContratadaCod ;
      private bool[] BC002013_n490ContagemResultado_ContratadaCod ;
      private String[] BC002013_A457ContagemResultado_Demanda ;
      private bool[] BC002013_n457ContagemResultado_Demanda ;
      private String[] BC002013_A493ContagemResultado_DemandaFM ;
      private bool[] BC002013_n493ContagemResultado_DemandaFM ;
      private int[] BC002014_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC002014_n52Contratada_AreaTrabalhoCod ;
      private String[] BC002015_A646TipoDocumento_Nome ;
      private int[] BC002016_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC002017_A490ContagemResultado_ContratadaCod ;
      private bool[] BC002017_n490ContagemResultado_ContratadaCod ;
      private int[] BC002017_A586ContagemResultadoEvidencia_Codigo ;
      private String[] BC002017_A457ContagemResultado_Demanda ;
      private bool[] BC002017_n457ContagemResultado_Demanda ;
      private String[] BC002017_A493ContagemResultado_DemandaFM ;
      private bool[] BC002017_n493ContagemResultado_DemandaFM ;
      private String[] BC002017_A1449ContagemResultadoEvidencia_Link ;
      private bool[] BC002017_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] BC002017_A591ContagemResultadoEvidencia_Data ;
      private bool[] BC002017_n591ContagemResultadoEvidencia_Data ;
      private String[] BC002017_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] BC002017_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] BC002017_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] BC002017_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private String[] BC002017_A646TipoDocumento_Nome ;
      private int[] BC002017_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] BC002017_n1493ContagemResultadoEvidencia_Owner ;
      private String[] BC002017_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] BC002017_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] BC002017_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] BC002017_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] BC002017_A456ContagemResultado_Codigo ;
      private int[] BC002017_A645TipoDocumento_Codigo ;
      private bool[] BC002017_n645TipoDocumento_Codigo ;
      private int[] BC002017_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC002017_n52Contratada_AreaTrabalhoCod ;
      private String[] BC002017_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] BC002017_n588ContagemResultadoEvidencia_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class contagemresultadoevidencia_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00207 ;
          prmBC00207 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00204 ;
          prmBC00204 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00206 ;
          prmBC00206 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00205 ;
          prmBC00205 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00208 ;
          prmBC00208 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00203 ;
          prmBC00203 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00202 ;
          prmBC00202 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00209 ;
          prmBC00209 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoEvidencia_RdmnCreated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002010 ;
          prmBC002010 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoEvidencia_RdmnCreated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002011 ;
          prmBC002011 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002012 ;
          prmBC002012 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002013 ;
          prmBC002013 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002014 ;
          prmBC002014 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002015 ;
          prmBC002015 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002016 ;
          prmBC002016 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002017 ;
          prmBC002017 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00202", "SELECT [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Owner], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_NomeArq], [ContagemResultado_Codigo], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (UPDLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00202,1,0,true,false )
             ,new CursorDef("BC00203", "SELECT [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Owner], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_NomeArq], [ContagemResultado_Codigo], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00203,1,0,true,false )
             ,new CursorDef("BC00204", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00204,1,0,true,false )
             ,new CursorDef("BC00205", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00205,1,0,true,false )
             ,new CursorDef("BC00206", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00206,1,0,true,false )
             ,new CursorDef("BC00207", "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[ContagemResultadoEvidencia_Codigo], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultadoEvidencia_Link], TM1.[ContagemResultadoEvidencia_Data], TM1.[ContagemResultadoEvidencia_Descricao], TM1.[ContagemResultadoEvidencia_RdmnCreated], T4.[TipoDocumento_Nome], TM1.[ContagemResultadoEvidencia_Owner], TM1.[ContagemResultadoEvidencia_TipoArq], TM1.[ContagemResultadoEvidencia_NomeArq], TM1.[ContagemResultado_Codigo], TM1.[TipoDocumento_Codigo], T3.[Contratada_AreaTrabalhoCod], TM1.[ContagemResultadoEvidencia_Arquivo] FROM ((([ContagemResultadoEvidencia] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [TipoDocumento] T4 WITH (NOLOCK) ON T4.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ORDER BY TM1.[ContagemResultadoEvidencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00207,100,0,true,false )
             ,new CursorDef("BC00208", "SELECT [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00208,1,0,true,false )
             ,new CursorDef("BC00209", "INSERT INTO [ContagemResultadoEvidencia]([ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Arquivo], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Owner], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_NomeArq], [ContagemResultado_Codigo], [TipoDocumento_Codigo]) VALUES(@ContagemResultadoEvidencia_Link, @ContagemResultadoEvidencia_Arquivo, @ContagemResultadoEvidencia_Data, @ContagemResultadoEvidencia_Descricao, @ContagemResultadoEvidencia_RdmnCreated, @ContagemResultadoEvidencia_Owner, @ContagemResultadoEvidencia_TipoArq, @ContagemResultadoEvidencia_NomeArq, @ContagemResultado_Codigo, @TipoDocumento_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00209)
             ,new CursorDef("BC002010", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_Link]=@ContagemResultadoEvidencia_Link, [ContagemResultadoEvidencia_Data]=@ContagemResultadoEvidencia_Data, [ContagemResultadoEvidencia_Descricao]=@ContagemResultadoEvidencia_Descricao, [ContagemResultadoEvidencia_RdmnCreated]=@ContagemResultadoEvidencia_RdmnCreated, [ContagemResultadoEvidencia_Owner]=@ContagemResultadoEvidencia_Owner, [ContagemResultadoEvidencia_TipoArq]=@ContagemResultadoEvidencia_TipoArq, [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq, [ContagemResultado_Codigo]=@ContagemResultado_Codigo, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmBC002010)
             ,new CursorDef("BC002011", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_Arquivo]=@ContagemResultadoEvidencia_Arquivo  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmBC002011)
             ,new CursorDef("BC002012", "DELETE FROM [ContagemResultadoEvidencia]  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmBC002012)
             ,new CursorDef("BC002013", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002013,1,0,true,false )
             ,new CursorDef("BC002014", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002014,1,0,true,false )
             ,new CursorDef("BC002015", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002015,1,0,true,false )
             ,new CursorDef("BC002016", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_EvdCod] = @ContagemResultadoEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002016,1,0,true,true )
             ,new CursorDef("BC002017", "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[ContagemResultadoEvidencia_Codigo], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultadoEvidencia_Link], TM1.[ContagemResultadoEvidencia_Data], TM1.[ContagemResultadoEvidencia_Descricao], TM1.[ContagemResultadoEvidencia_RdmnCreated], T4.[TipoDocumento_Nome], TM1.[ContagemResultadoEvidencia_Owner], TM1.[ContagemResultadoEvidencia_TipoArq], TM1.[ContagemResultadoEvidencia_NomeArq], TM1.[ContagemResultado_Codigo], TM1.[TipoDocumento_Codigo], T3.[Contratada_AreaTrabalhoCod], TM1.[ContagemResultadoEvidencia_Arquivo] FROM ((([ContagemResultadoEvidencia] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [TipoDocumento] T4 WITH (NOLOCK) ON T4.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ORDER BY TM1.[ContagemResultadoEvidencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002017,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(11, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(11, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getBLOBFile(16, rslt.getString(11, 10), rslt.getString(12, 50)) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getBLOBFile(16, rslt.getString(11, 10), rslt.getString(12, 50)) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                stmt.SetParameter(10, (int)parms[17]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
