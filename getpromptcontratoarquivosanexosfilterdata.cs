/*
               File: GetPromptContratoArquivosAnexosFilterData
        Description: Get Prompt Contrato Arquivos Anexos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:12.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoarquivosanexosfilterdata : GXProcedure
   {
      public getpromptcontratoarquivosanexosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoarquivosanexosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
         return AV39OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoarquivosanexosfilterdata objgetpromptcontratoarquivosanexosfilterdata;
         objgetpromptcontratoarquivosanexosfilterdata = new getpromptcontratoarquivosanexosfilterdata();
         objgetpromptcontratoarquivosanexosfilterdata.AV30DDOName = aP0_DDOName;
         objgetpromptcontratoarquivosanexosfilterdata.AV28SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoarquivosanexosfilterdata.AV29SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoarquivosanexosfilterdata.AV34OptionsJson = "" ;
         objgetpromptcontratoarquivosanexosfilterdata.AV37OptionsDescJson = "" ;
         objgetpromptcontratoarquivosanexosfilterdata.AV39OptionIndexesJson = "" ;
         objgetpromptcontratoarquivosanexosfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoarquivosanexosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoarquivosanexosfilterdata);
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoarquivosanexosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV33Options = (IGxCollection)(new GxSimpleCollection());
         AV36OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV38OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOARQUIVOSANEXOS_DESCRICAOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV34OptionsJson = AV33Options.ToJSonString(false);
         AV37OptionsDescJson = AV36OptionsDesc.ToJSonString(false);
         AV39OptionIndexesJson = AV38OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV41Session.Get("PromptContratoArquivosAnexosGridState"), "") == 0 )
         {
            AV43GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoArquivosAnexosGridState"), "");
         }
         else
         {
            AV43GridState.FromXml(AV41Session.Get("PromptContratoArquivosAnexosGridState"), "");
         }
         AV59GXV1 = 1;
         while ( AV59GXV1 <= AV43GridState.gxTpr_Filtervalues.Count )
         {
            AV44GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV43GridState.gxTpr_Filtervalues.Item(AV59GXV1));
            if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_CODIGO") == 0 )
            {
               AV10TFContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoArquivosAnexos_Codigo_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV12TFContrato_Codigo = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContrato_Codigo_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV14TFContrato_Numero = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV15TFContrato_Numero_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_CODIGO") == 0 )
            {
               AV16TFContratada_Codigo = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratada_Codigo_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACOD") == 0 )
            {
               AV18TFContratada_PessoaCod = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratada_PessoaCod_To = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV20TFContratada_PessoaNom = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV21TFContratada_PessoaNom_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV22TFContratada_PessoaCNPJ = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV23TFContratada_PessoaCNPJ_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV24TFContratoArquivosAnexos_Descricao = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL") == 0 )
            {
               AV25TFContratoArquivosAnexos_Descricao_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DATA") == 0 )
            {
               AV26TFContratoArquivosAnexos_Data = context.localUtil.CToT( AV44GridStateFilterValue.gxTpr_Value, 2);
               AV27TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( AV44GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV59GXV1 = (int)(AV59GXV1+1);
         }
         if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(1));
            AV46DynamicFiltersSelector1 = AV45GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV47DynamicFiltersOperator1 = AV45GridStateDynamicFilter.gxTpr_Operator;
               AV48ContratoArquivosAnexos_Descricao1 = AV45GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV49DynamicFiltersEnabled2 = true;
               AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(2));
               AV50DynamicFiltersSelector2 = AV45GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
               {
                  AV51DynamicFiltersOperator2 = AV45GridStateDynamicFilter.gxTpr_Operator;
                  AV52ContratoArquivosAnexos_Descricao2 = AV45GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV53DynamicFiltersEnabled3 = true;
                  AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(3));
                  AV54DynamicFiltersSelector3 = AV45GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
                  {
                     AV55DynamicFiltersOperator3 = AV45GridStateDynamicFilter.gxTpr_Operator;
                     AV56ContratoArquivosAnexos_Descricao3 = AV45GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV14TFContrato_Numero = AV28SearchTxt;
         AV15TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV46DynamicFiltersSelector1 ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV48ContratoArquivosAnexos_Descricao1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51DynamicFiltersOperator2 ,
                                              AV52ContratoArquivosAnexos_Descricao2 ,
                                              AV53DynamicFiltersEnabled3 ,
                                              AV54DynamicFiltersSelector3 ,
                                              AV55DynamicFiltersOperator3 ,
                                              AV56ContratoArquivosAnexos_Descricao3 ,
                                              AV10TFContratoArquivosAnexos_Codigo ,
                                              AV11TFContratoArquivosAnexos_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                              AV24TFContratoArquivosAnexos_Descricao ,
                                              AV26TFContratoArquivosAnexos_Data ,
                                              AV27TFContratoArquivosAnexos_Data_To ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A108ContratoArquivosAnexos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A113ContratoArquivosAnexos_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         lV24TFContratoArquivosAnexos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao), "%", "");
         /* Using cursor P00UJ2 */
         pr_default.execute(0, new Object[] {lV48ContratoArquivosAnexos_Descricao1, lV48ContratoArquivosAnexos_Descricao1, lV52ContratoArquivosAnexos_Descricao2, lV52ContratoArquivosAnexos_Descricao2, lV56ContratoArquivosAnexos_Descricao3, lV56ContratoArquivosAnexos_Descricao3, AV10TFContratoArquivosAnexos_Codigo, AV11TFContratoArquivosAnexos_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, lV24TFContratoArquivosAnexos_Descricao, AV25TFContratoArquivosAnexos_Descricao_Sel, AV26TFContratoArquivosAnexos_Data, AV27TFContratoArquivosAnexos_Data_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUJ2 = false;
            A77Contrato_Numero = P00UJ2_A77Contrato_Numero[0];
            A113ContratoArquivosAnexos_Data = P00UJ2_A113ContratoArquivosAnexos_Data[0];
            A42Contratada_PessoaCNPJ = P00UJ2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UJ2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ2_n41Contratada_PessoaNom[0];
            A40Contratada_PessoaCod = P00UJ2_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UJ2_A39Contratada_Codigo[0];
            A74Contrato_Codigo = P00UJ2_A74Contrato_Codigo[0];
            A108ContratoArquivosAnexos_Codigo = P00UJ2_A108ContratoArquivosAnexos_Codigo[0];
            A110ContratoArquivosAnexos_Descricao = P00UJ2_A110ContratoArquivosAnexos_Descricao[0];
            A77Contrato_Numero = P00UJ2_A77Contrato_Numero[0];
            A39Contratada_Codigo = P00UJ2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00UJ2_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UJ2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UJ2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ2_n41Contratada_PessoaNom[0];
            AV40count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UJ2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKUJ2 = false;
               A74Contrato_Codigo = P00UJ2_A74Contrato_Codigo[0];
               A108ContratoArquivosAnexos_Codigo = P00UJ2_A108ContratoArquivosAnexos_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKUJ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV32Option = A77Contrato_Numero;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUJ2 )
            {
               BRKUJ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV20TFContratada_PessoaNom = AV28SearchTxt;
         AV21TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV46DynamicFiltersSelector1 ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV48ContratoArquivosAnexos_Descricao1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51DynamicFiltersOperator2 ,
                                              AV52ContratoArquivosAnexos_Descricao2 ,
                                              AV53DynamicFiltersEnabled3 ,
                                              AV54DynamicFiltersSelector3 ,
                                              AV55DynamicFiltersOperator3 ,
                                              AV56ContratoArquivosAnexos_Descricao3 ,
                                              AV10TFContratoArquivosAnexos_Codigo ,
                                              AV11TFContratoArquivosAnexos_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                              AV24TFContratoArquivosAnexos_Descricao ,
                                              AV26TFContratoArquivosAnexos_Data ,
                                              AV27TFContratoArquivosAnexos_Data_To ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A108ContratoArquivosAnexos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A113ContratoArquivosAnexos_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         lV24TFContratoArquivosAnexos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao), "%", "");
         /* Using cursor P00UJ3 */
         pr_default.execute(1, new Object[] {lV48ContratoArquivosAnexos_Descricao1, lV48ContratoArquivosAnexos_Descricao1, lV52ContratoArquivosAnexos_Descricao2, lV52ContratoArquivosAnexos_Descricao2, lV56ContratoArquivosAnexos_Descricao3, lV56ContratoArquivosAnexos_Descricao3, AV10TFContratoArquivosAnexos_Codigo, AV11TFContratoArquivosAnexos_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, lV24TFContratoArquivosAnexos_Descricao, AV25TFContratoArquivosAnexos_Descricao_Sel, AV26TFContratoArquivosAnexos_Data, AV27TFContratoArquivosAnexos_Data_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUJ4 = false;
            A41Contratada_PessoaNom = P00UJ3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ3_n41Contratada_PessoaNom[0];
            A113ContratoArquivosAnexos_Data = P00UJ3_A113ContratoArquivosAnexos_Data[0];
            A42Contratada_PessoaCNPJ = P00UJ3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ3_n42Contratada_PessoaCNPJ[0];
            A40Contratada_PessoaCod = P00UJ3_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UJ3_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UJ3_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UJ3_A74Contrato_Codigo[0];
            A108ContratoArquivosAnexos_Codigo = P00UJ3_A108ContratoArquivosAnexos_Codigo[0];
            A110ContratoArquivosAnexos_Descricao = P00UJ3_A110ContratoArquivosAnexos_Descricao[0];
            A39Contratada_Codigo = P00UJ3_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UJ3_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00UJ3_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00UJ3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ3_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00UJ3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ3_n42Contratada_PessoaCNPJ[0];
            AV40count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UJ3_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKUJ4 = false;
               A40Contratada_PessoaCod = P00UJ3_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00UJ3_A39Contratada_Codigo[0];
               A74Contrato_Codigo = P00UJ3_A74Contrato_Codigo[0];
               A108ContratoArquivosAnexos_Codigo = P00UJ3_A108ContratoArquivosAnexos_Codigo[0];
               A39Contratada_Codigo = P00UJ3_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00UJ3_A40Contratada_PessoaCod[0];
               AV40count = (long)(AV40count+1);
               BRKUJ4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV32Option = A41Contratada_PessoaNom;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUJ4 )
            {
               BRKUJ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV22TFContratada_PessoaCNPJ = AV28SearchTxt;
         AV23TFContratada_PessoaCNPJ_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV46DynamicFiltersSelector1 ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV48ContratoArquivosAnexos_Descricao1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51DynamicFiltersOperator2 ,
                                              AV52ContratoArquivosAnexos_Descricao2 ,
                                              AV53DynamicFiltersEnabled3 ,
                                              AV54DynamicFiltersSelector3 ,
                                              AV55DynamicFiltersOperator3 ,
                                              AV56ContratoArquivosAnexos_Descricao3 ,
                                              AV10TFContratoArquivosAnexos_Codigo ,
                                              AV11TFContratoArquivosAnexos_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                              AV24TFContratoArquivosAnexos_Descricao ,
                                              AV26TFContratoArquivosAnexos_Data ,
                                              AV27TFContratoArquivosAnexos_Data_To ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A108ContratoArquivosAnexos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A113ContratoArquivosAnexos_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         lV24TFContratoArquivosAnexos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao), "%", "");
         /* Using cursor P00UJ4 */
         pr_default.execute(2, new Object[] {lV48ContratoArquivosAnexos_Descricao1, lV48ContratoArquivosAnexos_Descricao1, lV52ContratoArquivosAnexos_Descricao2, lV52ContratoArquivosAnexos_Descricao2, lV56ContratoArquivosAnexos_Descricao3, lV56ContratoArquivosAnexos_Descricao3, AV10TFContratoArquivosAnexos_Codigo, AV11TFContratoArquivosAnexos_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, lV24TFContratoArquivosAnexos_Descricao, AV25TFContratoArquivosAnexos_Descricao_Sel, AV26TFContratoArquivosAnexos_Data, AV27TFContratoArquivosAnexos_Data_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUJ6 = false;
            A42Contratada_PessoaCNPJ = P00UJ4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ4_n42Contratada_PessoaCNPJ[0];
            A113ContratoArquivosAnexos_Data = P00UJ4_A113ContratoArquivosAnexos_Data[0];
            A41Contratada_PessoaNom = P00UJ4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ4_n41Contratada_PessoaNom[0];
            A40Contratada_PessoaCod = P00UJ4_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UJ4_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UJ4_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UJ4_A74Contrato_Codigo[0];
            A108ContratoArquivosAnexos_Codigo = P00UJ4_A108ContratoArquivosAnexos_Codigo[0];
            A110ContratoArquivosAnexos_Descricao = P00UJ4_A110ContratoArquivosAnexos_Descricao[0];
            A39Contratada_Codigo = P00UJ4_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UJ4_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00UJ4_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UJ4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ4_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UJ4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ4_n41Contratada_PessoaNom[0];
            AV40count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00UJ4_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKUJ6 = false;
               A40Contratada_PessoaCod = P00UJ4_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00UJ4_A39Contratada_Codigo[0];
               A74Contrato_Codigo = P00UJ4_A74Contrato_Codigo[0];
               A108ContratoArquivosAnexos_Codigo = P00UJ4_A108ContratoArquivosAnexos_Codigo[0];
               A39Contratada_Codigo = P00UJ4_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00UJ4_A40Contratada_PessoaCod[0];
               AV40count = (long)(AV40count+1);
               BRKUJ6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV32Option = A42Contratada_PessoaCNPJ;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUJ6 )
            {
               BRKUJ6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOARQUIVOSANEXOS_DESCRICAOOPTIONS' Routine */
         AV24TFContratoArquivosAnexos_Descricao = AV28SearchTxt;
         AV25TFContratoArquivosAnexos_Descricao_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV46DynamicFiltersSelector1 ,
                                              AV47DynamicFiltersOperator1 ,
                                              AV48ContratoArquivosAnexos_Descricao1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51DynamicFiltersOperator2 ,
                                              AV52ContratoArquivosAnexos_Descricao2 ,
                                              AV53DynamicFiltersEnabled3 ,
                                              AV54DynamicFiltersSelector3 ,
                                              AV55DynamicFiltersOperator3 ,
                                              AV56ContratoArquivosAnexos_Descricao3 ,
                                              AV10TFContratoArquivosAnexos_Codigo ,
                                              AV11TFContratoArquivosAnexos_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                              AV24TFContratoArquivosAnexos_Descricao ,
                                              AV26TFContratoArquivosAnexos_Data ,
                                              AV27TFContratoArquivosAnexos_Data_To ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A108ContratoArquivosAnexos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A113ContratoArquivosAnexos_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV48ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV52ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV56ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         lV24TFContratoArquivosAnexos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao), "%", "");
         /* Using cursor P00UJ5 */
         pr_default.execute(3, new Object[] {lV48ContratoArquivosAnexos_Descricao1, lV48ContratoArquivosAnexos_Descricao1, lV52ContratoArquivosAnexos_Descricao2, lV52ContratoArquivosAnexos_Descricao2, lV56ContratoArquivosAnexos_Descricao3, lV56ContratoArquivosAnexos_Descricao3, AV10TFContratoArquivosAnexos_Codigo, AV11TFContratoArquivosAnexos_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, lV24TFContratoArquivosAnexos_Descricao, AV25TFContratoArquivosAnexos_Descricao_Sel, AV26TFContratoArquivosAnexos_Data, AV27TFContratoArquivosAnexos_Data_To});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKUJ8 = false;
            A110ContratoArquivosAnexos_Descricao = P00UJ5_A110ContratoArquivosAnexos_Descricao[0];
            A113ContratoArquivosAnexos_Data = P00UJ5_A113ContratoArquivosAnexos_Data[0];
            A42Contratada_PessoaCNPJ = P00UJ5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ5_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UJ5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ5_n41Contratada_PessoaNom[0];
            A40Contratada_PessoaCod = P00UJ5_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UJ5_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UJ5_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UJ5_A74Contrato_Codigo[0];
            A108ContratoArquivosAnexos_Codigo = P00UJ5_A108ContratoArquivosAnexos_Codigo[0];
            A39Contratada_Codigo = P00UJ5_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UJ5_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00UJ5_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UJ5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UJ5_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UJ5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UJ5_n41Contratada_PessoaNom[0];
            AV40count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00UJ5_A110ContratoArquivosAnexos_Descricao[0], A110ContratoArquivosAnexos_Descricao) == 0 ) )
            {
               BRKUJ8 = false;
               A108ContratoArquivosAnexos_Codigo = P00UJ5_A108ContratoArquivosAnexos_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKUJ8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A110ContratoArquivosAnexos_Descricao)) )
            {
               AV32Option = A110ContratoArquivosAnexos_Descricao;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUJ8 )
            {
               BRKUJ8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV33Options = new GxSimpleCollection();
         AV36OptionsDesc = new GxSimpleCollection();
         AV38OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV41Session = context.GetSession();
         AV43GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV44GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContrato_Numero = "";
         AV15TFContrato_Numero_Sel = "";
         AV20TFContratada_PessoaNom = "";
         AV21TFContratada_PessoaNom_Sel = "";
         AV22TFContratada_PessoaCNPJ = "";
         AV23TFContratada_PessoaCNPJ_Sel = "";
         AV24TFContratoArquivosAnexos_Descricao = "";
         AV25TFContratoArquivosAnexos_Descricao_Sel = "";
         AV26TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         AV27TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         AV45GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV46DynamicFiltersSelector1 = "";
         AV48ContratoArquivosAnexos_Descricao1 = "";
         AV50DynamicFiltersSelector2 = "";
         AV52ContratoArquivosAnexos_Descricao2 = "";
         AV54DynamicFiltersSelector3 = "";
         AV56ContratoArquivosAnexos_Descricao3 = "";
         scmdbuf = "";
         lV48ContratoArquivosAnexos_Descricao1 = "";
         lV52ContratoArquivosAnexos_Descricao2 = "";
         lV56ContratoArquivosAnexos_Descricao3 = "";
         lV14TFContrato_Numero = "";
         lV20TFContratada_PessoaNom = "";
         lV22TFContratada_PessoaCNPJ = "";
         lV24TFContratoArquivosAnexos_Descricao = "";
         A110ContratoArquivosAnexos_Descricao = "";
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         P00UJ2_A77Contrato_Numero = new String[] {""} ;
         P00UJ2_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00UJ2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UJ2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UJ2_A41Contratada_PessoaNom = new String[] {""} ;
         P00UJ2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UJ2_A40Contratada_PessoaCod = new int[1] ;
         P00UJ2_A39Contratada_Codigo = new int[1] ;
         P00UJ2_A74Contrato_Codigo = new int[1] ;
         P00UJ2_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00UJ2_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         AV32Option = "";
         P00UJ3_A41Contratada_PessoaNom = new String[] {""} ;
         P00UJ3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UJ3_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00UJ3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UJ3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UJ3_A40Contratada_PessoaCod = new int[1] ;
         P00UJ3_A39Contratada_Codigo = new int[1] ;
         P00UJ3_A77Contrato_Numero = new String[] {""} ;
         P00UJ3_A74Contrato_Codigo = new int[1] ;
         P00UJ3_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00UJ3_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00UJ4_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UJ4_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UJ4_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00UJ4_A41Contratada_PessoaNom = new String[] {""} ;
         P00UJ4_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UJ4_A40Contratada_PessoaCod = new int[1] ;
         P00UJ4_A39Contratada_Codigo = new int[1] ;
         P00UJ4_A77Contrato_Numero = new String[] {""} ;
         P00UJ4_A74Contrato_Codigo = new int[1] ;
         P00UJ4_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P00UJ4_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00UJ5_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         P00UJ5_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00UJ5_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UJ5_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UJ5_A41Contratada_PessoaNom = new String[] {""} ;
         P00UJ5_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UJ5_A40Contratada_PessoaCod = new int[1] ;
         P00UJ5_A39Contratada_Codigo = new int[1] ;
         P00UJ5_A77Contrato_Numero = new String[] {""} ;
         P00UJ5_A74Contrato_Codigo = new int[1] ;
         P00UJ5_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoarquivosanexosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UJ2_A77Contrato_Numero, P00UJ2_A113ContratoArquivosAnexos_Data, P00UJ2_A42Contratada_PessoaCNPJ, P00UJ2_n42Contratada_PessoaCNPJ, P00UJ2_A41Contratada_PessoaNom, P00UJ2_n41Contratada_PessoaNom, P00UJ2_A40Contratada_PessoaCod, P00UJ2_A39Contratada_Codigo, P00UJ2_A74Contrato_Codigo, P00UJ2_A108ContratoArquivosAnexos_Codigo,
               P00UJ2_A110ContratoArquivosAnexos_Descricao
               }
               , new Object[] {
               P00UJ3_A41Contratada_PessoaNom, P00UJ3_n41Contratada_PessoaNom, P00UJ3_A113ContratoArquivosAnexos_Data, P00UJ3_A42Contratada_PessoaCNPJ, P00UJ3_n42Contratada_PessoaCNPJ, P00UJ3_A40Contratada_PessoaCod, P00UJ3_A39Contratada_Codigo, P00UJ3_A77Contrato_Numero, P00UJ3_A74Contrato_Codigo, P00UJ3_A108ContratoArquivosAnexos_Codigo,
               P00UJ3_A110ContratoArquivosAnexos_Descricao
               }
               , new Object[] {
               P00UJ4_A42Contratada_PessoaCNPJ, P00UJ4_n42Contratada_PessoaCNPJ, P00UJ4_A113ContratoArquivosAnexos_Data, P00UJ4_A41Contratada_PessoaNom, P00UJ4_n41Contratada_PessoaNom, P00UJ4_A40Contratada_PessoaCod, P00UJ4_A39Contratada_Codigo, P00UJ4_A77Contrato_Numero, P00UJ4_A74Contrato_Codigo, P00UJ4_A108ContratoArquivosAnexos_Codigo,
               P00UJ4_A110ContratoArquivosAnexos_Descricao
               }
               , new Object[] {
               P00UJ5_A110ContratoArquivosAnexos_Descricao, P00UJ5_A113ContratoArquivosAnexos_Data, P00UJ5_A42Contratada_PessoaCNPJ, P00UJ5_n42Contratada_PessoaCNPJ, P00UJ5_A41Contratada_PessoaNom, P00UJ5_n41Contratada_PessoaNom, P00UJ5_A40Contratada_PessoaCod, P00UJ5_A39Contratada_Codigo, P00UJ5_A77Contrato_Numero, P00UJ5_A74Contrato_Codigo,
               P00UJ5_A108ContratoArquivosAnexos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV47DynamicFiltersOperator1 ;
      private short AV51DynamicFiltersOperator2 ;
      private short AV55DynamicFiltersOperator3 ;
      private int AV59GXV1 ;
      private int AV10TFContratoArquivosAnexos_Codigo ;
      private int AV11TFContratoArquivosAnexos_Codigo_To ;
      private int AV12TFContrato_Codigo ;
      private int AV13TFContrato_Codigo_To ;
      private int AV16TFContratada_Codigo ;
      private int AV17TFContratada_Codigo_To ;
      private int AV18TFContratada_PessoaCod ;
      private int AV19TFContratada_PessoaCod_To ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private long AV40count ;
      private String AV14TFContrato_Numero ;
      private String AV15TFContrato_Numero_Sel ;
      private String AV20TFContratada_PessoaNom ;
      private String AV21TFContratada_PessoaNom_Sel ;
      private String scmdbuf ;
      private String lV14TFContrato_Numero ;
      private String lV20TFContratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private String A41Contratada_PessoaNom ;
      private DateTime AV26TFContratoArquivosAnexos_Data ;
      private DateTime AV27TFContratoArquivosAnexos_Data_To ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private bool returnInSub ;
      private bool AV49DynamicFiltersEnabled2 ;
      private bool AV53DynamicFiltersEnabled3 ;
      private bool BRKUJ2 ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool BRKUJ4 ;
      private bool BRKUJ6 ;
      private bool BRKUJ8 ;
      private String AV39OptionIndexesJson ;
      private String AV34OptionsJson ;
      private String AV37OptionsDescJson ;
      private String AV48ContratoArquivosAnexos_Descricao1 ;
      private String AV52ContratoArquivosAnexos_Descricao2 ;
      private String AV56ContratoArquivosAnexos_Descricao3 ;
      private String lV48ContratoArquivosAnexos_Descricao1 ;
      private String lV52ContratoArquivosAnexos_Descricao2 ;
      private String lV56ContratoArquivosAnexos_Descricao3 ;
      private String A110ContratoArquivosAnexos_Descricao ;
      private String AV30DDOName ;
      private String AV28SearchTxt ;
      private String AV29SearchTxtTo ;
      private String AV22TFContratada_PessoaCNPJ ;
      private String AV23TFContratada_PessoaCNPJ_Sel ;
      private String AV24TFContratoArquivosAnexos_Descricao ;
      private String AV25TFContratoArquivosAnexos_Descricao_Sel ;
      private String AV46DynamicFiltersSelector1 ;
      private String AV50DynamicFiltersSelector2 ;
      private String AV54DynamicFiltersSelector3 ;
      private String lV22TFContratada_PessoaCNPJ ;
      private String lV24TFContratoArquivosAnexos_Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV32Option ;
      private IGxSession AV41Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00UJ2_A77Contrato_Numero ;
      private DateTime[] P00UJ2_A113ContratoArquivosAnexos_Data ;
      private String[] P00UJ2_A42Contratada_PessoaCNPJ ;
      private bool[] P00UJ2_n42Contratada_PessoaCNPJ ;
      private String[] P00UJ2_A41Contratada_PessoaNom ;
      private bool[] P00UJ2_n41Contratada_PessoaNom ;
      private int[] P00UJ2_A40Contratada_PessoaCod ;
      private int[] P00UJ2_A39Contratada_Codigo ;
      private int[] P00UJ2_A74Contrato_Codigo ;
      private int[] P00UJ2_A108ContratoArquivosAnexos_Codigo ;
      private String[] P00UJ2_A110ContratoArquivosAnexos_Descricao ;
      private String[] P00UJ3_A41Contratada_PessoaNom ;
      private bool[] P00UJ3_n41Contratada_PessoaNom ;
      private DateTime[] P00UJ3_A113ContratoArquivosAnexos_Data ;
      private String[] P00UJ3_A42Contratada_PessoaCNPJ ;
      private bool[] P00UJ3_n42Contratada_PessoaCNPJ ;
      private int[] P00UJ3_A40Contratada_PessoaCod ;
      private int[] P00UJ3_A39Contratada_Codigo ;
      private String[] P00UJ3_A77Contrato_Numero ;
      private int[] P00UJ3_A74Contrato_Codigo ;
      private int[] P00UJ3_A108ContratoArquivosAnexos_Codigo ;
      private String[] P00UJ3_A110ContratoArquivosAnexos_Descricao ;
      private String[] P00UJ4_A42Contratada_PessoaCNPJ ;
      private bool[] P00UJ4_n42Contratada_PessoaCNPJ ;
      private DateTime[] P00UJ4_A113ContratoArquivosAnexos_Data ;
      private String[] P00UJ4_A41Contratada_PessoaNom ;
      private bool[] P00UJ4_n41Contratada_PessoaNom ;
      private int[] P00UJ4_A40Contratada_PessoaCod ;
      private int[] P00UJ4_A39Contratada_Codigo ;
      private String[] P00UJ4_A77Contrato_Numero ;
      private int[] P00UJ4_A74Contrato_Codigo ;
      private int[] P00UJ4_A108ContratoArquivosAnexos_Codigo ;
      private String[] P00UJ4_A110ContratoArquivosAnexos_Descricao ;
      private String[] P00UJ5_A110ContratoArquivosAnexos_Descricao ;
      private DateTime[] P00UJ5_A113ContratoArquivosAnexos_Data ;
      private String[] P00UJ5_A42Contratada_PessoaCNPJ ;
      private bool[] P00UJ5_n42Contratada_PessoaCNPJ ;
      private String[] P00UJ5_A41Contratada_PessoaNom ;
      private bool[] P00UJ5_n41Contratada_PessoaNom ;
      private int[] P00UJ5_A40Contratada_PessoaCod ;
      private int[] P00UJ5_A39Contratada_Codigo ;
      private String[] P00UJ5_A77Contrato_Numero ;
      private int[] P00UJ5_A74Contrato_Codigo ;
      private int[] P00UJ5_A108ContratoArquivosAnexos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV43GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV44GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV45GridStateDynamicFilter ;
   }

   public class getpromptcontratoarquivosanexosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UJ2( IGxContext context ,
                                             String AV46DynamicFiltersSelector1 ,
                                             short AV47DynamicFiltersOperator1 ,
                                             String AV48ContratoArquivosAnexos_Descricao1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             short AV51DynamicFiltersOperator2 ,
                                             String AV52ContratoArquivosAnexos_Descricao2 ,
                                             bool AV53DynamicFiltersEnabled3 ,
                                             String AV54DynamicFiltersSelector3 ,
                                             short AV55DynamicFiltersOperator3 ,
                                             String AV56ContratoArquivosAnexos_Descricao3 ,
                                             int AV10TFContratoArquivosAnexos_Codigo ,
                                             int AV11TFContratoArquivosAnexos_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             String AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                             String AV24TFContratoArquivosAnexos_Descricao ,
                                             DateTime AV26TFContratoArquivosAnexos_Data ,
                                             DateTime AV27TFContratoArquivosAnexos_Data_To ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             int A108ContratoArquivosAnexos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             DateTime A113ContratoArquivosAnexos_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Numero], T1.[ContratoArquivosAnexos_Data], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T1.[Contrato_Codigo], T1.[ContratoArquivosAnexos_Codigo], T1.[ContratoArquivosAnexos_Descricao] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFContratoArquivosAnexos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFContratoArquivosAnexos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoArquivosAnexos_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoArquivosAnexos_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UJ3( IGxContext context ,
                                             String AV46DynamicFiltersSelector1 ,
                                             short AV47DynamicFiltersOperator1 ,
                                             String AV48ContratoArquivosAnexos_Descricao1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             short AV51DynamicFiltersOperator2 ,
                                             String AV52ContratoArquivosAnexos_Descricao2 ,
                                             bool AV53DynamicFiltersEnabled3 ,
                                             String AV54DynamicFiltersSelector3 ,
                                             short AV55DynamicFiltersOperator3 ,
                                             String AV56ContratoArquivosAnexos_Descricao3 ,
                                             int AV10TFContratoArquivosAnexos_Codigo ,
                                             int AV11TFContratoArquivosAnexos_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             String AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                             String AV24TFContratoArquivosAnexos_Descricao ,
                                             DateTime AV26TFContratoArquivosAnexos_Data ,
                                             DateTime AV27TFContratoArquivosAnexos_Data_To ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             int A108ContratoArquivosAnexos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             DateTime A113ContratoArquivosAnexos_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoArquivosAnexos_Data], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoArquivosAnexos_Codigo], T1.[ContratoArquivosAnexos_Descricao] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFContratoArquivosAnexos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFContratoArquivosAnexos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoArquivosAnexos_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoArquivosAnexos_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UJ4( IGxContext context ,
                                             String AV46DynamicFiltersSelector1 ,
                                             short AV47DynamicFiltersOperator1 ,
                                             String AV48ContratoArquivosAnexos_Descricao1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             short AV51DynamicFiltersOperator2 ,
                                             String AV52ContratoArquivosAnexos_Descricao2 ,
                                             bool AV53DynamicFiltersEnabled3 ,
                                             String AV54DynamicFiltersSelector3 ,
                                             short AV55DynamicFiltersOperator3 ,
                                             String AV56ContratoArquivosAnexos_Descricao3 ,
                                             int AV10TFContratoArquivosAnexos_Codigo ,
                                             int AV11TFContratoArquivosAnexos_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             String AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                             String AV24TFContratoArquivosAnexos_Descricao ,
                                             DateTime AV26TFContratoArquivosAnexos_Data ,
                                             DateTime AV27TFContratoArquivosAnexos_Data_To ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             int A108ContratoArquivosAnexos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             DateTime A113ContratoArquivosAnexos_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[ContratoArquivosAnexos_Data], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoArquivosAnexos_Codigo], T1.[ContratoArquivosAnexos_Descricao] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV10TFContratoArquivosAnexos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV11TFContratoArquivosAnexos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoArquivosAnexos_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoArquivosAnexos_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00UJ5( IGxContext context ,
                                             String AV46DynamicFiltersSelector1 ,
                                             short AV47DynamicFiltersOperator1 ,
                                             String AV48ContratoArquivosAnexos_Descricao1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             short AV51DynamicFiltersOperator2 ,
                                             String AV52ContratoArquivosAnexos_Descricao2 ,
                                             bool AV53DynamicFiltersEnabled3 ,
                                             String AV54DynamicFiltersSelector3 ,
                                             short AV55DynamicFiltersOperator3 ,
                                             String AV56ContratoArquivosAnexos_Descricao3 ,
                                             int AV10TFContratoArquivosAnexos_Codigo ,
                                             int AV11TFContratoArquivosAnexos_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             String AV25TFContratoArquivosAnexos_Descricao_Sel ,
                                             String AV24TFContratoArquivosAnexos_Descricao ,
                                             DateTime AV26TFContratoArquivosAnexos_Data ,
                                             DateTime AV27TFContratoArquivosAnexos_Data_To ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             int A108ContratoArquivosAnexos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             DateTime A113ContratoArquivosAnexos_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [24] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoArquivosAnexos_Descricao], T1.[ContratoArquivosAnexos_Data], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoArquivosAnexos_Codigo] FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV47DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV48ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV51DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV52ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV53DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV55DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV56ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ! (0==AV10TFContratoArquivosAnexos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] >= @AV10TFContratoArquivosAnexos_Codigo)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( ! (0==AV11TFContratoArquivosAnexos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] <= @AV11TFContratoArquivosAnexos_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoArquivosAnexos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV24TFContratoArquivosAnexos_Descricao)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV25TFContratoArquivosAnexos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoArquivosAnexos_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV26TFContratoArquivosAnexos_Data)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoArquivosAnexos_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV27TFContratoArquivosAnexos_Data_To)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoArquivosAnexos_Descricao]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UJ2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] );
               case 1 :
                     return conditional_P00UJ3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] );
               case 2 :
                     return conditional_P00UJ4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] );
               case 3 :
                     return conditional_P00UJ5(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UJ2 ;
          prmP00UJ2 = new Object[] {
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoArquivosAnexos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV24TFContratoArquivosAnexos_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoArquivosAnexos_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV27TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00UJ3 ;
          prmP00UJ3 = new Object[] {
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoArquivosAnexos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV24TFContratoArquivosAnexos_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoArquivosAnexos_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV27TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00UJ4 ;
          prmP00UJ4 = new Object[] {
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoArquivosAnexos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV24TFContratoArquivosAnexos_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoArquivosAnexos_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV27TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00UJ5 ;
          prmP00UJ5 = new Object[] {
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV48ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV52ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoArquivosAnexos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV24TFContratoArquivosAnexos_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoArquivosAnexos_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV27TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UJ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UJ2,100,0,true,false )
             ,new CursorDef("P00UJ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UJ3,100,0,true,false )
             ,new CursorDef("P00UJ4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UJ4,100,0,true,false )
             ,new CursorDef("P00UJ5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UJ5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(9) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(9) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(9) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoarquivosanexosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoarquivosanexosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoarquivosanexosfilterdata") )
          {
             return  ;
          }
          getpromptcontratoarquivosanexosfilterdata worker = new getpromptcontratoarquivosanexosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
