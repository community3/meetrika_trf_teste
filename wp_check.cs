/*
               File: WP_Check
        Description: Check List
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:26:30.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_check : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_check( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_check( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Check_AreaTrabalhoCod ,
                           ref short aP1_Check_Momento ,
                           int aP2_Codigo )
      {
         this.A1840Check_AreaTrabalhoCod = aP0_Check_AreaTrabalhoCod;
         this.AV51Check_Momento = aP1_Check_Momento;
         this.AV38Codigo = aP2_Codigo;
         executePrivate();
         aP0_Check_AreaTrabalhoCod=this.A1840Check_AreaTrabalhoCod;
         aP1_Check_Momento=this.AV51Check_Momento;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCtlcumpre = new GXCombobox();
         cmbavCtlobrigatorio = new GXCombobox();
         cmbavCtlsenaocumpre = new GXCombobox();
         chkavSucesso = new GXCheckbox();
         chkavRetorna = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_11_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A1840Check_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1840Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV51Check_Momento = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Check_Momento), 4, 0)));
                  AV38Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAO92( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTO92( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216263026");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_check.aspx") + "?" + UrlEncode("" +A1840Check_AreaTrabalhoCod) + "," + UrlEncode("" +AV51Check_Momento) + "," + UrlEncode("" +AV38Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_checklist1", AV17SDT_CheckList1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_checklist1", AV17SDT_CheckList1);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CHECKLIST1", AV17SDT_CheckList1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CHECKLIST1", AV17SDT_CheckList1);
         }
         GxWebStd.gx_hidden_field( context, "vPROSEGUIR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52Proseguir), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOG", AV29Log);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOG", AV29Log);
         }
         GxWebStd.gx_hidden_field( context, "vCHECK_MOMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51Check_Momento), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITEM", AV18SDT_Item);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITEM", AV18SDT_Item);
         }
         GxWebStd.gx_hidden_field( context, "CHECKLIST_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1846CheckList_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRAZOANALISE", context.localUtil.TToC( AV39PrazoAnalise, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vNOVOSTATUS", StringUtil.RTrim( AV37NovoStatus));
         GxWebStd.gx_hidden_field( context, "vSTATUSATUAL", StringUtil.RTrim( AV41StatusAtual));
         GxWebStd.gx_hidden_field( context, "vCHECK_NOME", StringUtil.RTrim( AV43Check_Nome));
         GxWebStd.gx_hidden_field( context, "vSTATUS", StringUtil.RTrim( AV36Status));
         GxWebStd.gx_hidden_field( context, "vOBSERVACAO", AV14Observacao);
         GxWebStd.gx_hidden_field( context, "vNAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53NaoConformidade_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV25WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV25WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vDESTINO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42Destino), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV47TipoDias));
         GxWebStd.gx_hidden_field( context, "CHECK_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "LOGRESPONSAVEL_OWNEREHCONTRATANTE", A1149LogResponsavel_OwnerEhContratante);
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1603ContagemResultado_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEO92( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTO92( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_check.aspx") + "?" + UrlEncode("" +A1840Check_AreaTrabalhoCod) + "," + UrlEncode("" +AV51Check_Momento) + "," + UrlEncode("" +AV38Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Check" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check List" ;
      }

      protected void WBO90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_O92( true) ;
         }
         else
         {
            wb_table1_2_O92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_O92e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTO92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Check List", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPO90( ) ;
      }

      protected void WSO92( )
      {
         STARTO92( ) ;
         EVTO92( ) ;
      }

      protected void EVTO92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11O92 */
                                    E11O92 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXIT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12O92 */
                              E12O92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "CTLCUMPRE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "CTLCUMPRE.CLICK") == 0 ) )
                           {
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              AV56GXV1 = nGXsfl_11_idx;
                              if ( ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && ( AV56GXV1 > 0 ) )
                              {
                                 AV17SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13O92 */
                                    E13O92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14O92 */
                                    E14O92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CTLCUMPRE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15O92 */
                                    E15O92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEO92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAO92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "CTLCUMPRE_" + sGXsfl_11_idx;
            cmbavCtlcumpre.Name = GXCCtl;
            cmbavCtlcumpre.WebTags = "";
            cmbavCtlcumpre.addItem("", "---", 0);
            cmbavCtlcumpre.addItem("S", "Sim", 0);
            cmbavCtlcumpre.addItem("N", "N�o", 0);
            cmbavCtlcumpre.addItem("X", "N/A", 0);
            if ( cmbavCtlcumpre.ItemCount > 0 )
            {
               if ( ( AV56GXV1 > 0 ) && ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre)) )
               {
                  ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre = cmbavCtlcumpre.getValidValue(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre);
               }
            }
            GXCCtl = "CTLOBRIGATORIO_" + sGXsfl_11_idx;
            cmbavCtlobrigatorio.Name = GXCCtl;
            cmbavCtlobrigatorio.WebTags = "";
            cmbavCtlobrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbavCtlobrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbavCtlobrigatorio.ItemCount > 0 )
            {
               if ( ( AV56GXV1 > 0 ) && ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && (false==((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio) )
               {
                  ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio = StringUtil.StrToBool( cmbavCtlobrigatorio.getValidValue(StringUtil.BoolToStr( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio)));
               }
            }
            GXCCtl = "CTLSENAOCUMPRE_" + sGXsfl_11_idx;
            cmbavCtlsenaocumpre.Name = GXCCtl;
            cmbavCtlsenaocumpre.WebTags = "";
            cmbavCtlsenaocumpre.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhuma)", 0);
            cmbavCtlsenaocumpre.addItem("1", "Retorna", 0);
            if ( cmbavCtlsenaocumpre.ItemCount > 0 )
            {
               if ( ( AV56GXV1 > 0 ) && ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && (0==((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre) )
               {
                  ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre = (short)(NumberUtil.Val( cmbavCtlsenaocumpre.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre), 4, 0))), "."));
               }
            }
            chkavSucesso.Name = "vSUCESSO";
            chkavSucesso.WebTags = "";
            chkavSucesso.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSucesso_Internalname, "TitleCaption", chkavSucesso.Caption);
            chkavSucesso.CheckedValue = "false";
            chkavRetorna.Name = "vRETORNA";
            chkavRetorna.WebTags = "";
            chkavRetorna.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRetorna_Internalname, "TitleCaption", chkavRetorna.Caption);
            chkavRetorna.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavSucesso_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFO92( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFO92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         cmbavCtlobrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlobrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlobrigatorio.Enabled), 5, 0)));
         edtavCtlnaoconformidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlnaoconformidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlnaoconformidade_Enabled), 5, 0)));
         cmbavCtlsenaocumpre.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlsenaocumpre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlsenaocumpre.Enabled), 5, 0)));
      }

      protected void RFO92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 11;
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(5), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            /* Execute user event: E14O92 */
            E14O92 ();
            wbEnd = 11;
            WBO90( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPO90( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         cmbavCtlobrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlobrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlobrigatorio.Enabled), 5, 0)));
         edtavCtlnaoconformidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlnaoconformidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlnaoconformidade_Enabled), 5, 0)));
         cmbavCtlsenaocumpre.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlsenaocumpre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlsenaocumpre.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13O92 */
         E13O92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_checklist1"), AV17SDT_CheckList1);
            /* Read variables values. */
            AV27Sucesso = StringUtil.StrToBool( cgiGet( chkavSucesso_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Sucesso", AV27Sucesso);
            AV32Retorna = StringUtil.StrToBool( cgiGet( chkavRetorna_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Retorna", AV32Retorna);
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_11"), ",", "."));
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_11"), ",", "."));
            nGXsfl_11_fel_idx = 0;
            while ( nGXsfl_11_fel_idx < nRC_GXsfl_11 )
            {
               nGXsfl_11_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_11_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_11_fel_idx+1));
               sGXsfl_11_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_112( ) ;
               AV56GXV1 = nGXsfl_11_fel_idx;
               if ( ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && ( AV56GXV1 > 0 ) )
               {
                  AV17SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1));
               }
            }
            if ( nGXsfl_11_fel_idx == 0 )
            {
               nGXsfl_11_idx = 1;
               sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
               SubsflControlProps_112( ) ;
            }
            nGXsfl_11_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13O92 */
         E13O92 ();
         if (returnInSub) return;
      }

      protected void E13O92( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV25WWPContext) ;
         AV5Codigos.Add(AV38Codigo, 0);
         AV23WebSession.Set("Codigos", AV5Codigos.ToXml(false, true, "Collection", ""));
         bttBtnenter_Visible = (AV25WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         /* Using cursor H00O93 */
         pr_default.execute(0, new Object[] {AV38Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00O93_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00O93_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = H00O93_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00O93_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00O93_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00O93_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00O93_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00O93_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00O93_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = H00O93_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00O93_n484ContagemResultado_StatusDmn[0];
            A771ContagemResultado_StatusDmnVnc = H00O93_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = H00O93_n771ContagemResultado_StatusDmnVnc[0];
            A457ContagemResultado_Demanda = H00O93_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00O93_n457ContagemResultado_Demanda[0];
            A1618ContagemResultado_PrzRsp = H00O93_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = H00O93_n1618ContagemResultado_PrzRsp[0];
            A1611ContagemResultado_PrzTpDias = H00O93_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = H00O93_n1611ContagemResultado_PrzTpDias[0];
            A682ContagemResultado_PFBFMUltima = H00O93_A682ContagemResultado_PFBFMUltima[0];
            n682ContagemResultado_PFBFMUltima = H00O93_n682ContagemResultado_PFBFMUltima[0];
            A601ContagemResultado_Servico = H00O93_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00O93_n601ContagemResultado_Servico[0];
            A1618ContagemResultado_PrzRsp = H00O93_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = H00O93_n1618ContagemResultado_PrzRsp[0];
            A1611ContagemResultado_PrzTpDias = H00O93_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = H00O93_n1611ContagemResultado_PrzTpDias[0];
            A771ContagemResultado_StatusDmnVnc = H00O93_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = H00O93_n771ContagemResultado_StatusDmnVnc[0];
            A682ContagemResultado_PFBFMUltima = H00O93_A682ContagemResultado_PFBFMUltima[0];
            n682ContagemResultado_PFBFMUltima = H00O93_n682ContagemResultado_PFBFMUltima[0];
            AV10Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            AV19Servico_Codigo = A601ContagemResultado_Servico;
            AV21Unidades = A682ContagemResultado_PFBFMUltima;
            AV41StatusAtual = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41StatusAtual", AV41StatusAtual);
            AV20StatusDmnPai = A771ContagemResultado_StatusDmnVnc;
            lblSubtitle_Caption = "AN�LISE DA OS "+A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSubtitle_Internalname, "Caption", lblSubtitle_Caption);
            AV46Dias = A1618ContagemResultado_PrzRsp;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Dias), 4, 0)));
            AV47TipoDias = A1611ContagemResultado_PrzTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TipoDias", AV47TipoDias);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         chkavSucesso.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSucesso_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSucesso.Visible), 5, 0)));
         chkavRetorna.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRetorna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRetorna.Visible), 5, 0)));
         AV34Sdt_Parametros.FromXml(AV23WebSession.Get("Parametros"), "");
         AV23WebSession.Remove("Parametros");
         AV35UserId = (int)(AV34Sdt_Parametros.gxTpr_Numeric.GetNumeric(1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         if ( AV51Check_Momento == 1 )
         {
            AV39PrazoAnalise = AV34Sdt_Parametros.gxTpr_Datetime.GetDatetime(1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoAnalise", context.localUtil.TToC( AV39PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            AV36Status = ((String)AV34Sdt_Parametros.gxTpr_Character.Item(1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Status", AV36Status);
            AV37NovoStatus = ((String)AV34Sdt_Parametros.gxTpr_Character.Item(2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37NovoStatus", AV37NovoStatus);
         }
         else if ( AV51Check_Momento == 4 )
         {
            AV53NaoConformidade_Codigo = (int)(AV34Sdt_Parametros.gxTpr_Numeric.GetNumeric(2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53NaoConformidade_Codigo), 6, 0)));
         }
         /* Execute user subroutine: 'CARREGAITENS' */
         S112 ();
         if (returnInSub) return;
      }

      private void E14O92( )
      {
         /* Load Routine */
         AV56GXV1 = 1;
         while ( AV56GXV1 <= AV17SDT_CheckList1.Count )
         {
            AV17SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 11;
            }
            sendrow_112( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(11, Grid1Row);
            }
            AV56GXV1 = (short)(AV56GXV1+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E11O92 */
         E11O92 ();
         if (returnInSub) return;
      }

      protected void E11O92( )
      {
         AV56GXV1 = nGXsfl_11_idx;
         if ( AV17SDT_CheckList1.Count >= AV56GXV1 )
         {
            AV17SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1));
         }
         /* Enter Routine */
         /* Execute user subroutine: 'CONFERIRCHECKLIST' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV18SDT_Item", AV18SDT_Item);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29Log", AV29Log);
      }

      protected void E12O92( )
      {
         /* 'DoExit' Routine */
         /* Execute user subroutine: 'DOEXIT' */
         S132 ();
         if (returnInSub) return;
      }

      protected void E15O92( )
      {
         AV56GXV1 = nGXsfl_11_idx;
         if ( AV17SDT_CheckList1.Count >= AV56GXV1 )
         {
            AV17SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1));
         }
         /* Ctlcumpre_Click Routine */
         if ( ((SdtSDT_CheckList_Item)(AV17SDT_CheckList1.CurrentItem)).gxTpr_Obrigatorio )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)(AV17SDT_CheckList1.CurrentItem)).gxTpr_Cumpre)) || ( StringUtil.StrCmp(((SdtSDT_CheckList_Item)(AV17SDT_CheckList1.CurrentItem)).gxTpr_Cumpre, "X") == 0 ) )
            {
               ((SdtSDT_CheckList_Item)(AV17SDT_CheckList1.CurrentItem)).gxTpr_Cumpre = "";
               GX_msglist.addItem("Resposta obrigat�ria!");
               GX_FocusControl = cmbavCtlcumpre_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17SDT_CheckList1", AV17SDT_CheckList1);
         nGXsfl_11_bak_idx = nGXsfl_11_idx;
         gxgrGrid1_refresh( ) ;
         nGXsfl_11_idx = nGXsfl_11_bak_idx;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
      }

      protected void S122( )
      {
         /* 'CONFERIRCHECKLIST' Routine */
         AV40Pendencias = false;
         AV64GXV2 = 1;
         while ( AV64GXV2 <= AV17SDT_CheckList1.Count )
         {
            AV18SDT_Item = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV64GXV2));
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18SDT_Item.gxTpr_Cumpre)) )
            {
               AV40Pendencias = true;
               GX_msglist.addItem("Existem itens sem resposta!");
               if (true) break;
            }
            AV64GXV2 = (int)(AV64GXV2+1);
         }
         if ( ! AV40Pendencias )
         {
            AV14Observacao = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Observacao", AV14Observacao);
            AV27Sucesso = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Sucesso", AV27Sucesso);
            AV65GXV3 = 1;
            while ( AV65GXV3 <= AV17SDT_CheckList1.Count )
            {
               AV18SDT_Item = ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV65GXV3));
               if ( ( StringUtil.StrCmp(AV18SDT_Item.gxTpr_Cumpre, AV18SDT_Item.gxTpr_Naocnfqdo) == 0 ) || ( StringUtil.StrCmp(AV18SDT_Item.gxTpr_Cumpre, "X") == 0 ) )
               {
                  AV27Sucesso = (bool)(((StringUtil.StrCmp(AV18SDT_Item.gxTpr_Cumpre, "X")==0)||!AV18SDT_Item.gxTpr_Impeditivo));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Sucesso", AV27Sucesso);
                  AV32Retorna = (bool)((0==AV52Proseguir)&&(AV32Retorna||((AV18SDT_Item.gxTpr_Senaocumpre==1))));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Retorna", AV32Retorna);
                  AV14Observacao = AV14Observacao + AV18SDT_Item.gxTpr_Descricao + " - N�o cumprido." + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Observacao", AV14Observacao);
                  /* Execute user subroutine: 'GRAVARLOG' */
                  S142 ();
                  if (returnInSub) return;
               }
               AV65GXV3 = (int)(AV65GXV3+1);
            }
         }
         if ( AV27Sucesso )
         {
            /* Execute user subroutine: 'GRAVARLOG' */
            S142 ();
            if (returnInSub) return;
            AV29Log.Save();
            if ( AV29Log.Success() )
            {
               context.CommitDataStores( "WP_Check");
               if ( AV51Check_Momento == 1 )
               {
                  /* Execute user subroutine: 'EFETIVACAPTURA' */
                  S152 ();
                  if (returnInSub) return;
               }
               else if ( AV51Check_Momento == 3 )
               {
                  /* Execute user subroutine: 'EFETIVARCANCELAMENTO' */
                  S162 ();
                  if (returnInSub) return;
               }
               else if ( AV51Check_Momento == 5 )
               {
                  /* Execute user subroutine: 'EFETIVARREJEICAO' */
                  S172 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               context.RollbackDataStores( "WP_Check");
               /* Execute user subroutine: 'ERROSDOSAVE' */
               S182 ();
               if (returnInSub) return;
            }
         }
         else if ( ! AV40Pendencias )
         {
            AV29Log.Save();
            if ( AV29Log.Success() )
            {
               context.CommitDataStores( "WP_Check");
               if ( AV32Retorna )
               {
                  /* Execute user subroutine: 'RETORNO' */
                  S192 ();
                  if (returnInSub) return;
               }
               else
               {
                  /* Execute user subroutine: 'DOEXIT' */
                  S132 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               context.RollbackDataStores( "WP_Check");
               /* Execute user subroutine: 'ERROSDOSAVE' */
               S182 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S112( )
      {
         /* 'CARREGAITENS' Routine */
         /* Using cursor H00O94 */
         pr_default.execute(1, new Object[] {A1840Check_AreaTrabalhoCod, AV51Check_Momento});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1839Check_Codigo = H00O94_A1839Check_Codigo[0];
            n1839Check_Codigo = H00O94_n1839Check_Codigo[0];
            A1843Check_Momento = H00O94_A1843Check_Momento[0];
            A1841Check_Nome = H00O94_A1841Check_Nome[0];
            A1856Check_QdoNaoCmp = H00O94_A1856Check_QdoNaoCmp[0];
            n1856Check_QdoNaoCmp = H00O94_n1856Check_QdoNaoCmp[0];
            AV43Check_Nome = StringUtil.Trim( A1841Check_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Check_Nome", AV43Check_Nome);
            AV52Proseguir = A1856Check_QdoNaoCmp;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Proseguir", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52Proseguir), 4, 0)));
            lblSubtitle_Caption = AV43Check_Nome;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSubtitle_Internalname, "Caption", lblSubtitle_Caption);
            /* Using cursor H00O95 */
            pr_default.execute(2, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1151CheckList_Ativo = H00O95_A1151CheckList_Ativo[0];
               A758CheckList_Codigo = H00O95_A758CheckList_Codigo[0];
               A763CheckList_Descricao = H00O95_A763CheckList_Descricao[0];
               A1845CheckList_Obrigatorio = H00O95_A1845CheckList_Obrigatorio[0];
               n1845CheckList_Obrigatorio = H00O95_n1845CheckList_Obrigatorio[0];
               A1848CheckList_SeNaoCumpre = H00O95_A1848CheckList_SeNaoCumpre[0];
               n1848CheckList_SeNaoCumpre = H00O95_n1848CheckList_SeNaoCumpre[0];
               A1851CheckList_NaoCnfQdo = H00O95_A1851CheckList_NaoCnfQdo[0];
               n1851CheckList_NaoCnfQdo = H00O95_n1851CheckList_NaoCnfQdo[0];
               A1850CheckList_Impeditivo = H00O95_A1850CheckList_Impeditivo[0];
               n1850CheckList_Impeditivo = H00O95_n1850CheckList_Impeditivo[0];
               AV18SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
               AV18SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
               AV18SDT_Item.gxTpr_Obrigatorio = A1845CheckList_Obrigatorio;
               AV18SDT_Item.gxTpr_Senaocumpre = A1848CheckList_SeNaoCumpre;
               AV18SDT_Item.gxTpr_Naocnfqdo = A1851CheckList_NaoCnfQdo;
               AV18SDT_Item.gxTpr_Impeditivo = A1850CheckList_Impeditivo;
               AV17SDT_CheckList1.Add(AV18SDT_Item, 0);
               gx_BV11 = true;
               AV18SDT_Item = new SdtSDT_CheckList_Item(context);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void S142( )
      {
         /* 'GRAVARLOG' Routine */
         AV29Log = new SdtContagemResultadoChckLstLog(context);
         if ( AV27Sucesso )
         {
            AV29Log.gxTv_SdtContagemResultadoChckLstLog_Contagemresultadochcklstlog_chcklstcod_SetNull();
         }
         else
         {
            AV29Log.gxTpr_Contagemresultadochcklstlog_chcklstcod = AV18SDT_Item.gxTpr_Codigo;
            /* Using cursor H00O96 */
            pr_default.execute(3, new Object[] {AV18SDT_Item.gxTpr_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A758CheckList_Codigo = H00O96_A758CheckList_Codigo[0];
               A1846CheckList_NaoCnfCod = H00O96_A1846CheckList_NaoCnfCod[0];
               n1846CheckList_NaoCnfCod = H00O96_n1846CheckList_NaoCnfCod[0];
               AV18SDT_Item.gxTpr_Naoconformidade = A1846CheckList_NaoCnfCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
         }
         if ( AV18SDT_Item.gxTpr_Naoconformidade > 0 )
         {
            AV29Log.gxTpr_Naoconformidade_codigo = AV18SDT_Item.gxTpr_Naoconformidade;
         }
         else
         {
            AV29Log.gxTv_SdtContagemResultadoChckLstLog_Naoconformidade_codigo_SetNull();
         }
         AV29Log.gxTpr_Contagemresultadochcklstlog_oscodigo = AV38Codigo;
         AV29Log.gxTpr_Contagemresultadochcklstlog_datahora = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV29Log.gxTpr_Contagemresultadochcklstlog_usuariocod = AV35UserId;
         AV29Log.gxTpr_Contagemresultadochcklstlog_etapa = 0;
      }

      protected void S192( )
      {
         /* 'RETORNO' Routine */
         /* Execute user subroutine: 'ULTIMOATORDACONTRATANTE' */
         S202 ();
         if (returnInSub) return;
         if ( AV42Destino > 0 )
         {
            if ( StringUtil.StrCmp(AV41StatusAtual, "E") == 0 )
            {
               new prc_setfimanl(context ).execute( ref  AV38Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV41StatusAtual, "A") == 0 )
            {
               new prc_setfimexc(context ).execute( ref  AV38Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
            }
            if ( (0==AV46Dias) )
            {
               AV46Dias = 5;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Dias), 4, 0)));
            }
            GXt_dtime1 = AV45PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  DateTimeUtil.ServerNow( context, "DEFAULT"),  AV46Dias,  AV47TipoDias, out  GXt_dtime1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TipoDias", AV47TipoDias);
            AV45PrazoEntrega = GXt_dtime1;
            new prc_savenewdataentrega(context ).execute(  AV38Codigo,  AV45PrazoEntrega,  false) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
            new prc_asignardemanda(context ).execute( ref  AV38Codigo,  AV42Destino,  "R",  "D") ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Destino", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Destino), 6, 0)));
            new prc_inslogresponsavel(context ).execute( ref  AV38Codigo,  AV42Destino,  "R",  "D",  AV35UserId,  0,  AV41StatusAtual,  "D",  "Itens do check list "+AV43Check_Nome+" n�o cumpridos.",  AV45PrazoEntrega,  true) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Destino", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Destino), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41StatusAtual", AV41StatusAtual);
            /* Execute user subroutine: 'DOFECHAR' */
            S212 ();
            if (returnInSub) return;
         }
         else
         {
            GX_msglist.addItem("Contratante n�o achado para efetivar o retorno!");
         }
      }

      protected void S212( )
      {
         /* 'DOFECHAR' Routine */
         lblTbjava_Caption = "<script language=\"javascript\" type=\"javascript\">parent.location.replace(parent.location.href); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void S132( )
      {
         /* 'DOEXIT' Routine */
         context.setWebReturnParms(new Object[] {(int)A1840Check_AreaTrabalhoCod,(short)AV51Check_Momento});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S152( )
      {
         /* 'EFETIVACAPTURA' Routine */
         new prc_savenewdataentrega(context ).execute(  AV38Codigo,  AV39PrazoAnalise,  false) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoAnalise", context.localUtil.TToC( AV39PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         new prc_asignardemanda(context ).execute( ref  AV38Codigo,  AV35UserId,  "C",  AV37NovoStatus) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37NovoStatus", AV37NovoStatus);
         new prc_setinicioanl(context ).execute( ref  AV38Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         new prc_inslogresponsavel(context ).execute( ref  AV38Codigo,  0,  "F",  "D",  AV35UserId,  0,  AV41StatusAtual,  AV41StatusAtual,  "Check list "+AV43Check_Nome+" finalizado.",  AV39PrazoAnalise,  false) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41StatusAtual", AV41StatusAtual);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41StatusAtual", AV41StatusAtual);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoAnalise", context.localUtil.TToC( AV39PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         new prc_inslogresponsavel(context ).execute( ref  AV38Codigo,  AV35UserId,  "C",  "D",  AV35UserId,  0,  AV36Status,  AV37NovoStatus,  "",  AV39PrazoAnalise,  true) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Status", AV36Status);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37NovoStatus", AV37NovoStatus);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39PrazoAnalise", context.localUtil.TToC( AV39PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         new prc_disparoservicovinculado(context ).execute(  AV38Codigo,  AV35UserId) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         /* Execute user subroutine: 'DOFECHAR' */
         S212 ();
         if (returnInSub) return;
      }

      protected void S162( )
      {
         /* 'EFETIVARCANCELAMENTO' Routine */
         new prc_cancelaros(context ).execute(  AV38Codigo,  AV35UserId,  AV14Observacao,  AV53NaoConformidade_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Observacao", AV14Observacao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53NaoConformidade_Codigo), 6, 0)));
         GXt_int2 = AV25WWPContext.gxTpr_Areatrabalho_codigo;
         GXt_int3 = 4;
         if ( new prc_temchecklist(context).executeUdp( ref  GXt_int2, ref  GXt_int3) )
         {
            AV51Check_Momento = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Check_Momento), 4, 0)));
            context.DoAjaxRefresh();
         }
         else
         {
            /* Execute user subroutine: 'DOFECHAR' */
            S212 ();
            if (returnInSub) return;
         }
      }

      protected void S172( )
      {
         /* 'EFETIVARREJEICAO' Routine */
         new prc_contagemresultadoalterastatus(context ).execute( ref  AV38Codigo,  "D",  0,  AV14Observacao, ref  AV27Sucesso) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Observacao", AV14Observacao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Sucesso", AV27Sucesso);
         GXt_int2 = AV25WWPContext.gxTpr_Areatrabalho_codigo;
         GXt_int3 = 6;
         if ( new prc_temchecklist(context).executeUdp( ref  GXt_int2, ref  GXt_int3) )
         {
            AV51Check_Momento = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Check_Momento), 4, 0)));
            context.DoAjaxRefresh();
         }
         else
         {
            /* Execute user subroutine: 'DOFECHAR' */
            S212 ();
            if (returnInSub) return;
         }
      }

      protected void S202( )
      {
         /* 'ULTIMOATORDACONTRATANTE' Routine */
         AV69GXLvl248 = 0;
         /* Using cursor H00O97 */
         pr_default.execute(4, new Object[] {AV38Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1797LogResponsavel_Codigo = H00O97_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = H00O97_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = H00O97_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00O97_n892LogResponsavel_DemandaCod[0];
            GXt_boolean4 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
            if ( A1149LogResponsavel_OwnerEhContratante )
            {
               AV69GXLvl248 = 1;
               AV42Destino = A896LogResponsavel_Owner;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Destino", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Destino), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( AV69GXLvl248 == 0 )
         {
            /* Using cursor H00O98 */
            pr_default.execute(5, new Object[] {AV38Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00O98_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00O98_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = H00O98_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00O98_n1603ContagemResultado_CntCod[0];
               A456ContagemResultado_Codigo = H00O98_A456ContagemResultado_Codigo[0];
               A1603ContagemResultado_CntCod = H00O98_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00O98_n1603ContagemResultado_CntCod[0];
               /* Using cursor H00O99 */
               pr_default.execute(6, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00O99_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00O99_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00O99_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00O99_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00O99_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00O99_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean4 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean4) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean4;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV42Destino = A1079ContratoGestor_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Destino", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Destino), 6, 0)));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(6);
               }
               pr_default.close(6);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
         }
      }

      protected void S182( )
      {
         /* 'ERROSDOSAVE' Routine */
         AV73GXV5 = 1;
         AV72GXV4 = AV29Log.GetMessages();
         while ( AV73GXV5 <= AV72GXV4.Count )
         {
            AV50Message = ((SdtMessages_Message)AV72GXV4.Item(AV73GXV5));
            GX_msglist.addItem(AV50Message.gxTpr_Description);
            AV73GXV5 = (int)(AV73GXV5+1);
         }
      }

      protected void wb_table1_2_O92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(500), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSubtitle_Internalname, lblSubtitle_Caption, "", "", lblSubtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 5, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(69), 4, 0))+"px"+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"font-family:'Calibri'; font-size:11.0pt; font-weight:bold; font-style:normal; color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Adequa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Obrigat�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "N�o Conformidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Se Nao Cumpre") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "Grid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(5), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcodigo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldescricao_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlobrigatorio.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlnaoconformidade_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlsenaocumpre.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV56GXV1 = nGXsfl_11_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Check.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOEXIT\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_Check.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSucesso_Internalname, StringUtil.BoolToStr( AV27Sucesso), "", "", chkavSucesso.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(27, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRetorna_Internalname, StringUtil.BoolToStr( AV32Retorna), "", "", chkavRetorna.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(28, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_O92e( true) ;
         }
         else
         {
            wb_table1_2_O92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1840Check_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1840Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0)));
         AV51Check_Momento = Convert.ToInt16(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51Check_Momento), 4, 0)));
         AV38Codigo = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAO92( ) ;
         WSO92( ) ;
         WEO92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216263112");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_check.js", "?20206216263112");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtavCtlcodigo_Internalname = "CTLCODIGO_"+sGXsfl_11_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_11_idx;
         cmbavCtlcumpre_Internalname = "CTLCUMPRE_"+sGXsfl_11_idx;
         cmbavCtlobrigatorio_Internalname = "CTLOBRIGATORIO_"+sGXsfl_11_idx;
         edtavCtlnaoconformidade_Internalname = "CTLNAOCONFORMIDADE_"+sGXsfl_11_idx;
         cmbavCtlsenaocumpre_Internalname = "CTLSENAOCUMPRE_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtavCtlcodigo_Internalname = "CTLCODIGO_"+sGXsfl_11_fel_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_11_fel_idx;
         cmbavCtlcumpre_Internalname = "CTLCUMPRE_"+sGXsfl_11_fel_idx;
         cmbavCtlobrigatorio_Internalname = "CTLOBRIGATORIO_"+sGXsfl_11_fel_idx;
         edtavCtlnaoconformidade_Internalname = "CTLNAOCONFORMIDADE_"+sGXsfl_11_fel_idx;
         cmbavCtlsenaocumpre_Internalname = "CTLSENAOCUMPRE_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBO90( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Codigo), 6, 0, ",", "")),((edtavCtlcodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescricao_Internalname,((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Descricao,((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldescricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldescricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)11,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLCUMPRE_" + sGXsfl_11_idx;
            cmbavCtlcumpre.Name = GXCCtl;
            cmbavCtlcumpre.WebTags = "";
            cmbavCtlcumpre.addItem("", "---", 0);
            cmbavCtlcumpre.addItem("S", "Sim", 0);
            cmbavCtlcumpre.addItem("N", "N�o", 0);
            cmbavCtlcumpre.addItem("X", "N/A", 0);
            if ( cmbavCtlcumpre.ItemCount > 0 )
            {
               if ( ( AV56GXV1 > 0 ) && ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre)) )
               {
                  ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre = cmbavCtlcumpre.getValidValue(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre);
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlcumpre,(String)cmbavCtlcumpre_Internalname,StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre),(short)1,(String)cmbavCtlcumpre_Jsonclick,(short)5,"'"+""+"'"+",false,"+"'"+"ECTLCUMPRE.CLICK."+sGXsfl_11_idx+"'",(String)"char",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)69,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,14);\"" : " "),(String)"",(bool)true});
         cmbavCtlcumpre.CurrentValue = StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Cumpre);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre_Internalname, "Values", (String)(cmbavCtlcumpre.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLOBRIGATORIO_" + sGXsfl_11_idx;
            cmbavCtlobrigatorio.Name = GXCCtl;
            cmbavCtlobrigatorio.WebTags = "";
            cmbavCtlobrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbavCtlobrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbavCtlobrigatorio.ItemCount > 0 )
            {
               if ( ( AV56GXV1 > 0 ) && ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && (false==((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio) )
               {
                  ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio = StringUtil.StrToBool( cmbavCtlobrigatorio.getValidValue(StringUtil.BoolToStr( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio)));
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlobrigatorio,(String)cmbavCtlobrigatorio_Internalname,StringUtil.BoolToStr( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio),(short)1,(String)cmbavCtlobrigatorio_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)0,cmbavCtlobrigatorio.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbavCtlobrigatorio.CurrentValue = StringUtil.BoolToStr( ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Obrigatorio);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlobrigatorio_Internalname, "Values", (String)(cmbavCtlobrigatorio.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlnaoconformidade_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Naoconformidade), 6, 0, ",", "")),((edtavCtlnaoconformidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Naoconformidade), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Naoconformidade), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlnaoconformidade_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlnaoconformidade_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLSENAOCUMPRE_" + sGXsfl_11_idx;
            cmbavCtlsenaocumpre.Name = GXCCtl;
            cmbavCtlsenaocumpre.WebTags = "";
            cmbavCtlsenaocumpre.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhuma)", 0);
            cmbavCtlsenaocumpre.addItem("1", "Retorna", 0);
            if ( cmbavCtlsenaocumpre.ItemCount > 0 )
            {
               if ( ( AV56GXV1 > 0 ) && ( AV17SDT_CheckList1.Count >= AV56GXV1 ) && (0==((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre) )
               {
                  ((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre = (short)(NumberUtil.Val( cmbavCtlsenaocumpre.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre), 4, 0))), "."));
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlsenaocumpre,(String)cmbavCtlsenaocumpre_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre), 4, 0)),(short)1,(String)cmbavCtlsenaocumpre_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)0,cmbavCtlsenaocumpre.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbavCtlsenaocumpre.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_CheckList_Item)AV17SDT_CheckList1.Item(AV56GXV1)).gxTpr_Senaocumpre), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlsenaocumpre_Internalname, "Values", (String)(cmbavCtlsenaocumpre.ToJavascriptSource()));
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_11_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         lblSubtitle_Internalname = "SUBTITLE";
         edtavCtlcodigo_Internalname = "CTLCODIGO";
         edtavCtldescricao_Internalname = "CTLDESCRICAO";
         cmbavCtlcumpre_Internalname = "CTLCUMPRE";
         cmbavCtlobrigatorio_Internalname = "CTLOBRIGATORIO";
         edtavCtlnaoconformidade_Internalname = "CTLNAOCONFORMIDADE";
         cmbavCtlsenaocumpre_Internalname = "CTLSENAOCUMPRE";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         lblTbjava_Internalname = "TBJAVA";
         chkavSucesso_Internalname = "vSUCESSO";
         chkavRetorna_Internalname = "vRETORNA";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavCtlsenaocumpre_Jsonclick = "";
         edtavCtlnaoconformidade_Jsonclick = "";
         cmbavCtlobrigatorio_Jsonclick = "";
         cmbavCtlcumpre_Jsonclick = "";
         cmbavCtlcumpre.Visible = -1;
         cmbavCtlcumpre.Enabled = 1;
         edtavCtldescricao_Jsonclick = "";
         edtavCtlcodigo_Jsonclick = "";
         lblTbjava_Visible = 1;
         bttBtnenter_Visible = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         cmbavCtlsenaocumpre.Enabled = 0;
         edtavCtlnaoconformidade_Enabled = 0;
         cmbavCtlobrigatorio.Enabled = 0;
         edtavCtldescricao_Enabled = 0;
         edtavCtlcodigo_Enabled = 0;
         subGrid1_Class = "Grid";
         lblTbjava_Caption = "Java";
         chkavRetorna.Visible = 1;
         chkavSucesso.Visible = 1;
         lblSubtitle_Caption = "CHECK LIST";
         subGrid1_Backcolorstyle = 0;
         cmbavCtlsenaocumpre.Enabled = -1;
         edtavCtlnaoconformidade_Enabled = -1;
         cmbavCtlobrigatorio.Enabled = -1;
         edtavCtldescricao_Enabled = -1;
         edtavCtlcodigo_Enabled = -1;
         chkavRetorna.Caption = "";
         chkavSucesso.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Check List";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'AV17SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:11,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E11O92',iparms:[{av:'AV17SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:11,pic:'',nv:null},{av:'AV52Proseguir',fld:'vPROSEGUIR',pic:'ZZZ9',nv:0},{av:'AV32Retorna',fld:'vRETORNA',pic:'',nv:false},{av:'AV27Sucesso',fld:'vSUCESSO',pic:'',nv:false},{av:'AV29Log',fld:'vLOG',pic:'',nv:null},{av:'AV51Check_Momento',fld:'vCHECK_MOMENTO',pic:'ZZZ9',nv:0},{av:'AV18SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'AV38Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0},{av:'AV39PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV37NovoStatus',fld:'vNOVOSTATUS',pic:'',nv:''},{av:'AV41StatusAtual',fld:'vSTATUSATUAL',pic:'',nv:''},{av:'AV43Check_Nome',fld:'vCHECK_NOME',pic:'@!',nv:''},{av:'AV36Status',fld:'vSTATUS',pic:'',nv:''},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'AV14Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV53NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV42Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'AV46Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV47TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A1840Check_AreaTrabalhoCod',fld:'CHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV18SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'AV14Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV27Sucesso',fld:'vSUCESSO',pic:'',nv:false},{av:'AV32Retorna',fld:'vRETORNA',pic:'',nv:false},{av:'AV29Log',fld:'vLOG',pic:'',nv:null},{av:'AV38Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Check_Momento',fld:'vCHECK_MOMENTO',pic:'ZZZ9',nv:0},{av:'AV46Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV42Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOEXIT'","{handler:'E12O92',iparms:[{av:'AV51Check_Momento',fld:'vCHECK_MOMENTO',pic:'ZZZ9',nv:0},{av:'A1840Check_AreaTrabalhoCod',fld:'CHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CTLCUMPRE.CLICK","{handler:'E15O92',iparms:[{av:'AV17SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:11,pic:'',nv:null},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0}],oparms:[{av:'AV17SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:11,pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV17SDT_CheckList1 = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         AV29Log = new SdtContagemResultadoChckLstLog(context);
         AV18SDT_Item = new SdtSDT_CheckList_Item(context);
         AV39PrazoAnalise = (DateTime)(DateTime.MinValue);
         AV37NovoStatus = "";
         AV41StatusAtual = "";
         AV43Check_Nome = "";
         AV36Status = "";
         AV14Observacao = "";
         AV25WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV47TipoDias = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Grid1Container = new GXWebGrid( context);
         AV5Codigos = new GxSimpleCollection();
         AV23WebSession = context.GetSession();
         scmdbuf = "";
         H00O93_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00O93_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00O93_A602ContagemResultado_OSVinculada = new int[1] ;
         H00O93_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00O93_A456ContagemResultado_Codigo = new int[1] ;
         H00O93_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00O93_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00O93_A601ContagemResultado_Servico = new int[1] ;
         H00O93_n601ContagemResultado_Servico = new bool[] {false} ;
         H00O93_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00O93_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00O93_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         H00O93_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         H00O93_A457ContagemResultado_Demanda = new String[] {""} ;
         H00O93_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00O93_A1618ContagemResultado_PrzRsp = new short[1] ;
         H00O93_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         H00O93_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         H00O93_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         H00O93_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00O93_n682ContagemResultado_PFBFMUltima = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A771ContagemResultado_StatusDmnVnc = "";
         A457ContagemResultado_Demanda = "";
         A1611ContagemResultado_PrzTpDias = "";
         AV20StatusDmnPai = "";
         AV34Sdt_Parametros = new SdtSDT_Parametros(context);
         Grid1Row = new GXWebRow();
         H00O94_A1840Check_AreaTrabalhoCod = new int[1] ;
         H00O94_A1839Check_Codigo = new int[1] ;
         H00O94_n1839Check_Codigo = new bool[] {false} ;
         H00O94_A1843Check_Momento = new short[1] ;
         H00O94_A1841Check_Nome = new String[] {""} ;
         H00O94_A1856Check_QdoNaoCmp = new short[1] ;
         H00O94_n1856Check_QdoNaoCmp = new bool[] {false} ;
         A1841Check_Nome = "";
         H00O95_A1839Check_Codigo = new int[1] ;
         H00O95_n1839Check_Codigo = new bool[] {false} ;
         H00O95_A1151CheckList_Ativo = new bool[] {false} ;
         H00O95_A758CheckList_Codigo = new int[1] ;
         H00O95_A763CheckList_Descricao = new String[] {""} ;
         H00O95_A1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O95_n1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O95_A1848CheckList_SeNaoCumpre = new short[1] ;
         H00O95_n1848CheckList_SeNaoCumpre = new bool[] {false} ;
         H00O95_A1851CheckList_NaoCnfQdo = new String[] {""} ;
         H00O95_n1851CheckList_NaoCnfQdo = new bool[] {false} ;
         H00O95_A1850CheckList_Impeditivo = new bool[] {false} ;
         H00O95_n1850CheckList_Impeditivo = new bool[] {false} ;
         A763CheckList_Descricao = "";
         A1851CheckList_NaoCnfQdo = "";
         H00O96_A758CheckList_Codigo = new int[1] ;
         H00O96_A1846CheckList_NaoCnfCod = new int[1] ;
         H00O96_n1846CheckList_NaoCnfCod = new bool[] {false} ;
         AV45PrazoEntrega = (DateTime)(DateTime.MinValue);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         H00O97_A1797LogResponsavel_Codigo = new long[1] ;
         H00O97_A896LogResponsavel_Owner = new int[1] ;
         H00O97_A892LogResponsavel_DemandaCod = new int[1] ;
         H00O97_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00O98_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00O98_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00O98_A1603ContagemResultado_CntCod = new int[1] ;
         H00O98_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00O98_A456ContagemResultado_Codigo = new int[1] ;
         H00O99_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00O99_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00O99_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00O99_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV72GXV4 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV50Message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblSubtitle_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_check__default(),
            new Object[][] {
                new Object[] {
               H00O93_A1553ContagemResultado_CntSrvCod, H00O93_n1553ContagemResultado_CntSrvCod, H00O93_A602ContagemResultado_OSVinculada, H00O93_n602ContagemResultado_OSVinculada, H00O93_A456ContagemResultado_Codigo, H00O93_A490ContagemResultado_ContratadaCod, H00O93_n490ContagemResultado_ContratadaCod, H00O93_A601ContagemResultado_Servico, H00O93_n601ContagemResultado_Servico, H00O93_A484ContagemResultado_StatusDmn,
               H00O93_n484ContagemResultado_StatusDmn, H00O93_A771ContagemResultado_StatusDmnVnc, H00O93_n771ContagemResultado_StatusDmnVnc, H00O93_A457ContagemResultado_Demanda, H00O93_n457ContagemResultado_Demanda, H00O93_A1618ContagemResultado_PrzRsp, H00O93_n1618ContagemResultado_PrzRsp, H00O93_A1611ContagemResultado_PrzTpDias, H00O93_n1611ContagemResultado_PrzTpDias, H00O93_A682ContagemResultado_PFBFMUltima,
               H00O93_n682ContagemResultado_PFBFMUltima
               }
               , new Object[] {
               H00O94_A1840Check_AreaTrabalhoCod, H00O94_A1839Check_Codigo, H00O94_A1843Check_Momento, H00O94_A1841Check_Nome, H00O94_A1856Check_QdoNaoCmp, H00O94_n1856Check_QdoNaoCmp
               }
               , new Object[] {
               H00O95_A1839Check_Codigo, H00O95_n1839Check_Codigo, H00O95_A1151CheckList_Ativo, H00O95_A758CheckList_Codigo, H00O95_A763CheckList_Descricao, H00O95_A1845CheckList_Obrigatorio, H00O95_n1845CheckList_Obrigatorio, H00O95_A1848CheckList_SeNaoCumpre, H00O95_n1848CheckList_SeNaoCumpre, H00O95_A1851CheckList_NaoCnfQdo,
               H00O95_n1851CheckList_NaoCnfQdo, H00O95_A1850CheckList_Impeditivo, H00O95_n1850CheckList_Impeditivo
               }
               , new Object[] {
               H00O96_A758CheckList_Codigo, H00O96_A1846CheckList_NaoCnfCod, H00O96_n1846CheckList_NaoCnfCod
               }
               , new Object[] {
               H00O97_A1797LogResponsavel_Codigo, H00O97_A896LogResponsavel_Owner, H00O97_A892LogResponsavel_DemandaCod, H00O97_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00O98_A1553ContagemResultado_CntSrvCod, H00O98_n1553ContagemResultado_CntSrvCod, H00O98_A1603ContagemResultado_CntCod, H00O98_n1603ContagemResultado_CntCod, H00O98_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00O99_A1078ContratoGestor_ContratoCod, H00O99_A1079ContratoGestor_UsuarioCod, H00O99_A1446ContratoGestor_ContratadaAreaCod, H00O99_n1446ContratoGestor_ContratadaAreaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         edtavCtldescricao_Enabled = 0;
         cmbavCtlobrigatorio.Enabled = 0;
         edtavCtlnaoconformidade_Enabled = 0;
         cmbavCtlsenaocumpre.Enabled = 0;
      }

      private short AV51Check_Momento ;
      private short wcpOAV51Check_Momento ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV52Proseguir ;
      private short AV46Dias ;
      private short wbEnd ;
      private short wbStart ;
      private short AV56GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_11_fel_idx=1 ;
      private short A1618ContagemResultado_PrzRsp ;
      private short GRID1_nEOF ;
      private short nGXsfl_11_bak_idx=1 ;
      private short A1843Check_Momento ;
      private short A1856Check_QdoNaoCmp ;
      private short A1848CheckList_SeNaoCumpre ;
      private short GXt_int3 ;
      private short AV69GXLvl248 ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int A1840Check_AreaTrabalhoCod ;
      private int AV38Codigo ;
      private int wcpOA1840Check_AreaTrabalhoCod ;
      private int wcpOAV38Codigo ;
      private int A758CheckList_Codigo ;
      private int A1846CheckList_NaoCnfCod ;
      private int AV35UserId ;
      private int AV53NaoConformidade_Codigo ;
      private int AV42Destino ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A456ContagemResultado_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int subGrid1_Islastpage ;
      private int edtavCtlcodigo_Enabled ;
      private int edtavCtldescricao_Enabled ;
      private int edtavCtlnaoconformidade_Enabled ;
      private int bttBtnenter_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV10Contratada_Codigo ;
      private int AV19Servico_Codigo ;
      private int lblTbjava_Visible ;
      private int AV64GXV2 ;
      private int AV65GXV3 ;
      private int A1839Check_Codigo ;
      private int GXt_int2 ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int AV73GXV5 ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long A1797LogResponsavel_Codigo ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV21Unidades ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_11_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV37NovoStatus ;
      private String AV41StatusAtual ;
      private String AV43Check_Nome ;
      private String AV36Status ;
      private String AV47TipoDias ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String chkavSucesso_Internalname ;
      private String chkavRetorna_Internalname ;
      private String edtavCtlcodigo_Internalname ;
      private String edtavCtldescricao_Internalname ;
      private String cmbavCtlobrigatorio_Internalname ;
      private String edtavCtlnaoconformidade_Internalname ;
      private String cmbavCtlsenaocumpre_Internalname ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String bttBtnenter_Internalname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV20StatusDmnPai ;
      private String lblSubtitle_Caption ;
      private String lblSubtitle_Internalname ;
      private String lblTbjava_Internalname ;
      private String cmbavCtlcumpre_Internalname ;
      private String A1841Check_Nome ;
      private String A1851CheckList_NaoCnfQdo ;
      private String lblTbjava_Caption ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblSubtitle_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String TempTags ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlcodigo_Jsonclick ;
      private String edtavCtldescricao_Jsonclick ;
      private String cmbavCtlcumpre_Jsonclick ;
      private String cmbavCtlobrigatorio_Jsonclick ;
      private String edtavCtlnaoconformidade_Jsonclick ;
      private String cmbavCtlsenaocumpre_Jsonclick ;
      private DateTime AV39PrazoAnalise ;
      private DateTime AV45PrazoEntrega ;
      private DateTime GXt_dtime1 ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV27Sucesso ;
      private bool AV32Retorna ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n457ContagemResultado_Demanda ;
      private bool n1618ContagemResultado_PrzRsp ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n682ContagemResultado_PFBFMUltima ;
      private bool AV40Pendencias ;
      private bool n1839Check_Codigo ;
      private bool n1856Check_QdoNaoCmp ;
      private bool A1151CheckList_Ativo ;
      private bool A1845CheckList_Obrigatorio ;
      private bool n1845CheckList_Obrigatorio ;
      private bool n1848CheckList_SeNaoCumpre ;
      private bool n1851CheckList_NaoCnfQdo ;
      private bool A1850CheckList_Impeditivo ;
      private bool n1850CheckList_Impeditivo ;
      private bool gx_BV11 ;
      private bool n1846CheckList_NaoCnfCod ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool GXt_boolean4 ;
      private String AV14Observacao ;
      private String A763CheckList_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Check_AreaTrabalhoCod ;
      private short aP1_Check_Momento ;
      private GXCombobox cmbavCtlcumpre ;
      private GXCombobox cmbavCtlobrigatorio ;
      private GXCombobox cmbavCtlsenaocumpre ;
      private GXCheckbox chkavSucesso ;
      private GXCheckbox chkavRetorna ;
      private IDataStoreProvider pr_default ;
      private int[] H00O93_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00O93_n1553ContagemResultado_CntSrvCod ;
      private int[] H00O93_A602ContagemResultado_OSVinculada ;
      private bool[] H00O93_n602ContagemResultado_OSVinculada ;
      private int[] H00O93_A456ContagemResultado_Codigo ;
      private int[] H00O93_A490ContagemResultado_ContratadaCod ;
      private bool[] H00O93_n490ContagemResultado_ContratadaCod ;
      private int[] H00O93_A601ContagemResultado_Servico ;
      private bool[] H00O93_n601ContagemResultado_Servico ;
      private String[] H00O93_A484ContagemResultado_StatusDmn ;
      private bool[] H00O93_n484ContagemResultado_StatusDmn ;
      private String[] H00O93_A771ContagemResultado_StatusDmnVnc ;
      private bool[] H00O93_n771ContagemResultado_StatusDmnVnc ;
      private String[] H00O93_A457ContagemResultado_Demanda ;
      private bool[] H00O93_n457ContagemResultado_Demanda ;
      private short[] H00O93_A1618ContagemResultado_PrzRsp ;
      private bool[] H00O93_n1618ContagemResultado_PrzRsp ;
      private String[] H00O93_A1611ContagemResultado_PrzTpDias ;
      private bool[] H00O93_n1611ContagemResultado_PrzTpDias ;
      private decimal[] H00O93_A682ContagemResultado_PFBFMUltima ;
      private bool[] H00O93_n682ContagemResultado_PFBFMUltima ;
      private int[] H00O94_A1840Check_AreaTrabalhoCod ;
      private int[] H00O94_A1839Check_Codigo ;
      private bool[] H00O94_n1839Check_Codigo ;
      private short[] H00O94_A1843Check_Momento ;
      private String[] H00O94_A1841Check_Nome ;
      private short[] H00O94_A1856Check_QdoNaoCmp ;
      private bool[] H00O94_n1856Check_QdoNaoCmp ;
      private int[] H00O95_A1839Check_Codigo ;
      private bool[] H00O95_n1839Check_Codigo ;
      private bool[] H00O95_A1151CheckList_Ativo ;
      private int[] H00O95_A758CheckList_Codigo ;
      private String[] H00O95_A763CheckList_Descricao ;
      private bool[] H00O95_A1845CheckList_Obrigatorio ;
      private bool[] H00O95_n1845CheckList_Obrigatorio ;
      private short[] H00O95_A1848CheckList_SeNaoCumpre ;
      private bool[] H00O95_n1848CheckList_SeNaoCumpre ;
      private String[] H00O95_A1851CheckList_NaoCnfQdo ;
      private bool[] H00O95_n1851CheckList_NaoCnfQdo ;
      private bool[] H00O95_A1850CheckList_Impeditivo ;
      private bool[] H00O95_n1850CheckList_Impeditivo ;
      private int[] H00O96_A758CheckList_Codigo ;
      private int[] H00O96_A1846CheckList_NaoCnfCod ;
      private bool[] H00O96_n1846CheckList_NaoCnfCod ;
      private long[] H00O97_A1797LogResponsavel_Codigo ;
      private int[] H00O97_A896LogResponsavel_Owner ;
      private int[] H00O97_A892LogResponsavel_DemandaCod ;
      private bool[] H00O97_n892LogResponsavel_DemandaCod ;
      private int[] H00O98_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00O98_n1553ContagemResultado_CntSrvCod ;
      private int[] H00O98_A1603ContagemResultado_CntCod ;
      private bool[] H00O98_n1603ContagemResultado_CntCod ;
      private int[] H00O98_A456ContagemResultado_Codigo ;
      private int[] H00O99_A1078ContratoGestor_ContratoCod ;
      private int[] H00O99_A1079ContratoGestor_UsuarioCod ;
      private int[] H00O99_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00O99_n1446ContratoGestor_ContratadaAreaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV23WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV5Codigos ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV72GXV4 ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV17SDT_CheckList1 ;
      private GXWebForm Form ;
      private SdtContagemResultadoChckLstLog AV29Log ;
      private SdtMessages_Message AV50Message ;
      private SdtSDT_CheckList_Item AV18SDT_Item ;
      private SdtSDT_Parametros AV34Sdt_Parametros ;
      private wwpbaseobjects.SdtWWPContext AV25WWPContext ;
   }

   public class wp_check__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00O93 ;
          prmH00O93 = new Object[] {
          new Object[] {"@AV38Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00O94 ;
          prmH00O94 = new Object[] {
          new Object[] {"@Check_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51Check_Momento",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00O95 ;
          prmH00O95 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00O96 ;
          prmH00O96 = new Object[] {
          new Object[] {"@AV18SDT_Item__Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00O97 ;
          prmH00O97 = new Object[] {
          new Object[] {"@AV38Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00O98 ;
          prmH00O98 = new Object[] {
          new Object[] {"@AV38Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00O99 ;
          prmH00O99 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00O93", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T3.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_Demanda], T2.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV38Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O93,1,0,false,true )
             ,new CursorDef("H00O94", "SELECT TOP 1 [Check_AreaTrabalhoCod], [Check_Codigo], [Check_Momento], [Check_Nome], [Check_QdoNaoCmp] FROM [Check] WITH (NOLOCK) WHERE ([Check_AreaTrabalhoCod] = @Check_AreaTrabalhoCod) AND ([Check_Momento] = @AV51Check_Momento) ORDER BY [Check_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O94,1,0,true,true )
             ,new CursorDef("H00O95", "SELECT [Check_Codigo], [CheckList_Ativo], [CheckList_Codigo], [CheckList_Descricao], [CheckList_Obrigatorio], [CheckList_SeNaoCumpre], [CheckList_NaoCnfQdo], [CheckList_Impeditivo] FROM [CheckList] WITH (NOLOCK) WHERE ([Check_Codigo] = @Check_Codigo) AND ([CheckList_Ativo] = 1) ORDER BY [Check_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O95,100,0,false,false )
             ,new CursorDef("H00O96", "SELECT TOP 1 [CheckList_Codigo], [CheckList_NaoCnfCod] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @AV18SDT_Item__Codigo ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O96,1,0,false,true )
             ,new CursorDef("H00O97", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV38Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O97,100,0,true,false )
             ,new CursorDef("H00O98", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV38Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O98,1,0,true,true )
             ,new CursorDef("H00O99", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O99,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
