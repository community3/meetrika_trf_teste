/*
               File: PRC_SistemaUpdFromExcel
        Description: Sistema Upd From Excel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:31.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistemaupdfromexcel : GXProcedure
   {
      public prc_sistemaupdfromexcel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sistemaupdfromexcel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Arquivo ,
                           ref int aP1_Sistema_AreaTrabalhoCod )
      {
         this.AV9Arquivo = aP0_Arquivo;
         this.A135Sistema_AreaTrabalhoCod = aP1_Sistema_AreaTrabalhoCod;
         initialize();
         executePrivate();
         aP0_Arquivo=this.AV9Arquivo;
         aP1_Sistema_AreaTrabalhoCod=this.A135Sistema_AreaTrabalhoCod;
      }

      public int executeUdp( ref String aP0_Arquivo )
      {
         this.AV9Arquivo = aP0_Arquivo;
         this.A135Sistema_AreaTrabalhoCod = aP1_Sistema_AreaTrabalhoCod;
         initialize();
         executePrivate();
         aP0_Arquivo=this.AV9Arquivo;
         aP1_Sistema_AreaTrabalhoCod=this.A135Sistema_AreaTrabalhoCod;
         return A135Sistema_AreaTrabalhoCod ;
      }

      public void executeSubmit( ref String aP0_Arquivo ,
                                 ref int aP1_Sistema_AreaTrabalhoCod )
      {
         prc_sistemaupdfromexcel objprc_sistemaupdfromexcel;
         objprc_sistemaupdfromexcel = new prc_sistemaupdfromexcel();
         objprc_sistemaupdfromexcel.AV9Arquivo = aP0_Arquivo;
         objprc_sistemaupdfromexcel.A135Sistema_AreaTrabalhoCod = aP1_Sistema_AreaTrabalhoCod;
         objprc_sistemaupdfromexcel.context.SetSubmitInitialConfig(context);
         objprc_sistemaupdfromexcel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistemaupdfromexcel);
         aP0_Arquivo=this.AV9Arquivo;
         aP1_Sistema_AreaTrabalhoCod=this.A135Sistema_AreaTrabalhoCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistemaupdfromexcel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12ErrCod = AV13ExcelDocument.Open(AV9Arquivo);
         AV15Ln = 2;
         while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13ExcelDocument.get_Cells(AV15Ln, 1, 1, 1).Text)) )
         {
            AV10ContagemResultado_Demanda = AV13ExcelDocument.get_Cells(AV15Ln, 5, 1, 1).Text;
            /* Using cursor P003E2 */
            pr_default.execute(0, new Object[] {AV10ContagemResultado_Demanda});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A146Modulo_Codigo = P003E2_A146Modulo_Codigo[0];
               n146Modulo_Codigo = P003E2_n146Modulo_Codigo[0];
               A127Sistema_Codigo = P003E2_A127Sistema_Codigo[0];
               A456ContagemResultado_Codigo = P003E2_A456ContagemResultado_Codigo[0];
               A457ContagemResultado_Demanda = P003E2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P003E2_n457ContagemResultado_Demanda[0];
               A494ContagemResultado_Descricao = P003E2_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P003E2_n494ContagemResultado_Descricao[0];
               A484ContagemResultado_StatusDmn = P003E2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P003E2_n484ContagemResultado_StatusDmn[0];
               A489ContagemResultado_SistemaCod = P003E2_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P003E2_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P003E2_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P003E2_n509ContagemrResultado_SistemaSigla[0];
               A127Sistema_Codigo = P003E2_A127Sistema_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P003E2_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P003E2_n509ContagemrResultado_SistemaSigla[0];
               A494ContagemResultado_Descricao = StringUtil.Trim( AV13ExcelDocument.get_Cells(AV15Ln, 3, 1, 1).Text);
               n494ContagemResultado_Descricao = false;
               AV16PontosFinal = (decimal)(AV13ExcelDocument.get_Cells(AV15Ln, 9, 1, 1).Number);
               /* Using cursor P003E3 */
               pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A458ContagemResultado_PFBFS = P003E3_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P003E3_n458ContagemResultado_PFBFS[0];
                  A460ContagemResultado_PFBFM = P003E3_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P003E3_n460ContagemResultado_PFBFM[0];
                  A473ContagemResultado_DataCnt = P003E3_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P003E3_A511ContagemResultado_HoraCnt[0];
                  if ( ( A458ContagemResultado_PFBFS > Convert.ToDecimal( 0 )) )
                  {
                     AV11ContagemResultado_PFFinal = ((A458ContagemResultado_PFBFS<A460ContagemResultado_PFBFM) ? A458ContagemResultado_PFBFS : A460ContagemResultado_PFBFM);
                  }
                  else
                  {
                     AV11ContagemResultado_PFFinal = A460ContagemResultado_PFBFM;
                  }
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV11ContagemResultado_PFFinal == AV16PontosFinal )
               {
                  A484ContagemResultado_StatusDmn = "C";
                  n484ContagemResultado_StatusDmn = false;
               }
               AV17Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV8Sistema_Nome = StringUtil.Upper( StringUtil.Trim( AV13ExcelDocument.get_Cells(AV15Ln, 2, 1, 1).Text));
               AV14i = (short)(StringUtil.StringSearch( AV8Sistema_Nome, "-", 1)-1);
               AV19Sistema_Sigla = StringUtil.Substring( AV8Sistema_Nome, 1, AV14i);
               AV14i = (short)(AV14i+1);
               AV8Sistema_Nome = StringUtil.Substring( AV8Sistema_Nome, AV14i, StringUtil.Len( AV8Sistema_Nome));
               if ( StringUtil.StringSearch( AV8Sistema_Nome, StringUtil.Trim( A509ContagemrResultado_SistemaSigla), 1) == 0 )
               {
                  /* Optimized UPDATE. */
                  /* Using cursor P003E4 */
                  pr_default.execute(2, new Object[] {AV8Sistema_Nome, AV19Sistema_Sigla, AV17Sistema_Codigo, A135Sistema_AreaTrabalhoCod});
                  pr_default.close(2);
                  dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                  dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                  /* End optimized UPDATE. */
               }
               BatchSize = 100;
               pr_default.initializeBatch( 3, BatchSize, this, "Executebatchp003e5");
               /* Using cursor P003E5 */
               pr_default.addRecord(3, new Object[] {n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               if ( pr_default.recordCount(3) == pr_default.getBatchSize(3) )
               {
                  Executebatchp003e5( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(0);
            }
            if ( pr_default.getBatchSize(3) > 0 )
            {
               Executebatchp003e5( ) ;
            }
            pr_default.close(0);
            AV15Ln = (short)(AV15Ln+1);
         }
         AV12ErrCod = AV13ExcelDocument.Close();
         this.cleanup();
      }

      protected void Executebatchp003e5( )
      {
         /* Using cursor P003E5 */
         pr_default.executeBatch(3);
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_SistemaUpdFromExcel");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(3);
      }

      public override void initialize( )
      {
         AV13ExcelDocument = new ExcelDocumentI();
         AV10ContagemResultado_Demanda = "";
         scmdbuf = "";
         P003E2_A146Modulo_Codigo = new int[1] ;
         P003E2_n146Modulo_Codigo = new bool[] {false} ;
         P003E2_A127Sistema_Codigo = new int[1] ;
         P003E2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P003E2_A456ContagemResultado_Codigo = new int[1] ;
         P003E2_A457ContagemResultado_Demanda = new String[] {""} ;
         P003E2_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003E2_A494ContagemResultado_Descricao = new String[] {""} ;
         P003E2_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003E2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003E2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003E2_A489ContagemResultado_SistemaCod = new int[1] ;
         P003E2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003E2_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003E2_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A494ContagemResultado_Descricao = "";
         A484ContagemResultado_StatusDmn = "";
         A509ContagemrResultado_SistemaSigla = "";
         P003E3_A456ContagemResultado_Codigo = new int[1] ;
         P003E3_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003E3_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003E3_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003E3_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003E3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003E3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV8Sistema_Nome = "";
         AV19Sistema_Sigla = "";
         P003E5_A494ContagemResultado_Descricao = new String[] {""} ;
         P003E5_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003E5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003E5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003E5_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_sistemaupdfromexcel__default(),
            new Object[][] {
                new Object[] {
               P003E2_A146Modulo_Codigo, P003E2_n146Modulo_Codigo, P003E2_A127Sistema_Codigo, P003E2_A135Sistema_AreaTrabalhoCod, P003E2_A456ContagemResultado_Codigo, P003E2_A457ContagemResultado_Demanda, P003E2_n457ContagemResultado_Demanda, P003E2_A494ContagemResultado_Descricao, P003E2_n494ContagemResultado_Descricao, P003E2_A484ContagemResultado_StatusDmn,
               P003E2_n484ContagemResultado_StatusDmn, P003E2_A489ContagemResultado_SistemaCod, P003E2_n489ContagemResultado_SistemaCod, P003E2_A509ContagemrResultado_SistemaSigla, P003E2_n509ContagemrResultado_SistemaSigla
               }
               , new Object[] {
               P003E3_A456ContagemResultado_Codigo, P003E3_A458ContagemResultado_PFBFS, P003E3_n458ContagemResultado_PFBFS, P003E3_A460ContagemResultado_PFBFM, P003E3_n460ContagemResultado_PFBFM, P003E3_A473ContagemResultado_DataCnt, P003E3_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12ErrCod ;
      private short AV15Ln ;
      private short AV14i ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV17Sistema_Codigo ;
      private int BatchSize ;
      private decimal AV16PontosFinal ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal AV11ContagemResultado_PFFinal ;
      private String AV9Arquivo ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV19Sistema_Sigla ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n146Modulo_Codigo ;
      private bool n457ContagemResultado_Demanda ;
      private bool n494ContagemResultado_Descricao ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n460ContagemResultado_PFBFM ;
      private String AV10ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String AV8Sistema_Nome ;
      private ExcelDocumentI AV13ExcelDocument ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Arquivo ;
      private int aP1_Sistema_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P003E2_A146Modulo_Codigo ;
      private bool[] P003E2_n146Modulo_Codigo ;
      private int[] P003E2_A127Sistema_Codigo ;
      private int[] P003E2_A135Sistema_AreaTrabalhoCod ;
      private int[] P003E2_A456ContagemResultado_Codigo ;
      private String[] P003E2_A457ContagemResultado_Demanda ;
      private bool[] P003E2_n457ContagemResultado_Demanda ;
      private String[] P003E2_A494ContagemResultado_Descricao ;
      private bool[] P003E2_n494ContagemResultado_Descricao ;
      private String[] P003E2_A484ContagemResultado_StatusDmn ;
      private bool[] P003E2_n484ContagemResultado_StatusDmn ;
      private int[] P003E2_A489ContagemResultado_SistemaCod ;
      private bool[] P003E2_n489ContagemResultado_SistemaCod ;
      private String[] P003E2_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003E2_n509ContagemrResultado_SistemaSigla ;
      private int[] P003E3_A456ContagemResultado_Codigo ;
      private decimal[] P003E3_A458ContagemResultado_PFBFS ;
      private bool[] P003E3_n458ContagemResultado_PFBFS ;
      private decimal[] P003E3_A460ContagemResultado_PFBFM ;
      private bool[] P003E3_n460ContagemResultado_PFBFM ;
      private DateTime[] P003E3_A473ContagemResultado_DataCnt ;
      private String[] P003E3_A511ContagemResultado_HoraCnt ;
      private String[] P003E5_A494ContagemResultado_Descricao ;
      private bool[] P003E5_n494ContagemResultado_Descricao ;
      private String[] P003E5_A484ContagemResultado_StatusDmn ;
      private bool[] P003E5_n484ContagemResultado_StatusDmn ;
      private int[] P003E5_A456ContagemResultado_Codigo ;
   }

   public class prc_sistemaupdfromexcel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new BatchUpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003E2 ;
          prmP003E2 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP003E3 ;
          prmP003E3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003E4 ;
          prmP003E4 = new Object[] {
          new Object[] {"@AV8Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV19Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV17Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003E5 ;
          prmP003E5 = new Object[] {
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003E2", "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T4.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla FROM ((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Demanda] = @AV10ContagemResultado_Demanda ORDER BY T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003E2,1,0,true,false )
             ,new CursorDef("P003E3", "SELECT [ContagemResultado_Codigo], [ContagemResultado_PFBFS], [ContagemResultado_PFBFM], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003E3,100,0,false,false )
             ,new CursorDef("P003E4", "UPDATE [Sistema] SET [Sistema_Nome]=LTRIM(@AV8Sistema_Nome), [Sistema_Sigla]=RTRIM(@AV19Sistema_Sigla)  WHERE ([Sistema_Codigo] = @AV17Sistema_Codigo) AND ([Sistema_AreaTrabalhoCod] = @Sistema_AreaTrabalhoCod)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003E4)
             ,new CursorDef("P003E5", "UPDATE [ContagemResultado] SET [ContagemResultado_Descricao]=@ContagemResultado_Descricao, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003E5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 25) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
