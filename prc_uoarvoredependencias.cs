/*
               File: PRC_UOArvoreDependencias
        Description: Unidade Organizacional Arvore de Dependencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:42.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_uoarvoredependencias : GXProcedure
   {
      public prc_uoarvoredependencias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_uoarvoredependencias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UnidadeOrganizacional_Codigo ,
                           out String aP1_Arvore )
      {
         this.AV8UnidadeOrganizacional_Codigo = aP0_UnidadeOrganizacional_Codigo;
         this.AV9Arvore = "" ;
         initialize();
         executePrivate();
         aP1_Arvore=this.AV9Arvore;
      }

      public String executeUdp( int aP0_UnidadeOrganizacional_Codigo )
      {
         this.AV8UnidadeOrganizacional_Codigo = aP0_UnidadeOrganizacional_Codigo;
         this.AV9Arvore = "" ;
         initialize();
         executePrivate();
         aP1_Arvore=this.AV9Arvore;
         return AV9Arvore ;
      }

      public void executeSubmit( int aP0_UnidadeOrganizacional_Codigo ,
                                 out String aP1_Arvore )
      {
         prc_uoarvoredependencias objprc_uoarvoredependencias;
         objprc_uoarvoredependencias = new prc_uoarvoredependencias();
         objprc_uoarvoredependencias.AV8UnidadeOrganizacional_Codigo = aP0_UnidadeOrganizacional_Codigo;
         objprc_uoarvoredependencias.AV9Arvore = "" ;
         objprc_uoarvoredependencias.context.SetSubmitInitialConfig(context);
         objprc_uoarvoredependencias.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_uoarvoredependencias);
         aP1_Arvore=this.AV9Arvore;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_uoarvoredependencias)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Arvore = "";
         AV10UnidadeOrganizacional_Vinculada = AV8UnidadeOrganizacional_Codigo;
         while ( ! (0==AV10UnidadeOrganizacional_Vinculada) )
         {
            /* Using cursor P007I2 */
            pr_default.execute(0, new Object[] {AV10UnidadeOrganizacional_Vinculada});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A611UnidadeOrganizacional_Codigo = P007I2_A611UnidadeOrganizacional_Codigo[0];
               A612UnidadeOrganizacional_Nome = P007I2_A612UnidadeOrganizacional_Nome[0];
               A613UnidadeOrganizacional_Vinculada = P007I2_A613UnidadeOrganizacional_Vinculada[0];
               n613UnidadeOrganizacional_Vinculada = P007I2_n613UnidadeOrganizacional_Vinculada[0];
               AV9Arvore = AV9Arvore + StringUtil.Trim( A612UnidadeOrganizacional_Nome) + "<>";
               AV10UnidadeOrganizacional_Vinculada = A613UnidadeOrganizacional_Vinculada;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         AV9Arvore = StringUtil.Substring( AV9Arvore, 1, StringUtil.Len( AV9Arvore)-1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007I2_A611UnidadeOrganizacional_Codigo = new int[1] ;
         P007I2_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P007I2_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P007I2_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         A612UnidadeOrganizacional_Nome = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_uoarvoredependencias__default(),
            new Object[][] {
                new Object[] {
               P007I2_A611UnidadeOrganizacional_Codigo, P007I2_A612UnidadeOrganizacional_Nome, P007I2_A613UnidadeOrganizacional_Vinculada, P007I2_n613UnidadeOrganizacional_Vinculada
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8UnidadeOrganizacional_Codigo ;
      private int AV10UnidadeOrganizacional_Vinculada ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private String AV9Arvore ;
      private String scmdbuf ;
      private String A612UnidadeOrganizacional_Nome ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P007I2_A611UnidadeOrganizacional_Codigo ;
      private String[] P007I2_A612UnidadeOrganizacional_Nome ;
      private int[] P007I2_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P007I2_n613UnidadeOrganizacional_Vinculada ;
      private String aP1_Arvore ;
   }

   public class prc_uoarvoredependencias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007I2 ;
          prmP007I2 = new Object[] {
          new Object[] {"@AV10UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007I2", "SELECT TOP 1 [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Vinculada] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @AV10UnidadeOrganizacional_Vinculada ORDER BY [UnidadeOrganizacional_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007I2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
