/*
               File: PRC_VinculaDemandaItens
        Description: Vincula Demanda Itens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:16.22
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_vinculademandaitens : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_vinculademandaitens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_vinculademandaitens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aprc_vinculademandaitens objaprc_vinculademandaitens;
         objaprc_vinculademandaitens = new aprc_vinculademandaitens();
         objaprc_vinculademandaitens.context.SetSubmitInitialConfig(context);
         objaprc_vinculademandaitens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_vinculademandaitens);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_vinculademandaitens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005K2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A773ContagemResultadoItem_ContagemCod = P005K2_A773ContagemResultadoItem_ContagemCod[0];
            n773ContagemResultadoItem_ContagemCod = P005K2_n773ContagemResultadoItem_ContagemCod[0];
            A809ContagemResultadoItem_DemandaCorrigida = P005K2_A809ContagemResultadoItem_DemandaCorrigida[0];
            n809ContagemResultadoItem_DemandaCorrigida = P005K2_n809ContagemResultadoItem_DemandaCorrigida[0];
            A793ContagemResultadoItem_Demanda = P005K2_A793ContagemResultadoItem_Demanda[0];
            AV9ContagemResultadoItem_Demanda = StringUtil.Trim( A793ContagemResultadoItem_Demanda);
            if ( P005K2_n809ContagemResultadoItem_DemandaCorrigida[0] )
            {
               AV11ContagemResultadoItem_DemandaCorrigida = StringUtil.Trim( A793ContagemResultadoItem_Demanda);
            }
            else
            {
               AV11ContagemResultadoItem_DemandaCorrigida = StringUtil.Trim( A809ContagemResultadoItem_DemandaCorrigida);
            }
            /* Execute user subroutine: 'CONTAGEMCOD' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         context.wjLoc = formatLink("wp_estatisticassistemas.aspx") ;
         context.wjLocDisableFrm = 1;
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CONTAGEMCOD' Routine */
         AV15GXLvl21 = 0;
         /* Using cursor P005K3 */
         pr_default.execute(1, new Object[] {AV11ContagemResultadoItem_DemandaCorrigida});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A457ContagemResultado_Demanda = P005K3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P005K3_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P005K3_A456ContagemResultado_Codigo[0];
            AV15GXLvl21 = 1;
            AV8ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            /* Execute user subroutine: 'ATUALIZAITEM' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV15GXLvl21 == 0 )
         {
            if ( StringUtil.StrCmp(AV9ContagemResultadoItem_Demanda, AV11ContagemResultadoItem_DemandaCorrigida) != 0 )
            {
               /* Using cursor P005K4 */
               pr_default.execute(2, new Object[] {AV9ContagemResultadoItem_Demanda});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A457ContagemResultado_Demanda = P005K4_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P005K4_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P005K4_A456ContagemResultado_Codigo[0];
                  AV8ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                  /* Execute user subroutine: 'ATUALIZAITEM' */
                  S123 ();
                  if ( returnInSub )
                  {
                     pr_default.close(2);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
            }
         }
      }

      protected void S123( )
      {
         /* 'ATUALIZAITEM' Routine */
         /* Using cursor P005K5 */
         pr_default.execute(3, new Object[] {AV9ContagemResultadoItem_Demanda});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A793ContagemResultadoItem_Demanda = P005K5_A793ContagemResultadoItem_Demanda[0];
            A773ContagemResultadoItem_ContagemCod = P005K5_A773ContagemResultadoItem_ContagemCod[0];
            n773ContagemResultadoItem_ContagemCod = P005K5_n773ContagemResultadoItem_ContagemCod[0];
            A772ContagemResultadoItem_Codigo = P005K5_A772ContagemResultadoItem_Codigo[0];
            A773ContagemResultadoItem_ContagemCod = AV8ContagemResultado_Codigo;
            n773ContagemResultadoItem_ContagemCod = false;
            BatchSize = 200;
            pr_default.initializeBatch( 4, BatchSize, this, "Executebatchp005k6");
            /* Using cursor P005K6 */
            pr_default.addRecord(4, new Object[] {n773ContagemResultadoItem_ContagemCod, A773ContagemResultadoItem_ContagemCod, A772ContagemResultadoItem_Codigo});
            if ( pr_default.recordCount(4) == pr_default.getBatchSize(4) )
            {
               Executebatchp005k6( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoItem") ;
            pr_default.readNext(3);
         }
         if ( pr_default.getBatchSize(4) > 0 )
         {
            Executebatchp005k6( ) ;
         }
         pr_default.close(3);
         context.CommitDataStores( "PRC_VinculaDemandaItens");
      }

      protected void Executebatchp005k6( )
      {
         /* Using cursor P005K6 */
         pr_default.executeBatch(4);
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_VinculaDemandaItens");
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(4);
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P005K2_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         P005K2_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         P005K2_A809ContagemResultadoItem_DemandaCorrigida = new String[] {""} ;
         P005K2_n809ContagemResultadoItem_DemandaCorrigida = new bool[] {false} ;
         P005K2_A793ContagemResultadoItem_Demanda = new String[] {""} ;
         A809ContagemResultadoItem_DemandaCorrigida = "";
         A793ContagemResultadoItem_Demanda = "";
         AV9ContagemResultadoItem_Demanda = "";
         AV11ContagemResultadoItem_DemandaCorrigida = "";
         P005K3_A457ContagemResultado_Demanda = new String[] {""} ;
         P005K3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P005K3_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         P005K4_A457ContagemResultado_Demanda = new String[] {""} ;
         P005K4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P005K4_A456ContagemResultado_Codigo = new int[1] ;
         P005K5_A793ContagemResultadoItem_Demanda = new String[] {""} ;
         P005K5_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         P005K5_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         P005K5_A772ContagemResultadoItem_Codigo = new int[1] ;
         P005K6_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         P005K6_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         P005K6_A772ContagemResultadoItem_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_vinculademandaitens__default(),
            new Object[][] {
                new Object[] {
               P005K2_A773ContagemResultadoItem_ContagemCod, P005K2_n773ContagemResultadoItem_ContagemCod, P005K2_A809ContagemResultadoItem_DemandaCorrigida, P005K2_n809ContagemResultadoItem_DemandaCorrigida, P005K2_A793ContagemResultadoItem_Demanda
               }
               , new Object[] {
               P005K3_A457ContagemResultado_Demanda, P005K3_n457ContagemResultado_Demanda, P005K3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P005K4_A457ContagemResultado_Demanda, P005K4_n457ContagemResultado_Demanda, P005K4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P005K5_A793ContagemResultadoItem_Demanda, P005K5_A773ContagemResultadoItem_ContagemCod, P005K5_n773ContagemResultadoItem_ContagemCod, P005K5_A772ContagemResultadoItem_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV15GXLvl21 ;
      private int A773ContagemResultadoItem_ContagemCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV8ContagemResultado_Codigo ;
      private int A772ContagemResultadoItem_Codigo ;
      private int BatchSize ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private bool entryPointCalled ;
      private bool n773ContagemResultadoItem_ContagemCod ;
      private bool n809ContagemResultadoItem_DemandaCorrigida ;
      private bool returnInSub ;
      private bool n457ContagemResultado_Demanda ;
      private String A809ContagemResultadoItem_DemandaCorrigida ;
      private String A793ContagemResultadoItem_Demanda ;
      private String AV9ContagemResultadoItem_Demanda ;
      private String AV11ContagemResultadoItem_DemandaCorrigida ;
      private String A457ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P005K2_A773ContagemResultadoItem_ContagemCod ;
      private bool[] P005K2_n773ContagemResultadoItem_ContagemCod ;
      private String[] P005K2_A809ContagemResultadoItem_DemandaCorrigida ;
      private bool[] P005K2_n809ContagemResultadoItem_DemandaCorrigida ;
      private String[] P005K2_A793ContagemResultadoItem_Demanda ;
      private String[] P005K3_A457ContagemResultado_Demanda ;
      private bool[] P005K3_n457ContagemResultado_Demanda ;
      private int[] P005K3_A456ContagemResultado_Codigo ;
      private String[] P005K4_A457ContagemResultado_Demanda ;
      private bool[] P005K4_n457ContagemResultado_Demanda ;
      private int[] P005K4_A456ContagemResultado_Codigo ;
      private String[] P005K5_A793ContagemResultadoItem_Demanda ;
      private int[] P005K5_A773ContagemResultadoItem_ContagemCod ;
      private bool[] P005K5_n773ContagemResultadoItem_ContagemCod ;
      private int[] P005K5_A772ContagemResultadoItem_Codigo ;
      private int[] P005K6_A773ContagemResultadoItem_ContagemCod ;
      private bool[] P005K6_n773ContagemResultadoItem_ContagemCod ;
      private int[] P005K6_A772ContagemResultadoItem_Codigo ;
   }

   public class aprc_vinculademandaitens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new BatchUpdateCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005K2 ;
          prmP005K2 = new Object[] {
          } ;
          Object[] prmP005K3 ;
          prmP005K3 = new Object[] {
          new Object[] {"@AV11ContagemResultadoItem_DemandaCorrigida",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP005K4 ;
          prmP005K4 = new Object[] {
          new Object[] {"@AV9ContagemResultadoItem_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP005K5 ;
          prmP005K5 = new Object[] {
          new Object[] {"@AV9ContagemResultadoItem_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP005K6 ;
          prmP005K6 = new Object[] {
          new Object[] {"@ContagemResultadoItem_ContagemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005K2", "SELECT DISTINCT NULL AS [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_DemandaCorrigida], [ContagemResultadoItem_Demanda] FROM ( SELECT TOP(100) PERCENT [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_DemandaCorrigida], [ContagemResultadoItem_Demanda] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] IS NULL or [ContagemResultadoItem_ContagemCod] = 0 ORDER BY [ContagemResultadoItem_Demanda]) DistinctT ORDER BY [ContagemResultadoItem_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005K2,100,0,true,false )
             ,new CursorDef("P005K3", "SELECT TOP 1 [ContagemResultado_Demanda], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Demanda] = @AV11ContagemResultadoItem_DemandaCorrigida ORDER BY [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005K3,1,0,true,true )
             ,new CursorDef("P005K4", "SELECT TOP 1 [ContagemResultado_Demanda], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Demanda] = @AV9ContagemResultadoItem_Demanda ORDER BY [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005K4,1,0,true,true )
             ,new CursorDef("P005K5", "SELECT [ContagemResultadoItem_Demanda], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Codigo] FROM [ContagemResultadoItem] WITH (UPDLOCK) WHERE [ContagemResultadoItem_Demanda] = @AV9ContagemResultadoItem_Demanda ORDER BY [ContagemResultadoItem_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005K5,1,0,true,false )
             ,new CursorDef("P005K6", "UPDATE [ContagemResultadoItem] SET [ContagemResultadoItem_ContagemCod]=@ContagemResultadoItem_ContagemCod  WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005K6)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
