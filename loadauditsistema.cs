/*
               File: LoadAuditSistema
        Description: Load Audit Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:3:55.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditsistema : GXProcedure
   {
      public loadauditsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Sistema_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Sistema_Codigo = aP2_Sistema_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Sistema_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditsistema objloadauditsistema;
         objloadauditsistema = new loadauditsistema();
         objloadauditsistema.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditsistema.AV10AuditingObject = aP1_AuditingObject;
         objloadauditsistema.AV16Sistema_Codigo = aP2_Sistema_Codigo;
         objloadauditsistema.AV14ActualMode = aP3_ActualMode;
         objloadauditsistema.context.SetSubmitInitialConfig(context);
         objloadauditsistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditsistema);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditsistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A72 */
         pr_default.execute(0, new Object[] {AV16Sistema_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A416Sistema_Nome = P00A72_A416Sistema_Nome[0];
            A128Sistema_Descricao = P00A72_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00A72_n128Sistema_Descricao[0];
            A129Sistema_Sigla = P00A72_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = P00A72_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00A72_n513Sistema_Coordenacao[0];
            A135Sistema_AreaTrabalhoCod = P00A72_A135Sistema_AreaTrabalhoCod[0];
            A136Sistema_AreaTrabalhoDes = P00A72_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = P00A72_n136Sistema_AreaTrabalhoDes[0];
            A351AmbienteTecnologico_Codigo = P00A72_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00A72_n351AmbienteTecnologico_Codigo[0];
            A352AmbienteTecnologico_Descricao = P00A72_A352AmbienteTecnologico_Descricao[0];
            A137Metodologia_Codigo = P00A72_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00A72_n137Metodologia_Codigo[0];
            A138Metodologia_Descricao = P00A72_A138Metodologia_Descricao[0];
            A700Sistema_Tecnica = P00A72_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00A72_n700Sistema_Tecnica[0];
            A687Sistema_Custo = P00A72_A687Sistema_Custo[0];
            n687Sistema_Custo = P00A72_n687Sistema_Custo[0];
            A688Sistema_Prazo = P00A72_A688Sistema_Prazo[0];
            n688Sistema_Prazo = P00A72_n688Sistema_Prazo[0];
            A689Sistema_Esforco = P00A72_A689Sistema_Esforco[0];
            n689Sistema_Esforco = P00A72_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = P00A72_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = P00A72_n1401Sistema_ImpData[0];
            A1399Sistema_ImpUserCod = P00A72_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = P00A72_n1399Sistema_ImpUserCod[0];
            A1402Sistema_ImpUserPesCod = P00A72_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = P00A72_n1402Sistema_ImpUserPesCod[0];
            A1403Sistema_ImpUserPesNom = P00A72_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = P00A72_n1403Sistema_ImpUserPesNom[0];
            A1831Sistema_Responsavel = P00A72_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = P00A72_n1831Sistema_Responsavel[0];
            A1859SistemaVersao_Codigo = P00A72_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00A72_n1859SistemaVersao_Codigo[0];
            A1860SistemaVersao_Id = P00A72_A1860SistemaVersao_Id[0];
            A130Sistema_Ativo = P00A72_A130Sistema_Ativo[0];
            A2109Sistema_Repositorio = P00A72_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = P00A72_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = P00A72_A2130Sistema_Ultimo[0];
            A2161Sistema_GpoObjCtrlCod = P00A72_A2161Sistema_GpoObjCtrlCod[0];
            n2161Sistema_GpoObjCtrlCod = P00A72_n2161Sistema_GpoObjCtrlCod[0];
            A699Sistema_Tipo = P00A72_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00A72_n699Sistema_Tipo[0];
            A686Sistema_FatorAjuste = P00A72_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = P00A72_n686Sistema_FatorAjuste[0];
            A127Sistema_Codigo = P00A72_A127Sistema_Codigo[0];
            A136Sistema_AreaTrabalhoDes = P00A72_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = P00A72_n136Sistema_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = P00A72_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = P00A72_A138Metodologia_Descricao[0];
            A1402Sistema_ImpUserPesCod = P00A72_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = P00A72_n1402Sistema_ImpUserPesCod[0];
            A1403Sistema_ImpUserPesNom = P00A72_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = P00A72_n1403Sistema_ImpUserPesNom[0];
            A1860SistemaVersao_Id = P00A72_A1860SistemaVersao_Id[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
            {
               A707Sistema_TipoSigla = "PD";
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
               {
                  A707Sistema_TipoSigla = "PM";
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                  {
                     A707Sistema_TipoSigla = "PC";
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                     {
                        A707Sistema_TipoSigla = "A";
                     }
                     else
                     {
                        A707Sistema_TipoSigla = "";
                     }
                  }
               }
            }
            GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
            GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
            new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
            A2161Sistema_GpoObjCtrlCod = GXt_int3;
            A2162Sistema_GpoObjCtrlRsp = GXt_int2;
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Sistema";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A416Sistema_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A128Sistema_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A129Sistema_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Coordenacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Coordena��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A513Sistema_Coordenacao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. �rea Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_AreaTrabalhoDes";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A136Sistema_AreaTrabalhoDes;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AmbienteTecnologico_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Amb. T�c.";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AmbienteTecnologico_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ambiente Tecnol�gico";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A352AmbienteTecnologico_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Metodologia_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Met.";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Metodologia_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Metodologia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A138Metodologia_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_PF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PF.B";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A395Sistema_PF, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_PFA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PF.A";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A690Sistema_PFA, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A699Sistema_Tipo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_TipoSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A707Sistema_TipoSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Tecnica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "T�cnica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A700Sistema_Tecnica;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_FatorAjuste";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fator de Ajuste";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A686Sistema_FatorAjuste, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Custo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Custo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A687Sistema_Custo, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Prazo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Esforco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Esforco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpData";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Importado em";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpUserCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Importado por";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1399Sistema_ImpUserCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpUserPesCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpUserPesNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Importado por";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1403Sistema_ImpUserPesNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Responsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_GpoObjCtrlCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Gpo. Obj. de Controle";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_GpoObjCtrlRsp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "SistemaVersao_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vers�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "SistemaVersao_Id";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vers�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1860SistemaVersao_Id;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A130Sistema_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Repositorio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Reposit�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2109Sistema_Repositorio;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Ultimo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema_Ultimo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Using cursor P00A73 */
            pr_default.execute(1, new Object[] {A127Sistema_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A2128SistemaTecnologiaLinha = P00A73_A2128SistemaTecnologiaLinha[0];
               A131Tecnologia_Codigo = P00A73_A131Tecnologia_Codigo[0];
               A132Tecnologia_Nome = P00A73_A132Tecnologia_Nome[0];
               A355Tecnologia_TipoTecnologia = P00A73_A355Tecnologia_TipoTecnologia[0];
               n355Tecnologia_TipoTecnologia = P00A73_n355Tecnologia_TipoTecnologia[0];
               A132Tecnologia_Nome = P00A73_A132Tecnologia_Nome[0];
               A355Tecnologia_TipoTecnologia = P00A73_A355Tecnologia_TipoTecnologia[0];
               n355Tecnologia_TipoTecnologia = P00A73_n355Tecnologia_TipoTecnologia[0];
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "SistemaTecnologia";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "SistemaTecnologiaLinha";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2128SistemaTecnologiaLinha), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Tecnologia_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tecnologia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Tecnologia_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tecnologia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A132Tecnologia_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Tecnologia_TipoTecnologia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A355Tecnologia_TipoTecnologia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A75 */
         pr_default.execute(2, new Object[] {AV16Sistema_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A416Sistema_Nome = P00A75_A416Sistema_Nome[0];
            A128Sistema_Descricao = P00A75_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00A75_n128Sistema_Descricao[0];
            A129Sistema_Sigla = P00A75_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = P00A75_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00A75_n513Sistema_Coordenacao[0];
            A135Sistema_AreaTrabalhoCod = P00A75_A135Sistema_AreaTrabalhoCod[0];
            A136Sistema_AreaTrabalhoDes = P00A75_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = P00A75_n136Sistema_AreaTrabalhoDes[0];
            A351AmbienteTecnologico_Codigo = P00A75_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00A75_n351AmbienteTecnologico_Codigo[0];
            A352AmbienteTecnologico_Descricao = P00A75_A352AmbienteTecnologico_Descricao[0];
            A137Metodologia_Codigo = P00A75_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00A75_n137Metodologia_Codigo[0];
            A138Metodologia_Descricao = P00A75_A138Metodologia_Descricao[0];
            A700Sistema_Tecnica = P00A75_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00A75_n700Sistema_Tecnica[0];
            A687Sistema_Custo = P00A75_A687Sistema_Custo[0];
            n687Sistema_Custo = P00A75_n687Sistema_Custo[0];
            A688Sistema_Prazo = P00A75_A688Sistema_Prazo[0];
            n688Sistema_Prazo = P00A75_n688Sistema_Prazo[0];
            A689Sistema_Esforco = P00A75_A689Sistema_Esforco[0];
            n689Sistema_Esforco = P00A75_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = P00A75_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = P00A75_n1401Sistema_ImpData[0];
            A1399Sistema_ImpUserCod = P00A75_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = P00A75_n1399Sistema_ImpUserCod[0];
            A1402Sistema_ImpUserPesCod = P00A75_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = P00A75_n1402Sistema_ImpUserPesCod[0];
            A1403Sistema_ImpUserPesNom = P00A75_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = P00A75_n1403Sistema_ImpUserPesNom[0];
            A1831Sistema_Responsavel = P00A75_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = P00A75_n1831Sistema_Responsavel[0];
            A1859SistemaVersao_Codigo = P00A75_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00A75_n1859SistemaVersao_Codigo[0];
            A1860SistemaVersao_Id = P00A75_A1860SistemaVersao_Id[0];
            A130Sistema_Ativo = P00A75_A130Sistema_Ativo[0];
            A2109Sistema_Repositorio = P00A75_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = P00A75_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = P00A75_A2130Sistema_Ultimo[0];
            A40000GXC1 = P00A75_A40000GXC1[0];
            n40000GXC1 = P00A75_n40000GXC1[0];
            A2161Sistema_GpoObjCtrlCod = P00A75_A2161Sistema_GpoObjCtrlCod[0];
            n2161Sistema_GpoObjCtrlCod = P00A75_n2161Sistema_GpoObjCtrlCod[0];
            A699Sistema_Tipo = P00A75_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00A75_n699Sistema_Tipo[0];
            A686Sistema_FatorAjuste = P00A75_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = P00A75_n686Sistema_FatorAjuste[0];
            A127Sistema_Codigo = P00A75_A127Sistema_Codigo[0];
            A136Sistema_AreaTrabalhoDes = P00A75_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = P00A75_n136Sistema_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = P00A75_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = P00A75_A138Metodologia_Descricao[0];
            A1402Sistema_ImpUserPesCod = P00A75_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = P00A75_n1402Sistema_ImpUserPesCod[0];
            A1403Sistema_ImpUserPesNom = P00A75_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = P00A75_n1403Sistema_ImpUserPesNom[0];
            A1860SistemaVersao_Id = P00A75_A1860SistemaVersao_Id[0];
            A40000GXC1 = P00A75_A40000GXC1[0];
            n40000GXC1 = P00A75_n40000GXC1[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
            {
               A707Sistema_TipoSigla = "PD";
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
               {
                  A707Sistema_TipoSigla = "PM";
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                  {
                     A707Sistema_TipoSigla = "PC";
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                     {
                        A707Sistema_TipoSigla = "A";
                     }
                     else
                     {
                        A707Sistema_TipoSigla = "";
                     }
                  }
               }
            }
            GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
            GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
            new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
            A2161Sistema_GpoObjCtrlCod = GXt_int3;
            A2162Sistema_GpoObjCtrlRsp = GXt_int2;
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Sistema";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A416Sistema_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A128Sistema_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A129Sistema_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Coordenacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Coordena��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A513Sistema_Coordenacao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�d. �rea Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_AreaTrabalhoDes";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A136Sistema_AreaTrabalhoDes;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AmbienteTecnologico_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Amb. T�c.";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AmbienteTecnologico_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ambiente Tecnol�gico";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A352AmbienteTecnologico_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Metodologia_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Met.";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Metodologia_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Metodologia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A138Metodologia_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_PF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PF.B";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A395Sistema_PF, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_PFA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PF.A";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A690Sistema_PFA, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A699Sistema_Tipo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_TipoSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A707Sistema_TipoSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Tecnica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "T�cnica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A700Sistema_Tecnica;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_FatorAjuste";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fator de Ajuste";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A686Sistema_FatorAjuste, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Custo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Custo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A687Sistema_Custo, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Prazo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Esforco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Esforco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpData";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Importado em";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpUserCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Importado por";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1399Sistema_ImpUserCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpUserPesCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_ImpUserPesNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Importado por";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1403Sistema_ImpUserPesNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Responsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_GpoObjCtrlCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Gpo. Obj. de Controle";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_GpoObjCtrlRsp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "SistemaVersao_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vers�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "SistemaVersao_Id";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vers�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1860SistemaVersao_Id;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A130Sistema_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Repositorio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Reposit�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2109Sistema_Repositorio;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Sistema_Ultimo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema_Ultimo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               /* Using cursor P00A76 */
               pr_default.execute(3, new Object[] {A127Sistema_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A2128SistemaTecnologiaLinha = P00A76_A2128SistemaTecnologiaLinha[0];
                  A131Tecnologia_Codigo = P00A76_A131Tecnologia_Codigo[0];
                  A132Tecnologia_Nome = P00A76_A132Tecnologia_Nome[0];
                  A355Tecnologia_TipoTecnologia = P00A76_A355Tecnologia_TipoTecnologia[0];
                  n355Tecnologia_TipoTecnologia = P00A76_n355Tecnologia_TipoTecnologia[0];
                  A132Tecnologia_Nome = P00A76_A132Tecnologia_Nome[0];
                  A355Tecnologia_TipoTecnologia = P00A76_A355Tecnologia_TipoTecnologia[0];
                  n355Tecnologia_TipoTecnologia = P00A76_n355Tecnologia_TipoTecnologia[0];
                  AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                  AV11AuditingObjectRecordItem.gxTpr_Tablename = "SistemaTecnologia";
                  AV11AuditingObjectRecordItem.gxTpr_Mode = "INS";
                  AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "SistemaTecnologiaLinha";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2128SistemaTecnologiaLinha), 4, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Tecnologia_Codigo";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tecnologia";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Tecnologia_Nome";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tecnologia";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A132Tecnologia_Nome;
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Tecnologia_TipoTecnologia";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A355Tecnologia_TipoTecnologia;
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV24CountUpdatedSistemaTecnologiaLinha = 0;
               AV33GXV1 = 1;
               while ( AV33GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV33GXV1));
                  if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "Sistema") == 0 )
                  {
                     while ( (pr_default.getStatus(2) != 101) && ( P00A75_A127Sistema_Codigo[0] == A127Sistema_Codigo ) )
                     {
                        A416Sistema_Nome = P00A75_A416Sistema_Nome[0];
                        A128Sistema_Descricao = P00A75_A128Sistema_Descricao[0];
                        n128Sistema_Descricao = P00A75_n128Sistema_Descricao[0];
                        A129Sistema_Sigla = P00A75_A129Sistema_Sigla[0];
                        A513Sistema_Coordenacao = P00A75_A513Sistema_Coordenacao[0];
                        n513Sistema_Coordenacao = P00A75_n513Sistema_Coordenacao[0];
                        A135Sistema_AreaTrabalhoCod = P00A75_A135Sistema_AreaTrabalhoCod[0];
                        A136Sistema_AreaTrabalhoDes = P00A75_A136Sistema_AreaTrabalhoDes[0];
                        n136Sistema_AreaTrabalhoDes = P00A75_n136Sistema_AreaTrabalhoDes[0];
                        A351AmbienteTecnologico_Codigo = P00A75_A351AmbienteTecnologico_Codigo[0];
                        n351AmbienteTecnologico_Codigo = P00A75_n351AmbienteTecnologico_Codigo[0];
                        A352AmbienteTecnologico_Descricao = P00A75_A352AmbienteTecnologico_Descricao[0];
                        A137Metodologia_Codigo = P00A75_A137Metodologia_Codigo[0];
                        n137Metodologia_Codigo = P00A75_n137Metodologia_Codigo[0];
                        A138Metodologia_Descricao = P00A75_A138Metodologia_Descricao[0];
                        A700Sistema_Tecnica = P00A75_A700Sistema_Tecnica[0];
                        n700Sistema_Tecnica = P00A75_n700Sistema_Tecnica[0];
                        A687Sistema_Custo = P00A75_A687Sistema_Custo[0];
                        n687Sistema_Custo = P00A75_n687Sistema_Custo[0];
                        A688Sistema_Prazo = P00A75_A688Sistema_Prazo[0];
                        n688Sistema_Prazo = P00A75_n688Sistema_Prazo[0];
                        A689Sistema_Esforco = P00A75_A689Sistema_Esforco[0];
                        n689Sistema_Esforco = P00A75_n689Sistema_Esforco[0];
                        A1401Sistema_ImpData = P00A75_A1401Sistema_ImpData[0];
                        n1401Sistema_ImpData = P00A75_n1401Sistema_ImpData[0];
                        A1399Sistema_ImpUserCod = P00A75_A1399Sistema_ImpUserCod[0];
                        n1399Sistema_ImpUserCod = P00A75_n1399Sistema_ImpUserCod[0];
                        A1402Sistema_ImpUserPesCod = P00A75_A1402Sistema_ImpUserPesCod[0];
                        n1402Sistema_ImpUserPesCod = P00A75_n1402Sistema_ImpUserPesCod[0];
                        A1403Sistema_ImpUserPesNom = P00A75_A1403Sistema_ImpUserPesNom[0];
                        n1403Sistema_ImpUserPesNom = P00A75_n1403Sistema_ImpUserPesNom[0];
                        A1831Sistema_Responsavel = P00A75_A1831Sistema_Responsavel[0];
                        n1831Sistema_Responsavel = P00A75_n1831Sistema_Responsavel[0];
                        A1859SistemaVersao_Codigo = P00A75_A1859SistemaVersao_Codigo[0];
                        n1859SistemaVersao_Codigo = P00A75_n1859SistemaVersao_Codigo[0];
                        A1860SistemaVersao_Id = P00A75_A1860SistemaVersao_Id[0];
                        A130Sistema_Ativo = P00A75_A130Sistema_Ativo[0];
                        A2109Sistema_Repositorio = P00A75_A2109Sistema_Repositorio[0];
                        n2109Sistema_Repositorio = P00A75_n2109Sistema_Repositorio[0];
                        A2130Sistema_Ultimo = P00A75_A2130Sistema_Ultimo[0];
                        A2161Sistema_GpoObjCtrlCod = P00A75_A2161Sistema_GpoObjCtrlCod[0];
                        n2161Sistema_GpoObjCtrlCod = P00A75_n2161Sistema_GpoObjCtrlCod[0];
                        A699Sistema_Tipo = P00A75_A699Sistema_Tipo[0];
                        n699Sistema_Tipo = P00A75_n699Sistema_Tipo[0];
                        A686Sistema_FatorAjuste = P00A75_A686Sistema_FatorAjuste[0];
                        n686Sistema_FatorAjuste = P00A75_n686Sistema_FatorAjuste[0];
                        A136Sistema_AreaTrabalhoDes = P00A75_A136Sistema_AreaTrabalhoDes[0];
                        n136Sistema_AreaTrabalhoDes = P00A75_n136Sistema_AreaTrabalhoDes[0];
                        A352AmbienteTecnologico_Descricao = P00A75_A352AmbienteTecnologico_Descricao[0];
                        A138Metodologia_Descricao = P00A75_A138Metodologia_Descricao[0];
                        A1402Sistema_ImpUserPesCod = P00A75_A1402Sistema_ImpUserPesCod[0];
                        n1402Sistema_ImpUserPesCod = P00A75_n1402Sistema_ImpUserPesCod[0];
                        A1403Sistema_ImpUserPesNom = P00A75_A1403Sistema_ImpUserPesNom[0];
                        n1403Sistema_ImpUserPesNom = P00A75_n1403Sistema_ImpUserPesNom[0];
                        A1860SistemaVersao_Id = P00A75_A1860SistemaVersao_Id[0];
                        GXt_decimal1 = A395Sistema_PF;
                        new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
                        A395Sistema_PF = GXt_decimal1;
                        A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
                        if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
                        {
                           A707Sistema_TipoSigla = "PD";
                        }
                        else
                        {
                           if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
                           {
                              A707Sistema_TipoSigla = "PM";
                           }
                           else
                           {
                              if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                              {
                                 A707Sistema_TipoSigla = "PC";
                              }
                              else
                              {
                                 if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                                 {
                                    A707Sistema_TipoSigla = "A";
                                 }
                                 else
                                 {
                                    A707Sistema_TipoSigla = "";
                                 }
                              }
                           }
                        }
                        GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
                        GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
                        new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
                        A2161Sistema_GpoObjCtrlCod = GXt_int3;
                        A2162Sistema_GpoObjCtrlRsp = GXt_int2;
                        AV35GXV2 = 1;
                        while ( AV35GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                        {
                           AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV35GXV2));
                           if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Nome") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A416Sistema_Nome;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Descricao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A128Sistema_Descricao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Sigla") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A129Sistema_Sigla;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Coordenacao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A513Sistema_Coordenacao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_AreaTrabalhoCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_AreaTrabalhoDes") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A136Sistema_AreaTrabalhoDes;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AmbienteTecnologico_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AmbienteTecnologico_Descricao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A352AmbienteTecnologico_Descricao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Metodologia_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Metodologia_Descricao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A138Metodologia_Descricao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_PF") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A395Sistema_PF, 14, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_PFA") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A690Sistema_PFA, 14, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Tipo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A699Sistema_Tipo;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_TipoSigla") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A707Sistema_TipoSigla;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Tecnica") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A700Sistema_Tecnica;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_FatorAjuste") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A686Sistema_FatorAjuste, 6, 2);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Custo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A687Sistema_Custo, 18, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Prazo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Esforco") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_ImpData") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " ");
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_ImpUserCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1399Sistema_ImpUserCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_ImpUserPesCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_ImpUserPesNom") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1403Sistema_ImpUserPesNom;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Responsavel") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_GpoObjCtrlCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_GpoObjCtrlRsp") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "SistemaVersao_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "SistemaVersao_Id") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1860SistemaVersao_Id;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Ativo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A130Sistema_Ativo);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Repositorio") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2109Sistema_Repositorio;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Sistema_Ultimo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0);
                           }
                           AV35GXV2 = (int)(AV35GXV2+1);
                        }
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "SistemaTecnologia") == 0 )
                  {
                     AV20CountKeyAttributes = 0;
                     AV36GXV3 = 1;
                     while ( AV36GXV3 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV36GXV3));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "SistemaTecnologiaLinha") == 0 )
                        {
                           AV25KeySistemaTecnologiaLinha = AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue;
                           AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                           if ( AV20CountKeyAttributes == 1 )
                           {
                              if (true) break;
                           }
                        }
                        AV36GXV3 = (int)(AV36GXV3+1);
                     }
                     AV37GXLvl767 = 0;
                     /* Using cursor P00A77 */
                     pr_default.execute(4, new Object[] {A127Sistema_Codigo});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A2128SistemaTecnologiaLinha = P00A77_A2128SistemaTecnologiaLinha[0];
                        A131Tecnologia_Codigo = P00A77_A131Tecnologia_Codigo[0];
                        A132Tecnologia_Nome = P00A77_A132Tecnologia_Nome[0];
                        A355Tecnologia_TipoTecnologia = P00A77_A355Tecnologia_TipoTecnologia[0];
                        n355Tecnologia_TipoTecnologia = P00A77_n355Tecnologia_TipoTecnologia[0];
                        A132Tecnologia_Nome = P00A77_A132Tecnologia_Nome[0];
                        A355Tecnologia_TipoTecnologia = P00A77_A355Tecnologia_TipoTecnologia[0];
                        n355Tecnologia_TipoTecnologia = P00A77_n355Tecnologia_TipoTecnologia[0];
                        if ( StringUtil.StrCmp(StringUtil.Str( (decimal)(A2128SistemaTecnologiaLinha), 4, 0), AV25KeySistemaTecnologiaLinha) == 0 )
                        {
                           AV37GXLvl767 = 1;
                           AV11AuditingObjectRecordItem.gxTpr_Mode = "UPD";
                           AV24CountUpdatedSistemaTecnologiaLinha = (short)(AV24CountUpdatedSistemaTecnologiaLinha+1);
                           AV38GXV4 = 1;
                           while ( AV38GXV4 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV38GXV4));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "SistemaTecnologiaLinha") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2128SistemaTecnologiaLinha), 4, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Tecnologia_Codigo") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Tecnologia_Nome") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A132Tecnologia_Nome;
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Tecnologia_TipoTecnologia") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A355Tecnologia_TipoTecnologia;
                              }
                              AV38GXV4 = (int)(AV38GXV4+1);
                           }
                        }
                        pr_default.readNext(4);
                     }
                     pr_default.close(4);
                     if ( AV37GXLvl767 == 0 )
                     {
                        AV11AuditingObjectRecordItem.gxTpr_Mode = "DLT";
                     }
                  }
                  AV33GXV1 = (int)(AV33GXV1+1);
               }
               if ( AV24CountUpdatedSistemaTecnologiaLinha < A40000GXC1 )
               {
                  AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
                  /* Using cursor P00A78 */
                  pr_default.execute(5, new Object[] {AV16Sistema_Codigo});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A127Sistema_Codigo = P00A78_A127Sistema_Codigo[0];
                     A2128SistemaTecnologiaLinha = P00A78_A2128SistemaTecnologiaLinha[0];
                     A131Tecnologia_Codigo = P00A78_A131Tecnologia_Codigo[0];
                     A132Tecnologia_Nome = P00A78_A132Tecnologia_Nome[0];
                     A355Tecnologia_TipoTecnologia = P00A78_A355Tecnologia_TipoTecnologia[0];
                     n355Tecnologia_TipoTecnologia = P00A78_n355Tecnologia_TipoTecnologia[0];
                     A132Tecnologia_Nome = P00A78_A132Tecnologia_Nome[0];
                     A355Tecnologia_TipoTecnologia = P00A78_A355Tecnologia_TipoTecnologia[0];
                     n355Tecnologia_TipoTecnologia = P00A78_n355Tecnologia_TipoTecnologia[0];
                     AV25KeySistemaTecnologiaLinha = StringUtil.Str( (decimal)(A2128SistemaTecnologiaLinha), 4, 0);
                     AV26RecordExistsSistemaTecnologiaLinha = false;
                     AV40GXV5 = 1;
                     while ( AV40GXV5 <= AV10AuditingObject.gxTpr_Record.Count )
                     {
                        AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV40GXV5));
                        if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "SistemaTecnologia") == 0 )
                        {
                           AV20CountKeyAttributes = 0;
                           AV41GXV6 = 1;
                           while ( AV41GXV6 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV41GXV6));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "SistemaTecnologiaLinha") == 0 )
                              {
                                 if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue, AV25KeySistemaTecnologiaLinha) == 0 )
                                 {
                                    AV26RecordExistsSistemaTecnologiaLinha = true;
                                    AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                                    if ( AV20CountKeyAttributes == 1 )
                                    {
                                       if (true) break;
                                    }
                                 }
                              }
                              AV41GXV6 = (int)(AV41GXV6+1);
                           }
                        }
                        AV40GXV5 = (int)(AV40GXV5+1);
                     }
                     if ( ! ( AV26RecordExistsSistemaTecnologiaLinha ) )
                     {
                        AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                        AV18AuditingObjectRecordItemNew.gxTpr_Tablename = "SistemaTecnologia";
                        AV18AuditingObjectRecordItemNew.gxTpr_Mode = "INS";
                        AV17AuditingObjectNewRecords.gxTpr_Record.Add(AV18AuditingObjectRecordItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "SistemaTecnologiaLinha";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2128SistemaTecnologiaLinha), 4, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "Tecnologia_Codigo";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "Tecnologia_Nome";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = A132Tecnologia_Nome;
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "Tecnologia_TipoTecnologia";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = A355Tecnologia_TipoTecnologia;
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                     }
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
                  AV42GXV7 = 1;
                  while ( AV42GXV7 <= AV17AuditingObjectNewRecords.gxTpr_Record.Count )
                  {
                     AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV17AuditingObjectNewRecords.gxTpr_Record.Item(AV42GXV7));
                     AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                     AV42GXV7 = (int)(AV42GXV7+1);
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A72_A416Sistema_Nome = new String[] {""} ;
         P00A72_A128Sistema_Descricao = new String[] {""} ;
         P00A72_n128Sistema_Descricao = new bool[] {false} ;
         P00A72_A129Sistema_Sigla = new String[] {""} ;
         P00A72_A513Sistema_Coordenacao = new String[] {""} ;
         P00A72_n513Sistema_Coordenacao = new bool[] {false} ;
         P00A72_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00A72_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         P00A72_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         P00A72_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00A72_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00A72_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00A72_A137Metodologia_Codigo = new int[1] ;
         P00A72_n137Metodologia_Codigo = new bool[] {false} ;
         P00A72_A138Metodologia_Descricao = new String[] {""} ;
         P00A72_A700Sistema_Tecnica = new String[] {""} ;
         P00A72_n700Sistema_Tecnica = new bool[] {false} ;
         P00A72_A687Sistema_Custo = new decimal[1] ;
         P00A72_n687Sistema_Custo = new bool[] {false} ;
         P00A72_A688Sistema_Prazo = new short[1] ;
         P00A72_n688Sistema_Prazo = new bool[] {false} ;
         P00A72_A689Sistema_Esforco = new short[1] ;
         P00A72_n689Sistema_Esforco = new bool[] {false} ;
         P00A72_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         P00A72_n1401Sistema_ImpData = new bool[] {false} ;
         P00A72_A1399Sistema_ImpUserCod = new int[1] ;
         P00A72_n1399Sistema_ImpUserCod = new bool[] {false} ;
         P00A72_A1402Sistema_ImpUserPesCod = new int[1] ;
         P00A72_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         P00A72_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         P00A72_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         P00A72_A1831Sistema_Responsavel = new int[1] ;
         P00A72_n1831Sistema_Responsavel = new bool[] {false} ;
         P00A72_A1859SistemaVersao_Codigo = new int[1] ;
         P00A72_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00A72_A1860SistemaVersao_Id = new String[] {""} ;
         P00A72_A130Sistema_Ativo = new bool[] {false} ;
         P00A72_A2109Sistema_Repositorio = new String[] {""} ;
         P00A72_n2109Sistema_Repositorio = new bool[] {false} ;
         P00A72_A2130Sistema_Ultimo = new short[1] ;
         P00A72_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         P00A72_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         P00A72_A699Sistema_Tipo = new String[] {""} ;
         P00A72_n699Sistema_Tipo = new bool[] {false} ;
         P00A72_A686Sistema_FatorAjuste = new decimal[1] ;
         P00A72_n686Sistema_FatorAjuste = new bool[] {false} ;
         P00A72_A127Sistema_Codigo = new int[1] ;
         A416Sistema_Nome = "";
         A128Sistema_Descricao = "";
         A129Sistema_Sigla = "";
         A513Sistema_Coordenacao = "";
         A136Sistema_AreaTrabalhoDes = "";
         A352AmbienteTecnologico_Descricao = "";
         A138Metodologia_Descricao = "";
         A700Sistema_Tecnica = "";
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         A1403Sistema_ImpUserPesNom = "";
         A1860SistemaVersao_Id = "";
         A2109Sistema_Repositorio = "";
         A699Sistema_Tipo = "";
         A707Sistema_TipoSigla = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A73_A127Sistema_Codigo = new int[1] ;
         P00A73_A2128SistemaTecnologiaLinha = new short[1] ;
         P00A73_A131Tecnologia_Codigo = new int[1] ;
         P00A73_A132Tecnologia_Nome = new String[] {""} ;
         P00A73_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         P00A73_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         A132Tecnologia_Nome = "";
         A355Tecnologia_TipoTecnologia = "";
         P00A75_A416Sistema_Nome = new String[] {""} ;
         P00A75_A128Sistema_Descricao = new String[] {""} ;
         P00A75_n128Sistema_Descricao = new bool[] {false} ;
         P00A75_A129Sistema_Sigla = new String[] {""} ;
         P00A75_A513Sistema_Coordenacao = new String[] {""} ;
         P00A75_n513Sistema_Coordenacao = new bool[] {false} ;
         P00A75_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00A75_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         P00A75_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         P00A75_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00A75_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00A75_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00A75_A137Metodologia_Codigo = new int[1] ;
         P00A75_n137Metodologia_Codigo = new bool[] {false} ;
         P00A75_A138Metodologia_Descricao = new String[] {""} ;
         P00A75_A700Sistema_Tecnica = new String[] {""} ;
         P00A75_n700Sistema_Tecnica = new bool[] {false} ;
         P00A75_A687Sistema_Custo = new decimal[1] ;
         P00A75_n687Sistema_Custo = new bool[] {false} ;
         P00A75_A688Sistema_Prazo = new short[1] ;
         P00A75_n688Sistema_Prazo = new bool[] {false} ;
         P00A75_A689Sistema_Esforco = new short[1] ;
         P00A75_n689Sistema_Esforco = new bool[] {false} ;
         P00A75_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         P00A75_n1401Sistema_ImpData = new bool[] {false} ;
         P00A75_A1399Sistema_ImpUserCod = new int[1] ;
         P00A75_n1399Sistema_ImpUserCod = new bool[] {false} ;
         P00A75_A1402Sistema_ImpUserPesCod = new int[1] ;
         P00A75_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         P00A75_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         P00A75_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         P00A75_A1831Sistema_Responsavel = new int[1] ;
         P00A75_n1831Sistema_Responsavel = new bool[] {false} ;
         P00A75_A1859SistemaVersao_Codigo = new int[1] ;
         P00A75_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00A75_A1860SistemaVersao_Id = new String[] {""} ;
         P00A75_A130Sistema_Ativo = new bool[] {false} ;
         P00A75_A2109Sistema_Repositorio = new String[] {""} ;
         P00A75_n2109Sistema_Repositorio = new bool[] {false} ;
         P00A75_A2130Sistema_Ultimo = new short[1] ;
         P00A75_A40000GXC1 = new int[1] ;
         P00A75_n40000GXC1 = new bool[] {false} ;
         P00A75_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         P00A75_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         P00A75_A699Sistema_Tipo = new String[] {""} ;
         P00A75_n699Sistema_Tipo = new bool[] {false} ;
         P00A75_A686Sistema_FatorAjuste = new decimal[1] ;
         P00A75_n686Sistema_FatorAjuste = new bool[] {false} ;
         P00A75_A127Sistema_Codigo = new int[1] ;
         P00A76_A127Sistema_Codigo = new int[1] ;
         P00A76_A2128SistemaTecnologiaLinha = new short[1] ;
         P00A76_A131Tecnologia_Codigo = new int[1] ;
         P00A76_A132Tecnologia_Nome = new String[] {""} ;
         P00A76_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         P00A76_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         AV25KeySistemaTecnologiaLinha = "";
         P00A77_A127Sistema_Codigo = new int[1] ;
         P00A77_A2128SistemaTecnologiaLinha = new short[1] ;
         P00A77_A131Tecnologia_Codigo = new int[1] ;
         P00A77_A132Tecnologia_Nome = new String[] {""} ;
         P00A77_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         P00A77_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
         P00A78_A127Sistema_Codigo = new int[1] ;
         P00A78_A2128SistemaTecnologiaLinha = new short[1] ;
         P00A78_A131Tecnologia_Codigo = new int[1] ;
         P00A78_A132Tecnologia_Nome = new String[] {""} ;
         P00A78_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         P00A78_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditsistema__default(),
            new Object[][] {
                new Object[] {
               P00A72_A416Sistema_Nome, P00A72_A128Sistema_Descricao, P00A72_n128Sistema_Descricao, P00A72_A129Sistema_Sigla, P00A72_A513Sistema_Coordenacao, P00A72_n513Sistema_Coordenacao, P00A72_A135Sistema_AreaTrabalhoCod, P00A72_A136Sistema_AreaTrabalhoDes, P00A72_n136Sistema_AreaTrabalhoDes, P00A72_A351AmbienteTecnologico_Codigo,
               P00A72_n351AmbienteTecnologico_Codigo, P00A72_A352AmbienteTecnologico_Descricao, P00A72_A137Metodologia_Codigo, P00A72_n137Metodologia_Codigo, P00A72_A138Metodologia_Descricao, P00A72_A700Sistema_Tecnica, P00A72_n700Sistema_Tecnica, P00A72_A687Sistema_Custo, P00A72_n687Sistema_Custo, P00A72_A688Sistema_Prazo,
               P00A72_n688Sistema_Prazo, P00A72_A689Sistema_Esforco, P00A72_n689Sistema_Esforco, P00A72_A1401Sistema_ImpData, P00A72_n1401Sistema_ImpData, P00A72_A1399Sistema_ImpUserCod, P00A72_n1399Sistema_ImpUserCod, P00A72_A1402Sistema_ImpUserPesCod, P00A72_n1402Sistema_ImpUserPesCod, P00A72_A1403Sistema_ImpUserPesNom,
               P00A72_n1403Sistema_ImpUserPesNom, P00A72_A1831Sistema_Responsavel, P00A72_n1831Sistema_Responsavel, P00A72_A1859SistemaVersao_Codigo, P00A72_n1859SistemaVersao_Codigo, P00A72_A1860SistemaVersao_Id, P00A72_A130Sistema_Ativo, P00A72_A2109Sistema_Repositorio, P00A72_n2109Sistema_Repositorio, P00A72_A2130Sistema_Ultimo,
               P00A72_A2161Sistema_GpoObjCtrlCod, P00A72_n2161Sistema_GpoObjCtrlCod, P00A72_A699Sistema_Tipo, P00A72_n699Sistema_Tipo, P00A72_A686Sistema_FatorAjuste, P00A72_n686Sistema_FatorAjuste, P00A72_A127Sistema_Codigo
               }
               , new Object[] {
               P00A73_A127Sistema_Codigo, P00A73_A2128SistemaTecnologiaLinha, P00A73_A131Tecnologia_Codigo, P00A73_A132Tecnologia_Nome, P00A73_A355Tecnologia_TipoTecnologia, P00A73_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               P00A75_A416Sistema_Nome, P00A75_A128Sistema_Descricao, P00A75_n128Sistema_Descricao, P00A75_A129Sistema_Sigla, P00A75_A513Sistema_Coordenacao, P00A75_n513Sistema_Coordenacao, P00A75_A135Sistema_AreaTrabalhoCod, P00A75_A136Sistema_AreaTrabalhoDes, P00A75_n136Sistema_AreaTrabalhoDes, P00A75_A351AmbienteTecnologico_Codigo,
               P00A75_n351AmbienteTecnologico_Codigo, P00A75_A352AmbienteTecnologico_Descricao, P00A75_A137Metodologia_Codigo, P00A75_n137Metodologia_Codigo, P00A75_A138Metodologia_Descricao, P00A75_A700Sistema_Tecnica, P00A75_n700Sistema_Tecnica, P00A75_A687Sistema_Custo, P00A75_n687Sistema_Custo, P00A75_A688Sistema_Prazo,
               P00A75_n688Sistema_Prazo, P00A75_A689Sistema_Esforco, P00A75_n689Sistema_Esforco, P00A75_A1401Sistema_ImpData, P00A75_n1401Sistema_ImpData, P00A75_A1399Sistema_ImpUserCod, P00A75_n1399Sistema_ImpUserCod, P00A75_A1402Sistema_ImpUserPesCod, P00A75_n1402Sistema_ImpUserPesCod, P00A75_A1403Sistema_ImpUserPesNom,
               P00A75_n1403Sistema_ImpUserPesNom, P00A75_A1831Sistema_Responsavel, P00A75_n1831Sistema_Responsavel, P00A75_A1859SistemaVersao_Codigo, P00A75_n1859SistemaVersao_Codigo, P00A75_A1860SistemaVersao_Id, P00A75_A130Sistema_Ativo, P00A75_A2109Sistema_Repositorio, P00A75_n2109Sistema_Repositorio, P00A75_A2130Sistema_Ultimo,
               P00A75_A40000GXC1, P00A75_n40000GXC1, P00A75_A2161Sistema_GpoObjCtrlCod, P00A75_n2161Sistema_GpoObjCtrlCod, P00A75_A699Sistema_Tipo, P00A75_n699Sistema_Tipo, P00A75_A686Sistema_FatorAjuste, P00A75_n686Sistema_FatorAjuste, P00A75_A127Sistema_Codigo
               }
               , new Object[] {
               P00A76_A127Sistema_Codigo, P00A76_A2128SistemaTecnologiaLinha, P00A76_A131Tecnologia_Codigo, P00A76_A132Tecnologia_Nome, P00A76_A355Tecnologia_TipoTecnologia, P00A76_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               P00A77_A127Sistema_Codigo, P00A77_A2128SistemaTecnologiaLinha, P00A77_A131Tecnologia_Codigo, P00A77_A132Tecnologia_Nome, P00A77_A355Tecnologia_TipoTecnologia, P00A77_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               P00A78_A127Sistema_Codigo, P00A78_A2128SistemaTecnologiaLinha, P00A78_A131Tecnologia_Codigo, P00A78_A132Tecnologia_Nome, P00A78_A355Tecnologia_TipoTecnologia, P00A78_n355Tecnologia_TipoTecnologia
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A688Sistema_Prazo ;
      private short A689Sistema_Esforco ;
      private short A2130Sistema_Ultimo ;
      private short A2128SistemaTecnologiaLinha ;
      private short AV24CountUpdatedSistemaTecnologiaLinha ;
      private short GXt_int3 ;
      private short AV20CountKeyAttributes ;
      private short AV37GXLvl767 ;
      private int AV16Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A137Metodologia_Codigo ;
      private int A1399Sistema_ImpUserCod ;
      private int A1402Sistema_ImpUserPesCod ;
      private int A1831Sistema_Responsavel ;
      private int A1859SistemaVersao_Codigo ;
      private int A2161Sistema_GpoObjCtrlCod ;
      private int A127Sistema_Codigo ;
      private int A2162Sistema_GpoObjCtrlRsp ;
      private int A131Tecnologia_Codigo ;
      private int A40000GXC1 ;
      private int AV33GXV1 ;
      private int GXt_int2 ;
      private int AV35GXV2 ;
      private int AV36GXV3 ;
      private int AV38GXV4 ;
      private int AV40GXV5 ;
      private int AV41GXV6 ;
      private int AV42GXV7 ;
      private decimal A687Sistema_Custo ;
      private decimal A686Sistema_FatorAjuste ;
      private decimal A395Sistema_PF ;
      private decimal A690Sistema_PFA ;
      private decimal GXt_decimal1 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A129Sistema_Sigla ;
      private String A700Sistema_Tecnica ;
      private String A1403Sistema_ImpUserPesNom ;
      private String A1860SistemaVersao_Id ;
      private String A699Sistema_Tipo ;
      private String A707Sistema_TipoSigla ;
      private String A132Tecnologia_Nome ;
      private String A355Tecnologia_TipoTecnologia ;
      private DateTime A1401Sistema_ImpData ;
      private bool returnInSub ;
      private bool n128Sistema_Descricao ;
      private bool n513Sistema_Coordenacao ;
      private bool n136Sistema_AreaTrabalhoDes ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n137Metodologia_Codigo ;
      private bool n700Sistema_Tecnica ;
      private bool n687Sistema_Custo ;
      private bool n688Sistema_Prazo ;
      private bool n689Sistema_Esforco ;
      private bool n1401Sistema_ImpData ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n1402Sistema_ImpUserPesCod ;
      private bool n1403Sistema_ImpUserPesNom ;
      private bool n1831Sistema_Responsavel ;
      private bool n1859SistemaVersao_Codigo ;
      private bool A130Sistema_Ativo ;
      private bool n2109Sistema_Repositorio ;
      private bool n2161Sistema_GpoObjCtrlCod ;
      private bool n699Sistema_Tipo ;
      private bool n686Sistema_FatorAjuste ;
      private bool n355Tecnologia_TipoTecnologia ;
      private bool n40000GXC1 ;
      private bool AV26RecordExistsSistemaTecnologiaLinha ;
      private String A128Sistema_Descricao ;
      private String A416Sistema_Nome ;
      private String A513Sistema_Coordenacao ;
      private String A136Sistema_AreaTrabalhoDes ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A138Metodologia_Descricao ;
      private String A2109Sistema_Repositorio ;
      private String AV25KeySistemaTecnologiaLinha ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private String[] P00A72_A416Sistema_Nome ;
      private String[] P00A72_A128Sistema_Descricao ;
      private bool[] P00A72_n128Sistema_Descricao ;
      private String[] P00A72_A129Sistema_Sigla ;
      private String[] P00A72_A513Sistema_Coordenacao ;
      private bool[] P00A72_n513Sistema_Coordenacao ;
      private int[] P00A72_A135Sistema_AreaTrabalhoCod ;
      private String[] P00A72_A136Sistema_AreaTrabalhoDes ;
      private bool[] P00A72_n136Sistema_AreaTrabalhoDes ;
      private int[] P00A72_A351AmbienteTecnologico_Codigo ;
      private bool[] P00A72_n351AmbienteTecnologico_Codigo ;
      private String[] P00A72_A352AmbienteTecnologico_Descricao ;
      private int[] P00A72_A137Metodologia_Codigo ;
      private bool[] P00A72_n137Metodologia_Codigo ;
      private String[] P00A72_A138Metodologia_Descricao ;
      private String[] P00A72_A700Sistema_Tecnica ;
      private bool[] P00A72_n700Sistema_Tecnica ;
      private decimal[] P00A72_A687Sistema_Custo ;
      private bool[] P00A72_n687Sistema_Custo ;
      private short[] P00A72_A688Sistema_Prazo ;
      private bool[] P00A72_n688Sistema_Prazo ;
      private short[] P00A72_A689Sistema_Esforco ;
      private bool[] P00A72_n689Sistema_Esforco ;
      private DateTime[] P00A72_A1401Sistema_ImpData ;
      private bool[] P00A72_n1401Sistema_ImpData ;
      private int[] P00A72_A1399Sistema_ImpUserCod ;
      private bool[] P00A72_n1399Sistema_ImpUserCod ;
      private int[] P00A72_A1402Sistema_ImpUserPesCod ;
      private bool[] P00A72_n1402Sistema_ImpUserPesCod ;
      private String[] P00A72_A1403Sistema_ImpUserPesNom ;
      private bool[] P00A72_n1403Sistema_ImpUserPesNom ;
      private int[] P00A72_A1831Sistema_Responsavel ;
      private bool[] P00A72_n1831Sistema_Responsavel ;
      private int[] P00A72_A1859SistemaVersao_Codigo ;
      private bool[] P00A72_n1859SistemaVersao_Codigo ;
      private String[] P00A72_A1860SistemaVersao_Id ;
      private bool[] P00A72_A130Sistema_Ativo ;
      private String[] P00A72_A2109Sistema_Repositorio ;
      private bool[] P00A72_n2109Sistema_Repositorio ;
      private short[] P00A72_A2130Sistema_Ultimo ;
      private int[] P00A72_A2161Sistema_GpoObjCtrlCod ;
      private bool[] P00A72_n2161Sistema_GpoObjCtrlCod ;
      private String[] P00A72_A699Sistema_Tipo ;
      private bool[] P00A72_n699Sistema_Tipo ;
      private decimal[] P00A72_A686Sistema_FatorAjuste ;
      private bool[] P00A72_n686Sistema_FatorAjuste ;
      private int[] P00A72_A127Sistema_Codigo ;
      private int[] P00A73_A127Sistema_Codigo ;
      private short[] P00A73_A2128SistemaTecnologiaLinha ;
      private int[] P00A73_A131Tecnologia_Codigo ;
      private String[] P00A73_A132Tecnologia_Nome ;
      private String[] P00A73_A355Tecnologia_TipoTecnologia ;
      private bool[] P00A73_n355Tecnologia_TipoTecnologia ;
      private String[] P00A75_A416Sistema_Nome ;
      private String[] P00A75_A128Sistema_Descricao ;
      private bool[] P00A75_n128Sistema_Descricao ;
      private String[] P00A75_A129Sistema_Sigla ;
      private String[] P00A75_A513Sistema_Coordenacao ;
      private bool[] P00A75_n513Sistema_Coordenacao ;
      private int[] P00A75_A135Sistema_AreaTrabalhoCod ;
      private String[] P00A75_A136Sistema_AreaTrabalhoDes ;
      private bool[] P00A75_n136Sistema_AreaTrabalhoDes ;
      private int[] P00A75_A351AmbienteTecnologico_Codigo ;
      private bool[] P00A75_n351AmbienteTecnologico_Codigo ;
      private String[] P00A75_A352AmbienteTecnologico_Descricao ;
      private int[] P00A75_A137Metodologia_Codigo ;
      private bool[] P00A75_n137Metodologia_Codigo ;
      private String[] P00A75_A138Metodologia_Descricao ;
      private String[] P00A75_A700Sistema_Tecnica ;
      private bool[] P00A75_n700Sistema_Tecnica ;
      private decimal[] P00A75_A687Sistema_Custo ;
      private bool[] P00A75_n687Sistema_Custo ;
      private short[] P00A75_A688Sistema_Prazo ;
      private bool[] P00A75_n688Sistema_Prazo ;
      private short[] P00A75_A689Sistema_Esforco ;
      private bool[] P00A75_n689Sistema_Esforco ;
      private DateTime[] P00A75_A1401Sistema_ImpData ;
      private bool[] P00A75_n1401Sistema_ImpData ;
      private int[] P00A75_A1399Sistema_ImpUserCod ;
      private bool[] P00A75_n1399Sistema_ImpUserCod ;
      private int[] P00A75_A1402Sistema_ImpUserPesCod ;
      private bool[] P00A75_n1402Sistema_ImpUserPesCod ;
      private String[] P00A75_A1403Sistema_ImpUserPesNom ;
      private bool[] P00A75_n1403Sistema_ImpUserPesNom ;
      private int[] P00A75_A1831Sistema_Responsavel ;
      private bool[] P00A75_n1831Sistema_Responsavel ;
      private int[] P00A75_A1859SistemaVersao_Codigo ;
      private bool[] P00A75_n1859SistemaVersao_Codigo ;
      private String[] P00A75_A1860SistemaVersao_Id ;
      private bool[] P00A75_A130Sistema_Ativo ;
      private String[] P00A75_A2109Sistema_Repositorio ;
      private bool[] P00A75_n2109Sistema_Repositorio ;
      private short[] P00A75_A2130Sistema_Ultimo ;
      private int[] P00A75_A40000GXC1 ;
      private bool[] P00A75_n40000GXC1 ;
      private int[] P00A75_A2161Sistema_GpoObjCtrlCod ;
      private bool[] P00A75_n2161Sistema_GpoObjCtrlCod ;
      private String[] P00A75_A699Sistema_Tipo ;
      private bool[] P00A75_n699Sistema_Tipo ;
      private decimal[] P00A75_A686Sistema_FatorAjuste ;
      private bool[] P00A75_n686Sistema_FatorAjuste ;
      private int[] P00A75_A127Sistema_Codigo ;
      private int[] P00A76_A127Sistema_Codigo ;
      private short[] P00A76_A2128SistemaTecnologiaLinha ;
      private int[] P00A76_A131Tecnologia_Codigo ;
      private String[] P00A76_A132Tecnologia_Nome ;
      private String[] P00A76_A355Tecnologia_TipoTecnologia ;
      private bool[] P00A76_n355Tecnologia_TipoTecnologia ;
      private int[] P00A77_A127Sistema_Codigo ;
      private short[] P00A77_A2128SistemaTecnologiaLinha ;
      private int[] P00A77_A131Tecnologia_Codigo ;
      private String[] P00A77_A132Tecnologia_Nome ;
      private String[] P00A77_A355Tecnologia_TipoTecnologia ;
      private bool[] P00A77_n355Tecnologia_TipoTecnologia ;
      private int[] P00A78_A127Sistema_Codigo ;
      private short[] P00A78_A2128SistemaTecnologiaLinha ;
      private int[] P00A78_A131Tecnologia_Codigo ;
      private String[] P00A78_A132Tecnologia_Nome ;
      private String[] P00A78_A355Tecnologia_TipoTecnologia ;
      private bool[] P00A78_n355Tecnologia_TipoTecnologia ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject AV17AuditingObjectNewRecords ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV18AuditingObjectRecordItemNew ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV19AuditingObjectRecordItemAttributeItemNew ;
   }

   public class loadauditsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A72 ;
          prmP00A72 = new Object[] {
          new Object[] {"@AV16Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A73 ;
          prmP00A73 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A75 ;
          prmP00A75 = new Object[] {
          new Object[] {"@AV16Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A76 ;
          prmP00A76 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A77 ;
          prmP00A77 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A78 ;
          prmP00A78 = new Object[] {
          new Object[] {"@AV16Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00A72", "SELECT T1.[Sistema_Nome], T1.[Sistema_Descricao], T1.[Sistema_Sigla], T1.[Sistema_Coordenacao], T1.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes, T1.[AmbienteTecnologico_Codigo], T3.[AmbienteTecnologico_Descricao], T1.[Metodologia_Codigo], T4.[Metodologia_Descricao], T1.[Sistema_Tecnica], T1.[Sistema_Custo], T1.[Sistema_Prazo], T1.[Sistema_Esforco], T1.[Sistema_ImpData], T1.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, T5.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod, T6.[Pessoa_Nome] AS Sistema_ImpUserPesNom, T1.[Sistema_Responsavel], T1.[SistemaVersao_Codigo], T7.[SistemaVersao_Id], T1.[Sistema_Ativo], T1.[Sistema_Repositorio], T1.[Sistema_Ultimo], T1.[Sistema_GpoObjCtrlCod], T1.[Sistema_Tipo], T1.[Sistema_FatorAjuste], T1.[Sistema_Codigo] FROM (((((([Sistema] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Sistema_AreaTrabalhoCod]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T4 WITH (NOLOCK) ON T4.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [SistemaVersao] T7 WITH (NOLOCK) ON T7.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) WHERE T1.[Sistema_Codigo] = @AV16Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A72,1,0,true,true )
             ,new CursorDef("P00A73", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T1.[Tecnologia_Codigo], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A73,100,0,false,false )
             ,new CursorDef("P00A75", "SELECT T1.[Sistema_Nome], T1.[Sistema_Descricao], T1.[Sistema_Sigla], T1.[Sistema_Coordenacao], T1.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes, T1.[AmbienteTecnologico_Codigo], T3.[AmbienteTecnologico_Descricao], T1.[Metodologia_Codigo], T4.[Metodologia_Descricao], T1.[Sistema_Tecnica], T1.[Sistema_Custo], T1.[Sistema_Prazo], T1.[Sistema_Esforco], T1.[Sistema_ImpData], T1.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, T5.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod, T6.[Pessoa_Nome] AS Sistema_ImpUserPesNom, T1.[Sistema_Responsavel], T1.[SistemaVersao_Codigo], T7.[SistemaVersao_Id], T1.[Sistema_Ativo], T1.[Sistema_Repositorio], T1.[Sistema_Ultimo], COALESCE( T8.[GXC1], 0) AS GXC1, T1.[Sistema_GpoObjCtrlCod], T1.[Sistema_Tipo], T1.[Sistema_FatorAjuste], T1.[Sistema_Codigo] FROM (((((([Sistema] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Sistema_AreaTrabalhoCod]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T4 WITH (NOLOCK) ON T4.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T1.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [SistemaVersao] T7 WITH (NOLOCK) ON T7.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]),  (SELECT COUNT(*) AS GXC1 FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV16Sistema_Codigo ) T8 WHERE T1.[Sistema_Codigo] = @AV16Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A75,1,0,true,true )
             ,new CursorDef("P00A76", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T1.[Tecnologia_Codigo], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A76,100,0,false,false )
             ,new CursorDef("P00A77", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T1.[Tecnologia_Codigo], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A77,100,0,false,false )
             ,new CursorDef("P00A78", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T1.[Tecnologia_Codigo], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @AV16Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A78,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 25) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[15])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((int[]) buf[25])[0] = rslt.getInt(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 100) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getString(21, 20) ;
                ((bool[]) buf[36])[0] = rslt.getBool(22) ;
                ((String[]) buf[37])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(23);
                ((short[]) buf[39])[0] = rslt.getShort(24) ;
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((String[]) buf[42])[0] = rslt.getString(26, 1) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(27) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(27);
                ((int[]) buf[46])[0] = rslt.getInt(28) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 25) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[15])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((int[]) buf[25])[0] = rslt.getInt(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 100) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getString(21, 20) ;
                ((bool[]) buf[36])[0] = rslt.getBool(22) ;
                ((String[]) buf[37])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(23);
                ((short[]) buf[39])[0] = rslt.getShort(24) ;
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((int[]) buf[42])[0] = rslt.getInt(26) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((String[]) buf[44])[0] = rslt.getString(27, 1) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(27);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(28) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(28);
                ((int[]) buf[48])[0] = rslt.getInt(29) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
