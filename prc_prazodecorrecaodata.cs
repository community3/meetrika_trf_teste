/*
               File: PRC_PrazoDeCorrecaoData
        Description: Data do Prazo De Correcao Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:27.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_prazodecorrecaodata : GXProcedure
   {
      public prc_prazodecorrecaodata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_prazodecorrecaodata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           String aP1_PrazoCorrecaoTipoIn ,
                           short aP2_PrazoCorrecaoIn ,
                           ref short aP3_DiasParaAnaliseIn ,
                           short aP4_PrazoInicial ,
                           short aP5_PrazoInicioIn ,
                           DateTime aP6_FimDoExpedienteIn ,
                           ref DateTime aP7_PrazoEntrega )
      {
         this.AV24ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV16PrazoCorrecaoTipoIn = aP1_PrazoCorrecaoTipoIn;
         this.AV15PrazoCorrecaoIn = aP2_PrazoCorrecaoIn;
         this.AV18DiasParaAnaliseIn = aP3_DiasParaAnaliseIn;
         this.AV14PrazoInicial = aP4_PrazoInicial;
         this.AV26PrazoInicioIn = aP5_PrazoInicioIn;
         this.AV22FimDoExpedienteIn = aP6_FimDoExpedienteIn;
         this.AV12PrazoEntrega = aP7_PrazoEntrega;
         initialize();
         executePrivate();
         aP3_DiasParaAnaliseIn=this.AV18DiasParaAnaliseIn;
         aP7_PrazoEntrega=this.AV12PrazoEntrega;
      }

      public DateTime executeUdp( int aP0_ContratoServicos_Codigo ,
                                  String aP1_PrazoCorrecaoTipoIn ,
                                  short aP2_PrazoCorrecaoIn ,
                                  ref short aP3_DiasParaAnaliseIn ,
                                  short aP4_PrazoInicial ,
                                  short aP5_PrazoInicioIn ,
                                  DateTime aP6_FimDoExpedienteIn )
      {
         this.AV24ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV16PrazoCorrecaoTipoIn = aP1_PrazoCorrecaoTipoIn;
         this.AV15PrazoCorrecaoIn = aP2_PrazoCorrecaoIn;
         this.AV18DiasParaAnaliseIn = aP3_DiasParaAnaliseIn;
         this.AV14PrazoInicial = aP4_PrazoInicial;
         this.AV26PrazoInicioIn = aP5_PrazoInicioIn;
         this.AV22FimDoExpedienteIn = aP6_FimDoExpedienteIn;
         this.AV12PrazoEntrega = aP7_PrazoEntrega;
         initialize();
         executePrivate();
         aP3_DiasParaAnaliseIn=this.AV18DiasParaAnaliseIn;
         aP7_PrazoEntrega=this.AV12PrazoEntrega;
         return AV12PrazoEntrega ;
      }

      public void executeSubmit( int aP0_ContratoServicos_Codigo ,
                                 String aP1_PrazoCorrecaoTipoIn ,
                                 short aP2_PrazoCorrecaoIn ,
                                 ref short aP3_DiasParaAnaliseIn ,
                                 short aP4_PrazoInicial ,
                                 short aP5_PrazoInicioIn ,
                                 DateTime aP6_FimDoExpedienteIn ,
                                 ref DateTime aP7_PrazoEntrega )
      {
         prc_prazodecorrecaodata objprc_prazodecorrecaodata;
         objprc_prazodecorrecaodata = new prc_prazodecorrecaodata();
         objprc_prazodecorrecaodata.AV24ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_prazodecorrecaodata.AV16PrazoCorrecaoTipoIn = aP1_PrazoCorrecaoTipoIn;
         objprc_prazodecorrecaodata.AV15PrazoCorrecaoIn = aP2_PrazoCorrecaoIn;
         objprc_prazodecorrecaodata.AV18DiasParaAnaliseIn = aP3_DiasParaAnaliseIn;
         objprc_prazodecorrecaodata.AV14PrazoInicial = aP4_PrazoInicial;
         objprc_prazodecorrecaodata.AV26PrazoInicioIn = aP5_PrazoInicioIn;
         objprc_prazodecorrecaodata.AV22FimDoExpedienteIn = aP6_FimDoExpedienteIn;
         objprc_prazodecorrecaodata.AV12PrazoEntrega = aP7_PrazoEntrega;
         objprc_prazodecorrecaodata.context.SetSubmitInitialConfig(context);
         objprc_prazodecorrecaodata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_prazodecorrecaodata);
         aP3_DiasParaAnaliseIn=this.AV18DiasParaAnaliseIn;
         aP7_PrazoEntrega=this.AV12PrazoEntrega;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_prazodecorrecaodata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==AV24ContratoServicos_Codigo) )
         {
            AV10PrazoCorrecaoTipo = AV16PrazoCorrecaoTipoIn;
            AV11PrazoCorrecao = AV15PrazoCorrecaoIn;
            AV17DiasParaAnalise = AV18DiasParaAnaliseIn;
            AV25PrazoInicio = AV26PrazoInicioIn;
            AV19FimDoExpediente = AV22FimDoExpedienteIn;
         }
         else
         {
            /* Using cursor P00822 */
            pr_default.execute(0, new Object[] {AV24ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = P00822_A74Contrato_Codigo[0];
               A39Contratada_Codigo = P00822_A39Contratada_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P00822_A52Contratada_AreaTrabalhoCod[0];
               A160ContratoServicos_Codigo = P00822_A160ContratoServicos_Codigo[0];
               A1225ContratoServicos_PrazoCorrecaoTipo = P00822_A1225ContratoServicos_PrazoCorrecaoTipo[0];
               n1225ContratoServicos_PrazoCorrecaoTipo = P00822_n1225ContratoServicos_PrazoCorrecaoTipo[0];
               A1224ContratoServicos_PrazoCorrecao = P00822_A1224ContratoServicos_PrazoCorrecao[0];
               n1224ContratoServicos_PrazoCorrecao = P00822_n1224ContratoServicos_PrazoCorrecao[0];
               A1152ContratoServicos_PrazoAnalise = P00822_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = P00822_n1152ContratoServicos_PrazoAnalise[0];
               A1649ContratoServicos_PrazoInicio = P00822_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = P00822_n1649ContratoServicos_PrazoInicio[0];
               A1454ContratoServicos_PrazoTpDias = P00822_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = P00822_n1454ContratoServicos_PrazoTpDias[0];
               A39Contratada_Codigo = P00822_A39Contratada_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P00822_A52Contratada_AreaTrabalhoCod[0];
               OV25PrazoInicio = AV25PrazoInicio;
               AV10PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
               AV11PrazoCorrecao = A1224ContratoServicos_PrazoCorrecao;
               AV17DiasParaAnalise = A1152ContratoServicos_PrazoAnalise;
               AV25PrazoInicio = A1649ContratoServicos_PrazoInicio;
               AV23TipoDias = A1454ContratoServicos_PrazoTpDias;
               /* Using cursor P00823 */
               pr_default.execute(1, new Object[] {A52Contratada_AreaTrabalhoCod});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A5AreaTrabalho_Codigo = P00823_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = P00823_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00823_n29Contratante_Codigo[0];
                  A1192Contratante_FimDoExpediente = P00823_A1192Contratante_FimDoExpediente[0];
                  n1192Contratante_FimDoExpediente = P00823_n1192Contratante_FimDoExpediente[0];
                  A1192Contratante_FimDoExpediente = P00823_A1192Contratante_FimDoExpediente[0];
                  n1192Contratante_FimDoExpediente = P00823_n1192Contratante_FimDoExpediente[0];
                  /* Using cursor P00824 */
                  pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     AV19FimDoExpediente = A1192Contratante_FimDoExpediente;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(2);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         if ( (DateTime.MinValue==AV12PrazoEntrega) )
         {
            AV12PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         }
         if ( AV25PrazoInicio == 1 )
         {
            AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 86400*(1));
         }
         else if ( AV25PrazoInicio == 2 )
         {
            GXt_dtime1 = AV12PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV12PrazoEntrega,  1,  "U", out  GXt_dtime1) ;
            AV12PrazoEntrega = GXt_dtime1;
         }
         if ( StringUtil.StrCmp(AV10PrazoCorrecaoTipo, "F") == 0 )
         {
            GXt_dtime1 = AV12PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV12PrazoEntrega,  AV11PrazoCorrecao,  AV23TipoDias, out  GXt_dtime1) ;
            AV12PrazoEntrega = GXt_dtime1;
         }
         else if ( StringUtil.StrCmp(AV10PrazoCorrecaoTipo, "I") == 0 )
         {
            GXt_dtime1 = AV12PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV12PrazoEntrega,  AV14PrazoInicial,  AV23TipoDias, out  GXt_dtime1) ;
            AV12PrazoEntrega = GXt_dtime1;
         }
         else if ( StringUtil.StrCmp(AV10PrazoCorrecaoTipo, "P") == 0 )
         {
            AV13p = (decimal)(AV14PrazoInicial*(AV11PrazoCorrecao/ (decimal)(100)));
            if ( ( AV13p != Convert.ToDecimal( NumberUtil.Int( (long)(AV13p)) )) )
            {
               AV13p = (decimal)(NumberUtil.Int( (long)(AV13p))+1);
            }
            GXt_dtime1 = AV12PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV12PrazoEntrega,  (short)(AV13p),  AV23TipoDias, out  GXt_dtime1) ;
            AV12PrazoEntrega = GXt_dtime1;
         }
         else
         {
            AV11PrazoCorrecao = (short)(AV17DiasParaAnalise+1);
            GXt_dtime1 = AV12PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV12PrazoEntrega,  AV11PrazoCorrecao,  AV23TipoDias, out  GXt_dtime1) ;
            AV12PrazoEntrega = GXt_dtime1;
         }
         AV12PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV12PrazoEntrega) ) ;
         if ( AV25PrazoInicio == 3 )
         {
            AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 3600*(DateTimeUtil.Hour( DateTimeUtil.Now( context))));
            AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 60*(DateTimeUtil.Minute( DateTimeUtil.Now( context))));
         }
         else
         {
            if ( DateTimeUtil.Hour( AV19FimDoExpediente) > 0 )
            {
               AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 3600*(DateTimeUtil.Hour( AV19FimDoExpediente)));
               AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 60*(DateTimeUtil.Minute( AV19FimDoExpediente)));
            }
            else
            {
               AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 3600*(18));
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10PrazoCorrecaoTipo = "";
         AV25PrazoInicio = 1;
         AV19FimDoExpediente = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         P00822_A74Contrato_Codigo = new int[1] ;
         P00822_A39Contratada_Codigo = new int[1] ;
         P00822_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00822_A160ContratoServicos_Codigo = new int[1] ;
         P00822_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00822_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00822_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00822_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00822_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00822_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00822_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00822_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00822_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00822_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV23TipoDias = "";
         P00823_A5AreaTrabalho_Codigo = new int[1] ;
         P00823_A29Contratante_Codigo = new int[1] ;
         P00823_n29Contratante_Codigo = new bool[] {false} ;
         P00823_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00823_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         P00824_A29Contratante_Codigo = new int[1] ;
         P00824_n29Contratante_Codigo = new bool[] {false} ;
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_prazodecorrecaodata__default(),
            new Object[][] {
                new Object[] {
               P00822_A74Contrato_Codigo, P00822_A39Contratada_Codigo, P00822_A52Contratada_AreaTrabalhoCod, P00822_A160ContratoServicos_Codigo, P00822_A1225ContratoServicos_PrazoCorrecaoTipo, P00822_n1225ContratoServicos_PrazoCorrecaoTipo, P00822_A1224ContratoServicos_PrazoCorrecao, P00822_n1224ContratoServicos_PrazoCorrecao, P00822_A1152ContratoServicos_PrazoAnalise, P00822_n1152ContratoServicos_PrazoAnalise,
               P00822_A1649ContratoServicos_PrazoInicio, P00822_n1649ContratoServicos_PrazoInicio, P00822_A1454ContratoServicos_PrazoTpDias, P00822_n1454ContratoServicos_PrazoTpDias
               }
               , new Object[] {
               P00823_A5AreaTrabalho_Codigo, P00823_A29Contratante_Codigo, P00823_n29Contratante_Codigo, P00823_A1192Contratante_FimDoExpediente, P00823_n1192Contratante_FimDoExpediente
               }
               , new Object[] {
               P00824_A29Contratante_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15PrazoCorrecaoIn ;
      private short AV18DiasParaAnaliseIn ;
      private short AV14PrazoInicial ;
      private short AV26PrazoInicioIn ;
      private short AV11PrazoCorrecao ;
      private short AV17DiasParaAnalise ;
      private short AV25PrazoInicio ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short OV25PrazoInicio ;
      private int AV24ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A160ContratoServicos_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private decimal AV13p ;
      private String AV16PrazoCorrecaoTipoIn ;
      private String AV10PrazoCorrecaoTipo ;
      private String scmdbuf ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV23TipoDias ;
      private DateTime AV22FimDoExpedienteIn ;
      private DateTime AV12PrazoEntrega ;
      private DateTime AV19FimDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private DateTime GXt_dtime1 ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n29Contratante_Codigo ;
      private bool n1192Contratante_FimDoExpediente ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP3_DiasParaAnaliseIn ;
      private DateTime aP7_PrazoEntrega ;
      private IDataStoreProvider pr_default ;
      private int[] P00822_A74Contrato_Codigo ;
      private int[] P00822_A39Contratada_Codigo ;
      private int[] P00822_A52Contratada_AreaTrabalhoCod ;
      private int[] P00822_A160ContratoServicos_Codigo ;
      private String[] P00822_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00822_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00822_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00822_n1224ContratoServicos_PrazoCorrecao ;
      private short[] P00822_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00822_n1152ContratoServicos_PrazoAnalise ;
      private short[] P00822_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00822_n1649ContratoServicos_PrazoInicio ;
      private String[] P00822_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00822_n1454ContratoServicos_PrazoTpDias ;
      private int[] P00823_A5AreaTrabalho_Codigo ;
      private int[] P00823_A29Contratante_Codigo ;
      private bool[] P00823_n29Contratante_Codigo ;
      private DateTime[] P00823_A1192Contratante_FimDoExpediente ;
      private bool[] P00823_n1192Contratante_FimDoExpediente ;
      private int[] P00824_A29Contratante_Codigo ;
      private bool[] P00824_n29Contratante_Codigo ;
   }

   public class prc_prazodecorrecaodata__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00822 ;
          prmP00822 = new Object[] {
          new Object[] {"@AV24ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00823 ;
          prmP00823 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00824 ;
          prmP00824 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00822", "SELECT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_AreaTrabalhoCod], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_PrazoCorrecaoTipo], T1.[ContratoServicos_PrazoCorrecao], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoInicio], T1.[ContratoServicos_PrazoTpDias] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV24ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00822,1,0,true,true )
             ,new CursorDef("P00823", "SELECT TOP 1 T1.[AreaTrabalho_Codigo], T1.[Contratante_Codigo], T2.[Contratante_FimDoExpediente] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00823,1,0,true,true )
             ,new CursorDef("P00824", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00824,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((short[]) buf[10])[0] = rslt.getShort(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
