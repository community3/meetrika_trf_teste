/*
               File: PRC_NaoAcata
        Description: Nao Acata
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:34.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_naoacata : GXProcedure
   {
      public prc_naoacata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_naoacata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_OsVinculada ,
                           int aP2_UserId ,
                           decimal aP3_PFB ,
                           decimal aP4_PFL ,
                           int aP5_Responsavel_Codigo ,
                           String aP6_Observacao ,
                           bool aP7_NovaContagem ,
                           bool aP8_EmReuniao ,
                           bool aP9_NaoAcatou )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV14OsVinculada = aP1_OsVinculada;
         this.AV18UserId = aP2_UserId;
         this.AV15PFB = aP3_PFB;
         this.AV16PFL = aP4_PFL;
         this.AV30Responsavel_Codigo = aP5_Responsavel_Codigo;
         this.AV31Observacao = aP6_Observacao;
         this.AV34NovaContagem = aP7_NovaContagem;
         this.AV35EmReuniao = aP8_EmReuniao;
         this.AV47NaoAcatou = aP9_NaoAcatou;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_OsVinculada ,
                                 int aP2_UserId ,
                                 decimal aP3_PFB ,
                                 decimal aP4_PFL ,
                                 int aP5_Responsavel_Codigo ,
                                 String aP6_Observacao ,
                                 bool aP7_NovaContagem ,
                                 bool aP8_EmReuniao ,
                                 bool aP9_NaoAcatou )
      {
         prc_naoacata objprc_naoacata;
         objprc_naoacata = new prc_naoacata();
         objprc_naoacata.AV8Codigo = aP0_Codigo;
         objprc_naoacata.AV14OsVinculada = aP1_OsVinculada;
         objprc_naoacata.AV18UserId = aP2_UserId;
         objprc_naoacata.AV15PFB = aP3_PFB;
         objprc_naoacata.AV16PFL = aP4_PFL;
         objprc_naoacata.AV30Responsavel_Codigo = aP5_Responsavel_Codigo;
         objprc_naoacata.AV31Observacao = aP6_Observacao;
         objprc_naoacata.AV34NovaContagem = aP7_NovaContagem;
         objprc_naoacata.AV35EmReuniao = aP8_EmReuniao;
         objprc_naoacata.AV47NaoAcatou = aP9_NaoAcatou;
         objprc_naoacata.context.SetSubmitInitialConfig(context);
         objprc_naoacata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_naoacata);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_naoacata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new prc_setfimcrr(context ).execute( ref  AV8Codigo) ;
         if ( (0==AV14OsVinculada) )
         {
            /* Using cursor P008K2 */
            pr_default.execute(0, new Object[] {AV8Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P008K2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008K2_n490ContagemResultado_ContratadaCod[0];
               A460ContagemResultado_PFBFM = P008K2_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P008K2_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P008K2_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P008K2_n461ContagemResultado_PFLFM[0];
               A458ContagemResultado_PFBFS = P008K2_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P008K2_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P008K2_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P008K2_n459ContagemResultado_PFLFS[0];
               A517ContagemResultado_Ultima = P008K2_A517ContagemResultado_Ultima[0];
               A456ContagemResultado_Codigo = P008K2_A456ContagemResultado_Codigo[0];
               A473ContagemResultado_DataCnt = P008K2_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P008K2_A511ContagemResultado_HoraCnt[0];
               A490ContagemResultado_ContratadaCod = P008K2_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008K2_n490ContagemResultado_ContratadaCod[0];
               AV63GXLvl8 = 0;
               /* Using cursor P008K3 */
               pr_default.execute(1, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, AV18UserId});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = P008K3_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = P008K3_A69ContratadaUsuario_UsuarioCod[0];
                  AV63GXLvl8 = 1;
                  AV59UsuarioDaContratada = true;
                  if ( AV47NaoAcatou )
                  {
                     AV37ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                     AV39ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                     AV38ContagemResultado_PFBFS = AV15PFB;
                     AV40ContagemResultado_PFLFS = AV16PFL;
                  }
                  else
                  {
                     AV38ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
                     AV40ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
                     AV37ContagemResultado_PFBFM = AV15PFB;
                     AV39ContagemResultado_PFLFM = AV16PFL;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
               if ( AV63GXLvl8 == 0 )
               {
                  AV37ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                  AV39ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                  AV38ContagemResultado_PFBFS = AV15PFB;
                  AV40ContagemResultado_PFLFS = AV16PFL;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else
         {
            /* Using cursor P008K4 */
            pr_default.execute(2, new Object[] {AV14OsVinculada});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P008K4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008K4_n490ContagemResultado_ContratadaCod[0];
               A517ContagemResultado_Ultima = P008K4_A517ContagemResultado_Ultima[0];
               A456ContagemResultado_Codigo = P008K4_A456ContagemResultado_Codigo[0];
               A1326ContagemResultado_ContratadaTipoFab = P008K4_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P008K4_n1326ContagemResultado_ContratadaTipoFab[0];
               A458ContagemResultado_PFBFS = P008K4_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P008K4_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P008K4_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P008K4_n459ContagemResultado_PFLFS[0];
               A460ContagemResultado_PFBFM = P008K4_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P008K4_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P008K4_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P008K4_n461ContagemResultado_PFLFM[0];
               A473ContagemResultado_DataCnt = P008K4_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P008K4_A511ContagemResultado_HoraCnt[0];
               A490ContagemResultado_ContratadaCod = P008K4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008K4_n490ContagemResultado_ContratadaCod[0];
               A1326ContagemResultado_ContratadaTipoFab = P008K4_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P008K4_n1326ContagemResultado_ContratadaTipoFab[0];
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
               {
                  AV38ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
                  AV40ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
                  AV37ContagemResultado_PFBFM = AV15PFB;
                  AV39ContagemResultado_PFLFM = AV16PFL;
               }
               else
               {
                  AV37ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                  AV39ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                  AV38ContagemResultado_PFBFS = AV15PFB;
                  AV40ContagemResultado_PFLFS = AV16PFL;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         new prc_getparmindicedivergenciadaos(context ).execute(  AV8Codigo, out  AV25IndiceDivergencia, out  AV24CalculoDivergencia, out  AV23ContagemResultado_ValorPF) ;
         GXt_decimal1 = AV22Divergencia;
         new prc_calculardivergencia(context ).execute(  AV24CalculoDivergencia,  AV38ContagemResultado_PFBFS,  AV37ContagemResultado_PFBFM,  AV40ContagemResultado_PFLFS,  AV39ContagemResultado_PFLFM, out  GXt_decimal1) ;
         AV22Divergencia = GXt_decimal1;
         if ( AV14OsVinculada > 0 )
         {
            AV56OSCodigo = AV14OsVinculada;
            /* Execute user subroutine: 'STATUS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P008K5 */
            pr_default.execute(3, new Object[] {AV14OsVinculada});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A456ContagemResultado_Codigo = P008K5_A456ContagemResultado_Codigo[0];
               A890ContagemResultado_Responsavel = P008K5_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P008K5_n890ContagemResultado_Responsavel[0];
               A1553ContagemResultado_CntSrvCod = P008K5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008K5_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P008K5_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008K5_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P008K5_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008K5_n472ContagemResultado_DataEntrega[0];
               A490ContagemResultado_ContratadaCod = P008K5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008K5_n490ContagemResultado_ContratadaCod[0];
               A601ContagemResultado_Servico = P008K5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008K5_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008K5_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008K5_n1611ContagemResultado_PrzTpDias[0];
               A1511ContagemResultado_InicioCrr = P008K5_A1511ContagemResultado_InicioCrr[0];
               n1511ContagemResultado_InicioCrr = P008K5_n1511ContagemResultado_InicioCrr[0];
               A912ContagemResultado_HoraEntrega = P008K5_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008K5_n912ContagemResultado_HoraEntrega[0];
               A1351ContagemResultado_DataPrevista = P008K5_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P008K5_n1351ContagemResultado_DataPrevista[0];
               A601ContagemResultado_Servico = P008K5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P008K5_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P008K5_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008K5_n1611ContagemResultado_PrzTpDias[0];
               AV12ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV48Deestinatario = A890ContagemResultado_Responsavel;
               AV52ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
               AV36StatusDmn = A484ContagemResultado_StatusDmn;
               AV17PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
               if ( AV35EmReuniao )
               {
                  if ( AV22Divergencia > AV25IndiceDivergencia )
                  {
                     AV57StatusCnt = 7;
                     AV28NovoStatus = "B";
                  }
                  else
                  {
                     AV57StatusCnt = 5;
                     AV28NovoStatus = AV55Status;
                  }
               }
               else
               {
                  if ( AV22Divergencia > AV25IndiceDivergencia )
                  {
                     AV57StatusCnt = 7;
                     AV28NovoStatus = "A";
                     AV50Contratada = A490ContagemResultado_ContratadaCod;
                     AV51Servico = A601ContagemResultado_Servico;
                     AV49TipoDias = A1611ContagemResultado_PrzTpDias;
                     /* Execute user subroutine: 'PRAZOCORRECAO' */
                     S121 ();
                     if ( returnInSub )
                     {
                        pr_default.close(3);
                        this.cleanup();
                        if (true) return;
                     }
                     A1511ContagemResultado_InicioCrr = DateTimeUtil.ServerNow( context, "DEFAULT");
                     n1511ContagemResultado_InicioCrr = false;
                     new prc_novocicloexecucao(context ).execute(  AV8Codigo,  DateTimeUtil.ServerNow( context, "DEFAULT"),  AV17PrazoEntrega,  AV49TipoDias,  true) ;
                  }
                  else
                  {
                     AV57StatusCnt = 5;
                     AV28NovoStatus = AV55Status;
                     new prc_setfimcrr(context ).execute( ref  AV14OsVinculada) ;
                  }
               }
               AV17PrazoEntrega = DateTimeUtil.TAdd( AV17PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV17PrazoEntrega = DateTimeUtil.TAdd( AV17PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               /* Using cursor P008K6 */
               pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A517ContagemResultado_Ultima = P008K6_A517ContagemResultado_Ultima[0];
                  A483ContagemResultado_StatusCnt = P008K6_A483ContagemResultado_StatusCnt[0];
                  A462ContagemResultado_Divergencia = P008K6_A462ContagemResultado_Divergencia[0];
                  A470ContagemResultado_ContadorFMCod = P008K6_A470ContagemResultado_ContadorFMCod[0];
                  A800ContagemResultado_Deflator = P008K6_A800ContagemResultado_Deflator[0];
                  n800ContagemResultado_Deflator = P008K6_n800ContagemResultado_Deflator[0];
                  A473ContagemResultado_DataCnt = P008K6_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P008K6_A511ContagemResultado_HoraCnt[0];
                  if ( AV34NovaContagem )
                  {
                     A517ContagemResultado_Ultima = false;
                  }
                  else
                  {
                     A483ContagemResultado_StatusCnt = AV57StatusCnt;
                     A462ContagemResultado_Divergencia = AV22Divergencia;
                  }
                  AV9ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                  AV11ContagemResultado_Deflator = A800ContagemResultado_Deflator;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P008K7 */
                  pr_default.execute(5, new Object[] {A517ContagemResultado_Ultima, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(5);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  if (true) break;
                  /* Using cursor P008K8 */
                  pr_default.execute(6, new Object[] {A517ContagemResultado_Ultima, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               new prc_alterastatusdmn(context ).execute( ref  A456ContagemResultado_Codigo,  AV28NovoStatus,  0) ;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV17PrazoEntrega);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV17PrazoEntrega);
               n912ContagemResultado_HoraEntrega = false;
               A1351ContagemResultado_DataPrevista = AV17PrazoEntrega;
               n1351ContagemResultado_DataPrevista = false;
               /* Using cursor P008K9 */
               pr_default.execute(7, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( AV34NovaContagem )
            {
               /* Execute user subroutine: 'NEWCONTAGEM' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            new prc_inslogresponsavel(context ).execute( ref  AV12ContagemResultado_Codigo,  0,  "N",  "D",  AV18UserId,  0,  AV36StatusDmn,  AV28NovoStatus,  AV31Observacao,  AV17PrazoEntrega,  true) ;
            new prc_disparoservicovinculado(context ).execute(  AV12ContagemResultado_Codigo,  AV18UserId) ;
         }
         AV56OSCodigo = AV8Codigo;
         /* Execute user subroutine: 'STATUS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Using cursor P008K10 */
         pr_default.execute(8, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A517ContagemResultado_Ultima = P008K10_A517ContagemResultado_Ultima[0];
            A456ContagemResultado_Codigo = P008K10_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P008K10_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008K10_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = P008K10_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P008K10_n472ContagemResultado_DataEntrega[0];
            A1553ContagemResultado_CntSrvCod = P008K10_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008K10_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P008K10_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008K10_n1611ContagemResultado_PrzTpDias[0];
            A1618ContagemResultado_PrzRsp = P008K10_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P008K10_n1618ContagemResultado_PrzRsp[0];
            A490ContagemResultado_ContratadaCod = P008K10_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008K10_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P008K10_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P008K10_n601ContagemResultado_Servico[0];
            A805ContagemResultado_ContratadaOrigemCod = P008K10_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P008K10_n805ContagemResultado_ContratadaOrigemCod[0];
            A1511ContagemResultado_InicioCrr = P008K10_A1511ContagemResultado_InicioCrr[0];
            n1511ContagemResultado_InicioCrr = P008K10_n1511ContagemResultado_InicioCrr[0];
            A912ContagemResultado_HoraEntrega = P008K10_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008K10_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = P008K10_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P008K10_n1351ContagemResultado_DataPrevista[0];
            A483ContagemResultado_StatusCnt = P008K10_A483ContagemResultado_StatusCnt[0];
            A462ContagemResultado_Divergencia = P008K10_A462ContagemResultado_Divergencia[0];
            A470ContagemResultado_ContadorFMCod = P008K10_A470ContagemResultado_ContadorFMCod[0];
            A800ContagemResultado_Deflator = P008K10_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P008K10_n800ContagemResultado_Deflator[0];
            A52Contratada_AreaTrabalhoCod = P008K10_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P008K10_n52Contratada_AreaTrabalhoCod[0];
            A890ContagemResultado_Responsavel = P008K10_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P008K10_n890ContagemResultado_Responsavel[0];
            A473ContagemResultado_DataCnt = P008K10_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P008K10_A511ContagemResultado_HoraCnt[0];
            A484ContagemResultado_StatusDmn = P008K10_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008K10_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = P008K10_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P008K10_n472ContagemResultado_DataEntrega[0];
            A1553ContagemResultado_CntSrvCod = P008K10_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008K10_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P008K10_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008K10_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P008K10_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P008K10_n805ContagemResultado_ContratadaOrigemCod[0];
            A1511ContagemResultado_InicioCrr = P008K10_A1511ContagemResultado_InicioCrr[0];
            n1511ContagemResultado_InicioCrr = P008K10_n1511ContagemResultado_InicioCrr[0];
            A912ContagemResultado_HoraEntrega = P008K10_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008K10_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = P008K10_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P008K10_n1351ContagemResultado_DataPrevista[0];
            A890ContagemResultado_Responsavel = P008K10_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P008K10_n890ContagemResultado_Responsavel[0];
            A1611ContagemResultado_PrzTpDias = P008K10_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008K10_n1611ContagemResultado_PrzTpDias[0];
            A1618ContagemResultado_PrzRsp = P008K10_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P008K10_n1618ContagemResultado_PrzRsp[0];
            A601ContagemResultado_Servico = P008K10_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P008K10_n601ContagemResultado_Servico[0];
            A52Contratada_AreaTrabalhoCod = P008K10_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P008K10_n52Contratada_AreaTrabalhoCod[0];
            GXt_int2 = A1229ContagemResultado_ContratadaDoResponsavel;
            new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int2) ;
            A1229ContagemResultado_ContratadaDoResponsavel = GXt_int2;
            AV36StatusDmn = A484ContagemResultado_StatusDmn;
            AV17PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV52ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            AV12ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            AV49TipoDias = A1611ContagemResultado_PrzTpDias;
            AV44Dias = A1618ContagemResultado_PrzRsp;
            if ( AV35EmReuniao )
            {
               if ( AV22Divergencia > AV25IndiceDivergencia )
               {
                  AV57StatusCnt = 7;
                  if ( (0==AV14OsVinculada) )
                  {
                     AV28NovoStatus = "A";
                  }
                  else
                  {
                     AV28NovoStatus = "B";
                  }
                  if ( AV30Responsavel_Codigo > 0 )
                  {
                     A890ContagemResultado_Responsavel = AV30Responsavel_Codigo;
                     n890ContagemResultado_Responsavel = false;
                  }
               }
               else
               {
                  AV57StatusCnt = 5;
                  AV28NovoStatus = AV55Status;
               }
            }
            else
            {
               if ( AV22Divergencia > AV25IndiceDivergencia )
               {
                  AV57StatusCnt = 7;
                  if ( (0==AV14OsVinculada) )
                  {
                     AV28NovoStatus = "A";
                     AV50Contratada = A490ContagemResultado_ContratadaCod;
                     AV51Servico = A601ContagemResultado_Servico;
                     AV49TipoDias = A1611ContagemResultado_PrzTpDias;
                     if ( AV59UsuarioDaContratada && P008K10_n805ContagemResultado_ContratadaOrigemCod[0] )
                     {
                        /* Execute user subroutine: 'PRAZORESPOSTA' */
                        S131 ();
                        if ( returnInSub )
                        {
                           pr_default.close(8);
                           this.cleanup();
                           if (true) return;
                        }
                     }
                     else
                     {
                        /* Execute user subroutine: 'PRAZOCORRECAO' */
                        S121 ();
                        if ( returnInSub )
                        {
                           pr_default.close(8);
                           this.cleanup();
                           if (true) return;
                        }
                        A1511ContagemResultado_InicioCrr = DateTimeUtil.ServerNow( context, "DEFAULT");
                        n1511ContagemResultado_InicioCrr = false;
                        new prc_novocicloexecucao(context ).execute(  AV8Codigo,  DateTimeUtil.ServerNow( context, "DEFAULT"),  AV17PrazoEntrega,  AV49TipoDias,  true) ;
                     }
                     if ( AV30Responsavel_Codigo > 0 )
                     {
                        A890ContagemResultado_Responsavel = AV30Responsavel_Codigo;
                        n890ContagemResultado_Responsavel = false;
                     }
                  }
                  else
                  {
                     AV28NovoStatus = "B";
                  }
               }
               else
               {
                  AV57StatusCnt = 5;
                  AV28NovoStatus = AV55Status;
                  new prc_setfimcrr(context ).execute( ref  AV14OsVinculada) ;
               }
               AV17PrazoEntrega = DateTimeUtil.TAdd( AV17PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV17PrazoEntrega = DateTimeUtil.TAdd( AV17PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               GXt_dtime3 = DateTimeUtil.ServerNow( context, "DEFAULT");
               new prc_setdataentregaexecucao(context ).execute( ref  A456ContagemResultado_Codigo, ref  GXt_dtime3) ;
               new prc_alterastatusdmn(context ).execute( ref  A456ContagemResultado_Codigo,  AV28NovoStatus,  0) ;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV17PrazoEntrega);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV17PrazoEntrega);
               n912ContagemResultado_HoraEntrega = false;
               if ( A1229ContagemResultado_ContratadaDoResponsavel == A490ContagemResultado_ContratadaCod )
               {
                  A1351ContagemResultado_DataPrevista = AV17PrazoEntrega;
                  n1351ContagemResultado_DataPrevista = false;
               }
            }
            if ( AV34NovaContagem )
            {
               A517ContagemResultado_Ultima = false;
            }
            else
            {
               A483ContagemResultado_StatusCnt = AV57StatusCnt;
               A462ContagemResultado_Divergencia = AV22Divergencia;
            }
            AV9ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            AV11ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            if ( (0==AV14OsVinculada) )
            {
               new prc_inslogresponsavel(context ).execute( ref  AV8Codigo,  AV30Responsavel_Codigo,  "N",  "D",  AV18UserId,  0,  AV36StatusDmn,  AV28NovoStatus,  AV31Observacao,  AV17PrazoEntrega,  true) ;
            }
            else
            {
               new prc_inslogresponsavel(context ).execute( ref  AV8Codigo,  0,  "N",  "D",  AV18UserId,  0,  AV36StatusDmn,  AV28NovoStatus,  AV31Observacao,  AV17PrazoEntrega,  true) ;
            }
            new prc_disparoservicovinculado(context ).execute(  AV8Codigo,  AV18UserId) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P008K11 */
            pr_default.execute(9, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P008K12 */
            pr_default.execute(10, new Object[] {A517ContagemResultado_Ultima, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P008K13 */
            pr_default.execute(11, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n1511ContagemResultado_InicioCrr, A1511ContagemResultado_InicioCrr, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P008K14 */
            pr_default.execute(12, new Object[] {A517ContagemResultado_Ultima, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(8);
         }
         pr_default.close(8);
         if ( AV34NovaContagem )
         {
            /* Execute user subroutine: 'NEWCONTAGEM' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWCONTAGEM' Routine */
         GXt_char4 = AV53CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  AV52ContratoServicos_Codigo, ref  AV9ContagemResultado_ContadorFMCod, ref  AV54CstUntNrm, out  GXt_char4) ;
         AV53CalculoPFinal = GXt_char4;
         AV13Hora = context.localUtil.ServerTime( context, "DEFAULT");
         /*
            INSERT RECORD ON TABLE ContagemResultadoContagens

         */
         A456ContagemResultado_Codigo = AV12ContagemResultado_Codigo;
         A473ContagemResultado_DataCnt = DateTimeUtil.ServerDate( context, "DEFAULT");
         A511ContagemResultado_HoraCnt = AV13Hora;
         A460ContagemResultado_PFBFM = AV37ContagemResultado_PFBFM;
         n460ContagemResultado_PFBFM = false;
         A458ContagemResultado_PFBFS = AV38ContagemResultado_PFBFS;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = AV40ContagemResultado_PFLFS;
         n459ContagemResultado_PFLFS = false;
         A461ContagemResultado_PFLFM = AV39ContagemResultado_PFLFM;
         n461ContagemResultado_PFLFM = false;
         A483ContagemResultado_StatusCnt = AV57StatusCnt;
         A800ContagemResultado_Deflator = AV11ContagemResultado_Deflator;
         n800ContagemResultado_Deflator = false;
         A833ContagemResultado_CstUntPrd = AV54CstUntNrm;
         n833ContagemResultado_CstUntPrd = false;
         A462ContagemResultado_Divergencia = AV22Divergencia;
         A482ContagemResultadoContagens_Esforco = 0;
         A470ContagemResultado_ContadorFMCod = AV9ContagemResultado_ContadorFMCod;
         A852ContagemResultado_Planilha = "";
         n852ContagemResultado_Planilha = false;
         n852ContagemResultado_Planilha = true;
         A853ContagemResultado_NomePla = "";
         n853ContagemResultado_NomePla = false;
         n853ContagemResultado_NomePla = true;
         A854ContagemResultado_TipoPla = "";
         n854ContagemResultado_TipoPla = false;
         n854ContagemResultado_TipoPla = true;
         A469ContagemResultado_NaoCnfCntCod = 0;
         n469ContagemResultado_NaoCnfCntCod = false;
         n469ContagemResultado_NaoCnfCntCod = true;
         A463ContagemResultado_ParecerTcn = "";
         n463ContagemResultado_ParecerTcn = false;
         n463ContagemResultado_ParecerTcn = true;
         A517ContagemResultado_Ultima = true;
         /* Using cursor P008K15 */
         A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
         n853ContagemResultado_NomePla = false;
         A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
         n854ContagemResultado_TipoPla = false;
         pr_default.execute(13, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla});
         pr_default.close(13);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         if ( (pr_default.getStatus(13) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            GXt_char4 = AV13Hora;
            new prc_horasnominutoseguinte(context ).execute(  AV13Hora, out  GXt_char4) ;
            AV13Hora = GXt_char4;
            /*
               INSERT RECORD ON TABLE ContagemResultadoContagens

            */
            W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            W460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            W458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            W459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            W461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            W483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            W800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            W833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
            n833ContagemResultado_CstUntPrd = false;
            W462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            W482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
            W470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            W852ContagemResultado_Planilha = A852ContagemResultado_Planilha;
            n852ContagemResultado_Planilha = false;
            W853ContagemResultado_NomePla = A853ContagemResultado_NomePla;
            n853ContagemResultado_NomePla = false;
            W854ContagemResultado_TipoPla = A854ContagemResultado_TipoPla;
            n854ContagemResultado_TipoPla = false;
            W469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
            W463ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            W517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
            A456ContagemResultado_Codigo = AV12ContagemResultado_Codigo;
            A473ContagemResultado_DataCnt = DateTimeUtil.ServerDate( context, "DEFAULT");
            A511ContagemResultado_HoraCnt = AV13Hora;
            A460ContagemResultado_PFBFM = AV37ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A458ContagemResultado_PFBFS = AV38ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            A459ContagemResultado_PFLFS = AV40ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            A461ContagemResultado_PFLFM = AV39ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            A483ContagemResultado_StatusCnt = AV57StatusCnt;
            A800ContagemResultado_Deflator = AV11ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            A833ContagemResultado_CstUntPrd = AV54CstUntNrm;
            n833ContagemResultado_CstUntPrd = false;
            A462ContagemResultado_Divergencia = AV22Divergencia;
            A482ContagemResultadoContagens_Esforco = 0;
            A470ContagemResultado_ContadorFMCod = AV9ContagemResultado_ContadorFMCod;
            A852ContagemResultado_Planilha = "";
            n852ContagemResultado_Planilha = false;
            n852ContagemResultado_Planilha = true;
            A853ContagemResultado_NomePla = "";
            n853ContagemResultado_NomePla = false;
            n853ContagemResultado_NomePla = true;
            A854ContagemResultado_TipoPla = "";
            n854ContagemResultado_TipoPla = false;
            n854ContagemResultado_TipoPla = true;
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            n469ContagemResultado_NaoCnfCntCod = true;
            A463ContagemResultado_ParecerTcn = "";
            n463ContagemResultado_ParecerTcn = false;
            n463ContagemResultado_ParecerTcn = true;
            A517ContagemResultado_Ultima = true;
            /* Using cursor P008K16 */
            A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
            n854ContagemResultado_TipoPla = false;
            pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if ( (pr_default.getStatus(14) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
            A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
            A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
            A460ContagemResultado_PFBFM = W460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A458ContagemResultado_PFBFS = W458ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            A459ContagemResultado_PFLFS = W459ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            A461ContagemResultado_PFLFM = W461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            A483ContagemResultado_StatusCnt = W483ContagemResultado_StatusCnt;
            A800ContagemResultado_Deflator = W800ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            A833ContagemResultado_CstUntPrd = W833ContagemResultado_CstUntPrd;
            n833ContagemResultado_CstUntPrd = false;
            A462ContagemResultado_Divergencia = W462ContagemResultado_Divergencia;
            A482ContagemResultadoContagens_Esforco = W482ContagemResultadoContagens_Esforco;
            A470ContagemResultado_ContadorFMCod = W470ContagemResultado_ContadorFMCod;
            A852ContagemResultado_Planilha = W852ContagemResultado_Planilha;
            n852ContagemResultado_Planilha = false;
            A853ContagemResultado_NomePla = W853ContagemResultado_NomePla;
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = W854ContagemResultado_TipoPla;
            n854ContagemResultado_TipoPla = false;
            A469ContagemResultado_NaoCnfCntCod = W469ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
            A463ContagemResultado_ParecerTcn = W463ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            A517ContagemResultado_Ultima = W517ContagemResultado_Ultima;
            /* End Insert */
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S121( )
      {
         /* 'PRAZOCORRECAO' Routine */
         AV17PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         GXt_int5 = AV44Dias;
         new prc_diasparacorrecao(context ).execute( ref  AV52ContratoServicos_Codigo,  AV12ContagemResultado_Codigo, out  GXt_int5) ;
         AV44Dias = GXt_int5;
         GXt_dtime3 = AV17PrazoEntrega;
         new prc_adddiasuteis(context ).execute(  AV17PrazoEntrega,  AV44Dias,  AV49TipoDias, out  GXt_dtime3) ;
         AV17PrazoEntrega = GXt_dtime3;
      }

      protected void S131( )
      {
         /* 'PRAZORESPOSTA' Routine */
         AV17PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         if ( (0==AV44Dias) )
         {
            AV44Dias = 5;
         }
         GXt_dtime3 = AV17PrazoEntrega;
         new prc_adddiasuteis(context ).execute(  AV17PrazoEntrega,  AV44Dias,  AV49TipoDias, out  GXt_dtime3) ;
         AV17PrazoEntrega = GXt_dtime3;
      }

      protected void S141( )
      {
         /* 'STATUS' Routine */
         AV55Status = "R";
         if ( new prc_estevecancelada(context).executeUdp( ref  AV56OSCodigo) )
         {
            AV55Status = "X";
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NaoAcata");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008K2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008K2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008K2_A460ContagemResultado_PFBFM = new decimal[1] ;
         P008K2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P008K2_A461ContagemResultado_PFLFM = new decimal[1] ;
         P008K2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P008K2_A458ContagemResultado_PFBFS = new decimal[1] ;
         P008K2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P008K2_A459ContagemResultado_PFLFS = new decimal[1] ;
         P008K2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P008K2_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008K2_A456ContagemResultado_Codigo = new int[1] ;
         P008K2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008K2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         P008K3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P008K3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P008K4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008K4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008K4_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008K4_A456ContagemResultado_Codigo = new int[1] ;
         P008K4_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P008K4_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P008K4_A458ContagemResultado_PFBFS = new decimal[1] ;
         P008K4_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P008K4_A459ContagemResultado_PFLFS = new decimal[1] ;
         P008K4_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P008K4_A460ContagemResultado_PFBFM = new decimal[1] ;
         P008K4_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P008K4_A461ContagemResultado_PFLFM = new decimal[1] ;
         P008K4_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P008K4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008K4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         AV24CalculoDivergencia = "";
         P008K5_A456ContagemResultado_Codigo = new int[1] ;
         P008K5_A890ContagemResultado_Responsavel = new int[1] ;
         P008K5_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P008K5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008K5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008K5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008K5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008K5_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008K5_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008K5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008K5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008K5_A601ContagemResultado_Servico = new int[1] ;
         P008K5_n601ContagemResultado_Servico = new bool[] {false} ;
         P008K5_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008K5_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008K5_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P008K5_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P008K5_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008K5_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008K5_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P008K5_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1611ContagemResultado_PrzTpDias = "";
         A1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         AV36StatusDmn = "";
         AV17PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV28NovoStatus = "";
         AV55Status = "";
         AV49TipoDias = "";
         P008K6_A456ContagemResultado_Codigo = new int[1] ;
         P008K6_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008K6_A483ContagemResultado_StatusCnt = new short[1] ;
         P008K6_A462ContagemResultado_Divergencia = new decimal[1] ;
         P008K6_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P008K6_A800ContagemResultado_Deflator = new decimal[1] ;
         P008K6_n800ContagemResultado_Deflator = new bool[] {false} ;
         P008K6_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008K6_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P008K10_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008K10_A456ContagemResultado_Codigo = new int[1] ;
         P008K10_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008K10_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008K10_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008K10_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008K10_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008K10_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008K10_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008K10_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008K10_A1618ContagemResultado_PrzRsp = new short[1] ;
         P008K10_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         P008K10_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008K10_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008K10_A601ContagemResultado_Servico = new int[1] ;
         P008K10_n601ContagemResultado_Servico = new bool[] {false} ;
         P008K10_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P008K10_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P008K10_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P008K10_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P008K10_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008K10_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008K10_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P008K10_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P008K10_A483ContagemResultado_StatusCnt = new short[1] ;
         P008K10_A462ContagemResultado_Divergencia = new decimal[1] ;
         P008K10_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P008K10_A800ContagemResultado_Deflator = new decimal[1] ;
         P008K10_n800ContagemResultado_Deflator = new bool[] {false} ;
         P008K10_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P008K10_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P008K10_A890ContagemResultado_Responsavel = new int[1] ;
         P008K10_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P008K10_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008K10_A511ContagemResultado_HoraCnt = new String[] {""} ;
         AV53CalculoPFinal = "";
         AV13Hora = "";
         A852ContagemResultado_Planilha = "";
         A853ContagemResultado_NomePla = "";
         A854ContagemResultado_TipoPla = "";
         A463ContagemResultado_ParecerTcn = "";
         Gx_emsg = "";
         GXt_char4 = "";
         W473ContagemResultado_DataCnt = DateTime.MinValue;
         W511ContagemResultado_HoraCnt = "";
         W852ContagemResultado_Planilha = "";
         W853ContagemResultado_NomePla = "";
         W854ContagemResultado_TipoPla = "";
         W463ContagemResultado_ParecerTcn = "";
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_naoacata__default(),
            new Object[][] {
                new Object[] {
               P008K2_A490ContagemResultado_ContratadaCod, P008K2_n490ContagemResultado_ContratadaCod, P008K2_A460ContagemResultado_PFBFM, P008K2_n460ContagemResultado_PFBFM, P008K2_A461ContagemResultado_PFLFM, P008K2_n461ContagemResultado_PFLFM, P008K2_A458ContagemResultado_PFBFS, P008K2_n458ContagemResultado_PFBFS, P008K2_A459ContagemResultado_PFLFS, P008K2_n459ContagemResultado_PFLFS,
               P008K2_A517ContagemResultado_Ultima, P008K2_A456ContagemResultado_Codigo, P008K2_A473ContagemResultado_DataCnt, P008K2_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P008K3_A66ContratadaUsuario_ContratadaCod, P008K3_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P008K4_A490ContagemResultado_ContratadaCod, P008K4_n490ContagemResultado_ContratadaCod, P008K4_A517ContagemResultado_Ultima, P008K4_A456ContagemResultado_Codigo, P008K4_A1326ContagemResultado_ContratadaTipoFab, P008K4_n1326ContagemResultado_ContratadaTipoFab, P008K4_A458ContagemResultado_PFBFS, P008K4_n458ContagemResultado_PFBFS, P008K4_A459ContagemResultado_PFLFS, P008K4_n459ContagemResultado_PFLFS,
               P008K4_A460ContagemResultado_PFBFM, P008K4_n460ContagemResultado_PFBFM, P008K4_A461ContagemResultado_PFLFM, P008K4_n461ContagemResultado_PFLFM, P008K4_A473ContagemResultado_DataCnt, P008K4_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P008K5_A456ContagemResultado_Codigo, P008K5_A890ContagemResultado_Responsavel, P008K5_n890ContagemResultado_Responsavel, P008K5_A1553ContagemResultado_CntSrvCod, P008K5_n1553ContagemResultado_CntSrvCod, P008K5_A484ContagemResultado_StatusDmn, P008K5_n484ContagemResultado_StatusDmn, P008K5_A472ContagemResultado_DataEntrega, P008K5_n472ContagemResultado_DataEntrega, P008K5_A490ContagemResultado_ContratadaCod,
               P008K5_n490ContagemResultado_ContratadaCod, P008K5_A601ContagemResultado_Servico, P008K5_n601ContagemResultado_Servico, P008K5_A1611ContagemResultado_PrzTpDias, P008K5_n1611ContagemResultado_PrzTpDias, P008K5_A1511ContagemResultado_InicioCrr, P008K5_n1511ContagemResultado_InicioCrr, P008K5_A912ContagemResultado_HoraEntrega, P008K5_n912ContagemResultado_HoraEntrega, P008K5_A1351ContagemResultado_DataPrevista,
               P008K5_n1351ContagemResultado_DataPrevista
               }
               , new Object[] {
               P008K6_A456ContagemResultado_Codigo, P008K6_A517ContagemResultado_Ultima, P008K6_A483ContagemResultado_StatusCnt, P008K6_A462ContagemResultado_Divergencia, P008K6_A470ContagemResultado_ContadorFMCod, P008K6_A800ContagemResultado_Deflator, P008K6_n800ContagemResultado_Deflator, P008K6_A473ContagemResultado_DataCnt, P008K6_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008K10_A517ContagemResultado_Ultima, P008K10_A456ContagemResultado_Codigo, P008K10_A484ContagemResultado_StatusDmn, P008K10_n484ContagemResultado_StatusDmn, P008K10_A472ContagemResultado_DataEntrega, P008K10_n472ContagemResultado_DataEntrega, P008K10_A1553ContagemResultado_CntSrvCod, P008K10_n1553ContagemResultado_CntSrvCod, P008K10_A1611ContagemResultado_PrzTpDias, P008K10_n1611ContagemResultado_PrzTpDias,
               P008K10_A1618ContagemResultado_PrzRsp, P008K10_n1618ContagemResultado_PrzRsp, P008K10_A490ContagemResultado_ContratadaCod, P008K10_n490ContagemResultado_ContratadaCod, P008K10_A601ContagemResultado_Servico, P008K10_n601ContagemResultado_Servico, P008K10_A805ContagemResultado_ContratadaOrigemCod, P008K10_n805ContagemResultado_ContratadaOrigemCod, P008K10_A1511ContagemResultado_InicioCrr, P008K10_n1511ContagemResultado_InicioCrr,
               P008K10_A912ContagemResultado_HoraEntrega, P008K10_n912ContagemResultado_HoraEntrega, P008K10_A1351ContagemResultado_DataPrevista, P008K10_n1351ContagemResultado_DataPrevista, P008K10_A483ContagemResultado_StatusCnt, P008K10_A462ContagemResultado_Divergencia, P008K10_A470ContagemResultado_ContadorFMCod, P008K10_A800ContagemResultado_Deflator, P008K10_n800ContagemResultado_Deflator, P008K10_A52Contratada_AreaTrabalhoCod,
               P008K10_n52Contratada_AreaTrabalhoCod, P008K10_A890ContagemResultado_Responsavel, P008K10_n890ContagemResultado_Responsavel, P008K10_A473ContagemResultado_DataCnt, P008K10_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV63GXLvl8 ;
      private short AV57StatusCnt ;
      private short A483ContagemResultado_StatusCnt ;
      private short A1618ContagemResultado_PrzRsp ;
      private short AV44Dias ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short W483ContagemResultado_StatusCnt ;
      private short W482ContagemResultadoContagens_Esforco ;
      private short GXt_int5 ;
      private int AV8Codigo ;
      private int AV14OsVinculada ;
      private int AV18UserId ;
      private int AV30Responsavel_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV56OSCodigo ;
      private int A890ContagemResultado_Responsavel ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int AV12ContagemResultado_Codigo ;
      private int AV48Deestinatario ;
      private int AV52ContratoServicos_Codigo ;
      private int AV50Contratada ;
      private int AV51Servico ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int AV9ContagemResultado_ContadorFMCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int GXt_int2 ;
      private int GX_INS72 ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int W456ContagemResultado_Codigo ;
      private int W470ContagemResultado_ContadorFMCod ;
      private int W469ContagemResultado_NaoCnfCntCod ;
      private decimal AV15PFB ;
      private decimal AV16PFL ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal AV37ContagemResultado_PFBFM ;
      private decimal AV39ContagemResultado_PFLFM ;
      private decimal AV38ContagemResultado_PFBFS ;
      private decimal AV40ContagemResultado_PFLFS ;
      private decimal AV25IndiceDivergencia ;
      private decimal AV23ContagemResultado_ValorPF ;
      private decimal AV22Divergencia ;
      private decimal GXt_decimal1 ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal AV11ContagemResultado_Deflator ;
      private decimal AV54CstUntNrm ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal W460ContagemResultado_PFBFM ;
      private decimal W458ContagemResultado_PFBFS ;
      private decimal W459ContagemResultado_PFLFS ;
      private decimal W461ContagemResultado_PFLFM ;
      private decimal W800ContagemResultado_Deflator ;
      private decimal W833ContagemResultado_CstUntPrd ;
      private decimal W462ContagemResultado_Divergencia ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String AV24CalculoDivergencia ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV36StatusDmn ;
      private String AV28NovoStatus ;
      private String AV55Status ;
      private String AV49TipoDias ;
      private String AV53CalculoPFinal ;
      private String AV13Hora ;
      private String A853ContagemResultado_NomePla ;
      private String A854ContagemResultado_TipoPla ;
      private String Gx_emsg ;
      private String GXt_char4 ;
      private String W511ContagemResultado_HoraCnt ;
      private String W853ContagemResultado_NomePla ;
      private String W854ContagemResultado_TipoPla ;
      private DateTime A1511ContagemResultado_InicioCrr ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV17PrazoEntrega ;
      private DateTime GXt_dtime3 ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime W473ContagemResultado_DataCnt ;
      private bool AV34NovaContagem ;
      private bool AV35EmReuniao ;
      private bool AV47NaoAcatou ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool A517ContagemResultado_Ultima ;
      private bool AV59UsuarioDaContratada ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool returnInSub ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n601ContagemResultado_Servico ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1511ContagemResultado_InicioCrr ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n800ContagemResultado_Deflator ;
      private bool n1618ContagemResultado_PrzRsp ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n852ContagemResultado_Planilha ;
      private bool n853ContagemResultado_NomePla ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool W517ContagemResultado_Ultima ;
      private String AV31Observacao ;
      private String A463ContagemResultado_ParecerTcn ;
      private String W463ContagemResultado_ParecerTcn ;
      private String A852ContagemResultado_Planilha ;
      private String W852ContagemResultado_Planilha ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008K2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008K2_n490ContagemResultado_ContratadaCod ;
      private decimal[] P008K2_A460ContagemResultado_PFBFM ;
      private bool[] P008K2_n460ContagemResultado_PFBFM ;
      private decimal[] P008K2_A461ContagemResultado_PFLFM ;
      private bool[] P008K2_n461ContagemResultado_PFLFM ;
      private decimal[] P008K2_A458ContagemResultado_PFBFS ;
      private bool[] P008K2_n458ContagemResultado_PFBFS ;
      private decimal[] P008K2_A459ContagemResultado_PFLFS ;
      private bool[] P008K2_n459ContagemResultado_PFLFS ;
      private bool[] P008K2_A517ContagemResultado_Ultima ;
      private int[] P008K2_A456ContagemResultado_Codigo ;
      private DateTime[] P008K2_A473ContagemResultado_DataCnt ;
      private String[] P008K2_A511ContagemResultado_HoraCnt ;
      private int[] P008K3_A66ContratadaUsuario_ContratadaCod ;
      private int[] P008K3_A69ContratadaUsuario_UsuarioCod ;
      private int[] P008K4_A490ContagemResultado_ContratadaCod ;
      private bool[] P008K4_n490ContagemResultado_ContratadaCod ;
      private bool[] P008K4_A517ContagemResultado_Ultima ;
      private int[] P008K4_A456ContagemResultado_Codigo ;
      private String[] P008K4_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P008K4_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P008K4_A458ContagemResultado_PFBFS ;
      private bool[] P008K4_n458ContagemResultado_PFBFS ;
      private decimal[] P008K4_A459ContagemResultado_PFLFS ;
      private bool[] P008K4_n459ContagemResultado_PFLFS ;
      private decimal[] P008K4_A460ContagemResultado_PFBFM ;
      private bool[] P008K4_n460ContagemResultado_PFBFM ;
      private decimal[] P008K4_A461ContagemResultado_PFLFM ;
      private bool[] P008K4_n461ContagemResultado_PFLFM ;
      private DateTime[] P008K4_A473ContagemResultado_DataCnt ;
      private String[] P008K4_A511ContagemResultado_HoraCnt ;
      private int[] P008K5_A456ContagemResultado_Codigo ;
      private int[] P008K5_A890ContagemResultado_Responsavel ;
      private bool[] P008K5_n890ContagemResultado_Responsavel ;
      private int[] P008K5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008K5_n1553ContagemResultado_CntSrvCod ;
      private String[] P008K5_A484ContagemResultado_StatusDmn ;
      private bool[] P008K5_n484ContagemResultado_StatusDmn ;
      private DateTime[] P008K5_A472ContagemResultado_DataEntrega ;
      private bool[] P008K5_n472ContagemResultado_DataEntrega ;
      private int[] P008K5_A490ContagemResultado_ContratadaCod ;
      private bool[] P008K5_n490ContagemResultado_ContratadaCod ;
      private int[] P008K5_A601ContagemResultado_Servico ;
      private bool[] P008K5_n601ContagemResultado_Servico ;
      private String[] P008K5_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008K5_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P008K5_A1511ContagemResultado_InicioCrr ;
      private bool[] P008K5_n1511ContagemResultado_InicioCrr ;
      private DateTime[] P008K5_A912ContagemResultado_HoraEntrega ;
      private bool[] P008K5_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P008K5_A1351ContagemResultado_DataPrevista ;
      private bool[] P008K5_n1351ContagemResultado_DataPrevista ;
      private int[] P008K6_A456ContagemResultado_Codigo ;
      private bool[] P008K6_A517ContagemResultado_Ultima ;
      private short[] P008K6_A483ContagemResultado_StatusCnt ;
      private decimal[] P008K6_A462ContagemResultado_Divergencia ;
      private int[] P008K6_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P008K6_A800ContagemResultado_Deflator ;
      private bool[] P008K6_n800ContagemResultado_Deflator ;
      private DateTime[] P008K6_A473ContagemResultado_DataCnt ;
      private String[] P008K6_A511ContagemResultado_HoraCnt ;
      private bool[] P008K10_A517ContagemResultado_Ultima ;
      private int[] P008K10_A456ContagemResultado_Codigo ;
      private String[] P008K10_A484ContagemResultado_StatusDmn ;
      private bool[] P008K10_n484ContagemResultado_StatusDmn ;
      private DateTime[] P008K10_A472ContagemResultado_DataEntrega ;
      private bool[] P008K10_n472ContagemResultado_DataEntrega ;
      private int[] P008K10_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008K10_n1553ContagemResultado_CntSrvCod ;
      private String[] P008K10_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008K10_n1611ContagemResultado_PrzTpDias ;
      private short[] P008K10_A1618ContagemResultado_PrzRsp ;
      private bool[] P008K10_n1618ContagemResultado_PrzRsp ;
      private int[] P008K10_A490ContagemResultado_ContratadaCod ;
      private bool[] P008K10_n490ContagemResultado_ContratadaCod ;
      private int[] P008K10_A601ContagemResultado_Servico ;
      private bool[] P008K10_n601ContagemResultado_Servico ;
      private int[] P008K10_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P008K10_n805ContagemResultado_ContratadaOrigemCod ;
      private DateTime[] P008K10_A1511ContagemResultado_InicioCrr ;
      private bool[] P008K10_n1511ContagemResultado_InicioCrr ;
      private DateTime[] P008K10_A912ContagemResultado_HoraEntrega ;
      private bool[] P008K10_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P008K10_A1351ContagemResultado_DataPrevista ;
      private bool[] P008K10_n1351ContagemResultado_DataPrevista ;
      private short[] P008K10_A483ContagemResultado_StatusCnt ;
      private decimal[] P008K10_A462ContagemResultado_Divergencia ;
      private int[] P008K10_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P008K10_A800ContagemResultado_Deflator ;
      private bool[] P008K10_n800ContagemResultado_Deflator ;
      private int[] P008K10_A52Contratada_AreaTrabalhoCod ;
      private bool[] P008K10_n52Contratada_AreaTrabalhoCod ;
      private int[] P008K10_A890ContagemResultado_Responsavel ;
      private bool[] P008K10_n890ContagemResultado_Responsavel ;
      private DateTime[] P008K10_A473ContagemResultado_DataCnt ;
      private String[] P008K10_A511ContagemResultado_HoraCnt ;
   }

   public class prc_naoacata__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008K2 ;
          prmP008K2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K3 ;
          prmP008K3 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K4 ;
          prmP008K4 = new Object[] {
          new Object[] {"@AV14OsVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K5 ;
          prmP008K5 = new Object[] {
          new Object[] {"@AV14OsVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K6 ;
          prmP008K6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K7 ;
          prmP008K7 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008K8 ;
          prmP008K8 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008K9 ;
          prmP008K9 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K10 ;
          prmP008K10 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K11 ;
          prmP008K11 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K12 ;
          prmP008K12 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008K13 ;
          prmP008K13 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_InicioCrr",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008K14 ;
          prmP008K14 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008K15 ;
          prmP008K15 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0}
          } ;
          Object[] prmP008K16 ;
          prmP008K16 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008K2", "SELECT TOP 1 T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV8Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008K2,1,0,true,true )
             ,new CursorDef("P008K3", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @AV18UserId ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008K3,1,0,false,true )
             ,new CursorDef("P008K4", "SELECT TOP 1 T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV14OsVinculada) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008K4,1,0,false,true )
             ,new CursorDef("P008K5", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_InicioCrr], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataPrevista] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV14OsVinculada ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008K5,1,0,true,true )
             ,new CursorDef("P008K6", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_StatusCnt], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_Deflator], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008K6,1,0,true,true )
             ,new CursorDef("P008K7", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K7)
             ,new CursorDef("P008K8", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K8)
             ,new CursorDef("P008K9", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K9)
             ,new CursorDef("P008K10", "SELECT TOP 1 T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T3.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_ContratadaOrigemCod], T2.[ContagemResultado_InicioCrr], T2.[ContagemResultado_HoraEntrega], T2.[ContagemResultado_DataPrevista], T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_ContadorFMCod], T1.[ContagemResultado_Deflator], T4.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_Responsavel], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ((([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV8Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008K10,1,0,true,true )
             ,new CursorDef("P008K11", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K11)
             ,new CursorDef("P008K12", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K12)
             ,new CursorDef("P008K13", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_InicioCrr]=@ContagemResultado_InicioCrr, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K13)
             ,new CursorDef("P008K14", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008K14)
             ,new CursorDef("P008K15", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP008K15)
             ,new CursorDef("P008K16", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP008K16)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(8) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[15])[0] = rslt.getString(10, 5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 5) ;
                return;
             case 8 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((short[]) buf[24])[0] = rslt.getShort(14) ;
                ((decimal[]) buf[25])[0] = rslt.getDecimal(15) ;
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((decimal[]) buf[27])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[33])[0] = rslt.getGXDate(20) ;
                ((String[]) buf[34])[0] = rslt.getString(21, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (DateTime)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 6 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (DateTime)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 10 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (DateTime)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 12 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (DateTime)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                stmt.SetParameter(10, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (short)parms[17]);
                stmt.SetParameter(13, (short)parms[18]);
                stmt.SetParameter(14, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                stmt.SetParameter(10, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (short)parms[17]);
                stmt.SetParameter(13, (short)parms[18]);
                stmt.SetParameter(14, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                return;
       }
    }

 }

}
