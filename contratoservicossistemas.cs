/*
               File: ContratoServicosSistemas
        Description: Sistemas do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:58.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicossistemas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A160ContratoServicos_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1067ContratoServicosSistemas_ServicoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A1063ContratoServicosSistemas_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A1063ContratoServicosSistemas_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContratoServicosSistemas_ServicoCod.Name = "CONTRATOSERVICOSSISTEMAS_SERVICOCOD";
         dynContratoServicosSistemas_ServicoCod.WebTags = "";
         dynContratoServicosSistemas_ServicoCod.removeAllItems();
         /* Using cursor T00347 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            dynContratoServicosSistemas_ServicoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T00347_A155Servico_Codigo[0]), 6, 0)), T00347_A608Servico_Nome[0], 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( dynContratoServicosSistemas_ServicoCod.ItemCount > 0 )
         {
            A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( dynContratoServicosSistemas_ServicoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Sistemas do Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoServicos_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicossistemas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicossistemas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContratoServicosSistemas_ServicoCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContratoServicosSistemas_ServicoCod.ItemCount > 0 )
         {
            A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( dynContratoServicosSistemas_ServicoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_34128( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_34128e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_34128( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_34128( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_34128e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Sistemas do Servi�o", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContratoServicosSistemas.htm");
            wb_table3_28_34128( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_34128e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_34128e( true) ;
         }
         else
         {
            wb_table1_2_34128e( false) ;
         }
      }

      protected void wb_table3_28_34128( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_34128( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_34128e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_34128e( true) ;
         }
         else
         {
            wb_table3_28_34128e( false) ;
         }
      }

      protected void wb_table4_34_34128( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_codigo_Internalname, "C�digo do Servi�o", "", "", lblTextblockcontratoservicos_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), ((edtContratoServicos_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicos_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicossistemas_servicocod_Internalname, "Nome", "", "", lblTextblockcontratoservicossistemas_servicocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratoServicosSistemas_ServicoCod, dynContratoServicosSistemas_ServicoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)), 1, dynContratoServicosSistemas_ServicoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratoServicosSistemas_ServicoCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_ContratoServicosSistemas.htm");
            dynContratoServicosSistemas_ServicoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosSistemas_ServicoCod_Internalname, "Values", (String)(dynContratoServicosSistemas_ServicoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicossistemas_servicosigla_Internalname, "Sistemas_Servico Sigla", "", "", lblTextblockcontratoservicossistemas_servicosigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosSistemas_ServicoSigla_Internalname, StringUtil.RTrim( A1068ContratoServicosSistemas_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A1068ContratoServicosSistemas_ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosSistemas_ServicoSigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicosSistemas_ServicoSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicossistemas_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontratoservicossistemas_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosSistemas_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0, ",", "")), ((edtContratoServicosSistemas_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1063ContratoServicosSistemas_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1063ContratoServicosSistemas_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosSistemas_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicosSistemas_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicossistemas_sistemasigla_Internalname, "Sistema", "", "", lblTextblockcontratoservicossistemas_sistemasigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosSistemas_SistemaSigla_Internalname, StringUtil.RTrim( A1064ContratoServicosSistemas_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( A1064ContratoServicosSistemas_SistemaSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosSistemas_SistemaSigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicosSistemas_SistemaSigla_Enabled, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_34128e( true) ;
         }
         else
         {
            wb_table4_34_34128e( false) ;
         }
      }

      protected void wb_table2_5_34128( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosSistemas.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_34128e( true) ;
         }
         else
         {
            wb_table2_5_34128e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A160ContratoServicos_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               }
               else
               {
                  A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               }
               dynContratoServicosSistemas_ServicoCod.CurrentValue = cgiGet( dynContratoServicosSistemas_ServicoCod_Internalname);
               A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( cgiGet( dynContratoServicosSistemas_ServicoCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
               A1068ContratoServicosSistemas_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoServicosSistemas_ServicoSigla_Internalname));
               n1068ContratoServicosSistemas_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosSistemas_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosSistemas_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosSistemas_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1063ContratoServicosSistemas_SistemaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
               }
               else
               {
                  A1063ContratoServicosSistemas_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosSistemas_SistemaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
               }
               A1064ContratoServicosSistemas_SistemaSigla = StringUtil.Upper( cgiGet( edtContratoServicosSistemas_SistemaSigla_Internalname));
               n1064ContratoServicosSistemas_SistemaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
               /* Read saved values. */
               Z160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z160ContratoServicos_Codigo"), ",", "."));
               Z1067ContratoServicosSistemas_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1067ContratoServicosSistemas_ServicoCod"), ",", "."));
               Z1063ContratoServicosSistemas_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z1063ContratoServicosSistemas_SistemaCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
                  A1063ContratoServicosSistemas_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll34128( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes34128( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption340( )
      {
      }

      protected void ZM34128( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
            Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
            Z1068ContratoServicosSistemas_ServicoSigla = A1068ContratoServicosSistemas_ServicoSigla;
            Z1064ContratoServicosSistemas_SistemaSigla = A1064ContratoServicosSistemas_SistemaSigla;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load34128( )
      {
         /* Using cursor T00348 */
         pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound128 = 1;
            A1068ContratoServicosSistemas_ServicoSigla = T00348_A1068ContratoServicosSistemas_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
            n1068ContratoServicosSistemas_ServicoSigla = T00348_n1068ContratoServicosSistemas_ServicoSigla[0];
            A1064ContratoServicosSistemas_SistemaSigla = T00348_A1064ContratoServicosSistemas_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
            n1064ContratoServicosSistemas_SistemaSigla = T00348_n1064ContratoServicosSistemas_SistemaSigla[0];
            ZM34128( -1) ;
         }
         pr_default.close(6);
         OnLoadActions34128( ) ;
      }

      protected void OnLoadActions34128( )
      {
      }

      protected void CheckExtendedTable34128( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00344 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00345 */
         pr_default.execute(3, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosSistemas_ServicoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1068ContratoServicosSistemas_ServicoSigla = T00345_A1068ContratoServicosSistemas_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
         n1068ContratoServicosSistemas_ServicoSigla = T00345_n1068ContratoServicosSistemas_ServicoSigla[0];
         pr_default.close(3);
         /* Using cursor T00346 */
         pr_default.execute(4, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosSistemas_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1064ContratoServicosSistemas_SistemaSigla = T00346_A1064ContratoServicosSistemas_SistemaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
         n1064ContratoServicosSistemas_SistemaSigla = T00346_n1064ContratoServicosSistemas_SistemaSigla[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors34128( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A160ContratoServicos_Codigo )
      {
         /* Using cursor T00349 */
         pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_3( int A1067ContratoServicosSistemas_ServicoCod )
      {
         /* Using cursor T003410 */
         pr_default.execute(8, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosSistemas_ServicoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1068ContratoServicosSistemas_ServicoSigla = T003410_A1068ContratoServicosSistemas_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
         n1068ContratoServicosSistemas_ServicoSigla = T003410_n1068ContratoServicosSistemas_ServicoSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1068ContratoServicosSistemas_ServicoSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_4( int A1063ContratoServicosSistemas_SistemaCod )
      {
         /* Using cursor T003411 */
         pr_default.execute(9, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosSistemas_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1064ContratoServicosSistemas_SistemaSigla = T003411_A1064ContratoServicosSistemas_SistemaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
         n1064ContratoServicosSistemas_SistemaSigla = T003411_n1064ContratoServicosSistemas_SistemaSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1064ContratoServicosSistemas_SistemaSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey34128( )
      {
         /* Using cursor T003412 */
         pr_default.execute(10, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound128 = 1;
         }
         else
         {
            RcdFound128 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00343 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM34128( 1) ;
            RcdFound128 = 1;
            A160ContratoServicos_Codigo = T00343_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1067ContratoServicosSistemas_ServicoCod = T00343_A1067ContratoServicosSistemas_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
            A1063ContratoServicosSistemas_SistemaCod = T00343_A1063ContratoServicosSistemas_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
            Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
            sMode128 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load34128( ) ;
            if ( AnyError == 1 )
            {
               RcdFound128 = 0;
               InitializeNonKey34128( ) ;
            }
            Gx_mode = sMode128;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound128 = 0;
            InitializeNonKey34128( ) ;
            sMode128 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode128;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey34128( ) ;
         if ( RcdFound128 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound128 = 0;
         /* Using cursor T003413 */
         pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T003413_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T003413_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003413_A1067ContratoServicosSistemas_ServicoCod[0] < A1067ContratoServicosSistemas_ServicoCod ) || ( T003413_A1067ContratoServicosSistemas_ServicoCod[0] == A1067ContratoServicosSistemas_ServicoCod ) && ( T003413_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003413_A1063ContratoServicosSistemas_SistemaCod[0] < A1063ContratoServicosSistemas_SistemaCod ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T003413_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T003413_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003413_A1067ContratoServicosSistemas_ServicoCod[0] > A1067ContratoServicosSistemas_ServicoCod ) || ( T003413_A1067ContratoServicosSistemas_ServicoCod[0] == A1067ContratoServicosSistemas_ServicoCod ) && ( T003413_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003413_A1063ContratoServicosSistemas_SistemaCod[0] > A1063ContratoServicosSistemas_SistemaCod ) ) )
            {
               A160ContratoServicos_Codigo = T003413_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A1067ContratoServicosSistemas_ServicoCod = T003413_A1067ContratoServicosSistemas_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
               A1063ContratoServicosSistemas_SistemaCod = T003413_A1063ContratoServicosSistemas_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
               RcdFound128 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound128 = 0;
         /* Using cursor T003414 */
         pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T003414_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T003414_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003414_A1067ContratoServicosSistemas_ServicoCod[0] > A1067ContratoServicosSistemas_ServicoCod ) || ( T003414_A1067ContratoServicosSistemas_ServicoCod[0] == A1067ContratoServicosSistemas_ServicoCod ) && ( T003414_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003414_A1063ContratoServicosSistemas_SistemaCod[0] > A1063ContratoServicosSistemas_SistemaCod ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T003414_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T003414_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003414_A1067ContratoServicosSistemas_ServicoCod[0] < A1067ContratoServicosSistemas_ServicoCod ) || ( T003414_A1067ContratoServicosSistemas_ServicoCod[0] == A1067ContratoServicosSistemas_ServicoCod ) && ( T003414_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003414_A1063ContratoServicosSistemas_SistemaCod[0] < A1063ContratoServicosSistemas_SistemaCod ) ) )
            {
               A160ContratoServicos_Codigo = T003414_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A1067ContratoServicosSistemas_ServicoCod = T003414_A1067ContratoServicosSistemas_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
               A1063ContratoServicosSistemas_SistemaCod = T003414_A1063ContratoServicosSistemas_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
               RcdFound128 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey34128( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert34128( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound128 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1067ContratoServicosSistemas_ServicoCod = Z1067ContratoServicosSistemas_ServicoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
                  A1063ContratoServicosSistemas_SistemaCod = Z1063ContratoServicosSistemas_SistemaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update34128( ) ;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert34128( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert34128( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
         {
            A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1067ContratoServicosSistemas_ServicoCod = Z1067ContratoServicosSistemas_ServicoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
            A1063ContratoServicosSistemas_SistemaCod = Z1063ContratoServicosSistemas_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart34128( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd34128( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart34128( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound128 != 0 )
            {
               ScanNext34128( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd34128( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency34128( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00342 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosSistemas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosSistemas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert34128( )
      {
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable34128( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM34128( 0) ;
            CheckOptimisticConcurrency34128( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm34128( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert34128( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003415 */
                     pr_default.execute(13, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosSistemas") ;
                     if ( (pr_default.getStatus(13) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption340( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load34128( ) ;
            }
            EndLevel34128( ) ;
         }
         CloseExtendedTableCursors34128( ) ;
      }

      protected void Update34128( )
      {
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable34128( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency34128( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm34128( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate34128( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContratoServicosSistemas] */
                     DeferredUpdate34128( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption340( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel34128( ) ;
         }
         CloseExtendedTableCursors34128( ) ;
      }

      protected void DeferredUpdate34128( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency34128( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls34128( ) ;
            AfterConfirm34128( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete34128( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003416 */
                  pr_default.execute(14, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosSistemas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound128 == 0 )
                        {
                           InitAll34128( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption340( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode128 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel34128( ) ;
         Gx_mode = sMode128;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls34128( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003417 */
            pr_default.execute(15, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
            A1068ContratoServicosSistemas_ServicoSigla = T003417_A1068ContratoServicosSistemas_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
            n1068ContratoServicosSistemas_ServicoSigla = T003417_n1068ContratoServicosSistemas_ServicoSigla[0];
            pr_default.close(15);
            /* Using cursor T003418 */
            pr_default.execute(16, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
            A1064ContratoServicosSistemas_SistemaSigla = T003418_A1064ContratoServicosSistemas_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
            n1064ContratoServicosSistemas_SistemaSigla = T003418_n1064ContratoServicosSistemas_SistemaSigla[0];
            pr_default.close(16);
         }
      }

      protected void EndLevel34128( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete34128( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            context.CommitDataStores( "ContratoServicosSistemas");
            if ( AnyError == 0 )
            {
               ConfirmValues340( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            context.RollbackDataStores( "ContratoServicosSistemas");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart34128( )
      {
         /* Using cursor T003419 */
         pr_default.execute(17);
         RcdFound128 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound128 = 1;
            A160ContratoServicos_Codigo = T003419_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1067ContratoServicosSistemas_ServicoCod = T003419_A1067ContratoServicosSistemas_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
            A1063ContratoServicosSistemas_SistemaCod = T003419_A1063ContratoServicosSistemas_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext34128( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound128 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound128 = 1;
            A160ContratoServicos_Codigo = T003419_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1067ContratoServicosSistemas_ServicoCod = T003419_A1067ContratoServicosSistemas_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
            A1063ContratoServicosSistemas_SistemaCod = T003419_A1063ContratoServicosSistemas_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
         }
      }

      protected void ScanEnd34128( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm34128( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert34128( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate34128( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete34128( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete34128( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate34128( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes34128( )
      {
         edtContratoServicos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Enabled), 5, 0)));
         dynContratoServicosSistemas_ServicoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosSistemas_ServicoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosSistemas_ServicoCod.Enabled), 5, 0)));
         edtContratoServicosSistemas_ServicoSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosSistemas_ServicoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosSistemas_ServicoSigla_Enabled), 5, 0)));
         edtContratoServicosSistemas_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosSistemas_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosSistemas_SistemaCod_Enabled), 5, 0)));
         edtContratoServicosSistemas_SistemaSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosSistemas_SistemaSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosSistemas_SistemaSigla_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues340( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299305980");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicossistemas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1067ContratoServicosSistemas_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1063ContratoServicosSistemas_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicossistemas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosSistemas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistemas do Servi�o" ;
      }

      protected void InitializeNonKey34128( )
      {
         A1068ContratoServicosSistemas_ServicoSigla = "";
         n1068ContratoServicosSistemas_ServicoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
         A1064ContratoServicosSistemas_SistemaSigla = "";
         n1064ContratoServicosSistemas_SistemaSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
      }

      protected void InitAll34128( )
      {
         A160ContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         A1067ContratoServicosSistemas_ServicoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1067ContratoServicosSistemas_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0)));
         A1063ContratoServicosSistemas_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1063ContratoServicosSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0)));
         InitializeNonKey34128( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299305984");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicossistemas.js", "?20205299305985");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontratoservicos_codigo_Internalname = "TEXTBLOCKCONTRATOSERVICOS_CODIGO";
         edtContratoServicos_Codigo_Internalname = "CONTRATOSERVICOS_CODIGO";
         lblTextblockcontratoservicossistemas_servicocod_Internalname = "TEXTBLOCKCONTRATOSERVICOSSISTEMAS_SERVICOCOD";
         dynContratoServicosSistemas_ServicoCod_Internalname = "CONTRATOSERVICOSSISTEMAS_SERVICOCOD";
         lblTextblockcontratoservicossistemas_servicosigla_Internalname = "TEXTBLOCKCONTRATOSERVICOSSISTEMAS_SERVICOSIGLA";
         edtContratoServicosSistemas_ServicoSigla_Internalname = "CONTRATOSERVICOSSISTEMAS_SERVICOSIGLA";
         lblTextblockcontratoservicossistemas_sistemacod_Internalname = "TEXTBLOCKCONTRATOSERVICOSSISTEMAS_SISTEMACOD";
         edtContratoServicosSistemas_SistemaCod_Internalname = "CONTRATOSERVICOSSISTEMAS_SISTEMACOD";
         lblTextblockcontratoservicossistemas_sistemasigla_Internalname = "TEXTBLOCKCONTRATOSERVICOSSISTEMAS_SISTEMASIGLA";
         edtContratoServicosSistemas_SistemaSigla_Internalname = "CONTRATOSERVICOSSISTEMAS_SISTEMASIGLA";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Sistemas do Servi�o";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContratoServicosSistemas_SistemaSigla_Jsonclick = "";
         edtContratoServicosSistemas_SistemaSigla_Enabled = 0;
         edtContratoServicosSistemas_SistemaCod_Jsonclick = "";
         edtContratoServicosSistemas_SistemaCod_Enabled = 1;
         edtContratoServicosSistemas_ServicoSigla_Jsonclick = "";
         edtContratoServicosSistemas_ServicoSigla_Enabled = 0;
         dynContratoServicosSistemas_ServicoCod_Jsonclick = "";
         dynContratoServicosSistemas_ServicoCod.Enabled = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATOSERVICOSSISTEMAS_SERVICOCOD341( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOSERVICOSSISTEMAS_SERVICOCOD_data341( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOSERVICOSSISTEMAS_SERVICOCOD_html341( )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOSERVICOSSISTEMAS_SERVICOCOD_data341( ) ;
         gxdynajaxindex = 1;
         dynContratoServicosSistemas_ServicoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoServicosSistemas_ServicoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOSERVICOSSISTEMAS_SERVICOCOD_data341( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T003420 */
         pr_default.execute(18);
         while ( (pr_default.getStatus(18) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003420_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003420_A608Servico_Nome[0]));
            pr_default.readNext(18);
         }
         pr_default.close(18);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003421 */
         pr_default.execute(19, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(19);
         /* Using cursor T003417 */
         pr_default.execute(15, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosSistemas_ServicoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1068ContratoServicosSistemas_ServicoSigla = T003417_A1068ContratoServicosSistemas_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1068ContratoServicosSistemas_ServicoSigla", A1068ContratoServicosSistemas_ServicoSigla);
         n1068ContratoServicosSistemas_ServicoSigla = T003417_n1068ContratoServicosSistemas_ServicoSigla[0];
         pr_default.close(15);
         /* Using cursor T003418 */
         pr_default.execute(16, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosSistemas_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1064ContratoServicosSistemas_SistemaSigla = T003418_A1064ContratoServicosSistemas_SistemaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1064ContratoServicosSistemas_SistemaSigla", A1064ContratoServicosSistemas_SistemaSigla);
         n1064ContratoServicosSistemas_SistemaSigla = T003418_n1064ContratoServicosSistemas_SistemaSigla[0];
         pr_default.close(16);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contratoservicos_codigo( int GX_Parm1 )
      {
         A160ContratoServicos_Codigo = GX_Parm1;
         /* Using cursor T003421 */
         pr_default.execute(19, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
         }
         pr_default.close(19);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicossistemas_servicocod( GXCombobox dynGX_Parm1 ,
                                                             String GX_Parm2 )
      {
         dynContratoServicosSistemas_ServicoCod = dynGX_Parm1;
         A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( dynContratoServicosSistemas_ServicoCod.CurrentValue, "."));
         A1068ContratoServicosSistemas_ServicoSigla = GX_Parm2;
         n1068ContratoServicosSistemas_ServicoSigla = false;
         /* Using cursor T003417 */
         pr_default.execute(15, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosSistemas_ServicoCod_Internalname;
         }
         A1068ContratoServicosSistemas_ServicoSigla = T003417_A1068ContratoServicosSistemas_ServicoSigla[0];
         n1068ContratoServicosSistemas_ServicoSigla = T003417_n1068ContratoServicosSistemas_ServicoSigla[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1068ContratoServicosSistemas_ServicoSigla = "";
            n1068ContratoServicosSistemas_ServicoSigla = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A1068ContratoServicosSistemas_ServicoSigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicossistemas_sistemacod( int GX_Parm1 ,
                                                             GXCombobox dynGX_Parm2 ,
                                                             int GX_Parm3 ,
                                                             String GX_Parm4 )
      {
         A160ContratoServicos_Codigo = GX_Parm1;
         dynContratoServicosSistemas_ServicoCod = dynGX_Parm2;
         A1067ContratoServicosSistemas_ServicoCod = (int)(NumberUtil.Val( dynContratoServicosSistemas_ServicoCod.CurrentValue, "."));
         A1063ContratoServicosSistemas_SistemaCod = GX_Parm3;
         A1064ContratoServicosSistemas_SistemaSigla = GX_Parm4;
         n1064ContratoServicosSistemas_SistemaSigla = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T003418 */
         pr_default.execute(16, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosSistemas_SistemaCod_Internalname;
         }
         A1064ContratoServicosSistemas_SistemaSigla = T003418_A1064ContratoServicosSistemas_SistemaSigla[0];
         n1064ContratoServicosSistemas_SistemaSigla = T003418_n1064ContratoServicosSistemas_SistemaSigla[0];
         pr_default.close(16);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1068ContratoServicosSistemas_ServicoSigla = "";
            n1068ContratoServicosSistemas_ServicoSigla = false;
            A1064ContratoServicosSistemas_SistemaSigla = "";
            n1064ContratoServicosSistemas_SistemaSigla = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A1068ContratoServicosSistemas_ServicoSigla));
         isValidOutput.Add(StringUtil.RTrim( A1064ContratoServicosSistemas_SistemaSigla));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1067ContratoServicosSistemas_ServicoCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1063ContratoServicosSistemas_SistemaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1068ContratoServicosSistemas_ServicoSigla));
         isValidOutput.Add(StringUtil.RTrim( Z1064ContratoServicosSistemas_SistemaSigla));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(15);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T00347_A155Servico_Codigo = new int[1] ;
         T00347_A608Servico_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontratoservicos_codigo_Jsonclick = "";
         lblTextblockcontratoservicossistemas_servicocod_Jsonclick = "";
         lblTextblockcontratoservicossistemas_servicosigla_Jsonclick = "";
         A1068ContratoServicosSistemas_ServicoSigla = "";
         lblTextblockcontratoservicossistemas_sistemacod_Jsonclick = "";
         lblTextblockcontratoservicossistemas_sistemasigla_Jsonclick = "";
         A1064ContratoServicosSistemas_SistemaSigla = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1068ContratoServicosSistemas_ServicoSigla = "";
         Z1064ContratoServicosSistemas_SistemaSigla = "";
         T00348_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         T00348_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         T00348_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         T00348_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         T00348_A160ContratoServicos_Codigo = new int[1] ;
         T00348_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T00348_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T00344_A160ContratoServicos_Codigo = new int[1] ;
         T00345_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         T00345_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         T00346_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         T00346_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         T00349_A160ContratoServicos_Codigo = new int[1] ;
         T003410_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         T003410_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         T003411_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         T003411_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         T003412_A160ContratoServicos_Codigo = new int[1] ;
         T003412_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T003412_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T00343_A160ContratoServicos_Codigo = new int[1] ;
         T00343_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T00343_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         sMode128 = "";
         T003413_A160ContratoServicos_Codigo = new int[1] ;
         T003413_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T003413_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T003414_A160ContratoServicos_Codigo = new int[1] ;
         T003414_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T003414_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T00342_A160ContratoServicos_Codigo = new int[1] ;
         T00342_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T00342_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T003417_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         T003417_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         T003418_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         T003418_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         T003419_A160ContratoServicos_Codigo = new int[1] ;
         T003419_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T003419_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003420_A155Servico_Codigo = new int[1] ;
         T003420_A608Servico_Nome = new String[] {""} ;
         T003421_A160ContratoServicos_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicossistemas__default(),
            new Object[][] {
                new Object[] {
               T00342_A160ContratoServicos_Codigo, T00342_A1067ContratoServicosSistemas_ServicoCod, T00342_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T00343_A160ContratoServicos_Codigo, T00343_A1067ContratoServicosSistemas_ServicoCod, T00343_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T00344_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T00345_A1068ContratoServicosSistemas_ServicoSigla, T00345_n1068ContratoServicosSistemas_ServicoSigla
               }
               , new Object[] {
               T00346_A1064ContratoServicosSistemas_SistemaSigla, T00346_n1064ContratoServicosSistemas_SistemaSigla
               }
               , new Object[] {
               T00347_A155Servico_Codigo, T00347_A608Servico_Nome
               }
               , new Object[] {
               T00348_A1068ContratoServicosSistemas_ServicoSigla, T00348_n1068ContratoServicosSistemas_ServicoSigla, T00348_A1064ContratoServicosSistemas_SistemaSigla, T00348_n1064ContratoServicosSistemas_SistemaSigla, T00348_A160ContratoServicos_Codigo, T00348_A1067ContratoServicosSistemas_ServicoCod, T00348_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T00349_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003410_A1068ContratoServicosSistemas_ServicoSigla, T003410_n1068ContratoServicosSistemas_ServicoSigla
               }
               , new Object[] {
               T003411_A1064ContratoServicosSistemas_SistemaSigla, T003411_n1064ContratoServicosSistemas_SistemaSigla
               }
               , new Object[] {
               T003412_A160ContratoServicos_Codigo, T003412_A1067ContratoServicosSistemas_ServicoCod, T003412_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T003413_A160ContratoServicos_Codigo, T003413_A1067ContratoServicosSistemas_ServicoCod, T003413_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T003414_A160ContratoServicos_Codigo, T003414_A1067ContratoServicosSistemas_ServicoCod, T003414_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003417_A1068ContratoServicosSistemas_ServicoSigla, T003417_n1068ContratoServicosSistemas_ServicoSigla
               }
               , new Object[] {
               T003418_A1064ContratoServicosSistemas_SistemaSigla, T003418_n1064ContratoServicosSistemas_SistemaSigla
               }
               , new Object[] {
               T003419_A160ContratoServicos_Codigo, T003419_A1067ContratoServicosSistemas_ServicoCod, T003419_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T003420_A155Servico_Codigo, T003420_A608Servico_Nome
               }
               , new Object[] {
               T003421_A160ContratoServicos_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound128 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z160ContratoServicos_Codigo ;
      private int Z1067ContratoServicosSistemas_ServicoCod ;
      private int Z1063ContratoServicosSistemas_SistemaCod ;
      private int A160ContratoServicos_Codigo ;
      private int A1067ContratoServicosSistemas_ServicoCod ;
      private int A1063ContratoServicosSistemas_SistemaCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContratoServicos_Codigo_Enabled ;
      private int edtContratoServicosSistemas_ServicoSigla_Enabled ;
      private int edtContratoServicosSistemas_SistemaCod_Enabled ;
      private int edtContratoServicosSistemas_SistemaSigla_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontratoservicos_codigo_Internalname ;
      private String lblTextblockcontratoservicos_codigo_Jsonclick ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String lblTextblockcontratoservicossistemas_servicocod_Internalname ;
      private String lblTextblockcontratoservicossistemas_servicocod_Jsonclick ;
      private String dynContratoServicosSistemas_ServicoCod_Internalname ;
      private String dynContratoServicosSistemas_ServicoCod_Jsonclick ;
      private String lblTextblockcontratoservicossistemas_servicosigla_Internalname ;
      private String lblTextblockcontratoservicossistemas_servicosigla_Jsonclick ;
      private String edtContratoServicosSistemas_ServicoSigla_Internalname ;
      private String A1068ContratoServicosSistemas_ServicoSigla ;
      private String edtContratoServicosSistemas_ServicoSigla_Jsonclick ;
      private String lblTextblockcontratoservicossistemas_sistemacod_Internalname ;
      private String lblTextblockcontratoservicossistemas_sistemacod_Jsonclick ;
      private String edtContratoServicosSistemas_SistemaCod_Internalname ;
      private String edtContratoServicosSistemas_SistemaCod_Jsonclick ;
      private String lblTextblockcontratoservicossistemas_sistemasigla_Internalname ;
      private String lblTextblockcontratoservicossistemas_sistemasigla_Jsonclick ;
      private String edtContratoServicosSistemas_SistemaSigla_Internalname ;
      private String A1064ContratoServicosSistemas_SistemaSigla ;
      private String edtContratoServicosSistemas_SistemaSigla_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1068ContratoServicosSistemas_ServicoSigla ;
      private String Z1064ContratoServicosSistemas_SistemaSigla ;
      private String sMode128 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1068ContratoServicosSistemas_ServicoSigla ;
      private bool n1064ContratoServicosSistemas_SistemaSigla ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T00347_A155Servico_Codigo ;
      private String[] T00347_A608Servico_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContratoServicosSistemas_ServicoCod ;
      private String[] T00348_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] T00348_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] T00348_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] T00348_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] T00348_A160ContratoServicos_Codigo ;
      private int[] T00348_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T00348_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T00344_A160ContratoServicos_Codigo ;
      private String[] T00345_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] T00345_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] T00346_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] T00346_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] T00349_A160ContratoServicos_Codigo ;
      private String[] T003410_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] T003410_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] T003411_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] T003411_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] T003412_A160ContratoServicos_Codigo ;
      private int[] T003412_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T003412_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T00343_A160ContratoServicos_Codigo ;
      private int[] T00343_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T00343_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T003413_A160ContratoServicos_Codigo ;
      private int[] T003413_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T003413_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T003414_A160ContratoServicos_Codigo ;
      private int[] T003414_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T003414_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T00342_A160ContratoServicos_Codigo ;
      private int[] T00342_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T00342_A1063ContratoServicosSistemas_SistemaCod ;
      private String[] T003417_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] T003417_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] T003418_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] T003418_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] T003419_A160ContratoServicos_Codigo ;
      private int[] T003419_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T003419_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T003420_A155Servico_Codigo ;
      private String[] T003420_A608Servico_Nome ;
      private int[] T003421_A160ContratoServicos_Codigo ;
      private GXWebForm Form ;
   }

   public class contratoservicossistemas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00347 ;
          prmT00347 = new Object[] {
          } ;
          Object[] prmT00348 ;
          prmT00348 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00344 ;
          prmT00344 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00345 ;
          prmT00345 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00346 ;
          prmT00346 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00349 ;
          prmT00349 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003410 ;
          prmT003410 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003411 ;
          prmT003411 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003412 ;
          prmT003412 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00343 ;
          prmT00343 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003413 ;
          prmT003413 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003414 ;
          prmT003414 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00342 ;
          prmT00342 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003415 ;
          prmT003415 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003416 ;
          prmT003416 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003419 ;
          prmT003419 = new Object[] {
          } ;
          Object[] prmT003420 ;
          prmT003420 = new Object[] {
          } ;
          Object[] prmT003421 ;
          prmT003421 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003417 ;
          prmT003417 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003418 ;
          prmT003418 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00342", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00342,1,0,true,false )
             ,new CursorDef("T00343", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00343,1,0,true,false )
             ,new CursorDef("T00344", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00344,1,0,true,false )
             ,new CursorDef("T00345", "SELECT [Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosSistemas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00345,1,0,true,false )
             ,new CursorDef("T00346", "SELECT [Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00346,1,0,true,false )
             ,new CursorDef("T00347", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT00347,0,0,true,false )
             ,new CursorDef("T00348", "SELECT T2.[Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla, T3.[Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla, TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, TM1.[ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM (([ContratoServicosSistemas] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[ContratoServicosSistemas_ServicoCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = TM1.[ContratoServicosSistemas_SistemaCod]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod and TM1.[ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosSistemas_ServicoCod], TM1.[ContratoServicosSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00348,100,0,true,false )
             ,new CursorDef("T00349", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00349,1,0,true,false )
             ,new CursorDef("T003410", "SELECT [Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosSistemas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003410,1,0,true,false )
             ,new CursorDef("T003411", "SELECT [Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003411,1,0,true,false )
             ,new CursorDef("T003412", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003412,1,0,true,false )
             ,new CursorDef("T003413", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] > @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosSistemas_ServicoCod] > @ContratoServicosSistemas_ServicoCod or [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod and [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosSistemas_SistemaCod] > @ContratoServicosSistemas_SistemaCod) ORDER BY [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003413,1,0,true,true )
             ,new CursorDef("T003414", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] < @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosSistemas_ServicoCod] < @ContratoServicosSistemas_ServicoCod or [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod and [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosSistemas_SistemaCod] < @ContratoServicosSistemas_SistemaCod) ORDER BY [ContratoServicos_Codigo] DESC, [ContratoServicosSistemas_ServicoCod] DESC, [ContratoServicosSistemas_SistemaCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003414,1,0,true,true )
             ,new CursorDef("T003415", "INSERT INTO [ContratoServicosSistemas]([ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod]) VALUES(@ContratoServicos_Codigo, @ContratoServicosSistemas_ServicoCod, @ContratoServicosSistemas_SistemaCod)", GxErrorMask.GX_NOMASK,prmT003415)
             ,new CursorDef("T003416", "DELETE FROM [ContratoServicosSistemas]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod", GxErrorMask.GX_NOMASK,prmT003416)
             ,new CursorDef("T003417", "SELECT [Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosSistemas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003417,1,0,true,false )
             ,new CursorDef("T003418", "SELECT [Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003418,1,0,true,false )
             ,new CursorDef("T003419", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) ORDER BY [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003419,100,0,true,false )
             ,new CursorDef("T003420", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003420,0,0,true,false )
             ,new CursorDef("T003421", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003421,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
