/*
               File: type_SdtSDT_ConsultaSistemas_Sistema
        Description: SDT_ConsultaSistemas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_ConsultaSistemas.Sistema" )]
   [XmlType(TypeName =  "SDT_ConsultaSistemas.Sistema" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_ConsultaSistemas_Sistema : GxUserType
   {
      public SdtSDT_ConsultaSistemas_Sistema( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla = "";
      }

      public SdtSDT_ConsultaSistemas_Sistema( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_ConsultaSistemas_Sistema deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_ConsultaSistemas_Sistema)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_ConsultaSistemas_Sistema obj ;
         obj = this;
         obj.gxTpr_Codigo = deserialized.gxTpr_Codigo;
         obj.gxTpr_Sigla = deserialized.gxTpr_Sigla;
         obj.gxTpr_Mes = deserialized.gxTpr_Mes;
         obj.gxTpr_Ano = deserialized.gxTpr_Ano;
         obj.gxTpr_Qtddmn = deserialized.gxTpr_Qtddmn;
         obj.gxTpr_Pffinal = deserialized.gxTpr_Pffinal;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Codigo") )
               {
                  gxTv_SdtSDT_ConsultaSistemas_Sistema_Codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sigla") )
               {
                  gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mes") )
               {
                  gxTv_SdtSDT_ConsultaSistemas_Sistema_Mes = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Ano") )
               {
                  gxTv_SdtSDT_ConsultaSistemas_Sistema_Ano = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "QtdDmn") )
               {
                  gxTv_SdtSDT_ConsultaSistemas_Sistema_Qtddmn = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFFinal") )
               {
                  gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_ConsultaSistemas.Sistema";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ConsultaSistemas_Sistema_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sigla", StringUtil.RTrim( gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Mes", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ConsultaSistemas_Sistema_Mes), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Ano", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ConsultaSistemas_Sistema_Ano), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("QtdDmn", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ConsultaSistemas_Sistema_Qtddmn), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("PFFinal", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Codigo", gxTv_SdtSDT_ConsultaSistemas_Sistema_Codigo, false);
         AddObjectProperty("Sigla", gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla, false);
         AddObjectProperty("Mes", gxTv_SdtSDT_ConsultaSistemas_Sistema_Mes, false);
         AddObjectProperty("Ano", gxTv_SdtSDT_ConsultaSistemas_Sistema_Ano, false);
         AddObjectProperty("QtdDmn", gxTv_SdtSDT_ConsultaSistemas_Sistema_Qtddmn, false);
         AddObjectProperty("PFFinal", gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal, false);
         return  ;
      }

      [  SoapElement( ElementName = "Codigo" )]
      [  XmlElement( ElementName = "Codigo"   )]
      public int gxTpr_Codigo
      {
         get {
            return gxTv_SdtSDT_ConsultaSistemas_Sistema_Codigo ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sigla" )]
      [  XmlElement( ElementName = "Sigla"   )]
      public String gxTpr_Sigla
      {
         get {
            return gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mes" )]
      [  XmlElement( ElementName = "Mes"   )]
      public long gxTpr_Mes
      {
         get {
            return gxTv_SdtSDT_ConsultaSistemas_Sistema_Mes ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Mes = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Ano" )]
      [  XmlElement( ElementName = "Ano"   )]
      public short gxTpr_Ano
      {
         get {
            return gxTv_SdtSDT_ConsultaSistemas_Sistema_Ano ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Ano = (short)(value);
         }

      }

      [  SoapElement( ElementName = "QtdDmn" )]
      [  XmlElement( ElementName = "QtdDmn"   )]
      public int gxTpr_Qtddmn
      {
         get {
            return gxTv_SdtSDT_ConsultaSistemas_Sistema_Qtddmn ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Qtddmn = (int)(value);
         }

      }

      [  SoapElement( ElementName = "PFFinal" )]
      [  XmlElement( ElementName = "PFFinal"   )]
      public double gxTpr_Pffinal_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal) ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pffinal
      {
         get {
            return gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal ;
         }

         set {
            gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal = (decimal)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_ConsultaSistemas_Sistema_Ano ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_ConsultaSistemas_Sistema_Codigo ;
      protected int gxTv_SdtSDT_ConsultaSistemas_Sistema_Qtddmn ;
      protected long gxTv_SdtSDT_ConsultaSistemas_Sistema_Mes ;
      protected decimal gxTv_SdtSDT_ConsultaSistemas_Sistema_Pffinal ;
      protected String gxTv_SdtSDT_ConsultaSistemas_Sistema_Sigla ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_ConsultaSistemas.Sistema", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_ConsultaSistemas_Sistema_RESTInterface : GxGenericCollectionItem<SdtSDT_ConsultaSistemas_Sistema>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_ConsultaSistemas_Sistema_RESTInterface( ) : base()
      {
      }

      public SdtSDT_ConsultaSistemas_Sistema_RESTInterface( SdtSDT_ConsultaSistemas_Sistema psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Codigo
      {
         get {
            return sdt.gxTpr_Codigo ;
         }

         set {
            sdt.gxTpr_Codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sigla" , Order = 1 )]
      public String gxTpr_Sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sigla) ;
         }

         set {
            sdt.gxTpr_Sigla = (String)(value);
         }

      }

      [DataMember( Name = "Mes" , Order = 2 )]
      public String gxTpr_Mes
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Mes), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Mes = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Ano" , Order = 3 )]
      public Nullable<short> gxTpr_Ano
      {
         get {
            return sdt.gxTpr_Ano ;
         }

         set {
            sdt.gxTpr_Ano = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "QtdDmn" , Order = 4 )]
      public Nullable<int> gxTpr_Qtddmn
      {
         get {
            return sdt.gxTpr_Qtddmn ;
         }

         set {
            sdt.gxTpr_Qtddmn = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "PFFinal" , Order = 5 )]
      public String gxTpr_Pffinal
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pffinal, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pffinal = NumberUtil.Val( (String)(value), ".");
         }

      }

      public SdtSDT_ConsultaSistemas_Sistema sdt
      {
         get {
            return (SdtSDT_ConsultaSistemas_Sistema)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_ConsultaSistemas_Sistema() ;
         }
      }

   }

}
