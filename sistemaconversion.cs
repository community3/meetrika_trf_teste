/*
               File: SistemaConversion
        Description: Conversion for table Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 7:45:15.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
namespace GeneXus.Programs {
   public class sistemaconversion : GXProcedure
   {
      public sistemaconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public sistemaconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         sistemaconversion objsistemaconversion;
         objsistemaconversion = new sistemaconversion();
         objsistemaconversion.context.SetSubmitInitialConfig(context);
         objsistemaconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objsistemaconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((sistemaconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         cmdBuffer=" SET IDENTITY_INSERT [GXA0025] ON "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         /* Using cursor SISTEMACON2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2130Sistema_Ultimo = SISTEMACON2_A2130Sistema_Ultimo[0];
            A2109Sistema_Repositorio = SISTEMACON2_A2109Sistema_Repositorio[0];
            n2109Sistema_Repositorio = SISTEMACON2_n2109Sistema_Repositorio[0];
            A1859SistemaVersao_Codigo = SISTEMACON2_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = SISTEMACON2_n1859SistemaVersao_Codigo[0];
            A1831Sistema_Responsavel = SISTEMACON2_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = SISTEMACON2_n1831Sistema_Responsavel[0];
            A1399Sistema_ImpUserCod = SISTEMACON2_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = SISTEMACON2_n1399Sistema_ImpUserCod[0];
            A1401Sistema_ImpData = SISTEMACON2_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = SISTEMACON2_n1401Sistema_ImpData[0];
            A700Sistema_Tecnica = SISTEMACON2_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = SISTEMACON2_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = SISTEMACON2_A699Sistema_Tipo[0];
            n699Sistema_Tipo = SISTEMACON2_n699Sistema_Tipo[0];
            A689Sistema_Esforco = SISTEMACON2_A689Sistema_Esforco[0];
            n689Sistema_Esforco = SISTEMACON2_n689Sistema_Esforco[0];
            A688Sistema_Prazo = SISTEMACON2_A688Sistema_Prazo[0];
            n688Sistema_Prazo = SISTEMACON2_n688Sistema_Prazo[0];
            A687Sistema_Custo = SISTEMACON2_A687Sistema_Custo[0];
            n687Sistema_Custo = SISTEMACON2_n687Sistema_Custo[0];
            A686Sistema_FatorAjuste = SISTEMACON2_A686Sistema_FatorAjuste[0];
            n686Sistema_FatorAjuste = SISTEMACON2_n686Sistema_FatorAjuste[0];
            A513Sistema_Coordenacao = SISTEMACON2_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = SISTEMACON2_n513Sistema_Coordenacao[0];
            A351AmbienteTecnologico_Codigo = SISTEMACON2_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = SISTEMACON2_n351AmbienteTecnologico_Codigo[0];
            A416Sistema_Nome = SISTEMACON2_A416Sistema_Nome[0];
            A137Metodologia_Codigo = SISTEMACON2_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = SISTEMACON2_n137Metodologia_Codigo[0];
            A135Sistema_AreaTrabalhoCod = SISTEMACON2_A135Sistema_AreaTrabalhoCod[0];
            A130Sistema_Ativo = SISTEMACON2_A130Sistema_Ativo[0];
            A129Sistema_Sigla = SISTEMACON2_A129Sistema_Sigla[0];
            A128Sistema_Descricao = SISTEMACON2_A128Sistema_Descricao[0];
            n128Sistema_Descricao = SISTEMACON2_n128Sistema_Descricao[0];
            A127Sistema_Codigo = SISTEMACON2_A127Sistema_Codigo[0];
            /*
               INSERT RECORD ON TABLE GXA0025

            */
            AV2Sistema_Codigo = A127Sistema_Codigo;
            if ( SISTEMACON2_n128Sistema_Descricao[0] )
            {
               AV3Sistema_Descricao = "";
               nV3Sistema_Descricao = false;
               nV3Sistema_Descricao = true;
            }
            else
            {
               AV3Sistema_Descricao = A128Sistema_Descricao;
               nV3Sistema_Descricao = false;
            }
            AV4Sistema_Sigla = A129Sistema_Sigla;
            AV5Sistema_Ativo = A130Sistema_Ativo;
            AV6Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
            if ( SISTEMACON2_n137Metodologia_Codigo[0] )
            {
               AV7Metodologia_Codigo = 0;
               nV7Metodologia_Codigo = false;
               nV7Metodologia_Codigo = true;
            }
            else
            {
               AV7Metodologia_Codigo = A137Metodologia_Codigo;
               nV7Metodologia_Codigo = false;
            }
            AV8Sistema_Nome = A416Sistema_Nome;
            if ( SISTEMACON2_n351AmbienteTecnologico_Codigo[0] )
            {
               AV9AmbienteTecnologico_Codigo = 0;
               nV9AmbienteTecnologico_Codigo = false;
               nV9AmbienteTecnologico_Codigo = true;
            }
            else
            {
               AV9AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
               nV9AmbienteTecnologico_Codigo = false;
            }
            if ( SISTEMACON2_n513Sistema_Coordenacao[0] )
            {
               AV10Sistema_Coordenacao = "";
               nV10Sistema_Coordenacao = false;
               nV10Sistema_Coordenacao = true;
            }
            else
            {
               AV10Sistema_Coordenacao = A513Sistema_Coordenacao;
               nV10Sistema_Coordenacao = false;
            }
            if ( SISTEMACON2_n686Sistema_FatorAjuste[0] )
            {
               AV11Sistema_FatorAjuste = 0;
               nV11Sistema_FatorAjuste = false;
               nV11Sistema_FatorAjuste = true;
            }
            else
            {
               AV11Sistema_FatorAjuste = A686Sistema_FatorAjuste;
               nV11Sistema_FatorAjuste = false;
            }
            if ( SISTEMACON2_n687Sistema_Custo[0] )
            {
               AV12Sistema_Custo = 0;
               nV12Sistema_Custo = false;
               nV12Sistema_Custo = true;
            }
            else
            {
               AV12Sistema_Custo = A687Sistema_Custo;
               nV12Sistema_Custo = false;
            }
            if ( SISTEMACON2_n688Sistema_Prazo[0] )
            {
               AV13Sistema_Prazo = 0;
               nV13Sistema_Prazo = false;
               nV13Sistema_Prazo = true;
            }
            else
            {
               AV13Sistema_Prazo = A688Sistema_Prazo;
               nV13Sistema_Prazo = false;
            }
            if ( SISTEMACON2_n689Sistema_Esforco[0] )
            {
               AV14Sistema_Esforco = 0;
               nV14Sistema_Esforco = false;
               nV14Sistema_Esforco = true;
            }
            else
            {
               AV14Sistema_Esforco = A689Sistema_Esforco;
               nV14Sistema_Esforco = false;
            }
            if ( SISTEMACON2_n699Sistema_Tipo[0] )
            {
               AV15Sistema_Tipo = "";
               nV15Sistema_Tipo = false;
               nV15Sistema_Tipo = true;
            }
            else
            {
               AV15Sistema_Tipo = A699Sistema_Tipo;
               nV15Sistema_Tipo = false;
            }
            if ( SISTEMACON2_n700Sistema_Tecnica[0] )
            {
               AV16Sistema_Tecnica = "";
               nV16Sistema_Tecnica = false;
               nV16Sistema_Tecnica = true;
            }
            else
            {
               AV16Sistema_Tecnica = A700Sistema_Tecnica;
               nV16Sistema_Tecnica = false;
            }
            if ( SISTEMACON2_n1401Sistema_ImpData[0] )
            {
               AV17Sistema_ImpData = (DateTime)(DateTime.MinValue);
               nV17Sistema_ImpData = false;
               nV17Sistema_ImpData = true;
            }
            else
            {
               AV17Sistema_ImpData = A1401Sistema_ImpData;
               nV17Sistema_ImpData = false;
            }
            if ( SISTEMACON2_n1399Sistema_ImpUserCod[0] )
            {
               AV18Sistema_ImpUserCod = 0;
               nV18Sistema_ImpUserCod = false;
               nV18Sistema_ImpUserCod = true;
            }
            else
            {
               AV18Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
               nV18Sistema_ImpUserCod = false;
            }
            if ( SISTEMACON2_n1831Sistema_Responsavel[0] )
            {
               AV19Sistema_Responsavel = 0;
               nV19Sistema_Responsavel = false;
               nV19Sistema_Responsavel = true;
            }
            else
            {
               AV19Sistema_Responsavel = A1831Sistema_Responsavel;
               nV19Sistema_Responsavel = false;
            }
            if ( SISTEMACON2_n1859SistemaVersao_Codigo[0] )
            {
               AV20SistemaVersao_Codigo = 0;
               nV20SistemaVersao_Codigo = false;
               nV20SistemaVersao_Codigo = true;
            }
            else
            {
               AV20SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
               nV20SistemaVersao_Codigo = false;
            }
            if ( SISTEMACON2_n2109Sistema_Repositorio[0] )
            {
               AV21Sistema_Repositorio = "";
               nV21Sistema_Repositorio = false;
               nV21Sistema_Repositorio = true;
            }
            else
            {
               AV21Sistema_Repositorio = A2109Sistema_Repositorio;
               nV21Sistema_Repositorio = false;
            }
            AV22Sistema_Ultimo = A2130Sistema_Ultimo;
            if ( (0==A1826GpoObjCtrl_Codigo) )
            {
               AV23Sistema_GpoObjCtrlCod = 0;
               nV23Sistema_GpoObjCtrlCod = false;
               nV23Sistema_GpoObjCtrlCod = true;
            }
            else
            {
               AV23Sistema_GpoObjCtrlCod = A1826GpoObjCtrl_Codigo;
               nV23Sistema_GpoObjCtrlCod = false;
            }
            /* Using cursor SISTEMACON3 */
            pr_default.execute(1, new Object[] {AV2Sistema_Codigo, nV3Sistema_Descricao, AV3Sistema_Descricao, AV4Sistema_Sigla, AV5Sistema_Ativo, AV6Sistema_AreaTrabalhoCod, nV7Metodologia_Codigo, AV7Metodologia_Codigo, AV8Sistema_Nome, nV9AmbienteTecnologico_Codigo, AV9AmbienteTecnologico_Codigo, nV10Sistema_Coordenacao, AV10Sistema_Coordenacao, nV11Sistema_FatorAjuste, AV11Sistema_FatorAjuste, nV12Sistema_Custo, AV12Sistema_Custo, nV13Sistema_Prazo, AV13Sistema_Prazo, nV14Sistema_Esforco, AV14Sistema_Esforco, nV15Sistema_Tipo, AV15Sistema_Tipo, nV16Sistema_Tecnica, AV16Sistema_Tecnica, nV17Sistema_ImpData, AV17Sistema_ImpData, nV18Sistema_ImpUserCod, AV18Sistema_ImpUserCod, nV19Sistema_Responsavel, AV19Sistema_Responsavel, nV20SistemaVersao_Codigo, AV20SistemaVersao_Codigo, nV21Sistema_Repositorio, AV21Sistema_Repositorio, AV22Sistema_Ultimo, nV23Sistema_GpoObjCtrlCod, AV23Sistema_GpoObjCtrlCod});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0025") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         cmdBuffer=" SET IDENTITY_INSERT [GXA0025] OFF "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         cmdBuffer = "";
         scmdbuf = "";
         SISTEMACON2_A2130Sistema_Ultimo = new short[1] ;
         SISTEMACON2_A2109Sistema_Repositorio = new String[] {""} ;
         SISTEMACON2_n2109Sistema_Repositorio = new bool[] {false} ;
         SISTEMACON2_A1859SistemaVersao_Codigo = new int[1] ;
         SISTEMACON2_n1859SistemaVersao_Codigo = new bool[] {false} ;
         SISTEMACON2_A1831Sistema_Responsavel = new int[1] ;
         SISTEMACON2_n1831Sistema_Responsavel = new bool[] {false} ;
         SISTEMACON2_A1399Sistema_ImpUserCod = new int[1] ;
         SISTEMACON2_n1399Sistema_ImpUserCod = new bool[] {false} ;
         SISTEMACON2_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         SISTEMACON2_n1401Sistema_ImpData = new bool[] {false} ;
         SISTEMACON2_A700Sistema_Tecnica = new String[] {""} ;
         SISTEMACON2_n700Sistema_Tecnica = new bool[] {false} ;
         SISTEMACON2_A699Sistema_Tipo = new String[] {""} ;
         SISTEMACON2_n699Sistema_Tipo = new bool[] {false} ;
         SISTEMACON2_A689Sistema_Esforco = new short[1] ;
         SISTEMACON2_n689Sistema_Esforco = new bool[] {false} ;
         SISTEMACON2_A688Sistema_Prazo = new short[1] ;
         SISTEMACON2_n688Sistema_Prazo = new bool[] {false} ;
         SISTEMACON2_A687Sistema_Custo = new decimal[1] ;
         SISTEMACON2_n687Sistema_Custo = new bool[] {false} ;
         SISTEMACON2_A686Sistema_FatorAjuste = new decimal[1] ;
         SISTEMACON2_n686Sistema_FatorAjuste = new bool[] {false} ;
         SISTEMACON2_A513Sistema_Coordenacao = new String[] {""} ;
         SISTEMACON2_n513Sistema_Coordenacao = new bool[] {false} ;
         SISTEMACON2_A351AmbienteTecnologico_Codigo = new int[1] ;
         SISTEMACON2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         SISTEMACON2_A416Sistema_Nome = new String[] {""} ;
         SISTEMACON2_A137Metodologia_Codigo = new int[1] ;
         SISTEMACON2_n137Metodologia_Codigo = new bool[] {false} ;
         SISTEMACON2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         SISTEMACON2_A130Sistema_Ativo = new bool[] {false} ;
         SISTEMACON2_A129Sistema_Sigla = new String[] {""} ;
         SISTEMACON2_A128Sistema_Descricao = new String[] {""} ;
         SISTEMACON2_n128Sistema_Descricao = new bool[] {false} ;
         SISTEMACON2_A127Sistema_Codigo = new int[1] ;
         A2109Sistema_Repositorio = "";
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         A700Sistema_Tecnica = "";
         A699Sistema_Tipo = "";
         A513Sistema_Coordenacao = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A128Sistema_Descricao = "";
         AV3Sistema_Descricao = "";
         AV4Sistema_Sigla = "";
         AV8Sistema_Nome = "";
         AV10Sistema_Coordenacao = "";
         AV15Sistema_Tipo = "";
         AV16Sistema_Tecnica = "";
         AV17Sistema_ImpData = (DateTime)(DateTime.MinValue);
         AV21Sistema_Repositorio = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemaconversion__default(),
            new Object[][] {
                new Object[] {
               SISTEMACON2_A2130Sistema_Ultimo, SISTEMACON2_A2109Sistema_Repositorio, SISTEMACON2_n2109Sistema_Repositorio, SISTEMACON2_A1859SistemaVersao_Codigo, SISTEMACON2_n1859SistemaVersao_Codigo, SISTEMACON2_A1831Sistema_Responsavel, SISTEMACON2_n1831Sistema_Responsavel, SISTEMACON2_A1399Sistema_ImpUserCod, SISTEMACON2_n1399Sistema_ImpUserCod, SISTEMACON2_A1401Sistema_ImpData,
               SISTEMACON2_n1401Sistema_ImpData, SISTEMACON2_A700Sistema_Tecnica, SISTEMACON2_n700Sistema_Tecnica, SISTEMACON2_A699Sistema_Tipo, SISTEMACON2_n699Sistema_Tipo, SISTEMACON2_A689Sistema_Esforco, SISTEMACON2_n689Sistema_Esforco, SISTEMACON2_A688Sistema_Prazo, SISTEMACON2_n688Sistema_Prazo, SISTEMACON2_A687Sistema_Custo,
               SISTEMACON2_n687Sistema_Custo, SISTEMACON2_A686Sistema_FatorAjuste, SISTEMACON2_n686Sistema_FatorAjuste, SISTEMACON2_A513Sistema_Coordenacao, SISTEMACON2_n513Sistema_Coordenacao, SISTEMACON2_A351AmbienteTecnologico_Codigo, SISTEMACON2_n351AmbienteTecnologico_Codigo, SISTEMACON2_A416Sistema_Nome, SISTEMACON2_A137Metodologia_Codigo, SISTEMACON2_n137Metodologia_Codigo,
               SISTEMACON2_A135Sistema_AreaTrabalhoCod, SISTEMACON2_A130Sistema_Ativo, SISTEMACON2_A129Sistema_Sigla, SISTEMACON2_A128Sistema_Descricao, SISTEMACON2_n128Sistema_Descricao, SISTEMACON2_A127Sistema_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2130Sistema_Ultimo ;
      private short A689Sistema_Esforco ;
      private short A688Sistema_Prazo ;
      private short AV13Sistema_Prazo ;
      private short AV14Sistema_Esforco ;
      private short AV22Sistema_Ultimo ;
      private int A1859SistemaVersao_Codigo ;
      private int A1831Sistema_Responsavel ;
      private int A1399Sistema_ImpUserCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A137Metodologia_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int GIGXA0025 ;
      private int AV2Sistema_Codigo ;
      private int AV6Sistema_AreaTrabalhoCod ;
      private int AV7Metodologia_Codigo ;
      private int AV9AmbienteTecnologico_Codigo ;
      private int AV18Sistema_ImpUserCod ;
      private int AV19Sistema_Responsavel ;
      private int AV20SistemaVersao_Codigo ;
      private int A1826GpoObjCtrl_Codigo ;
      private int AV23Sistema_GpoObjCtrlCod ;
      private decimal A687Sistema_Custo ;
      private decimal A686Sistema_FatorAjuste ;
      private decimal AV11Sistema_FatorAjuste ;
      private decimal AV12Sistema_Custo ;
      private String cmdBuffer ;
      private String scmdbuf ;
      private String A700Sistema_Tecnica ;
      private String A699Sistema_Tipo ;
      private String A129Sistema_Sigla ;
      private String AV4Sistema_Sigla ;
      private String AV15Sistema_Tipo ;
      private String AV16Sistema_Tecnica ;
      private String Gx_emsg ;
      private DateTime A1401Sistema_ImpData ;
      private DateTime AV17Sistema_ImpData ;
      private bool n2109Sistema_Repositorio ;
      private bool n1859SistemaVersao_Codigo ;
      private bool n1831Sistema_Responsavel ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n1401Sistema_ImpData ;
      private bool n700Sistema_Tecnica ;
      private bool n699Sistema_Tipo ;
      private bool n689Sistema_Esforco ;
      private bool n688Sistema_Prazo ;
      private bool n687Sistema_Custo ;
      private bool n686Sistema_FatorAjuste ;
      private bool n513Sistema_Coordenacao ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n137Metodologia_Codigo ;
      private bool A130Sistema_Ativo ;
      private bool n128Sistema_Descricao ;
      private bool nV3Sistema_Descricao ;
      private bool AV5Sistema_Ativo ;
      private bool nV7Metodologia_Codigo ;
      private bool nV9AmbienteTecnologico_Codigo ;
      private bool nV10Sistema_Coordenacao ;
      private bool nV11Sistema_FatorAjuste ;
      private bool nV12Sistema_Custo ;
      private bool nV13Sistema_Prazo ;
      private bool nV14Sistema_Esforco ;
      private bool nV15Sistema_Tipo ;
      private bool nV16Sistema_Tecnica ;
      private bool nV17Sistema_ImpData ;
      private bool nV18Sistema_ImpUserCod ;
      private bool nV19Sistema_Responsavel ;
      private bool nV20SistemaVersao_Codigo ;
      private bool nV21Sistema_Repositorio ;
      private bool nV23Sistema_GpoObjCtrlCod ;
      private String A128Sistema_Descricao ;
      private String AV3Sistema_Descricao ;
      private String A2109Sistema_Repositorio ;
      private String A513Sistema_Coordenacao ;
      private String A416Sistema_Nome ;
      private String AV8Sistema_Nome ;
      private String AV10Sistema_Coordenacao ;
      private String AV21Sistema_Repositorio ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GxCommand RGZ ;
      private IDataStoreProvider pr_default ;
      private short[] SISTEMACON2_A2130Sistema_Ultimo ;
      private String[] SISTEMACON2_A2109Sistema_Repositorio ;
      private bool[] SISTEMACON2_n2109Sistema_Repositorio ;
      private int[] SISTEMACON2_A1859SistemaVersao_Codigo ;
      private bool[] SISTEMACON2_n1859SistemaVersao_Codigo ;
      private int[] SISTEMACON2_A1831Sistema_Responsavel ;
      private bool[] SISTEMACON2_n1831Sistema_Responsavel ;
      private int[] SISTEMACON2_A1399Sistema_ImpUserCod ;
      private bool[] SISTEMACON2_n1399Sistema_ImpUserCod ;
      private DateTime[] SISTEMACON2_A1401Sistema_ImpData ;
      private bool[] SISTEMACON2_n1401Sistema_ImpData ;
      private String[] SISTEMACON2_A700Sistema_Tecnica ;
      private bool[] SISTEMACON2_n700Sistema_Tecnica ;
      private String[] SISTEMACON2_A699Sistema_Tipo ;
      private bool[] SISTEMACON2_n699Sistema_Tipo ;
      private short[] SISTEMACON2_A689Sistema_Esforco ;
      private bool[] SISTEMACON2_n689Sistema_Esforco ;
      private short[] SISTEMACON2_A688Sistema_Prazo ;
      private bool[] SISTEMACON2_n688Sistema_Prazo ;
      private decimal[] SISTEMACON2_A687Sistema_Custo ;
      private bool[] SISTEMACON2_n687Sistema_Custo ;
      private decimal[] SISTEMACON2_A686Sistema_FatorAjuste ;
      private bool[] SISTEMACON2_n686Sistema_FatorAjuste ;
      private String[] SISTEMACON2_A513Sistema_Coordenacao ;
      private bool[] SISTEMACON2_n513Sistema_Coordenacao ;
      private int[] SISTEMACON2_A351AmbienteTecnologico_Codigo ;
      private bool[] SISTEMACON2_n351AmbienteTecnologico_Codigo ;
      private String[] SISTEMACON2_A416Sistema_Nome ;
      private int[] SISTEMACON2_A137Metodologia_Codigo ;
      private bool[] SISTEMACON2_n137Metodologia_Codigo ;
      private int[] SISTEMACON2_A135Sistema_AreaTrabalhoCod ;
      private bool[] SISTEMACON2_A130Sistema_Ativo ;
      private String[] SISTEMACON2_A129Sistema_Sigla ;
      private String[] SISTEMACON2_A128Sistema_Descricao ;
      private bool[] SISTEMACON2_n128Sistema_Descricao ;
      private int[] SISTEMACON2_A127Sistema_Codigo ;
   }

   public class sistemaconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmSISTEMACON2 ;
          prmSISTEMACON2 = new Object[] {
          } ;
          Object[] prmSISTEMACON3 ;
          prmSISTEMACON3 = new Object[] {
          new Object[] {"@AV2Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV3Sistema_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV4Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV5Sistema_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Metodologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV9AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Sistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11Sistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV12Sistema_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV13Sistema_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV14Sistema_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15Sistema_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@AV16Sistema_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@AV17Sistema_ImpData",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18Sistema_ImpUserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19Sistema_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20SistemaVersao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21Sistema_Repositorio",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV22Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("SISTEMACON2", "SELECT [Sistema_Ultimo], [Sistema_Repositorio], [SistemaVersao_Codigo], [Sistema_Responsavel], [Sistema_ImpUserCod], [Sistema_ImpData], [Sistema_Tecnica], [Sistema_Tipo], [Sistema_Esforco], [Sistema_Prazo], [Sistema_Custo], [Sistema_FatorAjuste], [Sistema_Coordenacao], [AmbienteTecnologico_Codigo], [Sistema_Nome], [Metodologia_Codigo], [Sistema_AreaTrabalhoCod], [Sistema_Ativo], [Sistema_Sigla], [Sistema_Descricao], [Sistema_Codigo] FROM [Sistema] ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmSISTEMACON2,100,0,true,false )
             ,new CursorDef("SISTEMACON3", "INSERT INTO [GXA0025]([Sistema_Codigo], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Ativo], [Sistema_AreaTrabalhoCod], [Metodologia_Codigo], [Sistema_Nome], [AmbienteTecnologico_Codigo], [Sistema_Coordenacao], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_ImpData], [Sistema_ImpUserCod], [Sistema_Responsavel], [SistemaVersao_Codigo], [Sistema_Repositorio], [Sistema_Ultimo], [Sistema_GpoObjCtrlCod]) VALUES(@AV2Sistema_Codigo, @AV3Sistema_Descricao, @AV4Sistema_Sigla, @AV5Sistema_Ativo, @AV6Sistema_AreaTrabalhoCod, @AV7Metodologia_Codigo, @AV8Sistema_Nome, @AV9AmbienteTecnologico_Codigo, @AV10Sistema_Coordenacao, @AV11Sistema_FatorAjuste, @AV12Sistema_Custo, @AV13Sistema_Prazo, @AV14Sistema_Esforco, @AV15Sistema_Tipo, @AV16Sistema_Tecnica, @AV17Sistema_ImpData, @AV18Sistema_ImpUserCod, @AV19Sistema_Responsavel, @AV20SistemaVersao_Codigo, @AV21Sistema_Repositorio, @AV22Sistema_Ultimo, @AV23Sistema_GpoObjCtrlCod)", GxErrorMask.GX_NOMASK,prmSISTEMACON3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getVarchar(15) ;
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.getBool(18) ;
                ((String[]) buf[32])[0] = rslt.getString(19, 25) ;
                ((String[]) buf[33])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((int[]) buf[35])[0] = rslt.getInt(21) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (bool)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[7]);
                }
                stmt.SetParameter(7, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(16, (DateTime)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[34]);
                }
                stmt.SetParameter(21, (short)parms[35]);
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[37]);
                }
                return;
       }
    }

 }

}
