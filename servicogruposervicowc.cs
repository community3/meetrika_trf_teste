/*
               File: ServicoGrupoServicoWC
        Description: Servico Grupo Servico WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:28.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicogruposervicowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicogruposervicowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicogruposervicowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ServicoGrupo_Codigo )
      {
         this.AV7ServicoGrupo_Codigo = aP0_ServicoGrupo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavServico_tipohierarquia = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavServico_ispublico1 = new GXCombobox();
         cmbServico_Ativo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ServicoGrupo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_47 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_47_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_47_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV54Servico_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Servico_Nome1", AV54Servico_Nome1);
                  AV76Servico_IsPublico1 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76Servico_IsPublico1", AV76Servico_IsPublico1);
                  AV62TFServico_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
                  AV63TFServico_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFServico_Nome_Sel", AV63TFServico_Nome_Sel);
                  AV66TFServico_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
                  AV67TFServico_Sigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFServico_Sigla_Sel", AV67TFServico_Sigla_Sel);
                  AV70TFServico_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFServico_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0));
                  AV7ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0)));
                  AV64ddo_Servico_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_Servico_NomeTitleControlIdToReplace", AV64ddo_Servico_NomeTitleControlIdToReplace);
                  AV68ddo_Servico_SiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_Servico_SiglaTitleControlIdToReplace", AV68ddo_Servico_SiglaTitleControlIdToReplace);
                  AV71ddo_Servico_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71ddo_Servico_AtivoTitleControlIdToReplace", AV71ddo_Servico_AtivoTitleControlIdToReplace);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV87Pgmname = GetNextPar( );
                  AV57Servico_TipoHierarquia = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A632Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n601ContagemResultado_Servico = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
                  A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1636ContagemResultado_ServicoSS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV54Servico_Nome1, AV76Servico_IsPublico1, AV62TFServico_Nome, AV63TFServico_Nome_Sel, AV66TFServico_Sigla, AV67TFServico_Sigla_Sel, AV70TFServico_Ativo_Sel, AV7ServicoGrupo_Codigo, AV64ddo_Servico_NomeTitleControlIdToReplace, AV68ddo_Servico_SiglaTitleControlIdToReplace, AV71ddo_Servico_AtivoTitleControlIdToReplace, AV6WWPContext, AV87Pgmname, AV57Servico_TipoHierarquia, AV11GridState, A155Servico_Codigo, A632Servico_Ativo, A601ContagemResultado_Servico, A1636ContagemResultado_ServicoSS, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA6U2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV87Pgmname = "ServicoGrupoServicoWC";
               context.Gx_err = 0;
               WS6U2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Grupo Servico WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216152829");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicogruposervicowc.aspx") + "?" + UrlEncode("" +AV7ServicoGrupo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSERVICO_NOME1", StringUtil.RTrim( AV54Servico_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSERVICO_ISPUBLICO1", StringUtil.BoolToStr( AV76Servico_IsPublico1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICO_NOME", StringUtil.RTrim( AV62TFServico_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICO_NOME_SEL", StringUtil.RTrim( AV63TFServico_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICO_SIGLA", StringUtil.RTrim( AV66TFServico_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICO_SIGLA_SEL", StringUtil.RTrim( AV67TFServico_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70TFServico_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_47", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_47), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV72DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV72DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICO_NOMETITLEFILTERDATA", AV61Servico_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICO_NOMETITLEFILTERDATA", AV61Servico_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICO_SIGLATITLEFILTERDATA", AV65Servico_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICO_SIGLATITLEFILTERDATA", AV65Servico_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICO_ATIVOTITLEFILTERDATA", AV69Servico_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICO_ATIVOTITLEFILTERDATA", AV69Servico_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoGrupo_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV87Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_SERVICOSS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Closeable", StringUtil.BoolToStr( Confirmpanel_Closeable));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Caption", StringUtil.RTrim( Ddo_servico_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Tooltip", StringUtil.RTrim( Ddo_servico_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Cls", StringUtil.RTrim( Ddo_servico_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servico_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servico_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Filtertype", StringUtil.RTrim( Ddo_servico_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servico_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servico_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Datalisttype", StringUtil.RTrim( Ddo_servico_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Datalistproc", StringUtil.RTrim( Ddo_servico_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Sortasc", StringUtil.RTrim( Ddo_servico_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Sortdsc", StringUtil.RTrim( Ddo_servico_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Loadingdata", StringUtil.RTrim( Ddo_servico_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servico_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servico_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servico_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Caption", StringUtil.RTrim( Ddo_servico_sigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_servico_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Cls", StringUtil.RTrim( Ddo_servico_sigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_servico_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_servico_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_servico_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_servico_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_servico_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_servico_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_servico_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_servico_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_servico_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_servico_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_servico_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_servico_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_servico_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_servico_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_servico_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Caption", StringUtil.RTrim( Ddo_servico_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_servico_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Cls", StringUtil.RTrim( Ddo_servico_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_servico_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_servico_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_servico_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_servico_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_servico_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_servico_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_servico_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_servico_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_servico_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_servico_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servico_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_servico_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_servico_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_servico_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm6U2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicogruposervicowc.js", "?20206216152984");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoGrupoServicoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Grupo Servico WC" ;
      }

      protected void WB6U0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicogruposervicowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
               context.AddJavascriptSource("DVelop/Shared/dom.js", "");
               context.AddJavascriptSource("DVelop/Shared/event.js", "");
               context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
               context.AddJavascriptSource("DVelop/Shared/container.js", "");
               context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_6U2( true) ;
         }
         else
         {
            wb_table1_2_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_Internalname, StringUtil.RTrim( AV62TFServico_Nome), StringUtil.RTrim( context.localUtil.Format( AV62TFServico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGrupoServicoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_sel_Internalname, StringUtil.RTrim( AV63TFServico_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV63TFServico_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGrupoServicoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_sigla_Internalname, StringUtil.RTrim( AV66TFServico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV66TFServico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGrupoServicoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_sigla_sel_Internalname, StringUtil.RTrim( AV67TFServico_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV67TFServico_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGrupoServicoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70TFServico_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV70TFServico_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoGrupoServicoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, AV64ddo_Servico_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_servico_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoGrupoServicoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname, AV68ddo_Servico_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_servico_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoGrupoServicoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_ativotitlecontrolidtoreplace_Internalname, AV71ddo_Servico_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", 0, edtavDdo_servico_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoGrupoServicoWC.htm");
         }
         wbLoad = true;
      }

      protected void START6U2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Grupo Servico WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP6U0( ) ;
            }
         }
      }

      protected void WS6U2( )
      {
         START6U2( ) ;
         EVT6U2( ) ;
      }

      protected void EVT6U2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E116U2 */
                                    E116U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E126U2 */
                                    E126U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E136U2 */
                                    E136U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E146U2 */
                                    E146U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E156U2 */
                                    E156U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E166U2 */
                                    E166U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E176U2 */
                                    E176U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E186U2 */
                                    E186U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E196U2 */
                                    E196U2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VDELETE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VDELETE.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6U0( ) ;
                              }
                              nGXsfl_47_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
                              SubsflControlProps_472( ) ;
                              AV30Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV80Update_GXI : context.convertURL( context.PathToRelativeUrl( AV30Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV81Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV32Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Display)) ? AV82Display_GXI : context.convertURL( context.PathToRelativeUrl( AV32Display))));
                              A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                              A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
                              A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                              A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
                              cmbServico_Ativo.Name = cmbServico_Ativo_Internalname;
                              cmbServico_Ativo.CurrentValue = cgiGet( cmbServico_Ativo_Internalname);
                              A632Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbServico_Ativo_Internalname));
                              AV56AssociarUsuarios = cgiGet( edtavAssociarusuarios_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarusuarios_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV56AssociarUsuarios)) ? AV83Associarusuarios_GXI : context.convertURL( context.PathToRelativeUrl( AV56AssociarUsuarios))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E206U2 */
                                          E206U2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E216U2 */
                                          E216U2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E226U2 */
                                          E226U2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDELETE.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E236U2 */
                                          E236U2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Servico_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME1"), AV54Servico_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Servico_ispublico1 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vSERVICO_ISPUBLICO1")) != AV76Servico_IsPublico1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservico_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_NOME"), AV62TFServico_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservico_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_NOME_SEL"), AV63TFServico_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservico_sigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_SIGLA"), AV66TFServico_Sigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservico_sigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_SIGLA_SEL"), AV67TFServico_Sigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservico_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV70TFServico_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP6U0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6U2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm6U2( ) ;
            }
         }
      }

      protected void PA6U2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavServico_tipohierarquia.Name = "vSERVICO_TIPOHIERARQUIA";
            cmbavServico_tipohierarquia.WebTags = "";
            cmbavServico_tipohierarquia.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Todos", 0);
            cmbavServico_tipohierarquia.addItem("1", "Prim�rio", 0);
            cmbavServico_tipohierarquia.addItem("2", "Secund�rio", 0);
            if ( cmbavServico_tipohierarquia.ItemCount > 0 )
            {
               AV57Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbavServico_tipohierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICO_ISPUBLICO", "P�blico?", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavServico_ispublico1.Name = "vSERVICO_ISPUBLICO1";
            cmbavServico_ispublico1.WebTags = "";
            cmbavServico_ispublico1.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbavServico_ispublico1.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbavServico_ispublico1.ItemCount > 0 )
            {
               AV76Servico_IsPublico1 = StringUtil.StrToBool( cmbavServico_ispublico1.getValidValue(StringUtil.BoolToStr( AV76Servico_IsPublico1)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76Servico_IsPublico1", AV76Servico_IsPublico1);
            }
            GXCCtl = "SERVICO_ATIVO_" + sGXsfl_47_idx;
            cmbServico_Ativo.Name = GXCCtl;
            cmbServico_Ativo.WebTags = "";
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_Ativo.ItemCount > 0 )
            {
               A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_472( ) ;
         while ( nGXsfl_47_idx <= nRC_GXsfl_47 )
         {
            sendrow_472( ) ;
            nGXsfl_47_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_47_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_47_idx+1));
            sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
            SubsflControlProps_472( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV54Servico_Nome1 ,
                                       bool AV76Servico_IsPublico1 ,
                                       String AV62TFServico_Nome ,
                                       String AV63TFServico_Nome_Sel ,
                                       String AV66TFServico_Sigla ,
                                       String AV67TFServico_Sigla_Sel ,
                                       short AV70TFServico_Ativo_Sel ,
                                       int AV7ServicoGrupo_Codigo ,
                                       String AV64ddo_Servico_NomeTitleControlIdToReplace ,
                                       String AV68ddo_Servico_SiglaTitleControlIdToReplace ,
                                       String AV71ddo_Servico_AtivoTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV87Pgmname ,
                                       short AV57Servico_TipoHierarquia ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       int A155Servico_Codigo ,
                                       bool A632Servico_Ativo ,
                                       int A601ContagemResultado_Servico ,
                                       int A1636ContagemResultado_ServicoSS ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6U2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_ATIVO", StringUtil.BoolToStr( A632Servico_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavServico_tipohierarquia.ItemCount > 0 )
         {
            AV57Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbavServico_tipohierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavServico_ispublico1.ItemCount > 0 )
         {
            AV76Servico_IsPublico1 = StringUtil.StrToBool( cmbavServico_ispublico1.getValidValue(StringUtil.BoolToStr( AV76Servico_IsPublico1)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76Servico_IsPublico1", AV76Servico_IsPublico1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6U2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV87Pgmname = "ServicoGrupoServicoWC";
         context.Gx_err = 0;
      }

      protected void RF6U2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 47;
         /* Execute user event: E216U2 */
         E216U2 ();
         nGXsfl_47_idx = 1;
         sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
         SubsflControlProps_472( ) ;
         nGXsfl_47_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_472( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV54Servico_Nome1 ,
                                                 AV76Servico_IsPublico1 ,
                                                 AV63TFServico_Nome_Sel ,
                                                 AV62TFServico_Nome ,
                                                 AV67TFServico_Sigla_Sel ,
                                                 AV66TFServico_Sigla ,
                                                 AV70TFServico_Ativo_Sel ,
                                                 A608Servico_Nome ,
                                                 A1635Servico_IsPublico ,
                                                 A605Servico_Sigla ,
                                                 A632Servico_Ativo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A157ServicoGrupo_Codigo ,
                                                 AV7ServicoGrupo_Codigo ,
                                                 A1530Servico_TipoHierarquia },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV54Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Servico_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Servico_Nome1", AV54Servico_Nome1);
            lV62TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV62TFServico_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
            lV66TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV66TFServico_Sigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
            /* Using cursor H006U2 */
            pr_default.execute(0, new Object[] {AV7ServicoGrupo_Codigo, lV54Servico_Nome1, AV76Servico_IsPublico1, lV62TFServico_Nome, AV63TFServico_Nome_Sel, lV66TFServico_Sigla, AV67TFServico_Sigla_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_47_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A155Servico_Codigo = H006U2_A155Servico_Codigo[0];
               A1635Servico_IsPublico = H006U2_A1635Servico_IsPublico[0];
               A1530Servico_TipoHierarquia = H006U2_A1530Servico_TipoHierarquia[0];
               n1530Servico_TipoHierarquia = H006U2_n1530Servico_TipoHierarquia[0];
               A632Servico_Ativo = H006U2_A632Servico_Ativo[0];
               A605Servico_Sigla = H006U2_A605Servico_Sigla[0];
               A608Servico_Nome = H006U2_A608Servico_Nome[0];
               A157ServicoGrupo_Codigo = H006U2_A157ServicoGrupo_Codigo[0];
               /* Execute user event: E226U2 */
               E226U2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 47;
            WB6U0( ) ;
         }
         nGXsfl_47_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV54Servico_Nome1 ,
                                              AV76Servico_IsPublico1 ,
                                              AV63TFServico_Nome_Sel ,
                                              AV62TFServico_Nome ,
                                              AV67TFServico_Sigla_Sel ,
                                              AV66TFServico_Sigla ,
                                              AV70TFServico_Ativo_Sel ,
                                              A608Servico_Nome ,
                                              A1635Servico_IsPublico ,
                                              A605Servico_Sigla ,
                                              A632Servico_Ativo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A157ServicoGrupo_Codigo ,
                                              AV7ServicoGrupo_Codigo ,
                                              A1530Servico_TipoHierarquia },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV54Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV54Servico_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Servico_Nome1", AV54Servico_Nome1);
         lV62TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV62TFServico_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
         lV66TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV66TFServico_Sigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
         /* Using cursor H006U3 */
         pr_default.execute(1, new Object[] {AV7ServicoGrupo_Codigo, lV54Servico_Nome1, AV76Servico_IsPublico1, lV62TFServico_Nome, AV63TFServico_Nome_Sel, lV66TFServico_Sigla, AV67TFServico_Sigla_Sel});
         GRID_nRecordCount = H006U3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV54Servico_Nome1, AV76Servico_IsPublico1, AV62TFServico_Nome, AV63TFServico_Nome_Sel, AV66TFServico_Sigla, AV67TFServico_Sigla_Sel, AV70TFServico_Ativo_Sel, AV7ServicoGrupo_Codigo, AV64ddo_Servico_NomeTitleControlIdToReplace, AV68ddo_Servico_SiglaTitleControlIdToReplace, AV71ddo_Servico_AtivoTitleControlIdToReplace, AV6WWPContext, AV87Pgmname, AV57Servico_TipoHierarquia, AV11GridState, A155Servico_Codigo, A632Servico_Ativo, A601ContagemResultado_Servico, A1636ContagemResultado_ServicoSS, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV54Servico_Nome1, AV76Servico_IsPublico1, AV62TFServico_Nome, AV63TFServico_Nome_Sel, AV66TFServico_Sigla, AV67TFServico_Sigla_Sel, AV70TFServico_Ativo_Sel, AV7ServicoGrupo_Codigo, AV64ddo_Servico_NomeTitleControlIdToReplace, AV68ddo_Servico_SiglaTitleControlIdToReplace, AV71ddo_Servico_AtivoTitleControlIdToReplace, AV6WWPContext, AV87Pgmname, AV57Servico_TipoHierarquia, AV11GridState, A155Servico_Codigo, A632Servico_Ativo, A601ContagemResultado_Servico, A1636ContagemResultado_ServicoSS, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV54Servico_Nome1, AV76Servico_IsPublico1, AV62TFServico_Nome, AV63TFServico_Nome_Sel, AV66TFServico_Sigla, AV67TFServico_Sigla_Sel, AV70TFServico_Ativo_Sel, AV7ServicoGrupo_Codigo, AV64ddo_Servico_NomeTitleControlIdToReplace, AV68ddo_Servico_SiglaTitleControlIdToReplace, AV71ddo_Servico_AtivoTitleControlIdToReplace, AV6WWPContext, AV87Pgmname, AV57Servico_TipoHierarquia, AV11GridState, A155Servico_Codigo, A632Servico_Ativo, A601ContagemResultado_Servico, A1636ContagemResultado_ServicoSS, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV54Servico_Nome1, AV76Servico_IsPublico1, AV62TFServico_Nome, AV63TFServico_Nome_Sel, AV66TFServico_Sigla, AV67TFServico_Sigla_Sel, AV70TFServico_Ativo_Sel, AV7ServicoGrupo_Codigo, AV64ddo_Servico_NomeTitleControlIdToReplace, AV68ddo_Servico_SiglaTitleControlIdToReplace, AV71ddo_Servico_AtivoTitleControlIdToReplace, AV6WWPContext, AV87Pgmname, AV57Servico_TipoHierarquia, AV11GridState, A155Servico_Codigo, A632Servico_Ativo, A601ContagemResultado_Servico, A1636ContagemResultado_ServicoSS, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV54Servico_Nome1, AV76Servico_IsPublico1, AV62TFServico_Nome, AV63TFServico_Nome_Sel, AV66TFServico_Sigla, AV67TFServico_Sigla_Sel, AV70TFServico_Ativo_Sel, AV7ServicoGrupo_Codigo, AV64ddo_Servico_NomeTitleControlIdToReplace, AV68ddo_Servico_SiglaTitleControlIdToReplace, AV71ddo_Servico_AtivoTitleControlIdToReplace, AV6WWPContext, AV87Pgmname, AV57Servico_TipoHierarquia, AV11GridState, A155Servico_Codigo, A632Servico_Ativo, A601ContagemResultado_Servico, A1636ContagemResultado_ServicoSS, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6U0( )
      {
         /* Before Start, stand alone formulas. */
         AV87Pgmname = "ServicoGrupoServicoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E206U2 */
         E206U2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV72DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICO_NOMETITLEFILTERDATA"), AV61Servico_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICO_SIGLATITLEFILTERDATA"), AV65Servico_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICO_ATIVOTITLEFILTERDATA"), AV69Servico_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavServico_tipohierarquia.Name = cmbavServico_tipohierarquia_Internalname;
            cmbavServico_tipohierarquia.CurrentValue = cgiGet( cmbavServico_tipohierarquia_Internalname);
            AV57Servico_TipoHierarquia = (short)(NumberUtil.Val( cgiGet( cmbavServico_tipohierarquia_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV54Servico_Nome1 = StringUtil.Upper( cgiGet( edtavServico_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Servico_Nome1", AV54Servico_Nome1);
            cmbavServico_ispublico1.Name = cmbavServico_ispublico1_Internalname;
            cmbavServico_ispublico1.CurrentValue = cgiGet( cmbavServico_ispublico1_Internalname);
            AV76Servico_IsPublico1 = StringUtil.StrToBool( cgiGet( cmbavServico_ispublico1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76Servico_IsPublico1", AV76Servico_IsPublico1);
            AV62TFServico_Nome = StringUtil.Upper( cgiGet( edtavTfservico_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
            AV63TFServico_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservico_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFServico_Nome_Sel", AV63TFServico_Nome_Sel);
            AV66TFServico_Sigla = StringUtil.Upper( cgiGet( edtavTfservico_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
            AV67TFServico_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfservico_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFServico_Sigla_Sel", AV67TFServico_Sigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservico_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservico_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICO_ATIVO_SEL");
               GX_FocusControl = edtavTfservico_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFServico_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFServico_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0));
            }
            else
            {
               AV70TFServico_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfservico_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFServico_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0));
            }
            AV64ddo_Servico_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servico_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_Servico_NomeTitleControlIdToReplace", AV64ddo_Servico_NomeTitleControlIdToReplace);
            AV68ddo_Servico_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_Servico_SiglaTitleControlIdToReplace", AV68ddo_Servico_SiglaTitleControlIdToReplace);
            AV71ddo_Servico_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_servico_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71ddo_Servico_AtivoTitleControlIdToReplace", AV71ddo_Servico_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_47 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_47"), ",", "."));
            AV74GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV75GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoGrupo_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Confirmpanel_Width = cgiGet( sPrefix+"CONFIRMPANEL_Width");
            Confirmpanel_Closeable = StringUtil.StrToBool( cgiGet( sPrefix+"CONFIRMPANEL_Closeable"));
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( sPrefix+"CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( sPrefix+"CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( sPrefix+"CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Buttoncanceltext = cgiGet( sPrefix+"CONFIRMPANEL_Buttoncanceltext");
            Confirmpanel_Confirmtype = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtype");
            Ddo_servico_nome_Caption = cgiGet( sPrefix+"DDO_SERVICO_NOME_Caption");
            Ddo_servico_nome_Tooltip = cgiGet( sPrefix+"DDO_SERVICO_NOME_Tooltip");
            Ddo_servico_nome_Cls = cgiGet( sPrefix+"DDO_SERVICO_NOME_Cls");
            Ddo_servico_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICO_NOME_Filteredtext_set");
            Ddo_servico_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICO_NOME_Selectedvalue_set");
            Ddo_servico_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICO_NOME_Dropdownoptionstype");
            Ddo_servico_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICO_NOME_Titlecontrolidtoreplace");
            Ddo_servico_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_NOME_Includesortasc"));
            Ddo_servico_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_NOME_Includesortdsc"));
            Ddo_servico_nome_Sortedstatus = cgiGet( sPrefix+"DDO_SERVICO_NOME_Sortedstatus");
            Ddo_servico_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_NOME_Includefilter"));
            Ddo_servico_nome_Filtertype = cgiGet( sPrefix+"DDO_SERVICO_NOME_Filtertype");
            Ddo_servico_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_NOME_Filterisrange"));
            Ddo_servico_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_NOME_Includedatalist"));
            Ddo_servico_nome_Datalisttype = cgiGet( sPrefix+"DDO_SERVICO_NOME_Datalisttype");
            Ddo_servico_nome_Datalistproc = cgiGet( sPrefix+"DDO_SERVICO_NOME_Datalistproc");
            Ddo_servico_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SERVICO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_nome_Sortasc = cgiGet( sPrefix+"DDO_SERVICO_NOME_Sortasc");
            Ddo_servico_nome_Sortdsc = cgiGet( sPrefix+"DDO_SERVICO_NOME_Sortdsc");
            Ddo_servico_nome_Loadingdata = cgiGet( sPrefix+"DDO_SERVICO_NOME_Loadingdata");
            Ddo_servico_nome_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICO_NOME_Cleanfilter");
            Ddo_servico_nome_Noresultsfound = cgiGet( sPrefix+"DDO_SERVICO_NOME_Noresultsfound");
            Ddo_servico_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICO_NOME_Searchbuttontext");
            Ddo_servico_sigla_Caption = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Caption");
            Ddo_servico_sigla_Tooltip = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Tooltip");
            Ddo_servico_sigla_Cls = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Cls");
            Ddo_servico_sigla_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Filteredtext_set");
            Ddo_servico_sigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Selectedvalue_set");
            Ddo_servico_sigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Dropdownoptionstype");
            Ddo_servico_sigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Titlecontrolidtoreplace");
            Ddo_servico_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Includesortasc"));
            Ddo_servico_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Includesortdsc"));
            Ddo_servico_sigla_Sortedstatus = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Sortedstatus");
            Ddo_servico_sigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Includefilter"));
            Ddo_servico_sigla_Filtertype = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Filtertype");
            Ddo_servico_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Filterisrange"));
            Ddo_servico_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Includedatalist"));
            Ddo_servico_sigla_Datalisttype = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Datalisttype");
            Ddo_servico_sigla_Datalistproc = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Datalistproc");
            Ddo_servico_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_sigla_Sortasc = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Sortasc");
            Ddo_servico_sigla_Sortdsc = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Sortdsc");
            Ddo_servico_sigla_Loadingdata = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Loadingdata");
            Ddo_servico_sigla_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Cleanfilter");
            Ddo_servico_sigla_Noresultsfound = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Noresultsfound");
            Ddo_servico_sigla_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Searchbuttontext");
            Ddo_servico_ativo_Caption = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Caption");
            Ddo_servico_ativo_Tooltip = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Tooltip");
            Ddo_servico_ativo_Cls = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Cls");
            Ddo_servico_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Selectedvalue_set");
            Ddo_servico_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Dropdownoptionstype");
            Ddo_servico_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Titlecontrolidtoreplace");
            Ddo_servico_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Includesortasc"));
            Ddo_servico_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Includesortdsc"));
            Ddo_servico_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Sortedstatus");
            Ddo_servico_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Includefilter"));
            Ddo_servico_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Includedatalist"));
            Ddo_servico_ativo_Datalisttype = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Datalisttype");
            Ddo_servico_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Datalistfixedvalues");
            Ddo_servico_ativo_Sortasc = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Sortasc");
            Ddo_servico_ativo_Sortdsc = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Sortdsc");
            Ddo_servico_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Cleanfilter");
            Ddo_servico_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servico_nome_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICO_NOME_Activeeventkey");
            Ddo_servico_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICO_NOME_Filteredtext_get");
            Ddo_servico_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICO_NOME_Selectedvalue_get");
            Ddo_servico_sigla_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Activeeventkey");
            Ddo_servico_sigla_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Filteredtext_get");
            Ddo_servico_sigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICO_SIGLA_Selectedvalue_get");
            Ddo_servico_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Activeeventkey");
            Ddo_servico_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICO_ATIVO_Selectedvalue_get");
            Confirmpanel_Result = cgiGet( sPrefix+"CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME1"), AV54Servico_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vSERVICO_ISPUBLICO1")) != AV76Servico_IsPublico1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_NOME"), AV62TFServico_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_NOME_SEL"), AV63TFServico_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_SIGLA"), AV66TFServico_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICO_SIGLA_SEL"), AV67TFServico_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSERVICO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV70TFServico_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E206U2 */
         E206U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E206U2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV16DynamicFiltersSelector1 = "SERVICO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV57Servico_TipoHierarquia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
         edtavTfservico_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservico_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_Visible), 5, 0)));
         edtavTfservico_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservico_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_sel_Visible), 5, 0)));
         edtavTfservico_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservico_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_sigla_Visible), 5, 0)));
         edtavTfservico_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservico_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_sigla_sel_Visible), 5, 0)));
         edtavTfservico_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservico_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_ativo_sel_Visible), 5, 0)));
         Ddo_servico_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "TitleControlIdToReplace", Ddo_servico_nome_Titlecontrolidtoreplace);
         AV64ddo_Servico_NomeTitleControlIdToReplace = Ddo_servico_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_Servico_NomeTitleControlIdToReplace", AV64ddo_Servico_NomeTitleControlIdToReplace);
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "TitleControlIdToReplace", Ddo_servico_sigla_Titlecontrolidtoreplace);
         AV68ddo_Servico_SiglaTitleControlIdToReplace = Ddo_servico_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_Servico_SiglaTitleControlIdToReplace", AV68ddo_Servico_SiglaTitleControlIdToReplace);
         edtavDdo_servico_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "TitleControlIdToReplace", Ddo_servico_ativo_Titlecontrolidtoreplace);
         AV71ddo_Servico_AtivoTitleControlIdToReplace = Ddo_servico_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71ddo_Servico_AtivoTitleControlIdToReplace", AV71ddo_Servico_AtivoTitleControlIdToReplace);
         edtavDdo_servico_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servico_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o de Servi�o", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         cmbavOrderedby.addItem("3", "Ativo?", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV72DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV72DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E216U2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV61Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Servico_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Servico_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServico_Nome_Titleformat = 2;
         edtServico_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV64ddo_Servico_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Nome_Internalname, "Title", edtServico_Nome_Title);
         edtServico_Sigla_Titleformat = 2;
         edtServico_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV68ddo_Servico_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Sigla_Internalname, "Title", edtServico_Sigla_Title);
         cmbServico_Ativo_Titleformat = 2;
         cmbServico_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV71ddo_Servico_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Ativo_Internalname, "Title", cmbServico_Ativo.Title.Text);
         AV74GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74GridCurrentPage), 10, 0)));
         AV75GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75GridPageCount), 10, 0)));
         edtavAssociarusuarios_Title = "Usu�rios";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarusuarios_Internalname, "Title", edtavAssociarusuarios_Title);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV61Servico_NomeTitleFilterData", AV61Servico_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65Servico_SiglaTitleFilterData", AV65Servico_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV69Servico_AtivoTitleFilterData", AV69Servico_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E116U2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV73PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV73PageToGo) ;
         }
      }

      protected void E136U2( )
      {
         /* Ddo_servico_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFServico_Nome = Ddo_servico_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
            AV63TFServico_Nome_Sel = Ddo_servico_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFServico_Nome_Sel", AV63TFServico_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146U2( )
      {
         /* Ddo_servico_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_servico_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_servico_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFServico_Sigla = Ddo_servico_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
            AV67TFServico_Sigla_Sel = Ddo_servico_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFServico_Sigla_Sel", AV67TFServico_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E156U2( )
      {
         /* Ddo_servico_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_servico_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "SortedStatus", Ddo_servico_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servico_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_servico_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "SortedStatus", Ddo_servico_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servico_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFServico_Ativo_Sel = (short)(NumberUtil.Val( Ddo_servico_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFServico_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E226U2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV7ServicoGrupo_Codigo);
            AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
            AV80Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV30Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
            AV80Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV7ServicoGrupo_Codigo);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
            AV81Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
            AV81Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         AV32Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV32Display);
         AV82Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewservico.aspx") + "?" + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV56AssociarUsuarios = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarusuarios_Internalname, AV56AssociarUsuarios);
         AV83Associarusuarios_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarusuarios_Tooltiptext = "Clique aqui p/ associar os usu�rios deste servi�o!";
         if ( A632Servico_Ativo )
         {
            AV77Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV77Color = GXUtil.RGB( 255, 0, 0);
         }
         edtServico_Nome_Forecolor = (int)(AV77Color);
         edtServico_Sigla_Forecolor = (int)(AV77Color);
         if ( AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavAssociarusuarios_Enabled = 0;
            edtavAssociarusuarios_Tooltiptext = "Usu�rio logado n�o pertence a nenhuma Entidada!";
         }
         edtavDelete_Link = "";
         AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV30Update);
         AV80Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV29Delete);
         AV81Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         AV32Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV32Display);
         AV82Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         AV84GXLvl227 = 0;
         /* Using cursor H006U4 */
         pr_default.execute(2, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            AV84GXLvl227 = 1;
            edtavDelete_Visible = 0;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV84GXLvl227 == 0 )
         {
            AV85GXLvl231 = 0;
            /* Using cursor H006U5 */
            pr_default.execute(3, new Object[] {A155Servico_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H006U5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H006U5_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H006U5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H006U5_n601ContagemResultado_Servico[0];
               A601ContagemResultado_Servico = H006U5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H006U5_n601ContagemResultado_Servico[0];
               AV85GXLvl231 = 1;
               edtavDelete_Visible = 0;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            if ( AV85GXLvl231 == 0 )
            {
               AV86GXLvl236 = 0;
               /* Using cursor H006U6 */
               pr_default.execute(4, new Object[] {A155Servico_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1636ContagemResultado_ServicoSS = H006U6_A1636ContagemResultado_ServicoSS[0];
                  n1636ContagemResultado_ServicoSS = H006U6_n1636ContagemResultado_ServicoSS[0];
                  AV86GXLvl236 = 1;
                  edtavDelete_Visible = 0;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               if ( AV86GXLvl236 == 0 )
               {
                  edtavDelete_Visible = 1;
               }
            }
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 47;
         }
         sendrow_472( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_47_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(47, GridRow);
         }
      }

      protected void E166U2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E196U2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E176U2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavServico_tipohierarquia.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_tipohierarquia_Internalname, "Values", cmbavServico_tipohierarquia.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavServico_ispublico1.CurrentValue = StringUtil.BoolToStr( AV76Servico_IsPublico1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_ispublico1_Internalname, "Values", cmbavServico_ispublico1.ToJavascriptSource());
      }

      protected void E186U2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servico_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         Ddo_servico_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
         Ddo_servico_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "SortedStatus", Ddo_servico_ativo_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_servico_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_servico_sigla_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_servico_ativo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "SortedStatus", Ddo_servico_ativo_Sortedstatus);
         }
      }

      protected void S152( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServico_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         cmbavServico_ispublico1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_ispublico1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_ispublico1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 )
         {
            cmbavServico_ispublico1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_ispublico1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_ispublico1.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'CLEANFILTERS' Routine */
         AV57Servico_TipoHierarquia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
         AV62TFServico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
         Ddo_servico_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
         AV63TFServico_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFServico_Nome_Sel", AV63TFServico_Nome_Sel);
         Ddo_servico_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
         AV66TFServico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
         Ddo_servico_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "FilteredText_set", Ddo_servico_sigla_Filteredtext_set);
         AV67TFServico_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFServico_Sigla_Sel", AV67TFServico_Sigla_Sel);
         Ddo_servico_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "SelectedValue_set", Ddo_servico_sigla_Selectedvalue_set);
         AV70TFServico_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFServico_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0));
         Ddo_servico_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "SelectedValue_set", Ddo_servico_ativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "SERVICO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV54Servico_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Servico_Nome1", AV54Servico_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV87Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV87Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV87Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV88GXV1 = 1;
         while ( AV88GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV88GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "SERVICO_TIPOHIERARQUIA") == 0 )
            {
               AV57Servico_TipoHierarquia = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)));
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV62TFServico_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFServico_Nome", AV62TFServico_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFServico_Nome)) )
               {
                  Ddo_servico_nome_Filteredtext_set = AV62TFServico_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV63TFServico_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFServico_Nome_Sel", AV63TFServico_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFServico_Nome_Sel)) )
               {
                  Ddo_servico_nome_Selectedvalue_set = AV63TFServico_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA") == 0 )
            {
               AV66TFServico_Sigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFServico_Sigla", AV66TFServico_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFServico_Sigla)) )
               {
                  Ddo_servico_sigla_Filteredtext_set = AV66TFServico_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "FilteredText_set", Ddo_servico_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA_SEL") == 0 )
            {
               AV67TFServico_Sigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFServico_Sigla_Sel", AV67TFServico_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFServico_Sigla_Sel)) )
               {
                  Ddo_servico_sigla_Selectedvalue_set = AV67TFServico_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_sigla_Internalname, "SelectedValue_set", Ddo_servico_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICO_ATIVO_SEL") == 0 )
            {
               AV70TFServico_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFServico_Ativo_Sel", StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0));
               if ( ! (0==AV70TFServico_Ativo_Sel) )
               {
                  Ddo_servico_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servico_ativo_Internalname, "SelectedValue_set", Ddo_servico_ativo_Selectedvalue_set);
               }
            }
            AV88GXV1 = (int)(AV88GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV54Servico_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54Servico_Nome1", AV54Servico_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 )
            {
               AV76Servico_IsPublico1 = BooleanUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76Servico_IsPublico1", AV76Servico_IsPublico1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV87Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV57Servico_TipoHierarquia) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "SERVICO_TIPOHIERARQUIA";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFServico_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV62TFServico_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFServico_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV63TFServico_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFServico_Sigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICO_SIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV66TFServico_Sigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFServico_Sigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICO_SIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV67TFServico_Sigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV70TFServico_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICO_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV70TFServico_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ServicoGrupo_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SERVICOGRUPO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV87Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S222( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Servico_Nome1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV54Servico_Nome1;
         }
         else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 ) && ! (false==AV76Servico_IsPublico1) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV76Servico_IsPublico1);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV87Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Servico";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ServicoGrupo_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E236U2( )
      {
         /* Delete_Click Routine */
         /* Using cursor H006U7 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A829UsuarioServicos_ServicoCod = H006U7_A829UsuarioServicos_ServicoCod[0];
            AV58Found = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( AV58Found )
         {
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         else
         {
            context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV7ServicoGrupo_Codigo);
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E126U2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            new prc_dltservico(context ).execute( ref  A155Servico_Codigo) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_6U2( true) ;
         }
         else
         {
            wb_table2_8_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_44_6U2( true) ;
         }
         else
         {
            wb_table3_44_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table3_44_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_62_6U2( true) ;
         }
         else
         {
            wb_table4_62_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table4_62_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6U2e( true) ;
         }
         else
         {
            wb_table1_2_6U2e( false) ;
         }
      }

      protected void wb_table4_62_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsrtable_Internalname, tblUsrtable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_62_6U2e( true) ;
         }
         else
         {
            wb_table4_62_6U2e( false) ;
         }
      }

      protected void wb_table3_44_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"47\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Grupo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarusuarios_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A605Servico_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Sigla_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Sigla_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A632Servico_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV56AssociarUsuarios));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarusuarios_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarusuarios_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarusuarios_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 47 )
         {
            wbEnd = 0;
            nRC_GXsfl_47 = (short)(nGXsfl_47_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_44_6U2e( true) ;
         }
         else
         {
            wb_table3_44_6U2e( false) ;
         }
      }

      protected void wb_table2_8_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table5_11_6U2( true) ;
         }
         else
         {
            wb_table5_11_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ServicoGrupoServicoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_21_6U2( true) ;
         }
         else
         {
            wb_table6_21_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_6U2e( true) ;
         }
         else
         {
            wb_table2_8_6U2e( false) ;
         }
      }

      protected void wb_table6_21_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextservico_tipohierarquia_Internalname, "de Hierarquia", "", "", lblFiltertextservico_tipohierarquia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_tipohierarquia, cmbavServico_tipohierarquia_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0)), 1, cmbavServico_tipohierarquia_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_ServicoGrupoServicoWC.htm");
            cmbavServico_tipohierarquia.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57Servico_TipoHierarquia), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_tipohierarquia_Internalname, "Values", (String)(cmbavServico_tipohierarquia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table7_30_6U2( true) ;
         }
         else
         {
            wb_table7_30_6U2( false) ;
         }
         return  ;
      }

      protected void wb_table7_30_6U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_6U2e( true) ;
         }
         else
         {
            wb_table6_21_6U2e( false) ;
         }
      }

      protected void wb_table7_30_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_ServicoGrupoServicoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome1_Internalname, StringUtil.RTrim( AV54Servico_Nome1), StringUtil.RTrim( context.localUtil.Format( AV54Servico_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoGrupoServicoWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_ispublico1, cmbavServico_ispublico1_Internalname, StringUtil.BoolToStr( AV76Servico_IsPublico1), 1, cmbavServico_ispublico1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", cmbavServico_ispublico1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_ServicoGrupoServicoWC.htm");
            cmbavServico_ispublico1.CurrentValue = StringUtil.BoolToStr( AV76Servico_IsPublico1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavServico_ispublico1_Internalname, "Values", (String)(cmbavServico_ispublico1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_6U2e( true) ;
         }
         else
         {
            wb_table7_30_6U2e( false) ;
         }
      }

      protected void wb_table5_11_6U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoGrupoServicoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_6U2e( true) ;
         }
         else
         {
            wb_table5_11_6U2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ServicoGrupo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6U2( ) ;
         WS6U2( ) ;
         WE6U2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ServicoGrupo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA6U2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicogruposervicowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA6U2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ServicoGrupo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0)));
         }
         wcpOAV7ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoGrupo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ServicoGrupo_Codigo != wcpOAV7ServicoGrupo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ServicoGrupo_Codigo = AV7ServicoGrupo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ServicoGrupo_Codigo = cgiGet( sPrefix+"AV7ServicoGrupo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ServicoGrupo_Codigo) > 0 )
         {
            AV7ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ServicoGrupo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoGrupo_Codigo), 6, 0)));
         }
         else
         {
            AV7ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ServicoGrupo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA6U2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS6U2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS6U2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoGrupo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoGrupo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ServicoGrupo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoGrupo_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ServicoGrupo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE6U2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216153476");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicogruposervicowc.js", "?20206216153477");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_472( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_47_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_47_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_47_idx;
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO_"+sGXsfl_47_idx;
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO_"+sGXsfl_47_idx;
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME_"+sGXsfl_47_idx;
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA_"+sGXsfl_47_idx;
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO_"+sGXsfl_47_idx;
         edtavAssociarusuarios_Internalname = sPrefix+"vASSOCIARUSUARIOS_"+sGXsfl_47_idx;
      }

      protected void SubsflControlProps_fel_472( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_47_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_47_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_47_fel_idx;
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO_"+sGXsfl_47_fel_idx;
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO_"+sGXsfl_47_fel_idx;
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME_"+sGXsfl_47_fel_idx;
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA_"+sGXsfl_47_fel_idx;
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO_"+sGXsfl_47_fel_idx;
         edtavAssociarusuarios_Internalname = sPrefix+"vASSOCIARUSUARIOS_"+sGXsfl_47_fel_idx;
      }

      protected void sendrow_472( )
      {
         SubsflControlProps_472( ) ;
         WB6U0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_47_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_47_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_47_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV80Update_GXI : context.PathToRelativeUrl( AV30Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 49,'"+sPrefix+"',false,'',47)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV81Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV81Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDelete_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDELETE.CLICK."+sGXsfl_47_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV82Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Display)) ? AV82Display_GXI : context.PathToRelativeUrl( AV32Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoGrupo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtServico_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Sigla_Internalname,StringUtil.RTrim( A605Servico_Sigla),StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtServico_Sigla_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_47_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_ATIVO_" + sGXsfl_47_idx;
               cmbServico_Ativo.Name = GXCCtl;
               cmbServico_Ativo.WebTags = "";
               cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbServico_Ativo.ItemCount > 0 )
               {
                  A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_Ativo,(String)cmbServico_Ativo_Internalname,StringUtil.BoolToStr( A632Servico_Ativo),(short)1,(String)cmbServico_Ativo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_Ativo.CurrentValue = StringUtil.BoolToStr( A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Ativo_Internalname, "Values", (String)(cmbServico_Ativo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarusuarios_Enabled!=0)&&(edtavAssociarusuarios_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 56,'"+sPrefix+"',false,'',47)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV56AssociarUsuarios_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV56AssociarUsuarios))&&String.IsNullOrEmpty(StringUtil.RTrim( AV83Associarusuarios_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV56AssociarUsuarios)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarusuarios_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV56AssociarUsuarios)) ? AV83Associarusuarios_GXI : context.PathToRelativeUrl( AV56AssociarUsuarios)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavAssociarusuarios_Enabled,(String)"",(String)edtavAssociarusuarios_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarusuarios_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e246u2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV56AssociarUsuarios_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOGRUPO_CODIGO"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sPrefix+sGXsfl_47_idx, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_NOME"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sPrefix+sGXsfl_47_idx, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_SIGLA"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sPrefix+sGXsfl_47_idx, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATIVO"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sPrefix+sGXsfl_47_idx, A632Servico_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_47_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_47_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_47_idx+1));
            sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
            SubsflControlProps_472( ) ;
         }
         /* End function sendrow_472 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextservico_tipohierarquia_Internalname = sPrefix+"FILTERTEXTSERVICO_TIPOHIERARQUIA";
         cmbavServico_tipohierarquia_Internalname = sPrefix+"vSERVICO_TIPOHIERARQUIA";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavServico_nome1_Internalname = sPrefix+"vSERVICO_NOME1";
         cmbavServico_ispublico1_Internalname = sPrefix+"vSERVICO_ISPUBLICO1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO";
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME";
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA";
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO";
         edtavAssociarusuarios_Internalname = sPrefix+"vASSOCIARUSUARIOS";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         tblUsrtable_Internalname = sPrefix+"USRTABLE";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavTfservico_nome_Internalname = sPrefix+"vTFSERVICO_NOME";
         edtavTfservico_nome_sel_Internalname = sPrefix+"vTFSERVICO_NOME_SEL";
         edtavTfservico_sigla_Internalname = sPrefix+"vTFSERVICO_SIGLA";
         edtavTfservico_sigla_sel_Internalname = sPrefix+"vTFSERVICO_SIGLA_SEL";
         edtavTfservico_ativo_sel_Internalname = sPrefix+"vTFSERVICO_ATIVO_SEL";
         Ddo_servico_nome_Internalname = sPrefix+"DDO_SERVICO_NOME";
         edtavDdo_servico_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_servico_sigla_Internalname = sPrefix+"DDO_SERVICO_SIGLA";
         edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_servico_ativo_Internalname = sPrefix+"DDO_SERVICO_ATIVO";
         edtavDdo_servico_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavAssociarusuarios_Jsonclick = "";
         edtavAssociarusuarios_Visible = -1;
         cmbServico_Ativo_Jsonclick = "";
         edtServico_Sigla_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         cmbavServico_ispublico1_Jsonclick = "";
         edtavServico_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         cmbavServico_tipohierarquia_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAssociarusuarios_Tooltiptext = "Clique aqui p/ associar os usu�rios deste servi�o!";
         edtavAssociarusuarios_Enabled = 1;
         edtServico_Sigla_Forecolor = (int)(0xFFFFFF);
         edtServico_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         cmbServico_Ativo_Titleformat = 0;
         edtServico_Sigla_Titleformat = 0;
         edtServico_Nome_Titleformat = 0;
         edtavDelete_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavServico_ispublico1.Visible = 1;
         edtavServico_nome1_Visible = 1;
         edtavAssociarusuarios_Title = "";
         cmbServico_Ativo.Title.Text = "Ativo?";
         edtServico_Sigla_Title = "Sigla";
         edtServico_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_servico_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfservico_ativo_sel_Jsonclick = "";
         edtavTfservico_ativo_sel_Visible = 1;
         edtavTfservico_sigla_sel_Jsonclick = "";
         edtavTfservico_sigla_sel_Visible = 1;
         edtavTfservico_sigla_Jsonclick = "";
         edtavTfservico_sigla_Visible = 1;
         edtavTfservico_nome_sel_Jsonclick = "";
         edtavTfservico_nome_sel_Visible = 1;
         edtavTfservico_nome_Jsonclick = "";
         edtavTfservico_nome_Visible = 1;
         Ddo_servico_ativo_Searchbuttontext = "Pesquisar";
         Ddo_servico_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_servico_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_servico_ativo_Datalisttype = "FixedValues";
         Ddo_servico_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_servico_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_ativo_Titlecontrolidtoreplace = "";
         Ddo_servico_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_ativo_Cls = "ColumnSettings";
         Ddo_servico_ativo_Tooltip = "Op��es";
         Ddo_servico_ativo_Caption = "";
         Ddo_servico_sigla_Searchbuttontext = "Pesquisar";
         Ddo_servico_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_sigla_Loadingdata = "Carregando dados...";
         Ddo_servico_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_servico_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_servico_sigla_Datalistproc = "GetServicoGrupoServicoWCFilterData";
         Ddo_servico_sigla_Datalisttype = "Dynamic";
         Ddo_servico_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_sigla_Filtertype = "Character";
         Ddo_servico_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Titlecontrolidtoreplace = "";
         Ddo_servico_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_sigla_Cls = "ColumnSettings";
         Ddo_servico_sigla_Tooltip = "Op��es";
         Ddo_servico_sigla_Caption = "";
         Ddo_servico_nome_Searchbuttontext = "Pesquisar";
         Ddo_servico_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_nome_Loadingdata = "Carregando dados...";
         Ddo_servico_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servico_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servico_nome_Datalistproc = "GetServicoGrupoServicoWCFilterData";
         Ddo_servico_nome_Datalisttype = "Dynamic";
         Ddo_servico_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_nome_Filtertype = "Character";
         Ddo_servico_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Titlecontrolidtoreplace = "";
         Ddo_servico_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_nome_Cls = "ColumnSettings";
         Ddo_servico_nome_Tooltip = "Op��es";
         Ddo_servico_nome_Caption = "";
         Confirmpanel_Confirmtype = "1";
         Confirmpanel_Buttoncanceltext = "Cancelar";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Sim";
         Confirmpanel_Confirmtext = "Este servi�o esta vinculado a alguns usu�rios. Confirma elimina-lo?";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Closeable = Convert.ToBoolean( -1);
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false}],oparms:[{av:'AV61Servico_NomeTitleFilterData',fld:'vSERVICO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV65Servico_SiglaTitleFilterData',fld:'vSERVICO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV69Servico_AtivoTitleFilterData',fld:'vSERVICO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'cmbServico_Ativo'},{av:'AV74GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV75GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavAssociarusuarios_Title',ctrl:'vASSOCIARUSUARIOS',prop:'Title'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICO_NOME.ONOPTIONCLICKED","{handler:'E136U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_servico_nome_Activeeventkey',ctrl:'DDO_SERVICO_NOME',prop:'ActiveEventKey'},{av:'Ddo_servico_nome_Filteredtext_get',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_get'},{av:'Ddo_servico_nome_Selectedvalue_get',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'Ddo_servico_ativo_Sortedstatus',ctrl:'DDO_SERVICO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_SIGLA.ONOPTIONCLICKED","{handler:'E146U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_servico_sigla_Activeeventkey',ctrl:'DDO_SERVICO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_servico_sigla_Filteredtext_get',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_servico_sigla_Selectedvalue_get',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servico_ativo_Sortedstatus',ctrl:'DDO_SERVICO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_ATIVO.ONOPTIONCLICKED","{handler:'E156U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_servico_ativo_Activeeventkey',ctrl:'DDO_SERVICO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_servico_ativo_Selectedvalue_get',ctrl:'DDO_SERVICO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_ativo_Sortedstatus',ctrl:'DDO_SERVICO_ATIVO',prop:'SortedStatus'},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E226U2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV30Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'AV32Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV56AssociarUsuarios',fld:'vASSOCIARUSUARIOS',pic:'',nv:''},{av:'edtavAssociarusuarios_Tooltiptext',ctrl:'vASSOCIARUSUARIOS',prop:'Tooltiptext'},{av:'edtServico_Nome_Forecolor',ctrl:'SERVICO_NOME',prop:'Forecolor'},{av:'edtServico_Sigla_Forecolor',ctrl:'SERVICO_SIGLA',prop:'Forecolor'},{av:'edtavAssociarusuarios_Enabled',ctrl:'vASSOCIARUSUARIOS',prop:'Enabled'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E166U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E196U2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavServico_ispublico1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E176U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'Ddo_servico_nome_Filteredtext_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Selectedvalue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Filteredtext_set',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_set'},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Selectedvalue_set',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_set'},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_servico_ativo_Selectedvalue_set',ctrl:'DDO_SERVICO_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavServico_ispublico1'},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false}]}");
         setEventMetadata("'DOASSOCIARUSUARIOS'","{handler:'E246U2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E186U2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDELETE.CLICK","{handler:'E236U2',iparms:[{av:'A829UsuarioServicos_ServicoCod',fld:'USUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E126U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'}],oparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_servico_nome_Activeeventkey = "";
         Ddo_servico_nome_Filteredtext_get = "";
         Ddo_servico_nome_Selectedvalue_get = "";
         Ddo_servico_sigla_Activeeventkey = "";
         Ddo_servico_sigla_Filteredtext_get = "";
         Ddo_servico_sigla_Selectedvalue_get = "";
         Ddo_servico_ativo_Activeeventkey = "";
         Ddo_servico_ativo_Selectedvalue_get = "";
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV54Servico_Nome1 = "";
         AV76Servico_IsPublico1 = false;
         AV62TFServico_Nome = "";
         AV63TFServico_Nome_Sel = "";
         AV66TFServico_Sigla = "";
         AV67TFServico_Sigla_Sel = "";
         AV64ddo_Servico_NomeTitleControlIdToReplace = "";
         AV68ddo_Servico_SiglaTitleControlIdToReplace = "";
         AV71ddo_Servico_AtivoTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV87Pgmname = "";
         AV57Servico_TipoHierarquia = 1;
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV72DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV61Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Servico_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Servico_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Confirmpanel_Width = "";
         Ddo_servico_nome_Filteredtext_set = "";
         Ddo_servico_nome_Selectedvalue_set = "";
         Ddo_servico_nome_Sortedstatus = "";
         Ddo_servico_sigla_Filteredtext_set = "";
         Ddo_servico_sigla_Selectedvalue_set = "";
         Ddo_servico_sigla_Sortedstatus = "";
         Ddo_servico_ativo_Selectedvalue_set = "";
         Ddo_servico_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Update = "";
         AV80Update_GXI = "";
         AV29Delete = "";
         AV81Delete_GXI = "";
         AV32Display = "";
         AV82Display_GXI = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         AV56AssociarUsuarios = "";
         AV83Associarusuarios_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV54Servico_Nome1 = "";
         lV62TFServico_Nome = "";
         lV66TFServico_Sigla = "";
         H006U2_A155Servico_Codigo = new int[1] ;
         H006U2_A1635Servico_IsPublico = new bool[] {false} ;
         H006U2_A1530Servico_TipoHierarquia = new short[1] ;
         H006U2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H006U2_A632Servico_Ativo = new bool[] {false} ;
         H006U2_A605Servico_Sigla = new String[] {""} ;
         H006U2_A608Servico_Nome = new String[] {""} ;
         H006U2_A157ServicoGrupo_Codigo = new int[1] ;
         H006U3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H006U4_A160ContratoServicos_Codigo = new int[1] ;
         H006U4_A155Servico_Codigo = new int[1] ;
         H006U5_A456ContagemResultado_Codigo = new int[1] ;
         H006U5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H006U5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H006U5_A601ContagemResultado_Servico = new int[1] ;
         H006U5_n601ContagemResultado_Servico = new bool[] {false} ;
         H006U6_A456ContagemResultado_Codigo = new int[1] ;
         H006U6_A1636ContagemResultado_ServicoSS = new int[1] ;
         H006U6_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H006U7_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H006U7_A829UsuarioServicos_ServicoCod = new int[1] ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblFiltertextservico_tipohierarquia_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ServicoGrupo_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicogruposervicowc__default(),
            new Object[][] {
                new Object[] {
               H006U2_A155Servico_Codigo, H006U2_A1635Servico_IsPublico, H006U2_A1530Servico_TipoHierarquia, H006U2_n1530Servico_TipoHierarquia, H006U2_A632Servico_Ativo, H006U2_A605Servico_Sigla, H006U2_A608Servico_Nome, H006U2_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               H006U3_AGRID_nRecordCount
               }
               , new Object[] {
               H006U4_A160ContratoServicos_Codigo, H006U4_A155Servico_Codigo
               }
               , new Object[] {
               H006U5_A456ContagemResultado_Codigo, H006U5_A1553ContagemResultado_CntSrvCod, H006U5_n1553ContagemResultado_CntSrvCod, H006U5_A601ContagemResultado_Servico, H006U5_n601ContagemResultado_Servico
               }
               , new Object[] {
               H006U6_A456ContagemResultado_Codigo, H006U6_A1636ContagemResultado_ServicoSS, H006U6_n1636ContagemResultado_ServicoSS
               }
               , new Object[] {
               H006U7_A828UsuarioServicos_UsuarioCod, H006U7_A829UsuarioServicos_ServicoCod
               }
            }
         );
         AV87Pgmname = "ServicoGrupoServicoWC";
         /* GeneXus formulas. */
         AV87Pgmname = "ServicoGrupoServicoWC";
         context.Gx_err = 0;
      }

      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_47 ;
      private short nGXsfl_47_idx=1 ;
      private short AV14OrderedBy ;
      private short AV70TFServico_Ativo_Sel ;
      private short AV57Servico_TipoHierarquia ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_47_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short A1530Servico_TipoHierarquia ;
      private short edtServico_Nome_Titleformat ;
      private short edtServico_Sigla_Titleformat ;
      private short cmbServico_Ativo_Titleformat ;
      private short AV84GXLvl227 ;
      private short AV85GXLvl231 ;
      private short AV86GXLvl236 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ServicoGrupo_Codigo ;
      private int wcpOAV7ServicoGrupo_Codigo ;
      private int subGrid_Rows ;
      private int A155Servico_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A829UsuarioServicos_ServicoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servico_nome_Datalistupdateminimumcharacters ;
      private int Ddo_servico_sigla_Datalistupdateminimumcharacters ;
      private int edtavTfservico_nome_Visible ;
      private int edtavTfservico_nome_sel_Visible ;
      private int edtavTfservico_sigla_Visible ;
      private int edtavTfservico_sigla_sel_Visible ;
      private int edtavTfservico_ativo_sel_Visible ;
      private int edtavDdo_servico_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_ativotitlecontrolidtoreplace_Visible ;
      private int A157ServicoGrupo_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV73PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtServico_Nome_Forecolor ;
      private int edtServico_Sigla_Forecolor ;
      private int edtavAssociarusuarios_Enabled ;
      private int edtavDelete_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int imgInsert_Enabled ;
      private int edtavServico_nome1_Visible ;
      private int AV88GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAssociarusuarios_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV74GridCurrentPage ;
      private long AV75GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV77Color ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servico_nome_Activeeventkey ;
      private String Ddo_servico_nome_Filteredtext_get ;
      private String Ddo_servico_nome_Selectedvalue_get ;
      private String Ddo_servico_sigla_Activeeventkey ;
      private String Ddo_servico_sigla_Filteredtext_get ;
      private String Ddo_servico_sigla_Selectedvalue_get ;
      private String Ddo_servico_ativo_Activeeventkey ;
      private String Ddo_servico_ativo_Selectedvalue_get ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_47_idx="0001" ;
      private String AV54Servico_Nome1 ;
      private String AV62TFServico_Nome ;
      private String AV63TFServico_Nome_Sel ;
      private String AV66TFServico_Sigla ;
      private String AV67TFServico_Sigla_Sel ;
      private String AV87Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String Ddo_servico_nome_Caption ;
      private String Ddo_servico_nome_Tooltip ;
      private String Ddo_servico_nome_Cls ;
      private String Ddo_servico_nome_Filteredtext_set ;
      private String Ddo_servico_nome_Selectedvalue_set ;
      private String Ddo_servico_nome_Dropdownoptionstype ;
      private String Ddo_servico_nome_Titlecontrolidtoreplace ;
      private String Ddo_servico_nome_Sortedstatus ;
      private String Ddo_servico_nome_Filtertype ;
      private String Ddo_servico_nome_Datalisttype ;
      private String Ddo_servico_nome_Datalistproc ;
      private String Ddo_servico_nome_Sortasc ;
      private String Ddo_servico_nome_Sortdsc ;
      private String Ddo_servico_nome_Loadingdata ;
      private String Ddo_servico_nome_Cleanfilter ;
      private String Ddo_servico_nome_Noresultsfound ;
      private String Ddo_servico_nome_Searchbuttontext ;
      private String Ddo_servico_sigla_Caption ;
      private String Ddo_servico_sigla_Tooltip ;
      private String Ddo_servico_sigla_Cls ;
      private String Ddo_servico_sigla_Filteredtext_set ;
      private String Ddo_servico_sigla_Selectedvalue_set ;
      private String Ddo_servico_sigla_Dropdownoptionstype ;
      private String Ddo_servico_sigla_Titlecontrolidtoreplace ;
      private String Ddo_servico_sigla_Sortedstatus ;
      private String Ddo_servico_sigla_Filtertype ;
      private String Ddo_servico_sigla_Datalisttype ;
      private String Ddo_servico_sigla_Datalistproc ;
      private String Ddo_servico_sigla_Sortasc ;
      private String Ddo_servico_sigla_Sortdsc ;
      private String Ddo_servico_sigla_Loadingdata ;
      private String Ddo_servico_sigla_Cleanfilter ;
      private String Ddo_servico_sigla_Noresultsfound ;
      private String Ddo_servico_sigla_Searchbuttontext ;
      private String Ddo_servico_ativo_Caption ;
      private String Ddo_servico_ativo_Tooltip ;
      private String Ddo_servico_ativo_Cls ;
      private String Ddo_servico_ativo_Selectedvalue_set ;
      private String Ddo_servico_ativo_Dropdownoptionstype ;
      private String Ddo_servico_ativo_Titlecontrolidtoreplace ;
      private String Ddo_servico_ativo_Sortedstatus ;
      private String Ddo_servico_ativo_Datalisttype ;
      private String Ddo_servico_ativo_Datalistfixedvalues ;
      private String Ddo_servico_ativo_Sortasc ;
      private String Ddo_servico_ativo_Sortdsc ;
      private String Ddo_servico_ativo_Cleanfilter ;
      private String Ddo_servico_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavTfservico_nome_Internalname ;
      private String edtavTfservico_nome_Jsonclick ;
      private String edtavTfservico_nome_sel_Internalname ;
      private String edtavTfservico_nome_sel_Jsonclick ;
      private String edtavTfservico_sigla_Internalname ;
      private String edtavTfservico_sigla_Jsonclick ;
      private String edtavTfservico_sigla_sel_Internalname ;
      private String edtavTfservico_sigla_sel_Jsonclick ;
      private String edtavTfservico_ativo_sel_Internalname ;
      private String edtavTfservico_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_servico_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Internalname ;
      private String cmbServico_Ativo_Internalname ;
      private String edtavAssociarusuarios_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV54Servico_Nome1 ;
      private String lV62TFServico_Nome ;
      private String lV66TFServico_Sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavServico_tipohierarquia_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavServico_nome1_Internalname ;
      private String cmbavServico_ispublico1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servico_nome_Internalname ;
      private String Ddo_servico_sigla_Internalname ;
      private String Ddo_servico_ativo_Internalname ;
      private String edtServico_Nome_Title ;
      private String edtServico_Sigla_Title ;
      private String edtavAssociarusuarios_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavAssociarusuarios_Tooltiptext ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUsrtable_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextservico_tipohierarquia_Internalname ;
      private String lblFiltertextservico_tipohierarquia_Jsonclick ;
      private String cmbavServico_tipohierarquia_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavServico_nome1_Jsonclick ;
      private String cmbavServico_ispublico1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ServicoGrupo_Codigo ;
      private String sGXsfl_47_fel_idx="0001" ;
      private String edtavDelete_Jsonclick ;
      private String ROClassString ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String edtServico_Sigla_Jsonclick ;
      private String cmbServico_Ativo_Jsonclick ;
      private String edtavAssociarusuarios_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Confirmpanel_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV76Servico_IsPublico1 ;
      private bool A632Servico_Ativo ;
      private bool n601ContagemResultado_Servico ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Confirmpanel_Closeable ;
      private bool Ddo_servico_nome_Includesortasc ;
      private bool Ddo_servico_nome_Includesortdsc ;
      private bool Ddo_servico_nome_Includefilter ;
      private bool Ddo_servico_nome_Filterisrange ;
      private bool Ddo_servico_nome_Includedatalist ;
      private bool Ddo_servico_sigla_Includesortasc ;
      private bool Ddo_servico_sigla_Includesortdsc ;
      private bool Ddo_servico_sigla_Includefilter ;
      private bool Ddo_servico_sigla_Filterisrange ;
      private bool Ddo_servico_sigla_Includedatalist ;
      private bool Ddo_servico_ativo_Includesortasc ;
      private bool Ddo_servico_ativo_Includesortdsc ;
      private bool Ddo_servico_ativo_Includefilter ;
      private bool Ddo_servico_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1635Servico_IsPublico ;
      private bool n1530Servico_TipoHierarquia ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool AV58Found ;
      private bool AV30Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV32Display_IsBlob ;
      private bool AV56AssociarUsuarios_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV64ddo_Servico_NomeTitleControlIdToReplace ;
      private String AV68ddo_Servico_SiglaTitleControlIdToReplace ;
      private String AV71ddo_Servico_AtivoTitleControlIdToReplace ;
      private String AV80Update_GXI ;
      private String AV81Delete_GXI ;
      private String AV82Display_GXI ;
      private String AV83Associarusuarios_GXI ;
      private String AV30Update ;
      private String AV29Delete ;
      private String AV32Display ;
      private String AV56AssociarUsuarios ;
      private String imgInsert_Bitmap ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavServico_tipohierarquia ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavServico_ispublico1 ;
      private GXCombobox cmbServico_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H006U2_A155Servico_Codigo ;
      private bool[] H006U2_A1635Servico_IsPublico ;
      private short[] H006U2_A1530Servico_TipoHierarquia ;
      private bool[] H006U2_n1530Servico_TipoHierarquia ;
      private bool[] H006U2_A632Servico_Ativo ;
      private String[] H006U2_A605Servico_Sigla ;
      private String[] H006U2_A608Servico_Nome ;
      private int[] H006U2_A157ServicoGrupo_Codigo ;
      private long[] H006U3_AGRID_nRecordCount ;
      private int[] H006U4_A160ContratoServicos_Codigo ;
      private int[] H006U4_A155Servico_Codigo ;
      private int[] H006U5_A456ContagemResultado_Codigo ;
      private int[] H006U5_A1553ContagemResultado_CntSrvCod ;
      private bool[] H006U5_n1553ContagemResultado_CntSrvCod ;
      private int[] H006U5_A601ContagemResultado_Servico ;
      private bool[] H006U5_n601ContagemResultado_Servico ;
      private int[] H006U6_A456ContagemResultado_Codigo ;
      private int[] H006U6_A1636ContagemResultado_ServicoSS ;
      private bool[] H006U6_n1636ContagemResultado_ServicoSS ;
      private int[] H006U7_A828UsuarioServicos_UsuarioCod ;
      private int[] H006U7_A829UsuarioServicos_ServicoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61Servico_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65Servico_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Servico_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV72DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class servicogruposervicowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006U2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV54Servico_Nome1 ,
                                             bool AV76Servico_IsPublico1 ,
                                             String AV63TFServico_Nome_Sel ,
                                             String AV62TFServico_Nome ,
                                             String AV67TFServico_Sigla_Sel ,
                                             String AV66TFServico_Sigla ,
                                             short AV70TFServico_Ativo_Sel ,
                                             String A608Servico_Nome ,
                                             bool A1635Servico_IsPublico ,
                                             String A605Servico_Sigla ,
                                             bool A632Servico_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A157ServicoGrupo_Codigo ,
                                             int AV7ServicoGrupo_Codigo ,
                                             short A1530Servico_TipoHierarquia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Servico_Codigo], [Servico_IsPublico], [Servico_TipoHierarquia], [Servico_Ativo], [Servico_Sigla], [Servico_Nome], [ServicoGrupo_Codigo]";
         sFromString = " FROM [Servico] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([ServicoGrupo_Codigo] = @AV7ServicoGrupo_Codigo)";
         sWhereString = sWhereString + " and ([Servico_TipoHierarquia] = 1)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like '%' + @lV54Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 ) && ( ! (false==AV76Servico_IsPublico1) ) )
         {
            sWhereString = sWhereString + " and ([Servico_IsPublico] = @AV76Servico_IsPublico1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like @lV62TFServico_Nome)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] = @AV63TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] like @lV66TFServico_Sigla)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] = @AV67TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV70TFServico_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 1)";
         }
         if ( AV70TFServico_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 0)";
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo], [Servico_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo] DESC, [Servico_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo], [Servico_Sigla]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo] DESC, [Servico_Sigla] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo], [Servico_Ativo]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoGrupo_Codigo] DESC, [Servico_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Servico_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006U3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV54Servico_Nome1 ,
                                             bool AV76Servico_IsPublico1 ,
                                             String AV63TFServico_Nome_Sel ,
                                             String AV62TFServico_Nome ,
                                             String AV67TFServico_Sigla_Sel ,
                                             String AV66TFServico_Sigla ,
                                             short AV70TFServico_Ativo_Sel ,
                                             String A608Servico_Nome ,
                                             bool A1635Servico_IsPublico ,
                                             String A605Servico_Sigla ,
                                             bool A632Servico_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A157ServicoGrupo_Codigo ,
                                             int AV7ServicoGrupo_Codigo ,
                                             short A1530Servico_TipoHierarquia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Servico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoGrupo_Codigo] = @AV7ServicoGrupo_Codigo)";
         scmdbuf = scmdbuf + " and ([Servico_TipoHierarquia] = 1)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like '%' + @lV54Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 ) && ( ! (false==AV76Servico_IsPublico1) ) )
         {
            sWhereString = sWhereString + " and ([Servico_IsPublico] = @AV76Servico_IsPublico1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like @lV62TFServico_Nome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] = @AV63TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] like @lV66TFServico_Sigla)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] = @AV67TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV70TFServico_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 1)";
         }
         if ( AV70TFServico_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006U2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] );
               case 1 :
                     return conditional_H006U3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006U4 ;
          prmH006U4 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006U5 ;
          prmH006U5 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006U6 ;
          prmH006U6 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006U7 ;
          prmH006U7 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006U2 ;
          prmH006U2 = new Object[] {
          new Object[] {"@AV7ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76Servico_IsPublico1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV62TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV67TFServico_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006U3 ;
          prmH006U3 = new Object[] {
          new Object[] {"@AV7ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76Servico_IsPublico1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV62TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV67TFServico_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006U2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006U2,11,0,true,false )
             ,new CursorDef("H006U3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006U3,1,0,true,false )
             ,new CursorDef("H006U4", "SELECT TOP 1 [ContratoServicos_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006U4,1,0,false,true )
             ,new CursorDef("H006U5", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T2.[Servico_Codigo] = @Servico_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006U5,1,0,false,true )
             ,new CursorDef("H006U6", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_ServicoSS] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ServicoSS] = @Servico_Codigo ORDER BY [ContagemResultado_ServicoSS] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006U6,1,0,false,true )
             ,new CursorDef("H006U7", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_ServicoCod] = @Servico_Codigo ORDER BY [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006U7,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
