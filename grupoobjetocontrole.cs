/*
               File: GrupoObjetoControle
        Description: Grupos de Objetos de Controle
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:3.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class grupoobjetocontrole : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action4") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_4_4K201( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vRESPONSAVEL_CODIGO") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvRESPONSAVEL_CODIGO4K201( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel1"+"_"+"GPOOBJCTRL_RESPONSAVEL") == 0 )
         {
            A1826GpoObjCtrl_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX1ASAGPOOBJCTRL_RESPONSAVEL4K201( A1826GpoObjCtrl_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7GpoObjCtrl_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GpoObjCtrl_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGPOOBJCTRL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GpoObjCtrl_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynavResponsavel_codigo.Name = "vRESPONSAVEL_CODIGO";
         dynavResponsavel_codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Grupos de Objetos de Controle", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public grupoobjetocontrole( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public grupoobjetocontrole( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_GpoObjCtrl_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7GpoObjCtrl_Codigo = aP1_GpoObjCtrl_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavResponsavel_codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavResponsavel_codigo.ItemCount > 0 )
         {
            AV13Responsavel_Codigo = (int)(NumberUtil.Val( dynavResponsavel_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Responsavel_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Responsavel_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4K201( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4K201e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4K201( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4K201( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4K201e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_4K201( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_4K201e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4K201e( true) ;
         }
         else
         {
            wb_table1_2_4K201e( false) ;
         }
      }

      protected void wb_table3_26_4K201( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GrupoObjetoControle.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GrupoObjetoControle.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_GrupoObjetoControle.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_4K201e( true) ;
         }
         else
         {
            wb_table3_26_4K201e( false) ;
         }
      }

      protected void wb_table2_5_4K201( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4K201( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4K201e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4K201e( true) ;
         }
         else
         {
            wb_table2_5_4K201e( false) ;
         }
      }

      protected void wb_table4_13_4K201( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgpoobjctrl_nome_Internalname, "Nome", "", "", lblTextblockgpoobjctrl_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GrupoObjetoControle.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGpoObjCtrl_Nome_Internalname, StringUtil.RTrim( A1827GpoObjCtrl_Nome), StringUtil.RTrim( context.localUtil.Format( A1827GpoObjCtrl_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGpoObjCtrl_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGpoObjCtrl_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_GrupoObjetoControle.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockresponsavel_codigo_Internalname, "Respons�vel", "", "", lblTextblockresponsavel_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GrupoObjetoControle.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavResponsavel_codigo, dynavResponsavel_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13Responsavel_Codigo), 6, 0)), 1, dynavResponsavel_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavResponsavel_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_GrupoObjetoControle.htm");
            dynavResponsavel_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Responsavel_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Values", (String)(dynavResponsavel_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4K201e( true) ;
         }
         else
         {
            wb_table4_13_4K201e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114K2 */
         E114K2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1827GpoObjCtrl_Nome = StringUtil.Upper( cgiGet( edtGpoObjCtrl_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1827GpoObjCtrl_Nome", A1827GpoObjCtrl_Nome);
               dynavResponsavel_codigo.CurrentValue = cgiGet( dynavResponsavel_codigo_Internalname);
               AV13Responsavel_Codigo = (int)(NumberUtil.Val( cgiGet( dynavResponsavel_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Responsavel_Codigo), 6, 0)));
               /* Read saved values. */
               Z1826GpoObjCtrl_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1826GpoObjCtrl_Codigo"), ",", "."));
               Z1827GpoObjCtrl_Nome = cgiGet( "Z1827GpoObjCtrl_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A1826GpoObjCtrl_Codigo = (int)(context.localUtil.CToN( cgiGet( "GPOOBJCTRL_CODIGO"), ",", "."));
               A1828GpoObjCtrl_Responsavel = (int)(context.localUtil.CToN( cgiGet( "GPOOBJCTRL_RESPONSAVEL"), ",", "."));
               AV7GpoObjCtrl_Codigo = (int)(context.localUtil.CToN( cgiGet( "vGPOOBJCTRL_CODIGO"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               AV14Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATANTE_CODIGO"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "GrupoObjetoControle";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1826GpoObjCtrl_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("grupoobjetocontrole:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("grupoobjetocontrole:[SecurityCheckFailed value for]"+"GpoObjCtrl_Codigo:"+context.localUtil.Format( (decimal)(A1826GpoObjCtrl_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1826GpoObjCtrl_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode201 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode201;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound201 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4K0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114K2 */
                           E114K2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124K2 */
                           E124K2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124K2 */
            E124K2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4K201( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4K201( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4K0( )
      {
         BeforeValidate4K201( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4K201( ) ;
            }
            else
            {
               CheckExtendedTable4K201( ) ;
               CloseExtendedTableCursors4K201( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4K0( )
      {
      }

      protected void E114K2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         GXt_int1 = AV13Responsavel_Codigo;
         GXt_int2 = (short)(AV7GpoObjCtrl_Codigo);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int2, out  GXt_int1) ;
         AV7GpoObjCtrl_Codigo = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GpoObjCtrl_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGPOOBJCTRL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GpoObjCtrl_Codigo), "ZZZZZ9")));
         AV13Responsavel_Codigo = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Responsavel_Codigo), 6, 0)));
         if ( (0==AV8WWPContext.gxTpr_Contratante_codigo) )
         {
            GXt_int1 = AV14Contratante_Codigo;
            new prc_retornacodigocontratanteareaatrabalho(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_int1) ;
            AV14Contratante_Codigo = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Contratante_Codigo), 6, 0)));
         }
         else
         {
            AV14Contratante_Codigo = AV8WWPContext.gxTpr_Contratante_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Contratante_Codigo), 6, 0)));
         }
      }

      protected void E124K2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwgrupoobjetocontrole.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4K201( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1827GpoObjCtrl_Nome = T004K3_A1827GpoObjCtrl_Nome[0];
            }
            else
            {
               Z1827GpoObjCtrl_Nome = A1827GpoObjCtrl_Nome;
            }
         }
         if ( GX_JID == -5 )
         {
            Z1826GpoObjCtrl_Codigo = A1826GpoObjCtrl_Codigo;
            Z1827GpoObjCtrl_Nome = A1827GpoObjCtrl_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7GpoObjCtrl_Codigo) )
         {
            A1826GpoObjCtrl_Codigo = AV7GpoObjCtrl_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
         }
         GXt_int1 = A1828GpoObjCtrl_Responsavel;
         GXt_int2 = (short)(A1826GpoObjCtrl_Codigo);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int2, out  GXt_int1) ;
         A1826GpoObjCtrl_Codigo = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
         A1828GpoObjCtrl_Responsavel = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1828GpoObjCtrl_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1828GpoObjCtrl_Responsavel), 6, 0)));
         GXVvRESPONSAVEL_CODIGO_html4K201( AV8WWPContext) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load4K201( )
      {
         /* Using cursor T004K4 */
         pr_default.execute(2, new Object[] {A1826GpoObjCtrl_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound201 = 1;
            A1827GpoObjCtrl_Nome = T004K4_A1827GpoObjCtrl_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1827GpoObjCtrl_Nome", A1827GpoObjCtrl_Nome);
            ZM4K201( -5) ;
         }
         pr_default.close(2);
         OnLoadActions4K201( ) ;
      }

      protected void OnLoadActions4K201( )
      {
      }

      protected void CheckExtendedTable4K201( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors4K201( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4K201( )
      {
         /* Using cursor T004K5 */
         pr_default.execute(3, new Object[] {A1826GpoObjCtrl_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound201 = 1;
         }
         else
         {
            RcdFound201 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004K3 */
         pr_default.execute(1, new Object[] {A1826GpoObjCtrl_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4K201( 5) ;
            RcdFound201 = 1;
            A1826GpoObjCtrl_Codigo = T004K3_A1826GpoObjCtrl_Codigo[0];
            A1827GpoObjCtrl_Nome = T004K3_A1827GpoObjCtrl_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1827GpoObjCtrl_Nome", A1827GpoObjCtrl_Nome);
            Z1826GpoObjCtrl_Codigo = A1826GpoObjCtrl_Codigo;
            sMode201 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4K201( ) ;
            if ( AnyError == 1 )
            {
               RcdFound201 = 0;
               InitializeNonKey4K201( ) ;
            }
            Gx_mode = sMode201;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound201 = 0;
            InitializeNonKey4K201( ) ;
            sMode201 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode201;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4K201( ) ;
         if ( RcdFound201 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound201 = 0;
         /* Using cursor T004K6 */
         pr_default.execute(4, new Object[] {A1826GpoObjCtrl_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T004K6_A1826GpoObjCtrl_Codigo[0] < A1826GpoObjCtrl_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T004K6_A1826GpoObjCtrl_Codigo[0] > A1826GpoObjCtrl_Codigo ) ) )
            {
               A1826GpoObjCtrl_Codigo = T004K6_A1826GpoObjCtrl_Codigo[0];
               RcdFound201 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound201 = 0;
         /* Using cursor T004K7 */
         pr_default.execute(5, new Object[] {A1826GpoObjCtrl_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T004K7_A1826GpoObjCtrl_Codigo[0] > A1826GpoObjCtrl_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T004K7_A1826GpoObjCtrl_Codigo[0] < A1826GpoObjCtrl_Codigo ) ) )
            {
               A1826GpoObjCtrl_Codigo = T004K7_A1826GpoObjCtrl_Codigo[0];
               RcdFound201 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4K201( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4K201( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound201 == 1 )
            {
               if ( A1826GpoObjCtrl_Codigo != Z1826GpoObjCtrl_Codigo )
               {
                  A1826GpoObjCtrl_Codigo = Z1826GpoObjCtrl_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4K201( ) ;
                  GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1826GpoObjCtrl_Codigo != Z1826GpoObjCtrl_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4K201( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4K201( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1826GpoObjCtrl_Codigo != Z1826GpoObjCtrl_Codigo )
         {
            A1826GpoObjCtrl_Codigo = Z1826GpoObjCtrl_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGpoObjCtrl_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4K201( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004K2 */
            pr_default.execute(0, new Object[] {A1826GpoObjCtrl_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"GrupoObjetoControle"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1827GpoObjCtrl_Nome, T004K2_A1827GpoObjCtrl_Nome[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1827GpoObjCtrl_Nome, T004K2_A1827GpoObjCtrl_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("grupoobjetocontrole:[seudo value changed for attri]"+"GpoObjCtrl_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1827GpoObjCtrl_Nome);
                  GXUtil.WriteLogRaw("Current: ",T004K2_A1827GpoObjCtrl_Nome[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"GrupoObjetoControle"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4K201( )
      {
         BeforeValidate4K201( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4K201( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4K201( 0) ;
            CheckOptimisticConcurrency4K201( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4K201( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4K201( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004K8 */
                     pr_default.execute(6, new Object[] {A1827GpoObjCtrl_Nome});
                     A1826GpoObjCtrl_Codigo = T004K8_A1826GpoObjCtrl_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("GrupoObjetoControle") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_updgpoobjctrl_responsavel(context ).execute(  ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? "DLT" : "UPD"),  AV14Contratante_Codigo,  AV7GpoObjCtrl_Codigo,  AV13Responsavel_Codigo) ;
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4K0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4K201( ) ;
            }
            EndLevel4K201( ) ;
         }
         CloseExtendedTableCursors4K201( ) ;
      }

      protected void Update4K201( )
      {
         BeforeValidate4K201( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4K201( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4K201( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4K201( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4K201( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004K9 */
                     pr_default.execute(7, new Object[] {A1827GpoObjCtrl_Nome, A1826GpoObjCtrl_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("GrupoObjetoControle") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"GrupoObjetoControle"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4K201( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        new prc_updgpoobjctrl_responsavel(context ).execute(  ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? "DLT" : "UPD"),  AV14Contratante_Codigo,  AV7GpoObjCtrl_Codigo,  AV13Responsavel_Codigo) ;
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4K201( ) ;
         }
         CloseExtendedTableCursors4K201( ) ;
      }

      protected void DeferredUpdate4K201( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4K201( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4K201( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4K201( ) ;
            AfterConfirm4K201( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4K201( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004K10 */
                  pr_default.execute(8, new Object[] {A1826GpoObjCtrl_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("GrupoObjetoControle") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode201 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4K201( ) ;
         Gx_mode = sMode201;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4K201( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004K11 */
            pr_default.execute(9, new Object[] {A1826GpoObjCtrl_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor T004K12 */
            pr_default.execute(10, new Object[] {A1826GpoObjCtrl_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor T004K13 */
            pr_default.execute(11, new Object[] {A1826GpoObjCtrl_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Linhas de Neg�cio"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
         }
      }

      protected void EndLevel4K201( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4K201( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "GrupoObjetoControle");
            if ( AnyError == 0 )
            {
               ConfirmValues4K0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "GrupoObjetoControle");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4K201( )
      {
         /* Scan By routine */
         /* Using cursor T004K14 */
         pr_default.execute(12);
         RcdFound201 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound201 = 1;
            A1826GpoObjCtrl_Codigo = T004K14_A1826GpoObjCtrl_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4K201( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound201 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound201 = 1;
            A1826GpoObjCtrl_Codigo = T004K14_A1826GpoObjCtrl_Codigo[0];
         }
      }

      protected void ScanEnd4K201( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm4K201( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4K201( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4K201( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4K201( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4K201( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4K201( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4K201( )
      {
         edtGpoObjCtrl_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGpoObjCtrl_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGpoObjCtrl_Nome_Enabled), 5, 0)));
         dynavResponsavel_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4K0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529932414");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("grupoobjetocontrole.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7GpoObjCtrl_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1826GpoObjCtrl_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1827GpoObjCtrl_Nome", StringUtil.RTrim( Z1827GpoObjCtrl_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "GPOOBJCTRL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GPOOBJCTRL_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1828GpoObjCtrl_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGPOOBJCTRL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GpoObjCtrl_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vGPOOBJCTRL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GpoObjCtrl_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "GrupoObjetoControle";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1826GpoObjCtrl_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("grupoobjetocontrole:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("grupoobjetocontrole:[SendSecurityCheck value for]"+"GpoObjCtrl_Codigo:"+context.localUtil.Format( (decimal)(A1826GpoObjCtrl_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("grupoobjetocontrole.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7GpoObjCtrl_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "GrupoObjetoControle" ;
      }

      public override String GetPgmdesc( )
      {
         return "Grupos de Objetos de Controle" ;
      }

      protected void InitializeNonKey4K201( )
      {
         A1827GpoObjCtrl_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1827GpoObjCtrl_Nome", A1827GpoObjCtrl_Nome);
         Z1827GpoObjCtrl_Nome = "";
      }

      protected void InitAll4K201( )
      {
         A1826GpoObjCtrl_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
         InitializeNonKey4K201( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529932435");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("grupoobjetocontrole.js", "?2020529932435");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockgpoobjctrl_nome_Internalname = "TEXTBLOCKGPOOBJCTRL_NOME";
         edtGpoObjCtrl_Nome_Internalname = "GPOOBJCTRL_NOME";
         lblTextblockresponsavel_codigo_Internalname = "TEXTBLOCKRESPONSAVEL_CODIGO";
         dynavResponsavel_codigo_Internalname = "vRESPONSAVEL_CODIGO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Grupo de Objeto de Controle";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Grupos de Objetos de Controle";
         dynavResponsavel_codigo_Jsonclick = "";
         dynavResponsavel_codigo.Enabled = 1;
         edtGpoObjCtrl_Nome_Jsonclick = "";
         edtGpoObjCtrl_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvRESPONSAVEL_CODIGO4K201( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvRESPONSAVEL_CODIGO_data4K201( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvRESPONSAVEL_CODIGO_html4K201( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvRESPONSAVEL_CODIGO_data4K201( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynavResponsavel_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavResponsavel_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvRESPONSAVEL_CODIGO_data4K201( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T004K16 */
         pr_default.execute(13, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(13) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004K16_A60ContratanteUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004K16_A62ContratanteUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(13);
         }
         pr_default.close(13);
      }

      protected void GX1ASAGPOOBJCTRL_RESPONSAVEL4K201( int A1826GpoObjCtrl_Codigo )
      {
         GXt_int1 = A1828GpoObjCtrl_Responsavel;
         GXt_int2 = (short)(A1826GpoObjCtrl_Codigo);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int2, out  GXt_int1) ;
         A1826GpoObjCtrl_Codigo = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1826GpoObjCtrl_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1826GpoObjCtrl_Codigo), 6, 0)));
         A1828GpoObjCtrl_Responsavel = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1828GpoObjCtrl_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1828GpoObjCtrl_Responsavel), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1828GpoObjCtrl_Responsavel), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_4_4K201( )
      {
         new prc_updgpoobjctrl_responsavel(context ).execute(  ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? "DLT" : "UPD"),  AV14Contratante_Codigo,  AV7GpoObjCtrl_Codigo,  AV13Responsavel_Codigo) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7GpoObjCtrl_Codigo',fld:'vGPOOBJCTRL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124K2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1827GpoObjCtrl_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockgpoobjctrl_nome_Jsonclick = "";
         A1827GpoObjCtrl_Nome = "";
         lblTextblockresponsavel_codigo_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode201 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T004K4_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K4_A1827GpoObjCtrl_Nome = new String[] {""} ;
         T004K5_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K3_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K3_A1827GpoObjCtrl_Nome = new String[] {""} ;
         T004K6_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K7_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K2_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K2_A1827GpoObjCtrl_Nome = new String[] {""} ;
         T004K8_A1826GpoObjCtrl_Codigo = new int[1] ;
         T004K11_A127Sistema_Codigo = new int[1] ;
         T004K12_A648Projeto_Codigo = new int[1] ;
         T004K13_A2036LinhadeNegocio_Codigo = new int[1] ;
         T004K14_A1826GpoObjCtrl_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004K16_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T004K16_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T004K16_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T004K16_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T004K16_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T004K16_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T004K16_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         T004K16_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.grupoobjetocontrole__default(),
            new Object[][] {
                new Object[] {
               T004K2_A1826GpoObjCtrl_Codigo, T004K2_A1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T004K3_A1826GpoObjCtrl_Codigo, T004K3_A1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T004K4_A1826GpoObjCtrl_Codigo, T004K4_A1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T004K5_A1826GpoObjCtrl_Codigo
               }
               , new Object[] {
               T004K6_A1826GpoObjCtrl_Codigo
               }
               , new Object[] {
               T004K7_A1826GpoObjCtrl_Codigo
               }
               , new Object[] {
               T004K8_A1826GpoObjCtrl_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004K11_A127Sistema_Codigo
               }
               , new Object[] {
               T004K12_A648Projeto_Codigo
               }
               , new Object[] {
               T004K13_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               T004K14_A1826GpoObjCtrl_Codigo
               }
               , new Object[] {
               T004K16_A61ContratanteUsuario_UsuarioPessoaCod, T004K16_n61ContratanteUsuario_UsuarioPessoaCod, T004K16_A63ContratanteUsuario_ContratanteCod, T004K16_A60ContratanteUsuario_UsuarioCod, T004K16_A62ContratanteUsuario_UsuarioPessoaNom, T004K16_n62ContratanteUsuario_UsuarioPessoaNom, T004K16_A1020ContratanteUsuario_AreaTrabalhoCod, T004K16_n1020ContratanteUsuario_AreaTrabalhoCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound201 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short GXt_int2 ;
      private int wcpOAV7GpoObjCtrl_Codigo ;
      private int Z1826GpoObjCtrl_Codigo ;
      private int A1826GpoObjCtrl_Codigo ;
      private int AV7GpoObjCtrl_Codigo ;
      private int trnEnded ;
      private int AV13Responsavel_Codigo ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtGpoObjCtrl_Nome_Enabled ;
      private int A1828GpoObjCtrl_Responsavel ;
      private int AV14Contratante_Codigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1827GpoObjCtrl_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGpoObjCtrl_Nome_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockgpoobjctrl_nome_Internalname ;
      private String lblTextblockgpoobjctrl_nome_Jsonclick ;
      private String A1827GpoObjCtrl_Nome ;
      private String edtGpoObjCtrl_Nome_Jsonclick ;
      private String lblTextblockresponsavel_codigo_Internalname ;
      private String lblTextblockresponsavel_codigo_Jsonclick ;
      private String dynavResponsavel_codigo_Internalname ;
      private String dynavResponsavel_codigo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode201 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavResponsavel_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] T004K4_A1826GpoObjCtrl_Codigo ;
      private String[] T004K4_A1827GpoObjCtrl_Nome ;
      private int[] T004K5_A1826GpoObjCtrl_Codigo ;
      private int[] T004K3_A1826GpoObjCtrl_Codigo ;
      private String[] T004K3_A1827GpoObjCtrl_Nome ;
      private int[] T004K6_A1826GpoObjCtrl_Codigo ;
      private int[] T004K7_A1826GpoObjCtrl_Codigo ;
      private int[] T004K2_A1826GpoObjCtrl_Codigo ;
      private String[] T004K2_A1827GpoObjCtrl_Nome ;
      private int[] T004K8_A1826GpoObjCtrl_Codigo ;
      private int[] T004K11_A127Sistema_Codigo ;
      private int[] T004K12_A648Projeto_Codigo ;
      private int[] T004K13_A2036LinhadeNegocio_Codigo ;
      private int[] T004K14_A1826GpoObjCtrl_Codigo ;
      private int[] T004K16_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T004K16_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] T004K16_A63ContratanteUsuario_ContratanteCod ;
      private int[] T004K16_A60ContratanteUsuario_UsuarioCod ;
      private String[] T004K16_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T004K16_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] T004K16_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] T004K16_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class grupoobjetocontrole__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004K4 ;
          prmT004K4 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K5 ;
          prmT004K5 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K3 ;
          prmT004K3 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K6 ;
          prmT004K6 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K7 ;
          prmT004K7 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K2 ;
          prmT004K2 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K8 ;
          prmT004K8 = new Object[] {
          new Object[] {"@GpoObjCtrl_Nome",SqlDbType.Char,50,0}
          } ;
          Object[] prmT004K9 ;
          prmT004K9 = new Object[] {
          new Object[] {"@GpoObjCtrl_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K10 ;
          prmT004K10 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K11 ;
          prmT004K11 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K12 ;
          prmT004K12 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K13 ;
          prmT004K13 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004K14 ;
          prmT004K14 = new Object[] {
          } ;
          Object[] prmT004K16 ;
          prmT004K16 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004K2", "SELECT [GpoObjCtrl_Codigo], [GpoObjCtrl_Nome] FROM [GrupoObjetoControle] WITH (UPDLOCK) WHERE [GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004K2,1,0,true,false )
             ,new CursorDef("T004K3", "SELECT [GpoObjCtrl_Codigo], [GpoObjCtrl_Nome] FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004K3,1,0,true,false )
             ,new CursorDef("T004K4", "SELECT TM1.[GpoObjCtrl_Codigo], TM1.[GpoObjCtrl_Nome] FROM [GrupoObjetoControle] TM1 WITH (NOLOCK) WHERE TM1.[GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo ORDER BY TM1.[GpoObjCtrl_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004K4,100,0,true,false )
             ,new CursorDef("T004K5", "SELECT [GpoObjCtrl_Codigo] FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004K5,1,0,true,false )
             ,new CursorDef("T004K6", "SELECT TOP 1 [GpoObjCtrl_Codigo] FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE ( [GpoObjCtrl_Codigo] > @GpoObjCtrl_Codigo) ORDER BY [GpoObjCtrl_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004K6,1,0,true,true )
             ,new CursorDef("T004K7", "SELECT TOP 1 [GpoObjCtrl_Codigo] FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE ( [GpoObjCtrl_Codigo] < @GpoObjCtrl_Codigo) ORDER BY [GpoObjCtrl_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004K7,1,0,true,true )
             ,new CursorDef("T004K8", "INSERT INTO [GrupoObjetoControle]([GpoObjCtrl_Nome]) VALUES(@GpoObjCtrl_Nome); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004K8)
             ,new CursorDef("T004K9", "UPDATE [GrupoObjetoControle] SET [GpoObjCtrl_Nome]=@GpoObjCtrl_Nome  WHERE [GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo", GxErrorMask.GX_NOMASK,prmT004K9)
             ,new CursorDef("T004K10", "DELETE FROM [GrupoObjetoControle]  WHERE [GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo", GxErrorMask.GX_NOMASK,prmT004K10)
             ,new CursorDef("T004K11", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_GpoObjCtrlCod] = @GpoObjCtrl_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004K11,1,0,true,true )
             ,new CursorDef("T004K12", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_GpoObjCtrlCodigo] = @GpoObjCtrl_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004K12,1,0,true,true )
             ,new CursorDef("T004K13", "SELECT TOP 1 [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhaNegocio_GpoObjCtrlCod] = @GpoObjCtrl_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004K13,1,0,true,true )
             ,new CursorDef("T004K14", "SELECT [GpoObjCtrl_Codigo] FROM [GrupoObjetoControle] WITH (NOLOCK) ORDER BY [GpoObjCtrl_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004K14,100,0,true,false )
             ,new CursorDef("T004K16", "SELECT T3.[Pessoa_Codigo] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom, COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE T5.[Contratante_Codigo] = T6.[ContratanteUsuario_ContratanteCod] GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV8WWPCo_1Areatrabalho_codigo ORDER BY T3.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004K16,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
