/*
               File: type_SdtAnexos
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:36.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Anexos" )]
   [XmlType(TypeName =  "Anexos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtAnexos_De ))]
   [Serializable]
   public class SdtAnexos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAnexos( )
      {
         /* Constructor for serialization */
         gxTv_SdtAnexos_Anexo_nomearq = "";
         gxTv_SdtAnexos_Anexo_tipoarq = "";
         gxTv_SdtAnexos_Anexo_arquivo = "";
         gxTv_SdtAnexos_Anexo_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtAnexos_Tipodocumento_nome = "";
         gxTv_SdtAnexos_Anexo_descricao = "";
         gxTv_SdtAnexos_Anexo_link = "";
         gxTv_SdtAnexos_Anexo_entidade = "";
         gxTv_SdtAnexos_Mode = "";
         gxTv_SdtAnexos_Anexo_nomearq_Z = "";
         gxTv_SdtAnexos_Anexo_tipoarq_Z = "";
         gxTv_SdtAnexos_Anexo_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtAnexos_Tipodocumento_nome_Z = "";
         gxTv_SdtAnexos_Anexo_descricao_Z = "";
         gxTv_SdtAnexos_Anexo_entidade_Z = "";
      }

      public SdtAnexos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1106Anexo_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1106Anexo_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Anexo_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Anexos");
         metadata.Set("BT", "Anexos");
         metadata.Set("PK", "[ \"Anexo_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Anexo_Codigo\" ]");
         metadata.Set("Levels", "[ \"De\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"TipoDocumento_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_De_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_nomearq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_tipoarq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_userid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_owner_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_demandacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_entidade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_nomearq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_tipoarq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_arquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_link_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_owner_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_demandacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Anexo_areatrabalhocod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAnexos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAnexos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAnexos obj ;
         obj = this;
         obj.gxTpr_Anexo_codigo = deserialized.gxTpr_Anexo_codigo;
         obj.gxTpr_Anexo_nomearq = deserialized.gxTpr_Anexo_nomearq;
         obj.gxTpr_Anexo_tipoarq = deserialized.gxTpr_Anexo_tipoarq;
         obj.gxTpr_Anexo_arquivo = deserialized.gxTpr_Anexo_arquivo;
         obj.gxTpr_Anexo_userid = deserialized.gxTpr_Anexo_userid;
         obj.gxTpr_Anexo_data = deserialized.gxTpr_Anexo_data;
         obj.gxTpr_Tipodocumento_codigo = deserialized.gxTpr_Tipodocumento_codigo;
         obj.gxTpr_Tipodocumento_nome = deserialized.gxTpr_Tipodocumento_nome;
         obj.gxTpr_Anexo_descricao = deserialized.gxTpr_Anexo_descricao;
         obj.gxTpr_Anexo_link = deserialized.gxTpr_Anexo_link;
         obj.gxTpr_Anexo_owner = deserialized.gxTpr_Anexo_owner;
         obj.gxTpr_Anexo_demandacod = deserialized.gxTpr_Anexo_demandacod;
         obj.gxTpr_Anexo_areatrabalhocod = deserialized.gxTpr_Anexo_areatrabalhocod;
         obj.gxTpr_Anexo_entidade = deserialized.gxTpr_Anexo_entidade;
         obj.gxTpr_De = deserialized.gxTpr_De;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Anexo_codigo_Z = deserialized.gxTpr_Anexo_codigo_Z;
         obj.gxTpr_Anexo_nomearq_Z = deserialized.gxTpr_Anexo_nomearq_Z;
         obj.gxTpr_Anexo_tipoarq_Z = deserialized.gxTpr_Anexo_tipoarq_Z;
         obj.gxTpr_Anexo_userid_Z = deserialized.gxTpr_Anexo_userid_Z;
         obj.gxTpr_Anexo_data_Z = deserialized.gxTpr_Anexo_data_Z;
         obj.gxTpr_Tipodocumento_codigo_Z = deserialized.gxTpr_Tipodocumento_codigo_Z;
         obj.gxTpr_Tipodocumento_nome_Z = deserialized.gxTpr_Tipodocumento_nome_Z;
         obj.gxTpr_Anexo_descricao_Z = deserialized.gxTpr_Anexo_descricao_Z;
         obj.gxTpr_Anexo_owner_Z = deserialized.gxTpr_Anexo_owner_Z;
         obj.gxTpr_Anexo_demandacod_Z = deserialized.gxTpr_Anexo_demandacod_Z;
         obj.gxTpr_Anexo_areatrabalhocod_Z = deserialized.gxTpr_Anexo_areatrabalhocod_Z;
         obj.gxTpr_Anexo_entidade_Z = deserialized.gxTpr_Anexo_entidade_Z;
         obj.gxTpr_Anexo_nomearq_N = deserialized.gxTpr_Anexo_nomearq_N;
         obj.gxTpr_Anexo_tipoarq_N = deserialized.gxTpr_Anexo_tipoarq_N;
         obj.gxTpr_Anexo_arquivo_N = deserialized.gxTpr_Anexo_arquivo_N;
         obj.gxTpr_Tipodocumento_codigo_N = deserialized.gxTpr_Tipodocumento_codigo_N;
         obj.gxTpr_Anexo_descricao_N = deserialized.gxTpr_Anexo_descricao_N;
         obj.gxTpr_Anexo_link_N = deserialized.gxTpr_Anexo_link_N;
         obj.gxTpr_Anexo_owner_N = deserialized.gxTpr_Anexo_owner_N;
         obj.gxTpr_Anexo_demandacod_N = deserialized.gxTpr_Anexo_demandacod_N;
         obj.gxTpr_Anexo_areatrabalhocod_N = deserialized.gxTpr_Anexo_areatrabalhocod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Codigo") )
               {
                  gxTv_SdtAnexos_Anexo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_NomeArq") )
               {
                  gxTv_SdtAnexos_Anexo_nomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_TipoArq") )
               {
                  gxTv_SdtAnexos_Anexo_tipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Arquivo") )
               {
                  gxTv_SdtAnexos_Anexo_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_UserId") )
               {
                  gxTv_SdtAnexos_Anexo_userid = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtAnexos_Anexo_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtAnexos_Anexo_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo") )
               {
                  gxTv_SdtAnexos_Tipodocumento_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome") )
               {
                  gxTv_SdtAnexos_Tipodocumento_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Descricao") )
               {
                  gxTv_SdtAnexos_Anexo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Link") )
               {
                  gxTv_SdtAnexos_Anexo_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Owner") )
               {
                  gxTv_SdtAnexos_Anexo_owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_DemandaCod") )
               {
                  gxTv_SdtAnexos_Anexo_demandacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_AreaTrabalhoCod") )
               {
                  gxTv_SdtAnexos_Anexo_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Entidade") )
               {
                  gxTv_SdtAnexos_Anexo_entidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "De") )
               {
                  if ( gxTv_SdtAnexos_De == null )
                  {
                     gxTv_SdtAnexos_De = new GxSilentTrnGridCollection( context, "Anexos.De", "GxEv3Up14_MeetrikaVs3", "SdtAnexos_De", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtAnexos_De.readxml(oReader, "De");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAnexos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAnexos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Codigo_Z") )
               {
                  gxTv_SdtAnexos_Anexo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_NomeArq_Z") )
               {
                  gxTv_SdtAnexos_Anexo_nomearq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_TipoArq_Z") )
               {
                  gxTv_SdtAnexos_Anexo_tipoarq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_UserId_Z") )
               {
                  gxTv_SdtAnexos_Anexo_userid_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtAnexos_Anexo_data_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtAnexos_Anexo_data_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_Z") )
               {
                  gxTv_SdtAnexos_Tipodocumento_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome_Z") )
               {
                  gxTv_SdtAnexos_Tipodocumento_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Descricao_Z") )
               {
                  gxTv_SdtAnexos_Anexo_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Owner_Z") )
               {
                  gxTv_SdtAnexos_Anexo_owner_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_DemandaCod_Z") )
               {
                  gxTv_SdtAnexos_Anexo_demandacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtAnexos_Anexo_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Entidade_Z") )
               {
                  gxTv_SdtAnexos_Anexo_entidade_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_NomeArq_N") )
               {
                  gxTv_SdtAnexos_Anexo_nomearq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_TipoArq_N") )
               {
                  gxTv_SdtAnexos_Anexo_tipoarq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Arquivo_N") )
               {
                  gxTv_SdtAnexos_Anexo_arquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_N") )
               {
                  gxTv_SdtAnexos_Tipodocumento_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Descricao_N") )
               {
                  gxTv_SdtAnexos_Anexo_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Link_N") )
               {
                  gxTv_SdtAnexos_Anexo_link_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_Owner_N") )
               {
                  gxTv_SdtAnexos_Anexo_owner_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_DemandaCod_N") )
               {
                  gxTv_SdtAnexos_Anexo_demandacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexo_AreaTrabalhoCod_N") )
               {
                  gxTv_SdtAnexos_Anexo_areatrabalhocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Anexos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Anexo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_NomeArq", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_nomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_TipoArq", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_tipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_Arquivo", context.FileToBase64( gxTv_SdtAnexos_Anexo_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_UserId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_userid), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtAnexos_Anexo_data) )
         {
            oWriter.WriteStartElement("Anexo_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtAnexos_Anexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtAnexos_Anexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtAnexos_Anexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtAnexos_Anexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtAnexos_Anexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtAnexos_Anexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Anexo_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("TipoDocumento_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Tipodocumento_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoDocumento_Nome", StringUtil.RTrim( gxTv_SdtAnexos_Tipodocumento_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_Descricao", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_Link", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_DemandaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_demandacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Anexo_Entidade", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_entidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            if ( gxTv_SdtAnexos_De != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
               }
               gxTv_SdtAnexos_De.writexml(oWriter, "De", sNameSpace1);
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAnexos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_NomeArq_Z", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_nomearq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_TipoArq_Z", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_tipoarq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_UserId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_userid_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtAnexos_Anexo_data_Z) )
            {
               oWriter.WriteStartElement("Anexo_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtAnexos_Anexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtAnexos_Anexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtAnexos_Anexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtAnexos_Anexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtAnexos_Anexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtAnexos_Anexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Anexo_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("TipoDocumento_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Tipodocumento_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Nome_Z", StringUtil.RTrim( gxTv_SdtAnexos_Tipodocumento_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Descricao_Z", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Owner_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_owner_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_DemandaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_demandacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Entidade_Z", StringUtil.RTrim( gxTv_SdtAnexos_Anexo_entidade_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_NomeArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_nomearq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_TipoArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_tipoarq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Arquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_arquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Tipodocumento_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Link_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_link_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_Owner_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_owner_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_DemandaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_demandacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Anexo_AreaTrabalhoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAnexos_Anexo_areatrabalhocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Anexo_Codigo", gxTv_SdtAnexos_Anexo_codigo, false);
         AddObjectProperty("Anexo_NomeArq", gxTv_SdtAnexos_Anexo_nomearq, false);
         AddObjectProperty("Anexo_TipoArq", gxTv_SdtAnexos_Anexo_tipoarq, false);
         AddObjectProperty("Anexo_Arquivo", gxTv_SdtAnexos_Anexo_arquivo, false);
         AddObjectProperty("Anexo_UserId", gxTv_SdtAnexos_Anexo_userid, false);
         datetime_STZ = gxTv_SdtAnexos_Anexo_data;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Anexo_Data", sDateCnv, false);
         AddObjectProperty("TipoDocumento_Codigo", gxTv_SdtAnexos_Tipodocumento_codigo, false);
         AddObjectProperty("TipoDocumento_Nome", gxTv_SdtAnexos_Tipodocumento_nome, false);
         AddObjectProperty("Anexo_Descricao", gxTv_SdtAnexos_Anexo_descricao, false);
         AddObjectProperty("Anexo_Link", gxTv_SdtAnexos_Anexo_link, false);
         AddObjectProperty("Anexo_Owner", gxTv_SdtAnexos_Anexo_owner, false);
         AddObjectProperty("Anexo_DemandaCod", gxTv_SdtAnexos_Anexo_demandacod, false);
         AddObjectProperty("Anexo_AreaTrabalhoCod", gxTv_SdtAnexos_Anexo_areatrabalhocod, false);
         AddObjectProperty("Anexo_Entidade", gxTv_SdtAnexos_Anexo_entidade, false);
         if ( gxTv_SdtAnexos_De != null )
         {
            AddObjectProperty("De", gxTv_SdtAnexos_De, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAnexos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtAnexos_Initialized, false);
            AddObjectProperty("Anexo_Codigo_Z", gxTv_SdtAnexos_Anexo_codigo_Z, false);
            AddObjectProperty("Anexo_NomeArq_Z", gxTv_SdtAnexos_Anexo_nomearq_Z, false);
            AddObjectProperty("Anexo_TipoArq_Z", gxTv_SdtAnexos_Anexo_tipoarq_Z, false);
            AddObjectProperty("Anexo_UserId_Z", gxTv_SdtAnexos_Anexo_userid_Z, false);
            datetime_STZ = gxTv_SdtAnexos_Anexo_data_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Anexo_Data_Z", sDateCnv, false);
            AddObjectProperty("TipoDocumento_Codigo_Z", gxTv_SdtAnexos_Tipodocumento_codigo_Z, false);
            AddObjectProperty("TipoDocumento_Nome_Z", gxTv_SdtAnexos_Tipodocumento_nome_Z, false);
            AddObjectProperty("Anexo_Descricao_Z", gxTv_SdtAnexos_Anexo_descricao_Z, false);
            AddObjectProperty("Anexo_Owner_Z", gxTv_SdtAnexos_Anexo_owner_Z, false);
            AddObjectProperty("Anexo_DemandaCod_Z", gxTv_SdtAnexos_Anexo_demandacod_Z, false);
            AddObjectProperty("Anexo_AreaTrabalhoCod_Z", gxTv_SdtAnexos_Anexo_areatrabalhocod_Z, false);
            AddObjectProperty("Anexo_Entidade_Z", gxTv_SdtAnexos_Anexo_entidade_Z, false);
            AddObjectProperty("Anexo_NomeArq_N", gxTv_SdtAnexos_Anexo_nomearq_N, false);
            AddObjectProperty("Anexo_TipoArq_N", gxTv_SdtAnexos_Anexo_tipoarq_N, false);
            AddObjectProperty("Anexo_Arquivo_N", gxTv_SdtAnexos_Anexo_arquivo_N, false);
            AddObjectProperty("TipoDocumento_Codigo_N", gxTv_SdtAnexos_Tipodocumento_codigo_N, false);
            AddObjectProperty("Anexo_Descricao_N", gxTv_SdtAnexos_Anexo_descricao_N, false);
            AddObjectProperty("Anexo_Link_N", gxTv_SdtAnexos_Anexo_link_N, false);
            AddObjectProperty("Anexo_Owner_N", gxTv_SdtAnexos_Anexo_owner_N, false);
            AddObjectProperty("Anexo_DemandaCod_N", gxTv_SdtAnexos_Anexo_demandacod_N, false);
            AddObjectProperty("Anexo_AreaTrabalhoCod_N", gxTv_SdtAnexos_Anexo_areatrabalhocod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Anexo_Codigo" )]
      [  XmlElement( ElementName = "Anexo_Codigo"   )]
      public int gxTpr_Anexo_codigo
      {
         get {
            return gxTv_SdtAnexos_Anexo_codigo ;
         }

         set {
            if ( gxTv_SdtAnexos_Anexo_codigo != value )
            {
               gxTv_SdtAnexos_Mode = "INS";
               this.gxTv_SdtAnexos_Anexo_codigo_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_nomearq_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_tipoarq_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_userid_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_data_Z_SetNull( );
               this.gxTv_SdtAnexos_Tipodocumento_codigo_Z_SetNull( );
               this.gxTv_SdtAnexos_Tipodocumento_nome_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_descricao_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_owner_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_demandacod_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtAnexos_Anexo_entidade_Z_SetNull( );
               if ( gxTv_SdtAnexos_De != null )
               {
                  GxSilentTrnGridCollection collectionDe = gxTv_SdtAnexos_De ;
                  SdtAnexos_De currItemDe ;
                  short idx = 1 ;
                  while ( idx <= collectionDe.Count )
                  {
                     currItemDe = ((SdtAnexos_De)collectionDe.Item(idx));
                     currItemDe.gxTpr_Mode = "INS";
                     currItemDe.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtAnexos_Anexo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Anexo_NomeArq" )]
      [  XmlElement( ElementName = "Anexo_NomeArq"   )]
      public String gxTpr_Anexo_nomearq
      {
         get {
            return gxTv_SdtAnexos_Anexo_nomearq ;
         }

         set {
            gxTv_SdtAnexos_Anexo_nomearq_N = 0;
            gxTv_SdtAnexos_Anexo_nomearq = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_nomearq_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_nomearq_N = 1;
         gxTv_SdtAnexos_Anexo_nomearq = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_nomearq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_TipoArq" )]
      [  XmlElement( ElementName = "Anexo_TipoArq"   )]
      public String gxTpr_Anexo_tipoarq
      {
         get {
            return gxTv_SdtAnexos_Anexo_tipoarq ;
         }

         set {
            gxTv_SdtAnexos_Anexo_tipoarq_N = 0;
            gxTv_SdtAnexos_Anexo_tipoarq = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_tipoarq_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_tipoarq_N = 1;
         gxTv_SdtAnexos_Anexo_tipoarq = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_tipoarq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Arquivo" )]
      [  XmlElement( ElementName = "Anexo_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Anexo_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtAnexos_Anexo_arquivo) ;
         }

         set {
            gxTv_SdtAnexos_Anexo_arquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtAnexos_Anexo_arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Anexo_arquivo
      {
         get {
            return gxTv_SdtAnexos_Anexo_arquivo ;
         }

         set {
            gxTv_SdtAnexos_Anexo_arquivo_N = 0;
            gxTv_SdtAnexos_Anexo_arquivo = value;
         }

      }

      public void gxTv_SdtAnexos_Anexo_arquivo_SetBlob( String blob ,
                                                        String fileName ,
                                                        String fileType )
      {
         gxTv_SdtAnexos_Anexo_arquivo = blob;
         gxTv_SdtAnexos_Anexo_nomearq = fileName;
         gxTv_SdtAnexos_Anexo_tipoarq = fileType;
         return  ;
      }

      public void gxTv_SdtAnexos_Anexo_arquivo_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_arquivo_N = 1;
         gxTv_SdtAnexos_Anexo_arquivo = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_arquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_UserId" )]
      [  XmlElement( ElementName = "Anexo_UserId"   )]
      public int gxTpr_Anexo_userid
      {
         get {
            return gxTv_SdtAnexos_Anexo_userid ;
         }

         set {
            gxTv_SdtAnexos_Anexo_userid = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Anexo_Data" )]
      [  XmlElement( ElementName = "Anexo_Data"  , IsNullable=true )]
      public string gxTpr_Anexo_data_Nullable
      {
         get {
            if ( gxTv_SdtAnexos_Anexo_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtAnexos_Anexo_data).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtAnexos_Anexo_data = DateTime.MinValue;
            else
               gxTv_SdtAnexos_Anexo_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Anexo_data
      {
         get {
            return gxTv_SdtAnexos_Anexo_data ;
         }

         set {
            gxTv_SdtAnexos_Anexo_data = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo"   )]
      public int gxTpr_Tipodocumento_codigo
      {
         get {
            return gxTv_SdtAnexos_Tipodocumento_codigo ;
         }

         set {
            gxTv_SdtAnexos_Tipodocumento_codigo_N = 0;
            gxTv_SdtAnexos_Tipodocumento_codigo = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Tipodocumento_codigo_SetNull( )
      {
         gxTv_SdtAnexos_Tipodocumento_codigo_N = 1;
         gxTv_SdtAnexos_Tipodocumento_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Tipodocumento_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome"   )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return gxTv_SdtAnexos_Tipodocumento_nome ;
         }

         set {
            gxTv_SdtAnexos_Tipodocumento_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Anexo_Descricao" )]
      [  XmlElement( ElementName = "Anexo_Descricao"   )]
      public String gxTpr_Anexo_descricao
      {
         get {
            return gxTv_SdtAnexos_Anexo_descricao ;
         }

         set {
            gxTv_SdtAnexos_Anexo_descricao_N = 0;
            gxTv_SdtAnexos_Anexo_descricao = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_descricao_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_descricao_N = 1;
         gxTv_SdtAnexos_Anexo_descricao = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Link" )]
      [  XmlElement( ElementName = "Anexo_Link"   )]
      public String gxTpr_Anexo_link
      {
         get {
            return gxTv_SdtAnexos_Anexo_link ;
         }

         set {
            gxTv_SdtAnexos_Anexo_link_N = 0;
            gxTv_SdtAnexos_Anexo_link = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_link_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_link_N = 1;
         gxTv_SdtAnexos_Anexo_link = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_link_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Owner" )]
      [  XmlElement( ElementName = "Anexo_Owner"   )]
      public int gxTpr_Anexo_owner
      {
         get {
            return gxTv_SdtAnexos_Anexo_owner ;
         }

         set {
            gxTv_SdtAnexos_Anexo_owner_N = 0;
            gxTv_SdtAnexos_Anexo_owner = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_owner_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_owner_N = 1;
         gxTv_SdtAnexos_Anexo_owner = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_owner_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_DemandaCod" )]
      [  XmlElement( ElementName = "Anexo_DemandaCod"   )]
      public int gxTpr_Anexo_demandacod
      {
         get {
            return gxTv_SdtAnexos_Anexo_demandacod ;
         }

         set {
            gxTv_SdtAnexos_Anexo_demandacod_N = 0;
            gxTv_SdtAnexos_Anexo_demandacod = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_demandacod_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_demandacod_N = 1;
         gxTv_SdtAnexos_Anexo_demandacod = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_demandacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Anexo_AreaTrabalhoCod"   )]
      public int gxTpr_Anexo_areatrabalhocod
      {
         get {
            return gxTv_SdtAnexos_Anexo_areatrabalhocod ;
         }

         set {
            gxTv_SdtAnexos_Anexo_areatrabalhocod_N = 0;
            gxTv_SdtAnexos_Anexo_areatrabalhocod = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_areatrabalhocod_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_areatrabalhocod_N = 1;
         gxTv_SdtAnexos_Anexo_areatrabalhocod = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_areatrabalhocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Entidade" )]
      [  XmlElement( ElementName = "Anexo_Entidade"   )]
      public String gxTpr_Anexo_entidade
      {
         get {
            return gxTv_SdtAnexos_Anexo_entidade ;
         }

         set {
            gxTv_SdtAnexos_Anexo_entidade = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_entidade_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_entidade = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_entidade_IsNull( )
      {
         return false ;
      }

      public class gxTv_SdtAnexos_De_SdtAnexos_De_80compatibility:SdtAnexos_De {}
      [  SoapElement( ElementName = "De" )]
      [  XmlArray( ElementName = "De"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtAnexos_De ), ElementName= "Anexos.De"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_De_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtAnexos_De == null )
            {
               gxTv_SdtAnexos_De = new GxSilentTrnGridCollection( context, "Anexos.De", "GxEv3Up14_MeetrikaVs3", "SdtAnexos_De", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtAnexos_De ;
         }

         set {
            if ( gxTv_SdtAnexos_De == null )
            {
               gxTv_SdtAnexos_De = new GxSilentTrnGridCollection( context, "Anexos.De", "GxEv3Up14_MeetrikaVs3", "SdtAnexos_De", "GeneXus.Programs");
            }
            gxTv_SdtAnexos_De = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_De
      {
         get {
            if ( gxTv_SdtAnexos_De == null )
            {
               gxTv_SdtAnexos_De = new GxSilentTrnGridCollection( context, "Anexos.De", "GxEv3Up14_MeetrikaVs3", "SdtAnexos_De", "GeneXus.Programs");
            }
            return gxTv_SdtAnexos_De ;
         }

         set {
            gxTv_SdtAnexos_De = value;
         }

      }

      public void gxTv_SdtAnexos_De_SetNull( )
      {
         gxTv_SdtAnexos_De = null;
         return  ;
      }

      public bool gxTv_SdtAnexos_De_IsNull( )
      {
         if ( gxTv_SdtAnexos_De == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAnexos_Mode ;
         }

         set {
            gxTv_SdtAnexos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Mode_SetNull( )
      {
         gxTv_SdtAnexos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAnexos_Initialized ;
         }

         set {
            gxTv_SdtAnexos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Initialized_SetNull( )
      {
         gxTv_SdtAnexos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Codigo_Z" )]
      [  XmlElement( ElementName = "Anexo_Codigo_Z"   )]
      public int gxTpr_Anexo_codigo_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_codigo_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_codigo_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_NomeArq_Z" )]
      [  XmlElement( ElementName = "Anexo_NomeArq_Z"   )]
      public String gxTpr_Anexo_nomearq_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_nomearq_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_nomearq_Z = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_nomearq_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_nomearq_Z = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_nomearq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_TipoArq_Z" )]
      [  XmlElement( ElementName = "Anexo_TipoArq_Z"   )]
      public String gxTpr_Anexo_tipoarq_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_tipoarq_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_tipoarq_Z = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_tipoarq_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_tipoarq_Z = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_tipoarq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_UserId_Z" )]
      [  XmlElement( ElementName = "Anexo_UserId_Z"   )]
      public int gxTpr_Anexo_userid_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_userid_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_userid_Z = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_userid_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_userid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_userid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Data_Z" )]
      [  XmlElement( ElementName = "Anexo_Data_Z"  , IsNullable=true )]
      public string gxTpr_Anexo_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtAnexos_Anexo_data_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtAnexos_Anexo_data_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtAnexos_Anexo_data_Z = DateTime.MinValue;
            else
               gxTv_SdtAnexos_Anexo_data_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Anexo_data_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_data_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_data_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_data_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_Z"   )]
      public int gxTpr_Tipodocumento_codigo_Z
      {
         get {
            return gxTv_SdtAnexos_Tipodocumento_codigo_Z ;
         }

         set {
            gxTv_SdtAnexos_Tipodocumento_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Tipodocumento_codigo_Z_SetNull( )
      {
         gxTv_SdtAnexos_Tipodocumento_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Tipodocumento_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome_Z"   )]
      public String gxTpr_Tipodocumento_nome_Z
      {
         get {
            return gxTv_SdtAnexos_Tipodocumento_nome_Z ;
         }

         set {
            gxTv_SdtAnexos_Tipodocumento_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Tipodocumento_nome_Z_SetNull( )
      {
         gxTv_SdtAnexos_Tipodocumento_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Tipodocumento_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Descricao_Z" )]
      [  XmlElement( ElementName = "Anexo_Descricao_Z"   )]
      public String gxTpr_Anexo_descricao_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_descricao_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_descricao_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Owner_Z" )]
      [  XmlElement( ElementName = "Anexo_Owner_Z"   )]
      public int gxTpr_Anexo_owner_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_owner_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_owner_Z = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_owner_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_owner_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_owner_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_DemandaCod_Z" )]
      [  XmlElement( ElementName = "Anexo_DemandaCod_Z"   )]
      public int gxTpr_Anexo_demandacod_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_demandacod_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_demandacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_demandacod_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_demandacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_demandacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Anexo_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Anexo_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Entidade_Z" )]
      [  XmlElement( ElementName = "Anexo_Entidade_Z"   )]
      public String gxTpr_Anexo_entidade_Z
      {
         get {
            return gxTv_SdtAnexos_Anexo_entidade_Z ;
         }

         set {
            gxTv_SdtAnexos_Anexo_entidade_Z = (String)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_entidade_Z_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_entidade_Z = "";
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_entidade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_NomeArq_N" )]
      [  XmlElement( ElementName = "Anexo_NomeArq_N"   )]
      public short gxTpr_Anexo_nomearq_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_nomearq_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_nomearq_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_nomearq_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_nomearq_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_nomearq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_TipoArq_N" )]
      [  XmlElement( ElementName = "Anexo_TipoArq_N"   )]
      public short gxTpr_Anexo_tipoarq_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_tipoarq_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_tipoarq_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_tipoarq_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_tipoarq_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_tipoarq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Arquivo_N" )]
      [  XmlElement( ElementName = "Anexo_Arquivo_N"   )]
      public short gxTpr_Anexo_arquivo_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_arquivo_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_arquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_arquivo_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_arquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_arquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_N" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_N"   )]
      public short gxTpr_Tipodocumento_codigo_N
      {
         get {
            return gxTv_SdtAnexos_Tipodocumento_codigo_N ;
         }

         set {
            gxTv_SdtAnexos_Tipodocumento_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Tipodocumento_codigo_N_SetNull( )
      {
         gxTv_SdtAnexos_Tipodocumento_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Tipodocumento_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Descricao_N" )]
      [  XmlElement( ElementName = "Anexo_Descricao_N"   )]
      public short gxTpr_Anexo_descricao_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_descricao_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_descricao_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Link_N" )]
      [  XmlElement( ElementName = "Anexo_Link_N"   )]
      public short gxTpr_Anexo_link_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_link_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_link_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_link_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_link_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_link_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_Owner_N" )]
      [  XmlElement( ElementName = "Anexo_Owner_N"   )]
      public short gxTpr_Anexo_owner_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_owner_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_owner_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_owner_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_owner_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_owner_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_DemandaCod_N" )]
      [  XmlElement( ElementName = "Anexo_DemandaCod_N"   )]
      public short gxTpr_Anexo_demandacod_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_demandacod_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_demandacod_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_demandacod_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_demandacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_demandacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Anexo_AreaTrabalhoCod_N" )]
      [  XmlElement( ElementName = "Anexo_AreaTrabalhoCod_N"   )]
      public short gxTpr_Anexo_areatrabalhocod_N
      {
         get {
            return gxTv_SdtAnexos_Anexo_areatrabalhocod_N ;
         }

         set {
            gxTv_SdtAnexos_Anexo_areatrabalhocod_N = (short)(value);
         }

      }

      public void gxTv_SdtAnexos_Anexo_areatrabalhocod_N_SetNull( )
      {
         gxTv_SdtAnexos_Anexo_areatrabalhocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtAnexos_Anexo_areatrabalhocod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAnexos_Anexo_nomearq = "";
         gxTv_SdtAnexos_Anexo_tipoarq = "";
         gxTv_SdtAnexos_Anexo_arquivo = "";
         gxTv_SdtAnexos_Anexo_data = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtAnexos_Tipodocumento_nome = "";
         gxTv_SdtAnexos_Anexo_descricao = "";
         gxTv_SdtAnexos_Anexo_link = "";
         gxTv_SdtAnexos_Anexo_entidade = "";
         gxTv_SdtAnexos_Mode = "";
         gxTv_SdtAnexos_Anexo_nomearq_Z = "";
         gxTv_SdtAnexos_Anexo_tipoarq_Z = "";
         gxTv_SdtAnexos_Anexo_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtAnexos_Tipodocumento_nome_Z = "";
         gxTv_SdtAnexos_Anexo_descricao_Z = "";
         gxTv_SdtAnexos_Anexo_entidade_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "anexos", "GeneXus.Programs.anexos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtAnexos_Initialized ;
      private short gxTv_SdtAnexos_Anexo_nomearq_N ;
      private short gxTv_SdtAnexos_Anexo_tipoarq_N ;
      private short gxTv_SdtAnexos_Anexo_arquivo_N ;
      private short gxTv_SdtAnexos_Tipodocumento_codigo_N ;
      private short gxTv_SdtAnexos_Anexo_descricao_N ;
      private short gxTv_SdtAnexos_Anexo_link_N ;
      private short gxTv_SdtAnexos_Anexo_owner_N ;
      private short gxTv_SdtAnexos_Anexo_demandacod_N ;
      private short gxTv_SdtAnexos_Anexo_areatrabalhocod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAnexos_Anexo_codigo ;
      private int gxTv_SdtAnexos_Anexo_userid ;
      private int gxTv_SdtAnexos_Tipodocumento_codigo ;
      private int gxTv_SdtAnexos_Anexo_owner ;
      private int gxTv_SdtAnexos_Anexo_demandacod ;
      private int gxTv_SdtAnexos_Anexo_areatrabalhocod ;
      private int gxTv_SdtAnexos_Anexo_codigo_Z ;
      private int gxTv_SdtAnexos_Anexo_userid_Z ;
      private int gxTv_SdtAnexos_Tipodocumento_codigo_Z ;
      private int gxTv_SdtAnexos_Anexo_owner_Z ;
      private int gxTv_SdtAnexos_Anexo_demandacod_Z ;
      private int gxTv_SdtAnexos_Anexo_areatrabalhocod_Z ;
      private String gxTv_SdtAnexos_Anexo_nomearq ;
      private String gxTv_SdtAnexos_Anexo_tipoarq ;
      private String gxTv_SdtAnexos_Tipodocumento_nome ;
      private String gxTv_SdtAnexos_Anexo_entidade ;
      private String gxTv_SdtAnexos_Mode ;
      private String gxTv_SdtAnexos_Anexo_nomearq_Z ;
      private String gxTv_SdtAnexos_Anexo_tipoarq_Z ;
      private String gxTv_SdtAnexos_Tipodocumento_nome_Z ;
      private String gxTv_SdtAnexos_Anexo_entidade_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtAnexos_Anexo_data ;
      private DateTime gxTv_SdtAnexos_Anexo_data_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtAnexos_Anexo_link ;
      private String gxTv_SdtAnexos_Anexo_descricao ;
      private String gxTv_SdtAnexos_Anexo_descricao_Z ;
      private String gxTv_SdtAnexos_Anexo_arquivo ;
      private Assembly constructorCallingAssembly ;
      [ObjectCollection(ItemType=typeof( SdtAnexos_De ))]
      private GxSilentTrnGridCollection gxTv_SdtAnexos_De=null ;
   }

   [DataContract(Name = @"Anexos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtAnexos_RESTInterface : GxGenericCollectionItem<SdtAnexos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAnexos_RESTInterface( ) : base()
      {
      }

      public SdtAnexos_RESTInterface( SdtAnexos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Anexo_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexo_codigo
      {
         get {
            return sdt.gxTpr_Anexo_codigo ;
         }

         set {
            sdt.gxTpr_Anexo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Anexo_NomeArq" , Order = 1 )]
      public String gxTpr_Anexo_nomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Anexo_nomearq) ;
         }

         set {
            sdt.gxTpr_Anexo_nomearq = (String)(value);
         }

      }

      [DataMember( Name = "Anexo_TipoArq" , Order = 2 )]
      public String gxTpr_Anexo_tipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Anexo_tipoarq) ;
         }

         set {
            sdt.gxTpr_Anexo_tipoarq = (String)(value);
         }

      }

      [DataMember( Name = "Anexo_Arquivo" , Order = 3 )]
      [GxUpload()]
      public String gxTpr_Anexo_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Anexo_arquivo) ;
         }

         set {
            sdt.gxTpr_Anexo_arquivo = value;
         }

      }

      [DataMember( Name = "Anexo_UserId" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexo_userid
      {
         get {
            return sdt.gxTpr_Anexo_userid ;
         }

         set {
            sdt.gxTpr_Anexo_userid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Anexo_Data" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Anexo_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Anexo_data) ;
         }

         set {
            sdt.gxTpr_Anexo_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "TipoDocumento_Codigo" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tipodocumento_codigo
      {
         get {
            return sdt.gxTpr_Tipodocumento_codigo ;
         }

         set {
            sdt.gxTpr_Tipodocumento_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TipoDocumento_Nome" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento_nome) ;
         }

         set {
            sdt.gxTpr_Tipodocumento_nome = (String)(value);
         }

      }

      [DataMember( Name = "Anexo_Descricao" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Anexo_descricao
      {
         get {
            return sdt.gxTpr_Anexo_descricao ;
         }

         set {
            sdt.gxTpr_Anexo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Anexo_Link" , Order = 9 )]
      public String gxTpr_Anexo_link
      {
         get {
            return sdt.gxTpr_Anexo_link ;
         }

         set {
            sdt.gxTpr_Anexo_link = (String)(value);
         }

      }

      [DataMember( Name = "Anexo_Owner" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexo_owner
      {
         get {
            return sdt.gxTpr_Anexo_owner ;
         }

         set {
            sdt.gxTpr_Anexo_owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Anexo_DemandaCod" , Order = 11 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexo_demandacod
      {
         get {
            return sdt.gxTpr_Anexo_demandacod ;
         }

         set {
            sdt.gxTpr_Anexo_demandacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Anexo_AreaTrabalhoCod" , Order = 12 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Anexo_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Anexo_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Anexo_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Anexo_Entidade" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Anexo_entidade
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Anexo_entidade) ;
         }

         set {
            sdt.gxTpr_Anexo_entidade = (String)(value);
         }

      }

      [DataMember( Name = "De" , Order = 14 )]
      public GxGenericCollection<SdtAnexos_De_RESTInterface> gxTpr_De
      {
         get {
            return new GxGenericCollection<SdtAnexos_De_RESTInterface>(sdt.gxTpr_De) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_De);
         }

      }

      public SdtAnexos sdt
      {
         get {
            return (SdtAnexos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAnexos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 38 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
