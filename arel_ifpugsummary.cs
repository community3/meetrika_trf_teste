/*
               File: REL_IFPUGSummary
        Description: Summary das planilhas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:28:13.89
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_ifpugsummary : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV46User_Nome = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV21Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV9Area = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV18Colaborador = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV43Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV8Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV33Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_ifpugsummary( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_ifpugsummary( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_User_Nome ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Area ,
                           int aP3_Colaborador ,
                           int aP4_Servico ,
                           short aP5_Ano ,
                           long aP6_Mes )
      {
         this.AV46User_Nome = aP0_User_Nome;
         this.AV21Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV9Area = aP2_Area;
         this.AV18Colaborador = aP3_Colaborador;
         this.AV43Servico = aP4_Servico;
         this.AV8Ano = aP5_Ano;
         this.AV33Mes = aP6_Mes;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_User_Nome ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Area ,
                                 int aP3_Colaborador ,
                                 int aP4_Servico ,
                                 short aP5_Ano ,
                                 long aP6_Mes )
      {
         arel_ifpugsummary objarel_ifpugsummary;
         objarel_ifpugsummary = new arel_ifpugsummary();
         objarel_ifpugsummary.AV46User_Nome = aP0_User_Nome;
         objarel_ifpugsummary.AV21Contratada_Codigo = aP1_Contratada_Codigo;
         objarel_ifpugsummary.AV9Area = aP2_Area;
         objarel_ifpugsummary.AV18Colaborador = aP3_Colaborador;
         objarel_ifpugsummary.AV43Servico = aP4_Servico;
         objarel_ifpugsummary.AV8Ano = aP5_Ano;
         objarel_ifpugsummary.AV33Mes = aP6_Mes;
         objarel_ifpugsummary.context.SetSubmitInitialConfig(context);
         objarel_ifpugsummary.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_ifpugsummary);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_ifpugsummary)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV83SDT_Codigos.FromXml(AV48WebSession.Get("Codigos"), "SDT_CodigosCollection");
            AV94GXV1 = 1;
            while ( AV94GXV1 <= AV83SDT_Codigos.Count )
            {
               AV74Codigo = ((SdtSDT_Codigos)AV83SDT_Codigos.Item(AV94GXV1));
               AV17Codigos.Add(AV74Codigo.gxTpr_Codigo, 0);
               AV94GXV1 = (int)(AV94GXV1+1);
            }
            /* Execute user subroutine: 'FILTROS' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV17Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00W42 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P00W42_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00W42_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00W42_A456ContagemResultado_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P00W42_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00W42_n52Contratada_AreaTrabalhoCod[0];
               A52Contratada_AreaTrabalhoCod = P00W42_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00W42_n52Contratada_AreaTrabalhoCod[0];
               AV11Area_Codigos.Add(A52Contratada_AreaTrabalhoCod, 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV17Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00W43 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00W43_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00W43_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P00W43_A456ContagemResultado_Codigo[0];
               A1603ContagemResultado_CntCod = P00W43_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00W43_n1603ContagemResultado_CntCod[0];
               A1603ContagemResultado_CntCod = P00W43_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00W43_n1603ContagemResultado_CntCod[0];
               AV52Contrato_Codigos.Add(A1603ContagemResultado_CntCod, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV11Area_Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00W44 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A29Contratante_Codigo = P00W44_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00W44_n29Contratante_Codigo[0];
               A25Municipio_Codigo = P00W44_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00W44_n25Municipio_Codigo[0];
               A335Contratante_PessoaCod = P00W44_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00W44_A5AreaTrabalho_Codigo[0];
               A9Contratante_RazaoSocial = P00W44_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P00W44_n9Contratante_RazaoSocial[0];
               A31Contratante_Telefone = P00W44_A31Contratante_Telefone[0];
               A14Contratante_Email = P00W44_A14Contratante_Email[0];
               n14Contratante_Email = P00W44_n14Contratante_Email[0];
               A521Pessoa_CEP = P00W44_A521Pessoa_CEP[0];
               n521Pessoa_CEP = P00W44_n521Pessoa_CEP[0];
               A23Estado_UF = P00W44_A23Estado_UF[0];
               A26Municipio_Nome = P00W44_A26Municipio_Nome[0];
               A519Pessoa_Endereco = P00W44_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = P00W44_n519Pessoa_Endereco[0];
               A6AreaTrabalho_Descricao = P00W44_A6AreaTrabalho_Descricao[0];
               A25Municipio_Codigo = P00W44_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00W44_n25Municipio_Codigo[0];
               A335Contratante_PessoaCod = P00W44_A335Contratante_PessoaCod[0];
               A31Contratante_Telefone = P00W44_A31Contratante_Telefone[0];
               A14Contratante_Email = P00W44_A14Contratante_Email[0];
               n14Contratante_Email = P00W44_n14Contratante_Email[0];
               A23Estado_UF = P00W44_A23Estado_UF[0];
               A26Municipio_Nome = P00W44_A26Municipio_Nome[0];
               A9Contratante_RazaoSocial = P00W44_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = P00W44_n9Contratante_RazaoSocial[0];
               A521Pessoa_CEP = P00W44_A521Pessoa_CEP[0];
               n521Pessoa_CEP = P00W44_n521Pessoa_CEP[0];
               A519Pessoa_Endereco = P00W44_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = P00W44_n519Pessoa_Endereco[0];
               AV13AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
               AV14AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
               AV23Contratante_RazaoSocial = A9Contratante_RazaoSocial;
               AV87Contratante_Telefone = A31Contratante_Telefone;
               AV88Contratante_Email = A14Contratante_Email;
               AV89Contratante_Endereco = StringUtil.Trim( A519Pessoa_Endereco) + ", " + StringUtil.Trim( A26Municipio_Nome) + " - " + A23Estado_UF + " CEP " + StringUtil.Trim( A521Pessoa_CEP);
               pr_default.dynParam(3, new Object[]{ new Object[]{
                                                    A1603ContagemResultado_CntCod ,
                                                    AV52Contrato_Codigos ,
                                                    A456ContagemResultado_Codigo ,
                                                    AV17Codigos ,
                                                    A517ContagemResultado_Ultima ,
                                                    A5AreaTrabalho_Codigo ,
                                                    A52Contratada_AreaTrabalhoCod },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                    }
               });
               /* Using cursor P00W45 */
               pr_default.execute(3, new Object[] {A5AreaTrabalho_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  BRKW47 = false;
                  A490ContagemResultado_ContratadaCod = P00W45_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W45_n490ContagemResultado_ContratadaCod[0];
                  A489ContagemResultado_SistemaCod = P00W45_A489ContagemResultado_SistemaCod[0];
                  n489ContagemResultado_SistemaCod = P00W45_n489ContagemResultado_SistemaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W45_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W45_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = P00W45_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00W45_n1603ContagemResultado_CntCod[0];
                  A52Contratada_AreaTrabalhoCod = P00W45_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00W45_n52Contratada_AreaTrabalhoCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00W45_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00W45_n1326ContagemResultado_ContratadaTipoFab[0];
                  A460ContagemResultado_PFBFM = P00W45_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P00W45_n460ContagemResultado_PFBFM[0];
                  A458ContagemResultado_PFBFS = P00W45_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P00W45_n458ContagemResultado_PFBFS[0];
                  A509ContagemrResultado_SistemaSigla = P00W45_A509ContagemrResultado_SistemaSigla[0];
                  n509ContagemrResultado_SistemaSigla = P00W45_n509ContagemrResultado_SistemaSigla[0];
                  A456ContagemResultado_Codigo = P00W45_A456ContagemResultado_Codigo[0];
                  A517ContagemResultado_Ultima = P00W45_A517ContagemResultado_Ultima[0];
                  A598ContagemResultado_Baseline = P00W45_A598ContagemResultado_Baseline[0];
                  n598ContagemResultado_Baseline = P00W45_n598ContagemResultado_Baseline[0];
                  A473ContagemResultado_DataCnt = P00W45_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P00W45_A511ContagemResultado_HoraCnt[0];
                  A490ContagemResultado_ContratadaCod = P00W45_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W45_n490ContagemResultado_ContratadaCod[0];
                  A489ContagemResultado_SistemaCod = P00W45_A489ContagemResultado_SistemaCod[0];
                  n489ContagemResultado_SistemaCod = P00W45_n489ContagemResultado_SistemaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W45_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W45_n1553ContagemResultado_CntSrvCod[0];
                  A598ContagemResultado_Baseline = P00W45_A598ContagemResultado_Baseline[0];
                  n598ContagemResultado_Baseline = P00W45_n598ContagemResultado_Baseline[0];
                  A52Contratada_AreaTrabalhoCod = P00W45_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00W45_n52Contratada_AreaTrabalhoCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00W45_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00W45_n1326ContagemResultado_ContratadaTipoFab[0];
                  A509ContagemrResultado_SistemaSigla = P00W45_A509ContagemrResultado_SistemaSigla[0];
                  n509ContagemrResultado_SistemaSigla = P00W45_n509ContagemrResultado_SistemaSigla[0];
                  A1603ContagemResultado_CntCod = P00W45_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = P00W45_n1603ContagemResultado_CntCod[0];
                  W52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
                  n52Contratada_AreaTrabalhoCod = false;
                  if ( A598ContagemResultado_Baseline )
                  {
                     AV58Tipo = "Application";
                  }
                  else
                  {
                     AV58Tipo = "Project";
                  }
                  AV57DataCnt = A473ContagemResultado_DataCnt;
                  while ( (pr_default.getStatus(3) != 101) && ( P00W45_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( P00W45_A1603ContagemResultado_CntCod[0] == A1603ContagemResultado_CntCod ) )
                  {
                     BRKW47 = false;
                     A490ContagemResultado_ContratadaCod = P00W45_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00W45_n490ContagemResultado_ContratadaCod[0];
                     A489ContagemResultado_SistemaCod = P00W45_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P00W45_n489ContagemResultado_SistemaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00W45_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00W45_n1553ContagemResultado_CntSrvCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00W45_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00W45_n1326ContagemResultado_ContratadaTipoFab[0];
                     A460ContagemResultado_PFBFM = P00W45_A460ContagemResultado_PFBFM[0];
                     n460ContagemResultado_PFBFM = P00W45_n460ContagemResultado_PFBFM[0];
                     A458ContagemResultado_PFBFS = P00W45_A458ContagemResultado_PFBFS[0];
                     n458ContagemResultado_PFBFS = P00W45_n458ContagemResultado_PFBFS[0];
                     A509ContagemrResultado_SistemaSigla = P00W45_A509ContagemrResultado_SistemaSigla[0];
                     n509ContagemrResultado_SistemaSigla = P00W45_n509ContagemrResultado_SistemaSigla[0];
                     A456ContagemResultado_Codigo = P00W45_A456ContagemResultado_Codigo[0];
                     A473ContagemResultado_DataCnt = P00W45_A473ContagemResultado_DataCnt[0];
                     A511ContagemResultado_HoraCnt = P00W45_A511ContagemResultado_HoraCnt[0];
                     A490ContagemResultado_ContratadaCod = P00W45_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00W45_n490ContagemResultado_ContratadaCod[0];
                     A489ContagemResultado_SistemaCod = P00W45_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P00W45_n489ContagemResultado_SistemaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00W45_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00W45_n1553ContagemResultado_CntSrvCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00W45_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00W45_n1326ContagemResultado_ContratadaTipoFab[0];
                     A509ContagemrResultado_SistemaSigla = P00W45_A509ContagemrResultado_SistemaSigla[0];
                     n509ContagemrResultado_SistemaSigla = P00W45_n509ContagemrResultado_SistemaSigla[0];
                     if ( (AV52Contrato_Codigos.IndexOf(A1603ContagemResultado_CntCod)>0) )
                     {
                        if ( (AV17Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
                        {
                           if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
                           {
                              AV85PFB = A460ContagemResultado_PFBFM;
                           }
                           else if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
                           {
                              AV85PFB = A458ContagemResultado_PFBFS;
                           }
                           AV84Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
                           AV90ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                           /* Execute user subroutine: 'MONTARRESUMO' */
                           S121 ();
                           if ( returnInSub )
                           {
                              pr_default.close(3);
                              this.cleanup();
                              if (true) return;
                           }
                        }
                     }
                     BRKW47 = true;
                     pr_default.readNext(3);
                  }
                  A52Contratada_AreaTrabalhoCod = W52Contratada_AreaTrabalhoCod;
                  n52Contratada_AreaTrabalhoCod = false;
                  if ( ! BRKW47 )
                  {
                     BRKW47 = true;
                     pr_default.readNext(3);
                  }
               }
               pr_default.close(3);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HW40( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FILTROS' Routine */
         if ( (0==AV18Colaborador) )
         {
            AV19Colaborador_Nome = "Todos";
         }
         else
         {
            /* Using cursor P00W46 */
            pr_default.execute(4, new Object[] {AV18Colaborador});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A57Usuario_PessoaCod = P00W46_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = P00W46_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = P00W46_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00W46_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = P00W46_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00W46_n58Usuario_PessoaNom[0];
               AV19Colaborador_Nome = A58Usuario_PessoaNom;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
         }
      }

      protected void S121( )
      {
         /* 'MONTARRESUMO' Routine */
         AV76FileName = "";
         AV67SEA = 0;
         AV65SEB = 0;
         AV66SEM = 0;
         AV64EEA = 0;
         AV62EEB = 0;
         AV63EEM = 0;
         AV70CEA = 0;
         AV68CEB = 0;
         AV69CEM = 0;
         AV61AIEA = 0;
         AV59AIEB = 0;
         AV60AIEM = 0;
         AV73ALIA = 0;
         AV71ALIB = 0;
         AV72ALIM = 0;
         AV91Upload_Date = (DateTime)(DateTime.MinValue);
         /* Using cursor P00W47 */
         pr_default.execute(5, new Object[] {AV90ContagemResultado_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A590ContagemResultadoEvidencia_TipoArq = P00W47_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = P00W47_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A456ContagemResultado_Codigo = P00W47_A456ContagemResultado_Codigo[0];
            A589ContagemResultadoEvidencia_NomeArq = P00W47_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = P00W47_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A591ContagemResultadoEvidencia_Data = P00W47_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = P00W47_n591ContagemResultadoEvidencia_Data[0];
            A586ContagemResultadoEvidencia_Codigo = P00W47_A586ContagemResultadoEvidencia_Codigo[0];
            A588ContagemResultadoEvidencia_Arquivo = P00W47_A588ContagemResultadoEvidencia_Arquivo[0];
            n588ContagemResultadoEvidencia_Arquivo = P00W47_n588ContagemResultadoEvidencia_Arquivo[0];
            AV75Planilha = A588ContagemResultadoEvidencia_Arquivo;
            AV76FileName = StringUtil.Trim( A589ContagemResultadoEvidencia_NomeArq) + "." + A590ContagemResultadoEvidencia_TipoArq;
            AV91Upload_Date = A591ContagemResultadoEvidencia_Data;
            pr_default.readNext(5);
         }
         pr_default.close(5);
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              AV91Upload_Date ,
                                              A1103Anexo_Data ,
                                              A1108Anexo_TipoArq ,
                                              A1110AnexoDe_Tabela ,
                                              AV90ContagemResultado_Codigo ,
                                              A1109AnexoDe_Id },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor P00W48 */
         pr_default.execute(6, new Object[] {AV90ContagemResultado_Codigo, AV91Upload_Date});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1106Anexo_Codigo = P00W48_A1106Anexo_Codigo[0];
            A1103Anexo_Data = P00W48_A1103Anexo_Data[0];
            A1108Anexo_TipoArq = P00W48_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = P00W48_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1110AnexoDe_Tabela = P00W48_A1110AnexoDe_Tabela[0];
            A1109AnexoDe_Id = P00W48_A1109AnexoDe_Id[0];
            A1107Anexo_NomeArq = P00W48_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = P00W48_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A1101Anexo_Arquivo = P00W48_A1101Anexo_Arquivo[0];
            n1101Anexo_Arquivo = P00W48_n1101Anexo_Arquivo[0];
            A1103Anexo_Data = P00W48_A1103Anexo_Data[0];
            A1108Anexo_TipoArq = P00W48_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = P00W48_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = P00W48_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = P00W48_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A1101Anexo_Arquivo = P00W48_A1101Anexo_Arquivo[0];
            n1101Anexo_Arquivo = P00W48_n1101Anexo_Arquivo[0];
            AV75Planilha = A1101Anexo_Arquivo;
            AV76FileName = StringUtil.Trim( A1107Anexo_NomeArq) + "." + A1108Anexo_TipoArq;
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76FileName)) )
         {
            AV77ExcelDocument.Open(AV75Planilha);
            AV81Campo = "ALIB";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV71ALIB = AV80Valor;
            AV81Campo = "ALIM";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV72ALIM = AV80Valor;
            AV81Campo = "ALIA";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV73ALIA = AV80Valor;
            AV81Campo = "AIEB";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV59AIEB = AV80Valor;
            AV81Campo = "AIEM";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV60AIEM = AV80Valor;
            AV81Campo = "AIEA";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV61AIEA = AV80Valor;
            AV81Campo = "EEB";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV62EEB = AV80Valor;
            AV81Campo = "EEM";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV63EEM = AV80Valor;
            AV81Campo = "EEA";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV64EEA = AV80Valor;
            AV81Campo = "SEB";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV65SEB = AV80Valor;
            AV81Campo = "SEM";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV66SEM = AV80Valor;
            AV81Campo = "SEA";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV67SEA = AV80Valor;
            AV81Campo = "CEB";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV68CEB = AV80Valor;
            AV81Campo = "CEM";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV69CEM = AV80Valor;
            AV81Campo = "CEA";
            /* Execute user subroutine: 'GETVALOR' */
            S131 ();
            if (returnInSub) return;
            AV70CEA = AV80Valor;
            AV77ExcelDocument.Close();
            HW40( false, 1014) ;
            getPrinter().GxDrawRect(17, Gx_line+700, 484, Gx_line+900, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(250, Gx_line+700, 250, Gx_line+900, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(133, Gx_line+700, 133, Gx_line+900, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(367, Gx_line+700, 367, Gx_line+900, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+733, 484, Gx_line+733, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+767, 484, Gx_line+767, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+800, 484, Gx_line+800, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+867, 484, Gx_line+867, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+833, 484, Gx_line+833, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Enter the total number of occurrences for each function type by complexity.", 17, Gx_line+677, 459, Gx_line+695, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Function Point Count Summary Form", 25, Gx_line+67, 300, Gx_line+87, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Applicant�s Name: ", 25, Gx_line+150, 158, Gx_line+170, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Count Date:", 25, Gx_line+183, 125, Gx_line+203, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Type of Count (Project or Application):", 25, Gx_line+217, 300, Gx_line+237, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Scope of Count:", 25, Gx_line+367, 150, Gx_line+387, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Purpose of Count:", 25, Gx_line+250, 167, Gx_line+270, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Client or Manager Phone Number:", 17, Gx_line+550, 259, Gx_line+570, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Client or Manager E-Mail Address:", 17, Gx_line+567, 259, Gx_line+587, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Client or Manager Mailing Address:", 17, Gx_line+583, 267, Gx_line+603, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Client or Manager Name:", 17, Gx_line+533, 259, Gx_line+553, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Source File Name: ", 25, Gx_line+117, 167, Gx_line+137, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("List All Other CFPS Counters:", 17, Gx_line+483, 259, Gx_line+503, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Presta��o de servi�o especializado de contagem de pontos de fun��o, conforme estabelecido em Contrato.", 292, Gx_line+250, 792, Gx_line+350, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText("Contar os servi�os prestados de desenvolvimento pela F�brica de Software, conforme descritos nesta Ordem de Servi�o.", 292, Gx_line+367, 792, Gx_line+467, 0+16, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Function Point Count Summary", 17, Gx_line+650, 284, Gx_line+674, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Application for Certification Extension Credit � Perform a Function", 25, Gx_line+0, 792, Gx_line+28, 0, 0, 0, 1) ;
            getPrinter().GxDrawText("Point Count", 25, Gx_line+28, 575, Gx_line+56, 0, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV59AIEB, "ZZ,ZZZ,ZZ9.999")), 142, Gx_line+744, 231, Gx_line+759, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV60AIEM, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+744, 347, Gx_line+759, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV61AIEA, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+744, 464, Gx_line+759, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV62EEB, "ZZ,ZZZ,ZZ9.999")), 142, Gx_line+810, 231, Gx_line+825, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV63EEM, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+810, 347, Gx_line+825, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV64EEA, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+810, 464, Gx_line+825, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV65SEB, "ZZ,ZZZ,ZZ9.999")), 142, Gx_line+844, 231, Gx_line+859, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV66SEM, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+844, 347, Gx_line+859, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV67SEA, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+844, 464, Gx_line+859, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV68CEB, "ZZ,ZZZ,ZZ9.999")), 142, Gx_line+879, 231, Gx_line+896, 2, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV69CEM, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+879, 347, Gx_line+896, 2, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV70CEA, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+879, 464, Gx_line+896, 2, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV71ALIB, "ZZ,ZZZ,ZZ9.999")), 142, Gx_line+777, 231, Gx_line+792, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV72ALIM, "ZZ,ZZZ,ZZ9.999")), 258, Gx_line+777, 347, Gx_line+792, 2+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV73ALIA, "ZZ,ZZZ,ZZ9.999")), 375, Gx_line+777, 464, Gx_line+792, 2+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV23Contratante_RazaoSocial, "@!")), 292, Gx_line+533, 634, Gx_line+551, 0, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV58Tipo, "")), 317, Gx_line+217, 443, Gx_line+235, 0+256, 0, 0, 1) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV57DataCnt, "99/99/99"), 167, Gx_line+183, 220, Gx_line+201, 0+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV76FileName, "")), 175, Gx_line+117, 808, Gx_line+135, 0, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84Sistema_Sigla, "@!")), 167, Gx_line+150, 324, Gx_line+168, 0+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV85PFB, "ZZ,ZZZ,ZZ9.999")), 292, Gx_line+917, 395, Gx_line+935, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV85PFB, "ZZ,ZZZ,ZZ9.999")), 292, Gx_line+983, 395, Gx_line+1001, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("1", 383, Gx_line+950, 391, Gx_line+967, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV87Contratante_Telefone, "")), 292, Gx_line+550, 418, Gx_line+568, 0+256, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88Contratante_Email, "")), 292, Gx_line+567, 809, Gx_line+585, 0, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV89Contratante_Endereco, "")), 292, Gx_line+583, 809, Gx_line+633, 0+16, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV19Colaborador_Nome, "@!")), 292, Gx_line+483, 606, Gx_line+501, 0+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Low", 175, Gx_line+708, 206, Gx_line+725, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Average", 283, Gx_line+708, 345, Gx_line+725, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("High", 408, Gx_line+708, 443, Gx_line+725, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("ILFs", 58, Gx_line+743, 92, Gx_line+760, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("EIFs", 58, Gx_line+776, 94, Gx_line+793, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("EIs", 58, Gx_line+809, 84, Gx_line+826, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("EOs", 58, Gx_line+843, 91, Gx_line+860, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("EQs", 58, Gx_line+876, 91, Gx_line+900, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Total Unadjusted Function Points:", 17, Gx_line+917, 267, Gx_line+934, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Value Adjustment Factor:", 17, Gx_line+950, 206, Gx_line+967, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Total Adjusted Function Points:", 17, Gx_line+983, 247, Gx_line+1000, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+1014);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
         }
      }

      protected void S131( )
      {
         /* 'GETVALOR' Routine */
         AV82Aba = "";
         AV78row = 0;
         AV79col = 0;
         AV106GXLvl198 = 0;
         /* Using cursor P00W49 */
         pr_default.execute(7, new Object[] {AV13AreaTrabalho_Codigo, AV81Campo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A849ParametrosPln_Campo = P00W49_A849ParametrosPln_Campo[0];
            A847ParametrosPln_AreaTrabalhoCod = P00W49_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00W49_n847ParametrosPln_AreaTrabalhoCod[0];
            A2015ParametrosPln_Aba = P00W49_A2015ParametrosPln_Aba[0];
            A851ParametrosPln_Linha = P00W49_A851ParametrosPln_Linha[0];
            A850ParametrosPln_Coluna = P00W49_A850ParametrosPln_Coluna[0];
            A848ParametrosPln_Codigo = P00W49_A848ParametrosPln_Codigo[0];
            AV106GXLvl198 = 1;
            AV82Aba = StringUtil.Trim( A2015ParametrosPln_Aba);
            AV78row = (short)(A851ParametrosPln_Linha);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))) )
            {
               AV79col = (short)(StringUtil.Asc( A850ParametrosPln_Coluna)-64);
            }
            else
            {
               AV79col = (short)(StringUtil.Asc( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))-64+26);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(7);
         }
         pr_default.close(7);
         if ( AV106GXLvl198 == 0 )
         {
            /* Using cursor P00W410 */
            pr_default.execute(8, new Object[] {AV81Campo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A849ParametrosPln_Campo = P00W410_A849ParametrosPln_Campo[0];
               A847ParametrosPln_AreaTrabalhoCod = P00W410_A847ParametrosPln_AreaTrabalhoCod[0];
               n847ParametrosPln_AreaTrabalhoCod = P00W410_n847ParametrosPln_AreaTrabalhoCod[0];
               A2015ParametrosPln_Aba = P00W410_A2015ParametrosPln_Aba[0];
               A851ParametrosPln_Linha = P00W410_A851ParametrosPln_Linha[0];
               A850ParametrosPln_Coluna = P00W410_A850ParametrosPln_Coluna[0];
               A848ParametrosPln_Codigo = P00W410_A848ParametrosPln_Codigo[0];
               AV82Aba = StringUtil.Trim( A2015ParametrosPln_Aba);
               AV78row = (short)(A851ParametrosPln_Linha);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))) )
               {
                  AV79col = (short)(StringUtil.Asc( A850ParametrosPln_Coluna)-64);
               }
               else
               {
                  AV79col = (short)(StringUtil.Asc( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))-64+26);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(8);
            }
            pr_default.close(8);
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV82Aba)) || (0==AV78row) || (0==AV79col) ) )
         {
            AV77ExcelDocument.SelectSheet(AV82Aba);
            AV80Valor = NumberUtil.Val( AV77ExcelDocument.get_Cells(AV78row, AV79col, 1, 1).Value, ".");
            if ( (Convert.ToDecimal(0)==AV80Valor) )
            {
               AV80Valor = (decimal)(AV77ExcelDocument.get_Cells(AV78row, AV79col, 1, 1).Number);
            }
         }
      }

      protected void HW40( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxDrawLine(25, Gx_line+0, 800, Gx_line+0, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Copyright � 2002 International Function Point Users Group.  All rights reserved.", 242, Gx_line+17, 581, Gx_line+31, 0+256, 0, 0, 2) ;
                  getPrinter().GxDrawText("These commodities, technology, or software were exported from the United States in accordance with the Export Administration Regulations.  Diversion contrary to U.S. law is prohibited.  Individuals and corporations are required to comply with all import and export regulations.", 53, Gx_line+33, 770, Gx_line+66, 1+16, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 17, Gx_line+0, 66, Gx_line+15, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 67, Gx_line+0, 160, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 758, Gx_line+0, 797, Gx_line+15, 2+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+66);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawLine(25, Gx_line+17, 800, Gx_line+17, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("IFPUG CFPS Certification Extension Program", 25, Gx_line+0, 303, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+34);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV83SDT_Codigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV48WebSession = context.GetSession();
         AV74Codigo = new SdtSDT_Codigos(context);
         AV17Codigos = new GxSimpleCollection();
         scmdbuf = "";
         P00W42_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00W42_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00W42_A456ContagemResultado_Codigo = new int[1] ;
         P00W42_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00W42_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         AV11Area_Codigos = new GxSimpleCollection();
         P00W43_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W43_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W43_A456ContagemResultado_Codigo = new int[1] ;
         P00W43_A1603ContagemResultado_CntCod = new int[1] ;
         P00W43_n1603ContagemResultado_CntCod = new bool[] {false} ;
         AV52Contrato_Codigos = new GxSimpleCollection();
         P00W44_A29Contratante_Codigo = new int[1] ;
         P00W44_n29Contratante_Codigo = new bool[] {false} ;
         P00W44_A25Municipio_Codigo = new int[1] ;
         P00W44_n25Municipio_Codigo = new bool[] {false} ;
         P00W44_A335Contratante_PessoaCod = new int[1] ;
         P00W44_A5AreaTrabalho_Codigo = new int[1] ;
         P00W44_A9Contratante_RazaoSocial = new String[] {""} ;
         P00W44_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00W44_A31Contratante_Telefone = new String[] {""} ;
         P00W44_A14Contratante_Email = new String[] {""} ;
         P00W44_n14Contratante_Email = new bool[] {false} ;
         P00W44_A521Pessoa_CEP = new String[] {""} ;
         P00W44_n521Pessoa_CEP = new bool[] {false} ;
         P00W44_A23Estado_UF = new String[] {""} ;
         P00W44_A26Municipio_Nome = new String[] {""} ;
         P00W44_A519Pessoa_Endereco = new String[] {""} ;
         P00W44_n519Pessoa_Endereco = new bool[] {false} ;
         P00W44_A6AreaTrabalho_Descricao = new String[] {""} ;
         A9Contratante_RazaoSocial = "";
         A31Contratante_Telefone = "";
         A14Contratante_Email = "";
         A521Pessoa_CEP = "";
         A23Estado_UF = "";
         A26Municipio_Nome = "";
         A519Pessoa_Endereco = "";
         A6AreaTrabalho_Descricao = "";
         AV14AreaTrabalho_Descricao = "";
         AV23Contratante_RazaoSocial = "";
         AV87Contratante_Telefone = "";
         AV88Contratante_Email = "";
         AV89Contratante_Endereco = "";
         P00W45_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00W45_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00W45_A489ContagemResultado_SistemaCod = new int[1] ;
         P00W45_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00W45_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W45_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W45_A1603ContagemResultado_CntCod = new int[1] ;
         P00W45_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00W45_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00W45_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00W45_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00W45_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00W45_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00W45_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00W45_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00W45_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00W45_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00W45_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00W45_A456ContagemResultado_Codigo = new int[1] ;
         P00W45_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00W45_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00W45_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00W45_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00W45_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         A509ContagemrResultado_SistemaSigla = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV58Tipo = "";
         AV57DataCnt = DateTime.MinValue;
         AV84Sistema_Sigla = "";
         AV19Colaborador_Nome = "";
         P00W46_A57Usuario_PessoaCod = new int[1] ;
         P00W46_A1Usuario_Codigo = new int[1] ;
         P00W46_A58Usuario_PessoaNom = new String[] {""} ;
         P00W46_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         AV76FileName = "";
         AV91Upload_Date = (DateTime)(DateTime.MinValue);
         P00W47_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00W47_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00W47_A456ContagemResultado_Codigo = new int[1] ;
         P00W47_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00W47_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00W47_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00W47_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         P00W47_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00W47_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         P00W47_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A590ContagemResultadoEvidencia_TipoArq = "";
         A588ContagemResultadoEvidencia_Arquivo_Filetype = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A588ContagemResultadoEvidencia_Arquivo_Filename = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A588ContagemResultadoEvidencia_Arquivo = "";
         AV75Planilha = "";
         A1103Anexo_Data = (DateTime)(DateTime.MinValue);
         A1108Anexo_TipoArq = "";
         P00W48_A1106Anexo_Codigo = new int[1] ;
         P00W48_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00W48_A1108Anexo_TipoArq = new String[] {""} ;
         P00W48_n1108Anexo_TipoArq = new bool[] {false} ;
         P00W48_A1110AnexoDe_Tabela = new int[1] ;
         P00W48_A1109AnexoDe_Id = new int[1] ;
         P00W48_A1107Anexo_NomeArq = new String[] {""} ;
         P00W48_n1107Anexo_NomeArq = new bool[] {false} ;
         P00W48_A1101Anexo_Arquivo = new String[] {""} ;
         P00W48_n1101Anexo_Arquivo = new bool[] {false} ;
         A1101Anexo_Arquivo_Filetype = "";
         A1107Anexo_NomeArq = "";
         A1101Anexo_Arquivo_Filename = "";
         A1101Anexo_Arquivo = "";
         AV77ExcelDocument = new ExcelDocumentI();
         AV81Campo = "";
         AV82Aba = "";
         P00W49_A849ParametrosPln_Campo = new String[] {""} ;
         P00W49_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00W49_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00W49_A2015ParametrosPln_Aba = new String[] {""} ;
         P00W49_A851ParametrosPln_Linha = new int[1] ;
         P00W49_A850ParametrosPln_Coluna = new String[] {""} ;
         P00W49_A848ParametrosPln_Codigo = new int[1] ;
         A849ParametrosPln_Campo = "";
         A2015ParametrosPln_Aba = "";
         A850ParametrosPln_Coluna = "";
         P00W410_A849ParametrosPln_Campo = new String[] {""} ;
         P00W410_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00W410_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00W410_A2015ParametrosPln_Aba = new String[] {""} ;
         P00W410_A851ParametrosPln_Linha = new int[1] ;
         P00W410_A850ParametrosPln_Coluna = new String[] {""} ;
         P00W410_A848ParametrosPln_Codigo = new int[1] ;
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_ifpugsummary__default(),
            new Object[][] {
                new Object[] {
               P00W42_A490ContagemResultado_ContratadaCod, P00W42_n490ContagemResultado_ContratadaCod, P00W42_A456ContagemResultado_Codigo, P00W42_A52Contratada_AreaTrabalhoCod, P00W42_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P00W43_A1553ContagemResultado_CntSrvCod, P00W43_n1553ContagemResultado_CntSrvCod, P00W43_A456ContagemResultado_Codigo, P00W43_A1603ContagemResultado_CntCod, P00W43_n1603ContagemResultado_CntCod
               }
               , new Object[] {
               P00W44_A29Contratante_Codigo, P00W44_n29Contratante_Codigo, P00W44_A25Municipio_Codigo, P00W44_n25Municipio_Codigo, P00W44_A335Contratante_PessoaCod, P00W44_A5AreaTrabalho_Codigo, P00W44_A9Contratante_RazaoSocial, P00W44_n9Contratante_RazaoSocial, P00W44_A31Contratante_Telefone, P00W44_A14Contratante_Email,
               P00W44_n14Contratante_Email, P00W44_A521Pessoa_CEP, P00W44_n521Pessoa_CEP, P00W44_A23Estado_UF, P00W44_A26Municipio_Nome, P00W44_A519Pessoa_Endereco, P00W44_n519Pessoa_Endereco, P00W44_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00W45_A490ContagemResultado_ContratadaCod, P00W45_n490ContagemResultado_ContratadaCod, P00W45_A489ContagemResultado_SistemaCod, P00W45_n489ContagemResultado_SistemaCod, P00W45_A1553ContagemResultado_CntSrvCod, P00W45_n1553ContagemResultado_CntSrvCod, P00W45_A1603ContagemResultado_CntCod, P00W45_n1603ContagemResultado_CntCod, P00W45_A52Contratada_AreaTrabalhoCod, P00W45_n52Contratada_AreaTrabalhoCod,
               P00W45_A1326ContagemResultado_ContratadaTipoFab, P00W45_n1326ContagemResultado_ContratadaTipoFab, P00W45_A460ContagemResultado_PFBFM, P00W45_n460ContagemResultado_PFBFM, P00W45_A458ContagemResultado_PFBFS, P00W45_n458ContagemResultado_PFBFS, P00W45_A509ContagemrResultado_SistemaSigla, P00W45_n509ContagemrResultado_SistemaSigla, P00W45_A456ContagemResultado_Codigo, P00W45_A517ContagemResultado_Ultima,
               P00W45_A598ContagemResultado_Baseline, P00W45_n598ContagemResultado_Baseline, P00W45_A473ContagemResultado_DataCnt, P00W45_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P00W46_A57Usuario_PessoaCod, P00W46_A1Usuario_Codigo, P00W46_A58Usuario_PessoaNom, P00W46_n58Usuario_PessoaNom
               }
               , new Object[] {
               P00W47_A590ContagemResultadoEvidencia_TipoArq, P00W47_n590ContagemResultadoEvidencia_TipoArq, P00W47_A456ContagemResultado_Codigo, P00W47_A589ContagemResultadoEvidencia_NomeArq, P00W47_n589ContagemResultadoEvidencia_NomeArq, P00W47_A591ContagemResultadoEvidencia_Data, P00W47_n591ContagemResultadoEvidencia_Data, P00W47_A586ContagemResultadoEvidencia_Codigo, P00W47_A588ContagemResultadoEvidencia_Arquivo, P00W47_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               P00W48_A1106Anexo_Codigo, P00W48_A1103Anexo_Data, P00W48_A1108Anexo_TipoArq, P00W48_n1108Anexo_TipoArq, P00W48_A1110AnexoDe_Tabela, P00W48_A1109AnexoDe_Id, P00W48_A1107Anexo_NomeArq, P00W48_n1107Anexo_NomeArq, P00W48_A1101Anexo_Arquivo, P00W48_n1101Anexo_Arquivo
               }
               , new Object[] {
               P00W49_A849ParametrosPln_Campo, P00W49_A847ParametrosPln_AreaTrabalhoCod, P00W49_n847ParametrosPln_AreaTrabalhoCod, P00W49_A2015ParametrosPln_Aba, P00W49_A851ParametrosPln_Linha, P00W49_A850ParametrosPln_Coluna, P00W49_A848ParametrosPln_Codigo
               }
               , new Object[] {
               P00W410_A849ParametrosPln_Campo, P00W410_A847ParametrosPln_AreaTrabalhoCod, P00W410_n847ParametrosPln_AreaTrabalhoCod, P00W410_A2015ParametrosPln_Aba, P00W410_A851ParametrosPln_Linha, P00W410_A850ParametrosPln_Coluna, P00W410_A848ParametrosPln_Codigo
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV8Ano ;
      private short GxWebError ;
      private short AV78row ;
      private short AV79col ;
      private short AV106GXLvl198 ;
      private int AV21Contratada_Codigo ;
      private int AV9Area ;
      private int AV18Colaborador ;
      private int AV43Servico ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV94GXV1 ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int AV13AreaTrabalho_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int W52Contratada_AreaTrabalhoCod ;
      private int AV90ContagemResultado_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A1110AnexoDe_Tabela ;
      private int A1109AnexoDe_Id ;
      private int A1106Anexo_Codigo ;
      private int Gx_OldLine ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int A851ParametrosPln_Linha ;
      private int A848ParametrosPln_Codigo ;
      private long AV33Mes ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal AV85PFB ;
      private decimal AV67SEA ;
      private decimal AV65SEB ;
      private decimal AV66SEM ;
      private decimal AV64EEA ;
      private decimal AV62EEB ;
      private decimal AV63EEM ;
      private decimal AV70CEA ;
      private decimal AV68CEB ;
      private decimal AV69CEM ;
      private decimal AV61AIEA ;
      private decimal AV59AIEB ;
      private decimal AV60AIEM ;
      private decimal AV73ALIA ;
      private decimal AV71ALIB ;
      private decimal AV72ALIM ;
      private decimal AV80Valor ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV46User_Nome ;
      private String scmdbuf ;
      private String A9Contratante_RazaoSocial ;
      private String A31Contratante_Telefone ;
      private String A521Pessoa_CEP ;
      private String A23Estado_UF ;
      private String A26Municipio_Nome ;
      private String AV23Contratante_RazaoSocial ;
      private String AV87Contratante_Telefone ;
      private String AV89Contratante_Endereco ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV58Tipo ;
      private String AV84Sistema_Sigla ;
      private String AV19Colaborador_Nome ;
      private String A58Usuario_PessoaNom ;
      private String AV76FileName ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filetype ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filename ;
      private String A1108Anexo_TipoArq ;
      private String A1101Anexo_Arquivo_Filetype ;
      private String A1107Anexo_NomeArq ;
      private String A1101Anexo_Arquivo_Filename ;
      private String AV82Aba ;
      private String A2015ParametrosPln_Aba ;
      private String A850ParametrosPln_Coluna ;
      private String Gx_time ;
      private DateTime AV91Upload_Date ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime A1103Anexo_Data ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV57DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool n9Contratante_RazaoSocial ;
      private bool n14Contratante_Email ;
      private bool n521Pessoa_CEP ;
      private bool n519Pessoa_Endereco ;
      private bool A517ContagemResultado_Ultima ;
      private bool BRKW47 ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n58Usuario_PessoaNom ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n1108Anexo_TipoArq ;
      private bool n1107Anexo_NomeArq ;
      private bool n1101Anexo_Arquivo ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private String A14Contratante_Email ;
      private String A519Pessoa_Endereco ;
      private String A6AreaTrabalho_Descricao ;
      private String AV14AreaTrabalho_Descricao ;
      private String AV88Contratante_Email ;
      private String AV81Campo ;
      private String A849ParametrosPln_Campo ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private String AV75Planilha ;
      private String A1101Anexo_Arquivo ;
      private IGxSession AV48WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00W42_A490ContagemResultado_ContratadaCod ;
      private bool[] P00W42_n490ContagemResultado_ContratadaCod ;
      private int[] P00W42_A456ContagemResultado_Codigo ;
      private int[] P00W42_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00W42_n52Contratada_AreaTrabalhoCod ;
      private int[] P00W43_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W43_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W43_A456ContagemResultado_Codigo ;
      private int[] P00W43_A1603ContagemResultado_CntCod ;
      private bool[] P00W43_n1603ContagemResultado_CntCod ;
      private int[] P00W44_A29Contratante_Codigo ;
      private bool[] P00W44_n29Contratante_Codigo ;
      private int[] P00W44_A25Municipio_Codigo ;
      private bool[] P00W44_n25Municipio_Codigo ;
      private int[] P00W44_A335Contratante_PessoaCod ;
      private int[] P00W44_A5AreaTrabalho_Codigo ;
      private String[] P00W44_A9Contratante_RazaoSocial ;
      private bool[] P00W44_n9Contratante_RazaoSocial ;
      private String[] P00W44_A31Contratante_Telefone ;
      private String[] P00W44_A14Contratante_Email ;
      private bool[] P00W44_n14Contratante_Email ;
      private String[] P00W44_A521Pessoa_CEP ;
      private bool[] P00W44_n521Pessoa_CEP ;
      private String[] P00W44_A23Estado_UF ;
      private String[] P00W44_A26Municipio_Nome ;
      private String[] P00W44_A519Pessoa_Endereco ;
      private bool[] P00W44_n519Pessoa_Endereco ;
      private String[] P00W44_A6AreaTrabalho_Descricao ;
      private int[] P00W45_A490ContagemResultado_ContratadaCod ;
      private bool[] P00W45_n490ContagemResultado_ContratadaCod ;
      private int[] P00W45_A489ContagemResultado_SistemaCod ;
      private bool[] P00W45_n489ContagemResultado_SistemaCod ;
      private int[] P00W45_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W45_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W45_A1603ContagemResultado_CntCod ;
      private bool[] P00W45_n1603ContagemResultado_CntCod ;
      private int[] P00W45_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00W45_n52Contratada_AreaTrabalhoCod ;
      private String[] P00W45_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00W45_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00W45_A460ContagemResultado_PFBFM ;
      private bool[] P00W45_n460ContagemResultado_PFBFM ;
      private decimal[] P00W45_A458ContagemResultado_PFBFS ;
      private bool[] P00W45_n458ContagemResultado_PFBFS ;
      private String[] P00W45_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00W45_n509ContagemrResultado_SistemaSigla ;
      private int[] P00W45_A456ContagemResultado_Codigo ;
      private bool[] P00W45_A517ContagemResultado_Ultima ;
      private bool[] P00W45_A598ContagemResultado_Baseline ;
      private bool[] P00W45_n598ContagemResultado_Baseline ;
      private DateTime[] P00W45_A473ContagemResultado_DataCnt ;
      private String[] P00W45_A511ContagemResultado_HoraCnt ;
      private int[] P00W46_A57Usuario_PessoaCod ;
      private int[] P00W46_A1Usuario_Codigo ;
      private String[] P00W46_A58Usuario_PessoaNom ;
      private bool[] P00W46_n58Usuario_PessoaNom ;
      private String[] P00W47_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00W47_n590ContagemResultadoEvidencia_TipoArq ;
      private int[] P00W47_A456ContagemResultado_Codigo ;
      private String[] P00W47_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00W47_n589ContagemResultadoEvidencia_NomeArq ;
      private DateTime[] P00W47_A591ContagemResultadoEvidencia_Data ;
      private bool[] P00W47_n591ContagemResultadoEvidencia_Data ;
      private int[] P00W47_A586ContagemResultadoEvidencia_Codigo ;
      private String[] P00W47_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] P00W47_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] P00W48_A1106Anexo_Codigo ;
      private DateTime[] P00W48_A1103Anexo_Data ;
      private String[] P00W48_A1108Anexo_TipoArq ;
      private bool[] P00W48_n1108Anexo_TipoArq ;
      private int[] P00W48_A1110AnexoDe_Tabela ;
      private int[] P00W48_A1109AnexoDe_Id ;
      private String[] P00W48_A1107Anexo_NomeArq ;
      private bool[] P00W48_n1107Anexo_NomeArq ;
      private String[] P00W48_A1101Anexo_Arquivo ;
      private bool[] P00W48_n1101Anexo_Arquivo ;
      private String[] P00W49_A849ParametrosPln_Campo ;
      private int[] P00W49_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00W49_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] P00W49_A2015ParametrosPln_Aba ;
      private int[] P00W49_A851ParametrosPln_Linha ;
      private String[] P00W49_A850ParametrosPln_Coluna ;
      private int[] P00W49_A848ParametrosPln_Codigo ;
      private String[] P00W410_A849ParametrosPln_Campo ;
      private int[] P00W410_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00W410_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] P00W410_A2015ParametrosPln_Aba ;
      private int[] P00W410_A851ParametrosPln_Linha ;
      private String[] P00W410_A850ParametrosPln_Coluna ;
      private int[] P00W410_A848ParametrosPln_Codigo ;
      private ExcelDocumentI AV77ExcelDocument ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV11Area_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV52Contrato_Codigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV83SDT_Codigos ;
      private SdtSDT_Codigos AV74Codigo ;
   }

   public class arel_ifpugsummary__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00W42( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV17Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_ContratadaCod], NULL AS [ContagemResultado_Codigo], [Contratada_AreaTrabalhoCod] FROM ( SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      protected Object[] conditional_P00W43( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV17Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_CntSrvCod], NULL AS [ContagemResultado_Codigo], [ContagemResultado_CntCod] FROM ( SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00W44( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV11Area_Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T2.[Contratante_Telefone], T2.[Contratante_Email], T4.[Pessoa_CEP], T3.[Estado_UF], T3.[Municipio_Nome], T4.[Pessoa_Endereco], T1.[AreaTrabalho_Descricao] FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Area_Codigos, "T1.[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AreaTrabalho_Descricao]";
         GXv_Object5[0] = scmdbuf;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00W45( IGxContext context ,
                                             int A1603ContagemResultado_CntCod ,
                                             IGxCollection AV52Contrato_Codigos ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV17Codigos ,
                                             bool A517ContagemResultado_Ultima ,
                                             int A5AreaTrabalho_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [1] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_AreaTrabalhoCod], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFBFS], T4.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_Baseline], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV52Contrato_Codigos, "T5.[Contrato_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T5.[Contrato_Codigo], T1.[ContagemResultado_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00W48( IGxContext context ,
                                             DateTime AV91Upload_Date ,
                                             DateTime A1103Anexo_Data ,
                                             String A1108Anexo_TipoArq ,
                                             int A1110AnexoDe_Tabela ,
                                             int AV90ContagemResultado_Codigo ,
                                             int A1109AnexoDe_Id )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [2] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Anexo_Codigo], T2.[Anexo_Data], T2.[Anexo_TipoArq], T1.[AnexoDe_Tabela], T1.[AnexoDe_Id], T2.[Anexo_NomeArq], T2.[Anexo_Arquivo] FROM ([AnexosDe] T1 WITH (NOLOCK) INNER JOIN [Anexos] T2 WITH (NOLOCK) ON T2.[Anexo_Codigo] = T1.[Anexo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[AnexoDe_Id] = @AV90ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and (LOWER(RTRIM(LTRIM(T2.[Anexo_TipoArq]))) = 'xlsx')";
         scmdbuf = scmdbuf + " and (T1.[AnexoDe_Tabela] = 1)";
         if ( ! (DateTime.MinValue==AV91Upload_Date) )
         {
            sWhereString = sWhereString + " and (T2.[Anexo_Data] > @AV91Upload_Date)";
         }
         else
         {
            GXv_int9[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AnexoDe_Id]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00W42(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 1 :
                     return conditional_P00W43(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 2 :
                     return conditional_P00W44(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 3 :
                     return conditional_P00W45(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (bool)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
               case 6 :
                     return conditional_P00W48(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W46 ;
          prmP00W46 = new Object[] {
          new Object[] {"@AV18Colaborador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W47 ;
          prmP00W47 = new Object[] {
          new Object[] {"@AV90ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W49 ;
          prmP00W49 = new Object[] {
          new Object[] {"@AV13AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV81Campo",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00W410 ;
          prmP00W410 = new Object[] {
          new Object[] {"@AV81Campo",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00W42 ;
          prmP00W42 = new Object[] {
          } ;
          Object[] prmP00W43 ;
          prmP00W43 = new Object[] {
          } ;
          Object[] prmP00W44 ;
          prmP00W44 = new Object[] {
          } ;
          Object[] prmP00W45 ;
          prmP00W45 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W48 ;
          prmP00W48 = new Object[] {
          new Object[] {"@AV90ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV91Upload_Date",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W42,100,0,false,false )
             ,new CursorDef("P00W43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W43,100,0,false,false )
             ,new CursorDef("P00W44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W44,100,0,true,false )
             ,new CursorDef("P00W45", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W45,100,0,true,false )
             ,new CursorDef("P00W46", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV18Colaborador ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W46,1,0,false,true )
             ,new CursorDef("P00W47", "SELECT [ContagemResultadoEvidencia_TipoArq], [ContagemResultado_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @AV90ContagemResultado_Codigo) AND (LOWER(RTRIM(LTRIM([ContagemResultadoEvidencia_TipoArq]))) = 'xlsx') ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W47,100,0,false,false )
             ,new CursorDef("P00W48", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W48,100,0,false,false )
             ,new CursorDef("P00W49", "SELECT TOP 1 [ParametrosPln_Campo], [ParametrosPln_AreaTrabalhoCod], [ParametrosPln_Aba], [ParametrosPln_Linha], [ParametrosPln_Coluna], [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE ([ParametrosPln_AreaTrabalhoCod] = @AV13AreaTrabalho_Codigo) AND ([ParametrosPln_Campo] = @AV81Campo) ORDER BY [ParametrosPln_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W49,1,0,false,true )
             ,new CursorDef("P00W410", "SELECT TOP 1 [ParametrosPln_Campo], [ParametrosPln_AreaTrabalhoCod], [ParametrosPln_Aba], [ParametrosPln_Linha], [ParametrosPln_Coluna], [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE ([ParametrosPln_AreaTrabalhoCod] IS NULL or ([ParametrosPln_AreaTrabalhoCod] = convert(int, 0))) AND ([ParametrosPln_Campo] = @AV81Campo) ORDER BY [ParametrosPln_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W410,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 2) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 50) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getVarchar(12) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 25) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.getBool(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(13) ;
                ((String[]) buf[23])[0] = rslt.getString(14, 5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(6, rslt.getString(1, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getBLOBFile(7, rslt.getString(3, 10), rslt.getString(6, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 30) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 30) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[3]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
