/*
               File: Sistema
        Description: Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:12.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SISTEMAVERSAO_CODIGO") == 0 )
         {
            AV7Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Sistema_Codigo), "ZZZZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASISTEMAVERSAO_CODIGO0O25( AV7Sistema_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"AMBIENTETECNOLOGICO_CODIGO") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAAMBIENTETECNOLOGICO_CODIGO0O25( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"METODOLOGIA_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAMETODOLOGIA_CODIGO0O25( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SISTEMA_RESPONSAVEL") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASISTEMA_RESPONSAVEL0O25( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TECNOLOGIA_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATECNOLOGIA_CODIGO0O233( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"SISTEMA_GPOOBJCTRLRSP") == 0 )
         {
            A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2161Sistema_GpoObjCtrlCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASASISTEMA_GPOOBJCTRLRSP0O25( A2161Sistema_GpoObjCtrlCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"SISTEMA_PF") == 0 )
         {
            A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASASISTEMA_PF0O25( A127Sistema_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel29"+"_"+"SISTEMA_FATORAJUSTE") == 0 )
         {
            Gx_BScreen = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
            Gx_mode = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX29ASASISTEMA_FATORAJUSTE0O25( Gx_BScreen, Gx_mode) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_54") == 0 )
         {
            A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n351AmbienteTecnologico_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_54( A351AmbienteTecnologico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_53") == 0 )
         {
            A137Metodologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n137Metodologia_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_53( A137Metodologia_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_58") == 0 )
         {
            A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2161Sistema_GpoObjCtrlCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_58( A2161Sistema_GpoObjCtrlCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_55") == 0 )
         {
            A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_55( A1859SistemaVersao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_56") == 0 )
         {
            A135Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_56( A135Sistema_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_57") == 0 )
         {
            A1399Sistema_ImpUserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1399Sistema_ImpUserCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1399Sistema_ImpUserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1399Sistema_ImpUserCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_57( A1399Sistema_ImpUserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_59") == 0 )
         {
            A1402Sistema_ImpUserPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1402Sistema_ImpUserPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1402Sistema_ImpUserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_59( A1402Sistema_ImpUserPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_61") == 0 )
         {
            A131Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_61( A131Tecnologia_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
         {
            nRC_GXsfl_108 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_108_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_108_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGrid1_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Sistema_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynSistemaVersao_Codigo.Name = "SISTEMAVERSAO_CODIGO";
         dynSistemaVersao_Codigo.WebTags = "";
         dynAmbienteTecnologico_Codigo.Name = "AMBIENTETECNOLOGICO_CODIGO";
         dynAmbienteTecnologico_Codigo.WebTags = "";
         dynMetodologia_Codigo.Name = "METODOLOGIA_CODIGO";
         dynMetodologia_Codigo.WebTags = "";
         cmbSistema_Tipo.Name = "SISTEMA_TIPO";
         cmbSistema_Tipo.WebTags = "";
         cmbSistema_Tipo.addItem("", "(Nenhum)", 0);
         cmbSistema_Tipo.addItem("D", "Desenvolvimento", 0);
         cmbSistema_Tipo.addItem("M", "Melhoria", 0);
         cmbSistema_Tipo.addItem("A", "Aplica��o", 0);
         if ( cmbSistema_Tipo.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A699Sistema_Tipo)) )
            {
               A699Sistema_Tipo = "A";
               n699Sistema_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
            }
            A699Sistema_Tipo = cmbSistema_Tipo.getValidValue(A699Sistema_Tipo);
            n699Sistema_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
         }
         cmbSistema_Tecnica.Name = "SISTEMA_TECNICA";
         cmbSistema_Tecnica.WebTags = "";
         cmbSistema_Tecnica.addItem("", "(Nenhum)", 0);
         cmbSistema_Tecnica.addItem("I", "Indicativa", 0);
         cmbSistema_Tecnica.addItem("E", "Estimada", 0);
         cmbSistema_Tecnica.addItem("D", "Detalhada", 0);
         if ( cmbSistema_Tecnica.ItemCount > 0 )
         {
            A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
            n700Sistema_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
         }
         dynSistema_Responsavel.Name = "SISTEMA_RESPONSAVEL";
         dynSistema_Responsavel.WebTags = "";
         dynSistema_GpoObjCtrlCod.Name = "SISTEMA_GPOOBJCTRLCOD";
         dynSistema_GpoObjCtrlCod.WebTags = "";
         dynSistema_GpoObjCtrlCod.removeAllItems();
         /* Using cursor T000O14 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            dynSistema_GpoObjCtrlCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T000O14_A1826GpoObjCtrl_Codigo[0]), 6, 0)), T000O14_A1827GpoObjCtrl_Nome[0], 0);
            pr_default.readNext(12);
         }
         pr_default.close(12);
         if ( dynSistema_GpoObjCtrlCod.ItemCount > 0 )
         {
            A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( dynSistema_GpoObjCtrlCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0))), "."));
            n2161Sistema_GpoObjCtrlCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         }
         chkSistema_Ativo.Name = "SISTEMA_ATIVO";
         chkSistema_Ativo.WebTags = "";
         chkSistema_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "TitleCaption", chkSistema_Ativo.Caption);
         chkSistema_Ativo.CheckedValue = "false";
         GXCCtl = "TECNOLOGIA_CODIGO_" + sGXsfl_108_idx;
         dynTecnologia_Codigo.Name = GXCCtl;
         dynTecnologia_Codigo.WebTags = "";
         GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_108_idx;
         cmbTecnologia_TipoTecnologia.Name = GXCCtl;
         cmbTecnologia_TipoTecnologia.WebTags = "";
         cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
         cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
         cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
         cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
         cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
         cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
         cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
         cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
         cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
         cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
         if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
         {
            A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
            n355Tecnologia_TipoTecnologia = false;
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSistema_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public sistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public sistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Sistema_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Sistema_Codigo = aP1_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynSistemaVersao_Codigo = new GXCombobox();
         dynAmbienteTecnologico_Codigo = new GXCombobox();
         dynMetodologia_Codigo = new GXCombobox();
         cmbSistema_Tipo = new GXCombobox();
         cmbSistema_Tecnica = new GXCombobox();
         dynSistema_Responsavel = new GXCombobox();
         dynSistema_GpoObjCtrlCod = new GXCombobox();
         chkSistema_Ativo = new GXCheckbox();
         dynTecnologia_Codigo = new GXCombobox();
         cmbTecnologia_TipoTecnologia = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "sistema_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynSistemaVersao_Codigo.ItemCount > 0 )
         {
            A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( dynSistemaVersao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0))), "."));
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         }
         if ( dynAmbienteTecnologico_Codigo.ItemCount > 0 )
         {
            A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( dynAmbienteTecnologico_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0))), "."));
            n351AmbienteTecnologico_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
         if ( dynMetodologia_Codigo.ItemCount > 0 )
         {
            A137Metodologia_Codigo = (int)(NumberUtil.Val( dynMetodologia_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0))), "."));
            n137Metodologia_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
         }
         if ( cmbSistema_Tipo.ItemCount > 0 )
         {
            A699Sistema_Tipo = cmbSistema_Tipo.getValidValue(A699Sistema_Tipo);
            n699Sistema_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
         }
         if ( cmbSistema_Tecnica.ItemCount > 0 )
         {
            A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
            n700Sistema_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
         }
         if ( dynSistema_Responsavel.ItemCount > 0 )
         {
            A1831Sistema_Responsavel = (int)(NumberUtil.Val( dynSistema_Responsavel.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0))), "."));
            n1831Sistema_Responsavel = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1831Sistema_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0)));
         }
         if ( dynSistema_GpoObjCtrlCod.ItemCount > 0 )
         {
            A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( dynSistema_GpoObjCtrlCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0))), "."));
            n2161Sistema_GpoObjCtrlCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), ((edtSistema_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSistema_Codigo_Visible, edtSistema_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Sistema.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_212_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table3_212_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0O25e( true) ;
         }
         else
         {
            wb_table1_2_0O25e( false) ;
         }
      }

      protected void wb_table3_212_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_212_0O25e( true) ;
         }
         else
         {
            wb_table3_212_0O25e( false) ;
         }
      }

      protected void wb_table2_5_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTab1"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Sistema") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"Tab1"+"\" style=\"display:none;\">") ;
            wb_table4_14_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table4_14_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTAB2"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Tecnologias") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TAB2"+"\" style=\"display:none;\">") ;
            wb_table5_105_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table5_105_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTAB3"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Arquivos") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TAB3"+"\" style=\"display:none;\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"Body"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_unnamedtable1_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtable1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table6_121_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table6_121_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table7_194_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table7_194_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_200_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table8_200_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0O25e( true) ;
         }
         else
         {
            wb_table2_5_0O25e( false) ;
         }
      }

      protected void wb_table8_200_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 203,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 205,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 207,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 209,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncopiarcolar_Internalname, "", "Copiar e Colar", bttBtncopiarcolar_Jsonclick, 5, "Copiar e Colar", "", StyleString, ClassString, bttBtncopiarcolar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCOPIARCOLAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_200_0O25e( true) ;
         }
         else
         {
            wb_table8_200_0O25e( false) ;
         }
      }

      protected void wb_table7_194_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 197,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportarexcel_Internalname, "", "Importar de Excel", bttBtnimportarexcel_Jsonclick, 7, "Importar de Excel", "", StyleString, ClassString, bttBtnimportarexcel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e110o25_client"+"'", TempTags, "", 2, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_194_0O25e( true) ;
         }
         else
         {
            wb_table7_194_0O25e( false) ;
         }
      }

      protected void wb_table6_121_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblimportar_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblimportar_Internalname, tblTblimportar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_124_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table9_124_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_135_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table10_135_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_184_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table11_184_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_121_0O25e( true) ;
         }
         else
         {
            wb_table6_121_0O25e( false) ;
         }
      }

      protected void wb_table11_184_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV21FileName), StringUtil.RTrim( context.localUtil.Format( AV21FileName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavFilename_Enabled, 0, "text", "", 80, "chr", 1, "px", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"Center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-Center;text-align:-moz-Center;text-align:-webkit-Center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 191,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportar_Internalname, "", "Importar", bttBtnimportar_Jsonclick, 5, "Importar", "", StyleString, ClassString, bttBtnimportar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOIMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_184_0O25e( true) ;
         }
         else
         {
            wb_table11_184_0O25e( false) ;
         }
      }

      protected void wb_table10_135_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablepralinha_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpralinha_Internalname, "1� linha", "", "", lblTextblockpralinha_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPralinha_Internalname, "Pra Linha", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavPralinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49PraLinha), 4, 0, ",", "")), ((edtavPralinha_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49PraLinha), "ZZZ9")) : context.localUtil.Format( (decimal)(AV49PraLinha), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralinha_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPralinha_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablecolsisnom_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcolsisnom_Internalname, "Colunas: Sistema", "", "", lblTextblockcolsisnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavColsisnom_Internalname, "Col Sis Nom", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavColsisnom_Internalname, StringUtil.RTrim( AV42ColSisNom), StringUtil.RTrim( context.localUtil.Format( AV42ColSisNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColsisnom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavColsisnom_Enabled, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablecolsissgl_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcolsissgl_Internalname, "Sigla", "", "", lblTextblockcolsissgl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavColsissgl_Internalname, "Col Sis Sgl", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavColsissgl_Internalname, StringUtil.RTrim( AV43ColSisSgl), StringUtil.RTrim( context.localUtil.Format( AV43ColSisSgl, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,163);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColsissgl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavColsissgl_Enabled, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablecolmodnom_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcolmodnom_Internalname, "M�dulo", "", "", lblTextblockcolmodnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavColmodnom_Internalname, "Col Mod Nom", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavColmodnom_Internalname, StringUtil.RTrim( AV44ColModNom), StringUtil.RTrim( context.localUtil.Format( AV44ColModNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,172);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColmodnom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavColmodnom_Enabled, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablecolmodsgl_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcolmodsgl_Internalname, "Sigla", "", "", lblTextblockcolmodsgl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavColmodsgl_Internalname, "Col Mod Sgl", "col-sm-3 BootstrapAttributeLabel", 0, true);
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavColmodsgl_Internalname, StringUtil.RTrim( AV45ColModSgl), StringUtil.RTrim( context.localUtil.Format( AV45ColModSgl, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,181);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColmodsgl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavColmodsgl_Enabled, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_135_0O25e( true) ;
         }
         else
         {
            wb_table10_135_0O25e( false) ;
         }
      }

      protected void wb_table9_124_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavBlob_Internalname, "Arquivo", "col-sm-3 BootstrapAttributeLabel", 1, true);
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Blob)) )
            {
               gxblobfileaux.Source = AV48Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV48Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV48Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV48Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV48Blob), context.PathToRelativeUrl( AV48Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV48Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, edtavBlob_Display, edtavBlob_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", "", "", "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavAba_Internalname, "Aba", "col-sm-3 BootstrapAttributeLabel", 1, true);
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'',0)\"";
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavAba_Internalname, StringUtil.RTrim( AV47Aba), StringUtil.RTrim( context.localUtil.Format( AV47Aba, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAba_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavAba_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Sistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_124_0O25e( true) ;
         }
         else
         {
            wb_table9_124_0O25e( false) ;
         }
      }

      protected void wb_table5_105_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable6_Internalname, tblUnnamedtable6_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid1Container.AddObjectProperty("GridName", "Grid1");
            Grid1Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("CmpContext", "");
            Grid1Container.AddObjectProperty("InMasterPage", "false");
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2128SistemaTecnologiaLinha), 4, 0, ".", "")));
            Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0, ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", "")));
            Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0, ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( A355Tecnologia_TipoTecnologia));
            Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0, ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            nGXsfl_108_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount233 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_233 = 1;
                  ScanStart0O233( ) ;
                  while ( RcdFound233 != 0 )
                  {
                     init_level_properties233( ) ;
                     getByPrimaryKey0O233( ) ;
                     AddRow0O233( ) ;
                     ScanNext0O233( ) ;
                  }
                  ScanEnd0O233( ) ;
                  nBlankRcdCount233 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B2130Sistema_Ultimo = A2130Sistema_Ultimo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
               B416Sistema_Nome = A416Sistema_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
               B129Sistema_Sigla = A129Sistema_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               B513Sistema_Coordenacao = A513Sistema_Coordenacao;
               n513Sistema_Coordenacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
               standaloneNotModal0O233( ) ;
               standaloneModal0O233( ) ;
               sMode233 = Gx_mode;
               while ( nGXsfl_108_idx < nRC_GXsfl_108 )
               {
                  ReadRow0O233( ) ;
                  edtSistemaTecnologiaLinha_Enabled = (int)(context.localUtil.CToN( cgiGet( "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaTecnologiaLinha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0)));
                  dynTecnologia_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "TECNOLOGIA_CODIGO_"+sGXsfl_108_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTecnologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0)));
                  cmbTecnologia_TipoTecnologia.Enabled = (int)(context.localUtil.CToN( cgiGet( "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0)));
                  if ( ( nRcdExists_233 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal0O233( ) ;
                  }
                  SendRow0O233( ) ;
               }
               Gx_mode = sMode233;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A2130Sistema_Ultimo = B2130Sistema_Ultimo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
               A416Sistema_Nome = B416Sistema_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
               A129Sistema_Sigla = B129Sistema_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               A513Sistema_Coordenacao = B513Sistema_Coordenacao;
               n513Sistema_Coordenacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount233 = 5;
               nRcdExists_233 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart0O233( ) ;
                  while ( RcdFound233 != 0 )
                  {
                     sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_108233( ) ;
                     init_level_properties233( ) ;
                     standaloneNotModal0O233( ) ;
                     getByPrimaryKey0O233( ) ;
                     standaloneModal0O233( ) ;
                     AddRow0O233( ) ;
                     ScanNext0O233( ) ;
                  }
                  ScanEnd0O233( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode233 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_108233( ) ;
               InitAll0O233( ) ;
               init_level_properties233( ) ;
               B2130Sistema_Ultimo = A2130Sistema_Ultimo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
               B416Sistema_Nome = A416Sistema_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
               B129Sistema_Sigla = A129Sistema_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               B513Sistema_Coordenacao = A513Sistema_Coordenacao;
               n513Sistema_Coordenacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
               standaloneNotModal0O233( ) ;
               standaloneModal0O233( ) ;
               nRcdExists_233 = 0;
               nIsMod_233 = 0;
               nRcdDeleted_233 = 0;
               nBlankRcdCount233 = (short)(nBlankRcdUsr233+nBlankRcdCount233);
               fRowAdded = 0;
               while ( nBlankRcdCount233 > 0 )
               {
                  AddRow0O233( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = dynTecnologia_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount233 = (short)(nBlankRcdCount233-1);
               }
               Gx_mode = sMode233;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A2130Sistema_Ultimo = B2130Sistema_Ultimo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
               A416Sistema_Nome = B416Sistema_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
               A129Sistema_Sigla = B129Sistema_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               A513Sistema_Coordenacao = B513Sistema_Coordenacao;
               n513Sistema_Coordenacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_105_0O25e( true) ;
         }
         else
         {
            wb_table5_105_0O25e( false) ;
         }
      }

      protected void wb_table4_14_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table12_19_0O25( true) ;
         }
         return  ;
      }

      protected void wb_table12_19_0O25e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_0O25e( true) ;
         }
         else
         {
            wb_table4_14_0O25e( false) ;
         }
      }

      protected void wb_table12_19_0O25( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_nome_Internalname, "Nome", "", "", lblTextblocksistema_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Nome_Internalname, A416Sistema_Nome, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Nome_Enabled, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_sigla_Internalname, "Sigla", "", "", lblTextblocksistema_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Sigla_Internalname, StringUtil.RTrim( A129Sistema_Sigla), StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Sigla_Enabled, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_codigo_Internalname, "Vers�o", "", "", lblTextblocksistemaversao_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynSistemaVersao_Codigo, dynSistemaVersao_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)), 1, dynSistemaVersao_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynSistemaVersao_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_Sistema.htm");
            dynSistemaVersao_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistemaVersao_Codigo_Internalname, "Values", (String)(dynSistemaVersao_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_descricao_Internalname, "Descri��o", "", "", lblTextblocksistema_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistema_Descricao_Internalname, A128Sistema_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, 1, edtSistema_Descricao_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_coordenacao_Internalname, "Coordena��o", "", "", lblTextblocksistema_coordenacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Coordenacao_Internalname, A513Sistema_Coordenacao, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Coordenacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Coordenacao_Enabled, 0, "text", "", edtSistema_Coordenacao_Width, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_repositorio_Internalname, "Reposit�rio", "", "", lblTextblocksistema_repositorio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistema_Repositorio_Internalname, A2109Sistema_Repositorio, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, 1, edtSistema_Repositorio_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_codigo_Internalname, "Amb. T�c.", "", "", lblTextblockambientetecnologico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAmbienteTecnologico_Codigo, dynAmbienteTecnologico_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)), 1, dynAmbienteTecnologico_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAmbienteTecnologico_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_Sistema.htm");
            dynAmbienteTecnologico_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAmbienteTecnologico_Codigo_Internalname, "Values", (String)(dynAmbienteTecnologico_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologia_codigo_Internalname, "Met.", "", "", lblTextblockmetodologia_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynMetodologia_Codigo, dynMetodologia_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)), 1, dynMetodologia_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynMetodologia_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_Sistema.htm");
            dynMetodologia_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMetodologia_Codigo_Internalname, "Values", (String)(dynMetodologia_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_tipo_Internalname, "Tipo", "", "", lblTextblocksistema_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSistema_Tipo, cmbSistema_Tipo_Internalname, StringUtil.RTrim( A699Sistema_Tipo), 1, cmbSistema_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbSistema_Tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_Sistema.htm");
            cmbSistema_Tipo.CurrentValue = StringUtil.RTrim( A699Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistema_Tipo_Internalname, "Values", (String)(cmbSistema_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_tecnica_Internalname, "T�cnica", "", "", lblTextblocksistema_tecnica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSistema_Tecnica, cmbSistema_Tecnica_Internalname, StringUtil.RTrim( A700Sistema_Tecnica), 1, cmbSistema_Tecnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbSistema_Tecnica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_Sistema.htm");
            cmbSistema_Tecnica.CurrentValue = StringUtil.RTrim( A700Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistema_Tecnica_Internalname, "Values", (String)(cmbSistema_Tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_fatorajuste_Internalname, "Fator de Ajuste", "", "", lblTextblocksistema_fatorajuste_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_FatorAjuste_Internalname, StringUtil.LTrim( StringUtil.NToC( A686Sistema_FatorAjuste, 6, 2, ",", "")), ((edtSistema_FatorAjuste_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99")) : context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_FatorAjuste_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_FatorAjuste_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_prazo_Internalname, "Prazo", "", "", lblTextblocksistema_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A688Sistema_Prazo), 4, 0, ",", "")), ((edtSistema_Prazo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A688Sistema_Prazo), "ZZZ9")) : context.localUtil.Format( (decimal)(A688Sistema_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Prazo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Prazo_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_custo_Internalname, "Custo", "", "", lblTextblocksistema_custo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Custo_Internalname, StringUtil.LTrim( StringUtil.NToC( A687Sistema_Custo, 18, 5, ",", "")), ((edtSistema_Custo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A687Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A687Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Custo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Custo_Enabled, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_esforco_Internalname, "Esforco", "", "", lblTextblocksistema_esforco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A689Sistema_Esforco), 4, 0, ",", "")), ((edtSistema_Esforco_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A689Sistema_Esforco), "ZZZ9")) : context.localUtil.Format( (decimal)(A689Sistema_Esforco), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Esforco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Esforco_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_responsavel_Internalname, "Respons�vel", "", "", lblTextblocksistema_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynSistema_Responsavel, dynSistema_Responsavel_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0)), 1, dynSistema_Responsavel_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynSistema_Responsavel.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "", true, "HLP_Sistema.htm");
            dynSistema_Responsavel.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistema_Responsavel_Internalname, "Values", (String)(dynSistema_Responsavel.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_gpoobjctrlcod_Internalname, "Gpo. Obj. de Controle", "", "", lblTextblocksistema_gpoobjctrlcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynSistema_GpoObjCtrlCod, dynSistema_GpoObjCtrlCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)), 1, dynSistema_GpoObjCtrlCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynSistema_GpoObjCtrlCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", true, "HLP_Sistema.htm");
            dynSistema_GpoObjCtrlCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistema_GpoObjCtrlCod_Internalname, "Values", (String)(dynSistema_GpoObjCtrlCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_ativo_Internalname, "Ativo", "", "", lblTextblocksistema_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblocksistema_ativo_Visible, 1, 0, "HLP_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'',0)\"";
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkSistema_Ativo_Internalname, StringUtil.BoolToStr( A130Sistema_Ativo), "", "", chkSistema_Ativo.Visible, chkSistema_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_19_0O25e( true) ;
         }
         else
         {
            wb_table12_19_0O25e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E120O2 */
         E120O2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
               A129Sistema_Sigla = StringUtil.Upper( cgiGet( edtSistema_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               dynSistemaVersao_Codigo.Name = dynSistemaVersao_Codigo_Internalname;
               dynSistemaVersao_Codigo.CurrentValue = cgiGet( dynSistemaVersao_Codigo_Internalname);
               A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( cgiGet( dynSistemaVersao_Codigo_Internalname), "."));
               n1859SistemaVersao_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               n1859SistemaVersao_Codigo = ((0==A1859SistemaVersao_Codigo) ? true : false);
               A128Sistema_Descricao = cgiGet( edtSistema_Descricao_Internalname);
               n128Sistema_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
               n128Sistema_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A128Sistema_Descricao)) ? true : false);
               A513Sistema_Coordenacao = StringUtil.Upper( cgiGet( edtSistema_Coordenacao_Internalname));
               n513Sistema_Coordenacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
               n513Sistema_Coordenacao = (String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) ? true : false);
               A2109Sistema_Repositorio = cgiGet( edtSistema_Repositorio_Internalname);
               n2109Sistema_Repositorio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2109Sistema_Repositorio", A2109Sistema_Repositorio);
               n2109Sistema_Repositorio = (String.IsNullOrEmpty(StringUtil.RTrim( A2109Sistema_Repositorio)) ? true : false);
               dynAmbienteTecnologico_Codigo.Name = dynAmbienteTecnologico_Codigo_Internalname;
               dynAmbienteTecnologico_Codigo.CurrentValue = cgiGet( dynAmbienteTecnologico_Codigo_Internalname);
               A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( cgiGet( dynAmbienteTecnologico_Codigo_Internalname), "."));
               n351AmbienteTecnologico_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               n351AmbienteTecnologico_Codigo = ((0==A351AmbienteTecnologico_Codigo) ? true : false);
               dynMetodologia_Codigo.Name = dynMetodologia_Codigo_Internalname;
               dynMetodologia_Codigo.CurrentValue = cgiGet( dynMetodologia_Codigo_Internalname);
               A137Metodologia_Codigo = (int)(NumberUtil.Val( cgiGet( dynMetodologia_Codigo_Internalname), "."));
               n137Metodologia_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
               n137Metodologia_Codigo = ((0==A137Metodologia_Codigo) ? true : false);
               cmbSistema_Tipo.Name = cmbSistema_Tipo_Internalname;
               cmbSistema_Tipo.CurrentValue = cgiGet( cmbSistema_Tipo_Internalname);
               A699Sistema_Tipo = cgiGet( cmbSistema_Tipo_Internalname);
               n699Sistema_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
               n699Sistema_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A699Sistema_Tipo)) ? true : false);
               cmbSistema_Tecnica.Name = cmbSistema_Tecnica_Internalname;
               cmbSistema_Tecnica.CurrentValue = cgiGet( cmbSistema_Tecnica_Internalname);
               A700Sistema_Tecnica = cgiGet( cmbSistema_Tecnica_Internalname);
               n700Sistema_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
               n700Sistema_Tecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A700Sistema_Tecnica)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistema_FatorAjuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistema_FatorAjuste_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMA_FATORAJUSTE");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_FatorAjuste_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A686Sistema_FatorAjuste = 0;
                  n686Sistema_FatorAjuste = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
               }
               else
               {
                  A686Sistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtSistema_FatorAjuste_Internalname), ",", ".");
                  n686Sistema_FatorAjuste = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
               }
               n686Sistema_FatorAjuste = ((Convert.ToDecimal(0)==A686Sistema_FatorAjuste) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistema_Prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistema_Prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMA_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A688Sistema_Prazo = 0;
                  n688Sistema_Prazo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
               }
               else
               {
                  A688Sistema_Prazo = (short)(context.localUtil.CToN( cgiGet( edtSistema_Prazo_Internalname), ",", "."));
                  n688Sistema_Prazo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
               }
               n688Sistema_Prazo = ((0==A688Sistema_Prazo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistema_Custo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtSistema_Custo_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMA_CUSTO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Custo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A687Sistema_Custo = 0;
                  n687Sistema_Custo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
               }
               else
               {
                  A687Sistema_Custo = context.localUtil.CToN( cgiGet( edtSistema_Custo_Internalname), ",", ".");
                  n687Sistema_Custo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
               }
               n687Sistema_Custo = ((Convert.ToDecimal(0)==A687Sistema_Custo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistema_Esforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistema_Esforco_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMA_ESFORCO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Esforco_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A689Sistema_Esforco = 0;
                  n689Sistema_Esforco = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
               }
               else
               {
                  A689Sistema_Esforco = (short)(context.localUtil.CToN( cgiGet( edtSistema_Esforco_Internalname), ",", "."));
                  n689Sistema_Esforco = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
               }
               n689Sistema_Esforco = ((0==A689Sistema_Esforco) ? true : false);
               dynSistema_Responsavel.Name = dynSistema_Responsavel_Internalname;
               dynSistema_Responsavel.CurrentValue = cgiGet( dynSistema_Responsavel_Internalname);
               A1831Sistema_Responsavel = (int)(NumberUtil.Val( cgiGet( dynSistema_Responsavel_Internalname), "."));
               n1831Sistema_Responsavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1831Sistema_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0)));
               n1831Sistema_Responsavel = ((0==A1831Sistema_Responsavel) ? true : false);
               dynSistema_GpoObjCtrlCod.Name = dynSistema_GpoObjCtrlCod_Internalname;
               dynSistema_GpoObjCtrlCod.CurrentValue = cgiGet( dynSistema_GpoObjCtrlCod_Internalname);
               A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( cgiGet( dynSistema_GpoObjCtrlCod_Internalname), "."));
               n2161Sistema_GpoObjCtrlCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
               n2161Sistema_GpoObjCtrlCod = ((0==A2161Sistema_GpoObjCtrlCod) ? true : false);
               A130Sistema_Ativo = StringUtil.StrToBool( cgiGet( chkSistema_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
               AV48Blob = cgiGet( edtavBlob_Internalname);
               AV47Aba = StringUtil.Upper( cgiGet( edtavAba_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Aba", AV47Aba);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINHA");
                  AnyError = 1;
                  GX_FocusControl = edtavPralinha_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV49PraLinha = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49PraLinha), 4, 0)));
               }
               else
               {
                  AV49PraLinha = (short)(context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49PraLinha), 4, 0)));
               }
               AV42ColSisNom = StringUtil.Upper( cgiGet( edtavColsisnom_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ColSisNom", AV42ColSisNom);
               AV43ColSisSgl = StringUtil.Upper( cgiGet( edtavColsissgl_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ColSisSgl", AV43ColSisSgl);
               AV44ColModNom = StringUtil.Upper( cgiGet( edtavColmodnom_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ColModNom", AV44ColModNom);
               AV45ColModSgl = StringUtil.Upper( cgiGet( edtavColmodsgl_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ColModSgl", AV45ColModSgl);
               AV21FileName = cgiGet( edtavFilename_Internalname);
               A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               /* Read saved values. */
               Z127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z127Sistema_Codigo"), ",", "."));
               Z416Sistema_Nome = cgiGet( "Z416Sistema_Nome");
               Z129Sistema_Sigla = cgiGet( "Z129Sistema_Sigla");
               Z513Sistema_Coordenacao = cgiGet( "Z513Sistema_Coordenacao");
               n513Sistema_Coordenacao = (String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) ? true : false);
               Z699Sistema_Tipo = cgiGet( "Z699Sistema_Tipo");
               n699Sistema_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A699Sistema_Tipo)) ? true : false);
               Z700Sistema_Tecnica = cgiGet( "Z700Sistema_Tecnica");
               n700Sistema_Tecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A700Sistema_Tecnica)) ? true : false);
               Z686Sistema_FatorAjuste = context.localUtil.CToN( cgiGet( "Z686Sistema_FatorAjuste"), ",", ".");
               n686Sistema_FatorAjuste = ((Convert.ToDecimal(0)==A686Sistema_FatorAjuste) ? true : false);
               Z687Sistema_Custo = context.localUtil.CToN( cgiGet( "Z687Sistema_Custo"), ",", ".");
               n687Sistema_Custo = ((Convert.ToDecimal(0)==A687Sistema_Custo) ? true : false);
               Z688Sistema_Prazo = (short)(context.localUtil.CToN( cgiGet( "Z688Sistema_Prazo"), ",", "."));
               n688Sistema_Prazo = ((0==A688Sistema_Prazo) ? true : false);
               Z689Sistema_Esforco = (short)(context.localUtil.CToN( cgiGet( "Z689Sistema_Esforco"), ",", "."));
               n689Sistema_Esforco = ((0==A689Sistema_Esforco) ? true : false);
               Z1401Sistema_ImpData = context.localUtil.CToT( cgiGet( "Z1401Sistema_ImpData"), 0);
               n1401Sistema_ImpData = ((DateTime.MinValue==A1401Sistema_ImpData) ? true : false);
               Z1831Sistema_Responsavel = (int)(context.localUtil.CToN( cgiGet( "Z1831Sistema_Responsavel"), ",", "."));
               n1831Sistema_Responsavel = ((0==A1831Sistema_Responsavel) ? true : false);
               Z130Sistema_Ativo = StringUtil.StrToBool( cgiGet( "Z130Sistema_Ativo"));
               Z2109Sistema_Repositorio = cgiGet( "Z2109Sistema_Repositorio");
               n2109Sistema_Repositorio = (String.IsNullOrEmpty(StringUtil.RTrim( A2109Sistema_Repositorio)) ? true : false);
               Z2130Sistema_Ultimo = (short)(context.localUtil.CToN( cgiGet( "Z2130Sistema_Ultimo"), ",", "."));
               Z137Metodologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z137Metodologia_Codigo"), ",", "."));
               n137Metodologia_Codigo = ((0==A137Metodologia_Codigo) ? true : false);
               Z351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z351AmbienteTecnologico_Codigo"), ",", "."));
               n351AmbienteTecnologico_Codigo = ((0==A351AmbienteTecnologico_Codigo) ? true : false);
               Z1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1859SistemaVersao_Codigo"), ",", "."));
               n1859SistemaVersao_Codigo = ((0==A1859SistemaVersao_Codigo) ? true : false);
               Z135Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z135Sistema_AreaTrabalhoCod"), ",", "."));
               Z1399Sistema_ImpUserCod = (int)(context.localUtil.CToN( cgiGet( "Z1399Sistema_ImpUserCod"), ",", "."));
               n1399Sistema_ImpUserCod = ((0==A1399Sistema_ImpUserCod) ? true : false);
               Z2161Sistema_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( "Z2161Sistema_GpoObjCtrlCod"), ",", "."));
               n2161Sistema_GpoObjCtrlCod = ((0==A2161Sistema_GpoObjCtrlCod) ? true : false);
               A1401Sistema_ImpData = context.localUtil.CToT( cgiGet( "Z1401Sistema_ImpData"), 0);
               n1401Sistema_ImpData = false;
               n1401Sistema_ImpData = ((DateTime.MinValue==A1401Sistema_ImpData) ? true : false);
               A2130Sistema_Ultimo = (short)(context.localUtil.CToN( cgiGet( "Z2130Sistema_Ultimo"), ",", "."));
               A135Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z135Sistema_AreaTrabalhoCod"), ",", "."));
               A1399Sistema_ImpUserCod = (int)(context.localUtil.CToN( cgiGet( "Z1399Sistema_ImpUserCod"), ",", "."));
               n1399Sistema_ImpUserCod = false;
               n1399Sistema_ImpUserCod = ((0==A1399Sistema_ImpUserCod) ? true : false);
               O2130Sistema_Ultimo = (short)(context.localUtil.CToN( cgiGet( "O2130Sistema_Ultimo"), ",", "."));
               O416Sistema_Nome = cgiGet( "O416Sistema_Nome");
               O129Sistema_Sigla = cgiGet( "O129Sistema_Sigla");
               O513Sistema_Coordenacao = cgiGet( "O513Sistema_Coordenacao");
               n513Sistema_Coordenacao = (String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_108 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_108"), ",", "."));
               N135Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N135Sistema_AreaTrabalhoCod"), ",", "."));
               N351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( "N351AmbienteTecnologico_Codigo"), ",", "."));
               n351AmbienteTecnologico_Codigo = ((0==A351AmbienteTecnologico_Codigo) ? true : false);
               N137Metodologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "N137Metodologia_Codigo"), ",", "."));
               n137Metodologia_Codigo = ((0==A137Metodologia_Codigo) ? true : false);
               N1399Sistema_ImpUserCod = (int)(context.localUtil.CToN( cgiGet( "N1399Sistema_ImpUserCod"), ",", "."));
               n1399Sistema_ImpUserCod = ((0==A1399Sistema_ImpUserCod) ? true : false);
               N2161Sistema_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( "N2161Sistema_GpoObjCtrlCod"), ",", "."));
               n2161Sistema_GpoObjCtrlCod = ((0==A2161Sistema_GpoObjCtrlCod) ? true : false);
               N1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( "N1859SistemaVersao_Codigo"), ",", "."));
               n1859SistemaVersao_Codigo = ((0==A1859SistemaVersao_Codigo) ? true : false);
               A2162Sistema_GpoObjCtrlRsp = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_GPOOBJCTRLRSP"), ",", "."));
               A395Sistema_PF = context.localUtil.CToN( cgiGet( "SISTEMA_PF"), ",", ".");
               A690Sistema_PFA = context.localUtil.CToN( cgiGet( "SISTEMA_PFA"), ",", ".");
               A707Sistema_TipoSigla = cgiGet( "SISTEMA_TIPOSIGLA");
               AV7Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSISTEMA_CODIGO"), ",", "."));
               AV11Insert_Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMA_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A135Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_AREATRABALHOCOD"), ",", "."));
               AV16Insert_AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_AMBIENTETECNOLOGICO_CODIGO"), ",", "."));
               AV13Insert_Metodologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_METODOLOGIA_CODIGO"), ",", "."));
               AV40Insert_Sistema_ImpUserCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMA_IMPUSERCOD"), ",", "."));
               A1399Sistema_ImpUserCod = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_IMPUSERCOD"), ",", "."));
               n1399Sistema_ImpUserCod = ((0==A1399Sistema_ImpUserCod) ? true : false);
               AV58Insert_Sistema_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMA_GPOOBJCTRLCOD"), ",", "."));
               AV56Insert_SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMAVERSAO_CODIGO"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV54AuditingObject);
               A1401Sistema_ImpData = context.localUtil.CToT( cgiGet( "SISTEMA_IMPDATA"), 0);
               n1401Sistema_ImpData = ((DateTime.MinValue==A1401Sistema_ImpData) ? true : false);
               A2130Sistema_Ultimo = (short)(context.localUtil.CToN( cgiGet( "SISTEMA_ULTIMO"), ",", "."));
               A138Metodologia_Descricao = cgiGet( "METODOLOGIA_DESCRICAO");
               A352AmbienteTecnologico_Descricao = cgiGet( "AMBIENTETECNOLOGICO_DESCRICAO");
               A1860SistemaVersao_Id = cgiGet( "SISTEMAVERSAO_ID");
               A136Sistema_AreaTrabalhoDes = cgiGet( "SISTEMA_AREATRABALHODES");
               n136Sistema_AreaTrabalhoDes = false;
               A1402Sistema_ImpUserPesCod = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_IMPUSERPESCOD"), ",", "."));
               A1403Sistema_ImpUserPesNom = cgiGet( "SISTEMA_IMPUSERPESNOM");
               n1403Sistema_ImpUserPesNom = false;
               AV60Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               A132Tecnologia_Nome = cgiGet( "TECNOLOGIA_NOME");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               Dvpanel_unnamedtable1_Width = cgiGet( "DVPANEL_UNNAMEDTABLE1_Width");
               Dvpanel_unnamedtable1_Height = cgiGet( "DVPANEL_UNNAMEDTABLE1_Height");
               Dvpanel_unnamedtable1_Cls = cgiGet( "DVPANEL_UNNAMEDTABLE1_Cls");
               Dvpanel_unnamedtable1_Title = cgiGet( "DVPANEL_UNNAMEDTABLE1_Title");
               Dvpanel_unnamedtable1_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsible"));
               Dvpanel_unnamedtable1_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsed"));
               Dvpanel_unnamedtable1_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Enabled"));
               Dvpanel_unnamedtable1_Class = cgiGet( "DVPANEL_UNNAMEDTABLE1_Class");
               Dvpanel_unnamedtable1_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autowidth"));
               Dvpanel_unnamedtable1_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoheight"));
               Dvpanel_unnamedtable1_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Showheader"));
               Dvpanel_unnamedtable1_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Showcollapseicon"));
               Dvpanel_unnamedtable1_Iconposition = cgiGet( "DVPANEL_UNNAMEDTABLE1_Iconposition");
               Dvpanel_unnamedtable1_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoscroll"));
               Dvpanel_unnamedtable1_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Visible"));
               Gxuitabspanel_tabmain_Width = cgiGet( "GXUITABSPANEL_TABMAIN_Width");
               Gxuitabspanel_tabmain_Height = cgiGet( "GXUITABSPANEL_TABMAIN_Height");
               Gxuitabspanel_tabmain_Cls = cgiGet( "GXUITABSPANEL_TABMAIN_Cls");
               Gxuitabspanel_tabmain_Enabled = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Enabled"));
               Gxuitabspanel_tabmain_Class = cgiGet( "GXUITABSPANEL_TABMAIN_Class");
               Gxuitabspanel_tabmain_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autowidth"));
               Gxuitabspanel_tabmain_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoheight"));
               Gxuitabspanel_tabmain_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoscroll"));
               Gxuitabspanel_tabmain_Activetabid = cgiGet( "GXUITABSPANEL_TABMAIN_Activetabid");
               Gxuitabspanel_tabmain_Designtimetabs = cgiGet( "GXUITABSPANEL_TABMAIN_Designtimetabs");
               Gxuitabspanel_tabmain_Selectedtabindex = (int)(context.localUtil.CToN( cgiGet( "GXUITABSPANEL_TABMAIN_Selectedtabindex"), ",", "."));
               Gxuitabspanel_tabmain_Visible = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Visible"));
               edtavBlob_Filename = cgiGet( "BLOB_Filename");
               edtavBlob_Filetype = cgiGet( "BLOB_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Blob)) )
               {
                  edtavBlob_Filename = (String)(CGIGetFileName(edtavBlob_Internalname));
                  edtavBlob_Filetype = (String)(CGIGetFileType(edtavBlob_Internalname));
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Sistema";
               A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A135Sistema_AreaTrabalhoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1399Sistema_ImpUserCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV60Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("sistema:[SecurityCheckFailed value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("sistema:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("sistema:[SecurityCheckFailed value for]"+"Sistema_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A135Sistema_AreaTrabalhoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("sistema:[SecurityCheckFailed value for]"+"Sistema_ImpUserCod:"+context.localUtil.Format( (decimal)(A1399Sistema_ImpUserCod), "ZZZZZ9"));
                  GXUtil.WriteLog("sistema:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV60Pgmname, "")));
                  GXUtil.WriteLog("sistema:[SecurityCheckFailed value for]"+"Sistema_ImpData:"+context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode25 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode25;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound25 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0O0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SISTEMA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtSistema_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120O2 */
                           E120O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130O2 */
                           E130O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCOPIARCOLAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140O2 */
                           E140O2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOIMPORTAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E150O2 */
                           E150O2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E130O2 */
            E130O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0O25( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0O25( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBlob_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAba_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAba_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPralinha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPralinha_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColsisnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColsisnom_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColsissgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColsissgl_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColmodnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColmodnom_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColmodsgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColmodsgl_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0O0( )
      {
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0O25( ) ;
            }
            else
            {
               CheckExtendedTable0O25( ) ;
               CloseExtendedTableCursors0O25( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode25 = Gx_mode;
            CONFIRM_0O233( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode25;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode25;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_0O233( )
      {
         s2130Sistema_Ultimo = O2130Sistema_Ultimo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         nGXsfl_108_idx = 0;
         while ( nGXsfl_108_idx < nRC_GXsfl_108 )
         {
            ReadRow0O233( ) ;
            if ( ( nRcdExists_233 != 0 ) || ( nIsMod_233 != 0 ) )
            {
               GetKey0O233( ) ;
               if ( ( nRcdExists_233 == 0 ) && ( nRcdDeleted_233 == 0 ) )
               {
                  if ( RcdFound233 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate0O233( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0O233( ) ;
                        CloseExtendedTableCursors0O233( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound233 != 0 )
                  {
                     if ( nRcdDeleted_233 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey0O233( ) ;
                        Load0O233( ) ;
                        BeforeValidate0O233( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0O233( ) ;
                           O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_233 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate0O233( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0O233( ) ;
                              CloseExtendedTableCursors0O233( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_233 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            ChangePostValue( edtSistemaTecnologiaLinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2128SistemaTecnologiaLinha), 4, 0, ",", ""))) ;
            ChangePostValue( dynTecnologia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( cmbTecnologia_TipoTecnologia_Internalname, StringUtil.RTrim( A355Tecnologia_TipoTecnologia)) ;
            ChangePostValue( "ZT_"+"Z2128SistemaTecnologiaLinha_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2128SistemaTecnologiaLinha), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z131Tecnologia_Codigo_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z131Tecnologia_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_233_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_233), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_233_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_233), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_233_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_233), 4, 0, ",", ""))) ;
            if ( nIsMod_233 != 0 )
            {
               ChangePostValue( "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TECNOLOGIA_CODIGO_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0, ".", ""))) ;
            }
         }
         O2130Sistema_Ultimo = s2130Sistema_Ultimo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption0O0( )
      {
      }

      protected void E120O2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.1 - Data: 26/03/2020 09:10", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV60Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV61GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GXV1), 8, 0)));
            while ( AV61GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV61GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Sistema_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Sistema_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "AmbienteTecnologico_Codigo") == 0 )
               {
                  AV16Insert_AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_AmbienteTecnologico_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Metodologia_Codigo") == 0 )
               {
                  AV13Insert_Metodologia_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Metodologia_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Sistema_ImpUserCod") == 0 )
               {
                  AV40Insert_Sistema_ImpUserCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Insert_Sistema_ImpUserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Insert_Sistema_ImpUserCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Sistema_GpoObjCtrlCod") == 0 )
               {
                  AV58Insert_Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Insert_Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Insert_Sistema_GpoObjCtrlCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "SistemaVersao_Codigo") == 0 )
               {
                  AV56Insert_SistemaVersao_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Insert_SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56Insert_SistemaVersao_Codigo), 6, 0)));
               }
               AV61GXV1 = (int)(AV61GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GXV1), 8, 0)));
            }
         }
         edtSistema_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Visible), 5, 0)));
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            lblTextblocksistema_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocksistema_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocksistema_ativo_Visible), 5, 0)));
            chkSistema_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkSistema_Ativo.Visible), 5, 0)));
         }
         tblTblimportar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblimportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblimportar_Visible), 5, 0)));
         AV49PraLinha = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49PraLinha), 4, 0)));
         edtavBlob_Display = 1;
      }

      protected void E130O2( )
      {
         /* After Trn Routine */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) || ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) ) )
         {
            AV10WebSession.Remove("Tabela_Codigo");
            AV10WebSession.Remove("FuncaoDados_Codigo");
            AV10WebSession.Remove("APFTabela_Codigo");
         }
         if ( ( NumberUtil.Val( AV10WebSession.Get("Sistema_Codigo"), ".") > Convert.ToDecimal( 0 )) )
         {
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) && ( StringUtil.StrCmp(O513Sistema_Coordenacao, "") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) )
         {
            new prc_upderrosfaltacoordenacao(context ).execute( ref  A127Sistema_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         else
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A127Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               context.wjLocDisableFrm = 1;
            }
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV54AuditingObject,  AV60Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Pgmname", AV60Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwsistema.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E140O2( )
      {
         /* 'DoCopiarColar' Routine */
         context.wjLoc = formatLink("wp_copiarcolar.aspx") + "?" + UrlEncode("" +AV7Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim("Sis"));
         context.wjLocDisableFrm = 1;
      }

      protected void E150O2( )
      {
         /* 'DoImportar' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV42ColSisNom, 2, 1))) )
         {
            AV52ColSisNomn = (short)(StringUtil.Asc( AV42ColSisNom)-64);
         }
         else
         {
            AV52ColSisNomn = (short)(StringUtil.Asc( StringUtil.Substring( AV42ColSisNom, 2, 1))-64+26);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV43ColSisSgl, 2, 1))) )
         {
            AV51ColSisSgln = (short)(StringUtil.Asc( AV43ColSisSgl)-64);
         }
         else
         {
            AV51ColSisSgln = (short)(StringUtil.Asc( StringUtil.Substring( AV43ColSisSgl, 2, 1))-64+26);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV44ColModNom, 2, 1))) )
         {
            AV53ColModNomn = (short)(StringUtil.Asc( AV44ColModNom)-64);
         }
         else
         {
            AV53ColModNomn = (short)(StringUtil.Asc( StringUtil.Substring( AV44ColModNom, 2, 1))-64+26);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV45ColModSgl, 2, 1))) )
         {
            AV50ColModSgln = (short)(StringUtil.Asc( AV45ColModSgl)-64);
         }
         else
         {
            AV50ColModSgln = (short)(StringUtil.Asc( StringUtil.Substring( AV45ColModSgl, 2, 1))-64+26);
         }
      }

      protected void ZM0O25( short GX_JID )
      {
         if ( ( GX_JID == 52 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z416Sistema_Nome = T000O6_A416Sistema_Nome[0];
               Z129Sistema_Sigla = T000O6_A129Sistema_Sigla[0];
               Z513Sistema_Coordenacao = T000O6_A513Sistema_Coordenacao[0];
               Z699Sistema_Tipo = T000O6_A699Sistema_Tipo[0];
               Z700Sistema_Tecnica = T000O6_A700Sistema_Tecnica[0];
               Z686Sistema_FatorAjuste = T000O6_A686Sistema_FatorAjuste[0];
               Z687Sistema_Custo = T000O6_A687Sistema_Custo[0];
               Z688Sistema_Prazo = T000O6_A688Sistema_Prazo[0];
               Z689Sistema_Esforco = T000O6_A689Sistema_Esforco[0];
               Z1401Sistema_ImpData = T000O6_A1401Sistema_ImpData[0];
               Z1831Sistema_Responsavel = T000O6_A1831Sistema_Responsavel[0];
               Z130Sistema_Ativo = T000O6_A130Sistema_Ativo[0];
               Z2109Sistema_Repositorio = T000O6_A2109Sistema_Repositorio[0];
               Z2130Sistema_Ultimo = T000O6_A2130Sistema_Ultimo[0];
               Z137Metodologia_Codigo = T000O6_A137Metodologia_Codigo[0];
               Z351AmbienteTecnologico_Codigo = T000O6_A351AmbienteTecnologico_Codigo[0];
               Z1859SistemaVersao_Codigo = T000O6_A1859SistemaVersao_Codigo[0];
               Z135Sistema_AreaTrabalhoCod = T000O6_A135Sistema_AreaTrabalhoCod[0];
               Z1399Sistema_ImpUserCod = T000O6_A1399Sistema_ImpUserCod[0];
               Z2161Sistema_GpoObjCtrlCod = T000O6_A2161Sistema_GpoObjCtrlCod[0];
            }
            else
            {
               Z416Sistema_Nome = A416Sistema_Nome;
               Z129Sistema_Sigla = A129Sistema_Sigla;
               Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
               Z699Sistema_Tipo = A699Sistema_Tipo;
               Z700Sistema_Tecnica = A700Sistema_Tecnica;
               Z686Sistema_FatorAjuste = A686Sistema_FatorAjuste;
               Z687Sistema_Custo = A687Sistema_Custo;
               Z688Sistema_Prazo = A688Sistema_Prazo;
               Z689Sistema_Esforco = A689Sistema_Esforco;
               Z1401Sistema_ImpData = A1401Sistema_ImpData;
               Z1831Sistema_Responsavel = A1831Sistema_Responsavel;
               Z130Sistema_Ativo = A130Sistema_Ativo;
               Z2109Sistema_Repositorio = A2109Sistema_Repositorio;
               Z2130Sistema_Ultimo = A2130Sistema_Ultimo;
               Z137Metodologia_Codigo = A137Metodologia_Codigo;
               Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
               Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
               Z135Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
               Z1399Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
               Z2161Sistema_GpoObjCtrlCod = A2161Sistema_GpoObjCtrlCod;
            }
         }
         if ( GX_JID == -52 )
         {
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z416Sistema_Nome = A416Sistema_Nome;
            Z128Sistema_Descricao = A128Sistema_Descricao;
            Z129Sistema_Sigla = A129Sistema_Sigla;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z699Sistema_Tipo = A699Sistema_Tipo;
            Z700Sistema_Tecnica = A700Sistema_Tecnica;
            Z686Sistema_FatorAjuste = A686Sistema_FatorAjuste;
            Z687Sistema_Custo = A687Sistema_Custo;
            Z688Sistema_Prazo = A688Sistema_Prazo;
            Z689Sistema_Esforco = A689Sistema_Esforco;
            Z1401Sistema_ImpData = A1401Sistema_ImpData;
            Z1831Sistema_Responsavel = A1831Sistema_Responsavel;
            Z130Sistema_Ativo = A130Sistema_Ativo;
            Z2109Sistema_Repositorio = A2109Sistema_Repositorio;
            Z2130Sistema_Ultimo = A2130Sistema_Ultimo;
            Z137Metodologia_Codigo = A137Metodologia_Codigo;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            Z135Sistema_AreaTrabalhoCod = A135Sistema_AreaTrabalhoCod;
            Z1399Sistema_ImpUserCod = A1399Sistema_ImpUserCod;
            Z2161Sistema_GpoObjCtrlCod = A2161Sistema_GpoObjCtrlCod;
            Z136Sistema_AreaTrabalhoDes = A136Sistema_AreaTrabalhoDes;
            Z1402Sistema_ImpUserPesCod = A1402Sistema_ImpUserPesCod;
            Z1403Sistema_ImpUserPesNom = A1403Sistema_ImpUserPesNom;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z138Metodologia_Descricao = A138Metodologia_Descricao;
            Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAMETODOLOGIA_CODIGO_html0O25( ) ;
         edtSistema_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
         edtSistema_Coordenacao_Width = 300;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Coordenacao_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Coordenacao_Width), 9, 0)));
         AV60Pgmname = "Sistema";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Pgmname", AV60Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtSistema_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         GXASISTEMAVERSAO_CODIGO_html0O25( AV7Sistema_Codigo) ;
         if ( ! (0==AV7Sistema_Codigo) )
         {
            A127Sistema_Codigo = AV7Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         GXAAMBIENTETECNOLOGICO_CODIGO_html0O25( AV8WWPContext) ;
         GXASISTEMA_RESPONSAVEL_html0O25( AV8WWPContext) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_AmbienteTecnologico_Codigo) )
         {
            dynAmbienteTecnologico_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAmbienteTecnologico_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynAmbienteTecnologico_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAmbienteTecnologico_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Metodologia_Codigo) )
         {
            dynMetodologia_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMetodologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynMetodologia_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynMetodologia_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMetodologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynMetodologia_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV58Insert_Sistema_GpoObjCtrlCod) )
         {
            dynSistema_GpoObjCtrlCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistema_GpoObjCtrlCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistema_GpoObjCtrlCod.Enabled), 5, 0)));
         }
         else
         {
            dynSistema_GpoObjCtrlCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistema_GpoObjCtrlCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistema_GpoObjCtrlCod.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV56Insert_SistemaVersao_Codigo) )
         {
            dynSistemaVersao_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistemaVersao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistemaVersao_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynSistemaVersao_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistemaVersao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistemaVersao_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkSistema_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkSistema_Ativo.Visible), 5, 0)));
         bttBtncopiarcolar_Visible = ((( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) ) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtncopiarcolar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtncopiarcolar_Visible), 5, 0)));
         bttBtnimportarexcel_Visible = ((( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) ) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnimportarexcel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportarexcel_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV56Insert_SistemaVersao_Codigo) )
         {
            A1859SistemaVersao_Codigo = AV56Insert_SistemaVersao_Codigo;
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV58Insert_Sistema_GpoObjCtrlCod) )
         {
            A2161Sistema_GpoObjCtrlCod = AV58Insert_Sistema_GpoObjCtrlCod;
            n2161Sistema_GpoObjCtrlCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV40Insert_Sistema_ImpUserCod) )
         {
            A1399Sistema_ImpUserCod = AV40Insert_Sistema_ImpUserCod;
            n1399Sistema_ImpUserCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1399Sistema_ImpUserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1399Sistema_ImpUserCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Metodologia_Codigo) )
         {
            A137Metodologia_Codigo = AV13Insert_Metodologia_Codigo;
            n137Metodologia_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_AmbienteTecnologico_Codigo) )
         {
            A351AmbienteTecnologico_Codigo = AV16Insert_AmbienteTecnologico_Codigo;
            n351AmbienteTecnologico_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Sistema_AreaTrabalhoCod) )
         {
            A135Sistema_AreaTrabalhoCod = AV11Insert_Sistema_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A135Sistema_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A130Sistema_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A130Sistema_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A699Sistema_Tipo)) && ( Gx_BScreen == 0 ) )
         {
            A699Sistema_Tipo = "A";
            n699Sistema_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A686Sistema_FatorAjuste) && ( Gx_BScreen == 0 ) )
         {
            GXt_decimal1 = A686Sistema_FatorAjuste;
            new prc_fapadrao(context ).execute( out  GXt_decimal1) ;
            A686Sistema_FatorAjuste = GXt_decimal1;
            n686Sistema_FatorAjuste = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000O7 */
            pr_default.execute(5, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
            A138Metodologia_Descricao = T000O7_A138Metodologia_Descricao[0];
            pr_default.close(5);
            /* Using cursor T000O9 */
            pr_default.execute(7, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
            A1860SistemaVersao_Id = T000O9_A1860SistemaVersao_Id[0];
            pr_default.close(7);
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A395Sistema_PF = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
            /* Using cursor T000O8 */
            pr_default.execute(6, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            A352AmbienteTecnologico_Descricao = T000O8_A352AmbienteTecnologico_Descricao[0];
            pr_default.close(6);
            GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
            GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
            new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
            A2161Sistema_GpoObjCtrlCod = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            A2162Sistema_GpoObjCtrlRsp = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2162Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0)));
            /* Using cursor T000O11 */
            pr_default.execute(9, new Object[] {n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod});
            A1402Sistema_ImpUserPesCod = T000O11_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = T000O11_n1402Sistema_ImpUserPesCod[0];
            pr_default.close(9);
            /* Using cursor T000O13 */
            pr_default.execute(11, new Object[] {n1402Sistema_ImpUserPesCod, A1402Sistema_ImpUserPesCod});
            A1403Sistema_ImpUserPesNom = T000O13_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = T000O13_n1403Sistema_ImpUserPesNom[0];
            pr_default.close(11);
            /* Using cursor T000O10 */
            pr_default.execute(8, new Object[] {A135Sistema_AreaTrabalhoCod});
            A136Sistema_AreaTrabalhoDes = T000O10_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = T000O10_n136Sistema_AreaTrabalhoDes[0];
            pr_default.close(8);
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
            {
               A707Sistema_TipoSigla = "PD";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
               {
                  A707Sistema_TipoSigla = "PM";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                  {
                     A707Sistema_TipoSigla = "PC";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                     {
                        A707Sistema_TipoSigla = "A";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                     }
                     else
                     {
                        A707Sistema_TipoSigla = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                     }
                  }
               }
            }
            A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
         }
      }

      protected void Load0O25( )
      {
         /* Using cursor T000O15 */
         pr_default.execute(13, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound25 = 1;
            A416Sistema_Nome = T000O15_A416Sistema_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
            A128Sistema_Descricao = T000O15_A128Sistema_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
            n128Sistema_Descricao = T000O15_n128Sistema_Descricao[0];
            A129Sistema_Sigla = T000O15_A129Sistema_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
            A513Sistema_Coordenacao = T000O15_A513Sistema_Coordenacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
            n513Sistema_Coordenacao = T000O15_n513Sistema_Coordenacao[0];
            A136Sistema_AreaTrabalhoDes = T000O15_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = T000O15_n136Sistema_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = T000O15_A352AmbienteTecnologico_Descricao[0];
            A138Metodologia_Descricao = T000O15_A138Metodologia_Descricao[0];
            A699Sistema_Tipo = T000O15_A699Sistema_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
            n699Sistema_Tipo = T000O15_n699Sistema_Tipo[0];
            A700Sistema_Tecnica = T000O15_A700Sistema_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
            n700Sistema_Tecnica = T000O15_n700Sistema_Tecnica[0];
            A686Sistema_FatorAjuste = T000O15_A686Sistema_FatorAjuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
            n686Sistema_FatorAjuste = T000O15_n686Sistema_FatorAjuste[0];
            A687Sistema_Custo = T000O15_A687Sistema_Custo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
            n687Sistema_Custo = T000O15_n687Sistema_Custo[0];
            A688Sistema_Prazo = T000O15_A688Sistema_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
            n688Sistema_Prazo = T000O15_n688Sistema_Prazo[0];
            A689Sistema_Esforco = T000O15_A689Sistema_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
            n689Sistema_Esforco = T000O15_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = T000O15_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = T000O15_n1401Sistema_ImpData[0];
            A1403Sistema_ImpUserPesNom = T000O15_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = T000O15_n1403Sistema_ImpUserPesNom[0];
            A1831Sistema_Responsavel = T000O15_A1831Sistema_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1831Sistema_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0)));
            n1831Sistema_Responsavel = T000O15_n1831Sistema_Responsavel[0];
            A1860SistemaVersao_Id = T000O15_A1860SistemaVersao_Id[0];
            A130Sistema_Ativo = T000O15_A130Sistema_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
            A2109Sistema_Repositorio = T000O15_A2109Sistema_Repositorio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2109Sistema_Repositorio", A2109Sistema_Repositorio);
            n2109Sistema_Repositorio = T000O15_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = T000O15_A2130Sistema_Ultimo[0];
            A137Metodologia_Codigo = T000O15_A137Metodologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
            n137Metodologia_Codigo = T000O15_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = T000O15_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            n351AmbienteTecnologico_Codigo = T000O15_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = T000O15_A1859SistemaVersao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            n1859SistemaVersao_Codigo = T000O15_n1859SistemaVersao_Codigo[0];
            A135Sistema_AreaTrabalhoCod = T000O15_A135Sistema_AreaTrabalhoCod[0];
            A1399Sistema_ImpUserCod = T000O15_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = T000O15_n1399Sistema_ImpUserCod[0];
            A2161Sistema_GpoObjCtrlCod = T000O15_A2161Sistema_GpoObjCtrlCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            n2161Sistema_GpoObjCtrlCod = T000O15_n2161Sistema_GpoObjCtrlCod[0];
            A1402Sistema_ImpUserPesCod = T000O15_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = T000O15_n1402Sistema_ImpUserPesCod[0];
            ZM0O25( -52) ;
         }
         pr_default.close(13);
         OnLoadActions0O25( ) ;
      }

      protected void OnLoadActions0O25( )
      {
         GXt_decimal1 = A395Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         A395Sistema_PF = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
         A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
         if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
         {
            A707Sistema_TipoSigla = "PD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
         }
         else
         {
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
            {
               A707Sistema_TipoSigla = "PM";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
               {
                  A707Sistema_TipoSigla = "PC";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                  {
                     A707Sistema_TipoSigla = "A";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
                  else
                  {
                     A707Sistema_TipoSigla = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
               }
            }
         }
         GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
         GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
         A2161Sistema_GpoObjCtrlCod = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         A2162Sistema_GpoObjCtrlRsp = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2162Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0)));
      }

      protected void CheckExtendedTable0O25( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         GXt_decimal1 = A395Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         A395Sistema_PF = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
         A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A416Sistema_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "SISTEMA_NOME");
            AnyError = 1;
            GX_FocusControl = edtSistema_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A129Sistema_Sigla)) )
         {
            GX_msglist.addItem("Sigla � obrigat�rio.", 1, "SISTEMA_SIGLA");
            AnyError = 1;
            GX_FocusControl = edtSistema_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000O8 */
         pr_default.execute(6, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A351AmbienteTecnologico_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A352AmbienteTecnologico_Descricao = T000O8_A352AmbienteTecnologico_Descricao[0];
         pr_default.close(6);
         /* Using cursor T000O7 */
         pr_default.execute(5, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A137Metodologia_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Metodologia'.", "ForeignKeyNotFound", 1, "METODOLOGIA_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynMetodologia_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A138Metodologia_Descricao = T000O7_A138Metodologia_Descricao[0];
         pr_default.close(5);
         if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
         {
            A707Sistema_TipoSigla = "PD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
         }
         else
         {
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
            {
               A707Sistema_TipoSigla = "PM";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
               {
                  A707Sistema_TipoSigla = "PC";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                  {
                     A707Sistema_TipoSigla = "A";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
                  else
                  {
                     A707Sistema_TipoSigla = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
               }
            }
         }
         /* Using cursor T000O12 */
         pr_default.execute(10, new Object[] {n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A2161Sistema_GpoObjCtrlCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_GpoObjCtrl'.", "ForeignKeyNotFound", 1, "SISTEMA_GPOOBJCTRLCOD");
               AnyError = 1;
               GX_FocusControl = dynSistema_GpoObjCtrlCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(10);
         GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
         GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
         A2161Sistema_GpoObjCtrlCod = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         A2162Sistema_GpoObjCtrlRsp = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2162Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0)));
         /* Using cursor T000O9 */
         pr_default.execute(7, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A1859SistemaVersao_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T207'.", "ForeignKeyNotFound", 1, "SISTEMAVERSAO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynSistemaVersao_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1860SistemaVersao_Id = T000O9_A1860SistemaVersao_Id[0];
         pr_default.close(7);
         if ( ! ( GxRegex.IsMatch(AV42ColSisNom,"[A-Z]?") || String.IsNullOrEmpty(StringUtil.RTrim( AV42ColSisNom)) ) )
         {
            GX_msglist.addItem("Esperado A-Z", "OutOfRange", 1, "vCOLSISNOM");
            AnyError = 1;
            GX_FocusControl = edtavColsisnom_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( GxRegex.IsMatch(AV43ColSisSgl,"[A-Z]?") || String.IsNullOrEmpty(StringUtil.RTrim( AV43ColSisSgl)) ) )
         {
            GX_msglist.addItem("Esperado A-Z", "OutOfRange", 1, "vCOLSISSGL");
            AnyError = 1;
            GX_FocusControl = edtavColsissgl_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( GxRegex.IsMatch(AV44ColModNom,"[A-Z]?") || String.IsNullOrEmpty(StringUtil.RTrim( AV44ColModNom)) ) )
         {
            GX_msglist.addItem("Esperado A-Z", "OutOfRange", 1, "vCOLMODNOM");
            AnyError = 1;
            GX_FocusControl = edtavColmodnom_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( GxRegex.IsMatch(AV45ColModSgl,"[A-Z]?") || String.IsNullOrEmpty(StringUtil.RTrim( AV45ColModSgl)) ) )
         {
            GX_msglist.addItem("Esperado A-Z", "OutOfRange", 1, "vCOLMODSGL");
            AnyError = 1;
            GX_FocusControl = edtavColmodsgl_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000O10 */
         pr_default.execute(8, new Object[] {A135Sistema_AreaTrabalhoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Sistema_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A136Sistema_AreaTrabalhoDes = T000O10_A136Sistema_AreaTrabalhoDes[0];
         n136Sistema_AreaTrabalhoDes = T000O10_n136Sistema_AreaTrabalhoDes[0];
         pr_default.close(8);
         /* Using cursor T000O11 */
         pr_default.execute(9, new Object[] {n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A1399Sistema_ImpUserCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_Usuario da Importa��o'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1402Sistema_ImpUserPesCod = T000O11_A1402Sistema_ImpUserPesCod[0];
         n1402Sistema_ImpUserPesCod = T000O11_n1402Sistema_ImpUserPesCod[0];
         pr_default.close(9);
         /* Using cursor T000O13 */
         pr_default.execute(11, new Object[] {n1402Sistema_ImpUserPesCod, A1402Sistema_ImpUserPesCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A1402Sistema_ImpUserPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1403Sistema_ImpUserPesNom = T000O13_A1403Sistema_ImpUserPesNom[0];
         n1403Sistema_ImpUserPesNom = T000O13_n1403Sistema_ImpUserPesNom[0];
         pr_default.close(11);
      }

      protected void CloseExtendedTableCursors0O25( )
      {
         pr_default.close(6);
         pr_default.close(5);
         pr_default.close(10);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(9);
         pr_default.close(11);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_54( int A351AmbienteTecnologico_Codigo )
      {
         /* Using cursor T000O16 */
         pr_default.execute(14, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A351AmbienteTecnologico_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A352AmbienteTecnologico_Descricao = T000O16_A352AmbienteTecnologico_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A352AmbienteTecnologico_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_53( int A137Metodologia_Codigo )
      {
         /* Using cursor T000O17 */
         pr_default.execute(15, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A137Metodologia_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Metodologia'.", "ForeignKeyNotFound", 1, "METODOLOGIA_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynMetodologia_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A138Metodologia_Descricao = T000O17_A138Metodologia_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A138Metodologia_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_58( int A2161Sistema_GpoObjCtrlCod )
      {
         /* Using cursor T000O18 */
         pr_default.execute(16, new Object[] {n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A2161Sistema_GpoObjCtrlCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_GpoObjCtrl'.", "ForeignKeyNotFound", 1, "SISTEMA_GPOOBJCTRLCOD");
               AnyError = 1;
               GX_FocusControl = dynSistema_GpoObjCtrlCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_55( int A1859SistemaVersao_Codigo )
      {
         /* Using cursor T000O19 */
         pr_default.execute(17, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A1859SistemaVersao_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T207'.", "ForeignKeyNotFound", 1, "SISTEMAVERSAO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynSistemaVersao_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1860SistemaVersao_Id = T000O19_A1860SistemaVersao_Id[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1860SistemaVersao_Id))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_56( int A135Sistema_AreaTrabalhoCod )
      {
         /* Using cursor T000O20 */
         pr_default.execute(18, new Object[] {A135Sistema_AreaTrabalhoCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Sistema_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A136Sistema_AreaTrabalhoDes = T000O20_A136Sistema_AreaTrabalhoDes[0];
         n136Sistema_AreaTrabalhoDes = T000O20_n136Sistema_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A136Sistema_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void gxLoad_57( int A1399Sistema_ImpUserCod )
      {
         /* Using cursor T000O21 */
         pr_default.execute(19, new Object[] {n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            if ( ! ( (0==A1399Sistema_ImpUserCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_Usuario da Importa��o'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1402Sistema_ImpUserPesCod = T000O21_A1402Sistema_ImpUserPesCod[0];
         n1402Sistema_ImpUserPesCod = T000O21_n1402Sistema_ImpUserPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(19) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(19);
      }

      protected void gxLoad_59( int A1402Sistema_ImpUserPesCod )
      {
         /* Using cursor T000O22 */
         pr_default.execute(20, new Object[] {n1402Sistema_ImpUserPesCod, A1402Sistema_ImpUserPesCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            if ( ! ( (0==A1402Sistema_ImpUserPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1403Sistema_ImpUserPesNom = T000O22_A1403Sistema_ImpUserPesNom[0];
         n1403Sistema_ImpUserPesNom = T000O22_n1403Sistema_ImpUserPesNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1403Sistema_ImpUserPesNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void GetKey0O25( )
      {
         /* Using cursor T000O23 */
         pr_default.execute(21, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound25 = 1;
         }
         else
         {
            RcdFound25 = 0;
         }
         pr_default.close(21);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000O6 */
         pr_default.execute(4, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM0O25( 52) ;
            RcdFound25 = 1;
            A127Sistema_Codigo = T000O6_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A416Sistema_Nome = T000O6_A416Sistema_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
            A128Sistema_Descricao = T000O6_A128Sistema_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
            n128Sistema_Descricao = T000O6_n128Sistema_Descricao[0];
            A129Sistema_Sigla = T000O6_A129Sistema_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
            A513Sistema_Coordenacao = T000O6_A513Sistema_Coordenacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
            n513Sistema_Coordenacao = T000O6_n513Sistema_Coordenacao[0];
            A699Sistema_Tipo = T000O6_A699Sistema_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
            n699Sistema_Tipo = T000O6_n699Sistema_Tipo[0];
            A700Sistema_Tecnica = T000O6_A700Sistema_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
            n700Sistema_Tecnica = T000O6_n700Sistema_Tecnica[0];
            A686Sistema_FatorAjuste = T000O6_A686Sistema_FatorAjuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
            n686Sistema_FatorAjuste = T000O6_n686Sistema_FatorAjuste[0];
            A687Sistema_Custo = T000O6_A687Sistema_Custo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
            n687Sistema_Custo = T000O6_n687Sistema_Custo[0];
            A688Sistema_Prazo = T000O6_A688Sistema_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
            n688Sistema_Prazo = T000O6_n688Sistema_Prazo[0];
            A689Sistema_Esforco = T000O6_A689Sistema_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
            n689Sistema_Esforco = T000O6_n689Sistema_Esforco[0];
            A1401Sistema_ImpData = T000O6_A1401Sistema_ImpData[0];
            n1401Sistema_ImpData = T000O6_n1401Sistema_ImpData[0];
            A1831Sistema_Responsavel = T000O6_A1831Sistema_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1831Sistema_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0)));
            n1831Sistema_Responsavel = T000O6_n1831Sistema_Responsavel[0];
            A130Sistema_Ativo = T000O6_A130Sistema_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
            A2109Sistema_Repositorio = T000O6_A2109Sistema_Repositorio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2109Sistema_Repositorio", A2109Sistema_Repositorio);
            n2109Sistema_Repositorio = T000O6_n2109Sistema_Repositorio[0];
            A2130Sistema_Ultimo = T000O6_A2130Sistema_Ultimo[0];
            A137Metodologia_Codigo = T000O6_A137Metodologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
            n137Metodologia_Codigo = T000O6_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = T000O6_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            n351AmbienteTecnologico_Codigo = T000O6_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = T000O6_A1859SistemaVersao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            n1859SistemaVersao_Codigo = T000O6_n1859SistemaVersao_Codigo[0];
            A135Sistema_AreaTrabalhoCod = T000O6_A135Sistema_AreaTrabalhoCod[0];
            A1399Sistema_ImpUserCod = T000O6_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = T000O6_n1399Sistema_ImpUserCod[0];
            A2161Sistema_GpoObjCtrlCod = T000O6_A2161Sistema_GpoObjCtrlCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            n2161Sistema_GpoObjCtrlCod = T000O6_n2161Sistema_GpoObjCtrlCod[0];
            O2130Sistema_Ultimo = A2130Sistema_Ultimo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
            O416Sistema_Nome = A416Sistema_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
            O129Sistema_Sigla = A129Sistema_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
            O513Sistema_Coordenacao = A513Sistema_Coordenacao;
            n513Sistema_Coordenacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
            Z127Sistema_Codigo = A127Sistema_Codigo;
            sMode25 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0O25( ) ;
            if ( AnyError == 1 )
            {
               RcdFound25 = 0;
               InitializeNonKey0O25( ) ;
            }
            Gx_mode = sMode25;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound25 = 0;
            InitializeNonKey0O25( ) ;
            sMode25 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode25;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey0O25( ) ;
         if ( RcdFound25 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound25 = 0;
         /* Using cursor T000O24 */
         pr_default.execute(22, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(22) != 101) )
         {
            while ( (pr_default.getStatus(22) != 101) && ( ( T000O24_A127Sistema_Codigo[0] < A127Sistema_Codigo ) ) )
            {
               pr_default.readNext(22);
            }
            if ( (pr_default.getStatus(22) != 101) && ( ( T000O24_A127Sistema_Codigo[0] > A127Sistema_Codigo ) ) )
            {
               A127Sistema_Codigo = T000O24_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               RcdFound25 = 1;
            }
         }
         pr_default.close(22);
      }

      protected void move_previous( )
      {
         RcdFound25 = 0;
         /* Using cursor T000O25 */
         pr_default.execute(23, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            while ( (pr_default.getStatus(23) != 101) && ( ( T000O25_A127Sistema_Codigo[0] > A127Sistema_Codigo ) ) )
            {
               pr_default.readNext(23);
            }
            if ( (pr_default.getStatus(23) != 101) && ( ( T000O25_A127Sistema_Codigo[0] < A127Sistema_Codigo ) ) )
            {
               A127Sistema_Codigo = T000O25_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               RcdFound25 = 1;
            }
         }
         pr_default.close(23);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0O25( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A2130Sistema_Ultimo = O2130Sistema_Ultimo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
            GX_FocusControl = edtSistema_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0O25( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound25 == 1 )
            {
               if ( A127Sistema_Codigo != Z127Sistema_Codigo )
               {
                  A127Sistema_Codigo = Z127Sistema_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SISTEMA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSistema_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                  Update0O25( ) ;
                  GX_FocusControl = edtSistema_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A127Sistema_Codigo != Z127Sistema_Codigo )
               {
                  /* Insert record */
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                  GX_FocusControl = edtSistema_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0O25( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SISTEMA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSistema_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                     GX_FocusControl = edtSistema_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0O25( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A127Sistema_Codigo != Z127Sistema_Codigo )
         {
            A127Sistema_Codigo = Z127Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A2130Sistema_Ultimo = O2130Sistema_Ultimo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSistema_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0O25( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000O5 */
            pr_default.execute(3, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Sistema"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z416Sistema_Nome, T000O5_A416Sistema_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z129Sistema_Sigla, T000O5_A129Sistema_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z513Sistema_Coordenacao, T000O5_A513Sistema_Coordenacao[0]) != 0 ) || ( StringUtil.StrCmp(Z699Sistema_Tipo, T000O5_A699Sistema_Tipo[0]) != 0 ) || ( StringUtil.StrCmp(Z700Sistema_Tecnica, T000O5_A700Sistema_Tecnica[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z686Sistema_FatorAjuste != T000O5_A686Sistema_FatorAjuste[0] ) || ( Z687Sistema_Custo != T000O5_A687Sistema_Custo[0] ) || ( Z688Sistema_Prazo != T000O5_A688Sistema_Prazo[0] ) || ( Z689Sistema_Esforco != T000O5_A689Sistema_Esforco[0] ) || ( Z1401Sistema_ImpData != T000O5_A1401Sistema_ImpData[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1831Sistema_Responsavel != T000O5_A1831Sistema_Responsavel[0] ) || ( Z130Sistema_Ativo != T000O5_A130Sistema_Ativo[0] ) || ( StringUtil.StrCmp(Z2109Sistema_Repositorio, T000O5_A2109Sistema_Repositorio[0]) != 0 ) || ( Z2130Sistema_Ultimo != T000O5_A2130Sistema_Ultimo[0] ) || ( Z137Metodologia_Codigo != T000O5_A137Metodologia_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z351AmbienteTecnologico_Codigo != T000O5_A351AmbienteTecnologico_Codigo[0] ) || ( Z1859SistemaVersao_Codigo != T000O5_A1859SistemaVersao_Codigo[0] ) || ( Z135Sistema_AreaTrabalhoCod != T000O5_A135Sistema_AreaTrabalhoCod[0] ) || ( Z1399Sistema_ImpUserCod != T000O5_A1399Sistema_ImpUserCod[0] ) || ( Z2161Sistema_GpoObjCtrlCod != T000O5_A2161Sistema_GpoObjCtrlCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z416Sistema_Nome, T000O5_A416Sistema_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z416Sistema_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A416Sistema_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z129Sistema_Sigla, T000O5_A129Sistema_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z129Sistema_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A129Sistema_Sigla[0]);
               }
               if ( StringUtil.StrCmp(Z513Sistema_Coordenacao, T000O5_A513Sistema_Coordenacao[0]) != 0 )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Coordenacao");
                  GXUtil.WriteLogRaw("Old: ",Z513Sistema_Coordenacao);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A513Sistema_Coordenacao[0]);
               }
               if ( StringUtil.StrCmp(Z699Sistema_Tipo, T000O5_A699Sistema_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z699Sistema_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A699Sistema_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z700Sistema_Tecnica, T000O5_A700Sistema_Tecnica[0]) != 0 )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Tecnica");
                  GXUtil.WriteLogRaw("Old: ",Z700Sistema_Tecnica);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A700Sistema_Tecnica[0]);
               }
               if ( Z686Sistema_FatorAjuste != T000O5_A686Sistema_FatorAjuste[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_FatorAjuste");
                  GXUtil.WriteLogRaw("Old: ",Z686Sistema_FatorAjuste);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A686Sistema_FatorAjuste[0]);
               }
               if ( Z687Sistema_Custo != T000O5_A687Sistema_Custo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Custo");
                  GXUtil.WriteLogRaw("Old: ",Z687Sistema_Custo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A687Sistema_Custo[0]);
               }
               if ( Z688Sistema_Prazo != T000O5_A688Sistema_Prazo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z688Sistema_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A688Sistema_Prazo[0]);
               }
               if ( Z689Sistema_Esforco != T000O5_A689Sistema_Esforco[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Esforco");
                  GXUtil.WriteLogRaw("Old: ",Z689Sistema_Esforco);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A689Sistema_Esforco[0]);
               }
               if ( Z1401Sistema_ImpData != T000O5_A1401Sistema_ImpData[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_ImpData");
                  GXUtil.WriteLogRaw("Old: ",Z1401Sistema_ImpData);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A1401Sistema_ImpData[0]);
               }
               if ( Z1831Sistema_Responsavel != T000O5_A1831Sistema_Responsavel[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Responsavel");
                  GXUtil.WriteLogRaw("Old: ",Z1831Sistema_Responsavel);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A1831Sistema_Responsavel[0]);
               }
               if ( Z130Sistema_Ativo != T000O5_A130Sistema_Ativo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z130Sistema_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A130Sistema_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z2109Sistema_Repositorio, T000O5_A2109Sistema_Repositorio[0]) != 0 )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Repositorio");
                  GXUtil.WriteLogRaw("Old: ",Z2109Sistema_Repositorio);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A2109Sistema_Repositorio[0]);
               }
               if ( Z2130Sistema_Ultimo != T000O5_A2130Sistema_Ultimo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_Ultimo");
                  GXUtil.WriteLogRaw("Old: ",Z2130Sistema_Ultimo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A2130Sistema_Ultimo[0]);
               }
               if ( Z137Metodologia_Codigo != T000O5_A137Metodologia_Codigo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Metodologia_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z137Metodologia_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A137Metodologia_Codigo[0]);
               }
               if ( Z351AmbienteTecnologico_Codigo != T000O5_A351AmbienteTecnologico_Codigo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"AmbienteTecnologico_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z351AmbienteTecnologico_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A351AmbienteTecnologico_Codigo[0]);
               }
               if ( Z1859SistemaVersao_Codigo != T000O5_A1859SistemaVersao_Codigo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"SistemaVersao_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1859SistemaVersao_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A1859SistemaVersao_Codigo[0]);
               }
               if ( Z135Sistema_AreaTrabalhoCod != T000O5_A135Sistema_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z135Sistema_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A135Sistema_AreaTrabalhoCod[0]);
               }
               if ( Z1399Sistema_ImpUserCod != T000O5_A1399Sistema_ImpUserCod[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_ImpUserCod");
                  GXUtil.WriteLogRaw("Old: ",Z1399Sistema_ImpUserCod);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A1399Sistema_ImpUserCod[0]);
               }
               if ( Z2161Sistema_GpoObjCtrlCod != T000O5_A2161Sistema_GpoObjCtrlCod[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Sistema_GpoObjCtrlCod");
                  GXUtil.WriteLogRaw("Old: ",Z2161Sistema_GpoObjCtrlCod);
                  GXUtil.WriteLogRaw("Current: ",T000O5_A2161Sistema_GpoObjCtrlCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Sistema"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0O25( )
      {
         if ( ! IsAuthorized("sistema_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0O25( 0) ;
            CheckOptimisticConcurrency0O25( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O25( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0O25( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000O26 */
                     pr_default.execute(24, new Object[] {A416Sistema_Nome, n128Sistema_Descricao, A128Sistema_Descricao, A129Sistema_Sigla, n513Sistema_Coordenacao, A513Sistema_Coordenacao, n699Sistema_Tipo, A699Sistema_Tipo, n700Sistema_Tecnica, A700Sistema_Tecnica, n686Sistema_FatorAjuste, A686Sistema_FatorAjuste, n687Sistema_Custo, A687Sistema_Custo, n688Sistema_Prazo, A688Sistema_Prazo, n689Sistema_Esforco, A689Sistema_Esforco, n1401Sistema_ImpData, A1401Sistema_ImpData, n1831Sistema_Responsavel, A1831Sistema_Responsavel, A130Sistema_Ativo, n2109Sistema_Repositorio, A2109Sistema_Repositorio, A2130Sistema_Ultimo, n137Metodologia_Codigo, A137Metodologia_Codigo, n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo, n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo, A135Sistema_AreaTrabalhoCod, n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod, n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod});
                     A127Sistema_Codigo = T000O26_A127Sistema_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0O25( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption0O0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0O25( ) ;
            }
            EndLevel0O25( ) ;
         }
         CloseExtendedTableCursors0O25( ) ;
      }

      protected void Update0O25( )
      {
         if ( ! IsAuthorized("sistema_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O25( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O25( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0O25( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000O27 */
                     pr_default.execute(25, new Object[] {A416Sistema_Nome, n128Sistema_Descricao, A128Sistema_Descricao, A129Sistema_Sigla, n513Sistema_Coordenacao, A513Sistema_Coordenacao, n699Sistema_Tipo, A699Sistema_Tipo, n700Sistema_Tecnica, A700Sistema_Tecnica, n686Sistema_FatorAjuste, A686Sistema_FatorAjuste, n687Sistema_Custo, A687Sistema_Custo, n688Sistema_Prazo, A688Sistema_Prazo, n689Sistema_Esforco, A689Sistema_Esforco, n1401Sistema_ImpData, A1401Sistema_ImpData, n1831Sistema_Responsavel, A1831Sistema_Responsavel, A130Sistema_Ativo, n2109Sistema_Repositorio, A2109Sistema_Repositorio, A2130Sistema_Ultimo, n137Metodologia_Codigo, A137Metodologia_Codigo, n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo, n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo, A135Sistema_AreaTrabalhoCod, n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod, n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod, A127Sistema_Codigo});
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                     if ( (pr_default.getStatus(25) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Sistema"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0O25( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0O25( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0O25( ) ;
         }
         CloseExtendedTableCursors0O25( ) ;
      }

      protected void DeferredUpdate0O25( )
      {
      }

      protected void delete( )
      {
         if ( ! IsAuthorized("sistema_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate0O25( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0O25( ) ;
            AfterConfirm0O25( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0O25( ) ;
               if ( AnyError == 0 )
               {
                  A2130Sistema_Ultimo = O2130Sistema_Ultimo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                  ScanStart0O233( ) ;
                  while ( RcdFound233 != 0 )
                  {
                     getByPrimaryKey0O233( ) ;
                     Delete0O233( ) ;
                     ScanNext0O233( ) ;
                     O2130Sistema_Ultimo = A2130Sistema_Ultimo;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
                  }
                  ScanEnd0O233( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000O28 */
                     pr_default.execute(26, new Object[] {A127Sistema_Codigo});
                     pr_default.close(26);
                     dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode25 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0O25( ) ;
         Gx_mode = sMode25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0O25( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A395Sistema_PF = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
            /* Using cursor T000O29 */
            pr_default.execute(27, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            A352AmbienteTecnologico_Descricao = T000O29_A352AmbienteTecnologico_Descricao[0];
            pr_default.close(27);
            /* Using cursor T000O30 */
            pr_default.execute(28, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
            A138Metodologia_Descricao = T000O30_A138Metodologia_Descricao[0];
            pr_default.close(28);
            if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
            {
               A707Sistema_TipoSigla = "PD";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
            }
            else
            {
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
               {
                  A707Sistema_TipoSigla = "PM";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                  {
                     A707Sistema_TipoSigla = "PC";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                     {
                        A707Sistema_TipoSigla = "A";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                     }
                     else
                     {
                        A707Sistema_TipoSigla = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                     }
                  }
               }
            }
            A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
            GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
            GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
            new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
            A2161Sistema_GpoObjCtrlCod = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            A2162Sistema_GpoObjCtrlRsp = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2162Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0)));
            /* Using cursor T000O31 */
            pr_default.execute(29, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
            A1860SistemaVersao_Id = T000O31_A1860SistemaVersao_Id[0];
            pr_default.close(29);
            /* Using cursor T000O32 */
            pr_default.execute(30, new Object[] {A135Sistema_AreaTrabalhoCod});
            A136Sistema_AreaTrabalhoDes = T000O32_A136Sistema_AreaTrabalhoDes[0];
            n136Sistema_AreaTrabalhoDes = T000O32_n136Sistema_AreaTrabalhoDes[0];
            pr_default.close(30);
            /* Using cursor T000O33 */
            pr_default.execute(31, new Object[] {n1399Sistema_ImpUserCod, A1399Sistema_ImpUserCod});
            A1402Sistema_ImpUserPesCod = T000O33_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = T000O33_n1402Sistema_ImpUserPesCod[0];
            pr_default.close(31);
            /* Using cursor T000O34 */
            pr_default.execute(32, new Object[] {n1402Sistema_ImpUserPesCod, A1402Sistema_ImpUserPesCod});
            A1403Sistema_ImpUserPesNom = T000O34_A1403Sistema_ImpUserPesNom[0];
            n1403Sistema_ImpUserPesNom = T000O34_n1403Sistema_ImpUserPesNom[0];
            pr_default.close(32);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000O35 */
            pr_default.execute(33, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor T000O36 */
            pr_default.execute(34, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T207"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor T000O37 */
            pr_default.execute(35, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas atendidos pelo Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor T000O38 */
            pr_default.execute(36, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor T000O39 */
            pr_default.execute(37, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicita��o de Mudan�a"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor T000O40 */
            pr_default.execute(38, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor T000O41 */
            pr_default.execute(39, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor T000O42 */
            pr_default.execute(40, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor T000O43 */
            pr_default.execute(41, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo L�gico de Dados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor T000O44 */
            pr_default.execute(42, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fun��es APF - An�lise de Ponto de Fun��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor T000O45 */
            pr_default.execute(43, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor T000O46 */
            pr_default.execute(44, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor T000O47 */
            pr_default.execute(45, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo Funcoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor T000O48 */
            pr_default.execute(46, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
            /* Using cursor T000O49 */
            pr_default.execute(47, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(47) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(47);
            /* Using cursor T000O50 */
            pr_default.execute(48, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(48) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"De Para"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(48);
         }
      }

      protected void ProcessNestedLevel0O233( )
      {
         s2130Sistema_Ultimo = O2130Sistema_Ultimo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         nGXsfl_108_idx = 0;
         while ( nGXsfl_108_idx < nRC_GXsfl_108 )
         {
            ReadRow0O233( ) ;
            if ( ( nRcdExists_233 != 0 ) || ( nIsMod_233 != 0 ) )
            {
               standaloneNotModal0O233( ) ;
               GetKey0O233( ) ;
               if ( ( nRcdExists_233 == 0 ) && ( nRcdDeleted_233 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert0O233( ) ;
               }
               else
               {
                  if ( RcdFound233 != 0 )
                  {
                     if ( ( nRcdDeleted_233 != 0 ) && ( nRcdExists_233 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete0O233( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_233 != 0 ) && ( nRcdExists_233 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update0O233( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_233 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               O2130Sistema_Ultimo = A2130Sistema_Ultimo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
            }
            ChangePostValue( edtSistemaTecnologiaLinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2128SistemaTecnologiaLinha), 4, 0, ",", ""))) ;
            ChangePostValue( dynTecnologia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( cmbTecnologia_TipoTecnologia_Internalname, StringUtil.RTrim( A355Tecnologia_TipoTecnologia)) ;
            ChangePostValue( "ZT_"+"Z2128SistemaTecnologiaLinha_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2128SistemaTecnologiaLinha), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z131Tecnologia_Codigo_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z131Tecnologia_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_233_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_233), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_233_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_233), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_233_"+sGXsfl_108_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_233), 4, 0, ",", ""))) ;
            if ( nIsMod_233 != 0 )
            {
               ChangePostValue( "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TECNOLOGIA_CODIGO_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0O233( ) ;
         if ( AnyError != 0 )
         {
            O2130Sistema_Ultimo = s2130Sistema_Ultimo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         }
         nRcdExists_233 = 0;
         nIsMod_233 = 0;
         nRcdDeleted_233 = 0;
      }

      protected void ProcessLevel0O25( )
      {
         /* Save parent mode. */
         sMode25 = Gx_mode;
         ProcessNestedLevel0O233( ) ;
         if ( AnyError != 0 )
         {
            O2130Sistema_Ultimo = s2130Sistema_Ultimo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
         /* Using cursor T000O51 */
         pr_default.execute(49, new Object[] {A2130Sistema_Ultimo, A127Sistema_Codigo});
         pr_default.close(49);
         dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
      }

      protected void EndLevel0O25( )
      {
         pr_default.close(3);
         if ( AnyError == 0 )
         {
            BeforeComplete0O25( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(28);
            pr_default.close(27);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(32);
            pr_default.close(2);
            context.CommitDataStores( "Sistema");
            if ( AnyError == 0 )
            {
               ConfirmValues0O0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(28);
            pr_default.close(27);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(32);
            pr_default.close(2);
            context.RollbackDataStores( "Sistema");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0O25( )
      {
         /* Scan By routine */
         /* Using cursor T000O52 */
         pr_default.execute(50);
         RcdFound25 = 0;
         if ( (pr_default.getStatus(50) != 101) )
         {
            RcdFound25 = 1;
            A127Sistema_Codigo = T000O52_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0O25( )
      {
         /* Scan next routine */
         pr_default.readNext(50);
         RcdFound25 = 0;
         if ( (pr_default.getStatus(50) != 101) )
         {
            RcdFound25 = 1;
            A127Sistema_Codigo = T000O52_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0O25( )
      {
         pr_default.close(50);
      }

      protected void AfterConfirm0O25( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0O25( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0O25( )
      {
         /* Before Update Rules */
         new loadauditsistema(context ).execute(  "Y", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete0O25( )
      {
         /* Before Delete Rules */
         new loadauditsistema(context ).execute(  "Y", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete0O25( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditsistema(context ).execute(  "N", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditsistema(context ).execute(  "N", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate0O25( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && new prc_existenomecadastrado(context).executeUdp(  "Sis",  A416Sistema_Nome) ) || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A416Sistema_Nome, O416Sistema_Nome) != 0 ) && ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A416Sistema_Nome), new prc_padronizastring(context).executeUdp(  O416Sistema_Nome)) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "Sis",  A416Sistema_Nome) ) )
         {
            GX_msglist.addItem("Esse nome j� foi cadastrado!", 1, "SISTEMA_NOME");
            AnyError = 1;
            GX_FocusControl = edtSistema_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && new prc_existenomecadastrado(context).executeUdp(  "Sig",  A129Sistema_Sigla) ) || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A129Sistema_Sigla, O129Sistema_Sigla) != 0 ) && ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A129Sistema_Sigla), new prc_padronizastring(context).executeUdp(  O129Sistema_Sigla)) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "Sig",  A129Sistema_Sigla) ) )
         {
            GX_msglist.addItem("Sigla usada em outro Sistema!", 1, "SISTEMA_SIGLA");
            AnyError = 1;
            GX_FocusControl = edtSistema_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes0O25( )
      {
         edtSistema_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Nome_Enabled), 5, 0)));
         edtSistema_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Sigla_Enabled), 5, 0)));
         dynSistemaVersao_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistemaVersao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistemaVersao_Codigo.Enabled), 5, 0)));
         edtSistema_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Descricao_Enabled), 5, 0)));
         edtSistema_Coordenacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Coordenacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Coordenacao_Enabled), 5, 0)));
         edtSistema_Repositorio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Repositorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Repositorio_Enabled), 5, 0)));
         dynAmbienteTecnologico_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAmbienteTecnologico_Codigo.Enabled), 5, 0)));
         dynMetodologia_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMetodologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynMetodologia_Codigo.Enabled), 5, 0)));
         cmbSistema_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistema_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSistema_Tipo.Enabled), 5, 0)));
         cmbSistema_Tecnica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistema_Tecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSistema_Tecnica.Enabled), 5, 0)));
         edtSistema_FatorAjuste_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_FatorAjuste_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_FatorAjuste_Enabled), 5, 0)));
         edtSistema_Prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Prazo_Enabled), 5, 0)));
         edtSistema_Custo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Custo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Custo_Enabled), 5, 0)));
         edtSistema_Esforco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Esforco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Esforco_Enabled), 5, 0)));
         dynSistema_Responsavel.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistema_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistema_Responsavel.Enabled), 5, 0)));
         dynSistema_GpoObjCtrlCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSistema_GpoObjCtrlCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSistema_GpoObjCtrlCod.Enabled), 5, 0)));
         chkSistema_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkSistema_Ativo.Enabled), 5, 0)));
         edtavBlob_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBlob_Enabled), 5, 0)));
         edtavAba_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAba_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAba_Enabled), 5, 0)));
         edtavPralinha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPralinha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPralinha_Enabled), 5, 0)));
         edtavColsisnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColsisnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColsisnom_Enabled), 5, 0)));
         edtavColsissgl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColsissgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColsissgl_Enabled), 5, 0)));
         edtavColmodnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColmodnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColmodnom_Enabled), 5, 0)));
         edtavColmodsgl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColmodsgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColmodsgl_Enabled), 5, 0)));
         edtSistema_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
      }

      protected void ZM0O233( short GX_JID )
      {
         if ( ( GX_JID == 60 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z131Tecnologia_Codigo = T000O3_A131Tecnologia_Codigo[0];
            }
            else
            {
               Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            }
         }
         if ( GX_JID == -60 )
         {
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
            Z355Tecnologia_TipoTecnologia = A355Tecnologia_TipoTecnologia;
         }
      }

      protected void standaloneNotModal0O233( )
      {
         GXATECNOLOGIA_CODIGO_html0O233( ) ;
         edtSistemaTecnologiaLinha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaTecnologiaLinha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0)));
      }

      protected void standaloneModal0O233( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2130Sistema_Ultimo = (short)(O2130Sistema_Ultimo+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A2128SistemaTecnologiaLinha = A2130Sistema_Ultimo;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000O4 */
            pr_default.execute(2, new Object[] {A131Tecnologia_Codigo});
            A132Tecnologia_Nome = T000O4_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = T000O4_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = T000O4_n355Tecnologia_TipoTecnologia[0];
            pr_default.close(2);
         }
      }

      protected void Load0O233( )
      {
         /* Using cursor T000O53 */
         pr_default.execute(51, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
         if ( (pr_default.getStatus(51) != 101) )
         {
            RcdFound233 = 1;
            A132Tecnologia_Nome = T000O53_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = T000O53_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = T000O53_n355Tecnologia_TipoTecnologia[0];
            A131Tecnologia_Codigo = T000O53_A131Tecnologia_Codigo[0];
            ZM0O233( -60) ;
         }
         pr_default.close(51);
         OnLoadActions0O233( ) ;
      }

      protected void OnLoadActions0O233( )
      {
      }

      protected void CheckExtendedTable0O233( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal0O233( ) ;
         /* Using cursor T000O4 */
         pr_default.execute(2, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "TECNOLOGIA_CODIGO_" + sGXsfl_108_idx;
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A132Tecnologia_Nome = T000O4_A132Tecnologia_Nome[0];
         A355Tecnologia_TipoTecnologia = T000O4_A355Tecnologia_TipoTecnologia[0];
         n355Tecnologia_TipoTecnologia = T000O4_n355Tecnologia_TipoTecnologia[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0O233( )
      {
         pr_default.close(2);
      }

      protected void enableDisable0O233( )
      {
      }

      protected void gxLoad_61( int A131Tecnologia_Codigo )
      {
         /* Using cursor T000O54 */
         pr_default.execute(52, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(52) == 101) )
         {
            GXCCtl = "TECNOLOGIA_CODIGO_" + sGXsfl_108_idx;
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A132Tecnologia_Nome = T000O54_A132Tecnologia_Nome[0];
         A355Tecnologia_TipoTecnologia = T000O54_A355Tecnologia_TipoTecnologia[0];
         n355Tecnologia_TipoTecnologia = T000O54_n355Tecnologia_TipoTecnologia[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A132Tecnologia_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A355Tecnologia_TipoTecnologia))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(52) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(52);
      }

      protected void GetKey0O233( )
      {
         /* Using cursor T000O55 */
         pr_default.execute(53, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
         if ( (pr_default.getStatus(53) != 101) )
         {
            RcdFound233 = 1;
         }
         else
         {
            RcdFound233 = 0;
         }
         pr_default.close(53);
      }

      protected void getByPrimaryKey0O233( )
      {
         /* Using cursor T000O3 */
         pr_default.execute(1, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0O233( 60) ;
            RcdFound233 = 1;
            InitializeNonKey0O233( ) ;
            A2128SistemaTecnologiaLinha = T000O3_A2128SistemaTecnologiaLinha[0];
            A131Tecnologia_Codigo = T000O3_A131Tecnologia_Codigo[0];
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z2128SistemaTecnologiaLinha = A2128SistemaTecnologiaLinha;
            sMode233 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0O233( ) ;
            Gx_mode = sMode233;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound233 = 0;
            InitializeNonKey0O233( ) ;
            sMode233 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal0O233( ) ;
            Gx_mode = sMode233;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0O233( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0O233( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000O2 */
            pr_default.execute(0, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaTecnologia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z131Tecnologia_Codigo != T000O2_A131Tecnologia_Codigo[0] ) )
            {
               if ( Z131Tecnologia_Codigo != T000O2_A131Tecnologia_Codigo[0] )
               {
                  GXUtil.WriteLog("sistema:[seudo value changed for attri]"+"Tecnologia_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z131Tecnologia_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000O2_A131Tecnologia_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SistemaTecnologia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0O233( )
      {
         if ( ! IsAuthorized("sistema_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate0O233( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O233( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0O233( 0) ;
            CheckOptimisticConcurrency0O233( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O233( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0O233( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000O56 */
                     pr_default.execute(54, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha, A131Tecnologia_Codigo});
                     pr_default.close(54);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaTecnologia") ;
                     if ( (pr_default.getStatus(54) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0O233( ) ;
            }
            EndLevel0O233( ) ;
         }
         CloseExtendedTableCursors0O233( ) ;
      }

      protected void Update0O233( )
      {
         if ( ! IsAuthorized("sistema_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate0O233( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0O233( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O233( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0O233( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0O233( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000O57 */
                     pr_default.execute(55, new Object[] {A131Tecnologia_Codigo, A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
                     pr_default.close(55);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaTecnologia") ;
                     if ( (pr_default.getStatus(55) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaTecnologia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0O233( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey0O233( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0O233( ) ;
         }
         CloseExtendedTableCursors0O233( ) ;
      }

      protected void DeferredUpdate0O233( )
      {
      }

      protected void Delete0O233( )
      {
         if ( ! IsAuthorized("sistema_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate0O233( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0O233( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0O233( ) ;
            AfterConfirm0O233( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0O233( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000O58 */
                  pr_default.execute(56, new Object[] {A127Sistema_Codigo, A2128SistemaTecnologiaLinha});
                  pr_default.close(56);
                  dsDefault.SmartCacheProvider.SetUpdated("SistemaTecnologia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode233 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0O233( ) ;
         Gx_mode = sMode233;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0O233( )
      {
         standaloneModal0O233( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000O59 */
            pr_default.execute(57, new Object[] {A131Tecnologia_Codigo});
            A132Tecnologia_Nome = T000O59_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = T000O59_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = T000O59_n355Tecnologia_TipoTecnologia[0];
            pr_default.close(57);
         }
      }

      protected void EndLevel0O233( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0O233( )
      {
         /* Scan By routine */
         /* Using cursor T000O60 */
         pr_default.execute(58, new Object[] {A127Sistema_Codigo});
         RcdFound233 = 0;
         if ( (pr_default.getStatus(58) != 101) )
         {
            RcdFound233 = 1;
            A2128SistemaTecnologiaLinha = T000O60_A2128SistemaTecnologiaLinha[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0O233( )
      {
         /* Scan next routine */
         pr_default.readNext(58);
         RcdFound233 = 0;
         if ( (pr_default.getStatus(58) != 101) )
         {
            RcdFound233 = 1;
            A2128SistemaTecnologiaLinha = T000O60_A2128SistemaTecnologiaLinha[0];
         }
      }

      protected void ScanEnd0O233( )
      {
         pr_default.close(58);
      }

      protected void AfterConfirm0O233( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0O233( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0O233( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0O233( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0O233( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0O233( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0O233( )
      {
         edtSistemaTecnologiaLinha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaTecnologiaLinha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0)));
         dynTecnologia_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTecnologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0)));
         cmbTecnologia_TipoTecnologia.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0)));
      }

      protected void SubsflControlProps_108233( )
      {
         edtSistemaTecnologiaLinha_Internalname = "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_idx;
         dynTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO_"+sGXsfl_108_idx;
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_idx;
      }

      protected void SubsflControlProps_fel_108233( )
      {
         edtSistemaTecnologiaLinha_Internalname = "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_fel_idx;
         dynTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO_"+sGXsfl_108_fel_idx;
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_fel_idx;
      }

      protected void AddRow0O233( )
      {
         nGXsfl_108_idx = (short)(nGXsfl_108_idx+1);
         sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx), 4, 0)), 4, "0");
         SubsflControlProps_108233( ) ;
         SendRow0O233( ) ;
      }

      protected void SendRow0O233( )
      {
         Grid1Row = GXWebRow.GetNew(context);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_108_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaTecnologiaLinha_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2128SistemaTecnologiaLinha), 4, 0, ",", "")),((edtSistemaTecnologiaLinha_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2128SistemaTecnologiaLinha), "ZZZ9")) : context.localUtil.Format( (decimal)(A2128SistemaTecnologiaLinha), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaTecnologiaLinha_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtSistemaTecnologiaLinha_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)108,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         GXATECNOLOGIA_CODIGO_html0O233( ) ;
         /* Subfile cell */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_233_" + sGXsfl_108_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_108_idx + "',108)\"";
         if ( ( nGXsfl_108_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "TECNOLOGIA_CODIGO_" + sGXsfl_108_idx;
            dynTecnologia_Codigo.Name = GXCCtl;
            dynTecnologia_Codigo.WebTags = "";
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynTecnologia_Codigo,(String)dynTecnologia_Codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)),(short)1,(String)dynTecnologia_Codigo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,dynTecnologia_Codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"",(String)"",(bool)true});
         dynTecnologia_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTecnologia_Codigo_Internalname, "Values", (String)(dynTecnologia_Codigo.ToJavascriptSource()));
         /* Subfile cell */
         GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_108_idx;
         cmbTecnologia_TipoTecnologia.Name = GXCCtl;
         cmbTecnologia_TipoTecnologia.WebTags = "";
         cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
         cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
         cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
         cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
         cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
         cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
         cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
         cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
         cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
         cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
         if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
         {
            A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
            n355Tecnologia_TipoTecnologia = false;
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTecnologia_TipoTecnologia,(String)cmbTecnologia_TipoTecnologia_Internalname,StringUtil.RTrim( A355Tecnologia_TipoTecnologia),(short)1,(String)cmbTecnologia_TipoTecnologia_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbTecnologia_TipoTecnologia.Enabled,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
         cmbTecnologia_TipoTecnologia.CurrentValue = StringUtil.RTrim( A355Tecnologia_TipoTecnologia);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Values", (String)(cmbTecnologia_TipoTecnologia.ToJavascriptSource()));
         context.httpAjaxContext.ajax_sending_grid_row(Grid1Row);
         GXCCtl = "Z2128SistemaTecnologiaLinha_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2128SistemaTecnologiaLinha), 4, 0, ",", "")));
         GXCCtl = "Z131Tecnologia_Codigo_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z131Tecnologia_Codigo), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_233_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_233), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_233_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_233), 4, 0, ",", "")));
         GXCCtl = "nIsMod_233_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_233), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vAUDITINGOBJECT_" + sGXsfl_108_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV54AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV54AuditingObject);
         }
         GXCCtl = "vPGMNAME_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( AV60Pgmname));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_108_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vSISTEMA_CODIGO_" + sGXsfl_108_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_CODIGO_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTecnologia_TipoTecnologia.Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Grid1Container.AddRow(Grid1Row);
      }

      protected void ReadRow0O233( )
      {
         nGXsfl_108_idx = (short)(nGXsfl_108_idx+1);
         sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx), 4, 0)), 4, "0");
         SubsflControlProps_108233( ) ;
         edtSistemaTecnologiaLinha_Enabled = (int)(context.localUtil.CToN( cgiGet( "SISTEMATECNOLOGIALINHA_"+sGXsfl_108_idx+"Enabled"), ",", "."));
         dynTecnologia_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "TECNOLOGIA_CODIGO_"+sGXsfl_108_idx+"Enabled"), ",", "."));
         cmbTecnologia_TipoTecnologia.Enabled = (int)(context.localUtil.CToN( cgiGet( "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_108_idx+"Enabled"), ",", "."));
         A2128SistemaTecnologiaLinha = (short)(context.localUtil.CToN( cgiGet( edtSistemaTecnologiaLinha_Internalname), ",", "."));
         dynTecnologia_Codigo.Name = dynTecnologia_Codigo_Internalname;
         dynTecnologia_Codigo.CurrentValue = cgiGet( dynTecnologia_Codigo_Internalname);
         A131Tecnologia_Codigo = (int)(NumberUtil.Val( cgiGet( dynTecnologia_Codigo_Internalname), "."));
         cmbTecnologia_TipoTecnologia.Name = cmbTecnologia_TipoTecnologia_Internalname;
         cmbTecnologia_TipoTecnologia.CurrentValue = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
         A355Tecnologia_TipoTecnologia = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
         n355Tecnologia_TipoTecnologia = false;
         GXCCtl = "Z2128SistemaTecnologiaLinha_" + sGXsfl_108_idx;
         Z2128SistemaTecnologiaLinha = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z131Tecnologia_Codigo_" + sGXsfl_108_idx;
         Z131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_233_" + sGXsfl_108_idx;
         nRcdDeleted_233 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_233_" + sGXsfl_108_idx;
         nRcdExists_233 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_233_" + sGXsfl_108_idx;
         nIsMod_233 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtSistemaTecnologiaLinha_Enabled = edtSistemaTecnologiaLinha_Enabled;
      }

      protected void ConfirmValues0O0( )
      {
         nGXsfl_108_idx = 0;
         sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx), 4, 0)), 4, "0");
         SubsflControlProps_108233( ) ;
         while ( nGXsfl_108_idx < nRC_GXsfl_108 )
         {
            nGXsfl_108_idx = (short)(nGXsfl_108_idx+1);
            sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx), 4, 0)), 4, "0");
            SubsflControlProps_108233( ) ;
            ChangePostValue( "Z2128SistemaTecnologiaLinha_"+sGXsfl_108_idx, cgiGet( "ZT_"+"Z2128SistemaTecnologiaLinha_"+sGXsfl_108_idx)) ;
            DeletePostValue( "ZT_"+"Z2128SistemaTecnologiaLinha_"+sGXsfl_108_idx) ;
            ChangePostValue( "Z131Tecnologia_Codigo_"+sGXsfl_108_idx, cgiGet( "ZT_"+"Z131Tecnologia_Codigo_"+sGXsfl_108_idx)) ;
            DeletePostValue( "ZT_"+"Z131Tecnologia_Codigo_"+sGXsfl_108_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216162159");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Sistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z416Sistema_Nome", Z416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "Z129Sistema_Sigla", StringUtil.RTrim( Z129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "Z513Sistema_Coordenacao", Z513Sistema_Coordenacao);
         GxWebStd.gx_hidden_field( context, "Z699Sistema_Tipo", StringUtil.RTrim( Z699Sistema_Tipo));
         GxWebStd.gx_hidden_field( context, "Z700Sistema_Tecnica", StringUtil.RTrim( Z700Sistema_Tecnica));
         GxWebStd.gx_hidden_field( context, "Z686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.NToC( Z686Sistema_FatorAjuste, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z687Sistema_Custo", StringUtil.LTrim( StringUtil.NToC( Z687Sistema_Custo, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z688Sistema_Prazo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z688Sistema_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z689Sistema_Esforco", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z689Sistema_Esforco), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1401Sistema_ImpData", context.localUtil.TToC( Z1401Sistema_ImpData, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1831Sistema_Responsavel", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1831Sistema_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z130Sistema_Ativo", Z130Sistema_Ativo);
         GxWebStd.gx_hidden_field( context, "Z2109Sistema_Repositorio", Z2109Sistema_Repositorio);
         GxWebStd.gx_hidden_field( context, "Z2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2130Sistema_Ultimo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z137Metodologia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z137Metodologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1859SistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1399Sistema_ImpUserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1399Sistema_ImpUserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2161Sistema_GpoObjCtrlCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.NToC( (decimal)(O2130Sistema_Ultimo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O416Sistema_Nome", O416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "O129Sistema_Sigla", StringUtil.RTrim( O129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "O513Sistema_Coordenacao", O513Sistema_Coordenacao);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_108", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_108_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N137Metodologia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1399Sistema_ImpUserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1399Sistema_ImpUserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "SISTEMA_GPOOBJCTRLRSP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_PF", StringUtil.LTrim( StringUtil.NToC( A395Sistema_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_PFA", StringUtil.LTrim( StringUtil.NToC( A690Sistema_PFA, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_TIPOSIGLA", StringUtil.RTrim( A707Sistema_TipoSigla));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_AMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Insert_AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_METODOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Metodologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMA_IMPUSERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Insert_Sistema_ImpUserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_IMPUSERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1399Sistema_ImpUserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMA_GPOOBJCTRLCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58Insert_Sistema_GpoObjCtrlCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56Insert_SistemaVersao_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV54AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV54AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "SISTEMA_IMPDATA", context.localUtil.TToC( A1401Sistema_ImpData, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "SISTEMA_ULTIMO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2130Sistema_Ultimo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIA_DESCRICAO", A138Metodologia_Descricao);
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_DESCRICAO", A352AmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_ID", StringUtil.RTrim( A1860SistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, "SISTEMA_AREATRABALHODES", A136Sistema_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "SISTEMA_IMPUSERPESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_IMPUSERPESNOM", StringUtil.RTrim( A1403Sistema_ImpUserPesNom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV60Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_NOME", StringUtil.RTrim( A132Tecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Width", StringUtil.RTrim( Dvpanel_unnamedtable1_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Cls", StringUtil.RTrim( Dvpanel_unnamedtable1_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Title", StringUtil.RTrim( Dvpanel_unnamedtable1_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Enabled", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable1_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Width", StringUtil.RTrim( Gxuitabspanel_tabmain_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Cls", StringUtil.RTrim( Gxuitabspanel_tabmain_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Enabled", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabmain_Designtimetabs));
         GxWebStd.gx_hidden_field( context, "BLOB_Filename", StringUtil.RTrim( edtavBlob_Filename));
         GxWebStd.gx_hidden_field( context, "BLOB_Filetype", StringUtil.RTrim( edtavBlob_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Sistema";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A135Sistema_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1399Sistema_ImpUserCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV60Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("sistema:[SendSecurityCheck value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("sistema:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("sistema:[SendSecurityCheck value for]"+"Sistema_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A135Sistema_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("sistema:[SendSecurityCheck value for]"+"Sistema_ImpUserCod:"+context.localUtil.Format( (decimal)(A1399Sistema_ImpUserCod), "ZZZZZ9"));
         GXUtil.WriteLog("sistema:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV60Pgmname, "")));
         GXUtil.WriteLog("sistema:[SendSecurityCheck value for]"+"Sistema_ImpData:"+context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Sistema_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Sistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema" ;
      }

      protected void InitializeNonKey0O25( )
      {
         A351AmbienteTecnologico_Codigo = 0;
         n351AmbienteTecnologico_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         n351AmbienteTecnologico_Codigo = ((0==A351AmbienteTecnologico_Codigo) ? true : false);
         A137Metodologia_Codigo = 0;
         n137Metodologia_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A137Metodologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A137Metodologia_Codigo), 6, 0)));
         n137Metodologia_Codigo = ((0==A137Metodologia_Codigo) ? true : false);
         A1399Sistema_ImpUserCod = 0;
         n1399Sistema_ImpUserCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1399Sistema_ImpUserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1399Sistema_ImpUserCod), 6, 0)));
         A2161Sistema_GpoObjCtrlCod = 0;
         n2161Sistema_GpoObjCtrlCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         n2161Sistema_GpoObjCtrlCod = ((0==A2161Sistema_GpoObjCtrlCod) ? true : false);
         A1859SistemaVersao_Codigo = 0;
         n1859SistemaVersao_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         n1859SistemaVersao_Codigo = ((0==A1859SistemaVersao_Codigo) ? true : false);
         AV42ColSisNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ColSisNom", AV42ColSisNom);
         AV43ColSisSgl = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ColSisSgl", AV43ColSisSgl);
         AV44ColModNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ColModNom", AV44ColModNom);
         AV45ColModSgl = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ColModSgl", AV45ColModSgl);
         AV48Blob = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV48Blob));
         AV47Aba = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Aba", AV47Aba);
         AV54AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A707Sistema_TipoSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
         A690Sistema_PFA = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
         A395Sistema_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
         A416Sistema_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
         A128Sistema_Descricao = "";
         n128Sistema_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
         n128Sistema_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A128Sistema_Descricao)) ? true : false);
         A129Sistema_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
         A513Sistema_Coordenacao = "";
         n513Sistema_Coordenacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
         n513Sistema_Coordenacao = (String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) ? true : false);
         A136Sistema_AreaTrabalhoDes = "";
         n136Sistema_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A136Sistema_AreaTrabalhoDes", A136Sistema_AreaTrabalhoDes);
         A352AmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
         A138Metodologia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
         A700Sistema_Tecnica = "";
         n700Sistema_Tecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
         n700Sistema_Tecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A700Sistema_Tecnica)) ? true : false);
         A687Sistema_Custo = 0;
         n687Sistema_Custo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
         n687Sistema_Custo = ((Convert.ToDecimal(0)==A687Sistema_Custo) ? true : false);
         A688Sistema_Prazo = 0;
         n688Sistema_Prazo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
         n688Sistema_Prazo = ((0==A688Sistema_Prazo) ? true : false);
         A689Sistema_Esforco = 0;
         n689Sistema_Esforco = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
         n689Sistema_Esforco = ((0==A689Sistema_Esforco) ? true : false);
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         n1401Sistema_ImpData = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1401Sistema_ImpData", context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " "));
         A1402Sistema_ImpUserPesCod = 0;
         n1402Sistema_ImpUserPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1402Sistema_ImpUserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1402Sistema_ImpUserPesCod), 6, 0)));
         A1403Sistema_ImpUserPesNom = "";
         n1403Sistema_ImpUserPesNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1403Sistema_ImpUserPesNom", A1403Sistema_ImpUserPesNom);
         A1831Sistema_Responsavel = 0;
         n1831Sistema_Responsavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1831Sistema_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1831Sistema_Responsavel), 6, 0)));
         n1831Sistema_Responsavel = ((0==A1831Sistema_Responsavel) ? true : false);
         A2162Sistema_GpoObjCtrlRsp = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2162Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0)));
         A1860SistemaVersao_Id = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
         A2109Sistema_Repositorio = "";
         n2109Sistema_Repositorio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2109Sistema_Repositorio", A2109Sistema_Repositorio);
         n2109Sistema_Repositorio = (String.IsNullOrEmpty(StringUtil.RTrim( A2109Sistema_Repositorio)) ? true : false);
         A2130Sistema_Ultimo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         A135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
         A699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
         A686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
         A130Sistema_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
         O2130Sistema_Ultimo = A2130Sistema_Ultimo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
         O416Sistema_Nome = A416Sistema_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
         O129Sistema_Sigla = A129Sistema_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
         O513Sistema_Coordenacao = A513Sistema_Coordenacao;
         n513Sistema_Coordenacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
         Z416Sistema_Nome = "";
         Z129Sistema_Sigla = "";
         Z513Sistema_Coordenacao = "";
         Z699Sistema_Tipo = "";
         Z700Sistema_Tecnica = "";
         Z686Sistema_FatorAjuste = 0;
         Z687Sistema_Custo = 0;
         Z688Sistema_Prazo = 0;
         Z689Sistema_Esforco = 0;
         Z1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         Z1831Sistema_Responsavel = 0;
         Z130Sistema_Ativo = false;
         Z2109Sistema_Repositorio = "";
         Z2130Sistema_Ultimo = 0;
         Z137Metodologia_Codigo = 0;
         Z351AmbienteTecnologico_Codigo = 0;
         Z1859SistemaVersao_Codigo = 0;
         Z135Sistema_AreaTrabalhoCod = 0;
         Z1399Sistema_ImpUserCod = 0;
         Z2161Sistema_GpoObjCtrlCod = 0;
      }

      protected void InitAll0O25( )
      {
         A127Sistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         InitializeNonKey0O25( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A135Sistema_AreaTrabalhoCod = i135Sistema_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
         A130Sistema_Ativo = i130Sistema_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
         A699Sistema_Tipo = i699Sistema_Tipo;
         n699Sistema_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
         A686Sistema_FatorAjuste = i686Sistema_FatorAjuste;
         n686Sistema_FatorAjuste = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
      }

      protected void InitializeNonKey0O233( )
      {
         A131Tecnologia_Codigo = 0;
         A132Tecnologia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
         A355Tecnologia_TipoTecnologia = "";
         n355Tecnologia_TipoTecnologia = false;
         Z131Tecnologia_Codigo = 0;
      }

      protected void InitAll0O233( )
      {
         A2128SistemaTecnologiaLinha = 0;
         InitializeNonKey0O233( ) ;
      }

      protected void StandaloneModalInsert0O233( )
      {
         A2130Sistema_Ultimo = i2130Sistema_Ultimo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2130Sistema_Ultimo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2130Sistema_Ultimo), 4, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216162256");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("sistema.js", "?20206216162256");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties233( )
      {
         edtSistemaTecnologiaLinha_Enabled = defedtSistemaTecnologiaLinha_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaTecnologiaLinha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaTecnologiaLinha_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblTextblocksistema_nome_Internalname = "TEXTBLOCKSISTEMA_NOME";
         edtSistema_Nome_Internalname = "SISTEMA_NOME";
         lblTextblocksistema_sigla_Internalname = "TEXTBLOCKSISTEMA_SIGLA";
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA";
         lblTextblocksistemaversao_codigo_Internalname = "TEXTBLOCKSISTEMAVERSAO_CODIGO";
         dynSistemaVersao_Codigo_Internalname = "SISTEMAVERSAO_CODIGO";
         lblTextblocksistema_descricao_Internalname = "TEXTBLOCKSISTEMA_DESCRICAO";
         edtSistema_Descricao_Internalname = "SISTEMA_DESCRICAO";
         lblTextblocksistema_coordenacao_Internalname = "TEXTBLOCKSISTEMA_COORDENACAO";
         edtSistema_Coordenacao_Internalname = "SISTEMA_COORDENACAO";
         lblTextblocksistema_repositorio_Internalname = "TEXTBLOCKSISTEMA_REPOSITORIO";
         edtSistema_Repositorio_Internalname = "SISTEMA_REPOSITORIO";
         lblTextblockambientetecnologico_codigo_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_CODIGO";
         dynAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO";
         lblTextblockmetodologia_codigo_Internalname = "TEXTBLOCKMETODOLOGIA_CODIGO";
         dynMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO";
         lblTextblocksistema_tipo_Internalname = "TEXTBLOCKSISTEMA_TIPO";
         cmbSistema_Tipo_Internalname = "SISTEMA_TIPO";
         lblTextblocksistema_tecnica_Internalname = "TEXTBLOCKSISTEMA_TECNICA";
         cmbSistema_Tecnica_Internalname = "SISTEMA_TECNICA";
         lblTextblocksistema_fatorajuste_Internalname = "TEXTBLOCKSISTEMA_FATORAJUSTE";
         edtSistema_FatorAjuste_Internalname = "SISTEMA_FATORAJUSTE";
         lblTextblocksistema_prazo_Internalname = "TEXTBLOCKSISTEMA_PRAZO";
         edtSistema_Prazo_Internalname = "SISTEMA_PRAZO";
         lblTextblocksistema_custo_Internalname = "TEXTBLOCKSISTEMA_CUSTO";
         edtSistema_Custo_Internalname = "SISTEMA_CUSTO";
         lblTextblocksistema_esforco_Internalname = "TEXTBLOCKSISTEMA_ESFORCO";
         edtSistema_Esforco_Internalname = "SISTEMA_ESFORCO";
         lblTextblocksistema_responsavel_Internalname = "TEXTBLOCKSISTEMA_RESPONSAVEL";
         dynSistema_Responsavel_Internalname = "SISTEMA_RESPONSAVEL";
         lblTextblocksistema_gpoobjctrlcod_Internalname = "TEXTBLOCKSISTEMA_GPOOBJCTRLCOD";
         dynSistema_GpoObjCtrlCod_Internalname = "SISTEMA_GPOOBJCTRLCOD";
         lblTextblocksistema_ativo_Internalname = "TEXTBLOCKSISTEMA_ATIVO";
         chkSistema_Ativo_Internalname = "SISTEMA_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblUnnamedtable7_Internalname = "UNNAMEDTABLE7";
         edtSistemaTecnologiaLinha_Internalname = "SISTEMATECNOLOGIALINHA";
         dynTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO";
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA";
         tblUnnamedtable6_Internalname = "UNNAMEDTABLE6";
         edtavBlob_Internalname = "vBLOB";
         div_Internalname = "";
         edtavAba_Internalname = "vABA";
         div_Internalname = "";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         lblTextblockpralinha_Internalname = "TEXTBLOCKPRALINHA";
         div_Internalname = "";
         div_Internalname = "";
         edtavPralinha_Internalname = "vPRALINHA";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablepralinha_Internalname = "UNNAMEDTABLEPRALINHA";
         lblTextblockcolsisnom_Internalname = "TEXTBLOCKCOLSISNOM";
         div_Internalname = "";
         div_Internalname = "";
         edtavColsisnom_Internalname = "vCOLSISNOM";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablecolsisnom_Internalname = "UNNAMEDTABLECOLSISNOM";
         lblTextblockcolsissgl_Internalname = "TEXTBLOCKCOLSISSGL";
         div_Internalname = "";
         div_Internalname = "";
         edtavColsissgl_Internalname = "vCOLSISSGL";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablecolsissgl_Internalname = "UNNAMEDTABLECOLSISSGL";
         lblTextblockcolmodnom_Internalname = "TEXTBLOCKCOLMODNOM";
         div_Internalname = "";
         div_Internalname = "";
         edtavColmodnom_Internalname = "vCOLMODNOM";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablecolmodnom_Internalname = "UNNAMEDTABLECOLMODNOM";
         lblTextblockcolmodsgl_Internalname = "TEXTBLOCKCOLMODSGL";
         div_Internalname = "";
         div_Internalname = "";
         edtavColmodsgl_Internalname = "vCOLMODSGL";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablecolmodsgl_Internalname = "UNNAMEDTABLECOLMODSGL";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         edtavFilename_Internalname = "vFILENAME";
         div_Internalname = "";
         bttBtnimportar_Internalname = "BTNIMPORTAR";
         tblUnnamedtable5_Internalname = "UNNAMEDTABLE5";
         tblTblimportar_Internalname = "TBLIMPORTAR";
         div_Internalname = "";
         div_Internalname = "";
         bttBtnimportarexcel_Internalname = "BTNIMPORTAREXCEL";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         div_Internalname = "";
         divLayout_unnamedtable1_Internalname = "LAYOUT_UNNAMEDTABLE1";
         Dvpanel_unnamedtable1_Internalname = "DVPANEL_UNNAMEDTABLE1";
         Gxuitabspanel_tabmain_Internalname = "GXUITABSPANEL_TABMAIN";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtncopiarcolar_Internalname = "BTNCOPIARCOLAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTable1_Internalname = "TABLE1";
         tblTablemain_Internalname = "TABLEMAIN";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavBlob_Filename = "";
         Gxuitabspanel_tabmain_Designtimetabs = "[{\"id\":\"Tab1\"},{\"id\":\"TAB2\"},{\"id\":\"TAB3\"}]";
         Gxuitabspanel_tabmain_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabmain_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tabmain_Width = "100%";
         Dvpanel_unnamedtable1_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Iconposition = "left";
         Dvpanel_unnamedtable1_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Title = "";
         Dvpanel_unnamedtable1_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable1_Width = "100%";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Sistema";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Sistema";
         cmbTecnologia_TipoTecnologia_Jsonclick = "";
         dynTecnologia_Codigo_Jsonclick = "";
         edtSistemaTecnologiaLinha_Jsonclick = "";
         subGrid1_Class = "WorkWithBorder WorkWith";
         chkSistema_Ativo.Enabled = 1;
         chkSistema_Ativo.Visible = 1;
         lblTextblocksistema_ativo_Visible = 1;
         dynSistema_GpoObjCtrlCod_Jsonclick = "";
         dynSistema_GpoObjCtrlCod.Enabled = 1;
         dynSistema_Responsavel_Jsonclick = "";
         dynSistema_Responsavel.Enabled = 1;
         edtSistema_Esforco_Jsonclick = "";
         edtSistema_Esforco_Enabled = 1;
         edtSistema_Custo_Jsonclick = "";
         edtSistema_Custo_Enabled = 1;
         edtSistema_Prazo_Jsonclick = "";
         edtSistema_Prazo_Enabled = 1;
         edtSistema_FatorAjuste_Jsonclick = "";
         edtSistema_FatorAjuste_Enabled = 1;
         cmbSistema_Tecnica_Jsonclick = "";
         cmbSistema_Tecnica.Enabled = 1;
         cmbSistema_Tipo_Jsonclick = "";
         cmbSistema_Tipo.Enabled = 1;
         dynMetodologia_Codigo_Jsonclick = "";
         dynMetodologia_Codigo.Enabled = 1;
         dynAmbienteTecnologico_Codigo_Jsonclick = "";
         dynAmbienteTecnologico_Codigo.Enabled = 1;
         edtSistema_Repositorio_Enabled = 1;
         edtSistema_Coordenacao_Jsonclick = "";
         edtSistema_Coordenacao_Width = 50;
         edtSistema_Coordenacao_Enabled = 1;
         edtSistema_Descricao_Enabled = 1;
         dynSistemaVersao_Codigo_Jsonclick = "";
         dynSistemaVersao_Codigo.Enabled = 1;
         edtSistema_Sigla_Jsonclick = "";
         edtSistema_Sigla_Enabled = 1;
         edtSistema_Nome_Jsonclick = "";
         edtSistema_Nome_Enabled = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         cmbTecnologia_TipoTecnologia.Enabled = 0;
         dynTecnologia_Codigo.Enabled = 1;
         edtSistemaTecnologiaLinha_Enabled = 0;
         subGrid1_Backcolorstyle = 3;
         edtavAba_Jsonclick = "";
         edtavAba_Enabled = 1;
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         edtavBlob_Display = 0;
         edtavBlob_Enabled = 1;
         edtavColmodsgl_Jsonclick = "";
         edtavColmodsgl_Enabled = 1;
         edtavColmodnom_Jsonclick = "";
         edtavColmodnom_Enabled = 1;
         edtavColsissgl_Jsonclick = "";
         edtavColsissgl_Enabled = 1;
         edtavColsisnom_Jsonclick = "";
         edtavColsisnom_Enabled = 1;
         edtavPralinha_Jsonclick = "";
         edtavPralinha_Enabled = 1;
         bttBtnimportar_Visible = 1;
         edtavFilename_Jsonclick = "";
         edtavFilename_Enabled = 0;
         tblTblimportar_Visible = 1;
         bttBtnimportarexcel_Visible = 1;
         bttBtncopiarcolar_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtSistema_Codigo_Jsonclick = "";
         edtSistema_Codigo_Enabled = 0;
         edtSistema_Codigo_Visible = 1;
         chkSistema_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLASISTEMA_GPOOBJCTRLCOD0O1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASISTEMA_GPOOBJCTRLCOD_data0O1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASISTEMA_GPOOBJCTRLCOD_html0O1( )
      {
         int gxdynajaxvalue ;
         GXDLASISTEMA_GPOOBJCTRLCOD_data0O1( ) ;
         gxdynajaxindex = 1;
         dynSistema_GpoObjCtrlCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynSistema_GpoObjCtrlCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASISTEMA_GPOOBJCTRLCOD_data0O1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T000O61 */
         pr_default.execute(59);
         while ( (pr_default.getStatus(59) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000O61_A1826GpoObjCtrl_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000O61_A1827GpoObjCtrl_Nome[0]));
            pr_default.readNext(59);
         }
         pr_default.close(59);
      }

      protected void GXDLASISTEMAVERSAO_CODIGO0O25( int AV7Sistema_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASISTEMAVERSAO_CODIGO_data0O25( AV7Sistema_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASISTEMAVERSAO_CODIGO_html0O25( int AV7Sistema_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLASISTEMAVERSAO_CODIGO_data0O25( AV7Sistema_Codigo) ;
         gxdynajaxindex = 1;
         dynSistemaVersao_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynSistemaVersao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASISTEMAVERSAO_CODIGO_data0O25( int AV7Sistema_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T000O62 */
         pr_default.execute(60, new Object[] {AV7Sistema_Codigo});
         while ( (pr_default.getStatus(60) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000O62_A1859SistemaVersao_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000O62_A1860SistemaVersao_Id[0]));
            pr_default.readNext(60);
         }
         pr_default.close(60);
      }

      protected void GXDLAAMBIENTETECNOLOGICO_CODIGO0O25( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAAMBIENTETECNOLOGICO_CODIGO_data0O25( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAAMBIENTETECNOLOGICO_CODIGO_html0O25( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLAAMBIENTETECNOLOGICO_CODIGO_data0O25( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynAmbienteTecnologico_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAmbienteTecnologico_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAAMBIENTETECNOLOGICO_CODIGO_data0O25( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000O63 */
         pr_default.execute(61, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(61) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000O63_A351AmbienteTecnologico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T000O63_A352AmbienteTecnologico_Descricao[0]);
            pr_default.readNext(61);
         }
         pr_default.close(61);
      }

      protected void GXDLAMETODOLOGIA_CODIGO0O25( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAMETODOLOGIA_CODIGO_data0O25( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAMETODOLOGIA_CODIGO_html0O25( )
      {
         int gxdynajaxvalue ;
         GXDLAMETODOLOGIA_CODIGO_data0O25( ) ;
         gxdynajaxindex = 1;
         dynMetodologia_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynMetodologia_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAMETODOLOGIA_CODIGO_data0O25( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T000O64 */
         pr_default.execute(62);
         while ( (pr_default.getStatus(62) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000O64_A137Metodologia_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T000O64_A138Metodologia_Descricao[0]);
            pr_default.readNext(62);
         }
         pr_default.close(62);
      }

      protected void GXDLASISTEMA_RESPONSAVEL0O25( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASISTEMA_RESPONSAVEL_data0O25( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASISTEMA_RESPONSAVEL_html0O25( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLASISTEMA_RESPONSAVEL_data0O25( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynSistema_Responsavel.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynSistema_Responsavel.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASISTEMA_RESPONSAVEL_data0O25( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000O66 */
         pr_default.execute(63, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(63) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000O66_A60ContratanteUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000O66_A62ContratanteUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(63);
         }
         pr_default.close(63);
      }

      protected void GXDLATECNOLOGIA_CODIGO0O233( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATECNOLOGIA_CODIGO_data0O233( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATECNOLOGIA_CODIGO_html0O233( )
      {
         int gxdynajaxvalue ;
         GXDLATECNOLOGIA_CODIGO_data0O233( ) ;
         gxdynajaxindex = 1;
         dynTecnologia_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTecnologia_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATECNOLOGIA_CODIGO_data0O233( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000O67 */
         pr_default.execute(64);
         while ( (pr_default.getStatus(64) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000O67_A131Tecnologia_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000O67_A132Tecnologia_Nome[0]));
            pr_default.readNext(64);
         }
         pr_default.close(64);
      }

      protected void GX7ASASISTEMA_GPOOBJCTRLRSP0O25( int A2161Sistema_GpoObjCtrlCod )
      {
         GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
         GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
         A2161Sistema_GpoObjCtrlCod = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
         A2162Sistema_GpoObjCtrlRsp = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2162Sistema_GpoObjCtrlRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX8ASASISTEMA_PF0O25( int A127Sistema_Codigo )
      {
         GXt_decimal1 = A395Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         A395Sistema_PF = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A395Sistema_PF, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX29ASASISTEMA_FATORAJUSTE0O25( short Gx_BScreen ,
                                                     String Gx_mode )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A686Sistema_FatorAjuste) && ( Gx_BScreen == 0 ) )
         {
            GXt_decimal1 = A686Sistema_FatorAjuste;
            new prc_fapadrao(context ).execute( out  GXt_decimal1) ;
            A686Sistema_FatorAjuste = GXt_decimal1;
            n686Sistema_FatorAjuste = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A686Sistema_FatorAjuste, 6, 2, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_42_0O25( wwpbaseobjects.SdtAuditingObject AV54AuditingObject ,
                                 int A127Sistema_Codigo ,
                                 String Gx_mode )
      {
         new loadauditsistema(context ).execute(  "Y", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV54AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_43_0O25( wwpbaseobjects.SdtAuditingObject AV54AuditingObject ,
                                 int A127Sistema_Codigo ,
                                 String Gx_mode )
      {
         new loadauditsistema(context ).execute(  "Y", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV54AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_44_0O25( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV54AuditingObject ,
                                 int A127Sistema_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditsistema(context ).execute(  "N", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV54AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_45_0O25( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV54AuditingObject ,
                                 int A127Sistema_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditsistema(context ).execute(  "N", ref  AV54AuditingObject,  A127Sistema_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV54AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_108233( ) ;
         while ( nGXsfl_108_idx <= nRC_GXsfl_108 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal0O233( ) ;
            standaloneModal0O233( ) ;
            dynSistemaVersao_Codigo.Name = "SISTEMAVERSAO_CODIGO";
            dynSistemaVersao_Codigo.WebTags = "";
            dynAmbienteTecnologico_Codigo.Name = "AMBIENTETECNOLOGICO_CODIGO";
            dynAmbienteTecnologico_Codigo.WebTags = "";
            dynMetodologia_Codigo.Name = "METODOLOGIA_CODIGO";
            dynMetodologia_Codigo.WebTags = "";
            cmbSistema_Tipo.Name = "SISTEMA_TIPO";
            cmbSistema_Tipo.WebTags = "";
            cmbSistema_Tipo.addItem("", "(Nenhum)", 0);
            cmbSistema_Tipo.addItem("D", "Desenvolvimento", 0);
            cmbSistema_Tipo.addItem("M", "Melhoria", 0);
            cmbSistema_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbSistema_Tipo.ItemCount > 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A699Sistema_Tipo)) )
               {
                  A699Sistema_Tipo = "A";
                  n699Sistema_Tipo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
               }
               A699Sistema_Tipo = cmbSistema_Tipo.getValidValue(A699Sistema_Tipo);
               n699Sistema_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
            }
            cmbSistema_Tecnica.Name = "SISTEMA_TECNICA";
            cmbSistema_Tecnica.WebTags = "";
            cmbSistema_Tecnica.addItem("", "(Nenhum)", 0);
            cmbSistema_Tecnica.addItem("I", "Indicativa", 0);
            cmbSistema_Tecnica.addItem("E", "Estimada", 0);
            cmbSistema_Tecnica.addItem("D", "Detalhada", 0);
            if ( cmbSistema_Tecnica.ItemCount > 0 )
            {
               A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
               n700Sistema_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
            }
            dynSistema_Responsavel.Name = "SISTEMA_RESPONSAVEL";
            dynSistema_Responsavel.WebTags = "";
            dynSistema_GpoObjCtrlCod.Name = "SISTEMA_GPOOBJCTRLCOD";
            dynSistema_GpoObjCtrlCod.WebTags = "";
            dynSistema_GpoObjCtrlCod.removeAllItems();
            /* Using cursor T000O68 */
            pr_default.execute(65);
            while ( (pr_default.getStatus(65) != 101) )
            {
               dynSistema_GpoObjCtrlCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T000O68_A1826GpoObjCtrl_Codigo[0]), 6, 0)), T000O68_A1827GpoObjCtrl_Nome[0], 0);
               pr_default.readNext(65);
            }
            pr_default.close(65);
            if ( dynSistema_GpoObjCtrlCod.ItemCount > 0 )
            {
               A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( dynSistema_GpoObjCtrlCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0))), "."));
               n2161Sistema_GpoObjCtrlCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2161Sistema_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2161Sistema_GpoObjCtrlCod), 6, 0)));
            }
            chkSistema_Ativo.Name = "SISTEMA_ATIVO";
            chkSistema_Ativo.WebTags = "";
            chkSistema_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "TitleCaption", chkSistema_Ativo.Caption);
            chkSistema_Ativo.CheckedValue = "false";
            GXCCtl = "TECNOLOGIA_CODIGO_" + sGXsfl_108_idx;
            dynTecnologia_Codigo.Name = GXCCtl;
            dynTecnologia_Codigo.WebTags = "";
            GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_108_idx;
            cmbTecnologia_TipoTecnologia.Name = GXCCtl;
            cmbTecnologia_TipoTecnologia.WebTags = "";
            cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
            cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
            cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
            cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
            cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
            cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
            cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
            cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
            cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
            cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
            if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
            {
               A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
               n355Tecnologia_TipoTecnologia = false;
            }
            dynload_actions( ) ;
            SendRow0O233( ) ;
            nGXsfl_108_idx = (short)(nGXsfl_108_idx+1);
            sGXsfl_108_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_108_idx), 4, 0)), 4, "0");
            SubsflControlProps_108233( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      public void Valid_Sistema_codigo( int GX_Parm1 ,
                                        decimal GX_Parm2 )
      {
         A127Sistema_Codigo = GX_Parm1;
         A395Sistema_PF = GX_Parm2;
         GXt_decimal1 = A395Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         A395Sistema_PF = GXt_decimal1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A395Sistema_PF, 14, 5, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Sistemaversao_codigo( GXCombobox dynGX_Parm1 ,
                                              String GX_Parm2 )
      {
         dynSistemaVersao_Codigo = dynGX_Parm1;
         A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( dynSistemaVersao_Codigo.CurrentValue, "."));
         n1859SistemaVersao_Codigo = false;
         A1860SistemaVersao_Id = GX_Parm2;
         /* Using cursor T000O31 */
         pr_default.execute(29, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(29) == 101) )
         {
            if ( ! ( (0==A1859SistemaVersao_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T207'.", "ForeignKeyNotFound", 1, "SISTEMAVERSAO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynSistemaVersao_Codigo_Internalname;
            }
         }
         A1860SistemaVersao_Id = T000O31_A1860SistemaVersao_Id[0];
         pr_default.close(29);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1860SistemaVersao_Id = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A1860SistemaVersao_Id));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Ambientetecnologico_codigo( GXCombobox dynGX_Parm1 ,
                                                    String GX_Parm2 )
      {
         dynAmbienteTecnologico_Codigo = dynGX_Parm1;
         A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( dynAmbienteTecnologico_Codigo.CurrentValue, "."));
         n351AmbienteTecnologico_Codigo = false;
         A352AmbienteTecnologico_Descricao = GX_Parm2;
         /* Using cursor T000O29 */
         pr_default.execute(27, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(27) == 101) )
         {
            if ( ! ( (0==A351AmbienteTecnologico_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            }
         }
         A352AmbienteTecnologico_Descricao = T000O29_A352AmbienteTecnologico_Descricao[0];
         pr_default.close(27);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A352AmbienteTecnologico_Descricao = "";
         }
         isValidOutput.Add(A352AmbienteTecnologico_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Metodologia_codigo( GXCombobox dynGX_Parm1 ,
                                            String GX_Parm2 )
      {
         dynMetodologia_Codigo = dynGX_Parm1;
         A137Metodologia_Codigo = (int)(NumberUtil.Val( dynMetodologia_Codigo.CurrentValue, "."));
         n137Metodologia_Codigo = false;
         A138Metodologia_Descricao = GX_Parm2;
         /* Using cursor T000O30 */
         pr_default.execute(28, new Object[] {n137Metodologia_Codigo, A137Metodologia_Codigo});
         if ( (pr_default.getStatus(28) == 101) )
         {
            if ( ! ( (0==A137Metodologia_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Metodologia'.", "ForeignKeyNotFound", 1, "METODOLOGIA_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynMetodologia_Codigo_Internalname;
            }
         }
         A138Metodologia_Descricao = T000O30_A138Metodologia_Descricao[0];
         pr_default.close(28);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A138Metodologia_Descricao = "";
         }
         isValidOutput.Add(A138Metodologia_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Sistema_gpoobjctrlcod( GXCombobox dynGX_Parm1 ,
                                               int GX_Parm2 )
      {
         dynSistema_GpoObjCtrlCod = dynGX_Parm1;
         A2161Sistema_GpoObjCtrlCod = (int)(NumberUtil.Val( dynSistema_GpoObjCtrlCod.CurrentValue, "."));
         n2161Sistema_GpoObjCtrlCod = false;
         A2162Sistema_GpoObjCtrlRsp = GX_Parm2;
         /* Using cursor T000O69 */
         pr_default.execute(66, new Object[] {n2161Sistema_GpoObjCtrlCod, A2161Sistema_GpoObjCtrlCod});
         if ( (pr_default.getStatus(66) == 101) )
         {
            if ( ! ( (0==A2161Sistema_GpoObjCtrlCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sistema_GpoObjCtrl'.", "ForeignKeyNotFound", 1, "SISTEMA_GPOOBJCTRLCOD");
               AnyError = 1;
               GX_FocusControl = dynSistema_GpoObjCtrlCod_Internalname;
            }
         }
         pr_default.close(66);
         GXt_int2 = A2162Sistema_GpoObjCtrlRsp;
         GXt_int3 = (short)(A2161Sistema_GpoObjCtrlCod);
         new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int3, out  GXt_int2) ;
         A2161Sistema_GpoObjCtrlCod = GXt_int3;
         A2162Sistema_GpoObjCtrlRsp = GXt_int2;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A2162Sistema_GpoObjCtrlRsp), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tecnologia_codigo( GXCombobox dynGX_Parm1 ,
                                           String GX_Parm2 ,
                                           GXCombobox cmbGX_Parm3 )
      {
         dynTecnologia_Codigo = dynGX_Parm1;
         A131Tecnologia_Codigo = (int)(NumberUtil.Val( dynTecnologia_Codigo.CurrentValue, "."));
         A132Tecnologia_Nome = GX_Parm2;
         cmbTecnologia_TipoTecnologia = cmbGX_Parm3;
         A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.CurrentValue;
         n355Tecnologia_TipoTecnologia = false;
         cmbTecnologia_TipoTecnologia.CurrentValue = A355Tecnologia_TipoTecnologia;
         /* Using cursor T000O59 */
         pr_default.execute(57, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(57) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
         }
         A132Tecnologia_Nome = T000O59_A132Tecnologia_Nome[0];
         A355Tecnologia_TipoTecnologia = T000O59_A355Tecnologia_TipoTecnologia[0];
         cmbTecnologia_TipoTecnologia.CurrentValue = A355Tecnologia_TipoTecnologia;
         n355Tecnologia_TipoTecnologia = T000O59_n355Tecnologia_TipoTecnologia[0];
         pr_default.close(57);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A132Tecnologia_Nome = "";
            A355Tecnologia_TipoTecnologia = "";
            n355Tecnologia_TipoTecnologia = false;
            cmbTecnologia_TipoTecnologia.CurrentValue = A355Tecnologia_TipoTecnologia;
         }
         isValidOutput.Add(StringUtil.RTrim( A132Tecnologia_Nome));
         cmbTecnologia_TipoTecnologia.CurrentValue = A355Tecnologia_TipoTecnologia;
         isValidOutput.Add(cmbTecnologia_TipoTecnologia);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E130O2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A513Sistema_Coordenacao',fld:'SISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV54AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOCOPIARCOLAR'","{handler:'E140O2',iparms:[{av:'AV7Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOIMPORTAREXCEL'","{handler:'E110O25',iparms:[],oparms:[{ctrl:'BTNIMPORTAREXCEL',prop:'Visible'},{av:'tblTblimportar_Visible',ctrl:'TBLIMPORTAR',prop:'Visible'}]}");
         setEventMetadata("'DOIMPORTAR'","{handler:'E150O2',iparms:[{av:'AV42ColSisNom',fld:'vCOLSISNOM',pic:'@!',nv:''},{av:'AV43ColSisSgl',fld:'vCOLSISSGL',pic:'@!',nv:''},{av:'AV44ColModNom',fld:'vCOLMODNOM',pic:'@!',nv:''},{av:'AV45ColModSgl',fld:'vCOLMODSGL',pic:'@!',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(57);
         pr_default.close(4);
         pr_default.close(28);
         pr_default.close(27);
         pr_default.close(29);
         pr_default.close(30);
         pr_default.close(31);
         pr_default.close(66);
         pr_default.close(32);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z416Sistema_Nome = "";
         Z129Sistema_Sigla = "";
         Z513Sistema_Coordenacao = "";
         Z699Sistema_Tipo = "";
         Z700Sistema_Tecnica = "";
         Z1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         Z2109Sistema_Repositorio = "";
         O416Sistema_Nome = "";
         O129Sistema_Sigla = "";
         O513Sistema_Coordenacao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         A699Sistema_Tipo = "";
         A700Sistema_Tecnica = "";
         T000O14_A1826GpoObjCtrl_Codigo = new int[1] ;
         T000O14_A1827GpoObjCtrl_Nome = new String[] {""} ;
         GXCCtl = "";
         A355Tecnologia_TipoTecnologia = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtncopiarcolar_Jsonclick = "";
         bttBtnimportarexcel_Jsonclick = "";
         AV21FileName = "";
         bttBtnimportar_Jsonclick = "";
         lblTextblockpralinha_Jsonclick = "";
         lblTextblockcolsisnom_Jsonclick = "";
         AV42ColSisNom = "";
         lblTextblockcolsissgl_Jsonclick = "";
         AV43ColSisSgl = "";
         lblTextblockcolmodnom_Jsonclick = "";
         AV44ColModNom = "";
         lblTextblockcolmodsgl_Jsonclick = "";
         AV45ColModSgl = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         AV48Blob = "";
         AV47Aba = "";
         Grid1Container = new GXWebGrid( context);
         Grid1Column = new GXWebColumn();
         B416Sistema_Nome = "";
         A416Sistema_Nome = "";
         B129Sistema_Sigla = "";
         A129Sistema_Sigla = "";
         B513Sistema_Coordenacao = "";
         A513Sistema_Coordenacao = "";
         sMode233 = "";
         lblTextblocksistema_nome_Jsonclick = "";
         lblTextblocksistema_sigla_Jsonclick = "";
         lblTextblocksistemaversao_codigo_Jsonclick = "";
         lblTextblocksistema_descricao_Jsonclick = "";
         A128Sistema_Descricao = "";
         lblTextblocksistema_coordenacao_Jsonclick = "";
         lblTextblocksistema_repositorio_Jsonclick = "";
         A2109Sistema_Repositorio = "";
         lblTextblockambientetecnologico_codigo_Jsonclick = "";
         lblTextblockmetodologia_codigo_Jsonclick = "";
         lblTextblocksistema_tipo_Jsonclick = "";
         lblTextblocksistema_tecnica_Jsonclick = "";
         lblTextblocksistema_fatorajuste_Jsonclick = "";
         lblTextblocksistema_prazo_Jsonclick = "";
         lblTextblocksistema_custo_Jsonclick = "";
         lblTextblocksistema_esforco_Jsonclick = "";
         lblTextblocksistema_responsavel_Jsonclick = "";
         lblTextblocksistema_gpoobjctrlcod_Jsonclick = "";
         lblTextblocksistema_ativo_Jsonclick = "";
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         A707Sistema_TipoSigla = "";
         AV54AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A138Metodologia_Descricao = "";
         A352AmbienteTecnologico_Descricao = "";
         A1860SistemaVersao_Id = "";
         A136Sistema_AreaTrabalhoDes = "";
         A1403Sistema_ImpUserPesNom = "";
         AV60Pgmname = "";
         A132Tecnologia_Nome = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         Dvpanel_unnamedtable1_Height = "";
         Dvpanel_unnamedtable1_Class = "";
         Gxuitabspanel_tabmain_Height = "";
         Gxuitabspanel_tabmain_Class = "";
         Gxuitabspanel_tabmain_Activetabid = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode25 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z128Sistema_Descricao = "";
         Z136Sistema_AreaTrabalhoDes = "";
         Z1403Sistema_ImpUserPesNom = "";
         Z352AmbienteTecnologico_Descricao = "";
         Z138Metodologia_Descricao = "";
         Z1860SistemaVersao_Id = "";
         T000O7_A138Metodologia_Descricao = new String[] {""} ;
         T000O9_A1860SistemaVersao_Id = new String[] {""} ;
         T000O8_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T000O11_A1402Sistema_ImpUserPesCod = new int[1] ;
         T000O11_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         T000O13_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         T000O13_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         T000O10_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         T000O10_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         T000O15_A127Sistema_Codigo = new int[1] ;
         T000O15_A416Sistema_Nome = new String[] {""} ;
         T000O15_A128Sistema_Descricao = new String[] {""} ;
         T000O15_n128Sistema_Descricao = new bool[] {false} ;
         T000O15_A129Sistema_Sigla = new String[] {""} ;
         T000O15_A513Sistema_Coordenacao = new String[] {""} ;
         T000O15_n513Sistema_Coordenacao = new bool[] {false} ;
         T000O15_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         T000O15_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         T000O15_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T000O15_A138Metodologia_Descricao = new String[] {""} ;
         T000O15_A699Sistema_Tipo = new String[] {""} ;
         T000O15_n699Sistema_Tipo = new bool[] {false} ;
         T000O15_A700Sistema_Tecnica = new String[] {""} ;
         T000O15_n700Sistema_Tecnica = new bool[] {false} ;
         T000O15_A686Sistema_FatorAjuste = new decimal[1] ;
         T000O15_n686Sistema_FatorAjuste = new bool[] {false} ;
         T000O15_A687Sistema_Custo = new decimal[1] ;
         T000O15_n687Sistema_Custo = new bool[] {false} ;
         T000O15_A688Sistema_Prazo = new short[1] ;
         T000O15_n688Sistema_Prazo = new bool[] {false} ;
         T000O15_A689Sistema_Esforco = new short[1] ;
         T000O15_n689Sistema_Esforco = new bool[] {false} ;
         T000O15_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         T000O15_n1401Sistema_ImpData = new bool[] {false} ;
         T000O15_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         T000O15_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         T000O15_A1831Sistema_Responsavel = new int[1] ;
         T000O15_n1831Sistema_Responsavel = new bool[] {false} ;
         T000O15_A1860SistemaVersao_Id = new String[] {""} ;
         T000O15_A130Sistema_Ativo = new bool[] {false} ;
         T000O15_A2109Sistema_Repositorio = new String[] {""} ;
         T000O15_n2109Sistema_Repositorio = new bool[] {false} ;
         T000O15_A2130Sistema_Ultimo = new short[1] ;
         T000O15_A137Metodologia_Codigo = new int[1] ;
         T000O15_n137Metodologia_Codigo = new bool[] {false} ;
         T000O15_A351AmbienteTecnologico_Codigo = new int[1] ;
         T000O15_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T000O15_A1859SistemaVersao_Codigo = new int[1] ;
         T000O15_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T000O15_A135Sistema_AreaTrabalhoCod = new int[1] ;
         T000O15_A1399Sistema_ImpUserCod = new int[1] ;
         T000O15_n1399Sistema_ImpUserCod = new bool[] {false} ;
         T000O15_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T000O15_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         T000O15_A1402Sistema_ImpUserPesCod = new int[1] ;
         T000O15_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         T000O12_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T000O12_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         T000O16_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T000O17_A138Metodologia_Descricao = new String[] {""} ;
         T000O18_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T000O18_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         T000O19_A1860SistemaVersao_Id = new String[] {""} ;
         T000O20_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         T000O20_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         T000O21_A1402Sistema_ImpUserPesCod = new int[1] ;
         T000O21_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         T000O22_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         T000O22_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         T000O23_A127Sistema_Codigo = new int[1] ;
         T000O6_A127Sistema_Codigo = new int[1] ;
         T000O6_A416Sistema_Nome = new String[] {""} ;
         T000O6_A128Sistema_Descricao = new String[] {""} ;
         T000O6_n128Sistema_Descricao = new bool[] {false} ;
         T000O6_A129Sistema_Sigla = new String[] {""} ;
         T000O6_A513Sistema_Coordenacao = new String[] {""} ;
         T000O6_n513Sistema_Coordenacao = new bool[] {false} ;
         T000O6_A699Sistema_Tipo = new String[] {""} ;
         T000O6_n699Sistema_Tipo = new bool[] {false} ;
         T000O6_A700Sistema_Tecnica = new String[] {""} ;
         T000O6_n700Sistema_Tecnica = new bool[] {false} ;
         T000O6_A686Sistema_FatorAjuste = new decimal[1] ;
         T000O6_n686Sistema_FatorAjuste = new bool[] {false} ;
         T000O6_A687Sistema_Custo = new decimal[1] ;
         T000O6_n687Sistema_Custo = new bool[] {false} ;
         T000O6_A688Sistema_Prazo = new short[1] ;
         T000O6_n688Sistema_Prazo = new bool[] {false} ;
         T000O6_A689Sistema_Esforco = new short[1] ;
         T000O6_n689Sistema_Esforco = new bool[] {false} ;
         T000O6_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         T000O6_n1401Sistema_ImpData = new bool[] {false} ;
         T000O6_A1831Sistema_Responsavel = new int[1] ;
         T000O6_n1831Sistema_Responsavel = new bool[] {false} ;
         T000O6_A130Sistema_Ativo = new bool[] {false} ;
         T000O6_A2109Sistema_Repositorio = new String[] {""} ;
         T000O6_n2109Sistema_Repositorio = new bool[] {false} ;
         T000O6_A2130Sistema_Ultimo = new short[1] ;
         T000O6_A137Metodologia_Codigo = new int[1] ;
         T000O6_n137Metodologia_Codigo = new bool[] {false} ;
         T000O6_A351AmbienteTecnologico_Codigo = new int[1] ;
         T000O6_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T000O6_A1859SistemaVersao_Codigo = new int[1] ;
         T000O6_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T000O6_A135Sistema_AreaTrabalhoCod = new int[1] ;
         T000O6_A1399Sistema_ImpUserCod = new int[1] ;
         T000O6_n1399Sistema_ImpUserCod = new bool[] {false} ;
         T000O6_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T000O6_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         T000O24_A127Sistema_Codigo = new int[1] ;
         T000O25_A127Sistema_Codigo = new int[1] ;
         T000O5_A127Sistema_Codigo = new int[1] ;
         T000O5_A416Sistema_Nome = new String[] {""} ;
         T000O5_A128Sistema_Descricao = new String[] {""} ;
         T000O5_n128Sistema_Descricao = new bool[] {false} ;
         T000O5_A129Sistema_Sigla = new String[] {""} ;
         T000O5_A513Sistema_Coordenacao = new String[] {""} ;
         T000O5_n513Sistema_Coordenacao = new bool[] {false} ;
         T000O5_A699Sistema_Tipo = new String[] {""} ;
         T000O5_n699Sistema_Tipo = new bool[] {false} ;
         T000O5_A700Sistema_Tecnica = new String[] {""} ;
         T000O5_n700Sistema_Tecnica = new bool[] {false} ;
         T000O5_A686Sistema_FatorAjuste = new decimal[1] ;
         T000O5_n686Sistema_FatorAjuste = new bool[] {false} ;
         T000O5_A687Sistema_Custo = new decimal[1] ;
         T000O5_n687Sistema_Custo = new bool[] {false} ;
         T000O5_A688Sistema_Prazo = new short[1] ;
         T000O5_n688Sistema_Prazo = new bool[] {false} ;
         T000O5_A689Sistema_Esforco = new short[1] ;
         T000O5_n689Sistema_Esforco = new bool[] {false} ;
         T000O5_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         T000O5_n1401Sistema_ImpData = new bool[] {false} ;
         T000O5_A1831Sistema_Responsavel = new int[1] ;
         T000O5_n1831Sistema_Responsavel = new bool[] {false} ;
         T000O5_A130Sistema_Ativo = new bool[] {false} ;
         T000O5_A2109Sistema_Repositorio = new String[] {""} ;
         T000O5_n2109Sistema_Repositorio = new bool[] {false} ;
         T000O5_A2130Sistema_Ultimo = new short[1] ;
         T000O5_A137Metodologia_Codigo = new int[1] ;
         T000O5_n137Metodologia_Codigo = new bool[] {false} ;
         T000O5_A351AmbienteTecnologico_Codigo = new int[1] ;
         T000O5_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T000O5_A1859SistemaVersao_Codigo = new int[1] ;
         T000O5_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T000O5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         T000O5_A1399Sistema_ImpUserCod = new int[1] ;
         T000O5_n1399Sistema_ImpUserCod = new bool[] {false} ;
         T000O5_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T000O5_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         T000O26_A127Sistema_Codigo = new int[1] ;
         T000O29_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T000O30_A138Metodologia_Descricao = new String[] {""} ;
         T000O31_A1860SistemaVersao_Id = new String[] {""} ;
         T000O32_A136Sistema_AreaTrabalhoDes = new String[] {""} ;
         T000O32_n136Sistema_AreaTrabalhoDes = new bool[] {false} ;
         T000O33_A1402Sistema_ImpUserPesCod = new int[1] ;
         T000O33_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         T000O34_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         T000O34_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         T000O35_A648Projeto_Codigo = new int[1] ;
         T000O36_A1859SistemaVersao_Codigo = new int[1] ;
         T000O36_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T000O37_A1725ContratoSistemas_CntCod = new int[1] ;
         T000O37_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T000O38_A160ContratoServicos_Codigo = new int[1] ;
         T000O38_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T000O38_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T000O39_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T000O40_A192Contagem_Codigo = new int[1] ;
         T000O41_A736ProjetoMelhoria_Codigo = new int[1] ;
         T000O42_A456ContagemResultado_Codigo = new int[1] ;
         T000O43_A368FuncaoDados_Codigo = new int[1] ;
         T000O44_A165FuncaoAPF_Codigo = new int[1] ;
         T000O45_A172Tabela_Codigo = new int[1] ;
         T000O46_A439Solicitacoes_Codigo = new int[1] ;
         T000O47_A161FuncaoUsuario_Codigo = new int[1] ;
         T000O48_A146Modulo_Codigo = new int[1] ;
         T000O49_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000O49_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000O49_A127Sistema_Codigo = new int[1] ;
         T000O50_A127Sistema_Codigo = new int[1] ;
         T000O50_A1460SistemaDePara_Codigo = new int[1] ;
         T000O52_A127Sistema_Codigo = new int[1] ;
         Z132Tecnologia_Nome = "";
         Z355Tecnologia_TipoTecnologia = "";
         T000O4_A132Tecnologia_Nome = new String[] {""} ;
         T000O4_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000O4_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000O53_A127Sistema_Codigo = new int[1] ;
         T000O53_A2128SistemaTecnologiaLinha = new short[1] ;
         T000O53_A132Tecnologia_Nome = new String[] {""} ;
         T000O53_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000O53_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000O53_A131Tecnologia_Codigo = new int[1] ;
         T000O54_A132Tecnologia_Nome = new String[] {""} ;
         T000O54_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000O54_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000O55_A127Sistema_Codigo = new int[1] ;
         T000O55_A2128SistemaTecnologiaLinha = new short[1] ;
         T000O3_A127Sistema_Codigo = new int[1] ;
         T000O3_A2128SistemaTecnologiaLinha = new short[1] ;
         T000O3_A131Tecnologia_Codigo = new int[1] ;
         T000O2_A127Sistema_Codigo = new int[1] ;
         T000O2_A2128SistemaTecnologiaLinha = new short[1] ;
         T000O2_A131Tecnologia_Codigo = new int[1] ;
         T000O59_A132Tecnologia_Nome = new String[] {""} ;
         T000O59_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         T000O59_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         T000O60_A127Sistema_Codigo = new int[1] ;
         T000O60_A2128SistemaTecnologiaLinha = new short[1] ;
         Grid1Row = new GXWebRow();
         subGrid1_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i699Sistema_Tipo = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000O61_A1826GpoObjCtrl_Codigo = new int[1] ;
         T000O61_A1827GpoObjCtrl_Nome = new String[] {""} ;
         T000O62_A1859SistemaVersao_Codigo = new int[1] ;
         T000O62_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T000O62_A1860SistemaVersao_Id = new String[] {""} ;
         T000O62_A1866SistemaVersao_SistemaCod = new int[1] ;
         T000O63_A351AmbienteTecnologico_Codigo = new int[1] ;
         T000O63_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T000O63_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T000O63_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T000O63_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         T000O64_A137Metodologia_Codigo = new int[1] ;
         T000O64_n137Metodologia_Codigo = new bool[] {false} ;
         T000O64_A138Metodologia_Descricao = new String[] {""} ;
         T000O66_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T000O66_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000O66_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000O66_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000O66_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000O66_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000O66_A54Usuario_Ativo = new bool[] {false} ;
         T000O66_n54Usuario_Ativo = new bool[] {false} ;
         T000O66_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         T000O66_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000O67_A131Tecnologia_Codigo = new int[1] ;
         T000O67_A132Tecnologia_Nome = new String[] {""} ;
         T000O68_A1826GpoObjCtrl_Codigo = new int[1] ;
         T000O68_A1827GpoObjCtrl_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000O69_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T000O69_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistema__default(),
            new Object[][] {
                new Object[] {
               T000O2_A127Sistema_Codigo, T000O2_A2128SistemaTecnologiaLinha, T000O2_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000O3_A127Sistema_Codigo, T000O3_A2128SistemaTecnologiaLinha, T000O3_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000O4_A132Tecnologia_Nome, T000O4_A355Tecnologia_TipoTecnologia, T000O4_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               T000O5_A127Sistema_Codigo, T000O5_A416Sistema_Nome, T000O5_A128Sistema_Descricao, T000O5_n128Sistema_Descricao, T000O5_A129Sistema_Sigla, T000O5_A513Sistema_Coordenacao, T000O5_n513Sistema_Coordenacao, T000O5_A699Sistema_Tipo, T000O5_n699Sistema_Tipo, T000O5_A700Sistema_Tecnica,
               T000O5_n700Sistema_Tecnica, T000O5_A686Sistema_FatorAjuste, T000O5_n686Sistema_FatorAjuste, T000O5_A687Sistema_Custo, T000O5_n687Sistema_Custo, T000O5_A688Sistema_Prazo, T000O5_n688Sistema_Prazo, T000O5_A689Sistema_Esforco, T000O5_n689Sistema_Esforco, T000O5_A1401Sistema_ImpData,
               T000O5_n1401Sistema_ImpData, T000O5_A1831Sistema_Responsavel, T000O5_n1831Sistema_Responsavel, T000O5_A130Sistema_Ativo, T000O5_A2109Sistema_Repositorio, T000O5_n2109Sistema_Repositorio, T000O5_A2130Sistema_Ultimo, T000O5_A137Metodologia_Codigo, T000O5_n137Metodologia_Codigo, T000O5_A351AmbienteTecnologico_Codigo,
               T000O5_n351AmbienteTecnologico_Codigo, T000O5_A1859SistemaVersao_Codigo, T000O5_n1859SistemaVersao_Codigo, T000O5_A135Sistema_AreaTrabalhoCod, T000O5_A1399Sistema_ImpUserCod, T000O5_n1399Sistema_ImpUserCod, T000O5_A2161Sistema_GpoObjCtrlCod, T000O5_n2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               T000O6_A127Sistema_Codigo, T000O6_A416Sistema_Nome, T000O6_A128Sistema_Descricao, T000O6_n128Sistema_Descricao, T000O6_A129Sistema_Sigla, T000O6_A513Sistema_Coordenacao, T000O6_n513Sistema_Coordenacao, T000O6_A699Sistema_Tipo, T000O6_n699Sistema_Tipo, T000O6_A700Sistema_Tecnica,
               T000O6_n700Sistema_Tecnica, T000O6_A686Sistema_FatorAjuste, T000O6_n686Sistema_FatorAjuste, T000O6_A687Sistema_Custo, T000O6_n687Sistema_Custo, T000O6_A688Sistema_Prazo, T000O6_n688Sistema_Prazo, T000O6_A689Sistema_Esforco, T000O6_n689Sistema_Esforco, T000O6_A1401Sistema_ImpData,
               T000O6_n1401Sistema_ImpData, T000O6_A1831Sistema_Responsavel, T000O6_n1831Sistema_Responsavel, T000O6_A130Sistema_Ativo, T000O6_A2109Sistema_Repositorio, T000O6_n2109Sistema_Repositorio, T000O6_A2130Sistema_Ultimo, T000O6_A137Metodologia_Codigo, T000O6_n137Metodologia_Codigo, T000O6_A351AmbienteTecnologico_Codigo,
               T000O6_n351AmbienteTecnologico_Codigo, T000O6_A1859SistemaVersao_Codigo, T000O6_n1859SistemaVersao_Codigo, T000O6_A135Sistema_AreaTrabalhoCod, T000O6_A1399Sistema_ImpUserCod, T000O6_n1399Sistema_ImpUserCod, T000O6_A2161Sistema_GpoObjCtrlCod, T000O6_n2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               T000O7_A138Metodologia_Descricao
               }
               , new Object[] {
               T000O8_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               T000O9_A1860SistemaVersao_Id
               }
               , new Object[] {
               T000O10_A136Sistema_AreaTrabalhoDes, T000O10_n136Sistema_AreaTrabalhoDes
               }
               , new Object[] {
               T000O11_A1402Sistema_ImpUserPesCod, T000O11_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               T000O12_A2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               T000O13_A1403Sistema_ImpUserPesNom, T000O13_n1403Sistema_ImpUserPesNom
               }
               , new Object[] {
               T000O14_A1826GpoObjCtrl_Codigo, T000O14_A1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T000O15_A127Sistema_Codigo, T000O15_A416Sistema_Nome, T000O15_A128Sistema_Descricao, T000O15_n128Sistema_Descricao, T000O15_A129Sistema_Sigla, T000O15_A513Sistema_Coordenacao, T000O15_n513Sistema_Coordenacao, T000O15_A136Sistema_AreaTrabalhoDes, T000O15_n136Sistema_AreaTrabalhoDes, T000O15_A352AmbienteTecnologico_Descricao,
               T000O15_A138Metodologia_Descricao, T000O15_A699Sistema_Tipo, T000O15_n699Sistema_Tipo, T000O15_A700Sistema_Tecnica, T000O15_n700Sistema_Tecnica, T000O15_A686Sistema_FatorAjuste, T000O15_n686Sistema_FatorAjuste, T000O15_A687Sistema_Custo, T000O15_n687Sistema_Custo, T000O15_A688Sistema_Prazo,
               T000O15_n688Sistema_Prazo, T000O15_A689Sistema_Esforco, T000O15_n689Sistema_Esforco, T000O15_A1401Sistema_ImpData, T000O15_n1401Sistema_ImpData, T000O15_A1403Sistema_ImpUserPesNom, T000O15_n1403Sistema_ImpUserPesNom, T000O15_A1831Sistema_Responsavel, T000O15_n1831Sistema_Responsavel, T000O15_A1860SistemaVersao_Id,
               T000O15_A130Sistema_Ativo, T000O15_A2109Sistema_Repositorio, T000O15_n2109Sistema_Repositorio, T000O15_A2130Sistema_Ultimo, T000O15_A137Metodologia_Codigo, T000O15_n137Metodologia_Codigo, T000O15_A351AmbienteTecnologico_Codigo, T000O15_n351AmbienteTecnologico_Codigo, T000O15_A1859SistemaVersao_Codigo, T000O15_n1859SistemaVersao_Codigo,
               T000O15_A135Sistema_AreaTrabalhoCod, T000O15_A1399Sistema_ImpUserCod, T000O15_n1399Sistema_ImpUserCod, T000O15_A2161Sistema_GpoObjCtrlCod, T000O15_n2161Sistema_GpoObjCtrlCod, T000O15_A1402Sistema_ImpUserPesCod, T000O15_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               T000O16_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               T000O17_A138Metodologia_Descricao
               }
               , new Object[] {
               T000O18_A2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               T000O19_A1860SistemaVersao_Id
               }
               , new Object[] {
               T000O20_A136Sistema_AreaTrabalhoDes, T000O20_n136Sistema_AreaTrabalhoDes
               }
               , new Object[] {
               T000O21_A1402Sistema_ImpUserPesCod, T000O21_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               T000O22_A1403Sistema_ImpUserPesNom, T000O22_n1403Sistema_ImpUserPesNom
               }
               , new Object[] {
               T000O23_A127Sistema_Codigo
               }
               , new Object[] {
               T000O24_A127Sistema_Codigo
               }
               , new Object[] {
               T000O25_A127Sistema_Codigo
               }
               , new Object[] {
               T000O26_A127Sistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000O29_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               T000O30_A138Metodologia_Descricao
               }
               , new Object[] {
               T000O31_A1860SistemaVersao_Id
               }
               , new Object[] {
               T000O32_A136Sistema_AreaTrabalhoDes, T000O32_n136Sistema_AreaTrabalhoDes
               }
               , new Object[] {
               T000O33_A1402Sistema_ImpUserPesCod, T000O33_n1402Sistema_ImpUserPesCod
               }
               , new Object[] {
               T000O34_A1403Sistema_ImpUserPesNom, T000O34_n1403Sistema_ImpUserPesNom
               }
               , new Object[] {
               T000O35_A648Projeto_Codigo
               }
               , new Object[] {
               T000O36_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               T000O37_A1725ContratoSistemas_CntCod, T000O37_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T000O38_A160ContratoServicos_Codigo, T000O38_A1067ContratoServicosSistemas_ServicoCod, T000O38_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T000O39_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               T000O40_A192Contagem_Codigo
               }
               , new Object[] {
               T000O41_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T000O42_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000O43_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T000O44_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000O45_A172Tabela_Codigo
               }
               , new Object[] {
               T000O46_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T000O47_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T000O48_A146Modulo_Codigo
               }
               , new Object[] {
               T000O49_A63ContratanteUsuario_ContratanteCod, T000O49_A60ContratanteUsuario_UsuarioCod, T000O49_A127Sistema_Codigo
               }
               , new Object[] {
               T000O50_A127Sistema_Codigo, T000O50_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               T000O52_A127Sistema_Codigo
               }
               , new Object[] {
               T000O53_A127Sistema_Codigo, T000O53_A2128SistemaTecnologiaLinha, T000O53_A132Tecnologia_Nome, T000O53_A355Tecnologia_TipoTecnologia, T000O53_n355Tecnologia_TipoTecnologia, T000O53_A131Tecnologia_Codigo
               }
               , new Object[] {
               T000O54_A132Tecnologia_Nome, T000O54_A355Tecnologia_TipoTecnologia, T000O54_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               T000O55_A127Sistema_Codigo, T000O55_A2128SistemaTecnologiaLinha
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000O59_A132Tecnologia_Nome, T000O59_A355Tecnologia_TipoTecnologia, T000O59_n355Tecnologia_TipoTecnologia
               }
               , new Object[] {
               T000O60_A127Sistema_Codigo, T000O60_A2128SistemaTecnologiaLinha
               }
               , new Object[] {
               T000O61_A1826GpoObjCtrl_Codigo, T000O61_A1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T000O62_A1859SistemaVersao_Codigo, T000O62_A1860SistemaVersao_Id, T000O62_A1866SistemaVersao_SistemaCod
               }
               , new Object[] {
               T000O63_A351AmbienteTecnologico_Codigo, T000O63_A352AmbienteTecnologico_Descricao, T000O63_A728AmbienteTecnologico_AreaTrabalhoCod, T000O63_A353AmbienteTecnologico_Ativo
               }
               , new Object[] {
               T000O64_A137Metodologia_Codigo, T000O64_A138Metodologia_Descricao
               }
               , new Object[] {
               T000O66_A61ContratanteUsuario_UsuarioPessoaCod, T000O66_n61ContratanteUsuario_UsuarioPessoaCod, T000O66_A63ContratanteUsuario_ContratanteCod, T000O66_A60ContratanteUsuario_UsuarioCod, T000O66_A62ContratanteUsuario_UsuarioPessoaNom, T000O66_n62ContratanteUsuario_UsuarioPessoaNom, T000O66_A54Usuario_Ativo, T000O66_n54Usuario_Ativo, T000O66_A1020ContratanteUsuario_AreaTrabalhoCod, T000O66_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               T000O67_A131Tecnologia_Codigo, T000O67_A132Tecnologia_Nome
               }
               , new Object[] {
               T000O68_A1826GpoObjCtrl_Codigo, T000O68_A1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T000O69_A2161Sistema_GpoObjCtrlCod
               }
            }
         );
         Z130Sistema_Ativo = true;
         A130Sistema_Ativo = true;
         i130Sistema_Ativo = true;
         Z699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         A699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         i699Sistema_Tipo = "A";
         n699Sistema_Tipo = false;
         AV60Pgmname = "Sistema";
         Z686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         A686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         i686Sistema_FatorAjuste = new prc_fapadrao(context).executeUdp( );
         n686Sistema_FatorAjuste = false;
         Z135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A135Sistema_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z688Sistema_Prazo ;
      private short Z689Sistema_Esforco ;
      private short Z2130Sistema_Ultimo ;
      private short O2130Sistema_Ultimo ;
      private short nRC_GXsfl_108 ;
      private short nGXsfl_108_idx=1 ;
      private short Z2128SistemaTecnologiaLinha ;
      private short nRcdDeleted_233 ;
      private short nRcdExists_233 ;
      private short nIsMod_233 ;
      private short GxWebError ;
      private short Gx_BScreen ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short AV49PraLinha ;
      private short edtavBlob_Display ;
      private short subGrid1_Backcolorstyle ;
      private short A2128SistemaTecnologiaLinha ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nBlankRcdCount233 ;
      private short RcdFound233 ;
      private short B2130Sistema_Ultimo ;
      private short A2130Sistema_Ultimo ;
      private short nBlankRcdUsr233 ;
      private short A688Sistema_Prazo ;
      private short A689Sistema_Esforco ;
      private short RcdFound25 ;
      private short s2130Sistema_Ultimo ;
      private short AV52ColSisNomn ;
      private short AV51ColSisSgln ;
      private short AV53ColModNomn ;
      private short AV50ColModSgln ;
      private short GX_JID ;
      private short subGrid1_Backstyle ;
      private short gxajaxcallmode ;
      private short i2130Sistema_Ultimo ;
      private short wbTemp ;
      private short GXt_int3 ;
      private int wcpOAV7Sistema_Codigo ;
      private int Z127Sistema_Codigo ;
      private int Z1831Sistema_Responsavel ;
      private int Z137Metodologia_Codigo ;
      private int Z351AmbienteTecnologico_Codigo ;
      private int Z1859SistemaVersao_Codigo ;
      private int Z135Sistema_AreaTrabalhoCod ;
      private int Z1399Sistema_ImpUserCod ;
      private int Z2161Sistema_GpoObjCtrlCod ;
      private int N135Sistema_AreaTrabalhoCod ;
      private int N351AmbienteTecnologico_Codigo ;
      private int N137Metodologia_Codigo ;
      private int N1399Sistema_ImpUserCod ;
      private int N2161Sistema_GpoObjCtrlCod ;
      private int N1859SistemaVersao_Codigo ;
      private int Z131Tecnologia_Codigo ;
      private int AV7Sistema_Codigo ;
      private int A2161Sistema_GpoObjCtrlCod ;
      private int A127Sistema_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A137Metodologia_Codigo ;
      private int A1859SistemaVersao_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A1399Sistema_ImpUserCod ;
      private int A1402Sistema_ImpUserPesCod ;
      private int A131Tecnologia_Codigo ;
      private int trnEnded ;
      private int A1831Sistema_Responsavel ;
      private int edtSistema_Codigo_Enabled ;
      private int edtSistema_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtncopiarcolar_Visible ;
      private int bttBtnimportarexcel_Visible ;
      private int tblTblimportar_Visible ;
      private int edtavFilename_Enabled ;
      private int bttBtnimportar_Visible ;
      private int edtavPralinha_Enabled ;
      private int edtavColsisnom_Enabled ;
      private int edtavColsissgl_Enabled ;
      private int edtavColmodnom_Enabled ;
      private int edtavColmodsgl_Enabled ;
      private int edtavBlob_Enabled ;
      private int edtavAba_Enabled ;
      private int edtSistemaTecnologiaLinha_Enabled ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int fRowAdded ;
      private int edtSistema_Nome_Enabled ;
      private int edtSistema_Sigla_Enabled ;
      private int edtSistema_Descricao_Enabled ;
      private int edtSistema_Coordenacao_Enabled ;
      private int edtSistema_Coordenacao_Width ;
      private int edtSistema_Repositorio_Enabled ;
      private int edtSistema_FatorAjuste_Enabled ;
      private int edtSistema_Prazo_Enabled ;
      private int edtSistema_Custo_Enabled ;
      private int edtSistema_Esforco_Enabled ;
      private int lblTextblocksistema_ativo_Visible ;
      private int A2162Sistema_GpoObjCtrlRsp ;
      private int AV11Insert_Sistema_AreaTrabalhoCod ;
      private int AV16Insert_AmbienteTecnologico_Codigo ;
      private int AV13Insert_Metodologia_Codigo ;
      private int AV40Insert_Sistema_ImpUserCod ;
      private int AV58Insert_Sistema_GpoObjCtrlCod ;
      private int AV56Insert_SistemaVersao_Codigo ;
      private int Gxuitabspanel_tabmain_Selectedtabindex ;
      private int AV61GXV1 ;
      private int Z1402Sistema_ImpUserPesCod ;
      private int subGrid1_Backcolor ;
      private int subGrid1_Allbackcolor ;
      private int defedtSistemaTecnologiaLinha_Enabled ;
      private int i135Sistema_AreaTrabalhoCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int2 ;
      private long GRID1_nFirstRecordOnPage ;
      private decimal Z686Sistema_FatorAjuste ;
      private decimal Z687Sistema_Custo ;
      private decimal A686Sistema_FatorAjuste ;
      private decimal A687Sistema_Custo ;
      private decimal A395Sistema_PF ;
      private decimal A690Sistema_PFA ;
      private decimal i686Sistema_FatorAjuste ;
      private decimal GXt_decimal1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z129Sistema_Sigla ;
      private String Z699Sistema_Tipo ;
      private String Z700Sistema_Tecnica ;
      private String O129Sistema_Sigla ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String sGXsfl_108_idx="0001" ;
      private String GXKey ;
      private String A699Sistema_Tipo ;
      private String A700Sistema_Tecnica ;
      private String chkSistema_Ativo_Internalname ;
      private String GXCCtl ;
      private String A355Tecnologia_TipoTecnologia ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSistema_Nome_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTable1_Internalname ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divLayout_unnamedtable1_Internalname ;
      private String divUnnamedtable1_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtncopiarcolar_Internalname ;
      private String bttBtncopiarcolar_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String bttBtnimportarexcel_Internalname ;
      private String bttBtnimportarexcel_Jsonclick ;
      private String tblTblimportar_Internalname ;
      private String tblUnnamedtable5_Internalname ;
      private String edtavFilename_Internalname ;
      private String AV21FileName ;
      private String edtavFilename_Jsonclick ;
      private String bttBtnimportar_Internalname ;
      private String bttBtnimportar_Jsonclick ;
      private String tblUnnamedtable4_Internalname ;
      private String divUnnamedtablepralinha_Internalname ;
      private String lblTextblockpralinha_Internalname ;
      private String lblTextblockpralinha_Jsonclick ;
      private String edtavPralinha_Internalname ;
      private String edtavPralinha_Jsonclick ;
      private String divUnnamedtablecolsisnom_Internalname ;
      private String lblTextblockcolsisnom_Internalname ;
      private String lblTextblockcolsisnom_Jsonclick ;
      private String edtavColsisnom_Internalname ;
      private String AV42ColSisNom ;
      private String edtavColsisnom_Jsonclick ;
      private String divUnnamedtablecolsissgl_Internalname ;
      private String lblTextblockcolsissgl_Internalname ;
      private String lblTextblockcolsissgl_Jsonclick ;
      private String edtavColsissgl_Internalname ;
      private String AV43ColSisSgl ;
      private String edtavColsissgl_Jsonclick ;
      private String divUnnamedtablecolmodnom_Internalname ;
      private String lblTextblockcolmodnom_Internalname ;
      private String lblTextblockcolmodnom_Jsonclick ;
      private String edtavColmodnom_Internalname ;
      private String AV44ColModNom ;
      private String edtavColmodnom_Jsonclick ;
      private String divUnnamedtablecolmodsgl_Internalname ;
      private String lblTextblockcolmodsgl_Internalname ;
      private String lblTextblockcolmodsgl_Jsonclick ;
      private String edtavColmodsgl_Internalname ;
      private String AV45ColModSgl ;
      private String edtavColmodsgl_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String edtavBlob_Internalname ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String edtavAba_Internalname ;
      private String AV47Aba ;
      private String edtavAba_Jsonclick ;
      private String tblUnnamedtable6_Internalname ;
      private String B129Sistema_Sigla ;
      private String A129Sistema_Sigla ;
      private String sMode233 ;
      private String edtSistemaTecnologiaLinha_Internalname ;
      private String dynTecnologia_Codigo_Internalname ;
      private String cmbTecnologia_TipoTecnologia_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksistema_nome_Internalname ;
      private String lblTextblocksistema_nome_Jsonclick ;
      private String edtSistema_Nome_Jsonclick ;
      private String lblTextblocksistema_sigla_Internalname ;
      private String lblTextblocksistema_sigla_Jsonclick ;
      private String edtSistema_Sigla_Internalname ;
      private String edtSistema_Sigla_Jsonclick ;
      private String lblTextblocksistemaversao_codigo_Internalname ;
      private String lblTextblocksistemaversao_codigo_Jsonclick ;
      private String dynSistemaVersao_Codigo_Internalname ;
      private String dynSistemaVersao_Codigo_Jsonclick ;
      private String lblTextblocksistema_descricao_Internalname ;
      private String lblTextblocksistema_descricao_Jsonclick ;
      private String edtSistema_Descricao_Internalname ;
      private String lblTextblocksistema_coordenacao_Internalname ;
      private String lblTextblocksistema_coordenacao_Jsonclick ;
      private String edtSistema_Coordenacao_Internalname ;
      private String edtSistema_Coordenacao_Jsonclick ;
      private String lblTextblocksistema_repositorio_Internalname ;
      private String lblTextblocksistema_repositorio_Jsonclick ;
      private String edtSistema_Repositorio_Internalname ;
      private String lblTextblockambientetecnologico_codigo_Internalname ;
      private String lblTextblockambientetecnologico_codigo_Jsonclick ;
      private String dynAmbienteTecnologico_Codigo_Internalname ;
      private String dynAmbienteTecnologico_Codigo_Jsonclick ;
      private String lblTextblockmetodologia_codigo_Internalname ;
      private String lblTextblockmetodologia_codigo_Jsonclick ;
      private String dynMetodologia_Codigo_Internalname ;
      private String dynMetodologia_Codigo_Jsonclick ;
      private String lblTextblocksistema_tipo_Internalname ;
      private String lblTextblocksistema_tipo_Jsonclick ;
      private String cmbSistema_Tipo_Internalname ;
      private String cmbSistema_Tipo_Jsonclick ;
      private String lblTextblocksistema_tecnica_Internalname ;
      private String lblTextblocksistema_tecnica_Jsonclick ;
      private String cmbSistema_Tecnica_Internalname ;
      private String cmbSistema_Tecnica_Jsonclick ;
      private String lblTextblocksistema_fatorajuste_Internalname ;
      private String lblTextblocksistema_fatorajuste_Jsonclick ;
      private String edtSistema_FatorAjuste_Internalname ;
      private String edtSistema_FatorAjuste_Jsonclick ;
      private String lblTextblocksistema_prazo_Internalname ;
      private String lblTextblocksistema_prazo_Jsonclick ;
      private String edtSistema_Prazo_Internalname ;
      private String edtSistema_Prazo_Jsonclick ;
      private String lblTextblocksistema_custo_Internalname ;
      private String lblTextblocksistema_custo_Jsonclick ;
      private String edtSistema_Custo_Internalname ;
      private String edtSistema_Custo_Jsonclick ;
      private String lblTextblocksistema_esforco_Internalname ;
      private String lblTextblocksistema_esforco_Jsonclick ;
      private String edtSistema_Esforco_Internalname ;
      private String edtSistema_Esforco_Jsonclick ;
      private String lblTextblocksistema_responsavel_Internalname ;
      private String lblTextblocksistema_responsavel_Jsonclick ;
      private String dynSistema_Responsavel_Internalname ;
      private String dynSistema_Responsavel_Jsonclick ;
      private String lblTextblocksistema_gpoobjctrlcod_Internalname ;
      private String lblTextblocksistema_gpoobjctrlcod_Jsonclick ;
      private String dynSistema_GpoObjCtrlCod_Internalname ;
      private String dynSistema_GpoObjCtrlCod_Jsonclick ;
      private String lblTextblocksistema_ativo_Internalname ;
      private String lblTextblocksistema_ativo_Jsonclick ;
      private String A707Sistema_TipoSigla ;
      private String A1860SistemaVersao_Id ;
      private String A1403Sistema_ImpUserPesNom ;
      private String AV60Pgmname ;
      private String A132Tecnologia_Nome ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Dvpanel_unnamedtable1_Width ;
      private String Dvpanel_unnamedtable1_Height ;
      private String Dvpanel_unnamedtable1_Cls ;
      private String Dvpanel_unnamedtable1_Title ;
      private String Dvpanel_unnamedtable1_Class ;
      private String Dvpanel_unnamedtable1_Iconposition ;
      private String Gxuitabspanel_tabmain_Width ;
      private String Gxuitabspanel_tabmain_Height ;
      private String Gxuitabspanel_tabmain_Cls ;
      private String Gxuitabspanel_tabmain_Class ;
      private String Gxuitabspanel_tabmain_Activetabid ;
      private String Gxuitabspanel_tabmain_Designtimetabs ;
      private String edtavBlob_Filename ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode25 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1403Sistema_ImpUserPesNom ;
      private String Z1860SistemaVersao_Id ;
      private String Z132Tecnologia_Nome ;
      private String Z355Tecnologia_TipoTecnologia ;
      private String sGXsfl_108_fel_idx="0001" ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String ROClassString ;
      private String edtSistemaTecnologiaLinha_Jsonclick ;
      private String dynTecnologia_Codigo_Jsonclick ;
      private String cmbTecnologia_TipoTecnologia_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i699Sistema_Tipo ;
      private String Dvpanel_tableattributes_Internalname ;
      private String div_Internalname ;
      private String Dvpanel_unnamedtable1_Internalname ;
      private String Gxuitabspanel_tabmain_Internalname ;
      private String subGrid1_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1401Sistema_ImpData ;
      private DateTime A1401Sistema_ImpData ;
      private bool Z130Sistema_Ativo ;
      private bool entryPointCalled ;
      private bool n2161Sistema_GpoObjCtrlCod ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n137Metodologia_Codigo ;
      private bool n1859SistemaVersao_Codigo ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n1402Sistema_ImpUserPesCod ;
      private bool toggleJsOutput ;
      private bool n699Sistema_Tipo ;
      private bool n700Sistema_Tecnica ;
      private bool n355Tecnologia_TipoTecnologia ;
      private bool wbErr ;
      private bool n1831Sistema_Responsavel ;
      private bool n513Sistema_Coordenacao ;
      private bool A130Sistema_Ativo ;
      private bool n128Sistema_Descricao ;
      private bool n2109Sistema_Repositorio ;
      private bool n686Sistema_FatorAjuste ;
      private bool n688Sistema_Prazo ;
      private bool n687Sistema_Custo ;
      private bool n689Sistema_Esforco ;
      private bool n1401Sistema_ImpData ;
      private bool n136Sistema_AreaTrabalhoDes ;
      private bool n1403Sistema_ImpUserPesNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool Dvpanel_unnamedtable1_Collapsible ;
      private bool Dvpanel_unnamedtable1_Collapsed ;
      private bool Dvpanel_unnamedtable1_Enabled ;
      private bool Dvpanel_unnamedtable1_Autowidth ;
      private bool Dvpanel_unnamedtable1_Autoheight ;
      private bool Dvpanel_unnamedtable1_Showheader ;
      private bool Dvpanel_unnamedtable1_Showcollapseicon ;
      private bool Dvpanel_unnamedtable1_Autoscroll ;
      private bool Dvpanel_unnamedtable1_Visible ;
      private bool Gxuitabspanel_tabmain_Enabled ;
      private bool Gxuitabspanel_tabmain_Autowidth ;
      private bool Gxuitabspanel_tabmain_Autoheight ;
      private bool Gxuitabspanel_tabmain_Autoscroll ;
      private bool Gxuitabspanel_tabmain_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i130Sistema_Ativo ;
      private String A128Sistema_Descricao ;
      private String Z128Sistema_Descricao ;
      private String Z416Sistema_Nome ;
      private String Z513Sistema_Coordenacao ;
      private String Z2109Sistema_Repositorio ;
      private String O416Sistema_Nome ;
      private String O513Sistema_Coordenacao ;
      private String B416Sistema_Nome ;
      private String A416Sistema_Nome ;
      private String B513Sistema_Coordenacao ;
      private String A513Sistema_Coordenacao ;
      private String A2109Sistema_Repositorio ;
      private String A138Metodologia_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A136Sistema_AreaTrabalhoDes ;
      private String Z136Sistema_AreaTrabalhoDes ;
      private String Z352AmbienteTecnologico_Descricao ;
      private String Z138Metodologia_Descricao ;
      private String AV48Blob ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IDataStoreProvider pr_default ;
      private int[] T000O14_A1826GpoObjCtrl_Codigo ;
      private String[] T000O14_A1827GpoObjCtrl_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynSistemaVersao_Codigo ;
      private GXCombobox dynAmbienteTecnologico_Codigo ;
      private GXCombobox dynMetodologia_Codigo ;
      private GXCombobox cmbSistema_Tipo ;
      private GXCombobox cmbSistema_Tecnica ;
      private GXCombobox dynSistema_Responsavel ;
      private GXCombobox dynSistema_GpoObjCtrlCod ;
      private GXCheckbox chkSistema_Ativo ;
      private GXCombobox dynTecnologia_Codigo ;
      private GXCombobox cmbTecnologia_TipoTecnologia ;
      private String[] T000O7_A138Metodologia_Descricao ;
      private String[] T000O9_A1860SistemaVersao_Id ;
      private String[] T000O8_A352AmbienteTecnologico_Descricao ;
      private int[] T000O11_A1402Sistema_ImpUserPesCod ;
      private bool[] T000O11_n1402Sistema_ImpUserPesCod ;
      private String[] T000O13_A1403Sistema_ImpUserPesNom ;
      private bool[] T000O13_n1403Sistema_ImpUserPesNom ;
      private String[] T000O10_A136Sistema_AreaTrabalhoDes ;
      private bool[] T000O10_n136Sistema_AreaTrabalhoDes ;
      private int[] T000O15_A127Sistema_Codigo ;
      private String[] T000O15_A416Sistema_Nome ;
      private String[] T000O15_A128Sistema_Descricao ;
      private bool[] T000O15_n128Sistema_Descricao ;
      private String[] T000O15_A129Sistema_Sigla ;
      private String[] T000O15_A513Sistema_Coordenacao ;
      private bool[] T000O15_n513Sistema_Coordenacao ;
      private String[] T000O15_A136Sistema_AreaTrabalhoDes ;
      private bool[] T000O15_n136Sistema_AreaTrabalhoDes ;
      private String[] T000O15_A352AmbienteTecnologico_Descricao ;
      private String[] T000O15_A138Metodologia_Descricao ;
      private String[] T000O15_A699Sistema_Tipo ;
      private bool[] T000O15_n699Sistema_Tipo ;
      private String[] T000O15_A700Sistema_Tecnica ;
      private bool[] T000O15_n700Sistema_Tecnica ;
      private decimal[] T000O15_A686Sistema_FatorAjuste ;
      private bool[] T000O15_n686Sistema_FatorAjuste ;
      private decimal[] T000O15_A687Sistema_Custo ;
      private bool[] T000O15_n687Sistema_Custo ;
      private short[] T000O15_A688Sistema_Prazo ;
      private bool[] T000O15_n688Sistema_Prazo ;
      private short[] T000O15_A689Sistema_Esforco ;
      private bool[] T000O15_n689Sistema_Esforco ;
      private DateTime[] T000O15_A1401Sistema_ImpData ;
      private bool[] T000O15_n1401Sistema_ImpData ;
      private String[] T000O15_A1403Sistema_ImpUserPesNom ;
      private bool[] T000O15_n1403Sistema_ImpUserPesNom ;
      private int[] T000O15_A1831Sistema_Responsavel ;
      private bool[] T000O15_n1831Sistema_Responsavel ;
      private String[] T000O15_A1860SistemaVersao_Id ;
      private bool[] T000O15_A130Sistema_Ativo ;
      private String[] T000O15_A2109Sistema_Repositorio ;
      private bool[] T000O15_n2109Sistema_Repositorio ;
      private short[] T000O15_A2130Sistema_Ultimo ;
      private int[] T000O15_A137Metodologia_Codigo ;
      private bool[] T000O15_n137Metodologia_Codigo ;
      private int[] T000O15_A351AmbienteTecnologico_Codigo ;
      private bool[] T000O15_n351AmbienteTecnologico_Codigo ;
      private int[] T000O15_A1859SistemaVersao_Codigo ;
      private bool[] T000O15_n1859SistemaVersao_Codigo ;
      private int[] T000O15_A135Sistema_AreaTrabalhoCod ;
      private int[] T000O15_A1399Sistema_ImpUserCod ;
      private bool[] T000O15_n1399Sistema_ImpUserCod ;
      private int[] T000O15_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T000O15_n2161Sistema_GpoObjCtrlCod ;
      private int[] T000O15_A1402Sistema_ImpUserPesCod ;
      private bool[] T000O15_n1402Sistema_ImpUserPesCod ;
      private int[] T000O12_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T000O12_n2161Sistema_GpoObjCtrlCod ;
      private String[] T000O16_A352AmbienteTecnologico_Descricao ;
      private String[] T000O17_A138Metodologia_Descricao ;
      private int[] T000O18_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T000O18_n2161Sistema_GpoObjCtrlCod ;
      private String[] T000O19_A1860SistemaVersao_Id ;
      private String[] T000O20_A136Sistema_AreaTrabalhoDes ;
      private bool[] T000O20_n136Sistema_AreaTrabalhoDes ;
      private int[] T000O21_A1402Sistema_ImpUserPesCod ;
      private bool[] T000O21_n1402Sistema_ImpUserPesCod ;
      private String[] T000O22_A1403Sistema_ImpUserPesNom ;
      private bool[] T000O22_n1403Sistema_ImpUserPesNom ;
      private int[] T000O23_A127Sistema_Codigo ;
      private int[] T000O6_A127Sistema_Codigo ;
      private String[] T000O6_A416Sistema_Nome ;
      private String[] T000O6_A128Sistema_Descricao ;
      private bool[] T000O6_n128Sistema_Descricao ;
      private String[] T000O6_A129Sistema_Sigla ;
      private String[] T000O6_A513Sistema_Coordenacao ;
      private bool[] T000O6_n513Sistema_Coordenacao ;
      private String[] T000O6_A699Sistema_Tipo ;
      private bool[] T000O6_n699Sistema_Tipo ;
      private String[] T000O6_A700Sistema_Tecnica ;
      private bool[] T000O6_n700Sistema_Tecnica ;
      private decimal[] T000O6_A686Sistema_FatorAjuste ;
      private bool[] T000O6_n686Sistema_FatorAjuste ;
      private decimal[] T000O6_A687Sistema_Custo ;
      private bool[] T000O6_n687Sistema_Custo ;
      private short[] T000O6_A688Sistema_Prazo ;
      private bool[] T000O6_n688Sistema_Prazo ;
      private short[] T000O6_A689Sistema_Esforco ;
      private bool[] T000O6_n689Sistema_Esforco ;
      private DateTime[] T000O6_A1401Sistema_ImpData ;
      private bool[] T000O6_n1401Sistema_ImpData ;
      private int[] T000O6_A1831Sistema_Responsavel ;
      private bool[] T000O6_n1831Sistema_Responsavel ;
      private bool[] T000O6_A130Sistema_Ativo ;
      private String[] T000O6_A2109Sistema_Repositorio ;
      private bool[] T000O6_n2109Sistema_Repositorio ;
      private short[] T000O6_A2130Sistema_Ultimo ;
      private int[] T000O6_A137Metodologia_Codigo ;
      private bool[] T000O6_n137Metodologia_Codigo ;
      private int[] T000O6_A351AmbienteTecnologico_Codigo ;
      private bool[] T000O6_n351AmbienteTecnologico_Codigo ;
      private int[] T000O6_A1859SistemaVersao_Codigo ;
      private bool[] T000O6_n1859SistemaVersao_Codigo ;
      private int[] T000O6_A135Sistema_AreaTrabalhoCod ;
      private int[] T000O6_A1399Sistema_ImpUserCod ;
      private bool[] T000O6_n1399Sistema_ImpUserCod ;
      private int[] T000O6_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T000O6_n2161Sistema_GpoObjCtrlCod ;
      private int[] T000O24_A127Sistema_Codigo ;
      private int[] T000O25_A127Sistema_Codigo ;
      private int[] T000O5_A127Sistema_Codigo ;
      private String[] T000O5_A416Sistema_Nome ;
      private String[] T000O5_A128Sistema_Descricao ;
      private bool[] T000O5_n128Sistema_Descricao ;
      private String[] T000O5_A129Sistema_Sigla ;
      private String[] T000O5_A513Sistema_Coordenacao ;
      private bool[] T000O5_n513Sistema_Coordenacao ;
      private String[] T000O5_A699Sistema_Tipo ;
      private bool[] T000O5_n699Sistema_Tipo ;
      private String[] T000O5_A700Sistema_Tecnica ;
      private bool[] T000O5_n700Sistema_Tecnica ;
      private decimal[] T000O5_A686Sistema_FatorAjuste ;
      private bool[] T000O5_n686Sistema_FatorAjuste ;
      private decimal[] T000O5_A687Sistema_Custo ;
      private bool[] T000O5_n687Sistema_Custo ;
      private short[] T000O5_A688Sistema_Prazo ;
      private bool[] T000O5_n688Sistema_Prazo ;
      private short[] T000O5_A689Sistema_Esforco ;
      private bool[] T000O5_n689Sistema_Esforco ;
      private DateTime[] T000O5_A1401Sistema_ImpData ;
      private bool[] T000O5_n1401Sistema_ImpData ;
      private int[] T000O5_A1831Sistema_Responsavel ;
      private bool[] T000O5_n1831Sistema_Responsavel ;
      private bool[] T000O5_A130Sistema_Ativo ;
      private String[] T000O5_A2109Sistema_Repositorio ;
      private bool[] T000O5_n2109Sistema_Repositorio ;
      private short[] T000O5_A2130Sistema_Ultimo ;
      private int[] T000O5_A137Metodologia_Codigo ;
      private bool[] T000O5_n137Metodologia_Codigo ;
      private int[] T000O5_A351AmbienteTecnologico_Codigo ;
      private bool[] T000O5_n351AmbienteTecnologico_Codigo ;
      private int[] T000O5_A1859SistemaVersao_Codigo ;
      private bool[] T000O5_n1859SistemaVersao_Codigo ;
      private int[] T000O5_A135Sistema_AreaTrabalhoCod ;
      private int[] T000O5_A1399Sistema_ImpUserCod ;
      private bool[] T000O5_n1399Sistema_ImpUserCod ;
      private int[] T000O5_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T000O5_n2161Sistema_GpoObjCtrlCod ;
      private int[] T000O26_A127Sistema_Codigo ;
      private String[] T000O29_A352AmbienteTecnologico_Descricao ;
      private String[] T000O30_A138Metodologia_Descricao ;
      private String[] T000O31_A1860SistemaVersao_Id ;
      private String[] T000O32_A136Sistema_AreaTrabalhoDes ;
      private bool[] T000O32_n136Sistema_AreaTrabalhoDes ;
      private int[] T000O33_A1402Sistema_ImpUserPesCod ;
      private bool[] T000O33_n1402Sistema_ImpUserPesCod ;
      private String[] T000O34_A1403Sistema_ImpUserPesNom ;
      private bool[] T000O34_n1403Sistema_ImpUserPesNom ;
      private int[] T000O35_A648Projeto_Codigo ;
      private int[] T000O36_A1859SistemaVersao_Codigo ;
      private bool[] T000O36_n1859SistemaVersao_Codigo ;
      private int[] T000O37_A1725ContratoSistemas_CntCod ;
      private int[] T000O37_A1726ContratoSistemas_SistemaCod ;
      private int[] T000O38_A160ContratoServicos_Codigo ;
      private int[] T000O38_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T000O38_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T000O39_A996SolicitacaoMudanca_Codigo ;
      private int[] T000O40_A192Contagem_Codigo ;
      private int[] T000O41_A736ProjetoMelhoria_Codigo ;
      private int[] T000O42_A456ContagemResultado_Codigo ;
      private int[] T000O43_A368FuncaoDados_Codigo ;
      private int[] T000O44_A165FuncaoAPF_Codigo ;
      private int[] T000O45_A172Tabela_Codigo ;
      private int[] T000O46_A439Solicitacoes_Codigo ;
      private int[] T000O47_A161FuncaoUsuario_Codigo ;
      private int[] T000O48_A146Modulo_Codigo ;
      private int[] T000O49_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000O49_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000O49_A127Sistema_Codigo ;
      private int[] T000O50_A127Sistema_Codigo ;
      private int[] T000O50_A1460SistemaDePara_Codigo ;
      private int[] T000O52_A127Sistema_Codigo ;
      private String[] T000O4_A132Tecnologia_Nome ;
      private String[] T000O4_A355Tecnologia_TipoTecnologia ;
      private bool[] T000O4_n355Tecnologia_TipoTecnologia ;
      private int[] T000O53_A127Sistema_Codigo ;
      private short[] T000O53_A2128SistemaTecnologiaLinha ;
      private String[] T000O53_A132Tecnologia_Nome ;
      private String[] T000O53_A355Tecnologia_TipoTecnologia ;
      private bool[] T000O53_n355Tecnologia_TipoTecnologia ;
      private int[] T000O53_A131Tecnologia_Codigo ;
      private String[] T000O54_A132Tecnologia_Nome ;
      private String[] T000O54_A355Tecnologia_TipoTecnologia ;
      private bool[] T000O54_n355Tecnologia_TipoTecnologia ;
      private int[] T000O55_A127Sistema_Codigo ;
      private short[] T000O55_A2128SistemaTecnologiaLinha ;
      private int[] T000O3_A127Sistema_Codigo ;
      private short[] T000O3_A2128SistemaTecnologiaLinha ;
      private int[] T000O3_A131Tecnologia_Codigo ;
      private int[] T000O2_A127Sistema_Codigo ;
      private short[] T000O2_A2128SistemaTecnologiaLinha ;
      private int[] T000O2_A131Tecnologia_Codigo ;
      private String[] T000O59_A132Tecnologia_Nome ;
      private String[] T000O59_A355Tecnologia_TipoTecnologia ;
      private bool[] T000O59_n355Tecnologia_TipoTecnologia ;
      private int[] T000O60_A127Sistema_Codigo ;
      private short[] T000O60_A2128SistemaTecnologiaLinha ;
      private int[] T000O61_A1826GpoObjCtrl_Codigo ;
      private String[] T000O61_A1827GpoObjCtrl_Nome ;
      private int[] T000O62_A1859SistemaVersao_Codigo ;
      private bool[] T000O62_n1859SistemaVersao_Codigo ;
      private String[] T000O62_A1860SistemaVersao_Id ;
      private int[] T000O62_A1866SistemaVersao_SistemaCod ;
      private int[] T000O63_A351AmbienteTecnologico_Codigo ;
      private bool[] T000O63_n351AmbienteTecnologico_Codigo ;
      private String[] T000O63_A352AmbienteTecnologico_Descricao ;
      private int[] T000O63_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private bool[] T000O63_A353AmbienteTecnologico_Ativo ;
      private int[] T000O64_A137Metodologia_Codigo ;
      private bool[] T000O64_n137Metodologia_Codigo ;
      private String[] T000O64_A138Metodologia_Descricao ;
      private int[] T000O66_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T000O66_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] T000O66_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000O66_A60ContratanteUsuario_UsuarioCod ;
      private String[] T000O66_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000O66_n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000O66_A54Usuario_Ativo ;
      private bool[] T000O66_n54Usuario_Ativo ;
      private int[] T000O66_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] T000O66_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] T000O67_A131Tecnologia_Codigo ;
      private String[] T000O67_A132Tecnologia_Nome ;
      private int[] T000O68_A1826GpoObjCtrl_Codigo ;
      private String[] T000O68_A1827GpoObjCtrl_Nome ;
      private int[] T000O69_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T000O69_n2161Sistema_GpoObjCtrlCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV54AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class sistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new UpdateCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new UpdateCursor(def[54])
         ,new UpdateCursor(def[55])
         ,new UpdateCursor(def[56])
         ,new ForEachCursor(def[57])
         ,new ForEachCursor(def[58])
         ,new ForEachCursor(def[59])
         ,new ForEachCursor(def[60])
         ,new ForEachCursor(def[61])
         ,new ForEachCursor(def[62])
         ,new ForEachCursor(def[63])
         ,new ForEachCursor(def[64])
         ,new ForEachCursor(def[65])
         ,new ForEachCursor(def[66])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000O14 ;
          prmT000O14 = new Object[] {
          } ;
          Object[] prmT000O15 ;
          prmT000O15 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O8 ;
          prmT000O8 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O7 ;
          prmT000O7 = new Object[] {
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O12 ;
          prmT000O12 = new Object[] {
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O9 ;
          prmT000O9 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O10 ;
          prmT000O10 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O11 ;
          prmT000O11 = new Object[] {
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O13 ;
          prmT000O13 = new Object[] {
          new Object[] {"@Sistema_ImpUserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O16 ;
          prmT000O16 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O17 ;
          prmT000O17 = new Object[] {
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O18 ;
          prmT000O18 = new Object[] {
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O19 ;
          prmT000O19 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O20 ;
          prmT000O20 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O21 ;
          prmT000O21 = new Object[] {
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O22 ;
          prmT000O22 = new Object[] {
          new Object[] {"@Sistema_ImpUserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O23 ;
          prmT000O23 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O6 ;
          prmT000O6 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O24 ;
          prmT000O24 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O25 ;
          prmT000O25 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O5 ;
          prmT000O5 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O26 ;
          prmT000O26 = new Object[] {
          new Object[] {"@Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Sistema_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@Sistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Sistema_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Sistema_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Sistema_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_ImpData",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Sistema_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Sistema_Repositorio",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O27 ;
          prmT000O27 = new Object[] {
          new Object[] {"@Sistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Sistema_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@Sistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Sistema_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Sistema_Custo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Sistema_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_ImpData",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Sistema_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Sistema_Repositorio",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O28 ;
          prmT000O28 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O32 ;
          prmT000O32 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O33 ;
          prmT000O33 = new Object[] {
          new Object[] {"@Sistema_ImpUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O34 ;
          prmT000O34 = new Object[] {
          new Object[] {"@Sistema_ImpUserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O35 ;
          prmT000O35 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O36 ;
          prmT000O36 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O37 ;
          prmT000O37 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O38 ;
          prmT000O38 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O39 ;
          prmT000O39 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O40 ;
          prmT000O40 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O41 ;
          prmT000O41 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O42 ;
          prmT000O42 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O43 ;
          prmT000O43 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O44 ;
          prmT000O44 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O45 ;
          prmT000O45 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O46 ;
          prmT000O46 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O47 ;
          prmT000O47 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O48 ;
          prmT000O48 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O49 ;
          prmT000O49 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O50 ;
          prmT000O50 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O51 ;
          prmT000O51 = new Object[] {
          new Object[] {"@Sistema_Ultimo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O52 ;
          prmT000O52 = new Object[] {
          } ;
          Object[] prmT000O53 ;
          prmT000O53 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000O4 ;
          prmT000O4 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O54 ;
          prmT000O54 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O55 ;
          prmT000O55 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000O3 ;
          prmT000O3 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000O2 ;
          prmT000O2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000O56 ;
          prmT000O56 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O57 ;
          prmT000O57 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000O58 ;
          prmT000O58 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaTecnologiaLinha",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000O60 ;
          prmT000O60 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O61 ;
          prmT000O61 = new Object[] {
          } ;
          Object[] prmT000O62 ;
          prmT000O62 = new Object[] {
          new Object[] {"@AV7Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O63 ;
          prmT000O63 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O64 ;
          prmT000O64 = new Object[] {
          } ;
          Object[] prmT000O66 ;
          prmT000O66 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O67 ;
          prmT000O67 = new Object[] {
          } ;
          Object[] prmT000O68 ;
          prmT000O68 = new Object[] {
          } ;
          Object[] prmT000O31 ;
          prmT000O31 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O29 ;
          prmT000O29 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O30 ;
          prmT000O30 = new Object[] {
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O69 ;
          prmT000O69 = new Object[] {
          new Object[] {"@Sistema_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000O59 ;
          prmT000O59 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000O2", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha], [Tecnologia_Codigo] FROM [SistemaTecnologia] WITH (UPDLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O2,1,0,true,false )
             ,new CursorDef("T000O3", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha], [Tecnologia_Codigo] FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O3,1,0,true,false )
             ,new CursorDef("T000O4", "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O4,1,0,true,false )
             ,new CursorDef("T000O5", "SELECT [Sistema_Codigo], [Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_ImpData], [Sistema_Responsavel], [Sistema_Ativo], [Sistema_Repositorio], [Sistema_Ultimo], [Metodologia_Codigo], [AmbienteTecnologico_Codigo], [SistemaVersao_Codigo], [Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, [Sistema_ImpUserCod] AS Sistema_ImpUserCod, [Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod FROM [Sistema] WITH (UPDLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O5,1,0,true,false )
             ,new CursorDef("T000O6", "SELECT [Sistema_Codigo], [Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_ImpData], [Sistema_Responsavel], [Sistema_Ativo], [Sistema_Repositorio], [Sistema_Ultimo], [Metodologia_Codigo], [AmbienteTecnologico_Codigo], [SistemaVersao_Codigo], [Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, [Sistema_ImpUserCod] AS Sistema_ImpUserCod, [Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O6,1,0,true,false )
             ,new CursorDef("T000O7", "SELECT [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) WHERE [Metodologia_Codigo] = @Metodologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O7,1,0,true,false )
             ,new CursorDef("T000O8", "SELECT [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O8,1,0,true,false )
             ,new CursorDef("T000O9", "SELECT [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O9,1,0,true,false )
             ,new CursorDef("T000O10", "SELECT [AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Sistema_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O10,1,0,true,false )
             ,new CursorDef("T000O11", "SELECT [Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Sistema_ImpUserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O11,1,0,true,false )
             ,new CursorDef("T000O12", "SELECT [GpoObjCtrl_Codigo] AS Sistema_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Sistema_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O12,1,0,true,false )
             ,new CursorDef("T000O13", "SELECT [Pessoa_Nome] AS Sistema_ImpUserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Sistema_ImpUserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O13,1,0,true,false )
             ,new CursorDef("T000O14", "SELECT [GpoObjCtrl_Codigo], [GpoObjCtrl_Nome] FROM [GrupoObjetoControle] WITH (NOLOCK) ORDER BY [GpoObjCtrl_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O14,0,0,true,false )
             ,new CursorDef("T000O15", "SELECT TM1.[Sistema_Codigo], TM1.[Sistema_Nome], TM1.[Sistema_Descricao], TM1.[Sistema_Sigla], TM1.[Sistema_Coordenacao], T2.[AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes, T5.[AmbienteTecnologico_Descricao], T6.[Metodologia_Descricao], TM1.[Sistema_Tipo], TM1.[Sistema_Tecnica], TM1.[Sistema_FatorAjuste], TM1.[Sistema_Custo], TM1.[Sistema_Prazo], TM1.[Sistema_Esforco], TM1.[Sistema_ImpData], T4.[Pessoa_Nome] AS Sistema_ImpUserPesNom, TM1.[Sistema_Responsavel], T7.[SistemaVersao_Id], TM1.[Sistema_Ativo], TM1.[Sistema_Repositorio], TM1.[Sistema_Ultimo], TM1.[Metodologia_Codigo], TM1.[AmbienteTecnologico_Codigo], TM1.[SistemaVersao_Codigo], TM1.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, TM1.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, TM1.[Sistema_GpoObjCtrlCod] AS Sistema_GpoObjCtrlCod, T3.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM (((((([Sistema] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN [AmbienteTecnologico] T5 WITH (NOLOCK) ON T5.[AmbienteTecnologico_Codigo] = TM1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T6 WITH (NOLOCK) ON T6.[Metodologia_Codigo] = TM1.[Metodologia_Codigo]) LEFT JOIN [SistemaVersao] T7 WITH (NOLOCK) ON T7.[SistemaVersao_Codigo] = TM1.[SistemaVersao_Codigo]) WHERE TM1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY TM1.[Sistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000O15,100,0,true,false )
             ,new CursorDef("T000O16", "SELECT [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O16,1,0,true,false )
             ,new CursorDef("T000O17", "SELECT [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) WHERE [Metodologia_Codigo] = @Metodologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O17,1,0,true,false )
             ,new CursorDef("T000O18", "SELECT [GpoObjCtrl_Codigo] AS Sistema_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Sistema_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O18,1,0,true,false )
             ,new CursorDef("T000O19", "SELECT [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O19,1,0,true,false )
             ,new CursorDef("T000O20", "SELECT [AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Sistema_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O20,1,0,true,false )
             ,new CursorDef("T000O21", "SELECT [Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Sistema_ImpUserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O21,1,0,true,false )
             ,new CursorDef("T000O22", "SELECT [Pessoa_Nome] AS Sistema_ImpUserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Sistema_ImpUserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O22,1,0,true,false )
             ,new CursorDef("T000O23", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000O23,1,0,true,false )
             ,new CursorDef("T000O24", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ( [Sistema_Codigo] > @Sistema_Codigo) ORDER BY [Sistema_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000O24,1,0,true,true )
             ,new CursorDef("T000O25", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ( [Sistema_Codigo] < @Sistema_Codigo) ORDER BY [Sistema_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000O25,1,0,true,true )
             ,new CursorDef("T000O26", "INSERT INTO [Sistema]([Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Tipo], [Sistema_Tecnica], [Sistema_FatorAjuste], [Sistema_Custo], [Sistema_Prazo], [Sistema_Esforco], [Sistema_ImpData], [Sistema_Responsavel], [Sistema_Ativo], [Sistema_Repositorio], [Sistema_Ultimo], [Metodologia_Codigo], [AmbienteTecnologico_Codigo], [SistemaVersao_Codigo], [Sistema_AreaTrabalhoCod], [Sistema_ImpUserCod], [Sistema_GpoObjCtrlCod]) VALUES(@Sistema_Nome, @Sistema_Descricao, @Sistema_Sigla, @Sistema_Coordenacao, @Sistema_Tipo, @Sistema_Tecnica, @Sistema_FatorAjuste, @Sistema_Custo, @Sistema_Prazo, @Sistema_Esforco, @Sistema_ImpData, @Sistema_Responsavel, @Sistema_Ativo, @Sistema_Repositorio, @Sistema_Ultimo, @Metodologia_Codigo, @AmbienteTecnologico_Codigo, @SistemaVersao_Codigo, @Sistema_AreaTrabalhoCod, @Sistema_ImpUserCod, @Sistema_GpoObjCtrlCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000O26)
             ,new CursorDef("T000O27", "UPDATE [Sistema] SET [Sistema_Nome]=@Sistema_Nome, [Sistema_Descricao]=@Sistema_Descricao, [Sistema_Sigla]=@Sistema_Sigla, [Sistema_Coordenacao]=@Sistema_Coordenacao, [Sistema_Tipo]=@Sistema_Tipo, [Sistema_Tecnica]=@Sistema_Tecnica, [Sistema_FatorAjuste]=@Sistema_FatorAjuste, [Sistema_Custo]=@Sistema_Custo, [Sistema_Prazo]=@Sistema_Prazo, [Sistema_Esforco]=@Sistema_Esforco, [Sistema_ImpData]=@Sistema_ImpData, [Sistema_Responsavel]=@Sistema_Responsavel, [Sistema_Ativo]=@Sistema_Ativo, [Sistema_Repositorio]=@Sistema_Repositorio, [Sistema_Ultimo]=@Sistema_Ultimo, [Metodologia_Codigo]=@Metodologia_Codigo, [AmbienteTecnologico_Codigo]=@AmbienteTecnologico_Codigo, [SistemaVersao_Codigo]=@SistemaVersao_Codigo, [Sistema_AreaTrabalhoCod]=@Sistema_AreaTrabalhoCod, [Sistema_ImpUserCod]=@Sistema_ImpUserCod, [Sistema_GpoObjCtrlCod]=@Sistema_GpoObjCtrlCod  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmT000O27)
             ,new CursorDef("T000O28", "DELETE FROM [Sistema]  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmT000O28)
             ,new CursorDef("T000O29", "SELECT [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O29,1,0,true,false )
             ,new CursorDef("T000O30", "SELECT [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) WHERE [Metodologia_Codigo] = @Metodologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O30,1,0,true,false )
             ,new CursorDef("T000O31", "SELECT [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O31,1,0,true,false )
             ,new CursorDef("T000O32", "SELECT [AreaTrabalho_Descricao] AS Sistema_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Sistema_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O32,1,0,true,false )
             ,new CursorDef("T000O33", "SELECT [Usuario_PessoaCod] AS Sistema_ImpUserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Sistema_ImpUserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O33,1,0,true,false )
             ,new CursorDef("T000O34", "SELECT [Pessoa_Nome] AS Sistema_ImpUserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Sistema_ImpUserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O34,1,0,true,false )
             ,new CursorDef("T000O35", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_SistemaCodigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O35,1,0,true,true )
             ,new CursorDef("T000O36", "SELECT TOP 1 [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O36,1,0,true,true )
             ,new CursorDef("T000O37", "SELECT TOP 1 [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O37,1,0,true,true )
             ,new CursorDef("T000O38", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicosSistemas_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O38,1,0,true,true )
             ,new CursorDef("T000O39", "SELECT TOP 1 [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O39,1,0,true,true )
             ,new CursorDef("T000O40", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O40,1,0,true,true )
             ,new CursorDef("T000O41", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O41,1,0,true,true )
             ,new CursorDef("T000O42", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O42,1,0,true,true )
             ,new CursorDef("T000O43", "SELECT TOP 1 [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O43,1,0,true,true )
             ,new CursorDef("T000O44", "SELECT TOP 1 [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O44,1,0,true,true )
             ,new CursorDef("T000O45", "SELECT TOP 1 [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_SistemaCod] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O45,1,0,true,true )
             ,new CursorDef("T000O46", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O46,1,0,true,true )
             ,new CursorDef("T000O47", "SELECT TOP 1 [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O47,1,0,true,true )
             ,new CursorDef("T000O48", "SELECT TOP 1 [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O48,1,0,true,true )
             ,new CursorDef("T000O49", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O49,1,0,true,true )
             ,new CursorDef("T000O50", "SELECT TOP 1 [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O50,1,0,true,true )
             ,new CursorDef("T000O51", "UPDATE [Sistema] SET [Sistema_Ultimo]=@Sistema_Ultimo  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmT000O51)
             ,new CursorDef("T000O52", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) ORDER BY [Sistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000O52,100,0,true,false )
             ,new CursorDef("T000O53", "SELECT T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha], T2.[Tecnologia_Nome], T2.[Tecnologia_TipoTecnologia], T1.[Tecnologia_Codigo] FROM ([SistemaTecnologia] T1 WITH (NOLOCK) INNER JOIN [Tecnologia] T2 WITH (NOLOCK) ON T2.[Tecnologia_Codigo] = T1.[Tecnologia_Codigo]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo and T1.[SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ORDER BY T1.[Sistema_Codigo], T1.[SistemaTecnologiaLinha] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O53,11,0,true,false )
             ,new CursorDef("T000O54", "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O54,1,0,true,false )
             ,new CursorDef("T000O55", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha] FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O55,1,0,true,false )
             ,new CursorDef("T000O56", "INSERT INTO [SistemaTecnologia]([Sistema_Codigo], [SistemaTecnologiaLinha], [Tecnologia_Codigo]) VALUES(@Sistema_Codigo, @SistemaTecnologiaLinha, @Tecnologia_Codigo)", GxErrorMask.GX_NOMASK,prmT000O56)
             ,new CursorDef("T000O57", "UPDATE [SistemaTecnologia] SET [Tecnologia_Codigo]=@Tecnologia_Codigo  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha", GxErrorMask.GX_NOMASK,prmT000O57)
             ,new CursorDef("T000O58", "DELETE FROM [SistemaTecnologia]  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaTecnologiaLinha] = @SistemaTecnologiaLinha", GxErrorMask.GX_NOMASK,prmT000O58)
             ,new CursorDef("T000O59", "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O59,1,0,true,false )
             ,new CursorDef("T000O60", "SELECT [Sistema_Codigo], [SistemaTecnologiaLinha] FROM [SistemaTecnologia] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ORDER BY [Sistema_Codigo], [SistemaTecnologiaLinha] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O60,11,0,true,false )
             ,new CursorDef("T000O61", "SELECT [GpoObjCtrl_Codigo], [GpoObjCtrl_Nome] FROM [GrupoObjetoControle] WITH (NOLOCK) ORDER BY [GpoObjCtrl_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O61,0,0,true,false )
             ,new CursorDef("T000O62", "SELECT [SistemaVersao_Codigo], [SistemaVersao_Id], [SistemaVersao_SistemaCod] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_SistemaCod] = @AV7Sistema_Codigo ORDER BY [SistemaVersao_Id] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O62,0,0,true,false )
             ,new CursorDef("T000O63", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao], [AmbienteTecnologico_AreaTrabalhoCod], [AmbienteTecnologico_Ativo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE ([AmbienteTecnologico_Ativo] = 1) AND ([AmbienteTecnologico_AreaTrabalhoCod] = @AV8WWPCo_1Areatrabalho_codigo) ORDER BY [AmbienteTecnologico_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O63,0,0,true,false )
             ,new CursorDef("T000O64", "SELECT [Metodologia_Codigo], [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) ORDER BY [Metodologia_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O64,0,0,true,false )
             ,new CursorDef("T000O66", "SELECT T3.[Pessoa_Codigo] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_Ativo], COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE T5.[Contratante_Codigo] = T6.[ContratanteUsuario_ContratanteCod] GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV8WWPCo_1Areatrabalho_codigo) ORDER BY T3.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O66,0,0,true,false )
             ,new CursorDef("T000O67", "SELECT [Tecnologia_Codigo], [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) ORDER BY [Tecnologia_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O67,0,0,true,false )
             ,new CursorDef("T000O68", "SELECT [GpoObjCtrl_Codigo], [GpoObjCtrl_Nome] FROM [GrupoObjetoControle] WITH (NOLOCK) ORDER BY [GpoObjCtrl_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O68,0,0,true,false )
             ,new CursorDef("T000O69", "SELECT [GpoObjCtrl_Codigo] AS Sistema_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Sistema_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000O69,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((bool[]) buf[23])[0] = rslt.getBool(14) ;
                ((String[]) buf[24])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((int[]) buf[36])[0] = rslt.getInt(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((bool[]) buf[23])[0] = rslt.getBool(14) ;
                ((String[]) buf[24])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((int[]) buf[36])[0] = rslt.getInt(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getString(18, 20) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((int[]) buf[34])[0] = rslt.getInt(22) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((int[]) buf[36])[0] = rslt.getInt(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((int[]) buf[38])[0] = rslt.getInt(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((int[]) buf[41])[0] = rslt.getInt(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 50 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 51 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 52 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 57 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 58 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 59 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
       getresults60( cursor, rslt, buf) ;
    }

    public void getresults60( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 60 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 61 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 62 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 63 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 64 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 65 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 66 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(11, (DateTime)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[21]);
                }
                stmt.SetParameter(13, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (short)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[31]);
                }
                stmt.SetParameter(19, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[36]);
                }
                return;
             case 25 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(11, (DateTime)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[21]);
                }
                stmt.SetParameter(13, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (short)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[31]);
                }
                stmt.SetParameter(19, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[36]);
                }
                stmt.SetParameter(22, (int)parms[37]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 48 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 49 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 51 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 52 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 54 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 55 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 56 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 57 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 58 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters60( cursor, stmt, parms) ;
    }

    public void setparameters60( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 60 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 61 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 63 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 66 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
