/*
               File: PRC_LimitarIFPUG
        Description: PRC_Limitar IFPUG
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:22.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_limitarifpug : GXProcedure
   {
      public prc_limitarifpug( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_limitarifpug( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref decimal aP0_LimitePF ,
                           ref short aP1_LimiteBaselines )
      {
         this.AV8LimitePF = aP0_LimitePF;
         this.AV9LimiteBaselines = aP1_LimiteBaselines;
         initialize();
         executePrivate();
         aP0_LimitePF=this.AV8LimitePF;
         aP1_LimiteBaselines=this.AV9LimiteBaselines;
      }

      public short executeUdp( ref decimal aP0_LimitePF )
      {
         this.AV8LimitePF = aP0_LimitePF;
         this.AV9LimiteBaselines = aP1_LimiteBaselines;
         initialize();
         executePrivate();
         aP0_LimitePF=this.AV8LimitePF;
         aP1_LimiteBaselines=this.AV9LimiteBaselines;
         return AV9LimiteBaselines ;
      }

      public void executeSubmit( ref decimal aP0_LimitePF ,
                                 ref short aP1_LimiteBaselines )
      {
         prc_limitarifpug objprc_limitarifpug;
         objprc_limitarifpug = new prc_limitarifpug();
         objprc_limitarifpug.AV8LimitePF = aP0_LimitePF;
         objprc_limitarifpug.AV9LimiteBaselines = aP1_LimiteBaselines;
         objprc_limitarifpug.context.SetSubmitInitialConfig(context);
         objprc_limitarifpug.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_limitarifpug);
         aP0_LimitePF=this.AV8LimitePF;
         aP1_LimiteBaselines=this.AV9LimiteBaselines;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_limitarifpug)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21SDT_Codigos.FromXml(AV20WebSession.Get("Codigos"), "SDT_CodigosCollection");
         AV25GXV1 = 1;
         while ( AV25GXV1 <= AV21SDT_Codigos.Count )
         {
            AV22Codigo = ((SdtSDT_Codigos)AV21SDT_Codigos.Item(AV25GXV1));
            AV13Codigos.Add(AV22Codigo.gxTpr_Codigo, 0);
            AV25GXV1 = (int)(AV25GXV1+1);
         }
         AV21SDT_Codigos.Clear();
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV13Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00W52 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00W52_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00W52_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = P00W52_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00W52_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00W52_n52Contratada_AreaTrabalhoCod[0];
            A52Contratada_AreaTrabalhoCod = P00W52_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00W52_n52Contratada_AreaTrabalhoCod[0];
            AV14Area_Codigos.Add(A52Contratada_AreaTrabalhoCod, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV13Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00W53 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00W53_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00W53_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00W53_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = P00W53_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00W53_n1603ContagemResultado_CntCod[0];
            A1603ContagemResultado_CntCod = P00W53_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00W53_n1603ContagemResultado_CntCod[0];
            AV15Contrato_Codigos.Add(A1603ContagemResultado_CntCod, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A5AreaTrabalho_Codigo ,
                                              AV14Area_Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00W54 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A5AreaTrabalho_Codigo = P00W54_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = P00W54_A6AreaTrabalho_Descricao[0];
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A1603ContagemResultado_CntCod ,
                                                 AV15Contrato_Codigos ,
                                                 A456ContagemResultado_Codigo ,
                                                 AV13Codigos ,
                                                 A598ContagemResultado_Baseline ,
                                                 A517ContagemResultado_Ultima ,
                                                 A5AreaTrabalho_Codigo ,
                                                 A52Contratada_AreaTrabalhoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00W55 */
            pr_default.execute(3, new Object[] {A5AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               BRKW55 = false;
               A490ContagemResultado_ContratadaCod = P00W55_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00W55_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = P00W55_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00W55_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P00W55_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00W55_n1603ContagemResultado_CntCod[0];
               A52Contratada_AreaTrabalhoCod = P00W55_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00W55_n52Contratada_AreaTrabalhoCod[0];
               A460ContagemResultado_PFBFM = P00W55_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P00W55_n460ContagemResultado_PFBFM[0];
               A1326ContagemResultado_ContratadaTipoFab = P00W55_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P00W55_n1326ContagemResultado_ContratadaTipoFab[0];
               A458ContagemResultado_PFBFS = P00W55_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P00W55_n458ContagemResultado_PFBFS[0];
               A456ContagemResultado_Codigo = P00W55_A456ContagemResultado_Codigo[0];
               A517ContagemResultado_Ultima = P00W55_A517ContagemResultado_Ultima[0];
               A598ContagemResultado_Baseline = P00W55_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P00W55_n598ContagemResultado_Baseline[0];
               A473ContagemResultado_DataCnt = P00W55_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P00W55_A511ContagemResultado_HoraCnt[0];
               A490ContagemResultado_ContratadaCod = P00W55_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00W55_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = P00W55_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00W55_n1553ContagemResultado_CntSrvCod[0];
               A598ContagemResultado_Baseline = P00W55_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P00W55_n598ContagemResultado_Baseline[0];
               A52Contratada_AreaTrabalhoCod = P00W55_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00W55_n52Contratada_AreaTrabalhoCod[0];
               A1326ContagemResultado_ContratadaTipoFab = P00W55_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P00W55_n1326ContagemResultado_ContratadaTipoFab[0];
               A1603ContagemResultado_CntCod = P00W55_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00W55_n1603ContagemResultado_CntCod[0];
               W52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
               n52Contratada_AreaTrabalhoCod = false;
               while ( (pr_default.getStatus(3) != 101) && ( P00W55_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( P00W55_A1603ContagemResultado_CntCod[0] == A1603ContagemResultado_CntCod ) )
               {
                  BRKW55 = false;
                  A490ContagemResultado_ContratadaCod = P00W55_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W55_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W55_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W55_n1553ContagemResultado_CntSrvCod[0];
                  A460ContagemResultado_PFBFM = P00W55_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P00W55_n460ContagemResultado_PFBFM[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00W55_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00W55_n1326ContagemResultado_ContratadaTipoFab[0];
                  A458ContagemResultado_PFBFS = P00W55_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P00W55_n458ContagemResultado_PFBFS[0];
                  A456ContagemResultado_Codigo = P00W55_A456ContagemResultado_Codigo[0];
                  A473ContagemResultado_DataCnt = P00W55_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P00W55_A511ContagemResultado_HoraCnt[0];
                  A490ContagemResultado_ContratadaCod = P00W55_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W55_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W55_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W55_n1553ContagemResultado_CntSrvCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00W55_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00W55_n1326ContagemResultado_ContratadaTipoFab[0];
                  if ( (AV15Contrato_Codigos.IndexOf(A1603ContagemResultado_CntCod)>0) )
                  {
                     if ( (AV13Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
                     {
                        if ( ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 ) && ( A460ContagemResultado_PFBFM > Convert.ToDecimal( 0 )) )
                        {
                           AV18Total = (decimal)(AV18Total+A460ContagemResultado_PFBFM);
                           AV22Codigo = new SdtSDT_Codigos(context);
                           AV22Codigo.gxTpr_Codigo = A456ContagemResultado_Codigo;
                           AV21SDT_Codigos.Add(AV22Codigo, 0);
                        }
                        else if ( ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 ) && ( A458ContagemResultado_PFBFS > Convert.ToDecimal( 0 )) )
                        {
                           AV18Total = (decimal)(AV18Total+A458ContagemResultado_PFBFS);
                           AV22Codigo = new SdtSDT_Codigos(context);
                           AV22Codigo.gxTpr_Codigo = A456ContagemResultado_Codigo;
                           AV21SDT_Codigos.Add(AV22Codigo, 0);
                        }
                        if ( AV18Total >= AV8LimitePF )
                        {
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                        }
                     }
                  }
                  BRKW55 = true;
                  pr_default.readNext(3);
               }
               if ( AV18Total >= AV8LimitePF )
               {
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               A52Contratada_AreaTrabalhoCod = W52Contratada_AreaTrabalhoCod;
               n52Contratada_AreaTrabalhoCod = false;
               if ( ! BRKW55 )
               {
                  BRKW55 = true;
                  pr_default.readNext(3);
               }
            }
            pr_default.close(3);
            if ( AV18Total >= AV8LimitePF )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV18Total = 0;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A5AreaTrabalho_Codigo ,
                                              AV14Area_Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00W56 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            A5AreaTrabalho_Codigo = P00W56_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = P00W56_A6AreaTrabalho_Descricao[0];
            pr_default.dynParam(5, new Object[]{ new Object[]{
                                                 A1603ContagemResultado_CntCod ,
                                                 AV15Contrato_Codigos ,
                                                 A456ContagemResultado_Codigo ,
                                                 AV13Codigos ,
                                                 A598ContagemResultado_Baseline ,
                                                 A517ContagemResultado_Ultima ,
                                                 A5AreaTrabalho_Codigo ,
                                                 A52Contratada_AreaTrabalhoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00W57 */
            pr_default.execute(5, new Object[] {A5AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               BRKW58 = false;
               A490ContagemResultado_ContratadaCod = P00W57_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00W57_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = P00W57_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00W57_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P00W57_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00W57_n1603ContagemResultado_CntCod[0];
               A52Contratada_AreaTrabalhoCod = P00W57_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00W57_n52Contratada_AreaTrabalhoCod[0];
               A460ContagemResultado_PFBFM = P00W57_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P00W57_n460ContagemResultado_PFBFM[0];
               A1326ContagemResultado_ContratadaTipoFab = P00W57_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P00W57_n1326ContagemResultado_ContratadaTipoFab[0];
               A458ContagemResultado_PFBFS = P00W57_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P00W57_n458ContagemResultado_PFBFS[0];
               A456ContagemResultado_Codigo = P00W57_A456ContagemResultado_Codigo[0];
               A517ContagemResultado_Ultima = P00W57_A517ContagemResultado_Ultima[0];
               A598ContagemResultado_Baseline = P00W57_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P00W57_n598ContagemResultado_Baseline[0];
               A473ContagemResultado_DataCnt = P00W57_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P00W57_A511ContagemResultado_HoraCnt[0];
               A490ContagemResultado_ContratadaCod = P00W57_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00W57_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = P00W57_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00W57_n1553ContagemResultado_CntSrvCod[0];
               A598ContagemResultado_Baseline = P00W57_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = P00W57_n598ContagemResultado_Baseline[0];
               A52Contratada_AreaTrabalhoCod = P00W57_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00W57_n52Contratada_AreaTrabalhoCod[0];
               A1326ContagemResultado_ContratadaTipoFab = P00W57_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P00W57_n1326ContagemResultado_ContratadaTipoFab[0];
               A1603ContagemResultado_CntCod = P00W57_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00W57_n1603ContagemResultado_CntCod[0];
               W52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
               n52Contratada_AreaTrabalhoCod = false;
               while ( (pr_default.getStatus(5) != 101) && ( P00W57_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( P00W57_A1603ContagemResultado_CntCod[0] == A1603ContagemResultado_CntCod ) )
               {
                  BRKW58 = false;
                  A490ContagemResultado_ContratadaCod = P00W57_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W57_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W57_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W57_n1553ContagemResultado_CntSrvCod[0];
                  A460ContagemResultado_PFBFM = P00W57_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P00W57_n460ContagemResultado_PFBFM[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00W57_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00W57_n1326ContagemResultado_ContratadaTipoFab[0];
                  A458ContagemResultado_PFBFS = P00W57_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P00W57_n458ContagemResultado_PFBFS[0];
                  A456ContagemResultado_Codigo = P00W57_A456ContagemResultado_Codigo[0];
                  A473ContagemResultado_DataCnt = P00W57_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P00W57_A511ContagemResultado_HoraCnt[0];
                  A490ContagemResultado_ContratadaCod = P00W57_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W57_n490ContagemResultado_ContratadaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W57_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W57_n1553ContagemResultado_CntSrvCod[0];
                  A1326ContagemResultado_ContratadaTipoFab = P00W57_A1326ContagemResultado_ContratadaTipoFab[0];
                  n1326ContagemResultado_ContratadaTipoFab = P00W57_n1326ContagemResultado_ContratadaTipoFab[0];
                  if ( (AV15Contrato_Codigos.IndexOf(A1603ContagemResultado_CntCod)>0) )
                  {
                     if ( (AV13Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
                     {
                        if ( ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 ) && ( A460ContagemResultado_PFBFM > Convert.ToDecimal( 0 )) )
                        {
                           AV18Total = (decimal)(AV18Total+1);
                           AV22Codigo = new SdtSDT_Codigos(context);
                           AV22Codigo.gxTpr_Codigo = A456ContagemResultado_Codigo;
                           AV21SDT_Codigos.Add(AV22Codigo, 0);
                        }
                        else if ( ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 ) && ( A458ContagemResultado_PFBFS > Convert.ToDecimal( 0 )) )
                        {
                           AV18Total = (decimal)(AV18Total+1);
                           AV22Codigo = new SdtSDT_Codigos(context);
                           AV22Codigo.gxTpr_Codigo = A456ContagemResultado_Codigo;
                           AV21SDT_Codigos.Add(AV22Codigo, 0);
                        }
                        if ( ( AV18Total == Convert.ToDecimal( AV9LimiteBaselines )) )
                        {
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                        }
                     }
                  }
                  BRKW58 = true;
                  pr_default.readNext(5);
               }
               if ( ( AV18Total == Convert.ToDecimal( AV9LimiteBaselines )) )
               {
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               A52Contratada_AreaTrabalhoCod = W52Contratada_AreaTrabalhoCod;
               n52Contratada_AreaTrabalhoCod = false;
               if ( ! BRKW58 )
               {
                  BRKW58 = true;
                  pr_default.readNext(5);
               }
            }
            pr_default.close(5);
            if ( AV18Total >= AV8LimitePF )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         AV20WebSession.Set("Codigos", AV21SDT_Codigos.ToXml(false, true, "SDT_CodigosCollection", "GxEv3Up14_MeetrikaVs3"));
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21SDT_Codigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV20WebSession = context.GetSession();
         AV22Codigo = new SdtSDT_Codigos(context);
         AV13Codigos = new GxSimpleCollection();
         scmdbuf = "";
         P00W52_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00W52_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00W52_A456ContagemResultado_Codigo = new int[1] ;
         P00W52_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00W52_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         AV14Area_Codigos = new GxSimpleCollection();
         P00W53_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W53_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W53_A456ContagemResultado_Codigo = new int[1] ;
         P00W53_A1603ContagemResultado_CntCod = new int[1] ;
         P00W53_n1603ContagemResultado_CntCod = new bool[] {false} ;
         AV15Contrato_Codigos = new GxSimpleCollection();
         P00W54_A5AreaTrabalho_Codigo = new int[1] ;
         P00W54_A6AreaTrabalho_Descricao = new String[] {""} ;
         A6AreaTrabalho_Descricao = "";
         P00W55_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00W55_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00W55_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W55_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W55_A1603ContagemResultado_CntCod = new int[1] ;
         P00W55_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00W55_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00W55_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00W55_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00W55_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00W55_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00W55_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00W55_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00W55_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00W55_A456ContagemResultado_Codigo = new int[1] ;
         P00W55_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00W55_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00W55_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00W55_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00W55_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         P00W56_A5AreaTrabalho_Codigo = new int[1] ;
         P00W56_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00W57_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00W57_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00W57_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W57_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W57_A1603ContagemResultado_CntCod = new int[1] ;
         P00W57_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00W57_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00W57_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00W57_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00W57_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00W57_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00W57_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00W57_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00W57_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00W57_A456ContagemResultado_Codigo = new int[1] ;
         P00W57_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00W57_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00W57_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00W57_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00W57_A511ContagemResultado_HoraCnt = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_limitarifpug__default(),
            new Object[][] {
                new Object[] {
               P00W52_A490ContagemResultado_ContratadaCod, P00W52_n490ContagemResultado_ContratadaCod, P00W52_A456ContagemResultado_Codigo, P00W52_A52Contratada_AreaTrabalhoCod, P00W52_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P00W53_A1553ContagemResultado_CntSrvCod, P00W53_n1553ContagemResultado_CntSrvCod, P00W53_A456ContagemResultado_Codigo, P00W53_A1603ContagemResultado_CntCod, P00W53_n1603ContagemResultado_CntCod
               }
               , new Object[] {
               P00W54_A5AreaTrabalho_Codigo, P00W54_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00W55_A490ContagemResultado_ContratadaCod, P00W55_n490ContagemResultado_ContratadaCod, P00W55_A1553ContagemResultado_CntSrvCod, P00W55_n1553ContagemResultado_CntSrvCod, P00W55_A1603ContagemResultado_CntCod, P00W55_n1603ContagemResultado_CntCod, P00W55_A52Contratada_AreaTrabalhoCod, P00W55_n52Contratada_AreaTrabalhoCod, P00W55_A460ContagemResultado_PFBFM, P00W55_n460ContagemResultado_PFBFM,
               P00W55_A1326ContagemResultado_ContratadaTipoFab, P00W55_n1326ContagemResultado_ContratadaTipoFab, P00W55_A458ContagemResultado_PFBFS, P00W55_n458ContagemResultado_PFBFS, P00W55_A456ContagemResultado_Codigo, P00W55_A517ContagemResultado_Ultima, P00W55_A598ContagemResultado_Baseline, P00W55_n598ContagemResultado_Baseline, P00W55_A473ContagemResultado_DataCnt, P00W55_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P00W56_A5AreaTrabalho_Codigo, P00W56_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00W57_A490ContagemResultado_ContratadaCod, P00W57_n490ContagemResultado_ContratadaCod, P00W57_A1553ContagemResultado_CntSrvCod, P00W57_n1553ContagemResultado_CntSrvCod, P00W57_A1603ContagemResultado_CntCod, P00W57_n1603ContagemResultado_CntCod, P00W57_A52Contratada_AreaTrabalhoCod, P00W57_n52Contratada_AreaTrabalhoCod, P00W57_A460ContagemResultado_PFBFM, P00W57_n460ContagemResultado_PFBFM,
               P00W57_A1326ContagemResultado_ContratadaTipoFab, P00W57_n1326ContagemResultado_ContratadaTipoFab, P00W57_A458ContagemResultado_PFBFS, P00W57_n458ContagemResultado_PFBFS, P00W57_A456ContagemResultado_Codigo, P00W57_A517ContagemResultado_Ultima, P00W57_A598ContagemResultado_Baseline, P00W57_n598ContagemResultado_Baseline, P00W57_A473ContagemResultado_DataCnt, P00W57_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9LimiteBaselines ;
      private int AV25GXV1 ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A5AreaTrabalho_Codigo ;
      private int W52Contratada_AreaTrabalhoCod ;
      private decimal AV8LimitePF ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal AV18Total ;
      private String scmdbuf ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool A598ContagemResultado_Baseline ;
      private bool A517ContagemResultado_Ultima ;
      private bool BRKW55 ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n598ContagemResultado_Baseline ;
      private bool BRKW58 ;
      private String A6AreaTrabalho_Descricao ;
      private IGxSession AV20WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private decimal aP0_LimitePF ;
      private short aP1_LimiteBaselines ;
      private IDataStoreProvider pr_default ;
      private int[] P00W52_A490ContagemResultado_ContratadaCod ;
      private bool[] P00W52_n490ContagemResultado_ContratadaCod ;
      private int[] P00W52_A456ContagemResultado_Codigo ;
      private int[] P00W52_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00W52_n52Contratada_AreaTrabalhoCod ;
      private int[] P00W53_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W53_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W53_A456ContagemResultado_Codigo ;
      private int[] P00W53_A1603ContagemResultado_CntCod ;
      private bool[] P00W53_n1603ContagemResultado_CntCod ;
      private int[] P00W54_A5AreaTrabalho_Codigo ;
      private String[] P00W54_A6AreaTrabalho_Descricao ;
      private int[] P00W55_A490ContagemResultado_ContratadaCod ;
      private bool[] P00W55_n490ContagemResultado_ContratadaCod ;
      private int[] P00W55_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W55_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W55_A1603ContagemResultado_CntCod ;
      private bool[] P00W55_n1603ContagemResultado_CntCod ;
      private int[] P00W55_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00W55_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00W55_A460ContagemResultado_PFBFM ;
      private bool[] P00W55_n460ContagemResultado_PFBFM ;
      private String[] P00W55_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00W55_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00W55_A458ContagemResultado_PFBFS ;
      private bool[] P00W55_n458ContagemResultado_PFBFS ;
      private int[] P00W55_A456ContagemResultado_Codigo ;
      private bool[] P00W55_A517ContagemResultado_Ultima ;
      private bool[] P00W55_A598ContagemResultado_Baseline ;
      private bool[] P00W55_n598ContagemResultado_Baseline ;
      private DateTime[] P00W55_A473ContagemResultado_DataCnt ;
      private String[] P00W55_A511ContagemResultado_HoraCnt ;
      private int[] P00W56_A5AreaTrabalho_Codigo ;
      private String[] P00W56_A6AreaTrabalho_Descricao ;
      private int[] P00W57_A490ContagemResultado_ContratadaCod ;
      private bool[] P00W57_n490ContagemResultado_ContratadaCod ;
      private int[] P00W57_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W57_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W57_A1603ContagemResultado_CntCod ;
      private bool[] P00W57_n1603ContagemResultado_CntCod ;
      private int[] P00W57_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00W57_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00W57_A460ContagemResultado_PFBFM ;
      private bool[] P00W57_n460ContagemResultado_PFBFM ;
      private String[] P00W57_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00W57_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00W57_A458ContagemResultado_PFBFS ;
      private bool[] P00W57_n458ContagemResultado_PFBFS ;
      private int[] P00W57_A456ContagemResultado_Codigo ;
      private bool[] P00W57_A517ContagemResultado_Ultima ;
      private bool[] P00W57_A598ContagemResultado_Baseline ;
      private bool[] P00W57_n598ContagemResultado_Baseline ;
      private DateTime[] P00W57_A473ContagemResultado_DataCnt ;
      private String[] P00W57_A511ContagemResultado_HoraCnt ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV14Area_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV15Contrato_Codigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV21SDT_Codigos ;
      private SdtSDT_Codigos AV22Codigo ;
   }

   public class prc_limitarifpug__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00W52( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV13Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_ContratadaCod], NULL AS [ContagemResultado_Codigo], [Contratada_AreaTrabalhoCod] FROM ( SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      protected Object[] conditional_P00W53( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV13Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_CntSrvCod], NULL AS [ContagemResultado_Codigo], [ContagemResultado_CntCod] FROM ( SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00W54( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV14Area_Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV14Area_Codigos, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object5[0] = scmdbuf;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00W55( IGxContext context ,
                                             int A1603ContagemResultado_CntCod ,
                                             IGxCollection AV15Contrato_Codigos ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV13Codigos ,
                                             bool A598ContagemResultado_Baseline ,
                                             bool A517ContagemResultado_Ultima ,
                                             int A5AreaTrabalho_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [1] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_PFBFM], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_Baseline], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15Contrato_Codigos, "T4.[Contrato_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_Baseline] IS NULL or Not T2.[ContagemResultado_Baseline] = 1)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T4.[Contrato_Codigo], T1.[ContagemResultado_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00W56( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV14Area_Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV14Area_Codigos, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object9[0] = scmdbuf;
         return GXv_Object9 ;
      }

      protected Object[] conditional_P00W57( IGxContext context ,
                                             int A1603ContagemResultado_CntCod ,
                                             IGxCollection AV15Contrato_Codigos ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV13Codigos ,
                                             bool A598ContagemResultado_Baseline ,
                                             bool A517ContagemResultado_Ultima ,
                                             int A5AreaTrabalho_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [1] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_PFBFM], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_Baseline], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15Contrato_Codigos, "T4.[Contrato_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_Baseline] = 1)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T4.[Contrato_Codigo], T1.[ContagemResultado_Codigo]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00W52(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 1 :
                     return conditional_P00W53(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 2 :
                     return conditional_P00W54(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 3 :
                     return conditional_P00W55(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 4 :
                     return conditional_P00W56(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 5 :
                     return conditional_P00W57(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W52 ;
          prmP00W52 = new Object[] {
          } ;
          Object[] prmP00W53 ;
          prmP00W53 = new Object[] {
          } ;
          Object[] prmP00W54 ;
          prmP00W54 = new Object[] {
          } ;
          Object[] prmP00W55 ;
          prmP00W55 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W56 ;
          prmP00W56 = new Object[] {
          } ;
          Object[] prmP00W57 ;
          prmP00W57 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W52,100,0,false,false )
             ,new CursorDef("P00W53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W53,100,0,false,false )
             ,new CursorDef("P00W54", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W54,100,0,true,false )
             ,new CursorDef("P00W55", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W55,100,0,true,false )
             ,new CursorDef("P00W56", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W56,100,0,true,false )
             ,new CursorDef("P00W57", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W57,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.getBool(9) ;
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((String[]) buf[19])[0] = rslt.getString(12, 5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.getBool(9) ;
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((String[]) buf[19])[0] = rslt.getString(12, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
