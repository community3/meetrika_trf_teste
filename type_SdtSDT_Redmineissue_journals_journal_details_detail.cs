/*
               File: type_SdtSDT_Redmineissue_journals_journal_details_detail
        Description: SDT_Redmineissue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:5.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineissue.journals.journal.details.detail" )]
   [XmlType(TypeName =  "SDT_Redmineissue.journals.journal.details.detail" , Namespace = "" )]
   [Serializable]
   public class SdtSDT_Redmineissue_journals_journal_details_detail : GxUserType
   {
      public SdtSDT_Redmineissue_journals_journal_details_detail( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value = "";
      }

      public SdtSDT_Redmineissue_journals_journal_details_detail( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineissue_journals_journal_details_detail deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineissue_journals_journal_details_detail)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineissue_journals_journal_details_detail obj ;
         obj = this;
         obj.gxTpr_Property = deserialized.gxTpr_Property;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Old_value = deserialized.gxTpr_Old_value;
         obj.gxTpr_New_value = deserialized.gxTpr_New_value;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("property") == 1 )
         {
            gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property = oReader.GetAttributeByName("property");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("name") == 1 )
         {
            gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name = oReader.GetAttributeByName("name");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "old_value") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "new_value") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineissue.journals.journal.details.detail";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("property", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property));
         oWriter.WriteAttribute("name", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name));
         oWriter.WriteElement("old_value", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("new_value", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("property", gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property, false);
         AddObjectProperty("name", gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name, false);
         AddObjectProperty("old_value", gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value, false);
         AddObjectProperty("new_value", gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value, false);
         return  ;
      }

      [SoapAttribute( AttributeName = "property" )]
      [XmlAttribute( AttributeName = "property" )]
      public String gxTpr_Property
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property = (String)(value);
         }

      }

      [SoapAttribute( AttributeName = "name" )]
      [XmlAttribute( AttributeName = "name" )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "old_value" )]
      [  XmlElement( ElementName = "old_value" , Namespace = ""  )]
      public String gxTpr_Old_value
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value = (String)(value);
         }

      }

      [  SoapElement( ElementName = "new_value" )]
      [  XmlElement( ElementName = "new_value" , Namespace = ""  )]
      public String gxTpr_New_value
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Property ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Name ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_Old_value ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_details_detail_New_value ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_Redmineissue.journals.journal.details.detail", Namespace = "")]
   public class SdtSDT_Redmineissue_journals_journal_details_detail_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineissue_journals_journal_details_detail>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineissue_journals_journal_details_detail_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineissue_journals_journal_details_detail_RESTInterface( SdtSDT_Redmineissue_journals_journal_details_detail psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "property" , Order = 0 )]
      public String gxTpr_Property
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Property) ;
         }

         set {
            sdt.gxTpr_Property = (String)(value);
         }

      }

      [DataMember( Name = "name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "old_value" , Order = 2 )]
      public String gxTpr_Old_value
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Old_value) ;
         }

         set {
            sdt.gxTpr_Old_value = (String)(value);
         }

      }

      [DataMember( Name = "new_value" , Order = 3 )]
      public String gxTpr_New_value
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_New_value) ;
         }

         set {
            sdt.gxTpr_New_value = (String)(value);
         }

      }

      public SdtSDT_Redmineissue_journals_journal_details_detail sdt
      {
         get {
            return (SdtSDT_Redmineissue_journals_journal_details_detail)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineissue_journals_journal_details_detail() ;
         }
      }

   }

}
