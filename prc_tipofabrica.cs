/*
               File: PRC_TipoFabrica
        Description: Tipo de Fabrica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:59.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_tipofabrica : GXProcedure
   {
      public prc_tipofabrica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_tipofabrica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_Codigo ,
                           out String aP1_TipoFabrica )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8TipoFabrica = "" ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_TipoFabrica=this.AV8TipoFabrica;
      }

      public String executeUdp( ref int aP0_Contratada_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8TipoFabrica = "" ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_TipoFabrica=this.AV8TipoFabrica;
         return AV8TipoFabrica ;
      }

      public void executeSubmit( ref int aP0_Contratada_Codigo ,
                                 out String aP1_TipoFabrica )
      {
         prc_tipofabrica objprc_tipofabrica;
         objprc_tipofabrica = new prc_tipofabrica();
         objprc_tipofabrica.A39Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_tipofabrica.AV8TipoFabrica = "" ;
         objprc_tipofabrica.context.SetSubmitInitialConfig(context);
         objprc_tipofabrica.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_tipofabrica);
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_TipoFabrica=this.AV8TipoFabrica;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_tipofabrica)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DC2 */
         pr_default.execute(0, new Object[] {A39Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A516Contratada_TipoFabrica = P00DC2_A516Contratada_TipoFabrica[0];
            OV8TipoFabrica = AV8TipoFabrica;
            AV8TipoFabrica = A516Contratada_TipoFabrica;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DC2_A39Contratada_Codigo = new int[1] ;
         P00DC2_A516Contratada_TipoFabrica = new String[] {""} ;
         A516Contratada_TipoFabrica = "";
         OV8TipoFabrica = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_tipofabrica__default(),
            new Object[][] {
                new Object[] {
               P00DC2_A39Contratada_Codigo, P00DC2_A516Contratada_TipoFabrica
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A39Contratada_Codigo ;
      private String AV8TipoFabrica ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private String OV8TipoFabrica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00DC2_A39Contratada_Codigo ;
      private String[] P00DC2_A516Contratada_TipoFabrica ;
      private String aP1_TipoFabrica ;
   }

   public class prc_tipofabrica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DC2 ;
          prmP00DC2 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DC2", "SELECT TOP 1 [Contratada_Codigo], [Contratada_TipoFabrica] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DC2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
