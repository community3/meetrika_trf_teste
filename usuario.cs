/*
               File: Usuario
        Description: Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:18.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvUNIDADEORGANIZACIONAL_CODIGO011( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel4"+"_"+"USUARIO_ENTIDADE") == 0 )
         {
            A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1Usuario_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX4ASAUSUARIO_ENTIDADE011( A1Usuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"USUARIO_EHAUDITORFM") == 0 )
         {
            A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1Usuario_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASAUSUARIO_EHAUDITORFM011( A1Usuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_59") == 0 )
         {
            A57Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_59( A57Usuario_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_60") == 0 )
         {
            A1073Usuario_CargoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1073Usuario_CargoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_60( A1073Usuario_CargoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_61") == 0 )
         {
            A1075Usuario_CargoUOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1075Usuario_CargoUOCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1075Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_61( A1075Usuario_CargoUOCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynavUnidadeorganizacional_codigo.Name = "vUNIDADEORGANIZACIONAL_CODIGO";
         dynavUnidadeorganizacional_codigo.WebTags = "";
         cmbUsuario_PessoaTip.Name = "USUARIO_PESSOATIP";
         cmbUsuario_PessoaTip.WebTags = "";
         cmbUsuario_PessoaTip.addItem("", "(Nenhum)", 0);
         cmbUsuario_PessoaTip.addItem("F", "F�sica", 0);
         cmbUsuario_PessoaTip.addItem("J", "Jur�dica", 0);
         if ( cmbUsuario_PessoaTip.ItemCount > 0 )
         {
            A59Usuario_PessoaTip = cmbUsuario_PessoaTip.getValidValue(A59Usuario_PessoaTip);
            n59Usuario_PessoaTip = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
         }
         chkavContratanteusuario_ehfiscal.Name = "vCONTRATANTEUSUARIO_EHFISCAL";
         chkavContratanteusuario_ehfiscal.WebTags = "";
         chkavContratanteusuario_ehfiscal.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratanteusuario_ehfiscal_Internalname, "TitleCaption", chkavContratanteusuario_ehfiscal.Caption);
         chkavContratanteusuario_ehfiscal.CheckedValue = "false";
         chkUsuario_EhContador.Name = "USUARIO_EHCONTADOR";
         chkUsuario_EhContador.WebTags = "";
         chkUsuario_EhContador.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContador_Internalname, "TitleCaption", chkUsuario_EhContador.Caption);
         chkUsuario_EhContador.CheckedValue = "false";
         chkUsuario_EhAuditorFM.Name = "USUARIO_EHAUDITORFM";
         chkUsuario_EhAuditorFM.WebTags = "";
         chkUsuario_EhAuditorFM.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhAuditorFM_Internalname, "TitleCaption", chkUsuario_EhAuditorFM.Caption);
         chkUsuario_EhAuditorFM.CheckedValue = "false";
         chkUsuario_EhFinanceiro.Name = "USUARIO_EHFINANCEIRO";
         chkUsuario_EhFinanceiro.WebTags = "";
         chkUsuario_EhFinanceiro.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhFinanceiro_Internalname, "TitleCaption", chkUsuario_EhFinanceiro.Caption);
         chkUsuario_EhFinanceiro.CheckedValue = "false";
         chkUsuario_EhContratada.Name = "USUARIO_EHCONTRATADA";
         chkUsuario_EhContratada.WebTags = "";
         chkUsuario_EhContratada.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratada_Internalname, "TitleCaption", chkUsuario_EhContratada.Caption);
         chkUsuario_EhContratada.CheckedValue = "false";
         chkUsuario_EhContratante.Name = "USUARIO_EHCONTRATANTE";
         chkUsuario_EhContratante.WebTags = "";
         chkUsuario_EhContratante.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratante_Internalname, "TitleCaption", chkUsuario_EhContratante.Caption);
         chkUsuario_EhContratante.CheckedValue = "false";
         chkUsuario_EhPreposto.Name = "USUARIO_EHPREPOSTO";
         chkUsuario_EhPreposto.WebTags = "";
         chkUsuario_EhPreposto.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhPreposto_Internalname, "TitleCaption", chkUsuario_EhPreposto.Caption);
         chkUsuario_EhPreposto.CheckedValue = "false";
         cmbUsuario_Notificar.Name = "USUARIO_NOTIFICAR";
         cmbUsuario_Notificar.WebTags = "";
         cmbUsuario_Notificar.addItem("", "Nunca", 0);
         cmbUsuario_Notificar.addItem("A", "Em Solicitada e Resolvida", 0);
         cmbUsuario_Notificar.addItem("D", "Em Diverg�ncia, Resolvida e Rejeitada", 0);
         cmbUsuario_Notificar.addItem("C", "Em cada Etapa da OS", 0);
         if ( cmbUsuario_Notificar.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1235Usuario_Notificar)) )
            {
               A1235Usuario_Notificar = "A";
               n1235Usuario_Notificar = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
            }
            A1235Usuario_Notificar = cmbUsuario_Notificar.getValidValue(A1235Usuario_Notificar);
            n1235Usuario_Notificar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
         }
         chkUsuario_DeFerias.Name = "USUARIO_DEFERIAS";
         chkUsuario_DeFerias.WebTags = "";
         chkUsuario_DeFerias.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_DeFerias_Internalname, "TitleCaption", chkUsuario_DeFerias.Caption);
         chkUsuario_DeFerias.CheckedValue = "false";
         chkUsuario_Ativo.Name = "USUARIO_ATIVO";
         chkUsuario_Ativo.WebTags = "";
         chkUsuario_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "TitleCaption", chkUsuario_Ativo.Caption);
         chkUsuario_Ativo.CheckedValue = "false";
         chkavEhcontratada_flag.Name = "vEHCONTRATADA_FLAG";
         chkavEhcontratada_flag.WebTags = "";
         chkavEhcontratada_flag.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEhcontratada_flag_Internalname, "TitleCaption", chkavEhcontratada_flag.Caption);
         chkavEhcontratada_flag.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public usuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Usuario_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Usuario_Codigo = aP1_Usuario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavUnidadeorganizacional_codigo = new GXCombobox();
         cmbUsuario_PessoaTip = new GXCombobox();
         chkavContratanteusuario_ehfiscal = new GXCheckbox();
         chkUsuario_EhContador = new GXCheckbox();
         chkUsuario_EhAuditorFM = new GXCheckbox();
         chkUsuario_EhFinanceiro = new GXCheckbox();
         chkUsuario_EhContratada = new GXCheckbox();
         chkUsuario_EhContratante = new GXCheckbox();
         chkUsuario_EhPreposto = new GXCheckbox();
         cmbUsuario_Notificar = new GXCombobox();
         chkUsuario_DeFerias = new GXCheckbox();
         chkUsuario_Ativo = new GXCheckbox();
         chkavEhcontratada_flag = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavUnidadeorganizacional_codigo.ItemCount > 0 )
         {
            AV26UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( dynavUnidadeorganizacional_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)));
         }
         if ( cmbUsuario_PessoaTip.ItemCount > 0 )
         {
            A59Usuario_PessoaTip = cmbUsuario_PessoaTip.getValidValue(A59Usuario_PessoaTip);
            n59Usuario_PessoaTip = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
         }
         if ( cmbUsuario_Notificar.ItemCount > 0 )
         {
            A1235Usuario_Notificar = cmbUsuario_Notificar.getValidValue(A1235Usuario_Notificar);
            n1235Usuario_Notificar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_011( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_011e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavEhcontratada_flag_Internalname, StringUtil.BoolToStr( AV23EhContratada_Flag), "", "", chkavEhcontratada_flag.Visible, chkavEhcontratada_flag.Enabled, "true", "", StyleString, ClassString, "", "");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavEntidade_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Entidade_Codigo), 6, 0, ",", "")), ((edtavEntidade_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24Entidade_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV24Entidade_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEntidade_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavEntidade_codigo_Visible, edtavEntidade_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Usuario.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")), ((edtUsuario_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_Codigo_Visible, edtUsuario_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Usuario.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")), ((edtUsuario_PessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_PessoaCod_Visible, edtUsuario_PessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Usuario.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableTitleCell'>") ;
            wb_table2_5_011( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_011e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_10_011( true) ;
         }
         return  ;
      }

      protected void wb_table3_10_011e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_150_011( true) ;
         }
         return  ;
      }

      protected void wb_table4_150_011e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_011e( true) ;
         }
         else
         {
            wb_table1_2_011e( false) ;
         }
      }

      protected void wb_table4_150_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_150_011e( true) ;
         }
         else
         {
            wb_table4_150_011e( false) ;
         }
      }

      protected void wb_table3_10_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_18_011( true) ;
         }
         return  ;
      }

      protected void wb_table5_18_011e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_10_011e( true) ;
         }
         else
         {
            wb_table3_10_011e( false) ;
         }
      }

      protected void wb_table5_18_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidadeorganizacional_codigo_Internalname, "Unidade Organizacional", "", "", lblTextblockunidadeorganizacional_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_23_011( true) ;
         }
         return  ;
      }

      protected void wb_table6_23_011e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_cargonom_Internalname, "Cargo", "", "", lblTextblockusuario_cargonom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_CargoNom_Internalname, A1074Usuario_CargoNom, StringUtil.RTrim( context.localUtil.Format( A1074Usuario_CargoNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_CargoNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_CargoNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_entidade_Internalname, "Entidade", "", "", lblTextblockusuario_entidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Entidade_Internalname, StringUtil.RTrim( A1083Usuario_Entidade), StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Entidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Entidade_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Nome", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoatip_Internalname, "Tipo", "", "", lblTextblockusuario_pessoatip_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbUsuario_PessoaTip, cmbUsuario_PessoaTip_Internalname, StringUtil.RTrim( A59Usuario_PessoaTip), 1, cmbUsuario_PessoaTip_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbUsuario_PessoaTip.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Usuario.htm");
            cmbUsuario_PessoaTip.CurrentValue = StringUtil.RTrim( A59Usuario_PessoaTip);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuario_PessoaTip_Internalname, "Values", (String)(cmbUsuario_PessoaTip.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoadoc_Internalname, "Documento(CPF)", "", "", lblTextblockusuario_pessoadoc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaDoc_Internalname, A325Usuario_PessoaDoc, StringUtil.RTrim( context.localUtil.Format( A325Usuario_PessoaDoc, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaDoc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_PessoaDoc_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_Internalname, "E-mail", "", "", lblTextblockemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_Internalname, AV18Email, StringUtil.RTrim( context.localUtil.Format( AV18Email, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavEmail_Enabled, 0, "text", "", 300, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratanteusuario_ehfiscal_cell_Internalname+"\"  class='"+cellTextblockcontratanteusuario_ehfiscal_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_ehfiscal_Internalname, "Fiscal?", "", "", lblTextblockcontratanteusuario_ehfiscal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratanteusuario_ehfiscal_cell_Internalname+"\"  class='"+cellContratanteusuario_ehfiscal_cell_Class+"'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavContratanteusuario_ehfiscal_Internalname, StringUtil.BoolToStr( AV29ContratanteUsuario_EhFiscal), "", "", chkavContratanteusuario_ehfiscal.Visible, chkavContratanteusuario_ehfiscal.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(61, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname+"\"  class='"+cellTextblockcontratadausuario_cstuntprdnrm_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_cstuntprdnrm_Internalname, "Custo Unit. Prod. Normal", "", "", lblTextblockcontratadausuario_cstuntprdnrm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratadausuario_cstuntprdnrm_cell_Internalname+"\"  class='"+cellContratadausuario_cstuntprdnrm_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_cstuntprdnrm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV20ContratadaUsuario_CstUntPrdNrm, 18, 5, ",", "")), ((edtavContratadausuario_cstuntprdnrm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV20ContratadaUsuario_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_cstuntprdnrm_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_cstuntprdnrm_Visible, edtavContratadausuario_cstuntprdnrm_Enabled, 0, "text", "", 55, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratadausuario_cstuntprdext_cell_Internalname+"\"  class='"+cellTextblockcontratadausuario_cstuntprdext_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadausuario_cstuntprdext_Internalname, "Custo Unit. Prod. Extra", "", "", lblTextblockcontratadausuario_cstuntprdext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratadausuario_cstuntprdext_cell_Internalname+"\"  class='"+cellContratadausuario_cstuntprdext_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_cstuntprdext_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21ContratadaUsuario_CstUntPrdExt, 18, 5, ",", "")), ((edtavContratadausuario_cstuntprdext_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV21ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV21ContratadaUsuario_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_cstuntprdext_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_cstuntprdext_Visible, edtavContratadausuario_cstuntprdext_Enabled, 0, "text", "", 55, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_usergamguid_cell_Internalname+"\"  class='"+cellTextblockusuario_usergamguid_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_usergamguid_Internalname, "Guid", "", "", lblTextblockusuario_usergamguid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_usergamguid_cell_Internalname+"\"  class='"+cellUsuario_usergamguid_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_UserGamGuid_Internalname, StringUtil.RTrim( A341Usuario_UserGamGuid), StringUtil.RTrim( context.localUtil.Format( A341Usuario_UserGamGuid, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_UserGamGuid_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtUsuario_UserGamGuid_Visible, edtUsuario_UserGamGuid_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_ehcontador_cell_Internalname+"\"  class='"+cellTextblockusuario_ehcontador_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehcontador_Internalname, "Contador?", "", "", lblTextblockusuario_ehcontador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_ehcontador_cell_Internalname+"\"  class='"+cellUsuario_ehcontador_cell_Class+"'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhContador_Internalname, StringUtil.BoolToStr( A289Usuario_EhContador), "", "", chkUsuario_EhContador.Visible, chkUsuario_EhContador.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_ehauditorfm_cell_Internalname+"\"  class='"+cellTextblockusuario_ehauditorfm_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehauditorfm_Internalname, "Auditor FM?", "", "", lblTextblockusuario_ehauditorfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_ehauditorfm_cell_Internalname+"\"  class='"+cellUsuario_ehauditorfm_cell_Class+"'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhAuditorFM_Internalname, StringUtil.BoolToStr( A290Usuario_EhAuditorFM), "", "", chkUsuario_EhAuditorFM.Visible, chkUsuario_EhAuditorFM.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_ehfinanceiro_cell_Internalname+"\"  class='"+cellTextblockusuario_ehfinanceiro_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehfinanceiro_Internalname, "Financeiro?", "", "", lblTextblockusuario_ehfinanceiro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_ehfinanceiro_cell_Internalname+"\"  class='"+cellUsuario_ehfinanceiro_cell_Class+"'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhFinanceiro_Internalname, StringUtil.BoolToStr( A293Usuario_EhFinanceiro), "", "", chkUsuario_EhFinanceiro.Visible, chkUsuario_EhFinanceiro.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(89, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_ehcontratada_cell_Internalname+"\"  class='"+cellTextblockusuario_ehcontratada_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehcontratada_Internalname, "Contratada?", "", "", lblTextblockusuario_ehcontratada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_ehcontratada_cell_Internalname+"\"  class='"+cellUsuario_ehcontratada_cell_Class+"'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhContratada_Internalname, StringUtil.BoolToStr( A291Usuario_EhContratada), "", "", chkUsuario_EhContratada.Visible, chkUsuario_EhContratada.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(94, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_ehcontratante_cell_Internalname+"\"  class='"+cellTextblockusuario_ehcontratante_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehcontratante_Internalname, "Contratante?", "", "", lblTextblockusuario_ehcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_ehcontratante_cell_Internalname+"\"  class='"+cellUsuario_ehcontratante_cell_Class+"'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhContratante_Internalname, StringUtil.BoolToStr( A292Usuario_EhContratante), "", "", chkUsuario_EhContratante.Visible, chkUsuario_EhContratante.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockusuario_ehpreposto_cell_Internalname+"\"  class='"+cellTextblockusuario_ehpreposto_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ehpreposto_Internalname, "Preposto?", "", "", lblTextblockusuario_ehpreposto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellUsuario_ehpreposto_cell_Internalname+"\"  class='"+cellUsuario_ehpreposto_cell_Class+"'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_EhPreposto_Internalname, StringUtil.BoolToStr( A1093Usuario_EhPreposto), "", "", chkUsuario_EhPreposto.Visible, chkUsuario_EhPreposto.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_notificar_Internalname, "Notifica��es (inativo)", "", "", lblTextblockusuario_notificar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbUsuario_Notificar, cmbUsuario_Notificar_Internalname, StringUtil.RTrim( A1235Usuario_Notificar), 1, cmbUsuario_Notificar_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbUsuario_Notificar.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "", true, "HLP_Usuario.htm");
            cmbUsuario_Notificar.CurrentValue = StringUtil.RTrim( A1235Usuario_Notificar);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuario_Notificar_Internalname, "Values", (String)(cmbUsuario_Notificar.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_email_Internalname, "E-mail do Usu�rio", "", "", lblTextblockusuario_email_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_Email_Internalname, A1647Usuario_Email, StringUtil.RTrim( context.localUtil.Format( A1647Usuario_Email, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "mailto:"+A1647Usuario_Email, "", "", "", edtUsuario_Email_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Email_Enabled, 0, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "Email", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoatelefone_Internalname, "Telefone", "", "", lblTextblockusuario_pessoatelefone_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaTelefone_Internalname, StringUtil.RTrim( A2095Usuario_PessoaTelefone), StringUtil.RTrim( context.localUtil.Format( A2095Usuario_PessoaTelefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaTelefone_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_PessoaTelefone_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_foto_Internalname, "Foto", "", "", lblTextblockusuario_foto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCell'>") ;
            /* Static Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            A1716Usuario_Foto_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Usuario_Foto_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)));
            GxWebStd.gx_bitmap( context, imgUsuario_Foto_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.PathToRelativeUrl( A1716Usuario_Foto)), "", "", "", context.GetTheme( ), 1, imgUsuario_Foto_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", "", "", "", 0, A1716Usuario_Foto_IsBlob, true, "HLP_Usuario.htm");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.PathToRelativeUrl( A1716Usuario_Foto)));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "IsBlob", StringUtil.BoolToStr( A1716Usuario_Foto_IsBlob));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_crtfpath_Internalname, "Certificado Digital", "", "", lblTextblockusuario_crtfpath_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_CrtfPath_Internalname, A1017Usuario_CrtfPath, StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "(path + file name)", edtUsuario_CrtfPath_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_CrtfPath_Enabled, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_deferias_Internalname, "De f�rias", "", "", lblTextblockusuario_deferias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_DeFerias_Internalname, StringUtil.BoolToStr( A1908Usuario_DeFerias), "", "", 1, chkUsuario_DeFerias.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(128, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_ativo_Internalname, "Ativo?", "", "", lblTextblockusuario_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockusuario_ativo_Visible, 1, 0, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUsuario_Ativo_Internalname, StringUtil.BoolToStr( A54Usuario_Ativo), "", "", chkUsuario_Ativo.Visible, chkUsuario_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(143, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_18_011e( true) ;
         }
         else
         {
            wb_table5_18_011e( false) ;
         }
      }

      protected void wb_table6_23_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedunidadeorganizacional_codigo_Internalname, tblTablemergedunidadeorganizacional_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_codigo, dynavUnidadeorganizacional_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)), 1, dynavUnidadeorganizacional_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavUnidadeorganizacional_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_Usuario.htm");
            dynavUnidadeorganizacional_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_codigo_Internalname, "Values", (String)(dynavUnidadeorganizacional_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgSelectuo_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgSelectuo_Visible, 1, "", "Selecionar Unidade Organizacional", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgSelectuo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e11011_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Usuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_011e( true) ;
         }
         else
         {
            wb_table6_23_011e( false) ;
         }
      }

      protected void wb_table2_5_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletitle_Internalname, tblTabletitle_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_011e( true) ;
         }
         else
         {
            wb_table2_5_011e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12012 */
         E12012 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynavUnidadeorganizacional_codigo.CurrentValue = cgiGet( dynavUnidadeorganizacional_codigo_Internalname);
               AV26UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)));
               A1074Usuario_CargoNom = StringUtil.Upper( cgiGet( edtUsuario_CargoNom_Internalname));
               n1074Usuario_CargoNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
               A1083Usuario_Entidade = StringUtil.Upper( cgiGet( edtUsuario_Entidade_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
               A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               cmbUsuario_PessoaTip.CurrentValue = cgiGet( cmbUsuario_PessoaTip_Internalname);
               A59Usuario_PessoaTip = cgiGet( cmbUsuario_PessoaTip_Internalname);
               n59Usuario_PessoaTip = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
               A325Usuario_PessoaDoc = cgiGet( edtUsuario_PessoaDoc_Internalname);
               n325Usuario_PessoaDoc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
               AV18Email = StringUtil.Upper( cgiGet( edtavEmail_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Email", AV18Email);
               AV29ContratanteUsuario_EhFiscal = StringUtil.StrToBool( cgiGet( chkavContratanteusuario_ehfiscal_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratanteUsuario_EhFiscal", AV29ContratanteUsuario_EhFiscal);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdnrm_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdnrm_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADAUSUARIO_CSTUNTPRDNRM");
                  AnyError = 1;
                  GX_FocusControl = edtavContratadausuario_cstuntprdnrm_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV20ContratadaUsuario_CstUntPrdNrm = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               }
               else
               {
                  AV20ContratadaUsuario_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdnrm_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdext_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdext_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADAUSUARIO_CSTUNTPRDEXT");
                  AnyError = 1;
                  GX_FocusControl = edtavContratadausuario_cstuntprdext_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV21ContratadaUsuario_CstUntPrdExt = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV21ContratadaUsuario_CstUntPrdExt, 18, 5)));
               }
               else
               {
                  AV21ContratadaUsuario_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtavContratadausuario_cstuntprdext_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV21ContratadaUsuario_CstUntPrdExt, 18, 5)));
               }
               A341Usuario_UserGamGuid = cgiGet( edtUsuario_UserGamGuid_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               A289Usuario_EhContador = StringUtil.StrToBool( cgiGet( chkUsuario_EhContador_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A289Usuario_EhContador", A289Usuario_EhContador);
               A290Usuario_EhAuditorFM = StringUtil.StrToBool( cgiGet( chkUsuario_EhAuditorFM_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
               A293Usuario_EhFinanceiro = StringUtil.StrToBool( cgiGet( chkUsuario_EhFinanceiro_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A293Usuario_EhFinanceiro", A293Usuario_EhFinanceiro);
               A291Usuario_EhContratada = StringUtil.StrToBool( cgiGet( chkUsuario_EhContratada_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A291Usuario_EhContratada", A291Usuario_EhContratada);
               A292Usuario_EhContratante = StringUtil.StrToBool( cgiGet( chkUsuario_EhContratante_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A292Usuario_EhContratante", A292Usuario_EhContratante);
               A1093Usuario_EhPreposto = StringUtil.StrToBool( cgiGet( chkUsuario_EhPreposto_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1093Usuario_EhPreposto", A1093Usuario_EhPreposto);
               cmbUsuario_Notificar.CurrentValue = cgiGet( cmbUsuario_Notificar_Internalname);
               A1235Usuario_Notificar = cgiGet( cmbUsuario_Notificar_Internalname);
               n1235Usuario_Notificar = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
               n1235Usuario_Notificar = (String.IsNullOrEmpty(StringUtil.RTrim( A1235Usuario_Notificar)) ? true : false);
               A1647Usuario_Email = cgiGet( edtUsuario_Email_Internalname);
               n1647Usuario_Email = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1647Usuario_Email", A1647Usuario_Email);
               n1647Usuario_Email = (String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) ? true : false);
               A2095Usuario_PessoaTelefone = cgiGet( edtUsuario_PessoaTelefone_Internalname);
               n2095Usuario_PessoaTelefone = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2095Usuario_PessoaTelefone", A2095Usuario_PessoaTelefone);
               n2095Usuario_PessoaTelefone = (String.IsNullOrEmpty(StringUtil.RTrim( A2095Usuario_PessoaTelefone)) ? true : false);
               A1716Usuario_Foto = cgiGet( imgUsuario_Foto_Internalname);
               n1716Usuario_Foto = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1716Usuario_Foto", A1716Usuario_Foto);
               n1716Usuario_Foto = (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? true : false);
               A1017Usuario_CrtfPath = cgiGet( edtUsuario_CrtfPath_Internalname);
               n1017Usuario_CrtfPath = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1017Usuario_CrtfPath", A1017Usuario_CrtfPath);
               n1017Usuario_CrtfPath = (String.IsNullOrEmpty(StringUtil.RTrim( A1017Usuario_CrtfPath)) ? true : false);
               A1908Usuario_DeFerias = StringUtil.StrToBool( cgiGet( chkUsuario_DeFerias_Internalname));
               n1908Usuario_DeFerias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
               n1908Usuario_DeFerias = ((false==A1908Usuario_DeFerias) ? true : false);
               A54Usuario_Ativo = StringUtil.StrToBool( cgiGet( chkUsuario_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
               AV23EhContratada_Flag = StringUtil.StrToBool( cgiGet( chkavEhcontratada_flag_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23EhContratada_Flag", AV23EhContratada_Flag);
               AV24Entidade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavEntidade_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Entidade_Codigo), 6, 0)));
               A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
               n1Usuario_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
               /* Read saved values. */
               Z1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1Usuario_Codigo"), ",", "."));
               Z341Usuario_UserGamGuid = cgiGet( "Z341Usuario_UserGamGuid");
               Z2Usuario_Nome = cgiGet( "Z2Usuario_Nome");
               n2Usuario_Nome = (String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) ? true : false);
               Z289Usuario_EhContador = StringUtil.StrToBool( cgiGet( "Z289Usuario_EhContador"));
               Z291Usuario_EhContratada = StringUtil.StrToBool( cgiGet( "Z291Usuario_EhContratada"));
               Z292Usuario_EhContratante = StringUtil.StrToBool( cgiGet( "Z292Usuario_EhContratante"));
               Z293Usuario_EhFinanceiro = StringUtil.StrToBool( cgiGet( "Z293Usuario_EhFinanceiro"));
               Z538Usuario_EhGestor = StringUtil.StrToBool( cgiGet( "Z538Usuario_EhGestor"));
               Z1093Usuario_EhPreposto = StringUtil.StrToBool( cgiGet( "Z1093Usuario_EhPreposto"));
               Z1017Usuario_CrtfPath = cgiGet( "Z1017Usuario_CrtfPath");
               n1017Usuario_CrtfPath = (String.IsNullOrEmpty(StringUtil.RTrim( A1017Usuario_CrtfPath)) ? true : false);
               Z1235Usuario_Notificar = cgiGet( "Z1235Usuario_Notificar");
               n1235Usuario_Notificar = (String.IsNullOrEmpty(StringUtil.RTrim( A1235Usuario_Notificar)) ? true : false);
               Z54Usuario_Ativo = StringUtil.StrToBool( cgiGet( "Z54Usuario_Ativo"));
               Z1647Usuario_Email = cgiGet( "Z1647Usuario_Email");
               n1647Usuario_Email = (String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) ? true : false);
               Z1908Usuario_DeFerias = StringUtil.StrToBool( cgiGet( "Z1908Usuario_DeFerias"));
               n1908Usuario_DeFerias = ((false==A1908Usuario_DeFerias) ? true : false);
               Z2016Usuario_UltimaArea = (int)(context.localUtil.CToN( cgiGet( "Z2016Usuario_UltimaArea"), ",", "."));
               n2016Usuario_UltimaArea = ((0==A2016Usuario_UltimaArea) ? true : false);
               Z57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "Z57Usuario_PessoaCod"), ",", "."));
               Z1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( "Z1073Usuario_CargoCod"), ",", "."));
               n1073Usuario_CargoCod = ((0==A1073Usuario_CargoCod) ? true : false);
               Z58Usuario_PessoaNom = cgiGet( "Z58Usuario_PessoaNom");
               Z59Usuario_PessoaTip = cgiGet( "Z59Usuario_PessoaTip");
               Z325Usuario_PessoaDoc = cgiGet( "Z325Usuario_PessoaDoc");
               Z2095Usuario_PessoaTelefone = cgiGet( "Z2095Usuario_PessoaTelefone");
               n2095Usuario_PessoaTelefone = (String.IsNullOrEmpty(StringUtil.RTrim( A2095Usuario_PessoaTelefone)) ? true : false);
               A2Usuario_Nome = cgiGet( "Z2Usuario_Nome");
               n2Usuario_Nome = false;
               n2Usuario_Nome = (String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) ? true : false);
               A538Usuario_EhGestor = StringUtil.StrToBool( cgiGet( "Z538Usuario_EhGestor"));
               A2016Usuario_UltimaArea = (int)(context.localUtil.CToN( cgiGet( "Z2016Usuario_UltimaArea"), ",", "."));
               n2016Usuario_UltimaArea = false;
               n2016Usuario_UltimaArea = ((0==A2016Usuario_UltimaArea) ? true : false);
               A1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( "Z1073Usuario_CargoCod"), ",", "."));
               n1073Usuario_CargoCod = false;
               n1073Usuario_CargoCod = ((0==A1073Usuario_CargoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( "N1073Usuario_CargoCod"), ",", "."));
               n1073Usuario_CargoCod = ((0==A1073Usuario_CargoCod) ? true : false);
               AV7Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "vUSUARIO_CODIGO"), ",", "."));
               AV25Insert_Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_USUARIO_CARGOCOD"), ",", "."));
               A1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( "USUARIO_CARGOCOD"), ",", "."));
               n1073Usuario_CargoCod = ((0==A1073Usuario_CargoCod) ? true : false);
               AV14Insert_Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_USUARIO_PESSOACOD"), ",", "."));
               A1075Usuario_CargoUOCod = (int)(context.localUtil.CToN( cgiGet( "USUARIO_CARGOUOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A538Usuario_EhGestor = StringUtil.StrToBool( cgiGet( "USUARIO_EHGESTOR"));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV28AuditingObject);
               A2Usuario_Nome = cgiGet( "USUARIO_NOME");
               n2Usuario_Nome = (String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) ? true : false);
               A40000Usuario_Foto_GXI = cgiGet( "USUARIO_FOTO_GXI");
               n40000Usuario_Foto_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000Usuario_Foto_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? true : false);
               A2016Usuario_UltimaArea = (int)(context.localUtil.CToN( cgiGet( "USUARIO_ULTIMAAREA"), ",", "."));
               n2016Usuario_UltimaArea = ((0==A2016Usuario_UltimaArea) ? true : false);
               A1076Usuario_CargoUONom = cgiGet( "USUARIO_CARGOUONOM");
               n1076Usuario_CargoUONom = false;
               AV32Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               getMultimediaValue(imgUsuario_Foto_Internalname, ref  A1716Usuario_Foto, ref  A40000Usuario_Foto_GXI);
               n40000Usuario_Foto_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000Usuario_Foto_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? true : false);
               n1716Usuario_Foto = (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? true : false);
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Usuario";
               A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
               n1Usuario_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9");
               A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9");
               AV23EhContratada_Flag = StringUtil.StrToBool( cgiGet( chkavEhcontratada_flag_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23EhContratada_Flag", AV23EhContratada_Flag);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( AV23EhContratada_Flag);
               AV24Entidade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavEntidade_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Entidade_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV24Entidade_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV32Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A538Usuario_EhGestor);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2016Usuario_UltimaArea), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuario_Codigo:"+context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuario_PessoaCod:"+context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"EhContratada_Flag:"+StringUtil.BoolToStr( AV23EhContratada_Flag));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Entidade_Codigo:"+context.localUtil.Format( (decimal)(AV24Entidade_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuario_CargoCod:"+context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV32Pgmname, "")));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuario_Nome:"+StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuario_EhGestor:"+StringUtil.BoolToStr( A538Usuario_EhGestor));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuario_UltimaArea:"+context.localUtil.Format( (decimal)(A2016Usuario_UltimaArea), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1Usuario_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode1 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode1;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound1 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_010( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "USUARIO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtUsuario_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12012 */
                           E12012 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13012 */
                           E13012 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E13012 */
            E13012 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll011( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes011( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratanteusuario_ehfiscal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdnrm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdext_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEhcontratada_flag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavEhcontratada_flag.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEntidade_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_codigo_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_010( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls011( ) ;
            }
            else
            {
               CheckExtendedTable011( ) ;
               CloseExtendedTableCursors011( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption010( )
      {
      }

      protected void E12012( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV32Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV33GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GXV1), 8, 0)));
            while ( AV33GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV33GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Usuario_CargoCod") == 0 )
               {
                  AV25Insert_Usuario_CargoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Insert_Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Insert_Usuario_CargoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Usuario_PessoaCod") == 0 )
               {
                  AV14Insert_Usuario_PessoaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_Usuario_PessoaCod), 6, 0)));
               }
               AV33GXV1 = (int)(AV33GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GXV1), 8, 0)));
            }
         }
         chkavEhcontratada_flag.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEhcontratada_flag_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavEhcontratada_flag.Visible), 5, 0)));
         edtavEntidade_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEntidade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_codigo_Visible), 5, 0)));
         edtUsuario_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Visible), 5, 0)));
         edtUsuario_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Visible), 5, 0)));
         GXt_char1 = AV27Usuario_UserGamGuid;
         new prc_getusergamguid(context ).execute(  AV7Usuario_Codigo, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
         AV27Usuario_UserGamGuid = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Usuario_UserGamGuid", AV27Usuario_UserGamGuid);
         AV15GamUser.load( AV27Usuario_UserGamGuid);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Usuario_UserGamGuid", AV27Usuario_UserGamGuid);
         Gx_msg = AV10WebSession.Get("Entidade");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
         AV23EhContratada_Flag = (bool)(((StringUtil.StrCmp(StringUtil.Substring( Gx_msg, 1, 1), "A")==0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23EhContratada_Flag", AV23EhContratada_Flag);
         AV24Entidade_Codigo = (int)(NumberUtil.Val( StringUtil.Substring( Gx_msg, 2, 20), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Entidade_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV18Email = AV15GamUser.gxTpr_Email;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Email", AV18Email);
            if ( AV23EhContratada_Flag )
            {
               AV22ContratadaUsuario.Load(AV24Entidade_Codigo, AV7Usuario_Codigo);
               AV20ContratadaUsuario_CstUntPrdNrm = AV22ContratadaUsuario.gxTpr_Contratadausuario_cstuntprdnrm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               AV21ContratadaUsuario_CstUntPrdExt = AV22ContratadaUsuario.gxTpr_Contratadausuario_cstuntprdext;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV21ContratadaUsuario_CstUntPrdExt, 18, 5)));
            }
            else
            {
               new prc_contratanteusuario_ehfiscal(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo,  AV8WWPContext.gxTpr_Contratante_codigo, ref  AV7Usuario_Codigo, ref  AV29ContratanteUsuario_EhFiscal,  "Read") ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratanteUsuario_EhFiscal", AV29ContratanteUsuario_EhFiscal);
            }
         }
      }

      protected void E13012( )
      {
         /* After Trn Routine */
         if ( false )
         {
            new wwpbaseobjects.audittransaction(context ).execute(  AV28AuditingObject,  AV32Pgmname) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pgmname", AV32Pgmname);
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwusuario.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         if ( AV8WWPContext.gxTpr_Areatrabalho_codigo == 0 )
         {
            new prc_parametrossistemaalteraflaglicensiado(context ).execute(  1,  true) ;
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV28AuditingObject,  AV32Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pgmname", AV32Pgmname);
         if ( ! AV23EhContratada_Flag )
         {
            new prc_contratanteusuario_ehfiscal(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo,  AV8WWPContext.gxTpr_Contratante_codigo, ref  AV7Usuario_Codigo, ref  AV29ContratanteUsuario_EhFiscal,  "Save") ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratanteUsuario_EhFiscal", AV29ContratanteUsuario_EhFiscal);
         }
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            new prc_usuarioemailupd(context ).execute( ref  A341Usuario_UserGamGuid, ref  AV18Email) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Email", AV18Email);
            if ( AV23EhContratada_Flag )
            {
               new prc_updcstuntprd(context ).execute( ref  AV24Entidade_Codigo, ref  AV7Usuario_Codigo,  AV20ContratadaUsuario_CstUntPrdNrm,  AV21ContratadaUsuario_CstUntPrdExt) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Entidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Entidade_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV20ContratadaUsuario_CstUntPrdNrm, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV21ContratadaUsuario_CstUntPrdExt, 18, 5)));
            }
         }
         AV10WebSession.Remove("Entidade");
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         chkavContratanteusuario_ehfiscal.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratanteusuario_ehfiscal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Visible), 5, 0)));
         cellContratanteusuario_ehfiscal_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratanteusuario_ehfiscal_cell_Internalname, "Class", cellContratanteusuario_ehfiscal_cell_Class);
         cellTextblockcontratanteusuario_ehfiscal_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratanteusuario_ehfiscal_cell_Internalname, "Class", cellTextblockcontratanteusuario_ehfiscal_cell_Class);
         edtavContratadausuario_cstuntprdnrm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdnrm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Visible), 5, 0)));
         cellContratadausuario_cstuntprdnrm_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellContratadausuario_cstuntprdnrm_cell_Class);
         cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdnrm_cell_Class);
         edtavContratadausuario_cstuntprdext_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Visible), 5, 0)));
         cellContratadausuario_cstuntprdext_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadausuario_cstuntprdext_cell_Internalname, "Class", cellContratadausuario_cstuntprdext_cell_Class);
         cellTextblockcontratadausuario_cstuntprdext_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadausuario_cstuntprdext_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdext_cell_Class);
         edtUsuario_UserGamGuid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_UserGamGuid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_UserGamGuid_Visible), 5, 0)));
         cellUsuario_usergamguid_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_usergamguid_cell_Internalname, "Class", cellUsuario_usergamguid_cell_Class);
         cellTextblockusuario_usergamguid_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_usergamguid_cell_Internalname, "Class", cellTextblockusuario_usergamguid_cell_Class);
         chkUsuario_EhContador.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContador.Visible), 5, 0)));
         cellUsuario_ehcontador_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontador_cell_Internalname, "Class", cellUsuario_ehcontador_cell_Class);
         cellTextblockusuario_ehcontador_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontador_cell_Internalname, "Class", cellTextblockusuario_ehcontador_cell_Class);
         chkUsuario_EhAuditorFM.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhAuditorFM_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhAuditorFM.Visible), 5, 0)));
         cellUsuario_ehauditorfm_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehauditorfm_cell_Internalname, "Class", cellUsuario_ehauditorfm_cell_Class);
         cellTextblockusuario_ehauditorfm_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehauditorfm_cell_Internalname, "Class", cellTextblockusuario_ehauditorfm_cell_Class);
         chkUsuario_EhFinanceiro.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhFinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhFinanceiro.Visible), 5, 0)));
         cellUsuario_ehfinanceiro_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehfinanceiro_cell_Internalname, "Class", cellUsuario_ehfinanceiro_cell_Class);
         cellTextblockusuario_ehfinanceiro_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehfinanceiro_cell_Internalname, "Class", cellTextblockusuario_ehfinanceiro_cell_Class);
         chkUsuario_EhContratada.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContratada.Visible), 5, 0)));
         cellUsuario_ehcontratada_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontratada_cell_Internalname, "Class", cellUsuario_ehcontratada_cell_Class);
         cellTextblockusuario_ehcontratada_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontratada_cell_Internalname, "Class", cellTextblockusuario_ehcontratada_cell_Class);
         chkUsuario_EhContratante.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContratante.Visible), 5, 0)));
         cellUsuario_ehcontratante_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontratante_cell_Internalname, "Class", cellUsuario_ehcontratante_cell_Class);
         cellTextblockusuario_ehcontratante_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontratante_cell_Internalname, "Class", cellTextblockusuario_ehcontratante_cell_Class);
         chkUsuario_EhPreposto.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhPreposto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhPreposto.Visible), 5, 0)));
         cellUsuario_ehpreposto_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehpreposto_cell_Internalname, "Class", cellUsuario_ehpreposto_cell_Class);
         cellTextblockusuario_ehpreposto_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehpreposto_cell_Internalname, "Class", cellTextblockusuario_ehpreposto_cell_Class);
      }

      protected void ZM011( short GX_JID )
      {
         if ( ( GX_JID == 58 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z341Usuario_UserGamGuid = T00013_A341Usuario_UserGamGuid[0];
               Z2Usuario_Nome = T00013_A2Usuario_Nome[0];
               Z289Usuario_EhContador = T00013_A289Usuario_EhContador[0];
               Z291Usuario_EhContratada = T00013_A291Usuario_EhContratada[0];
               Z292Usuario_EhContratante = T00013_A292Usuario_EhContratante[0];
               Z293Usuario_EhFinanceiro = T00013_A293Usuario_EhFinanceiro[0];
               Z538Usuario_EhGestor = T00013_A538Usuario_EhGestor[0];
               Z1093Usuario_EhPreposto = T00013_A1093Usuario_EhPreposto[0];
               Z1017Usuario_CrtfPath = T00013_A1017Usuario_CrtfPath[0];
               Z1235Usuario_Notificar = T00013_A1235Usuario_Notificar[0];
               Z54Usuario_Ativo = T00013_A54Usuario_Ativo[0];
               Z1647Usuario_Email = T00013_A1647Usuario_Email[0];
               Z1908Usuario_DeFerias = T00013_A1908Usuario_DeFerias[0];
               Z2016Usuario_UltimaArea = T00013_A2016Usuario_UltimaArea[0];
               Z57Usuario_PessoaCod = T00013_A57Usuario_PessoaCod[0];
               Z1073Usuario_CargoCod = T00013_A1073Usuario_CargoCod[0];
            }
            else
            {
               Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
               Z2Usuario_Nome = A2Usuario_Nome;
               Z289Usuario_EhContador = A289Usuario_EhContador;
               Z291Usuario_EhContratada = A291Usuario_EhContratada;
               Z292Usuario_EhContratante = A292Usuario_EhContratante;
               Z293Usuario_EhFinanceiro = A293Usuario_EhFinanceiro;
               Z538Usuario_EhGestor = A538Usuario_EhGestor;
               Z1093Usuario_EhPreposto = A1093Usuario_EhPreposto;
               Z1017Usuario_CrtfPath = A1017Usuario_CrtfPath;
               Z1235Usuario_Notificar = A1235Usuario_Notificar;
               Z54Usuario_Ativo = A54Usuario_Ativo;
               Z1647Usuario_Email = A1647Usuario_Email;
               Z1908Usuario_DeFerias = A1908Usuario_DeFerias;
               Z2016Usuario_UltimaArea = A2016Usuario_UltimaArea;
               Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
               Z1073Usuario_CargoCod = A1073Usuario_CargoCod;
            }
         }
         if ( ( GX_JID == 59 ) || ( GX_JID == 0 ) )
         {
            Z58Usuario_PessoaNom = T00015_A58Usuario_PessoaNom[0];
            Z59Usuario_PessoaTip = T00015_A59Usuario_PessoaTip[0];
            Z325Usuario_PessoaDoc = T00015_A325Usuario_PessoaDoc[0];
            Z2095Usuario_PessoaTelefone = T00015_A2095Usuario_PessoaTelefone[0];
         }
         if ( GX_JID == -58 )
         {
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z289Usuario_EhContador = A289Usuario_EhContador;
            Z291Usuario_EhContratada = A291Usuario_EhContratada;
            Z292Usuario_EhContratante = A292Usuario_EhContratante;
            Z293Usuario_EhFinanceiro = A293Usuario_EhFinanceiro;
            Z538Usuario_EhGestor = A538Usuario_EhGestor;
            Z1093Usuario_EhPreposto = A1093Usuario_EhPreposto;
            Z1017Usuario_CrtfPath = A1017Usuario_CrtfPath;
            Z1235Usuario_Notificar = A1235Usuario_Notificar;
            Z54Usuario_Ativo = A54Usuario_Ativo;
            Z1647Usuario_Email = A1647Usuario_Email;
            Z1716Usuario_Foto = A1716Usuario_Foto;
            Z40000Usuario_Foto_GXI = A40000Usuario_Foto_GXI;
            Z1908Usuario_DeFerias = A1908Usuario_DeFerias;
            Z2016Usuario_UltimaArea = A2016Usuario_UltimaArea;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z1073Usuario_CargoCod = A1073Usuario_CargoCod;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
            Z59Usuario_PessoaTip = A59Usuario_PessoaTip;
            Z325Usuario_PessoaDoc = A325Usuario_PessoaDoc;
            Z2095Usuario_PessoaTelefone = A2095Usuario_PessoaTelefone;
            Z1074Usuario_CargoNom = A1074Usuario_CargoNom;
            Z1075Usuario_CargoUOCod = A1075Usuario_CargoUOCod;
            Z1076Usuario_CargoUONom = A1076Usuario_CargoUONom;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvUNIDADEORGANIZACIONAL_CODIGO_html011( ) ;
         edtUsuario_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
         edtUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV32Pgmname = "Usuario";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pgmname", AV32Pgmname);
         edtUsuario_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
         edtUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Usuario_Codigo) )
         {
            A1Usuario_Codigo = AV7Usuario_Codigo;
            n1Usuario_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         edtUsuario_UserGamGuid_Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_UserGamGuid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_UserGamGuid_Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_usergamguid_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_usergamguid_cell_Internalname, "Class", cellUsuario_usergamguid_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_usergamguid_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_usergamguid_cell_Internalname, "Class", cellUsuario_usergamguid_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_usergamguid_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_usergamguid_cell_Internalname, "Class", cellTextblockusuario_usergamguid_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_usergamguid_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_usergamguid_cell_Internalname, "Class", cellTextblockusuario_usergamguid_cell_Class);
            }
         }
         chkUsuario_EhContador.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContador.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_ehcontador_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontador_cell_Internalname, "Class", cellUsuario_ehcontador_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_ehcontador_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontador_cell_Internalname, "Class", cellUsuario_ehcontador_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_ehcontador_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontador_cell_Internalname, "Class", cellTextblockusuario_ehcontador_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_ehcontador_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontador_cell_Internalname, "Class", cellTextblockusuario_ehcontador_cell_Class);
            }
         }
         chkUsuario_EhAuditorFM.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhAuditorFM_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhAuditorFM.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_ehauditorfm_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehauditorfm_cell_Internalname, "Class", cellUsuario_ehauditorfm_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_ehauditorfm_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehauditorfm_cell_Internalname, "Class", cellUsuario_ehauditorfm_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_ehauditorfm_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehauditorfm_cell_Internalname, "Class", cellTextblockusuario_ehauditorfm_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_ehauditorfm_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehauditorfm_cell_Internalname, "Class", cellTextblockusuario_ehauditorfm_cell_Class);
            }
         }
         chkUsuario_EhFinanceiro.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhFinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhFinanceiro.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_ehfinanceiro_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehfinanceiro_cell_Internalname, "Class", cellUsuario_ehfinanceiro_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_ehfinanceiro_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehfinanceiro_cell_Internalname, "Class", cellUsuario_ehfinanceiro_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_ehfinanceiro_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehfinanceiro_cell_Internalname, "Class", cellTextblockusuario_ehfinanceiro_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_ehfinanceiro_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehfinanceiro_cell_Internalname, "Class", cellTextblockusuario_ehfinanceiro_cell_Class);
            }
         }
         chkUsuario_EhContratada.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContratada.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_ehcontratada_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontratada_cell_Internalname, "Class", cellUsuario_ehcontratada_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_ehcontratada_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontratada_cell_Internalname, "Class", cellUsuario_ehcontratada_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_ehcontratada_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontratada_cell_Internalname, "Class", cellTextblockusuario_ehcontratada_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_ehcontratada_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontratada_cell_Internalname, "Class", cellTextblockusuario_ehcontratada_cell_Class);
            }
         }
         chkUsuario_EhContratante.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContratante.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_ehcontratante_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontratante_cell_Internalname, "Class", cellUsuario_ehcontratante_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_ehcontratante_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehcontratante_cell_Internalname, "Class", cellUsuario_ehcontratante_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_ehcontratante_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontratante_cell_Internalname, "Class", cellTextblockusuario_ehcontratante_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_ehcontratante_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehcontratante_cell_Internalname, "Class", cellTextblockusuario_ehcontratante_cell_Class);
            }
         }
         chkUsuario_EhPreposto.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhPreposto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhPreposto.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellUsuario_ehpreposto_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehpreposto_cell_Internalname, "Class", cellUsuario_ehpreposto_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellUsuario_ehpreposto_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellUsuario_ehpreposto_cell_Internalname, "Class", cellUsuario_ehpreposto_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockusuario_ehpreposto_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehpreposto_cell_Internalname, "Class", cellTextblockusuario_ehpreposto_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockusuario_ehpreposto_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockusuario_ehpreposto_cell_Internalname, "Class", cellTextblockusuario_ehpreposto_cell_Class);
            }
         }
         chkavContratanteusuario_ehfiscal.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratanteusuario_ehfiscal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellContratanteusuario_ehfiscal_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratanteusuario_ehfiscal_cell_Internalname, "Class", cellContratanteusuario_ehfiscal_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellContratanteusuario_ehfiscal_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratanteusuario_ehfiscal_cell_Internalname, "Class", cellContratanteusuario_ehfiscal_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockcontratanteusuario_ehfiscal_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratanteusuario_ehfiscal_cell_Internalname, "Class", cellTextblockcontratanteusuario_ehfiscal_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockcontratanteusuario_ehfiscal_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratanteusuario_ehfiscal_cell_Internalname, "Class", cellTextblockcontratanteusuario_ehfiscal_cell_Class);
            }
         }
         edtavContratadausuario_cstuntprdnrm_Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdnrm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellContratadausuario_cstuntprdnrm_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellContratadausuario_cstuntprdnrm_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellContratadausuario_cstuntprdnrm_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellContratadausuario_cstuntprdnrm_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdnrm_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdnrm_cell_Class);
            }
         }
         edtavContratadausuario_cstuntprdext_Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellContratadausuario_cstuntprdext_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadausuario_cstuntprdext_cell_Internalname, "Class", cellContratadausuario_cstuntprdext_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellContratadausuario_cstuntprdext_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadausuario_cstuntprdext_cell_Internalname, "Class", cellContratadausuario_cstuntprdext_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockcontratadausuario_cstuntprdext_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadausuario_cstuntprdext_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdext_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockcontratadausuario_cstuntprdext_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadausuario_cstuntprdext_cell_Internalname, "Class", cellTextblockcontratadausuario_cstuntprdext_cell_Class);
            }
         }
      }

      protected void standaloneModal( )
      {
         chkUsuario_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_Ativo.Visible), 5, 0)));
         lblTextblockusuario_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockusuario_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockusuario_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_Usuario_PessoaCod) )
         {
            A57Usuario_PessoaCod = AV14Insert_Usuario_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV25Insert_Usuario_CargoCod) )
         {
            A1073Usuario_CargoCod = AV25Insert_Usuario_CargoCod;
            n1073Usuario_CargoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1908Usuario_DeFerias) && ( Gx_BScreen == 0 ) )
         {
            A1908Usuario_DeFerias = false;
            n1908Usuario_DeFerias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A54Usuario_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A54Usuario_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1235Usuario_Notificar)) && ( Gx_BScreen == 0 ) )
         {
            A1235Usuario_Notificar = "A";
            n1235Usuario_Notificar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1093Usuario_EhPreposto) && ( Gx_BScreen == 0 ) )
         {
            A1093Usuario_EhPreposto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1093Usuario_EhPreposto", A1093Usuario_EhPreposto);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A538Usuario_EhGestor) && ( Gx_BScreen == 0 ) )
         {
            A538Usuario_EhGestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A538Usuario_EhGestor", A538Usuario_EhGestor);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A1083Usuario_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
            GXt_boolean2 = A290Usuario_EhAuditorFM;
            new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A290Usuario_EhAuditorFM = GXt_boolean2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
            /* Using cursor T00015 */
            pr_default.execute(3, new Object[] {A57Usuario_PessoaCod});
            ZM011( 59) ;
            A58Usuario_PessoaNom = T00015_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T00015_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = T00015_A59Usuario_PessoaTip[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
            n59Usuario_PessoaTip = T00015_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = T00015_A325Usuario_PessoaDoc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
            n325Usuario_PessoaDoc = T00015_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = T00015_A2095Usuario_PessoaTelefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2095Usuario_PessoaTelefone", A2095Usuario_PessoaTelefone);
            n2095Usuario_PessoaTelefone = T00015_n2095Usuario_PessoaTelefone[0];
            pr_default.close(3);
            /* Using cursor T00016 */
            pr_default.execute(4, new Object[] {n1073Usuario_CargoCod, A1073Usuario_CargoCod});
            A1074Usuario_CargoNom = T00016_A1074Usuario_CargoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
            n1074Usuario_CargoNom = T00016_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = T00016_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = T00016_n1075Usuario_CargoUOCod[0];
            pr_default.close(4);
            /* Using cursor T00017 */
            pr_default.execute(5, new Object[] {n1075Usuario_CargoUOCod, A1075Usuario_CargoUOCod});
            A1076Usuario_CargoUONom = T00017_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = T00017_n1076Usuario_CargoUONom[0];
            pr_default.close(5);
         }
      }

      protected void Load011( )
      {
         /* Using cursor T00018 */
         pr_default.execute(6, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound1 = 1;
            A1074Usuario_CargoNom = T00018_A1074Usuario_CargoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
            n1074Usuario_CargoNom = T00018_n1074Usuario_CargoNom[0];
            A1076Usuario_CargoUONom = T00018_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = T00018_n1076Usuario_CargoUONom[0];
            A58Usuario_PessoaNom = T00018_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T00018_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = T00018_A59Usuario_PessoaTip[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
            n59Usuario_PessoaTip = T00018_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = T00018_A325Usuario_PessoaDoc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
            n325Usuario_PessoaDoc = T00018_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = T00018_A2095Usuario_PessoaTelefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2095Usuario_PessoaTelefone", A2095Usuario_PessoaTelefone);
            n2095Usuario_PessoaTelefone = T00018_n2095Usuario_PessoaTelefone[0];
            A341Usuario_UserGamGuid = T00018_A341Usuario_UserGamGuid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
            A2Usuario_Nome = T00018_A2Usuario_Nome[0];
            n2Usuario_Nome = T00018_n2Usuario_Nome[0];
            A289Usuario_EhContador = T00018_A289Usuario_EhContador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A289Usuario_EhContador", A289Usuario_EhContador);
            A291Usuario_EhContratada = T00018_A291Usuario_EhContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A291Usuario_EhContratada", A291Usuario_EhContratada);
            A292Usuario_EhContratante = T00018_A292Usuario_EhContratante[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A292Usuario_EhContratante", A292Usuario_EhContratante);
            A293Usuario_EhFinanceiro = T00018_A293Usuario_EhFinanceiro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A293Usuario_EhFinanceiro", A293Usuario_EhFinanceiro);
            A538Usuario_EhGestor = T00018_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = T00018_A1093Usuario_EhPreposto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1093Usuario_EhPreposto", A1093Usuario_EhPreposto);
            A1017Usuario_CrtfPath = T00018_A1017Usuario_CrtfPath[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1017Usuario_CrtfPath", A1017Usuario_CrtfPath);
            n1017Usuario_CrtfPath = T00018_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = T00018_A1235Usuario_Notificar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
            n1235Usuario_Notificar = T00018_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = T00018_A54Usuario_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
            A1647Usuario_Email = T00018_A1647Usuario_Email[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1647Usuario_Email", A1647Usuario_Email);
            n1647Usuario_Email = T00018_n1647Usuario_Email[0];
            A40000Usuario_Foto_GXI = T00018_A40000Usuario_Foto_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
            n40000Usuario_Foto_GXI = T00018_n40000Usuario_Foto_GXI[0];
            A1908Usuario_DeFerias = T00018_A1908Usuario_DeFerias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
            n1908Usuario_DeFerias = T00018_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = T00018_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = T00018_n2016Usuario_UltimaArea[0];
            A57Usuario_PessoaCod = T00018_A57Usuario_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            A1073Usuario_CargoCod = T00018_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = T00018_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = T00018_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = T00018_n1075Usuario_CargoUOCod[0];
            A1716Usuario_Foto = T00018_A1716Usuario_Foto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1716Usuario_Foto", A1716Usuario_Foto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
            n1716Usuario_Foto = T00018_n1716Usuario_Foto[0];
            ZM011( -58) ;
         }
         pr_default.close(6);
         OnLoadActions011( ) ;
      }

      protected void OnLoadActions011( )
      {
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A1083Usuario_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A290Usuario_EhAuditorFM = GXt_boolean2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV26UnidadeOrganizacional_Codigo = A1075Usuario_CargoUOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable011( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A1083Usuario_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A290Usuario_EhAuditorFM = GXt_boolean2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
         if ( A292Usuario_EhContratante && A291Usuario_EhContratada )
         {
            GX_msglist.addItem("N�o pode ser Contratante e Contratada ao mesmo tempo!", 1, "USUARIO_EHCONTRATADA");
            AnyError = 1;
            GX_FocusControl = chkUsuario_EhContratada_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( GxRegex.IsMatch(A1647Usuario_Email,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) ) )
         {
            GX_msglist.addItem("O valor de Usuario_Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "USUARIO_EMAIL");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Email_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00015 */
         pr_default.execute(3, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T00015_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = T00015_n58Usuario_PessoaNom[0];
         A59Usuario_PessoaTip = T00015_A59Usuario_PessoaTip[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
         n59Usuario_PessoaTip = T00015_n59Usuario_PessoaTip[0];
         A325Usuario_PessoaDoc = T00015_A325Usuario_PessoaDoc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
         n325Usuario_PessoaDoc = T00015_n325Usuario_PessoaDoc[0];
         pr_default.close(3);
         /* Using cursor T00016 */
         pr_default.execute(4, new Object[] {n1073Usuario_CargoCod, A1073Usuario_CargoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1073Usuario_CargoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'USUARIO_CARGO'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1074Usuario_CargoNom = T00016_A1074Usuario_CargoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
         n1074Usuario_CargoNom = T00016_n1074Usuario_CargoNom[0];
         A1075Usuario_CargoUOCod = T00016_A1075Usuario_CargoUOCod[0];
         n1075Usuario_CargoUOCod = T00016_n1075Usuario_CargoUOCod[0];
         pr_default.close(4);
         /* Using cursor T00017 */
         pr_default.execute(5, new Object[] {n1075Usuario_CargoUOCod, A1075Usuario_CargoUOCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1075Usuario_CargoUOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1076Usuario_CargoUONom = T00017_A1076Usuario_CargoUONom[0];
         n1076Usuario_CargoUONom = T00017_n1076Usuario_CargoUONom[0];
         pr_default.close(5);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV26UnidadeOrganizacional_Codigo = A1075Usuario_CargoUOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)));
         }
      }

      protected void CloseExtendedTableCursors011( )
      {
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_59( int A57Usuario_PessoaCod )
      {
         /* Using cursor T00019 */
         pr_default.execute(7, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T00019_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = T00019_n58Usuario_PessoaNom[0];
         A59Usuario_PessoaTip = T00019_A59Usuario_PessoaTip[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
         n59Usuario_PessoaTip = T00019_n59Usuario_PessoaTip[0];
         A325Usuario_PessoaDoc = T00019_A325Usuario_PessoaDoc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
         n325Usuario_PessoaDoc = T00019_n325Usuario_PessoaDoc[0];
         A2095Usuario_PessoaTelefone = T00019_A2095Usuario_PessoaTelefone[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2095Usuario_PessoaTelefone", A2095Usuario_PessoaTelefone);
         n2095Usuario_PessoaTelefone = T00019_n2095Usuario_PessoaTelefone[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A58Usuario_PessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A59Usuario_PessoaTip))+"\""+","+"\""+GXUtil.EncodeJSConstant( A325Usuario_PessoaDoc)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2095Usuario_PessoaTelefone))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_60( int A1073Usuario_CargoCod )
      {
         /* Using cursor T000110 */
         pr_default.execute(8, new Object[] {n1073Usuario_CargoCod, A1073Usuario_CargoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A1073Usuario_CargoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'USUARIO_CARGO'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1074Usuario_CargoNom = T000110_A1074Usuario_CargoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
         n1074Usuario_CargoNom = T000110_n1074Usuario_CargoNom[0];
         A1075Usuario_CargoUOCod = T000110_A1075Usuario_CargoUOCod[0];
         n1075Usuario_CargoUOCod = T000110_n1075Usuario_CargoUOCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1074Usuario_CargoNom)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1075Usuario_CargoUOCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_61( int A1075Usuario_CargoUOCod )
      {
         /* Using cursor T000111 */
         pr_default.execute(9, new Object[] {n1075Usuario_CargoUOCod, A1075Usuario_CargoUOCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A1075Usuario_CargoUOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1076Usuario_CargoUONom = T000111_A1076Usuario_CargoUONom[0];
         n1076Usuario_CargoUONom = T000111_n1076Usuario_CargoUONom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1076Usuario_CargoUONom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey011( )
      {
         /* Using cursor T000112 */
         pr_default.execute(10, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound1 = 1;
         }
         else
         {
            RcdFound1 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00013 */
         pr_default.execute(1, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM011( 58) ;
            RcdFound1 = 1;
            A1Usuario_Codigo = T00013_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            n1Usuario_Codigo = T00013_n1Usuario_Codigo[0];
            A341Usuario_UserGamGuid = T00013_A341Usuario_UserGamGuid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
            A2Usuario_Nome = T00013_A2Usuario_Nome[0];
            n2Usuario_Nome = T00013_n2Usuario_Nome[0];
            A289Usuario_EhContador = T00013_A289Usuario_EhContador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A289Usuario_EhContador", A289Usuario_EhContador);
            A291Usuario_EhContratada = T00013_A291Usuario_EhContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A291Usuario_EhContratada", A291Usuario_EhContratada);
            A292Usuario_EhContratante = T00013_A292Usuario_EhContratante[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A292Usuario_EhContratante", A292Usuario_EhContratante);
            A293Usuario_EhFinanceiro = T00013_A293Usuario_EhFinanceiro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A293Usuario_EhFinanceiro", A293Usuario_EhFinanceiro);
            A538Usuario_EhGestor = T00013_A538Usuario_EhGestor[0];
            A1093Usuario_EhPreposto = T00013_A1093Usuario_EhPreposto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1093Usuario_EhPreposto", A1093Usuario_EhPreposto);
            A1017Usuario_CrtfPath = T00013_A1017Usuario_CrtfPath[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1017Usuario_CrtfPath", A1017Usuario_CrtfPath);
            n1017Usuario_CrtfPath = T00013_n1017Usuario_CrtfPath[0];
            A1235Usuario_Notificar = T00013_A1235Usuario_Notificar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
            n1235Usuario_Notificar = T00013_n1235Usuario_Notificar[0];
            A54Usuario_Ativo = T00013_A54Usuario_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
            A1647Usuario_Email = T00013_A1647Usuario_Email[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1647Usuario_Email", A1647Usuario_Email);
            n1647Usuario_Email = T00013_n1647Usuario_Email[0];
            A40000Usuario_Foto_GXI = T00013_A40000Usuario_Foto_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
            n40000Usuario_Foto_GXI = T00013_n40000Usuario_Foto_GXI[0];
            A1908Usuario_DeFerias = T00013_A1908Usuario_DeFerias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
            n1908Usuario_DeFerias = T00013_n1908Usuario_DeFerias[0];
            A2016Usuario_UltimaArea = T00013_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = T00013_n2016Usuario_UltimaArea[0];
            A57Usuario_PessoaCod = T00013_A57Usuario_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            A1073Usuario_CargoCod = T00013_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = T00013_n1073Usuario_CargoCod[0];
            A1716Usuario_Foto = T00013_A1716Usuario_Foto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1716Usuario_Foto", A1716Usuario_Foto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
            n1716Usuario_Foto = T00013_n1716Usuario_Foto[0];
            Z1Usuario_Codigo = A1Usuario_Codigo;
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load011( ) ;
            if ( AnyError == 1 )
            {
               RcdFound1 = 0;
               InitializeNonKey011( ) ;
            }
            Gx_mode = sMode1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound1 = 0;
            InitializeNonKey011( ) ;
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey011( ) ;
         if ( RcdFound1 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound1 = 0;
         /* Using cursor T000113 */
         pr_default.execute(11, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T000113_A1Usuario_Codigo[0] < A1Usuario_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T000113_A1Usuario_Codigo[0] > A1Usuario_Codigo ) ) )
            {
               A1Usuario_Codigo = T000113_A1Usuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               n1Usuario_Codigo = T000113_n1Usuario_Codigo[0];
               RcdFound1 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound1 = 0;
         /* Using cursor T000114 */
         pr_default.execute(12, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T000114_A1Usuario_Codigo[0] > A1Usuario_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T000114_A1Usuario_Codigo[0] < A1Usuario_Codigo ) ) )
            {
               A1Usuario_Codigo = T000114_A1Usuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               n1Usuario_Codigo = T000114_n1Usuario_Codigo[0];
               RcdFound1 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey011( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert011( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound1 == 1 )
            {
               if ( A1Usuario_Codigo != Z1Usuario_Codigo )
               {
                  A1Usuario_Codigo = Z1Usuario_Codigo;
                  n1Usuario_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "USUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update011( ) ;
                  GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1Usuario_Codigo != Z1Usuario_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert011( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "USUARIO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert011( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1Usuario_Codigo != Z1Usuario_Codigo )
         {
            A1Usuario_Codigo = Z1Usuario_Codigo;
            n1Usuario_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "USUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtUsuario_UserGamGuid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00012 */
            pr_default.execute(0, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Usuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z341Usuario_UserGamGuid, T00012_A341Usuario_UserGamGuid[0]) != 0 ) || ( StringUtil.StrCmp(Z2Usuario_Nome, T00012_A2Usuario_Nome[0]) != 0 ) || ( Z289Usuario_EhContador != T00012_A289Usuario_EhContador[0] ) || ( Z291Usuario_EhContratada != T00012_A291Usuario_EhContratada[0] ) || ( Z292Usuario_EhContratante != T00012_A292Usuario_EhContratante[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z293Usuario_EhFinanceiro != T00012_A293Usuario_EhFinanceiro[0] ) || ( Z538Usuario_EhGestor != T00012_A538Usuario_EhGestor[0] ) || ( Z1093Usuario_EhPreposto != T00012_A1093Usuario_EhPreposto[0] ) || ( StringUtil.StrCmp(Z1017Usuario_CrtfPath, T00012_A1017Usuario_CrtfPath[0]) != 0 ) || ( StringUtil.StrCmp(Z1235Usuario_Notificar, T00012_A1235Usuario_Notificar[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z54Usuario_Ativo != T00012_A54Usuario_Ativo[0] ) || ( StringUtil.StrCmp(Z1647Usuario_Email, T00012_A1647Usuario_Email[0]) != 0 ) || ( Z1908Usuario_DeFerias != T00012_A1908Usuario_DeFerias[0] ) || ( Z2016Usuario_UltimaArea != T00012_A2016Usuario_UltimaArea[0] ) || ( Z57Usuario_PessoaCod != T00012_A57Usuario_PessoaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1073Usuario_CargoCod != T00012_A1073Usuario_CargoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z341Usuario_UserGamGuid, T00012_A341Usuario_UserGamGuid[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_UserGamGuid");
                  GXUtil.WriteLogRaw("Old: ",Z341Usuario_UserGamGuid);
                  GXUtil.WriteLogRaw("Current: ",T00012_A341Usuario_UserGamGuid[0]);
               }
               if ( StringUtil.StrCmp(Z2Usuario_Nome, T00012_A2Usuario_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z2Usuario_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00012_A2Usuario_Nome[0]);
               }
               if ( Z289Usuario_EhContador != T00012_A289Usuario_EhContador[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_EhContador");
                  GXUtil.WriteLogRaw("Old: ",Z289Usuario_EhContador);
                  GXUtil.WriteLogRaw("Current: ",T00012_A289Usuario_EhContador[0]);
               }
               if ( Z291Usuario_EhContratada != T00012_A291Usuario_EhContratada[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_EhContratada");
                  GXUtil.WriteLogRaw("Old: ",Z291Usuario_EhContratada);
                  GXUtil.WriteLogRaw("Current: ",T00012_A291Usuario_EhContratada[0]);
               }
               if ( Z292Usuario_EhContratante != T00012_A292Usuario_EhContratante[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_EhContratante");
                  GXUtil.WriteLogRaw("Old: ",Z292Usuario_EhContratante);
                  GXUtil.WriteLogRaw("Current: ",T00012_A292Usuario_EhContratante[0]);
               }
               if ( Z293Usuario_EhFinanceiro != T00012_A293Usuario_EhFinanceiro[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_EhFinanceiro");
                  GXUtil.WriteLogRaw("Old: ",Z293Usuario_EhFinanceiro);
                  GXUtil.WriteLogRaw("Current: ",T00012_A293Usuario_EhFinanceiro[0]);
               }
               if ( Z538Usuario_EhGestor != T00012_A538Usuario_EhGestor[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_EhGestor");
                  GXUtil.WriteLogRaw("Old: ",Z538Usuario_EhGestor);
                  GXUtil.WriteLogRaw("Current: ",T00012_A538Usuario_EhGestor[0]);
               }
               if ( Z1093Usuario_EhPreposto != T00012_A1093Usuario_EhPreposto[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_EhPreposto");
                  GXUtil.WriteLogRaw("Old: ",Z1093Usuario_EhPreposto);
                  GXUtil.WriteLogRaw("Current: ",T00012_A1093Usuario_EhPreposto[0]);
               }
               if ( StringUtil.StrCmp(Z1017Usuario_CrtfPath, T00012_A1017Usuario_CrtfPath[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_CrtfPath");
                  GXUtil.WriteLogRaw("Old: ",Z1017Usuario_CrtfPath);
                  GXUtil.WriteLogRaw("Current: ",T00012_A1017Usuario_CrtfPath[0]);
               }
               if ( StringUtil.StrCmp(Z1235Usuario_Notificar, T00012_A1235Usuario_Notificar[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_Notificar");
                  GXUtil.WriteLogRaw("Old: ",Z1235Usuario_Notificar);
                  GXUtil.WriteLogRaw("Current: ",T00012_A1235Usuario_Notificar[0]);
               }
               if ( Z54Usuario_Ativo != T00012_A54Usuario_Ativo[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z54Usuario_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00012_A54Usuario_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z1647Usuario_Email, T00012_A1647Usuario_Email[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_Email");
                  GXUtil.WriteLogRaw("Old: ",Z1647Usuario_Email);
                  GXUtil.WriteLogRaw("Current: ",T00012_A1647Usuario_Email[0]);
               }
               if ( Z1908Usuario_DeFerias != T00012_A1908Usuario_DeFerias[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_DeFerias");
                  GXUtil.WriteLogRaw("Old: ",Z1908Usuario_DeFerias);
                  GXUtil.WriteLogRaw("Current: ",T00012_A1908Usuario_DeFerias[0]);
               }
               if ( Z2016Usuario_UltimaArea != T00012_A2016Usuario_UltimaArea[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_UltimaArea");
                  GXUtil.WriteLogRaw("Old: ",Z2016Usuario_UltimaArea);
                  GXUtil.WriteLogRaw("Current: ",T00012_A2016Usuario_UltimaArea[0]);
               }
               if ( Z57Usuario_PessoaCod != T00012_A57Usuario_PessoaCod[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_PessoaCod");
                  GXUtil.WriteLogRaw("Old: ",Z57Usuario_PessoaCod);
                  GXUtil.WriteLogRaw("Current: ",T00012_A57Usuario_PessoaCod[0]);
               }
               if ( Z1073Usuario_CargoCod != T00012_A1073Usuario_CargoCod[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_CargoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1073Usuario_CargoCod);
                  GXUtil.WriteLogRaw("Current: ",T00012_A1073Usuario_CargoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Usuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
         /* Using cursor T00014 */
         pr_default.execute(2, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(2) == 103) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Pessoa"}), "RecordIsLocked", 1, "");
            AnyError = 1;
            return  ;
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            if ( false || ( StringUtil.StrCmp(Z58Usuario_PessoaNom, T00014_A58Usuario_PessoaNom[0]) != 0 ) || ( StringUtil.StrCmp(Z59Usuario_PessoaTip, T00014_A59Usuario_PessoaTip[0]) != 0 ) || ( StringUtil.StrCmp(Z325Usuario_PessoaDoc, T00014_A325Usuario_PessoaDoc[0]) != 0 ) || ( StringUtil.StrCmp(Z2095Usuario_PessoaTelefone, T00014_A2095Usuario_PessoaTelefone[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z58Usuario_PessoaNom, T00014_A58Usuario_PessoaNom[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_PessoaNom");
                  GXUtil.WriteLogRaw("Old: ",Z58Usuario_PessoaNom);
                  GXUtil.WriteLogRaw("Current: ",T00014_A58Usuario_PessoaNom[0]);
               }
               if ( StringUtil.StrCmp(Z59Usuario_PessoaTip, T00014_A59Usuario_PessoaTip[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_PessoaTip");
                  GXUtil.WriteLogRaw("Old: ",Z59Usuario_PessoaTip);
                  GXUtil.WriteLogRaw("Current: ",T00014_A59Usuario_PessoaTip[0]);
               }
               if ( StringUtil.StrCmp(Z325Usuario_PessoaDoc, T00014_A325Usuario_PessoaDoc[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_PessoaDoc");
                  GXUtil.WriteLogRaw("Old: ",Z325Usuario_PessoaDoc);
                  GXUtil.WriteLogRaw("Current: ",T00014_A325Usuario_PessoaDoc[0]);
               }
               if ( StringUtil.StrCmp(Z2095Usuario_PessoaTelefone, T00014_A2095Usuario_PessoaTelefone[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuario_PessoaTelefone");
                  GXUtil.WriteLogRaw("Old: ",Z2095Usuario_PessoaTelefone);
                  GXUtil.WriteLogRaw("Current: ",T00014_A2095Usuario_PessoaTelefone[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Pessoa"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert011( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM011( 0) ;
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000115 */
                     pr_default.execute(13, new Object[] {A341Usuario_UserGamGuid, n2Usuario_Nome, A2Usuario_Nome, A289Usuario_EhContador, A291Usuario_EhContratada, A292Usuario_EhContratante, A293Usuario_EhFinanceiro, A538Usuario_EhGestor, A1093Usuario_EhPreposto, n1017Usuario_CrtfPath, A1017Usuario_CrtfPath, n1235Usuario_Notificar, A1235Usuario_Notificar, A54Usuario_Ativo, n1647Usuario_Email, A1647Usuario_Email, n1716Usuario_Foto, A1716Usuario_Foto, n40000Usuario_Foto_GXI, A40000Usuario_Foto_GXI, n1908Usuario_DeFerias, A1908Usuario_DeFerias, n2016Usuario_UltimaArea, A2016Usuario_UltimaArea, A57Usuario_PessoaCod, n1073Usuario_CargoCod, A1073Usuario_CargoCod});
                     A1Usuario_Codigo = T000115_A1Usuario_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                     n1Usuario_Codigo = T000115_n1Usuario_Codigo[0];
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
                     if ( AnyError == 0 )
                     {
                        UpdateTablesN1011( ) ;
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption010( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load011( ) ;
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void Update011( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000116 */
                     pr_default.execute(14, new Object[] {A341Usuario_UserGamGuid, n2Usuario_Nome, A2Usuario_Nome, A289Usuario_EhContador, A291Usuario_EhContratada, A292Usuario_EhContratante, A293Usuario_EhFinanceiro, A538Usuario_EhGestor, A1093Usuario_EhPreposto, n1017Usuario_CrtfPath, A1017Usuario_CrtfPath, n1235Usuario_Notificar, A1235Usuario_Notificar, A54Usuario_Ativo, n1647Usuario_Email, A1647Usuario_Email, n1908Usuario_DeFerias, A1908Usuario_DeFerias, n2016Usuario_UltimaArea, A2016Usuario_UltimaArea, A57Usuario_PessoaCod, n1073Usuario_CargoCod, A1073Usuario_CargoCod, n1Usuario_Codigo, A1Usuario_Codigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Usuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate011( ) ;
                     if ( AnyError == 0 )
                     {
                        UpdateTablesN1011( ) ;
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void DeferredUpdate011( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000117 */
            pr_default.execute(15, new Object[] {n1716Usuario_Foto, A1716Usuario_Foto, n40000Usuario_Foto_GXI, A40000Usuario_Foto_GXI, n1Usuario_Codigo, A1Usuario_Codigo});
            pr_default.close(15);
            dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls011( ) ;
            AfterConfirm011( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete011( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000118 */
                  pr_default.execute(16, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("Usuario") ;
                  if ( AnyError == 0 )
                  {
                     UpdateTablesN1011( ) ;
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode1 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel011( ) ;
         Gx_mode = sMode1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls011( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A1083Usuario_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
            GXt_boolean2 = A290Usuario_EhAuditorFM;
            new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A290Usuario_EhAuditorFM = GXt_boolean2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
            /* Using cursor T000119 */
            pr_default.execute(17, new Object[] {A57Usuario_PessoaCod});
            Z58Usuario_PessoaNom = T000119_A58Usuario_PessoaNom[0];
            Z59Usuario_PessoaTip = T000119_A59Usuario_PessoaTip[0];
            Z325Usuario_PessoaDoc = T000119_A325Usuario_PessoaDoc[0];
            Z2095Usuario_PessoaTelefone = T000119_A2095Usuario_PessoaTelefone[0];
            A58Usuario_PessoaNom = T000119_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T000119_n58Usuario_PessoaNom[0];
            A59Usuario_PessoaTip = T000119_A59Usuario_PessoaTip[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
            n59Usuario_PessoaTip = T000119_n59Usuario_PessoaTip[0];
            A325Usuario_PessoaDoc = T000119_A325Usuario_PessoaDoc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
            n325Usuario_PessoaDoc = T000119_n325Usuario_PessoaDoc[0];
            A2095Usuario_PessoaTelefone = T000119_A2095Usuario_PessoaTelefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2095Usuario_PessoaTelefone", A2095Usuario_PessoaTelefone);
            n2095Usuario_PessoaTelefone = T000119_n2095Usuario_PessoaTelefone[0];
            pr_default.close(17);
            /* Using cursor T000120 */
            pr_default.execute(18, new Object[] {n1073Usuario_CargoCod, A1073Usuario_CargoCod});
            A1074Usuario_CargoNom = T000120_A1074Usuario_CargoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
            n1074Usuario_CargoNom = T000120_n1074Usuario_CargoNom[0];
            A1075Usuario_CargoUOCod = T000120_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = T000120_n1075Usuario_CargoUOCod[0];
            pr_default.close(18);
            /* Using cursor T000121 */
            pr_default.execute(19, new Object[] {n1075Usuario_CargoUOCod, A1075Usuario_CargoUOCod});
            A1076Usuario_CargoUONom = T000121_A1076Usuario_CargoUONom[0];
            n1076Usuario_CargoUONom = T000121_n1076Usuario_CargoUONom[0];
            pr_default.close(19);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV26UnidadeOrganizacional_Codigo = A1075Usuario_CargoUOCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)));
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000122 */
            pr_default.execute(20, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Notifica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T000123 */
            pr_default.execute(21, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"+" ("+"Contagem Resultado QA_Para"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T000124 */
            pr_default.execute(22, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"+" ("+"Contagem Resultado QA_Usuario"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T000125 */
            pr_default.execute(23, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Auxiliar"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T000126 */
            pr_default.execute(24, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T180"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T000127 */
            pr_default.execute(25, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"+" ("+"Gestao_Usuario Gestor"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor T000128 */
            pr_default.execute(26, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"+" ("+"Gestao_Usuario"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor T000129 */
            pr_default.execute(27, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Custo do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor T000130 */
            pr_default.execute(28, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Destinatario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor T000131 */
            pr_default.execute(29, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Notifica��es da Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor T000132 */
            pr_default.execute(30, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor T000133 */
            pr_default.execute(31, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor T000134 */
            pr_default.execute(32, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Nota da OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor T000135 */
            pr_default.execute(33, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gestor do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor T000136 */
            pr_default.execute(34, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Importa��o Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor T000137 */
            pr_default.execute(35, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor T000138 */
            pr_default.execute(36, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicita��o de Mudan�a"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor T000139 */
            pr_default.execute(37, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Log de a��o dos Respons�veis"+" ("+"Log Responsavel_Usuario Owner"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor T000140 */
            pr_default.execute(38, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Log de a��o dos Respons�veis"+" ("+"Log Responsavel_Usuarios"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor T000141 */
            pr_default.execute(39, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor T000142 */
            pr_default.execute(40, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor T000143 */
            pr_default.execute(41, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Baseline"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor T000144 */
            pr_default.execute(42, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Erros nas confer�ncias de contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor T000145 */
            pr_default.execute(43, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Lote"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor T000146 */
            pr_default.execute(44, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor T000147 */
            pr_default.execute(45, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resultado_Usuario Responsavel"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor T000148 */
            pr_default.execute(46, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resultado_Usuario Contador Fab. de Soft"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
            /* Using cursor T000149 */
            pr_default.execute(47, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(47) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"+" ("+"Solicitacoes_Usuario Ult"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(47);
            /* Using cursor T000150 */
            pr_default.execute(48, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(48) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"+" ("+"ST_Solicitacoes_Usuario"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(48);
            /* Using cursor T000151 */
            pr_default.execute(49, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(49) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Arquivo a processar:"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(49);
            /* Using cursor T000152 */
            pr_default.execute(50, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(50) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Parecer"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(50);
            /* Using cursor T000153 */
            pr_default.execute(51, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(51) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(51);
            /* Using cursor T000154 */
            pr_default.execute(52, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(52) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(52);
            /* Using cursor T000155 */
            pr_default.execute(53, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(53) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(53);
            /* Using cursor T000156 */
            pr_default.execute(54, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(54) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Audit"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(54);
            /* Using cursor T000157 */
            pr_default.execute(55, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(55) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T237"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(55);
            /* Using cursor T000158 */
            pr_default.execute(56, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(56) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Linhas selecionadas numa grid"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(56);
            /* Using cursor T000159 */
            pr_default.execute(57, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            if ( (pr_default.getStatus(57) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario x Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(57);
         }
      }

      protected void UpdateTablesN1011( )
      {
         /* Using cursor T000160 */
         pr_default.execute(58, new Object[] {n2095Usuario_PessoaTelefone, A2095Usuario_PessoaTelefone, A57Usuario_PessoaCod});
         pr_default.close(58);
         dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
      }

      protected void EndLevel011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         pr_default.close(2);
         if ( AnyError == 0 )
         {
            BeforeComplete011( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.CommitDataStores( "Usuario");
            if ( AnyError == 0 )
            {
               ConfirmValues010( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.RollbackDataStores( "Usuario");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart011( )
      {
         /* Scan By routine */
         /* Using cursor T000161 */
         pr_default.execute(59);
         RcdFound1 = 0;
         if ( (pr_default.getStatus(59) != 101) )
         {
            RcdFound1 = 1;
            A1Usuario_Codigo = T000161_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            n1Usuario_Codigo = T000161_n1Usuario_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext011( )
      {
         /* Scan next routine */
         pr_default.readNext(59);
         RcdFound1 = 0;
         if ( (pr_default.getStatus(59) != 101) )
         {
            RcdFound1 = 1;
            A1Usuario_Codigo = T000161_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            n1Usuario_Codigo = T000161_n1Usuario_Codigo[0];
         }
      }

      protected void ScanEnd011( )
      {
         pr_default.close(59);
      }

      protected void AfterConfirm011( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert011( )
      {
         /* Before Insert Rules */
         AV15GamUser.save();
      }

      protected void BeforeUpdate011( )
      {
         /* Before Update Rules */
         AV15GamUser.save();
         new loadauditusuario(context ).execute(  "Y", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete011( )
      {
         /* Before Delete Rules */
         new loadauditusuario(context ).execute(  "Y", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete011( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditusuario(context ).execute(  "N", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditusuario(context ).execute(  "N", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate011( )
      {
         /* Before Validate Rules */
         AV15GamUser.gxTpr_Email = AV18Email;
      }

      protected void DisableAttributes011( )
      {
         dynavUnidadeorganizacional_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUnidadeorganizacional_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_codigo.Enabled), 5, 0)));
         edtUsuario_CargoNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_CargoNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_CargoNom_Enabled), 5, 0)));
         edtUsuario_Entidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Entidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Entidade_Enabled), 5, 0)));
         edtUsuario_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
         cmbUsuario_PessoaTip.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuario_PessoaTip_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbUsuario_PessoaTip.Enabled), 5, 0)));
         edtUsuario_PessoaDoc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaDoc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaDoc_Enabled), 5, 0)));
         edtavEmail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
         chkavContratanteusuario_ehfiscal.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratanteusuario_ehfiscal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratanteusuario_ehfiscal.Enabled), 5, 0)));
         edtavContratadausuario_cstuntprdnrm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdnrm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdnrm_Enabled), 5, 0)));
         edtavContratadausuario_cstuntprdext_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_cstuntprdext_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_cstuntprdext_Enabled), 5, 0)));
         edtUsuario_UserGamGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_UserGamGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_UserGamGuid_Enabled), 5, 0)));
         chkUsuario_EhContador.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContador.Enabled), 5, 0)));
         chkUsuario_EhAuditorFM.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhAuditorFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhAuditorFM.Enabled), 5, 0)));
         chkUsuario_EhFinanceiro.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhFinanceiro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhFinanceiro.Enabled), 5, 0)));
         chkUsuario_EhContratada.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContratada.Enabled), 5, 0)));
         chkUsuario_EhContratante.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhContratante.Enabled), 5, 0)));
         chkUsuario_EhPreposto.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhPreposto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_EhPreposto.Enabled), 5, 0)));
         cmbUsuario_Notificar.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuario_Notificar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbUsuario_Notificar.Enabled), 5, 0)));
         edtUsuario_Email_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Email_Enabled), 5, 0)));
         edtUsuario_PessoaTelefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaTelefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaTelefone_Enabled), 5, 0)));
         imgUsuario_Foto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgUsuario_Foto_Enabled), 5, 0)));
         edtUsuario_CrtfPath_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_CrtfPath_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_CrtfPath_Enabled), 5, 0)));
         chkUsuario_DeFerias.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_DeFerias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_DeFerias.Enabled), 5, 0)));
         chkUsuario_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUsuario_Ativo.Enabled), 5, 0)));
         chkavEhcontratada_flag.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEhcontratada_flag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavEhcontratada_flag.Enabled), 5, 0)));
         edtavEntidade_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEntidade_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_codigo_Enabled), 5, 0)));
         edtUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         edtUsuario_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues010( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216152657");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Usuario_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z341Usuario_UserGamGuid", StringUtil.RTrim( Z341Usuario_UserGamGuid));
         GxWebStd.gx_hidden_field( context, "Z2Usuario_Nome", StringUtil.RTrim( Z2Usuario_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "Z289Usuario_EhContador", Z289Usuario_EhContador);
         GxWebStd.gx_boolean_hidden_field( context, "Z291Usuario_EhContratada", Z291Usuario_EhContratada);
         GxWebStd.gx_boolean_hidden_field( context, "Z292Usuario_EhContratante", Z292Usuario_EhContratante);
         GxWebStd.gx_boolean_hidden_field( context, "Z293Usuario_EhFinanceiro", Z293Usuario_EhFinanceiro);
         GxWebStd.gx_boolean_hidden_field( context, "Z538Usuario_EhGestor", Z538Usuario_EhGestor);
         GxWebStd.gx_boolean_hidden_field( context, "Z1093Usuario_EhPreposto", Z1093Usuario_EhPreposto);
         GxWebStd.gx_hidden_field( context, "Z1017Usuario_CrtfPath", Z1017Usuario_CrtfPath);
         GxWebStd.gx_hidden_field( context, "Z1235Usuario_Notificar", StringUtil.RTrim( Z1235Usuario_Notificar));
         GxWebStd.gx_boolean_hidden_field( context, "Z54Usuario_Ativo", Z54Usuario_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1647Usuario_Email", Z1647Usuario_Email);
         GxWebStd.gx_boolean_hidden_field( context, "Z1908Usuario_DeFerias", Z1908Usuario_DeFerias);
         GxWebStd.gx_hidden_field( context, "Z2016Usuario_UltimaArea", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2016Usuario_UltimaArea), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z57Usuario_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1073Usuario_CargoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z58Usuario_PessoaNom", StringUtil.RTrim( Z58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "Z59Usuario_PessoaTip", StringUtil.RTrim( Z59Usuario_PessoaTip));
         GxWebStd.gx_hidden_field( context, "Z325Usuario_PessoaDoc", Z325Usuario_PessoaDoc);
         GxWebStd.gx_hidden_field( context, "Z2095Usuario_PessoaTelefone", StringUtil.RTrim( Z2095Usuario_PessoaTelefone));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_USUARIO_CARGOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25Insert_Usuario_CargoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_USUARIO_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_Usuario_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOUOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1075Usuario_CargoUOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_EHGESTOR", A538Usuario_EhGestor);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV28AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV28AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "USUARIO_NOME", StringUtil.RTrim( A2Usuario_Nome));
         GxWebStd.gx_hidden_field( context, "USUARIO_FOTO_GXI", A40000Usuario_Foto_GXI);
         GxWebStd.gx_hidden_field( context, "USUARIO_ULTIMAAREA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2016Usuario_UltimaArea), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOUONOM", StringUtil.RTrim( A1076Usuario_CargoUONom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV32Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuario_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "USUARIO_FOTO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A1716Usuario_Foto);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Usuario";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9");
         AV23EhContratada_Flag = StringUtil.StrToBool( StringUtil.BoolToStr( AV23EhContratada_Flag));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23EhContratada_Flag", AV23EhContratada_Flag);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( AV23EhContratada_Flag);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV24Entidade_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV32Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A538Usuario_EhGestor);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2016Usuario_UltimaArea), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuario_Codigo:"+context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuario_PessoaCod:"+context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"EhContratada_Flag:"+StringUtil.BoolToStr( AV23EhContratada_Flag));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Entidade_Codigo:"+context.localUtil.Format( (decimal)(AV24Entidade_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuario_CargoCod:"+context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV32Pgmname, "")));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuario_Nome:"+StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuario_EhGestor:"+StringUtil.BoolToStr( A538Usuario_EhGestor));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuario_UltimaArea:"+context.localUtil.Format( (decimal)(A2016Usuario_UltimaArea), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Usuario_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Usuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario" ;
      }

      protected void InitializeNonKey011( )
      {
         A1073Usuario_CargoCod = 0;
         n1073Usuario_CargoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
         A57Usuario_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         AV26UnidadeOrganizacional_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26UnidadeOrganizacional_Codigo), 6, 0)));
         AV28AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A290Usuario_EhAuditorFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
         A1083Usuario_Entidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
         A1074Usuario_CargoNom = "";
         n1074Usuario_CargoNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1074Usuario_CargoNom", A1074Usuario_CargoNom);
         A1075Usuario_CargoUOCod = 0;
         n1075Usuario_CargoUOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1075Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0)));
         A1076Usuario_CargoUONom = "";
         n1076Usuario_CargoUONom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         A59Usuario_PessoaTip = "";
         n59Usuario_PessoaTip = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59Usuario_PessoaTip", A59Usuario_PessoaTip);
         A325Usuario_PessoaDoc = "";
         n325Usuario_PessoaDoc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A325Usuario_PessoaDoc", A325Usuario_PessoaDoc);
         A2095Usuario_PessoaTelefone = "";
         n2095Usuario_PessoaTelefone = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2095Usuario_PessoaTelefone", A2095Usuario_PessoaTelefone);
         n2095Usuario_PessoaTelefone = (String.IsNullOrEmpty(StringUtil.RTrim( A2095Usuario_PessoaTelefone)) ? true : false);
         A341Usuario_UserGamGuid = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         A289Usuario_EhContador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A289Usuario_EhContador", A289Usuario_EhContador);
         A291Usuario_EhContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A291Usuario_EhContratada", A291Usuario_EhContratada);
         A292Usuario_EhContratante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A292Usuario_EhContratante", A292Usuario_EhContratante);
         A293Usuario_EhFinanceiro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A293Usuario_EhFinanceiro", A293Usuario_EhFinanceiro);
         A1017Usuario_CrtfPath = "";
         n1017Usuario_CrtfPath = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1017Usuario_CrtfPath", A1017Usuario_CrtfPath);
         n1017Usuario_CrtfPath = (String.IsNullOrEmpty(StringUtil.RTrim( A1017Usuario_CrtfPath)) ? true : false);
         A1647Usuario_Email = "";
         n1647Usuario_Email = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1647Usuario_Email", A1647Usuario_Email);
         n1647Usuario_Email = (String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) ? true : false);
         A1716Usuario_Foto = "";
         n1716Usuario_Foto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1716Usuario_Foto", A1716Usuario_Foto);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
         n1716Usuario_Foto = (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? true : false);
         A40000Usuario_Foto_GXI = "";
         n40000Usuario_Foto_GXI = false;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUsuario_Foto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1716Usuario_Foto)) ? A40000Usuario_Foto_GXI : context.convertURL( context.PathToRelativeUrl( A1716Usuario_Foto))));
         A2016Usuario_UltimaArea = 0;
         n2016Usuario_UltimaArea = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2016Usuario_UltimaArea", StringUtil.LTrim( StringUtil.Str( (decimal)(A2016Usuario_UltimaArea), 6, 0)));
         A538Usuario_EhGestor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A538Usuario_EhGestor", A538Usuario_EhGestor);
         A1093Usuario_EhPreposto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1093Usuario_EhPreposto", A1093Usuario_EhPreposto);
         A1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
         A54Usuario_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
         A1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
         Z341Usuario_UserGamGuid = "";
         Z2Usuario_Nome = "";
         Z289Usuario_EhContador = false;
         Z291Usuario_EhContratada = false;
         Z292Usuario_EhContratante = false;
         Z293Usuario_EhFinanceiro = false;
         Z538Usuario_EhGestor = false;
         Z1093Usuario_EhPreposto = false;
         Z1017Usuario_CrtfPath = "";
         Z1235Usuario_Notificar = "";
         Z54Usuario_Ativo = false;
         Z1647Usuario_Email = "";
         Z1908Usuario_DeFerias = false;
         Z2016Usuario_UltimaArea = 0;
         Z57Usuario_PessoaCod = 0;
         Z1073Usuario_CargoCod = 0;
         Z58Usuario_PessoaNom = "";
         Z59Usuario_PessoaTip = "";
         Z325Usuario_PessoaDoc = "";
         Z2095Usuario_PessoaTelefone = "";
      }

      protected void InitAll011( )
      {
         A1Usuario_Codigo = 0;
         n1Usuario_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         InitializeNonKey011( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1908Usuario_DeFerias = i1908Usuario_DeFerias;
         n1908Usuario_DeFerias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1908Usuario_DeFerias", A1908Usuario_DeFerias);
         A54Usuario_Ativo = i54Usuario_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
         A1235Usuario_Notificar = i1235Usuario_Notificar;
         n1235Usuario_Notificar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1235Usuario_Notificar", A1235Usuario_Notificar);
         A1093Usuario_EhPreposto = i1093Usuario_EhPreposto;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1093Usuario_EhPreposto", A1093Usuario_EhPreposto);
         A538Usuario_EhGestor = i538Usuario_EhGestor;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A538Usuario_EhGestor", A538Usuario_EhGestor);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216152731");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("usuario.js", "?20206216152732");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         tblTabletitle_Internalname = "TABLETITLE";
         lblTextblockunidadeorganizacional_codigo_Internalname = "TEXTBLOCKUNIDADEORGANIZACIONAL_CODIGO";
         dynavUnidadeorganizacional_codigo_Internalname = "vUNIDADEORGANIZACIONAL_CODIGO";
         imgSelectuo_Internalname = "SELECTUO";
         tblTablemergedunidadeorganizacional_codigo_Internalname = "TABLEMERGEDUNIDADEORGANIZACIONAL_CODIGO";
         lblTextblockusuario_cargonom_Internalname = "TEXTBLOCKUSUARIO_CARGONOM";
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM";
         lblTextblockusuario_entidade_Internalname = "TEXTBLOCKUSUARIO_ENTIDADE";
         edtUsuario_Entidade_Internalname = "USUARIO_ENTIDADE";
         lblTextblockusuario_pessoanom_Internalname = "TEXTBLOCKUSUARIO_PESSOANOM";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         lblTextblockusuario_pessoatip_Internalname = "TEXTBLOCKUSUARIO_PESSOATIP";
         cmbUsuario_PessoaTip_Internalname = "USUARIO_PESSOATIP";
         lblTextblockusuario_pessoadoc_Internalname = "TEXTBLOCKUSUARIO_PESSOADOC";
         edtUsuario_PessoaDoc_Internalname = "USUARIO_PESSOADOC";
         lblTextblockemail_Internalname = "TEXTBLOCKEMAIL";
         edtavEmail_Internalname = "vEMAIL";
         lblTextblockcontratanteusuario_ehfiscal_Internalname = "TEXTBLOCKCONTRATANTEUSUARIO_EHFISCAL";
         cellTextblockcontratanteusuario_ehfiscal_cell_Internalname = "TEXTBLOCKCONTRATANTEUSUARIO_EHFISCAL_CELL";
         chkavContratanteusuario_ehfiscal_Internalname = "vCONTRATANTEUSUARIO_EHFISCAL";
         cellContratanteusuario_ehfiscal_cell_Internalname = "CONTRATANTEUSUARIO_EHFISCAL_CELL";
         lblTextblockcontratadausuario_cstuntprdnrm_Internalname = "TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDNRM";
         cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname = "TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDNRM_CELL";
         edtavContratadausuario_cstuntprdnrm_Internalname = "vCONTRATADAUSUARIO_CSTUNTPRDNRM";
         cellContratadausuario_cstuntprdnrm_cell_Internalname = "CONTRATADAUSUARIO_CSTUNTPRDNRM_CELL";
         lblTextblockcontratadausuario_cstuntprdext_Internalname = "TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDEXT";
         cellTextblockcontratadausuario_cstuntprdext_cell_Internalname = "TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDEXT_CELL";
         edtavContratadausuario_cstuntprdext_Internalname = "vCONTRATADAUSUARIO_CSTUNTPRDEXT";
         cellContratadausuario_cstuntprdext_cell_Internalname = "CONTRATADAUSUARIO_CSTUNTPRDEXT_CELL";
         lblTextblockusuario_usergamguid_Internalname = "TEXTBLOCKUSUARIO_USERGAMGUID";
         cellTextblockusuario_usergamguid_cell_Internalname = "TEXTBLOCKUSUARIO_USERGAMGUID_CELL";
         edtUsuario_UserGamGuid_Internalname = "USUARIO_USERGAMGUID";
         cellUsuario_usergamguid_cell_Internalname = "USUARIO_USERGAMGUID_CELL";
         lblTextblockusuario_ehcontador_Internalname = "TEXTBLOCKUSUARIO_EHCONTADOR";
         cellTextblockusuario_ehcontador_cell_Internalname = "TEXTBLOCKUSUARIO_EHCONTADOR_CELL";
         chkUsuario_EhContador_Internalname = "USUARIO_EHCONTADOR";
         cellUsuario_ehcontador_cell_Internalname = "USUARIO_EHCONTADOR_CELL";
         lblTextblockusuario_ehauditorfm_Internalname = "TEXTBLOCKUSUARIO_EHAUDITORFM";
         cellTextblockusuario_ehauditorfm_cell_Internalname = "TEXTBLOCKUSUARIO_EHAUDITORFM_CELL";
         chkUsuario_EhAuditorFM_Internalname = "USUARIO_EHAUDITORFM";
         cellUsuario_ehauditorfm_cell_Internalname = "USUARIO_EHAUDITORFM_CELL";
         lblTextblockusuario_ehfinanceiro_Internalname = "TEXTBLOCKUSUARIO_EHFINANCEIRO";
         cellTextblockusuario_ehfinanceiro_cell_Internalname = "TEXTBLOCKUSUARIO_EHFINANCEIRO_CELL";
         chkUsuario_EhFinanceiro_Internalname = "USUARIO_EHFINANCEIRO";
         cellUsuario_ehfinanceiro_cell_Internalname = "USUARIO_EHFINANCEIRO_CELL";
         lblTextblockusuario_ehcontratada_Internalname = "TEXTBLOCKUSUARIO_EHCONTRATADA";
         cellTextblockusuario_ehcontratada_cell_Internalname = "TEXTBLOCKUSUARIO_EHCONTRATADA_CELL";
         chkUsuario_EhContratada_Internalname = "USUARIO_EHCONTRATADA";
         cellUsuario_ehcontratada_cell_Internalname = "USUARIO_EHCONTRATADA_CELL";
         lblTextblockusuario_ehcontratante_Internalname = "TEXTBLOCKUSUARIO_EHCONTRATANTE";
         cellTextblockusuario_ehcontratante_cell_Internalname = "TEXTBLOCKUSUARIO_EHCONTRATANTE_CELL";
         chkUsuario_EhContratante_Internalname = "USUARIO_EHCONTRATANTE";
         cellUsuario_ehcontratante_cell_Internalname = "USUARIO_EHCONTRATANTE_CELL";
         lblTextblockusuario_ehpreposto_Internalname = "TEXTBLOCKUSUARIO_EHPREPOSTO";
         cellTextblockusuario_ehpreposto_cell_Internalname = "TEXTBLOCKUSUARIO_EHPREPOSTO_CELL";
         chkUsuario_EhPreposto_Internalname = "USUARIO_EHPREPOSTO";
         cellUsuario_ehpreposto_cell_Internalname = "USUARIO_EHPREPOSTO_CELL";
         lblTextblockusuario_notificar_Internalname = "TEXTBLOCKUSUARIO_NOTIFICAR";
         cmbUsuario_Notificar_Internalname = "USUARIO_NOTIFICAR";
         lblTextblockusuario_email_Internalname = "TEXTBLOCKUSUARIO_EMAIL";
         edtUsuario_Email_Internalname = "USUARIO_EMAIL";
         lblTextblockusuario_pessoatelefone_Internalname = "TEXTBLOCKUSUARIO_PESSOATELEFONE";
         edtUsuario_PessoaTelefone_Internalname = "USUARIO_PESSOATELEFONE";
         lblTextblockusuario_foto_Internalname = "TEXTBLOCKUSUARIO_FOTO";
         imgUsuario_Foto_Internalname = "USUARIO_FOTO";
         lblTextblockusuario_crtfpath_Internalname = "TEXTBLOCKUSUARIO_CRTFPATH";
         edtUsuario_CrtfPath_Internalname = "USUARIO_CRTFPATH";
         lblTextblockusuario_deferias_Internalname = "TEXTBLOCKUSUARIO_DEFERIAS";
         chkUsuario_DeFerias_Internalname = "USUARIO_DEFERIAS";
         lblTextblockusuario_ativo_Internalname = "TEXTBLOCKUSUARIO_ATIVO";
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         chkavEhcontratada_flag_Internalname = "vEHCONTRATADA_FLAG";
         edtavEntidade_codigo_Internalname = "vENTIDADE_CODIGO";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Usu�rio";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Usuario";
         imgSelectuo_Visible = 1;
         dynavUnidadeorganizacional_codigo_Jsonclick = "";
         dynavUnidadeorganizacional_codigo.Enabled = 1;
         chkUsuario_Ativo.Enabled = 1;
         chkUsuario_Ativo.Visible = 1;
         lblTextblockusuario_ativo_Visible = 1;
         chkUsuario_DeFerias.Enabled = 1;
         edtUsuario_CrtfPath_Jsonclick = "";
         edtUsuario_CrtfPath_Enabled = 1;
         imgUsuario_Foto_Enabled = 1;
         edtUsuario_PessoaTelefone_Jsonclick = "";
         edtUsuario_PessoaTelefone_Enabled = 1;
         edtUsuario_Email_Jsonclick = "";
         edtUsuario_Email_Enabled = 1;
         cmbUsuario_Notificar_Jsonclick = "";
         cmbUsuario_Notificar.Enabled = 1;
         chkUsuario_EhPreposto.Enabled = 1;
         chkUsuario_EhPreposto.Visible = 1;
         cellUsuario_ehpreposto_cell_Class = "";
         cellTextblockusuario_ehpreposto_cell_Class = "";
         chkUsuario_EhContratante.Enabled = 1;
         chkUsuario_EhContratante.Visible = 1;
         cellUsuario_ehcontratante_cell_Class = "";
         cellTextblockusuario_ehcontratante_cell_Class = "";
         chkUsuario_EhContratada.Enabled = 1;
         chkUsuario_EhContratada.Visible = 1;
         cellUsuario_ehcontratada_cell_Class = "";
         cellTextblockusuario_ehcontratada_cell_Class = "";
         chkUsuario_EhFinanceiro.Enabled = 1;
         chkUsuario_EhFinanceiro.Visible = 1;
         cellUsuario_ehfinanceiro_cell_Class = "";
         cellTextblockusuario_ehfinanceiro_cell_Class = "";
         chkUsuario_EhAuditorFM.Enabled = 0;
         chkUsuario_EhAuditorFM.Visible = 1;
         cellUsuario_ehauditorfm_cell_Class = "";
         cellTextblockusuario_ehauditorfm_cell_Class = "";
         chkUsuario_EhContador.Enabled = 1;
         chkUsuario_EhContador.Visible = 1;
         cellUsuario_ehcontador_cell_Class = "";
         cellTextblockusuario_ehcontador_cell_Class = "";
         edtUsuario_UserGamGuid_Jsonclick = "";
         edtUsuario_UserGamGuid_Enabled = 1;
         edtUsuario_UserGamGuid_Visible = 1;
         cellUsuario_usergamguid_cell_Class = "";
         cellTextblockusuario_usergamguid_cell_Class = "";
         edtavContratadausuario_cstuntprdext_Jsonclick = "";
         edtavContratadausuario_cstuntprdext_Enabled = 1;
         edtavContratadausuario_cstuntprdext_Visible = 1;
         cellContratadausuario_cstuntprdext_cell_Class = "";
         cellTextblockcontratadausuario_cstuntprdext_cell_Class = "";
         edtavContratadausuario_cstuntprdnrm_Jsonclick = "";
         edtavContratadausuario_cstuntprdnrm_Enabled = 1;
         edtavContratadausuario_cstuntprdnrm_Visible = 1;
         cellContratadausuario_cstuntprdnrm_cell_Class = "";
         cellTextblockcontratadausuario_cstuntprdnrm_cell_Class = "";
         chkavContratanteusuario_ehfiscal.Enabled = 1;
         chkavContratanteusuario_ehfiscal.Visible = 1;
         cellContratanteusuario_ehfiscal_cell_Class = "";
         cellTextblockcontratanteusuario_ehfiscal_cell_Class = "";
         edtavEmail_Jsonclick = "";
         edtavEmail_Enabled = 1;
         edtUsuario_PessoaDoc_Jsonclick = "";
         edtUsuario_PessoaDoc_Enabled = 0;
         cmbUsuario_PessoaTip_Jsonclick = "";
         cmbUsuario_PessoaTip.Enabled = 0;
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_PessoaNom_Enabled = 0;
         edtUsuario_Entidade_Jsonclick = "";
         edtUsuario_Entidade_Enabled = 0;
         edtUsuario_CargoNom_Jsonclick = "";
         edtUsuario_CargoNom_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtUsuario_PessoaCod_Jsonclick = "";
         edtUsuario_PessoaCod_Enabled = 0;
         edtUsuario_PessoaCod_Visible = 1;
         edtUsuario_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Enabled = 0;
         edtUsuario_Codigo_Visible = 1;
         edtavEntidade_codigo_Jsonclick = "";
         edtavEntidade_codigo_Enabled = 0;
         edtavEntidade_codigo_Visible = 1;
         chkavEhcontratada_flag.Enabled = 0;
         chkavEhcontratada_flag.Visible = 1;
         chkavEhcontratada_flag.Caption = "";
         chkUsuario_Ativo.Caption = "";
         chkUsuario_DeFerias.Caption = "";
         chkUsuario_EhPreposto.Caption = "";
         chkUsuario_EhContratante.Caption = "";
         chkUsuario_EhContratada.Caption = "";
         chkUsuario_EhFinanceiro.Caption = "";
         chkUsuario_EhAuditorFM.Caption = "";
         chkUsuario_EhContador.Caption = "";
         chkavContratanteusuario_ehfiscal.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_CODIGO011( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_CODIGO_data011( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_CODIGO_html011( )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_CODIGO_data011( ) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_CODIGO_data011( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T000162 */
         pr_default.execute(60);
         while ( (pr_default.getStatus(60) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000162_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000162_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(60);
         }
         pr_default.close(60);
      }

      protected void GX4ASAUSUARIO_ENTIDADE011( int A1Usuario_Codigo )
      {
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A1083Usuario_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1083Usuario_Entidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX5ASAUSUARIO_EHAUDITORFM011( int A1Usuario_Codigo )
      {
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A290Usuario_EhAuditorFM = GXt_boolean2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A290Usuario_EhAuditorFM", A290Usuario_EhAuditorFM);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A290Usuario_EhAuditorFM))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_53_011( wwpbaseobjects.SdtAuditingObject AV28AuditingObject ,
                                int A1Usuario_Codigo ,
                                String Gx_mode )
      {
         new loadauditusuario(context ).execute(  "Y", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV28AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_54_011( wwpbaseobjects.SdtAuditingObject AV28AuditingObject ,
                                int A1Usuario_Codigo ,
                                String Gx_mode )
      {
         new loadauditusuario(context ).execute(  "Y", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV28AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_55_011( String Gx_mode ,
                                wwpbaseobjects.SdtAuditingObject AV28AuditingObject ,
                                int A1Usuario_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditusuario(context ).execute(  "N", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV28AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_56_011( String Gx_mode ,
                                wwpbaseobjects.SdtAuditingObject AV28AuditingObject ,
                                int A1Usuario_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditusuario(context ).execute(  "N", ref  AV28AuditingObject,  A1Usuario_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV28AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Usuario_codigo( int GX_Parm1 ,
                                        String GX_Parm2 ,
                                        bool GX_Parm3 )
      {
         A1Usuario_Codigo = GX_Parm1;
         n1Usuario_Codigo = false;
         A1083Usuario_Entidade = GX_Parm2;
         A290Usuario_EhAuditorFM = GX_Parm3;
         GXt_char1 = A1083Usuario_Entidade;
         new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
         A1083Usuario_Entidade = GXt_char1;
         GXt_boolean2 = A290Usuario_EhAuditorFM;
         new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
         A290Usuario_EhAuditorFM = GXt_boolean2;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( A1083Usuario_Entidade));
         isValidOutput.Add(A290Usuario_EhAuditorFM);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Usuario_pessoacod( int GX_Parm1 ,
                                           String GX_Parm2 ,
                                           GXCombobox cmbGX_Parm3 ,
                                           String GX_Parm4 ,
                                           String GX_Parm5 )
      {
         A57Usuario_PessoaCod = GX_Parm1;
         A58Usuario_PessoaNom = GX_Parm2;
         n58Usuario_PessoaNom = false;
         cmbUsuario_PessoaTip = cmbGX_Parm3;
         A59Usuario_PessoaTip = cmbUsuario_PessoaTip.CurrentValue;
         n59Usuario_PessoaTip = false;
         cmbUsuario_PessoaTip.CurrentValue = A59Usuario_PessoaTip;
         A325Usuario_PessoaDoc = GX_Parm4;
         n325Usuario_PessoaDoc = false;
         A2095Usuario_PessoaTelefone = GX_Parm5;
         n2095Usuario_PessoaTelefone = false;
         /* Using cursor T000163 */
         pr_default.execute(61, new Object[] {A57Usuario_PessoaCod});
         Z58Usuario_PessoaNom = T000163_A58Usuario_PessoaNom[0];
         Z59Usuario_PessoaTip = T000163_A59Usuario_PessoaTip[0];
         Z325Usuario_PessoaDoc = T000163_A325Usuario_PessoaDoc[0];
         Z2095Usuario_PessoaTelefone = T000163_A2095Usuario_PessoaTelefone[0];
         if ( (pr_default.getStatus(61) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = T000163_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = T000163_n58Usuario_PessoaNom[0];
         A59Usuario_PessoaTip = T000163_A59Usuario_PessoaTip[0];
         cmbUsuario_PessoaTip.CurrentValue = A59Usuario_PessoaTip;
         n59Usuario_PessoaTip = T000163_n59Usuario_PessoaTip[0];
         A325Usuario_PessoaDoc = T000163_A325Usuario_PessoaDoc[0];
         n325Usuario_PessoaDoc = T000163_n325Usuario_PessoaDoc[0];
         A2095Usuario_PessoaTelefone = T000163_A2095Usuario_PessoaTelefone[0];
         n2095Usuario_PessoaTelefone = T000163_n2095Usuario_PessoaTelefone[0];
         pr_default.close(61);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A58Usuario_PessoaNom = "";
            n58Usuario_PessoaNom = false;
            A59Usuario_PessoaTip = "";
            n59Usuario_PessoaTip = false;
            cmbUsuario_PessoaTip.CurrentValue = A59Usuario_PessoaTip;
            A325Usuario_PessoaDoc = "";
            n325Usuario_PessoaDoc = false;
            A2095Usuario_PessoaTelefone = "";
            n2095Usuario_PessoaTelefone = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A58Usuario_PessoaNom));
         cmbUsuario_PessoaTip.CurrentValue = A59Usuario_PessoaTip;
         isValidOutput.Add(cmbUsuario_PessoaTip);
         isValidOutput.Add(A325Usuario_PessoaDoc);
         isValidOutput.Add(StringUtil.RTrim( A2095Usuario_PessoaTelefone));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E13012',iparms:[{av:'AV28AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null},{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23EhContratada_Flag',fld:'vEHCONTRATADA_FLAG',pic:'',nv:false},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV29ContratanteUsuario_EhFiscal',fld:'vCONTRATANTEUSUARIO_EHFISCAL',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'AV18Email',fld:'vEMAIL',pic:'@!',nv:''},{av:'AV24Entidade_Codigo',fld:'vENTIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ContratadaUsuario_CstUntPrdNrm',fld:'vCONTRATADAUSUARIO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV21ContratadaUsuario_CstUntPrdExt',fld:'vCONTRATADAUSUARIO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV29ContratanteUsuario_EhFiscal',fld:'vCONTRATANTEUSUARIO_EHFISCAL',pic:'',nv:false},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV18Email',fld:'vEMAIL',pic:'@!',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'AV24Entidade_Codigo',fld:'vENTIDADE_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOSELECTUO'","{handler:'E11011',iparms:[{av:'AV26UnidadeOrganizacional_Codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26UnidadeOrganizacional_Codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(61);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z341Usuario_UserGamGuid = "";
         Z2Usuario_Nome = "";
         Z1017Usuario_CrtfPath = "";
         Z1235Usuario_Notificar = "";
         Z1647Usuario_Email = "";
         Z58Usuario_PessoaNom = "";
         Z59Usuario_PessoaTip = "";
         Z325Usuario_PessoaDoc = "";
         Z2095Usuario_PessoaTelefone = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A59Usuario_PessoaTip = "";
         A1235Usuario_Notificar = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         ClassString = "";
         StyleString = "";
         sStyleString = "";
         TempTags = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockunidadeorganizacional_codigo_Jsonclick = "";
         lblTextblockusuario_cargonom_Jsonclick = "";
         A1074Usuario_CargoNom = "";
         lblTextblockusuario_entidade_Jsonclick = "";
         A1083Usuario_Entidade = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         A58Usuario_PessoaNom = "";
         lblTextblockusuario_pessoatip_Jsonclick = "";
         lblTextblockusuario_pessoadoc_Jsonclick = "";
         A325Usuario_PessoaDoc = "";
         lblTextblockemail_Jsonclick = "";
         AV18Email = "";
         lblTextblockcontratanteusuario_ehfiscal_Jsonclick = "";
         lblTextblockcontratadausuario_cstuntprdnrm_Jsonclick = "";
         lblTextblockcontratadausuario_cstuntprdext_Jsonclick = "";
         lblTextblockusuario_usergamguid_Jsonclick = "";
         A341Usuario_UserGamGuid = "";
         lblTextblockusuario_ehcontador_Jsonclick = "";
         lblTextblockusuario_ehauditorfm_Jsonclick = "";
         lblTextblockusuario_ehfinanceiro_Jsonclick = "";
         lblTextblockusuario_ehcontratada_Jsonclick = "";
         lblTextblockusuario_ehcontratante_Jsonclick = "";
         lblTextblockusuario_ehpreposto_Jsonclick = "";
         lblTextblockusuario_notificar_Jsonclick = "";
         lblTextblockusuario_email_Jsonclick = "";
         A1647Usuario_Email = "";
         lblTextblockusuario_pessoatelefone_Jsonclick = "";
         A2095Usuario_PessoaTelefone = "";
         lblTextblockusuario_foto_Jsonclick = "";
         A1716Usuario_Foto = "";
         A40000Usuario_Foto_GXI = "";
         lblTextblockusuario_crtfpath_Jsonclick = "";
         A1017Usuario_CrtfPath = "";
         lblTextblockusuario_deferias_Jsonclick = "";
         lblTextblockusuario_ativo_Jsonclick = "";
         imgSelectuo_Jsonclick = "";
         A2Usuario_Nome = "";
         AV28AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A1076Usuario_CargoUONom = "";
         AV32Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode1 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV27Usuario_UserGamGuid = "";
         AV15GamUser = new SdtGAMUser(context);
         Gx_msg = "";
         AV22ContratadaUsuario = new SdtContratadaUsuario(context);
         Z1716Usuario_Foto = "";
         Z40000Usuario_Foto_GXI = "";
         Z1074Usuario_CargoNom = "";
         Z1076Usuario_CargoUONom = "";
         T00015_A58Usuario_PessoaNom = new String[] {""} ;
         T00015_n58Usuario_PessoaNom = new bool[] {false} ;
         T00015_A59Usuario_PessoaTip = new String[] {""} ;
         T00015_n59Usuario_PessoaTip = new bool[] {false} ;
         T00015_A325Usuario_PessoaDoc = new String[] {""} ;
         T00015_n325Usuario_PessoaDoc = new bool[] {false} ;
         T00015_A2095Usuario_PessoaTelefone = new String[] {""} ;
         T00015_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         T00016_A1074Usuario_CargoNom = new String[] {""} ;
         T00016_n1074Usuario_CargoNom = new bool[] {false} ;
         T00016_A1075Usuario_CargoUOCod = new int[1] ;
         T00016_n1075Usuario_CargoUOCod = new bool[] {false} ;
         T00017_A1076Usuario_CargoUONom = new String[] {""} ;
         T00017_n1076Usuario_CargoUONom = new bool[] {false} ;
         T00018_A1Usuario_Codigo = new int[1] ;
         T00018_n1Usuario_Codigo = new bool[] {false} ;
         T00018_A1074Usuario_CargoNom = new String[] {""} ;
         T00018_n1074Usuario_CargoNom = new bool[] {false} ;
         T00018_A1076Usuario_CargoUONom = new String[] {""} ;
         T00018_n1076Usuario_CargoUONom = new bool[] {false} ;
         T00018_A58Usuario_PessoaNom = new String[] {""} ;
         T00018_n58Usuario_PessoaNom = new bool[] {false} ;
         T00018_A59Usuario_PessoaTip = new String[] {""} ;
         T00018_n59Usuario_PessoaTip = new bool[] {false} ;
         T00018_A325Usuario_PessoaDoc = new String[] {""} ;
         T00018_n325Usuario_PessoaDoc = new bool[] {false} ;
         T00018_A2095Usuario_PessoaTelefone = new String[] {""} ;
         T00018_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         T00018_A341Usuario_UserGamGuid = new String[] {""} ;
         T00018_A2Usuario_Nome = new String[] {""} ;
         T00018_n2Usuario_Nome = new bool[] {false} ;
         T00018_A289Usuario_EhContador = new bool[] {false} ;
         T00018_A291Usuario_EhContratada = new bool[] {false} ;
         T00018_A292Usuario_EhContratante = new bool[] {false} ;
         T00018_A293Usuario_EhFinanceiro = new bool[] {false} ;
         T00018_A538Usuario_EhGestor = new bool[] {false} ;
         T00018_A1093Usuario_EhPreposto = new bool[] {false} ;
         T00018_A1017Usuario_CrtfPath = new String[] {""} ;
         T00018_n1017Usuario_CrtfPath = new bool[] {false} ;
         T00018_A1235Usuario_Notificar = new String[] {""} ;
         T00018_n1235Usuario_Notificar = new bool[] {false} ;
         T00018_A54Usuario_Ativo = new bool[] {false} ;
         T00018_A1647Usuario_Email = new String[] {""} ;
         T00018_n1647Usuario_Email = new bool[] {false} ;
         T00018_A40000Usuario_Foto_GXI = new String[] {""} ;
         T00018_n40000Usuario_Foto_GXI = new bool[] {false} ;
         T00018_A1908Usuario_DeFerias = new bool[] {false} ;
         T00018_n1908Usuario_DeFerias = new bool[] {false} ;
         T00018_A2016Usuario_UltimaArea = new int[1] ;
         T00018_n2016Usuario_UltimaArea = new bool[] {false} ;
         T00018_A57Usuario_PessoaCod = new int[1] ;
         T00018_A1073Usuario_CargoCod = new int[1] ;
         T00018_n1073Usuario_CargoCod = new bool[] {false} ;
         T00018_A1075Usuario_CargoUOCod = new int[1] ;
         T00018_n1075Usuario_CargoUOCod = new bool[] {false} ;
         T00018_A1716Usuario_Foto = new String[] {""} ;
         T00018_n1716Usuario_Foto = new bool[] {false} ;
         T00019_A58Usuario_PessoaNom = new String[] {""} ;
         T00019_n58Usuario_PessoaNom = new bool[] {false} ;
         T00019_A59Usuario_PessoaTip = new String[] {""} ;
         T00019_n59Usuario_PessoaTip = new bool[] {false} ;
         T00019_A325Usuario_PessoaDoc = new String[] {""} ;
         T00019_n325Usuario_PessoaDoc = new bool[] {false} ;
         T00019_A2095Usuario_PessoaTelefone = new String[] {""} ;
         T00019_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         T000110_A1074Usuario_CargoNom = new String[] {""} ;
         T000110_n1074Usuario_CargoNom = new bool[] {false} ;
         T000110_A1075Usuario_CargoUOCod = new int[1] ;
         T000110_n1075Usuario_CargoUOCod = new bool[] {false} ;
         T000111_A1076Usuario_CargoUONom = new String[] {""} ;
         T000111_n1076Usuario_CargoUONom = new bool[] {false} ;
         T000112_A1Usuario_Codigo = new int[1] ;
         T000112_n1Usuario_Codigo = new bool[] {false} ;
         T00013_A1Usuario_Codigo = new int[1] ;
         T00013_n1Usuario_Codigo = new bool[] {false} ;
         T00013_A341Usuario_UserGamGuid = new String[] {""} ;
         T00013_A2Usuario_Nome = new String[] {""} ;
         T00013_n2Usuario_Nome = new bool[] {false} ;
         T00013_A289Usuario_EhContador = new bool[] {false} ;
         T00013_A291Usuario_EhContratada = new bool[] {false} ;
         T00013_A292Usuario_EhContratante = new bool[] {false} ;
         T00013_A293Usuario_EhFinanceiro = new bool[] {false} ;
         T00013_A538Usuario_EhGestor = new bool[] {false} ;
         T00013_A1093Usuario_EhPreposto = new bool[] {false} ;
         T00013_A1017Usuario_CrtfPath = new String[] {""} ;
         T00013_n1017Usuario_CrtfPath = new bool[] {false} ;
         T00013_A1235Usuario_Notificar = new String[] {""} ;
         T00013_n1235Usuario_Notificar = new bool[] {false} ;
         T00013_A54Usuario_Ativo = new bool[] {false} ;
         T00013_A1647Usuario_Email = new String[] {""} ;
         T00013_n1647Usuario_Email = new bool[] {false} ;
         T00013_A40000Usuario_Foto_GXI = new String[] {""} ;
         T00013_n40000Usuario_Foto_GXI = new bool[] {false} ;
         T00013_A1908Usuario_DeFerias = new bool[] {false} ;
         T00013_n1908Usuario_DeFerias = new bool[] {false} ;
         T00013_A2016Usuario_UltimaArea = new int[1] ;
         T00013_n2016Usuario_UltimaArea = new bool[] {false} ;
         T00013_A57Usuario_PessoaCod = new int[1] ;
         T00013_A1073Usuario_CargoCod = new int[1] ;
         T00013_n1073Usuario_CargoCod = new bool[] {false} ;
         T00013_A1716Usuario_Foto = new String[] {""} ;
         T00013_n1716Usuario_Foto = new bool[] {false} ;
         T000113_A1Usuario_Codigo = new int[1] ;
         T000113_n1Usuario_Codigo = new bool[] {false} ;
         T000114_A1Usuario_Codigo = new int[1] ;
         T000114_n1Usuario_Codigo = new bool[] {false} ;
         T00012_A1Usuario_Codigo = new int[1] ;
         T00012_n1Usuario_Codigo = new bool[] {false} ;
         T00012_A341Usuario_UserGamGuid = new String[] {""} ;
         T00012_A2Usuario_Nome = new String[] {""} ;
         T00012_n2Usuario_Nome = new bool[] {false} ;
         T00012_A289Usuario_EhContador = new bool[] {false} ;
         T00012_A291Usuario_EhContratada = new bool[] {false} ;
         T00012_A292Usuario_EhContratante = new bool[] {false} ;
         T00012_A293Usuario_EhFinanceiro = new bool[] {false} ;
         T00012_A538Usuario_EhGestor = new bool[] {false} ;
         T00012_A1093Usuario_EhPreposto = new bool[] {false} ;
         T00012_A1017Usuario_CrtfPath = new String[] {""} ;
         T00012_n1017Usuario_CrtfPath = new bool[] {false} ;
         T00012_A1235Usuario_Notificar = new String[] {""} ;
         T00012_n1235Usuario_Notificar = new bool[] {false} ;
         T00012_A54Usuario_Ativo = new bool[] {false} ;
         T00012_A1647Usuario_Email = new String[] {""} ;
         T00012_n1647Usuario_Email = new bool[] {false} ;
         T00012_A40000Usuario_Foto_GXI = new String[] {""} ;
         T00012_n40000Usuario_Foto_GXI = new bool[] {false} ;
         T00012_A1908Usuario_DeFerias = new bool[] {false} ;
         T00012_n1908Usuario_DeFerias = new bool[] {false} ;
         T00012_A2016Usuario_UltimaArea = new int[1] ;
         T00012_n2016Usuario_UltimaArea = new bool[] {false} ;
         T00012_A57Usuario_PessoaCod = new int[1] ;
         T00012_A1073Usuario_CargoCod = new int[1] ;
         T00012_n1073Usuario_CargoCod = new bool[] {false} ;
         T00012_A1716Usuario_Foto = new String[] {""} ;
         T00012_n1716Usuario_Foto = new bool[] {false} ;
         T00014_A58Usuario_PessoaNom = new String[] {""} ;
         T00014_n58Usuario_PessoaNom = new bool[] {false} ;
         T00014_A59Usuario_PessoaTip = new String[] {""} ;
         T00014_n59Usuario_PessoaTip = new bool[] {false} ;
         T00014_A325Usuario_PessoaDoc = new String[] {""} ;
         T00014_n325Usuario_PessoaDoc = new bool[] {false} ;
         T00014_A2095Usuario_PessoaTelefone = new String[] {""} ;
         T00014_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         T000115_A1Usuario_Codigo = new int[1] ;
         T000115_n1Usuario_Codigo = new bool[] {false} ;
         T000119_A58Usuario_PessoaNom = new String[] {""} ;
         T000119_n58Usuario_PessoaNom = new bool[] {false} ;
         T000119_A59Usuario_PessoaTip = new String[] {""} ;
         T000119_n59Usuario_PessoaTip = new bool[] {false} ;
         T000119_A325Usuario_PessoaDoc = new String[] {""} ;
         T000119_n325Usuario_PessoaDoc = new bool[] {false} ;
         T000119_A2095Usuario_PessoaTelefone = new String[] {""} ;
         T000119_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         T000120_A1074Usuario_CargoNom = new String[] {""} ;
         T000120_n1074Usuario_CargoNom = new bool[] {false} ;
         T000120_A1075Usuario_CargoUOCod = new int[1] ;
         T000120_n1075Usuario_CargoUOCod = new bool[] {false} ;
         T000121_A1076Usuario_CargoUONom = new String[] {""} ;
         T000121_n1076Usuario_CargoUONom = new bool[] {false} ;
         T000122_A2077UsuarioNotifica_Codigo = new int[1] ;
         T000123_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T000124_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T000125_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         T000125_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         T000126_A1562HistoricoConsumo_Codigo = new int[1] ;
         T000127_A1482Gestao_Codigo = new int[1] ;
         T000128_A1482Gestao_Codigo = new int[1] ;
         T000129_A1473ContratoServicosCusto_Codigo = new int[1] ;
         T000130_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T000130_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         T000131_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T000132_A127Sistema_Codigo = new int[1] ;
         T000133_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T000133_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         T000134_A1331ContagemResultadoNota_Codigo = new int[1] ;
         T000135_A1078ContratoGestor_ContratoCod = new int[1] ;
         T000135_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T000136_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T000137_A74Contrato_Codigo = new int[1] ;
         T000138_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T000139_A1797LogResponsavel_Codigo = new long[1] ;
         T000140_A1797LogResponsavel_Codigo = new long[1] ;
         T000141_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T000141_A829UsuarioServicos_ServicoCod = new int[1] ;
         T000142_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T000143_A722Baseline_Codigo = new int[1] ;
         T000144_A456ContagemResultado_Codigo = new int[1] ;
         T000144_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T000145_A596Lote_Codigo = new int[1] ;
         T000146_A456ContagemResultado_Codigo = new int[1] ;
         T000146_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T000146_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T000147_A456ContagemResultado_Codigo = new int[1] ;
         T000148_A456ContagemResultado_Codigo = new int[1] ;
         T000149_A439Solicitacoes_Codigo = new int[1] ;
         T000150_A439Solicitacoes_Codigo = new int[1] ;
         T000151_A435File_UsuarioCod = new int[1] ;
         T000151_A504File_Row = new int[1] ;
         T000152_A243ContagemItemParecer_Codigo = new int[1] ;
         T000153_A192Contagem_Codigo = new int[1] ;
         T000154_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000154_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000155_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000155_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000156_A1430AuditId = new long[1] ;
         T000157_A2175Equipe_Codigo = new int[1] ;
         T000157_A1Usuario_Codigo = new int[1] ;
         T000157_n1Usuario_Codigo = new bool[] {false} ;
         T000158_A1Usuario_Codigo = new int[1] ;
         T000158_n1Usuario_Codigo = new bool[] {false} ;
         T000158_A487Selected_Codigo = new int[1] ;
         T000158_A488Selected_Flag = new bool[] {false} ;
         T000159_A1Usuario_Codigo = new int[1] ;
         T000159_n1Usuario_Codigo = new bool[] {false} ;
         T000159_A3Perfil_Codigo = new int[1] ;
         T000161_A1Usuario_Codigo = new int[1] ;
         T000161_n1Usuario_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         i1235Usuario_Notificar = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000162_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T000162_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T000162_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         GXt_char1 = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000163_A58Usuario_PessoaNom = new String[] {""} ;
         T000163_n58Usuario_PessoaNom = new bool[] {false} ;
         T000163_A59Usuario_PessoaTip = new String[] {""} ;
         T000163_n59Usuario_PessoaTip = new bool[] {false} ;
         T000163_A325Usuario_PessoaDoc = new String[] {""} ;
         T000163_n325Usuario_PessoaDoc = new bool[] {false} ;
         T000163_A2095Usuario_PessoaTelefone = new String[] {""} ;
         T000163_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuario__default(),
            new Object[][] {
                new Object[] {
               T00012_A1Usuario_Codigo, T00012_A341Usuario_UserGamGuid, T00012_A2Usuario_Nome, T00012_n2Usuario_Nome, T00012_A289Usuario_EhContador, T00012_A291Usuario_EhContratada, T00012_A292Usuario_EhContratante, T00012_A293Usuario_EhFinanceiro, T00012_A538Usuario_EhGestor, T00012_A1093Usuario_EhPreposto,
               T00012_A1017Usuario_CrtfPath, T00012_n1017Usuario_CrtfPath, T00012_A1235Usuario_Notificar, T00012_n1235Usuario_Notificar, T00012_A54Usuario_Ativo, T00012_A1647Usuario_Email, T00012_n1647Usuario_Email, T00012_A40000Usuario_Foto_GXI, T00012_n40000Usuario_Foto_GXI, T00012_A1908Usuario_DeFerias,
               T00012_n1908Usuario_DeFerias, T00012_A2016Usuario_UltimaArea, T00012_n2016Usuario_UltimaArea, T00012_A57Usuario_PessoaCod, T00012_A1073Usuario_CargoCod, T00012_n1073Usuario_CargoCod, T00012_A1716Usuario_Foto, T00012_n1716Usuario_Foto
               }
               , new Object[] {
               T00013_A1Usuario_Codigo, T00013_A341Usuario_UserGamGuid, T00013_A2Usuario_Nome, T00013_n2Usuario_Nome, T00013_A289Usuario_EhContador, T00013_A291Usuario_EhContratada, T00013_A292Usuario_EhContratante, T00013_A293Usuario_EhFinanceiro, T00013_A538Usuario_EhGestor, T00013_A1093Usuario_EhPreposto,
               T00013_A1017Usuario_CrtfPath, T00013_n1017Usuario_CrtfPath, T00013_A1235Usuario_Notificar, T00013_n1235Usuario_Notificar, T00013_A54Usuario_Ativo, T00013_A1647Usuario_Email, T00013_n1647Usuario_Email, T00013_A40000Usuario_Foto_GXI, T00013_n40000Usuario_Foto_GXI, T00013_A1908Usuario_DeFerias,
               T00013_n1908Usuario_DeFerias, T00013_A2016Usuario_UltimaArea, T00013_n2016Usuario_UltimaArea, T00013_A57Usuario_PessoaCod, T00013_A1073Usuario_CargoCod, T00013_n1073Usuario_CargoCod, T00013_A1716Usuario_Foto, T00013_n1716Usuario_Foto
               }
               , new Object[] {
               T00014_A58Usuario_PessoaNom, T00014_n58Usuario_PessoaNom, T00014_A59Usuario_PessoaTip, T00014_n59Usuario_PessoaTip, T00014_A325Usuario_PessoaDoc, T00014_n325Usuario_PessoaDoc, T00014_A2095Usuario_PessoaTelefone, T00014_n2095Usuario_PessoaTelefone
               }
               , new Object[] {
               T00015_A58Usuario_PessoaNom, T00015_n58Usuario_PessoaNom, T00015_A59Usuario_PessoaTip, T00015_n59Usuario_PessoaTip, T00015_A325Usuario_PessoaDoc, T00015_n325Usuario_PessoaDoc, T00015_A2095Usuario_PessoaTelefone, T00015_n2095Usuario_PessoaTelefone
               }
               , new Object[] {
               T00016_A1074Usuario_CargoNom, T00016_n1074Usuario_CargoNom, T00016_A1075Usuario_CargoUOCod, T00016_n1075Usuario_CargoUOCod
               }
               , new Object[] {
               T00017_A1076Usuario_CargoUONom, T00017_n1076Usuario_CargoUONom
               }
               , new Object[] {
               T00018_A1Usuario_Codigo, T00018_A1074Usuario_CargoNom, T00018_n1074Usuario_CargoNom, T00018_A1076Usuario_CargoUONom, T00018_n1076Usuario_CargoUONom, T00018_A58Usuario_PessoaNom, T00018_n58Usuario_PessoaNom, T00018_A59Usuario_PessoaTip, T00018_n59Usuario_PessoaTip, T00018_A325Usuario_PessoaDoc,
               T00018_n325Usuario_PessoaDoc, T00018_A2095Usuario_PessoaTelefone, T00018_n2095Usuario_PessoaTelefone, T00018_A341Usuario_UserGamGuid, T00018_A2Usuario_Nome, T00018_n2Usuario_Nome, T00018_A289Usuario_EhContador, T00018_A291Usuario_EhContratada, T00018_A292Usuario_EhContratante, T00018_A293Usuario_EhFinanceiro,
               T00018_A538Usuario_EhGestor, T00018_A1093Usuario_EhPreposto, T00018_A1017Usuario_CrtfPath, T00018_n1017Usuario_CrtfPath, T00018_A1235Usuario_Notificar, T00018_n1235Usuario_Notificar, T00018_A54Usuario_Ativo, T00018_A1647Usuario_Email, T00018_n1647Usuario_Email, T00018_A40000Usuario_Foto_GXI,
               T00018_n40000Usuario_Foto_GXI, T00018_A1908Usuario_DeFerias, T00018_n1908Usuario_DeFerias, T00018_A2016Usuario_UltimaArea, T00018_n2016Usuario_UltimaArea, T00018_A57Usuario_PessoaCod, T00018_A1073Usuario_CargoCod, T00018_n1073Usuario_CargoCod, T00018_A1075Usuario_CargoUOCod, T00018_n1075Usuario_CargoUOCod,
               T00018_A1716Usuario_Foto, T00018_n1716Usuario_Foto
               }
               , new Object[] {
               T00019_A58Usuario_PessoaNom, T00019_n58Usuario_PessoaNom, T00019_A59Usuario_PessoaTip, T00019_n59Usuario_PessoaTip, T00019_A325Usuario_PessoaDoc, T00019_n325Usuario_PessoaDoc, T00019_A2095Usuario_PessoaTelefone, T00019_n2095Usuario_PessoaTelefone
               }
               , new Object[] {
               T000110_A1074Usuario_CargoNom, T000110_n1074Usuario_CargoNom, T000110_A1075Usuario_CargoUOCod, T000110_n1075Usuario_CargoUOCod
               }
               , new Object[] {
               T000111_A1076Usuario_CargoUONom, T000111_n1076Usuario_CargoUONom
               }
               , new Object[] {
               T000112_A1Usuario_Codigo
               }
               , new Object[] {
               T000113_A1Usuario_Codigo
               }
               , new Object[] {
               T000114_A1Usuario_Codigo
               }
               , new Object[] {
               T000115_A1Usuario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000119_A58Usuario_PessoaNom, T000119_n58Usuario_PessoaNom, T000119_A59Usuario_PessoaTip, T000119_n59Usuario_PessoaTip, T000119_A325Usuario_PessoaDoc, T000119_n325Usuario_PessoaDoc, T000119_A2095Usuario_PessoaTelefone, T000119_n2095Usuario_PessoaTelefone
               }
               , new Object[] {
               T000120_A1074Usuario_CargoNom, T000120_n1074Usuario_CargoNom, T000120_A1075Usuario_CargoUOCod, T000120_n1075Usuario_CargoUOCod
               }
               , new Object[] {
               T000121_A1076Usuario_CargoUONom, T000121_n1076Usuario_CargoUONom
               }
               , new Object[] {
               T000122_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               T000123_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T000124_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T000125_A1824ContratoAuxiliar_ContratoCod, T000125_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               T000126_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               T000127_A1482Gestao_Codigo
               }
               , new Object[] {
               T000128_A1482Gestao_Codigo
               }
               , new Object[] {
               T000129_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               T000130_A1412ContagemResultadoNotificacao_Codigo, T000130_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               T000131_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               T000132_A127Sistema_Codigo
               }
               , new Object[] {
               T000133_A1033ContagemResultadoLiqLog_Codigo, T000133_A1370ContagemResultadoLiqLogOS_Codigo
               }
               , new Object[] {
               T000134_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               T000135_A1078ContratoGestor_ContratoCod, T000135_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               T000136_A1041ContagemResultadoImpLog_Codigo
               }
               , new Object[] {
               T000137_A74Contrato_Codigo
               }
               , new Object[] {
               T000138_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               T000139_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T000140_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T000141_A828UsuarioServicos_UsuarioCod, T000141_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T000142_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T000143_A722Baseline_Codigo
               }
               , new Object[] {
               T000144_A456ContagemResultado_Codigo, T000144_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               T000145_A596Lote_Codigo
               }
               , new Object[] {
               T000146_A456ContagemResultado_Codigo, T000146_A473ContagemResultado_DataCnt, T000146_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               T000147_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000148_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000149_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T000150_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T000151_A435File_UsuarioCod, T000151_A504File_Row
               }
               , new Object[] {
               T000152_A243ContagemItemParecer_Codigo
               }
               , new Object[] {
               T000153_A192Contagem_Codigo
               }
               , new Object[] {
               T000154_A66ContratadaUsuario_ContratadaCod, T000154_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               T000155_A63ContratanteUsuario_ContratanteCod, T000155_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               T000156_A1430AuditId
               }
               , new Object[] {
               T000157_A2175Equipe_Codigo, T000157_A1Usuario_Codigo
               }
               , new Object[] {
               T000158_A1Usuario_Codigo, T000158_A487Selected_Codigo, T000158_A488Selected_Flag
               }
               , new Object[] {
               T000159_A1Usuario_Codigo, T000159_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               T000161_A1Usuario_Codigo
               }
               , new Object[] {
               T000162_A611UnidadeOrganizacional_Codigo, T000162_A612UnidadeOrganizacional_Nome, T000162_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               T000163_A58Usuario_PessoaNom, T000163_n58Usuario_PessoaNom, T000163_A59Usuario_PessoaTip, T000163_n59Usuario_PessoaTip, T000163_A325Usuario_PessoaDoc, T000163_n325Usuario_PessoaDoc, T000163_A2095Usuario_PessoaTelefone, T000163_n2095Usuario_PessoaTelefone
               }
            }
         );
         Z1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         A1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         i1908Usuario_DeFerias = false;
         n1908Usuario_DeFerias = false;
         Z54Usuario_Ativo = true;
         A54Usuario_Ativo = true;
         i54Usuario_Ativo = true;
         Z1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         A1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         i1235Usuario_Notificar = "A";
         n1235Usuario_Notificar = false;
         Z1093Usuario_EhPreposto = false;
         A1093Usuario_EhPreposto = false;
         i1093Usuario_EhPreposto = false;
         Z538Usuario_EhGestor = false;
         A538Usuario_EhGestor = false;
         i538Usuario_EhGestor = false;
         AV32Pgmname = "Usuario";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound1 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Usuario_Codigo ;
      private int Z1Usuario_Codigo ;
      private int Z2016Usuario_UltimaArea ;
      private int Z57Usuario_PessoaCod ;
      private int Z1073Usuario_CargoCod ;
      private int N1073Usuario_CargoCod ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A1073Usuario_CargoCod ;
      private int A1075Usuario_CargoUOCod ;
      private int AV7Usuario_Codigo ;
      private int trnEnded ;
      private int AV26UnidadeOrganizacional_Codigo ;
      private int AV24Entidade_Codigo ;
      private int edtavEntidade_codigo_Enabled ;
      private int edtavEntidade_codigo_Visible ;
      private int edtUsuario_Codigo_Enabled ;
      private int edtUsuario_Codigo_Visible ;
      private int edtUsuario_PessoaCod_Enabled ;
      private int edtUsuario_PessoaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtUsuario_CargoNom_Enabled ;
      private int edtUsuario_Entidade_Enabled ;
      private int edtUsuario_PessoaNom_Enabled ;
      private int edtUsuario_PessoaDoc_Enabled ;
      private int edtavEmail_Enabled ;
      private int edtavContratadausuario_cstuntprdnrm_Enabled ;
      private int edtavContratadausuario_cstuntprdnrm_Visible ;
      private int edtavContratadausuario_cstuntprdext_Enabled ;
      private int edtavContratadausuario_cstuntprdext_Visible ;
      private int edtUsuario_UserGamGuid_Visible ;
      private int edtUsuario_UserGamGuid_Enabled ;
      private int edtUsuario_Email_Enabled ;
      private int edtUsuario_PessoaTelefone_Enabled ;
      private int imgUsuario_Foto_Enabled ;
      private int edtUsuario_CrtfPath_Enabled ;
      private int lblTextblockusuario_ativo_Visible ;
      private int imgSelectuo_Visible ;
      private int A2016Usuario_UltimaArea ;
      private int AV25Insert_Usuario_CargoCod ;
      private int AV14Insert_Usuario_PessoaCod ;
      private int AV33GXV1 ;
      private int Z1075Usuario_CargoUOCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal AV20ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV21ContratadaUsuario_CstUntPrdExt ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z341Usuario_UserGamGuid ;
      private String Z2Usuario_Nome ;
      private String Z1235Usuario_Notificar ;
      private String Z58Usuario_PessoaNom ;
      private String Z59Usuario_PessoaTip ;
      private String Z2095Usuario_PessoaTelefone ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A59Usuario_PessoaTip ;
      private String chkavContratanteusuario_ehfiscal_Internalname ;
      private String chkUsuario_EhContador_Internalname ;
      private String chkUsuario_EhAuditorFM_Internalname ;
      private String chkUsuario_EhFinanceiro_Internalname ;
      private String chkUsuario_EhContratada_Internalname ;
      private String chkUsuario_EhContratante_Internalname ;
      private String chkUsuario_EhPreposto_Internalname ;
      private String A1235Usuario_Notificar ;
      private String chkUsuario_DeFerias_Internalname ;
      private String chkUsuario_Ativo_Internalname ;
      private String chkavEhcontratada_flag_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtUsuario_UserGamGuid_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String edtavEntidade_codigo_Internalname ;
      private String edtavEntidade_codigo_Jsonclick ;
      private String edtUsuario_Codigo_Internalname ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockunidadeorganizacional_codigo_Internalname ;
      private String lblTextblockunidadeorganizacional_codigo_Jsonclick ;
      private String lblTextblockusuario_cargonom_Internalname ;
      private String lblTextblockusuario_cargonom_Jsonclick ;
      private String edtUsuario_CargoNom_Internalname ;
      private String edtUsuario_CargoNom_Jsonclick ;
      private String lblTextblockusuario_entidade_Internalname ;
      private String lblTextblockusuario_entidade_Jsonclick ;
      private String edtUsuario_Entidade_Internalname ;
      private String A1083Usuario_Entidade ;
      private String edtUsuario_Entidade_Jsonclick ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String lblTextblockusuario_pessoatip_Internalname ;
      private String lblTextblockusuario_pessoatip_Jsonclick ;
      private String cmbUsuario_PessoaTip_Internalname ;
      private String cmbUsuario_PessoaTip_Jsonclick ;
      private String lblTextblockusuario_pessoadoc_Internalname ;
      private String lblTextblockusuario_pessoadoc_Jsonclick ;
      private String edtUsuario_PessoaDoc_Internalname ;
      private String edtUsuario_PessoaDoc_Jsonclick ;
      private String lblTextblockemail_Internalname ;
      private String lblTextblockemail_Jsonclick ;
      private String edtavEmail_Internalname ;
      private String edtavEmail_Jsonclick ;
      private String cellTextblockcontratanteusuario_ehfiscal_cell_Internalname ;
      private String cellTextblockcontratanteusuario_ehfiscal_cell_Class ;
      private String lblTextblockcontratanteusuario_ehfiscal_Internalname ;
      private String lblTextblockcontratanteusuario_ehfiscal_Jsonclick ;
      private String cellContratanteusuario_ehfiscal_cell_Internalname ;
      private String cellContratanteusuario_ehfiscal_cell_Class ;
      private String cellTextblockcontratadausuario_cstuntprdnrm_cell_Internalname ;
      private String cellTextblockcontratadausuario_cstuntprdnrm_cell_Class ;
      private String lblTextblockcontratadausuario_cstuntprdnrm_Internalname ;
      private String lblTextblockcontratadausuario_cstuntprdnrm_Jsonclick ;
      private String cellContratadausuario_cstuntprdnrm_cell_Internalname ;
      private String cellContratadausuario_cstuntprdnrm_cell_Class ;
      private String edtavContratadausuario_cstuntprdnrm_Internalname ;
      private String edtavContratadausuario_cstuntprdnrm_Jsonclick ;
      private String cellTextblockcontratadausuario_cstuntprdext_cell_Internalname ;
      private String cellTextblockcontratadausuario_cstuntprdext_cell_Class ;
      private String lblTextblockcontratadausuario_cstuntprdext_Internalname ;
      private String lblTextblockcontratadausuario_cstuntprdext_Jsonclick ;
      private String cellContratadausuario_cstuntprdext_cell_Internalname ;
      private String cellContratadausuario_cstuntprdext_cell_Class ;
      private String edtavContratadausuario_cstuntprdext_Internalname ;
      private String edtavContratadausuario_cstuntprdext_Jsonclick ;
      private String cellTextblockusuario_usergamguid_cell_Internalname ;
      private String cellTextblockusuario_usergamguid_cell_Class ;
      private String lblTextblockusuario_usergamguid_Internalname ;
      private String lblTextblockusuario_usergamguid_Jsonclick ;
      private String cellUsuario_usergamguid_cell_Internalname ;
      private String cellUsuario_usergamguid_cell_Class ;
      private String A341Usuario_UserGamGuid ;
      private String edtUsuario_UserGamGuid_Jsonclick ;
      private String cellTextblockusuario_ehcontador_cell_Internalname ;
      private String cellTextblockusuario_ehcontador_cell_Class ;
      private String lblTextblockusuario_ehcontador_Internalname ;
      private String lblTextblockusuario_ehcontador_Jsonclick ;
      private String cellUsuario_ehcontador_cell_Internalname ;
      private String cellUsuario_ehcontador_cell_Class ;
      private String cellTextblockusuario_ehauditorfm_cell_Internalname ;
      private String cellTextblockusuario_ehauditorfm_cell_Class ;
      private String lblTextblockusuario_ehauditorfm_Internalname ;
      private String lblTextblockusuario_ehauditorfm_Jsonclick ;
      private String cellUsuario_ehauditorfm_cell_Internalname ;
      private String cellUsuario_ehauditorfm_cell_Class ;
      private String cellTextblockusuario_ehfinanceiro_cell_Internalname ;
      private String cellTextblockusuario_ehfinanceiro_cell_Class ;
      private String lblTextblockusuario_ehfinanceiro_Internalname ;
      private String lblTextblockusuario_ehfinanceiro_Jsonclick ;
      private String cellUsuario_ehfinanceiro_cell_Internalname ;
      private String cellUsuario_ehfinanceiro_cell_Class ;
      private String cellTextblockusuario_ehcontratada_cell_Internalname ;
      private String cellTextblockusuario_ehcontratada_cell_Class ;
      private String lblTextblockusuario_ehcontratada_Internalname ;
      private String lblTextblockusuario_ehcontratada_Jsonclick ;
      private String cellUsuario_ehcontratada_cell_Internalname ;
      private String cellUsuario_ehcontratada_cell_Class ;
      private String cellTextblockusuario_ehcontratante_cell_Internalname ;
      private String cellTextblockusuario_ehcontratante_cell_Class ;
      private String lblTextblockusuario_ehcontratante_Internalname ;
      private String lblTextblockusuario_ehcontratante_Jsonclick ;
      private String cellUsuario_ehcontratante_cell_Internalname ;
      private String cellUsuario_ehcontratante_cell_Class ;
      private String cellTextblockusuario_ehpreposto_cell_Internalname ;
      private String cellTextblockusuario_ehpreposto_cell_Class ;
      private String lblTextblockusuario_ehpreposto_Internalname ;
      private String lblTextblockusuario_ehpreposto_Jsonclick ;
      private String cellUsuario_ehpreposto_cell_Internalname ;
      private String cellUsuario_ehpreposto_cell_Class ;
      private String lblTextblockusuario_notificar_Internalname ;
      private String lblTextblockusuario_notificar_Jsonclick ;
      private String cmbUsuario_Notificar_Internalname ;
      private String cmbUsuario_Notificar_Jsonclick ;
      private String lblTextblockusuario_email_Internalname ;
      private String lblTextblockusuario_email_Jsonclick ;
      private String edtUsuario_Email_Internalname ;
      private String edtUsuario_Email_Jsonclick ;
      private String lblTextblockusuario_pessoatelefone_Internalname ;
      private String lblTextblockusuario_pessoatelefone_Jsonclick ;
      private String edtUsuario_PessoaTelefone_Internalname ;
      private String A2095Usuario_PessoaTelefone ;
      private String edtUsuario_PessoaTelefone_Jsonclick ;
      private String lblTextblockusuario_foto_Internalname ;
      private String lblTextblockusuario_foto_Jsonclick ;
      private String imgUsuario_Foto_Internalname ;
      private String lblTextblockusuario_crtfpath_Internalname ;
      private String lblTextblockusuario_crtfpath_Jsonclick ;
      private String edtUsuario_CrtfPath_Internalname ;
      private String edtUsuario_CrtfPath_Jsonclick ;
      private String lblTextblockusuario_deferias_Internalname ;
      private String lblTextblockusuario_deferias_Jsonclick ;
      private String lblTextblockusuario_ativo_Internalname ;
      private String lblTextblockusuario_ativo_Jsonclick ;
      private String tblTablemergedunidadeorganizacional_codigo_Internalname ;
      private String dynavUnidadeorganizacional_codigo_Internalname ;
      private String dynavUnidadeorganizacional_codigo_Jsonclick ;
      private String imgSelectuo_Internalname ;
      private String imgSelectuo_Jsonclick ;
      private String tblTabletitle_Internalname ;
      private String A2Usuario_Nome ;
      private String A1076Usuario_CargoUONom ;
      private String AV32Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode1 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV27Usuario_UserGamGuid ;
      private String Gx_msg ;
      private String Z1076Usuario_CargoUONom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String i1235Usuario_Notificar ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private String GXt_char1 ;
      private bool Z289Usuario_EhContador ;
      private bool Z291Usuario_EhContratada ;
      private bool Z292Usuario_EhContratante ;
      private bool Z293Usuario_EhFinanceiro ;
      private bool Z538Usuario_EhGestor ;
      private bool Z1093Usuario_EhPreposto ;
      private bool Z54Usuario_Ativo ;
      private bool Z1908Usuario_DeFerias ;
      private bool entryPointCalled ;
      private bool n1Usuario_Codigo ;
      private bool n1073Usuario_CargoCod ;
      private bool n1075Usuario_CargoUOCod ;
      private bool toggleJsOutput ;
      private bool n59Usuario_PessoaTip ;
      private bool n1235Usuario_Notificar ;
      private bool wbErr ;
      private bool AV23EhContratada_Flag ;
      private bool AV29ContratanteUsuario_EhFiscal ;
      private bool A289Usuario_EhContador ;
      private bool A290Usuario_EhAuditorFM ;
      private bool A293Usuario_EhFinanceiro ;
      private bool A291Usuario_EhContratada ;
      private bool A292Usuario_EhContratante ;
      private bool A1093Usuario_EhPreposto ;
      private bool A1716Usuario_Foto_IsBlob ;
      private bool A1908Usuario_DeFerias ;
      private bool A54Usuario_Ativo ;
      private bool n1074Usuario_CargoNom ;
      private bool n58Usuario_PessoaNom ;
      private bool n325Usuario_PessoaDoc ;
      private bool n1647Usuario_Email ;
      private bool n2095Usuario_PessoaTelefone ;
      private bool n1716Usuario_Foto ;
      private bool n1017Usuario_CrtfPath ;
      private bool n1908Usuario_DeFerias ;
      private bool n2Usuario_Nome ;
      private bool n2016Usuario_UltimaArea ;
      private bool A538Usuario_EhGestor ;
      private bool n40000Usuario_Foto_GXI ;
      private bool n1076Usuario_CargoUONom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i1908Usuario_DeFerias ;
      private bool i54Usuario_Ativo ;
      private bool i1093Usuario_EhPreposto ;
      private bool i538Usuario_EhGestor ;
      private bool GXt_boolean2 ;
      private String Z1017Usuario_CrtfPath ;
      private String Z1647Usuario_Email ;
      private String Z325Usuario_PessoaDoc ;
      private String A1074Usuario_CargoNom ;
      private String A325Usuario_PessoaDoc ;
      private String AV18Email ;
      private String A1647Usuario_Email ;
      private String A40000Usuario_Foto_GXI ;
      private String A1017Usuario_CrtfPath ;
      private String Z40000Usuario_Foto_GXI ;
      private String Z1074Usuario_CargoNom ;
      private String A1716Usuario_Foto ;
      private String Z1716Usuario_Foto ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavUnidadeorganizacional_codigo ;
      private GXCombobox cmbUsuario_PessoaTip ;
      private GXCheckbox chkavContratanteusuario_ehfiscal ;
      private GXCheckbox chkUsuario_EhContador ;
      private GXCheckbox chkUsuario_EhAuditorFM ;
      private GXCheckbox chkUsuario_EhFinanceiro ;
      private GXCheckbox chkUsuario_EhContratada ;
      private GXCheckbox chkUsuario_EhContratante ;
      private GXCheckbox chkUsuario_EhPreposto ;
      private GXCombobox cmbUsuario_Notificar ;
      private GXCheckbox chkUsuario_DeFerias ;
      private GXCheckbox chkUsuario_Ativo ;
      private GXCheckbox chkavEhcontratada_flag ;
      private IDataStoreProvider pr_default ;
      private String[] T00015_A58Usuario_PessoaNom ;
      private bool[] T00015_n58Usuario_PessoaNom ;
      private String[] T00015_A59Usuario_PessoaTip ;
      private bool[] T00015_n59Usuario_PessoaTip ;
      private String[] T00015_A325Usuario_PessoaDoc ;
      private bool[] T00015_n325Usuario_PessoaDoc ;
      private String[] T00015_A2095Usuario_PessoaTelefone ;
      private bool[] T00015_n2095Usuario_PessoaTelefone ;
      private String[] T00016_A1074Usuario_CargoNom ;
      private bool[] T00016_n1074Usuario_CargoNom ;
      private int[] T00016_A1075Usuario_CargoUOCod ;
      private bool[] T00016_n1075Usuario_CargoUOCod ;
      private String[] T00017_A1076Usuario_CargoUONom ;
      private bool[] T00017_n1076Usuario_CargoUONom ;
      private int[] T00018_A1Usuario_Codigo ;
      private bool[] T00018_n1Usuario_Codigo ;
      private String[] T00018_A1074Usuario_CargoNom ;
      private bool[] T00018_n1074Usuario_CargoNom ;
      private String[] T00018_A1076Usuario_CargoUONom ;
      private bool[] T00018_n1076Usuario_CargoUONom ;
      private String[] T00018_A58Usuario_PessoaNom ;
      private bool[] T00018_n58Usuario_PessoaNom ;
      private String[] T00018_A59Usuario_PessoaTip ;
      private bool[] T00018_n59Usuario_PessoaTip ;
      private String[] T00018_A325Usuario_PessoaDoc ;
      private bool[] T00018_n325Usuario_PessoaDoc ;
      private String[] T00018_A2095Usuario_PessoaTelefone ;
      private bool[] T00018_n2095Usuario_PessoaTelefone ;
      private String[] T00018_A341Usuario_UserGamGuid ;
      private String[] T00018_A2Usuario_Nome ;
      private bool[] T00018_n2Usuario_Nome ;
      private bool[] T00018_A289Usuario_EhContador ;
      private bool[] T00018_A291Usuario_EhContratada ;
      private bool[] T00018_A292Usuario_EhContratante ;
      private bool[] T00018_A293Usuario_EhFinanceiro ;
      private bool[] T00018_A538Usuario_EhGestor ;
      private bool[] T00018_A1093Usuario_EhPreposto ;
      private String[] T00018_A1017Usuario_CrtfPath ;
      private bool[] T00018_n1017Usuario_CrtfPath ;
      private String[] T00018_A1235Usuario_Notificar ;
      private bool[] T00018_n1235Usuario_Notificar ;
      private bool[] T00018_A54Usuario_Ativo ;
      private String[] T00018_A1647Usuario_Email ;
      private bool[] T00018_n1647Usuario_Email ;
      private String[] T00018_A40000Usuario_Foto_GXI ;
      private bool[] T00018_n40000Usuario_Foto_GXI ;
      private bool[] T00018_A1908Usuario_DeFerias ;
      private bool[] T00018_n1908Usuario_DeFerias ;
      private int[] T00018_A2016Usuario_UltimaArea ;
      private bool[] T00018_n2016Usuario_UltimaArea ;
      private int[] T00018_A57Usuario_PessoaCod ;
      private int[] T00018_A1073Usuario_CargoCod ;
      private bool[] T00018_n1073Usuario_CargoCod ;
      private int[] T00018_A1075Usuario_CargoUOCod ;
      private bool[] T00018_n1075Usuario_CargoUOCod ;
      private String[] T00018_A1716Usuario_Foto ;
      private bool[] T00018_n1716Usuario_Foto ;
      private String[] T00019_A58Usuario_PessoaNom ;
      private bool[] T00019_n58Usuario_PessoaNom ;
      private String[] T00019_A59Usuario_PessoaTip ;
      private bool[] T00019_n59Usuario_PessoaTip ;
      private String[] T00019_A325Usuario_PessoaDoc ;
      private bool[] T00019_n325Usuario_PessoaDoc ;
      private String[] T00019_A2095Usuario_PessoaTelefone ;
      private bool[] T00019_n2095Usuario_PessoaTelefone ;
      private String[] T000110_A1074Usuario_CargoNom ;
      private bool[] T000110_n1074Usuario_CargoNom ;
      private int[] T000110_A1075Usuario_CargoUOCod ;
      private bool[] T000110_n1075Usuario_CargoUOCod ;
      private String[] T000111_A1076Usuario_CargoUONom ;
      private bool[] T000111_n1076Usuario_CargoUONom ;
      private int[] T000112_A1Usuario_Codigo ;
      private bool[] T000112_n1Usuario_Codigo ;
      private int[] T00013_A1Usuario_Codigo ;
      private bool[] T00013_n1Usuario_Codigo ;
      private String[] T00013_A341Usuario_UserGamGuid ;
      private String[] T00013_A2Usuario_Nome ;
      private bool[] T00013_n2Usuario_Nome ;
      private bool[] T00013_A289Usuario_EhContador ;
      private bool[] T00013_A291Usuario_EhContratada ;
      private bool[] T00013_A292Usuario_EhContratante ;
      private bool[] T00013_A293Usuario_EhFinanceiro ;
      private bool[] T00013_A538Usuario_EhGestor ;
      private bool[] T00013_A1093Usuario_EhPreposto ;
      private String[] T00013_A1017Usuario_CrtfPath ;
      private bool[] T00013_n1017Usuario_CrtfPath ;
      private String[] T00013_A1235Usuario_Notificar ;
      private bool[] T00013_n1235Usuario_Notificar ;
      private bool[] T00013_A54Usuario_Ativo ;
      private String[] T00013_A1647Usuario_Email ;
      private bool[] T00013_n1647Usuario_Email ;
      private String[] T00013_A40000Usuario_Foto_GXI ;
      private bool[] T00013_n40000Usuario_Foto_GXI ;
      private bool[] T00013_A1908Usuario_DeFerias ;
      private bool[] T00013_n1908Usuario_DeFerias ;
      private int[] T00013_A2016Usuario_UltimaArea ;
      private bool[] T00013_n2016Usuario_UltimaArea ;
      private int[] T00013_A57Usuario_PessoaCod ;
      private int[] T00013_A1073Usuario_CargoCod ;
      private bool[] T00013_n1073Usuario_CargoCod ;
      private String[] T00013_A1716Usuario_Foto ;
      private bool[] T00013_n1716Usuario_Foto ;
      private int[] T000113_A1Usuario_Codigo ;
      private bool[] T000113_n1Usuario_Codigo ;
      private int[] T000114_A1Usuario_Codigo ;
      private bool[] T000114_n1Usuario_Codigo ;
      private int[] T00012_A1Usuario_Codigo ;
      private bool[] T00012_n1Usuario_Codigo ;
      private String[] T00012_A341Usuario_UserGamGuid ;
      private String[] T00012_A2Usuario_Nome ;
      private bool[] T00012_n2Usuario_Nome ;
      private bool[] T00012_A289Usuario_EhContador ;
      private bool[] T00012_A291Usuario_EhContratada ;
      private bool[] T00012_A292Usuario_EhContratante ;
      private bool[] T00012_A293Usuario_EhFinanceiro ;
      private bool[] T00012_A538Usuario_EhGestor ;
      private bool[] T00012_A1093Usuario_EhPreposto ;
      private String[] T00012_A1017Usuario_CrtfPath ;
      private bool[] T00012_n1017Usuario_CrtfPath ;
      private String[] T00012_A1235Usuario_Notificar ;
      private bool[] T00012_n1235Usuario_Notificar ;
      private bool[] T00012_A54Usuario_Ativo ;
      private String[] T00012_A1647Usuario_Email ;
      private bool[] T00012_n1647Usuario_Email ;
      private String[] T00012_A40000Usuario_Foto_GXI ;
      private bool[] T00012_n40000Usuario_Foto_GXI ;
      private bool[] T00012_A1908Usuario_DeFerias ;
      private bool[] T00012_n1908Usuario_DeFerias ;
      private int[] T00012_A2016Usuario_UltimaArea ;
      private bool[] T00012_n2016Usuario_UltimaArea ;
      private int[] T00012_A57Usuario_PessoaCod ;
      private int[] T00012_A1073Usuario_CargoCod ;
      private bool[] T00012_n1073Usuario_CargoCod ;
      private String[] T00012_A1716Usuario_Foto ;
      private bool[] T00012_n1716Usuario_Foto ;
      private String[] T00014_A58Usuario_PessoaNom ;
      private bool[] T00014_n58Usuario_PessoaNom ;
      private String[] T00014_A59Usuario_PessoaTip ;
      private bool[] T00014_n59Usuario_PessoaTip ;
      private String[] T00014_A325Usuario_PessoaDoc ;
      private bool[] T00014_n325Usuario_PessoaDoc ;
      private String[] T00014_A2095Usuario_PessoaTelefone ;
      private bool[] T00014_n2095Usuario_PessoaTelefone ;
      private int[] T000115_A1Usuario_Codigo ;
      private bool[] T000115_n1Usuario_Codigo ;
      private String[] T000119_A58Usuario_PessoaNom ;
      private bool[] T000119_n58Usuario_PessoaNom ;
      private String[] T000119_A59Usuario_PessoaTip ;
      private bool[] T000119_n59Usuario_PessoaTip ;
      private String[] T000119_A325Usuario_PessoaDoc ;
      private bool[] T000119_n325Usuario_PessoaDoc ;
      private String[] T000119_A2095Usuario_PessoaTelefone ;
      private bool[] T000119_n2095Usuario_PessoaTelefone ;
      private String[] T000120_A1074Usuario_CargoNom ;
      private bool[] T000120_n1074Usuario_CargoNom ;
      private int[] T000120_A1075Usuario_CargoUOCod ;
      private bool[] T000120_n1075Usuario_CargoUOCod ;
      private String[] T000121_A1076Usuario_CargoUONom ;
      private bool[] T000121_n1076Usuario_CargoUONom ;
      private int[] T000122_A2077UsuarioNotifica_Codigo ;
      private int[] T000123_A1984ContagemResultadoQA_Codigo ;
      private int[] T000124_A1984ContagemResultadoQA_Codigo ;
      private int[] T000125_A1824ContratoAuxiliar_ContratoCod ;
      private int[] T000125_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] T000126_A1562HistoricoConsumo_Codigo ;
      private int[] T000127_A1482Gestao_Codigo ;
      private int[] T000128_A1482Gestao_Codigo ;
      private int[] T000129_A1473ContratoServicosCusto_Codigo ;
      private long[] T000130_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T000130_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] T000131_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T000132_A127Sistema_Codigo ;
      private int[] T000133_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T000133_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] T000134_A1331ContagemResultadoNota_Codigo ;
      private int[] T000135_A1078ContratoGestor_ContratoCod ;
      private int[] T000135_A1079ContratoGestor_UsuarioCod ;
      private int[] T000136_A1041ContagemResultadoImpLog_Codigo ;
      private int[] T000137_A74Contrato_Codigo ;
      private int[] T000138_A996SolicitacaoMudanca_Codigo ;
      private long[] T000139_A1797LogResponsavel_Codigo ;
      private long[] T000140_A1797LogResponsavel_Codigo ;
      private int[] T000141_A828UsuarioServicos_UsuarioCod ;
      private int[] T000141_A829UsuarioServicos_ServicoCod ;
      private int[] T000142_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T000143_A722Baseline_Codigo ;
      private int[] T000144_A456ContagemResultado_Codigo ;
      private String[] T000144_A579ContagemResultadoErro_Tipo ;
      private int[] T000145_A596Lote_Codigo ;
      private int[] T000146_A456ContagemResultado_Codigo ;
      private DateTime[] T000146_A473ContagemResultado_DataCnt ;
      private String[] T000146_A511ContagemResultado_HoraCnt ;
      private int[] T000147_A456ContagemResultado_Codigo ;
      private int[] T000148_A456ContagemResultado_Codigo ;
      private int[] T000149_A439Solicitacoes_Codigo ;
      private int[] T000150_A439Solicitacoes_Codigo ;
      private int[] T000151_A435File_UsuarioCod ;
      private int[] T000151_A504File_Row ;
      private int[] T000152_A243ContagemItemParecer_Codigo ;
      private int[] T000153_A192Contagem_Codigo ;
      private int[] T000154_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000154_A69ContratadaUsuario_UsuarioCod ;
      private int[] T000155_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000155_A60ContratanteUsuario_UsuarioCod ;
      private long[] T000156_A1430AuditId ;
      private int[] T000157_A2175Equipe_Codigo ;
      private int[] T000157_A1Usuario_Codigo ;
      private bool[] T000157_n1Usuario_Codigo ;
      private int[] T000158_A1Usuario_Codigo ;
      private bool[] T000158_n1Usuario_Codigo ;
      private int[] T000158_A487Selected_Codigo ;
      private bool[] T000158_A488Selected_Flag ;
      private int[] T000159_A1Usuario_Codigo ;
      private bool[] T000159_n1Usuario_Codigo ;
      private int[] T000159_A3Perfil_Codigo ;
      private int[] T000161_A1Usuario_Codigo ;
      private bool[] T000161_n1Usuario_Codigo ;
      private int[] T000162_A611UnidadeOrganizacional_Codigo ;
      private String[] T000162_A612UnidadeOrganizacional_Nome ;
      private bool[] T000162_A629UnidadeOrganizacional_Ativo ;
      private String[] T000163_A58Usuario_PessoaNom ;
      private bool[] T000163_n58Usuario_PessoaNom ;
      private String[] T000163_A59Usuario_PessoaTip ;
      private bool[] T000163_n59Usuario_PessoaTip ;
      private String[] T000163_A325Usuario_PessoaDoc ;
      private bool[] T000163_n325Usuario_PessoaDoc ;
      private String[] T000163_A2095Usuario_PessoaTelefone ;
      private bool[] T000163_n2095Usuario_PessoaTelefone ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV28AuditingObject ;
      private SdtContratadaUsuario AV22ContratadaUsuario ;
      private SdtGAMUser AV15GamUser ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class usuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
         ,new ForEachCursor(def[57])
         ,new UpdateCursor(def[58])
         ,new ForEachCursor(def[59])
         ,new ForEachCursor(def[60])
         ,new ForEachCursor(def[61])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00018 ;
          prmT00018 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00015 ;
          prmT00015 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00016 ;
          prmT00016 = new Object[] {
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00017 ;
          prmT00017 = new Object[] {
          new Object[] {"@Usuario_CargoUOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00019 ;
          prmT00019 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000110 ;
          prmT000110 = new Object[] {
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000111 ;
          prmT000111 = new Object[] {
          new Object[] {"@Usuario_CargoUOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000112 ;
          prmT000112 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00013 ;
          prmT00013 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000113 ;
          prmT000113 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000114 ;
          prmT000114 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00012 ;
          prmT00012 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00014 ;
          prmT00014 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000115 ;
          prmT000115 = new Object[] {
          new Object[] {"@Usuario_UserGamGuid",SqlDbType.Char,40,0} ,
          new Object[] {"@Usuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Usuario_EhContador",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhFinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhGestor",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhPreposto",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_CrtfPath",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_Notificar",SqlDbType.Char,1,0} ,
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_Foto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Usuario_Foto_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Usuario_DeFerias",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_UltimaArea",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000116 ;
          prmT000116 = new Object[] {
          new Object[] {"@Usuario_UserGamGuid",SqlDbType.Char,40,0} ,
          new Object[] {"@Usuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Usuario_EhContador",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhContratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhFinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhGestor",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_EhPreposto",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_CrtfPath",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_Notificar",SqlDbType.Char,1,0} ,
          new Object[] {"@Usuario_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Usuario_DeFerias",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_UltimaArea",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000117 ;
          prmT000117 = new Object[] {
          new Object[] {"@Usuario_Foto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Usuario_Foto_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000118 ;
          prmT000118 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000119 ;
          prmT000119 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000120 ;
          prmT000120 = new Object[] {
          new Object[] {"@Usuario_CargoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000121 ;
          prmT000121 = new Object[] {
          new Object[] {"@Usuario_CargoUOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000122 ;
          prmT000122 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000123 ;
          prmT000123 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000124 ;
          prmT000124 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000125 ;
          prmT000125 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000126 ;
          prmT000126 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000127 ;
          prmT000127 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000128 ;
          prmT000128 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000129 ;
          prmT000129 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000130 ;
          prmT000130 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000131 ;
          prmT000131 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000132 ;
          prmT000132 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000133 ;
          prmT000133 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000134 ;
          prmT000134 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000135 ;
          prmT000135 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000136 ;
          prmT000136 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000137 ;
          prmT000137 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000138 ;
          prmT000138 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000139 ;
          prmT000139 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000140 ;
          prmT000140 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000141 ;
          prmT000141 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000142 ;
          prmT000142 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000143 ;
          prmT000143 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000144 ;
          prmT000144 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000145 ;
          prmT000145 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000146 ;
          prmT000146 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000147 ;
          prmT000147 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000148 ;
          prmT000148 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000149 ;
          prmT000149 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000150 ;
          prmT000150 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000151 ;
          prmT000151 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000152 ;
          prmT000152 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000153 ;
          prmT000153 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000154 ;
          prmT000154 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000155 ;
          prmT000155 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000156 ;
          prmT000156 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000157 ;
          prmT000157 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000158 ;
          prmT000158 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000159 ;
          prmT000159 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000160 ;
          prmT000160 = new Object[] {
          new Object[] {"@Usuario_PessoaTelefone",SqlDbType.Char,15,0} ,
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000161 ;
          prmT000161 = new Object[] {
          } ;
          Object[] prmT000162 ;
          prmT000162 = new Object[] {
          } ;
          Object[] prmT000163 ;
          prmT000163 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00012", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid], [Usuario_Nome], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_EhGestor], [Usuario_EhPreposto], [Usuario_CrtfPath], [Usuario_Notificar], [Usuario_Ativo], [Usuario_Email], [Usuario_Foto_GXI], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_CargoCod] AS Usuario_CargoCod, [Usuario_Foto] FROM [Usuario] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00012,1,0,true,false )
             ,new CursorDef("T00013", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid], [Usuario_Nome], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_EhGestor], [Usuario_EhPreposto], [Usuario_CrtfPath], [Usuario_Notificar], [Usuario_Ativo], [Usuario_Email], [Usuario_Foto_GXI], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_CargoCod] AS Usuario_CargoCod, [Usuario_Foto] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00013,1,0,true,false )
             ,new CursorDef("T00014", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (UPDLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00014,1,0,true,false )
             ,new CursorDef("T00015", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00015,1,0,true,false )
             ,new CursorDef("T00016", "SELECT [Cargo_Nome] AS Usuario_CargoNom, [Cargo_UOCod] AS Usuario_CargoUOCod FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Usuario_CargoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00016,1,0,true,false )
             ,new CursorDef("T00017", "SELECT [UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Usuario_CargoUOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00017,1,0,true,false )
             ,new CursorDef("T00018", "SELECT TM1.[Usuario_Codigo], T3.[Cargo_Nome] AS Usuario_CargoNom, T4.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T2.[Pessoa_Telefone] AS Usuario_PessoaTelefone, TM1.[Usuario_UserGamGuid], TM1.[Usuario_Nome], TM1.[Usuario_EhContador], TM1.[Usuario_EhContratada], TM1.[Usuario_EhContratante], TM1.[Usuario_EhFinanceiro], TM1.[Usuario_EhGestor], TM1.[Usuario_EhPreposto], TM1.[Usuario_CrtfPath], TM1.[Usuario_Notificar], TM1.[Usuario_Ativo], TM1.[Usuario_Email], TM1.[Usuario_Foto_GXI], TM1.[Usuario_DeFerias], TM1.[Usuario_UltimaArea], TM1.[Usuario_PessoaCod] AS Usuario_PessoaCod, TM1.[Usuario_CargoCod] AS Usuario_CargoCod, T3.[Cargo_UOCod] AS Usuario_CargoUOCod, TM1.[Usuario_Foto] FROM ((([Usuario] TM1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = TM1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = TM1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod]) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo ORDER BY TM1.[Usuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00018,100,0,true,false )
             ,new CursorDef("T00019", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00019,1,0,true,false )
             ,new CursorDef("T000110", "SELECT [Cargo_Nome] AS Usuario_CargoNom, [Cargo_UOCod] AS Usuario_CargoUOCod FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Usuario_CargoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000110,1,0,true,false )
             ,new CursorDef("T000111", "SELECT [UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Usuario_CargoUOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000111,1,0,true,false )
             ,new CursorDef("T000112", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000112,1,0,true,false )
             ,new CursorDef("T000113", "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE ( [Usuario_Codigo] > @Usuario_Codigo) ORDER BY [Usuario_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000113,1,0,true,true )
             ,new CursorDef("T000114", "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE ( [Usuario_Codigo] < @Usuario_Codigo) ORDER BY [Usuario_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000114,1,0,true,true )
             ,new CursorDef("T000115", "INSERT INTO [Usuario]([Usuario_UserGamGuid], [Usuario_Nome], [Usuario_EhContador], [Usuario_EhContratada], [Usuario_EhContratante], [Usuario_EhFinanceiro], [Usuario_EhGestor], [Usuario_EhPreposto], [Usuario_CrtfPath], [Usuario_Notificar], [Usuario_Ativo], [Usuario_Email], [Usuario_Foto], [Usuario_Foto_GXI], [Usuario_DeFerias], [Usuario_UltimaArea], [Usuario_PessoaCod], [Usuario_CargoCod]) VALUES(@Usuario_UserGamGuid, @Usuario_Nome, @Usuario_EhContador, @Usuario_EhContratada, @Usuario_EhContratante, @Usuario_EhFinanceiro, @Usuario_EhGestor, @Usuario_EhPreposto, @Usuario_CrtfPath, @Usuario_Notificar, @Usuario_Ativo, @Usuario_Email, @Usuario_Foto, @Usuario_Foto_GXI, @Usuario_DeFerias, @Usuario_UltimaArea, @Usuario_PessoaCod, @Usuario_CargoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000115)
             ,new CursorDef("T000116", "UPDATE [Usuario] SET [Usuario_UserGamGuid]=@Usuario_UserGamGuid, [Usuario_Nome]=@Usuario_Nome, [Usuario_EhContador]=@Usuario_EhContador, [Usuario_EhContratada]=@Usuario_EhContratada, [Usuario_EhContratante]=@Usuario_EhContratante, [Usuario_EhFinanceiro]=@Usuario_EhFinanceiro, [Usuario_EhGestor]=@Usuario_EhGestor, [Usuario_EhPreposto]=@Usuario_EhPreposto, [Usuario_CrtfPath]=@Usuario_CrtfPath, [Usuario_Notificar]=@Usuario_Notificar, [Usuario_Ativo]=@Usuario_Ativo, [Usuario_Email]=@Usuario_Email, [Usuario_DeFerias]=@Usuario_DeFerias, [Usuario_UltimaArea]=@Usuario_UltimaArea, [Usuario_PessoaCod]=@Usuario_PessoaCod, [Usuario_CargoCod]=@Usuario_CargoCod  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmT000116)
             ,new CursorDef("T000117", "UPDATE [Usuario] SET [Usuario_Foto]=@Usuario_Foto, [Usuario_Foto_GXI]=@Usuario_Foto_GXI  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmT000117)
             ,new CursorDef("T000118", "DELETE FROM [Usuario]  WHERE [Usuario_Codigo] = @Usuario_Codigo", GxErrorMask.GX_NOMASK,prmT000118)
             ,new CursorDef("T000119", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000119,1,0,true,false )
             ,new CursorDef("T000120", "SELECT [Cargo_Nome] AS Usuario_CargoNom, [Cargo_UOCod] AS Usuario_CargoUOCod FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Usuario_CargoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000120,1,0,true,false )
             ,new CursorDef("T000121", "SELECT [UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Usuario_CargoUOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000121,1,0,true,false )
             ,new CursorDef("T000122", "SELECT TOP 1 [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000122,1,0,true,true )
             ,new CursorDef("T000123", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_ParaCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000123,1,0,true,true )
             ,new CursorDef("T000124", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000124,1,0,true,true )
             ,new CursorDef("T000125", "SELECT TOP 1 [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000125,1,0,true,true )
             ,new CursorDef("T000126", "SELECT TOP 1 [HistoricoConsumo_Codigo] FROM [HistoricoConsumo] WITH (NOLOCK) WHERE [HistoricoConsumo_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000126,1,0,true,true )
             ,new CursorDef("T000127", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_GestorCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000127,1,0,true,true )
             ,new CursorDef("T000128", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000128,1,0,true,true )
             ,new CursorDef("T000129", "SELECT TOP 1 [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000129,1,0,true,true )
             ,new CursorDef("T000130", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo], [ContagemResultadoNotificacao_DestCod] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_DestCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000130,1,0,true,true )
             ,new CursorDef("T000131", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_UsuCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000131,1,0,true,true )
             ,new CursorDef("T000132", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_ImpUserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000132,1,0,true,true )
             ,new CursorDef("T000133", "SELECT TOP 1 [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLogOS_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000133,1,0,true,true )
             ,new CursorDef("T000134", "SELECT TOP 1 [ContagemResultadoNota_Codigo] FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000134,1,0,true,true )
             ,new CursorDef("T000135", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000135,1,0,true,true )
             ,new CursorDef("T000136", "SELECT TOP 1 [ContagemResultadoImpLog_Codigo] FROM [ContagemResultadoImpLog] WITH (NOLOCK) WHERE [ContagemResultadoImpLog_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000136,1,0,true,true )
             ,new CursorDef("T000137", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_PrepostoCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000137,1,0,true,true )
             ,new CursorDef("T000138", "SELECT TOP 1 [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Solicitante] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000138,1,0,true,true )
             ,new CursorDef("T000139", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Owner] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000139,1,0,true,true )
             ,new CursorDef("T000140", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000140,1,0,true,true )
             ,new CursorDef("T000141", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000141,1,0,true,true )
             ,new CursorDef("T000142", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000142,1,0,true,true )
             ,new CursorDef("T000143", "SELECT TOP 1 [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE [Baseline_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000143,1,0,true,true )
             ,new CursorDef("T000144", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultadoErro_ContadorFMCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000144,1,0,true,true )
             ,new CursorDef("T000145", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_UserCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000145,1,0,true,true )
             ,new CursorDef("T000146", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_ContadorFMCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000146,1,0,true,true )
             ,new CursorDef("T000147", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Responsavel] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000147,1,0,true,true )
             ,new CursorDef("T000148", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ContadorFSCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000148,1,0,true,true )
             ,new CursorDef("T000149", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Usuario_Ult] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000149,1,0,true,true )
             ,new CursorDef("T000150", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Usuario] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000150,1,0,true,true )
             ,new CursorDef("T000151", "SELECT TOP 1 [File_UsuarioCod], [File_Row] FROM [Tmp_File] WITH (NOLOCK) WHERE [File_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000151,1,0,true,true )
             ,new CursorDef("T000152", "SELECT TOP 1 [ContagemItemParecer_Codigo] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE [ContagemItemParecer_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000152,1,0,true,true )
             ,new CursorDef("T000153", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_UsuarioContadorCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000153,1,0,true,true )
             ,new CursorDef("T000154", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000154,1,0,true,true )
             ,new CursorDef("T000155", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_UsuarioCod] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000155,1,0,true,true )
             ,new CursorDef("T000156", "SELECT TOP 1 [AuditId] FROM [Audit] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000156,1,0,true,true )
             ,new CursorDef("T000157", "SELECT TOP 1 [Equipe_Codigo], [Usuario_Codigo] FROM [EquipeUsuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000157,1,0,true,true )
             ,new CursorDef("T000158", "SELECT TOP 1 [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000158,1,0,true,true )
             ,new CursorDef("T000159", "SELECT TOP 1 [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000159,1,0,true,true )
             ,new CursorDef("T000160", "UPDATE [Pessoa] SET [Pessoa_Telefone]=@Usuario_PessoaTelefone  WHERE [Pessoa_Codigo] = @Usuario_PessoaCod", GxErrorMask.GX_NOMASK,prmT000160)
             ,new CursorDef("T000161", "SELECT [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) ORDER BY [Usuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000161,100,0,true,false )
             ,new CursorDef("T000162", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000162,0,0,true,false )
             ,new CursorDef("T000163", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom, [Pessoa_TipoPessoa] AS Usuario_PessoaTip, [Pessoa_Docto] AS Usuario_PessoaDoc, [Pessoa_Telefone] AS Usuario_PessoaTelefone FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000163,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.getBool(9) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((bool[]) buf[14])[0] = rslt.getBool(12) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getMultimediaUri(14) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((int[]) buf[21])[0] = rslt.getInt(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((int[]) buf[23])[0] = rslt.getInt(17) ;
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(18);
                ((String[]) buf[26])[0] = rslt.getMultimediaFile(19, rslt.getVarchar(14)) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.getBool(9) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((String[]) buf[12])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((bool[]) buf[14])[0] = rslt.getBool(12) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getMultimediaUri(14) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((int[]) buf[21])[0] = rslt.getInt(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((int[]) buf[23])[0] = rslt.getInt(17) ;
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(18);
                ((String[]) buf[26])[0] = rslt.getMultimediaFile(19, rslt.getVarchar(14)) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 40) ;
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.getBool(12) ;
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(16);
                ((String[]) buf[24])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                ((bool[]) buf[26])[0] = rslt.getBool(18) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(19);
                ((String[]) buf[29])[0] = rslt.getMultimediaUri(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((bool[]) buf[31])[0] = rslt.getBool(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((int[]) buf[36])[0] = rslt.getInt(24) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(24);
                ((int[]) buf[38])[0] = rslt.getInt(25) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(25);
                ((String[]) buf[40])[0] = rslt.getMultimediaFile(26, rslt.getVarchar(20)) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(26);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 29 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 38 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 50 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 51 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 52 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 54 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 55 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 56 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 57 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 59 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults60( cursor, rslt, buf) ;
    }

    public void getresults60( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 60 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 61 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (bool)parms[4]);
                stmt.SetParameter(5, (bool)parms[5]);
                stmt.SetParameter(6, (bool)parms[6]);
                stmt.SetParameter(7, (bool)parms[7]);
                stmt.SetParameter(8, (bool)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[12]);
                }
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 13 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(14, (String)parms[19], (String)parms[17]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[23]);
                }
                stmt.SetParameter(17, (int)parms[24]);
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[26]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                stmt.SetParameter(4, (bool)parms[4]);
                stmt.SetParameter(5, (bool)parms[5]);
                stmt.SetParameter(6, (bool)parms[6]);
                stmt.SetParameter(7, (bool)parms[7]);
                stmt.SetParameter(8, (bool)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[12]);
                }
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[19]);
                }
                stmt.SetParameter(15, (int)parms[20]);
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[24]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 41 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 42 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 43 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 45 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 46 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 47 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 48 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 49 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 50 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 51 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 52 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 53 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 54 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 55 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 56 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 57 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 58 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 61 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
