/*
               File: GetWWContratoServicosVncFilterData
        Description: Get WWContrato Servicos Vnc Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:56.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoservicosvncfilterdata : GXProcedure
   {
      public getwwcontratoservicosvncfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoservicosvncfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
         return AV30OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoservicosvncfilterdata objgetwwcontratoservicosvncfilterdata;
         objgetwwcontratoservicosvncfilterdata = new getwwcontratoservicosvncfilterdata();
         objgetwwcontratoservicosvncfilterdata.AV21DDOName = aP0_DDOName;
         objgetwwcontratoservicosvncfilterdata.AV19SearchTxt = aP1_SearchTxt;
         objgetwwcontratoservicosvncfilterdata.AV20SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoservicosvncfilterdata.AV25OptionsJson = "" ;
         objgetwwcontratoservicosvncfilterdata.AV28OptionsDescJson = "" ;
         objgetwwcontratoservicosvncfilterdata.AV30OptionIndexesJson = "" ;
         objgetwwcontratoservicosvncfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoservicosvncfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoservicosvncfilterdata);
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoservicosvncfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV24Options = (IGxCollection)(new GxSimpleCollection());
         AV27OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV29OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_CONTRATOSRVVNC_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SERVICOSIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_CONTRATOSERVICOSVNC_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSVNC_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SERVICOVNCSIGLAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_PRESTADORAPESNOMOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SRVVNCCNTNUMOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV25OptionsJson = AV24Options.ToJSonString(false);
         AV28OptionsDescJson = AV27OptionsDesc.ToJSonString(false);
         AV30OptionIndexesJson = AV29OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get("WWContratoServicosVncGridState"), "") == 0 )
         {
            AV34GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoServicosVncGridState"), "");
         }
         else
         {
            AV34GridState.FromXml(AV32Session.Get("WWContratoServicosVncGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV34GridState.gxTpr_Filtervalues.Count )
         {
            AV35GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV34GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOSIGLA") == 0 )
            {
               AV10TFContratoSrvVnc_ServicoSigla = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOSIGLA_SEL") == 0 )
            {
               AV11TFContratoSrvVnc_ServicoSigla_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO") == 0 )
            {
               AV41TFContratoServicosVnc_Descricao = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO_SEL") == 0 )
            {
               AV42TFContratoServicosVnc_Descricao_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_STATUSDMN_SEL") == 0 )
            {
               AV12TFContratoSrvVnc_StatusDmn_SelsJson = AV35GridStateFilterValue.gxTpr_Value;
               AV13TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV12TFContratoSrvVnc_StatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
            {
               AV14TFContratoSrvVnc_ServicoVncSigla = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL") == 0 )
            {
               AV15TFContratoSrvVnc_ServicoVncSigla_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_PRESTADORAPESNOM") == 0 )
            {
               AV16TFContratoSrvVnc_PrestadoraPesNom = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL") == 0 )
            {
               AV17TFContratoSrvVnc_PrestadoraPesNom_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM") == 0 )
            {
               AV37TFContratoSrvVnc_SrvVncCntNum = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL") == 0 )
            {
               AV38TFContratoSrvVnc_SrvVncCntNum_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_CLONARLINK_SEL") == 0 )
            {
               AV39TFContratoServicosVnc_ClonarLink_Sel = (short)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SEMCUSTO_SEL") == 0 )
            {
               AV18TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_ATIVO_SEL") == 0 )
            {
               AV40TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSRVVNC_SERVICOSIGLAOPTIONS' Routine */
         AV10TFContratoSrvVnc_ServicoSigla = AV19SearchTxt;
         AV11TFContratoSrvVnc_ServicoSigla_Sel = "";
         AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV10TFContratoSrvVnc_ServicoSigla;
         AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV11TFContratoSrvVnc_ServicoSigla_Sel;
         AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV41TFContratoServicosVnc_Descricao;
         AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV42TFContratoServicosVnc_Descricao_Sel;
         AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV13TFContratoSrvVnc_StatusDmn_Sels;
         AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV14TFContratoSrvVnc_ServicoVncSigla;
         AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV15TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV16TFContratoSrvVnc_PrestadoraPesNom;
         AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV17TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV37TFContratoSrvVnc_SrvVncCntNum;
         AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV38TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV39TFContratoServicosVnc_ClonarLink_Sel;
         AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV18TFContratoSrvVnc_SemCusto_Sel;
         AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV40TFContratoServicosVnc_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                              AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1818ContratoServicosVnc_ClonarLink ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              A1092ContratoSrvVnc_PrestadoraPesNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
         lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
         lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
         lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
         lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
         /* Using cursor P00PD4 */
         pr_default.execute(0, new Object[] {AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPD2 = false;
            A915ContratoSrvVnc_CntSrvCod = P00PD4_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PD4_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD4_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD4_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD4_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD4_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD4_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD4_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD4_n923ContratoSrvVnc_ServicoVncCod[0];
            A917ContratoSrvVnc_Codigo = P00PD4_A917ContratoSrvVnc_Codigo[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD4_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD4_n922ContratoSrvVnc_ServicoSigla[0];
            A1453ContratoServicosVnc_Ativo = P00PD4_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00PD4_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00PD4_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00PD4_n1090ContratoSrvVnc_SemCusto[0];
            A1818ContratoServicosVnc_ClonarLink = P00PD4_A1818ContratoServicosVnc_ClonarLink[0];
            n1818ContratoServicosVnc_ClonarLink = P00PD4_n1818ContratoServicosVnc_ClonarLink[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD4_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD4_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD4_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD4_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00PD4_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PD4_n1084ContratoSrvVnc_StatusDmn[0];
            A2108ContratoServicosVnc_Descricao = P00PD4_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00PD4_n2108ContratoServicosVnc_Descricao[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD4_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD4_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            A921ContratoSrvVnc_ServicoCod = P00PD4_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD4_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD4_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD4_n922ContratoSrvVnc_ServicoSigla[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD4_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD4_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD4_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD4_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD4_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD4_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD4_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD4_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD4_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD4_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            AV31count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PD4_A922ContratoSrvVnc_ServicoSigla[0], A922ContratoSrvVnc_ServicoSigla) == 0 ) )
            {
               BRKPD2 = false;
               A915ContratoSrvVnc_CntSrvCod = P00PD4_A915ContratoSrvVnc_CntSrvCod[0];
               A921ContratoSrvVnc_ServicoCod = P00PD4_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = P00PD4_n921ContratoSrvVnc_ServicoCod[0];
               A917ContratoSrvVnc_Codigo = P00PD4_A917ContratoSrvVnc_Codigo[0];
               A921ContratoSrvVnc_ServicoCod = P00PD4_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = P00PD4_n921ContratoSrvVnc_ServicoCod[0];
               AV31count = (long)(AV31count+1);
               BRKPD2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla)) )
            {
               AV23Option = A922ContratoSrvVnc_ServicoSigla;
               AV24Options.Add(AV23Option, 0);
               AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPD2 )
            {
               BRKPD2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSERVICOSVNC_DESCRICAOOPTIONS' Routine */
         AV41TFContratoServicosVnc_Descricao = AV19SearchTxt;
         AV42TFContratoServicosVnc_Descricao_Sel = "";
         AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV10TFContratoSrvVnc_ServicoSigla;
         AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV11TFContratoSrvVnc_ServicoSigla_Sel;
         AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV41TFContratoServicosVnc_Descricao;
         AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV42TFContratoServicosVnc_Descricao_Sel;
         AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV13TFContratoSrvVnc_StatusDmn_Sels;
         AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV14TFContratoSrvVnc_ServicoVncSigla;
         AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV15TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV16TFContratoSrvVnc_PrestadoraPesNom;
         AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV17TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV37TFContratoSrvVnc_SrvVncCntNum;
         AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV38TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV39TFContratoServicosVnc_ClonarLink_Sel;
         AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV18TFContratoSrvVnc_SemCusto_Sel;
         AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV40TFContratoServicosVnc_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                              AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1818ContratoServicosVnc_ClonarLink ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              A1092ContratoSrvVnc_PrestadoraPesNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
         lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
         lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
         lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
         lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
         /* Using cursor P00PD7 */
         pr_default.execute(1, new Object[] {AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKPD4 = false;
            A915ContratoSrvVnc_CntSrvCod = P00PD7_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PD7_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD7_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD7_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD7_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD7_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD7_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD7_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD7_n923ContratoSrvVnc_ServicoVncCod[0];
            A917ContratoSrvVnc_Codigo = P00PD7_A917ContratoSrvVnc_Codigo[0];
            A2108ContratoServicosVnc_Descricao = P00PD7_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00PD7_n2108ContratoServicosVnc_Descricao[0];
            A1453ContratoServicosVnc_Ativo = P00PD7_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00PD7_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00PD7_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00PD7_n1090ContratoSrvVnc_SemCusto[0];
            A1818ContratoServicosVnc_ClonarLink = P00PD7_A1818ContratoServicosVnc_ClonarLink[0];
            n1818ContratoServicosVnc_ClonarLink = P00PD7_n1818ContratoServicosVnc_ClonarLink[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD7_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD7_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD7_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD7_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00PD7_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PD7_n1084ContratoSrvVnc_StatusDmn[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD7_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD7_n922ContratoSrvVnc_ServicoSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD7_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD7_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            A921ContratoSrvVnc_ServicoCod = P00PD7_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD7_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD7_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD7_n922ContratoSrvVnc_ServicoSigla[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD7_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD7_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD7_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD7_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD7_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD7_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD7_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD7_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD7_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD7_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            AV31count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00PD7_A2108ContratoServicosVnc_Descricao[0], A2108ContratoServicosVnc_Descricao) == 0 ) )
            {
               BRKPD4 = false;
               A917ContratoSrvVnc_Codigo = P00PD7_A917ContratoSrvVnc_Codigo[0];
               AV31count = (long)(AV31count+1);
               BRKPD4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2108ContratoServicosVnc_Descricao)) )
            {
               AV23Option = A2108ContratoServicosVnc_Descricao;
               AV24Options.Add(AV23Option, 0);
               AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPD4 )
            {
               BRKPD4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOSRVVNC_SERVICOVNCSIGLAOPTIONS' Routine */
         AV14TFContratoSrvVnc_ServicoVncSigla = AV19SearchTxt;
         AV15TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV10TFContratoSrvVnc_ServicoSigla;
         AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV11TFContratoSrvVnc_ServicoSigla_Sel;
         AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV41TFContratoServicosVnc_Descricao;
         AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV42TFContratoServicosVnc_Descricao_Sel;
         AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV13TFContratoSrvVnc_StatusDmn_Sels;
         AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV14TFContratoSrvVnc_ServicoVncSigla;
         AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV15TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV16TFContratoSrvVnc_PrestadoraPesNom;
         AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV17TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV37TFContratoSrvVnc_SrvVncCntNum;
         AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV38TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV39TFContratoServicosVnc_ClonarLink_Sel;
         AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV18TFContratoSrvVnc_SemCusto_Sel;
         AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV40TFContratoServicosVnc_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                              AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1818ContratoServicosVnc_ClonarLink ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              A1092ContratoSrvVnc_PrestadoraPesNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
         lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
         lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
         lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
         lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
         /* Using cursor P00PD10 */
         pr_default.execute(2, new Object[] {AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKPD6 = false;
            A915ContratoSrvVnc_CntSrvCod = P00PD10_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PD10_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD10_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD10_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD10_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD10_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD10_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD10_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD10_n923ContratoSrvVnc_ServicoVncCod[0];
            A917ContratoSrvVnc_Codigo = P00PD10_A917ContratoSrvVnc_Codigo[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD10_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD10_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1453ContratoServicosVnc_Ativo = P00PD10_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00PD10_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00PD10_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00PD10_n1090ContratoSrvVnc_SemCusto[0];
            A1818ContratoServicosVnc_ClonarLink = P00PD10_A1818ContratoServicosVnc_ClonarLink[0];
            n1818ContratoServicosVnc_ClonarLink = P00PD10_n1818ContratoServicosVnc_ClonarLink[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD10_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD10_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A1084ContratoSrvVnc_StatusDmn = P00PD10_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PD10_n1084ContratoSrvVnc_StatusDmn[0];
            A2108ContratoServicosVnc_Descricao = P00PD10_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00PD10_n2108ContratoServicosVnc_Descricao[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD10_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD10_n922ContratoSrvVnc_ServicoSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD10_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD10_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            A921ContratoSrvVnc_ServicoCod = P00PD10_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD10_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD10_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD10_n922ContratoSrvVnc_ServicoSigla[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD10_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD10_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD10_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD10_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD10_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD10_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD10_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD10_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD10_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD10_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            AV31count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00PD10_A924ContratoSrvVnc_ServicoVncSigla[0], A924ContratoSrvVnc_ServicoVncSigla) == 0 ) )
            {
               BRKPD6 = false;
               A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD10_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD10_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A923ContratoSrvVnc_ServicoVncCod = P00PD10_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = P00PD10_n923ContratoSrvVnc_ServicoVncCod[0];
               A917ContratoSrvVnc_Codigo = P00PD10_A917ContratoSrvVnc_Codigo[0];
               A923ContratoSrvVnc_ServicoVncCod = P00PD10_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = P00PD10_n923ContratoSrvVnc_ServicoVncCod[0];
               AV31count = (long)(AV31count+1);
               BRKPD6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla)) )
            {
               AV23Option = A924ContratoSrvVnc_ServicoVncSigla;
               AV24Options.Add(AV23Option, 0);
               AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPD6 )
            {
               BRKPD6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOSRVVNC_PRESTADORAPESNOMOPTIONS' Routine */
         AV16TFContratoSrvVnc_PrestadoraPesNom = AV19SearchTxt;
         AV17TFContratoSrvVnc_PrestadoraPesNom_Sel = "";
         AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV10TFContratoSrvVnc_ServicoSigla;
         AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV11TFContratoSrvVnc_ServicoSigla_Sel;
         AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV41TFContratoServicosVnc_Descricao;
         AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV42TFContratoServicosVnc_Descricao_Sel;
         AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV13TFContratoSrvVnc_StatusDmn_Sels;
         AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV14TFContratoSrvVnc_ServicoVncSigla;
         AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV15TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV16TFContratoSrvVnc_PrestadoraPesNom;
         AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV17TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV37TFContratoSrvVnc_SrvVncCntNum;
         AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV38TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV39TFContratoServicosVnc_ClonarLink_Sel;
         AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV18TFContratoSrvVnc_SemCusto_Sel;
         AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV40TFContratoServicosVnc_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                              AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1818ContratoServicosVnc_ClonarLink ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              A1092ContratoSrvVnc_PrestadoraPesNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
         lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
         lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
         lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
         lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
         /* Using cursor P00PD13 */
         pr_default.execute(3, new Object[] {AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A915ContratoSrvVnc_CntSrvCod = P00PD13_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PD13_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD13_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD13_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD13_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD13_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD13_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD13_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD13_n923ContratoSrvVnc_ServicoVncCod[0];
            A917ContratoSrvVnc_Codigo = P00PD13_A917ContratoSrvVnc_Codigo[0];
            A1453ContratoServicosVnc_Ativo = P00PD13_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00PD13_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00PD13_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00PD13_n1090ContratoSrvVnc_SemCusto[0];
            A1818ContratoServicosVnc_ClonarLink = P00PD13_A1818ContratoServicosVnc_ClonarLink[0];
            n1818ContratoServicosVnc_ClonarLink = P00PD13_n1818ContratoServicosVnc_ClonarLink[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD13_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD13_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD13_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD13_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00PD13_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PD13_n1084ContratoSrvVnc_StatusDmn[0];
            A2108ContratoServicosVnc_Descricao = P00PD13_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00PD13_n2108ContratoServicosVnc_Descricao[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD13_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD13_n922ContratoSrvVnc_ServicoSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD13_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD13_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            A921ContratoSrvVnc_ServicoCod = P00PD13_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD13_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD13_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD13_n922ContratoSrvVnc_ServicoSigla[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD13_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD13_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD13_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD13_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD13_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD13_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD13_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD13_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD13_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD13_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom)) )
            {
               AV23Option = A1092ContratoSrvVnc_PrestadoraPesNom;
               AV22InsertIndex = 1;
               while ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) < 0 ) )
               {
                  AV22InsertIndex = (int)(AV22InsertIndex+1);
               }
               if ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) == 0 ) )
               {
                  AV31count = (long)(NumberUtil.Val( ((String)AV29OptionIndexes.Item(AV22InsertIndex)), "."));
                  AV31count = (long)(AV31count+1);
                  AV29OptionIndexes.RemoveItem(AV22InsertIndex);
                  AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), AV22InsertIndex);
               }
               else
               {
                  AV24Options.Add(AV23Option, AV22InsertIndex);
                  AV29OptionIndexes.Add("1", AV22InsertIndex);
               }
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATOSRVVNC_SRVVNCCNTNUMOPTIONS' Routine */
         AV37TFContratoSrvVnc_SrvVncCntNum = AV19SearchTxt;
         AV38TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV10TFContratoSrvVnc_ServicoSigla;
         AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV11TFContratoSrvVnc_ServicoSigla_Sel;
         AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV41TFContratoServicosVnc_Descricao;
         AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV42TFContratoServicosVnc_Descricao_Sel;
         AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV13TFContratoSrvVnc_StatusDmn_Sels;
         AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV14TFContratoSrvVnc_ServicoVncSigla;
         AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV15TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV16TFContratoSrvVnc_PrestadoraPesNom;
         AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV17TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV37TFContratoSrvVnc_SrvVncCntNum;
         AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV38TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV39TFContratoServicosVnc_ClonarLink_Sel;
         AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV18TFContratoSrvVnc_SemCusto_Sel;
         AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV40TFContratoServicosVnc_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                              AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1818ContratoServicosVnc_ClonarLink ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              A1092ContratoSrvVnc_PrestadoraPesNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
         lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
         lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
         lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
         lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
         /* Using cursor P00PD16 */
         pr_default.execute(4, new Object[] {AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKPD9 = false;
            A915ContratoSrvVnc_CntSrvCod = P00PD16_A915ContratoSrvVnc_CntSrvCod[0];
            A921ContratoSrvVnc_ServicoCod = P00PD16_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD16_n921ContratoSrvVnc_ServicoCod[0];
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD16_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD16_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD16_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD16_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD16_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD16_n923ContratoSrvVnc_ServicoVncCod[0];
            A917ContratoSrvVnc_Codigo = P00PD16_A917ContratoSrvVnc_Codigo[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD16_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD16_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A1453ContratoServicosVnc_Ativo = P00PD16_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00PD16_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00PD16_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00PD16_n1090ContratoSrvVnc_SemCusto[0];
            A1818ContratoServicosVnc_ClonarLink = P00PD16_A1818ContratoServicosVnc_ClonarLink[0];
            n1818ContratoServicosVnc_ClonarLink = P00PD16_n1818ContratoServicosVnc_ClonarLink[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD16_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD16_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00PD16_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00PD16_n1084ContratoSrvVnc_StatusDmn[0];
            A2108ContratoServicosVnc_Descricao = P00PD16_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00PD16_n2108ContratoServicosVnc_Descricao[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD16_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD16_n922ContratoSrvVnc_ServicoSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD16_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD16_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            A921ContratoSrvVnc_ServicoCod = P00PD16_A921ContratoSrvVnc_ServicoCod[0];
            n921ContratoSrvVnc_ServicoCod = P00PD16_n921ContratoSrvVnc_ServicoCod[0];
            A922ContratoSrvVnc_ServicoSigla = P00PD16_A922ContratoSrvVnc_ServicoSigla[0];
            n922ContratoSrvVnc_ServicoSigla = P00PD16_n922ContratoSrvVnc_ServicoSigla[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00PD16_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00PD16_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00PD16_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00PD16_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00PD16_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00PD16_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00PD16_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00PD16_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1092ContratoSrvVnc_PrestadoraPesNom = P00PD16_A1092ContratoSrvVnc_PrestadoraPesNom[0];
            n1092ContratoSrvVnc_PrestadoraPesNom = P00PD16_n1092ContratoSrvVnc_PrestadoraPesNom[0];
            AV31count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00PD16_A1631ContratoSrvVnc_SrvVncCntNum[0], A1631ContratoSrvVnc_SrvVncCntNum) == 0 ) )
            {
               BRKPD9 = false;
               A1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD16_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = P00PD16_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A1628ContratoSrvVnc_SrvVncCntCod = P00PD16_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = P00PD16_n1628ContratoSrvVnc_SrvVncCntCod[0];
               A917ContratoSrvVnc_Codigo = P00PD16_A917ContratoSrvVnc_Codigo[0];
               A1628ContratoSrvVnc_SrvVncCntCod = P00PD16_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = P00PD16_n1628ContratoSrvVnc_SrvVncCntCod[0];
               AV31count = (long)(AV31count+1);
               BRKPD9 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum)) )
            {
               AV23Option = A1631ContratoSrvVnc_SrvVncCntNum;
               AV24Options.Add(AV23Option, 0);
               AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPD9 )
            {
               BRKPD9 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV24Options = new GxSimpleCollection();
         AV27OptionsDesc = new GxSimpleCollection();
         AV29OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV32Session = context.GetSession();
         AV34GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV35GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoSrvVnc_ServicoSigla = "";
         AV11TFContratoSrvVnc_ServicoSigla_Sel = "";
         AV41TFContratoServicosVnc_Descricao = "";
         AV42TFContratoServicosVnc_Descricao_Sel = "";
         AV12TFContratoSrvVnc_StatusDmn_SelsJson = "";
         AV13TFContratoSrvVnc_StatusDmn_Sels = new GxSimpleCollection();
         AV14TFContratoSrvVnc_ServicoVncSigla = "";
         AV15TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         AV16TFContratoSrvVnc_PrestadoraPesNom = "";
         AV17TFContratoSrvVnc_PrestadoraPesNom_Sel = "";
         AV37TFContratoSrvVnc_SrvVncCntNum = "";
         AV38TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = "";
         AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = "";
         AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = "";
         AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = "";
         AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = new GxSimpleCollection();
         AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = "";
         AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = "";
         AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = "";
         AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = "";
         AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = "";
         AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = "";
         lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = "";
         scmdbuf = "";
         lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = "";
         lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = "";
         lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = "";
         lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A922ContratoSrvVnc_ServicoSigla = "";
         A2108ContratoServicosVnc_Descricao = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         A1631ContratoSrvVnc_SrvVncCntNum = "";
         A1092ContratoSrvVnc_PrestadoraPesNom = "";
         P00PD4_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PD4_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PD4_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PD4_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PD4_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PD4_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00PD4_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00PD4_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PD4_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PD4_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00PD4_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PD4_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PD4_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD4_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD4_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD4_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD4_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD4_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD4_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00PD4_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00PD4_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PD4_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PD4_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PD4_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PD4_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00PD4_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00PD4_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         P00PD4_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         AV23Option = "";
         P00PD7_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PD7_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PD7_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PD7_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PD7_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PD7_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00PD7_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00PD7_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PD7_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PD7_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00PD7_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00PD7_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00PD7_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD7_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD7_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD7_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD7_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD7_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD7_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00PD7_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00PD7_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PD7_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PD7_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PD7_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PD7_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PD7_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PD7_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         P00PD7_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         P00PD10_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PD10_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PD10_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PD10_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PD10_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PD10_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00PD10_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00PD10_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PD10_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PD10_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00PD10_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PD10_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PD10_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD10_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD10_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD10_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD10_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD10_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD10_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00PD10_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00PD10_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PD10_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PD10_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00PD10_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00PD10_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PD10_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PD10_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         P00PD10_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         P00PD13_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PD13_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PD13_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PD13_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PD13_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PD13_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00PD13_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00PD13_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PD13_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PD13_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00PD13_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD13_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD13_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD13_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD13_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD13_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD13_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00PD13_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00PD13_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PD13_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PD13_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PD13_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PD13_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00PD13_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00PD13_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PD13_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PD13_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         P00PD13_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         P00PD16_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00PD16_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         P00PD16_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         P00PD16_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00PD16_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00PD16_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00PD16_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00PD16_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00PD16_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00PD16_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00PD16_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00PD16_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00PD16_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD16_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00PD16_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD16_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00PD16_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD16_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         P00PD16_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00PD16_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00PD16_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00PD16_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00PD16_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00PD16_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00PD16_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         P00PD16_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         P00PD16_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         P00PD16_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoservicosvncfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PD4_A915ContratoSrvVnc_CntSrvCod, P00PD4_A921ContratoSrvVnc_ServicoCod, P00PD4_n921ContratoSrvVnc_ServicoCod, P00PD4_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD4_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD4_A1628ContratoSrvVnc_SrvVncCntCod, P00PD4_n1628ContratoSrvVnc_SrvVncCntCod, P00PD4_A923ContratoSrvVnc_ServicoVncCod, P00PD4_n923ContratoSrvVnc_ServicoVncCod, P00PD4_A917ContratoSrvVnc_Codigo,
               P00PD4_A922ContratoSrvVnc_ServicoSigla, P00PD4_n922ContratoSrvVnc_ServicoSigla, P00PD4_A1453ContratoServicosVnc_Ativo, P00PD4_n1453ContratoServicosVnc_Ativo, P00PD4_A1090ContratoSrvVnc_SemCusto, P00PD4_n1090ContratoSrvVnc_SemCusto, P00PD4_A1818ContratoServicosVnc_ClonarLink, P00PD4_n1818ContratoServicosVnc_ClonarLink, P00PD4_A1631ContratoSrvVnc_SrvVncCntNum, P00PD4_n1631ContratoSrvVnc_SrvVncCntNum,
               P00PD4_A924ContratoSrvVnc_ServicoVncSigla, P00PD4_n924ContratoSrvVnc_ServicoVncSigla, P00PD4_A1084ContratoSrvVnc_StatusDmn, P00PD4_n1084ContratoSrvVnc_StatusDmn, P00PD4_A2108ContratoServicosVnc_Descricao, P00PD4_n2108ContratoServicosVnc_Descricao, P00PD4_A1092ContratoSrvVnc_PrestadoraPesNom, P00PD4_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               P00PD7_A915ContratoSrvVnc_CntSrvCod, P00PD7_A921ContratoSrvVnc_ServicoCod, P00PD7_n921ContratoSrvVnc_ServicoCod, P00PD7_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD7_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD7_A1628ContratoSrvVnc_SrvVncCntCod, P00PD7_n1628ContratoSrvVnc_SrvVncCntCod, P00PD7_A923ContratoSrvVnc_ServicoVncCod, P00PD7_n923ContratoSrvVnc_ServicoVncCod, P00PD7_A917ContratoSrvVnc_Codigo,
               P00PD7_A2108ContratoServicosVnc_Descricao, P00PD7_n2108ContratoServicosVnc_Descricao, P00PD7_A1453ContratoServicosVnc_Ativo, P00PD7_n1453ContratoServicosVnc_Ativo, P00PD7_A1090ContratoSrvVnc_SemCusto, P00PD7_n1090ContratoSrvVnc_SemCusto, P00PD7_A1818ContratoServicosVnc_ClonarLink, P00PD7_n1818ContratoServicosVnc_ClonarLink, P00PD7_A1631ContratoSrvVnc_SrvVncCntNum, P00PD7_n1631ContratoSrvVnc_SrvVncCntNum,
               P00PD7_A924ContratoSrvVnc_ServicoVncSigla, P00PD7_n924ContratoSrvVnc_ServicoVncSigla, P00PD7_A1084ContratoSrvVnc_StatusDmn, P00PD7_n1084ContratoSrvVnc_StatusDmn, P00PD7_A922ContratoSrvVnc_ServicoSigla, P00PD7_n922ContratoSrvVnc_ServicoSigla, P00PD7_A1092ContratoSrvVnc_PrestadoraPesNom, P00PD7_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               P00PD10_A915ContratoSrvVnc_CntSrvCod, P00PD10_A921ContratoSrvVnc_ServicoCod, P00PD10_n921ContratoSrvVnc_ServicoCod, P00PD10_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD10_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD10_A1628ContratoSrvVnc_SrvVncCntCod, P00PD10_n1628ContratoSrvVnc_SrvVncCntCod, P00PD10_A923ContratoSrvVnc_ServicoVncCod, P00PD10_n923ContratoSrvVnc_ServicoVncCod, P00PD10_A917ContratoSrvVnc_Codigo,
               P00PD10_A924ContratoSrvVnc_ServicoVncSigla, P00PD10_n924ContratoSrvVnc_ServicoVncSigla, P00PD10_A1453ContratoServicosVnc_Ativo, P00PD10_n1453ContratoServicosVnc_Ativo, P00PD10_A1090ContratoSrvVnc_SemCusto, P00PD10_n1090ContratoSrvVnc_SemCusto, P00PD10_A1818ContratoServicosVnc_ClonarLink, P00PD10_n1818ContratoServicosVnc_ClonarLink, P00PD10_A1631ContratoSrvVnc_SrvVncCntNum, P00PD10_n1631ContratoSrvVnc_SrvVncCntNum,
               P00PD10_A1084ContratoSrvVnc_StatusDmn, P00PD10_n1084ContratoSrvVnc_StatusDmn, P00PD10_A2108ContratoServicosVnc_Descricao, P00PD10_n2108ContratoServicosVnc_Descricao, P00PD10_A922ContratoSrvVnc_ServicoSigla, P00PD10_n922ContratoSrvVnc_ServicoSigla, P00PD10_A1092ContratoSrvVnc_PrestadoraPesNom, P00PD10_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               P00PD13_A915ContratoSrvVnc_CntSrvCod, P00PD13_A921ContratoSrvVnc_ServicoCod, P00PD13_n921ContratoSrvVnc_ServicoCod, P00PD13_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD13_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD13_A1628ContratoSrvVnc_SrvVncCntCod, P00PD13_n1628ContratoSrvVnc_SrvVncCntCod, P00PD13_A923ContratoSrvVnc_ServicoVncCod, P00PD13_n923ContratoSrvVnc_ServicoVncCod, P00PD13_A917ContratoSrvVnc_Codigo,
               P00PD13_A1453ContratoServicosVnc_Ativo, P00PD13_n1453ContratoServicosVnc_Ativo, P00PD13_A1090ContratoSrvVnc_SemCusto, P00PD13_n1090ContratoSrvVnc_SemCusto, P00PD13_A1818ContratoServicosVnc_ClonarLink, P00PD13_n1818ContratoServicosVnc_ClonarLink, P00PD13_A1631ContratoSrvVnc_SrvVncCntNum, P00PD13_n1631ContratoSrvVnc_SrvVncCntNum, P00PD13_A924ContratoSrvVnc_ServicoVncSigla, P00PD13_n924ContratoSrvVnc_ServicoVncSigla,
               P00PD13_A1084ContratoSrvVnc_StatusDmn, P00PD13_n1084ContratoSrvVnc_StatusDmn, P00PD13_A2108ContratoServicosVnc_Descricao, P00PD13_n2108ContratoServicosVnc_Descricao, P00PD13_A922ContratoSrvVnc_ServicoSigla, P00PD13_n922ContratoSrvVnc_ServicoSigla, P00PD13_A1092ContratoSrvVnc_PrestadoraPesNom, P00PD13_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               P00PD16_A915ContratoSrvVnc_CntSrvCod, P00PD16_A921ContratoSrvVnc_ServicoCod, P00PD16_n921ContratoSrvVnc_ServicoCod, P00PD16_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD16_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00PD16_A1628ContratoSrvVnc_SrvVncCntCod, P00PD16_n1628ContratoSrvVnc_SrvVncCntCod, P00PD16_A923ContratoSrvVnc_ServicoVncCod, P00PD16_n923ContratoSrvVnc_ServicoVncCod, P00PD16_A917ContratoSrvVnc_Codigo,
               P00PD16_A1631ContratoSrvVnc_SrvVncCntNum, P00PD16_n1631ContratoSrvVnc_SrvVncCntNum, P00PD16_A1453ContratoServicosVnc_Ativo, P00PD16_n1453ContratoServicosVnc_Ativo, P00PD16_A1090ContratoSrvVnc_SemCusto, P00PD16_n1090ContratoSrvVnc_SemCusto, P00PD16_A1818ContratoServicosVnc_ClonarLink, P00PD16_n1818ContratoServicosVnc_ClonarLink, P00PD16_A924ContratoSrvVnc_ServicoVncSigla, P00PD16_n924ContratoSrvVnc_ServicoVncSigla,
               P00PD16_A1084ContratoSrvVnc_StatusDmn, P00PD16_n1084ContratoSrvVnc_StatusDmn, P00PD16_A2108ContratoServicosVnc_Descricao, P00PD16_n2108ContratoServicosVnc_Descricao, P00PD16_A922ContratoSrvVnc_ServicoSigla, P00PD16_n922ContratoSrvVnc_ServicoSigla, P00PD16_A1092ContratoSrvVnc_PrestadoraPesNom, P00PD16_n1092ContratoSrvVnc_PrestadoraPesNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV39TFContratoServicosVnc_ClonarLink_Sel ;
      private short AV18TFContratoSrvVnc_SemCusto_Sel ;
      private short AV40TFContratoServicosVnc_Ativo_Sel ;
      private short AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ;
      private short AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ;
      private short AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ;
      private int AV45GXV1 ;
      private int AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private int AV22InsertIndex ;
      private long AV31count ;
      private String AV10TFContratoSrvVnc_ServicoSigla ;
      private String AV11TFContratoSrvVnc_ServicoSigla_Sel ;
      private String AV14TFContratoSrvVnc_ServicoVncSigla ;
      private String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ;
      private String AV16TFContratoSrvVnc_PrestadoraPesNom ;
      private String AV17TFContratoSrvVnc_PrestadoraPesNom_Sel ;
      private String AV37TFContratoSrvVnc_SrvVncCntNum ;
      private String AV38TFContratoSrvVnc_SrvVncCntNum_Sel ;
      private String AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ;
      private String AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ;
      private String AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ;
      private String AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ;
      private String AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ;
      private String AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ;
      private String AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ;
      private String AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ;
      private String lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ;
      private String scmdbuf ;
      private String lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ;
      private String lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ;
      private String lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String A1631ContratoSrvVnc_SrvVncCntNum ;
      private String A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool returnInSub ;
      private bool A1818ContratoServicosVnc_ClonarLink ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool BRKPD2 ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool n1818ContratoServicosVnc_ClonarLink ;
      private bool n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool BRKPD4 ;
      private bool BRKPD6 ;
      private bool BRKPD9 ;
      private String AV30OptionIndexesJson ;
      private String AV25OptionsJson ;
      private String AV28OptionsDescJson ;
      private String AV12TFContratoSrvVnc_StatusDmn_SelsJson ;
      private String AV21DDOName ;
      private String AV19SearchTxt ;
      private String AV20SearchTxtTo ;
      private String AV41TFContratoServicosVnc_Descricao ;
      private String AV42TFContratoServicosVnc_Descricao_Sel ;
      private String AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ;
      private String AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ;
      private String lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ;
      private String A2108ContratoServicosVnc_Descricao ;
      private String AV23Option ;
      private IGxSession AV32Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00PD4_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PD4_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PD4_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PD4_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PD4_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PD4_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00PD4_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00PD4_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PD4_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00PD4_A917ContratoSrvVnc_Codigo ;
      private String[] P00PD4_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PD4_n922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PD4_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD4_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD4_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD4_n1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD4_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P00PD4_n1818ContratoServicosVnc_ClonarLink ;
      private String[] P00PD4_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00PD4_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] P00PD4_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PD4_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00PD4_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PD4_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00PD4_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00PD4_n2108ContratoServicosVnc_Descricao ;
      private String[] P00PD4_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] P00PD4_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private int[] P00PD7_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PD7_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PD7_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PD7_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PD7_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PD7_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00PD7_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00PD7_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PD7_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00PD7_A917ContratoSrvVnc_Codigo ;
      private String[] P00PD7_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00PD7_n2108ContratoServicosVnc_Descricao ;
      private bool[] P00PD7_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD7_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD7_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD7_n1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD7_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P00PD7_n1818ContratoServicosVnc_ClonarLink ;
      private String[] P00PD7_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00PD7_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] P00PD7_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PD7_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00PD7_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PD7_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00PD7_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PD7_n922ContratoSrvVnc_ServicoSigla ;
      private String[] P00PD7_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] P00PD7_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private int[] P00PD10_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PD10_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PD10_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PD10_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PD10_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PD10_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00PD10_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00PD10_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PD10_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00PD10_A917ContratoSrvVnc_Codigo ;
      private String[] P00PD10_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PD10_n924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PD10_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD10_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD10_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD10_n1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD10_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P00PD10_n1818ContratoServicosVnc_ClonarLink ;
      private String[] P00PD10_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00PD10_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] P00PD10_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PD10_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00PD10_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00PD10_n2108ContratoServicosVnc_Descricao ;
      private String[] P00PD10_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PD10_n922ContratoSrvVnc_ServicoSigla ;
      private String[] P00PD10_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] P00PD10_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private int[] P00PD13_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PD13_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PD13_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PD13_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PD13_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PD13_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00PD13_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00PD13_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PD13_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00PD13_A917ContratoSrvVnc_Codigo ;
      private bool[] P00PD13_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD13_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD13_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD13_n1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD13_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P00PD13_n1818ContratoServicosVnc_ClonarLink ;
      private String[] P00PD13_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00PD13_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] P00PD13_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PD13_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00PD13_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PD13_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00PD13_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00PD13_n2108ContratoServicosVnc_Descricao ;
      private String[] P00PD13_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PD13_n922ContratoSrvVnc_ServicoSigla ;
      private String[] P00PD13_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] P00PD13_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private int[] P00PD16_A915ContratoSrvVnc_CntSrvCod ;
      private int[] P00PD16_A921ContratoSrvVnc_ServicoCod ;
      private bool[] P00PD16_n921ContratoSrvVnc_ServicoCod ;
      private int[] P00PD16_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00PD16_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00PD16_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00PD16_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00PD16_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00PD16_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00PD16_A917ContratoSrvVnc_Codigo ;
      private String[] P00PD16_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00PD16_n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00PD16_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD16_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00PD16_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD16_n1090ContratoSrvVnc_SemCusto ;
      private bool[] P00PD16_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] P00PD16_n1818ContratoServicosVnc_ClonarLink ;
      private String[] P00PD16_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00PD16_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00PD16_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00PD16_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00PD16_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00PD16_n2108ContratoServicosVnc_Descricao ;
      private String[] P00PD16_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] P00PD16_n922ContratoSrvVnc_ServicoSigla ;
      private String[] P00PD16_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] P00PD16_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV34GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV35GridStateFilterValue ;
   }

   public class getwwcontratoservicosvncfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PD4( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                             String AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                             String AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                             String AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                             String AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                             int AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                             String AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                             String AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                             String AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                             String AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                             short AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                             short AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                             short AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1818ContratoServicosVnc_ClonarLink ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             String AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                             String AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                             String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_Codigo], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_ClonarLink], T5.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoServicosVnc_Descricao], COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo]";
         scmdbuf = scmdbuf + " = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( (@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         scmdbuf = scmdbuf + " and ((@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00PD7( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                             String AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                             String AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                             String AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                             String AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                             int AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                             String AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                             String AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                             String AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                             String AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                             short AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                             short AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                             short AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1818ContratoServicosVnc_ClonarLink ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             String AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                             String AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                             String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_Codigo], T1.[ContratoServicosVnc_Descricao], T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_ClonarLink], T5.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo]";
         scmdbuf = scmdbuf + " = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( (@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         scmdbuf = scmdbuf + " and ((@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosVnc_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00PD10( IGxContext context ,
                                              String A1084ContratoSrvVnc_StatusDmn ,
                                              IGxCollection AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              String AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              String AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              String AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              String AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              int AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                              String AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              String AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              String AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              String AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              short AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              short AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              short AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              String A922ContratoSrvVnc_ServicoSigla ,
                                              String A2108ContratoServicosVnc_Descricao ,
                                              String A924ContratoSrvVnc_ServicoVncSigla ,
                                              String A1631ContratoSrvVnc_SrvVncCntNum ,
                                              bool A1818ContratoServicosVnc_ClonarLink ,
                                              bool A1090ContratoSrvVnc_SemCusto ,
                                              bool A1453ContratoServicosVnc_Ativo ,
                                              String AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              String AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [13] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_Codigo], T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_ClonarLink], T5.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoServicosVnc_Descricao], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo]";
         scmdbuf = scmdbuf + " = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( (@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         scmdbuf = scmdbuf + " and ((@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Sigla]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00PD13( IGxContext context ,
                                              String A1084ContratoSrvVnc_StatusDmn ,
                                              IGxCollection AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              String AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              String AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              String AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              String AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              int AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                              String AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              String AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              String AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              String AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              short AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              short AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              short AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              String A922ContratoSrvVnc_ServicoSigla ,
                                              String A2108ContratoServicosVnc_Descricao ,
                                              String A924ContratoSrvVnc_ServicoVncSigla ,
                                              String A1631ContratoSrvVnc_SrvVncCntNum ,
                                              bool A1818ContratoServicosVnc_ClonarLink ,
                                              bool A1090ContratoSrvVnc_SemCusto ,
                                              bool A1453ContratoServicosVnc_Ativo ,
                                              String AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              String AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [13] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_Codigo], T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_ClonarLink], T5.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoServicosVnc_Descricao], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo]";
         scmdbuf = scmdbuf + " = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( (@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         scmdbuf = scmdbuf + " and ((@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoSrvVnc_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00PD16( IGxContext context ,
                                              String A1084ContratoSrvVnc_StatusDmn ,
                                              IGxCollection AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              String AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              String AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              String AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              String AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              int AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                              String AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              String AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              String AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              String AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              short AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              short AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              short AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              String A922ContratoSrvVnc_ServicoSigla ,
                                              String A2108ContratoServicosVnc_Descricao ,
                                              String A924ContratoSrvVnc_ServicoVncSigla ,
                                              String A1631ContratoSrvVnc_SrvVncCntNum ,
                                              bool A1818ContratoServicosVnc_ClonarLink ,
                                              bool A1090ContratoSrvVnc_SemCusto ,
                                              bool A1453ContratoServicosVnc_Ativo ,
                                              String AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              String AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [13] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_Codigo], T5.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_ClonarLink], T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoServicosVnc_Descricao], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo]";
         scmdbuf = scmdbuf + " = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( (@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         scmdbuf = scmdbuf + " and ((@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV58WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV59WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV60WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T5.[Contrato_Numero]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PD4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
               case 1 :
                     return conditional_P00PD7(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
               case 2 :
                     return conditional_P00PD10(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
               case 3 :
                     return conditional_P00PD13(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
               case 4 :
                     return conditional_P00PD16(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PD4 ;
          prmP00PD4 = new Object[] {
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00PD7 ;
          prmP00PD7 = new Object[] {
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00PD10 ;
          prmP00PD10 = new Object[] {
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00PD13 ;
          prmP00PD13 = new Object[] {
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00PD16 ;
          prmP00PD16 = new Object[] {
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV48WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV53WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV56WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV57WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PD4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PD4,100,0,true,false )
             ,new CursorDef("P00PD7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PD7,100,0,true,false )
             ,new CursorDef("P00PD10", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PD10,100,0,true,false )
             ,new CursorDef("P00PD13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PD13,100,0,false,false )
             ,new CursorDef("P00PD16", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PD16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((bool[]) buf[16])[0] = rslt.getBool(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoservicosvncfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoservicosvncfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoservicosvncfilterdata") )
          {
             return  ;
          }
          getwwcontratoservicosvncfilterdata worker = new getwwcontratoservicosvncfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
