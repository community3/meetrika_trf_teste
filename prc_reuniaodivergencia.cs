/*
               File: PRC_ReuniaoDivergencia
        Description: Reuniao para tratamento de divergencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:35.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_reuniaodivergencia : GXProcedure
   {
      public prc_reuniaodivergencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_reuniaodivergencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_OsVinculada ,
                           int aP2_UserId ,
                           String aP3_Observacao ,
                           String aP4_UserName ,
                           ref String aP5_Resultado )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV10OsVinculada = aP1_OsVinculada;
         this.AV23UserId = aP2_UserId;
         this.AV9Observacao = aP3_Observacao;
         this.AV21UserName = aP4_UserName;
         this.AV18Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV18Resultado;
      }

      public String executeUdp( int aP0_Codigo ,
                                int aP1_OsVinculada ,
                                int aP2_UserId ,
                                String aP3_Observacao ,
                                String aP4_UserName )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV10OsVinculada = aP1_OsVinculada;
         this.AV23UserId = aP2_UserId;
         this.AV9Observacao = aP3_Observacao;
         this.AV21UserName = aP4_UserName;
         this.AV18Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV18Resultado;
         return AV18Resultado ;
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_OsVinculada ,
                                 int aP2_UserId ,
                                 String aP3_Observacao ,
                                 String aP4_UserName ,
                                 ref String aP5_Resultado )
      {
         prc_reuniaodivergencia objprc_reuniaodivergencia;
         objprc_reuniaodivergencia = new prc_reuniaodivergencia();
         objprc_reuniaodivergencia.AV8Codigo = aP0_Codigo;
         objprc_reuniaodivergencia.AV10OsVinculada = aP1_OsVinculada;
         objprc_reuniaodivergencia.AV23UserId = aP2_UserId;
         objprc_reuniaodivergencia.AV9Observacao = aP3_Observacao;
         objprc_reuniaodivergencia.AV21UserName = aP4_UserName;
         objprc_reuniaodivergencia.AV18Resultado = aP5_Resultado;
         objprc_reuniaodivergencia.context.SetSubmitInitialConfig(context);
         objprc_reuniaodivergencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_reuniaodivergencia);
         aP5_Resultado=this.AV18Resultado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_reuniaodivergencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008L2 */
         pr_default.execute(0, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P008L2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008L2_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P008L2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008L2_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P008L2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008L2_n1603ContagemResultado_CntCod[0];
            A1611ContagemResultado_PrzTpDias = P008L2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008L2_n1611ContagemResultado_PrzTpDias[0];
            A1618ContagemResultado_PrzRsp = P008L2_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P008L2_n1618ContagemResultado_PrzRsp[0];
            A912ContagemResultado_HoraEntrega = P008L2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008L2_n912ContagemResultado_HoraEntrega[0];
            A457ContagemResultado_Demanda = P008L2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P008L2_n457ContagemResultado_Demanda[0];
            A803ContagemResultado_ContratadaSigla = P008L2_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P008L2_n803ContagemResultado_ContratadaSigla[0];
            A805ContagemResultado_ContratadaOrigemCod = P008L2_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P008L2_n805ContagemResultado_ContratadaOrigemCod[0];
            A484ContagemResultado_StatusDmn = P008L2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008L2_n484ContagemResultado_StatusDmn[0];
            A1604ContagemResultado_CntPrpCod = P008L2_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P008L2_n1604ContagemResultado_CntPrpCod[0];
            A456ContagemResultado_Codigo = P008L2_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P008L2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P008L2_n52Contratada_AreaTrabalhoCod[0];
            A890ContagemResultado_Responsavel = P008L2_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P008L2_n890ContagemResultado_Responsavel[0];
            A803ContagemResultado_ContratadaSigla = P008L2_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P008L2_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P008L2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P008L2_n52Contratada_AreaTrabalhoCod[0];
            A1603ContagemResultado_CntCod = P008L2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008L2_n1603ContagemResultado_CntCod[0];
            A1611ContagemResultado_PrzTpDias = P008L2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008L2_n1611ContagemResultado_PrzTpDias[0];
            A1618ContagemResultado_PrzRsp = P008L2_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P008L2_n1618ContagemResultado_PrzRsp[0];
            A1604ContagemResultado_CntPrpCod = P008L2_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P008L2_n1604ContagemResultado_CntPrpCod[0];
            GXt_int1 = A1236ContagemResultado_ContratanteDoResponsavel;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int1) ;
            A1236ContagemResultado_ContratanteDoResponsavel = GXt_int1;
            GXt_int1 = A1229ContagemResultado_ContratadaDoResponsavel;
            new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int1) ;
            A1229ContagemResultado_ContratadaDoResponsavel = GXt_int1;
            AV30GXLvl5 = 0;
            /* Using cursor P008L3 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1797LogResponsavel_Codigo = P008L3_A1797LogResponsavel_Codigo[0];
               A896LogResponsavel_Owner = P008L3_A896LogResponsavel_Owner[0];
               A892LogResponsavel_DemandaCod = P008L3_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P008L3_n892LogResponsavel_DemandaCod[0];
               GXt_boolean2 = A1149LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean2) ;
               A1149LogResponsavel_OwnerEhContratante = GXt_boolean2;
               if ( ( A1229ContagemResultado_ContratadaDoResponsavel <= 0 ) || ( A1149LogResponsavel_OwnerEhContratante ) )
               {
                  if ( ( A1236ContagemResultado_ContratanteDoResponsavel <= 0 ) || ( ! A1149LogResponsavel_OwnerEhContratante ) )
                  {
                     AV30GXLvl5 = 1;
                     AV26Responsavel = A896LogResponsavel_Owner;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( AV30GXLvl5 == 0 )
            {
               /* Using cursor P008L4 */
               pr_default.execute(2, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P008L4_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P008L4_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P008L4_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P008L4_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P008L4_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P008L4_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
                  if ( ( A1229ContagemResultado_ContratadaDoResponsavel <= 0 ) || ( A1135ContratoGestor_UsuarioEhContratante ) )
                  {
                     if ( ( A1236ContagemResultado_ContratanteDoResponsavel <= 0 ) || ( ! A1135ContratoGestor_UsuarioEhContratante ) )
                     {
                        AV26Responsavel = A1079ContratoGestor_UsuarioCod;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                  }
                  pr_default.readNext(2);
               }
               pr_default.close(2);
            }
            if ( (0==AV26Responsavel) )
            {
               A890ContagemResultado_Responsavel = 0;
               n890ContagemResultado_Responsavel = false;
               n890ContagemResultado_Responsavel = true;
            }
            else
            {
               A890ContagemResultado_Responsavel = AV26Responsavel;
               n890ContagemResultado_Responsavel = false;
            }
            GXt_dtime3 = AV11PrazoEntrega;
            GXt_dtime4 = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
            new prc_adddiasuteis(context ).execute(  GXt_dtime4,  A1618ContagemResultado_PrzRsp,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime3) ;
            AV11PrazoEntrega = GXt_dtime3;
            AV11PrazoEntrega = DateTimeUtil.TAdd( AV11PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV11PrazoEntrega = DateTimeUtil.TAdd( AV11PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            AV20Demanda = A457ContagemResultado_Demanda;
            AV22Contratada_Sigla = StringUtil.Trim( A803ContagemResultado_ContratadaSigla);
            AV24ContratadaOrigem_Codigo = A805ContagemResultado_ContratadaOrigemCod;
            AV17AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV26Responsavel,  "RN",  "D",  AV23UserId,  0,  A484ContagemResultado_StatusDmn,  "B",  AV9Observacao,  AV11PrazoEntrega,  true) ;
            A484ContagemResultado_StatusDmn = "B";
            n484ContagemResultado_StatusDmn = false;
            AV12Usuarios.Add(A1604ContagemResultado_CntPrpCod, 0);
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A1079ContratoGestor_UsuarioCod ,
                                                 AV12Usuarios ,
                                                 A1135ContratoGestor_UsuarioEhContratante ,
                                                 A1603ContagemResultado_CntCod ,
                                                 A1078ContratoGestor_ContratoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor P008L5 */
            pr_default.execute(3, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P008L5_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P008L5_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P008L5_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P008L5_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P008L5_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P008L5_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV12Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Using cursor P008L6 */
            pr_default.execute(4, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV10OsVinculada ,
                                              A602ContagemResultado_OSVinculada ,
                                              AV8Codigo ,
                                              A456ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor P008L7 */
         pr_default.execute(5, new Object[] {AV8Codigo, AV10OsVinculada});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P008L7_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008L7_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P008L7_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008L7_n1603ContagemResultado_CntCod[0];
            A456ContagemResultado_Codigo = P008L7_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P008L7_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P008L7_n602ContagemResultado_OSVinculada[0];
            A472ContagemResultado_DataEntrega = P008L7_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P008L7_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P008L7_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008L7_n912ContagemResultado_HoraEntrega[0];
            A484ContagemResultado_StatusDmn = P008L7_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008L7_n484ContagemResultado_StatusDmn[0];
            A1604ContagemResultado_CntPrpCod = P008L7_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P008L7_n1604ContagemResultado_CntPrpCod[0];
            A1603ContagemResultado_CntCod = P008L7_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008L7_n1603ContagemResultado_CntCod[0];
            A1604ContagemResultado_CntPrpCod = P008L7_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P008L7_n1604ContagemResultado_CntPrpCod[0];
            AV11PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV11PrazoEntrega = DateTimeUtil.TAdd( AV11PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV11PrazoEntrega = DateTimeUtil.TAdd( AV11PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  0,  "RN",  "D",  AV23UserId,  0,  A484ContagemResultado_StatusDmn,  "B",  AV9Observacao,  AV11PrazoEntrega,  true) ;
            A484ContagemResultado_StatusDmn = "B";
            n484ContagemResultado_StatusDmn = false;
            AV34GXLvl59 = 0;
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 A1079ContratoGestor_UsuarioCod ,
                                                 AV12Usuarios ,
                                                 A1135ContratoGestor_UsuarioEhContratante ,
                                                 A1603ContagemResultado_CntCod ,
                                                 A1078ContratoGestor_ContratoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor P008L8 */
            pr_default.execute(6, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P008L8_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P008L8_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P008L8_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P008L8_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P008L8_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P008L8_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
               if ( ! A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV34GXLvl59 = 1;
                  AV12Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
               }
               pr_default.readNext(6);
            }
            pr_default.close(6);
            if ( AV34GXLvl59 == 0 )
            {
               AV12Usuarios.Add(A1604ContagemResultado_CntPrpCod, 0);
            }
            /* Using cursor P008L9 */
            pr_default.execute(7, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( AV12Usuarios.Count > 0 )
         {
            AV14Subject = "OS: " + StringUtil.Trim( AV20Demanda) + " - Reuni�o para tratamento de diverg�ncia (No reply)";
            AV13EmailText = AV13EmailText + StringUtil.NewLine( ) + "Solicittamos uma reuni�o presencial." + StringUtil.NewLine( );
            AV13EmailText = AV13EmailText + "Observa��o: " + AV9Observacao + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV13EmailText = AV13EmailText + AV22Contratada_Sigla + ", usu�rio " + StringUtil.Trim( AV21UserName) + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV25ContagemResultado_Codigos.Add(AV8Codigo, 0);
            AV15WebSession.Set("DemandaCodigo", AV25ContagemResultado_Codigos.ToXml(false, true, "Collection", ""));
            AV18Resultado = "Env�ado ";
            new prc_enviaremail(context ).execute(  AV17AreaTrabalho_Codigo,  AV12Usuarios,  AV14Subject,  AV13EmailText,  AV19Attachments, ref  AV18Resultado) ;
            AV15WebSession.Remove("DemandaCodigo");
         }
         else
         {
            AV18Resultado = "N�o foram achados usu�rio de Financeiro para envio das demandas em diverg�ncia!";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ReuniaoDivergencia");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008L2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008L2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008L2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008L2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008L2_A1603ContagemResultado_CntCod = new int[1] ;
         P008L2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P008L2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008L2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008L2_A1618ContagemResultado_PrzRsp = new short[1] ;
         P008L2_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         P008L2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008L2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008L2_A457ContagemResultado_Demanda = new String[] {""} ;
         P008L2_n457ContagemResultado_Demanda = new bool[] {false} ;
         P008L2_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P008L2_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P008L2_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P008L2_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P008L2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008L2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008L2_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P008L2_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P008L2_A456ContagemResultado_Codigo = new int[1] ;
         P008L2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P008L2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P008L2_A890ContagemResultado_Responsavel = new int[1] ;
         P008L2_n890ContagemResultado_Responsavel = new bool[] {false} ;
         A1611ContagemResultado_PrzTpDias = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A457ContagemResultado_Demanda = "";
         A803ContagemResultado_ContratadaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         P008L3_A1797LogResponsavel_Codigo = new long[1] ;
         P008L3_A896LogResponsavel_Owner = new int[1] ;
         P008L3_A892LogResponsavel_DemandaCod = new int[1] ;
         P008L3_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008L4_A1078ContratoGestor_ContratoCod = new int[1] ;
         P008L4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P008L4_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P008L4_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV11PrazoEntrega = (DateTime)(DateTime.MinValue);
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         GXt_dtime4 = (DateTime)(DateTime.MinValue);
         AV20Demanda = "";
         AV22Contratada_Sigla = "";
         AV12Usuarios = new GxSimpleCollection();
         P008L5_A1078ContratoGestor_ContratoCod = new int[1] ;
         P008L5_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P008L5_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P008L5_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P008L7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008L7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008L7_A1603ContagemResultado_CntCod = new int[1] ;
         P008L7_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P008L7_A456ContagemResultado_Codigo = new int[1] ;
         P008L7_A602ContagemResultado_OSVinculada = new int[1] ;
         P008L7_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P008L7_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008L7_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008L7_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008L7_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008L7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008L7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008L7_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P008L7_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         P008L8_A1078ContratoGestor_ContratoCod = new int[1] ;
         P008L8_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P008L8_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P008L8_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV14Subject = "";
         AV13EmailText = "";
         AV25ContagemResultado_Codigos = new GxSimpleCollection();
         AV15WebSession = context.GetSession();
         AV19Attachments = new GxSimpleCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_reuniaodivergencia__default(),
            new Object[][] {
                new Object[] {
               P008L2_A490ContagemResultado_ContratadaCod, P008L2_n490ContagemResultado_ContratadaCod, P008L2_A1553ContagemResultado_CntSrvCod, P008L2_n1553ContagemResultado_CntSrvCod, P008L2_A1603ContagemResultado_CntCod, P008L2_n1603ContagemResultado_CntCod, P008L2_A1611ContagemResultado_PrzTpDias, P008L2_n1611ContagemResultado_PrzTpDias, P008L2_A1618ContagemResultado_PrzRsp, P008L2_n1618ContagemResultado_PrzRsp,
               P008L2_A912ContagemResultado_HoraEntrega, P008L2_n912ContagemResultado_HoraEntrega, P008L2_A457ContagemResultado_Demanda, P008L2_n457ContagemResultado_Demanda, P008L2_A803ContagemResultado_ContratadaSigla, P008L2_n803ContagemResultado_ContratadaSigla, P008L2_A805ContagemResultado_ContratadaOrigemCod, P008L2_n805ContagemResultado_ContratadaOrigemCod, P008L2_A484ContagemResultado_StatusDmn, P008L2_n484ContagemResultado_StatusDmn,
               P008L2_A1604ContagemResultado_CntPrpCod, P008L2_n1604ContagemResultado_CntPrpCod, P008L2_A456ContagemResultado_Codigo, P008L2_A52Contratada_AreaTrabalhoCod, P008L2_n52Contratada_AreaTrabalhoCod, P008L2_A890ContagemResultado_Responsavel, P008L2_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               P008L3_A1797LogResponsavel_Codigo, P008L3_A896LogResponsavel_Owner, P008L3_A892LogResponsavel_DemandaCod, P008L3_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P008L4_A1078ContratoGestor_ContratoCod, P008L4_A1079ContratoGestor_UsuarioCod, P008L4_A1446ContratoGestor_ContratadaAreaCod, P008L4_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P008L5_A1078ContratoGestor_ContratoCod, P008L5_A1079ContratoGestor_UsuarioCod, P008L5_A1446ContratoGestor_ContratadaAreaCod, P008L5_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               }
               , new Object[] {
               P008L7_A1553ContagemResultado_CntSrvCod, P008L7_n1553ContagemResultado_CntSrvCod, P008L7_A1603ContagemResultado_CntCod, P008L7_n1603ContagemResultado_CntCod, P008L7_A456ContagemResultado_Codigo, P008L7_A602ContagemResultado_OSVinculada, P008L7_n602ContagemResultado_OSVinculada, P008L7_A472ContagemResultado_DataEntrega, P008L7_n472ContagemResultado_DataEntrega, P008L7_A912ContagemResultado_HoraEntrega,
               P008L7_n912ContagemResultado_HoraEntrega, P008L7_A484ContagemResultado_StatusDmn, P008L7_n484ContagemResultado_StatusDmn, P008L7_A1604ContagemResultado_CntPrpCod, P008L7_n1604ContagemResultado_CntPrpCod
               }
               , new Object[] {
               P008L8_A1078ContratoGestor_ContratoCod, P008L8_A1079ContratoGestor_UsuarioCod, P008L8_A1446ContratoGestor_ContratadaAreaCod, P008L8_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1618ContagemResultado_PrzRsp ;
      private short AV30GXLvl5 ;
      private short AV34GXLvl59 ;
      private int AV8Codigo ;
      private int AV10OsVinculada ;
      private int AV23UserId ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1236ContagemResultado_ContratanteDoResponsavel ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int GXt_int1 ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int AV26Responsavel ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int AV24ContratadaOrigem_Codigo ;
      private int AV17AreaTrabalho_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private long A1797LogResponsavel_Codigo ;
      private String AV21UserName ;
      private String AV18Resultado ;
      private String scmdbuf ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV22Contratada_Sigla ;
      private String AV14Subject ;
      private String AV13EmailText ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV11PrazoEntrega ;
      private DateTime GXt_dtime3 ;
      private DateTime GXt_dtime4 ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1618ContagemResultado_PrzRsp ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n457ContagemResultado_Demanda ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool GXt_boolean2 ;
      private String AV9Observacao ;
      private String A457ContagemResultado_Demanda ;
      private String AV20Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP5_Resultado ;
      private IDataStoreProvider pr_default ;
      private int[] P008L2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008L2_n490ContagemResultado_ContratadaCod ;
      private int[] P008L2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008L2_n1553ContagemResultado_CntSrvCod ;
      private int[] P008L2_A1603ContagemResultado_CntCod ;
      private bool[] P008L2_n1603ContagemResultado_CntCod ;
      private String[] P008L2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008L2_n1611ContagemResultado_PrzTpDias ;
      private short[] P008L2_A1618ContagemResultado_PrzRsp ;
      private bool[] P008L2_n1618ContagemResultado_PrzRsp ;
      private DateTime[] P008L2_A912ContagemResultado_HoraEntrega ;
      private bool[] P008L2_n912ContagemResultado_HoraEntrega ;
      private String[] P008L2_A457ContagemResultado_Demanda ;
      private bool[] P008L2_n457ContagemResultado_Demanda ;
      private String[] P008L2_A803ContagemResultado_ContratadaSigla ;
      private bool[] P008L2_n803ContagemResultado_ContratadaSigla ;
      private int[] P008L2_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P008L2_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] P008L2_A484ContagemResultado_StatusDmn ;
      private bool[] P008L2_n484ContagemResultado_StatusDmn ;
      private int[] P008L2_A1604ContagemResultado_CntPrpCod ;
      private bool[] P008L2_n1604ContagemResultado_CntPrpCod ;
      private int[] P008L2_A456ContagemResultado_Codigo ;
      private int[] P008L2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P008L2_n52Contratada_AreaTrabalhoCod ;
      private int[] P008L2_A890ContagemResultado_Responsavel ;
      private bool[] P008L2_n890ContagemResultado_Responsavel ;
      private long[] P008L3_A1797LogResponsavel_Codigo ;
      private int[] P008L3_A896LogResponsavel_Owner ;
      private int[] P008L3_A892LogResponsavel_DemandaCod ;
      private bool[] P008L3_n892LogResponsavel_DemandaCod ;
      private int[] P008L4_A1078ContratoGestor_ContratoCod ;
      private int[] P008L4_A1079ContratoGestor_UsuarioCod ;
      private int[] P008L4_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P008L4_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P008L5_A1078ContratoGestor_ContratoCod ;
      private int[] P008L5_A1079ContratoGestor_UsuarioCod ;
      private int[] P008L5_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P008L5_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P008L7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008L7_n1553ContagemResultado_CntSrvCod ;
      private int[] P008L7_A1603ContagemResultado_CntCod ;
      private bool[] P008L7_n1603ContagemResultado_CntCod ;
      private int[] P008L7_A456ContagemResultado_Codigo ;
      private int[] P008L7_A602ContagemResultado_OSVinculada ;
      private bool[] P008L7_n602ContagemResultado_OSVinculada ;
      private DateTime[] P008L7_A472ContagemResultado_DataEntrega ;
      private bool[] P008L7_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008L7_A912ContagemResultado_HoraEntrega ;
      private bool[] P008L7_n912ContagemResultado_HoraEntrega ;
      private String[] P008L7_A484ContagemResultado_StatusDmn ;
      private bool[] P008L7_n484ContagemResultado_StatusDmn ;
      private int[] P008L7_A1604ContagemResultado_CntPrpCod ;
      private bool[] P008L7_n1604ContagemResultado_CntPrpCod ;
      private int[] P008L8_A1078ContratoGestor_ContratoCod ;
      private int[] P008L8_A1079ContratoGestor_UsuarioCod ;
      private int[] P008L8_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P008L8_n1446ContratoGestor_ContratadaAreaCod ;
      private IGxSession AV15WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV12Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV25ContagemResultado_Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Attachments ;
   }

   public class prc_reuniaodivergencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P008L5( IGxContext context ,
                                             int A1079ContratoGestor_UsuarioCod ,
                                             IGxCollection AV12Usuarios ,
                                             bool A1135ContratoGestor_UsuarioEhContratante ,
                                             int A1603ContagemResultado_CntCod ,
                                             int A1078ContratoGestor_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [1] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Usuarios, "T1.[ContratoGestor_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_ContratoCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P008L7( IGxContext context ,
                                             int AV10OsVinculada ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int AV8Codigo ,
                                             int A456ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [2] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_StatusDmn], T3.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo])";
         if ( (0==AV10OsVinculada) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_OSVinculada] = @AV8Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_OSVinculada] = @AV8Codigo)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ! (0==AV10OsVinculada) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV10OsVinculada)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Codigo] = @AV10OsVinculada)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P008L8( IGxContext context ,
                                             int A1079ContratoGestor_UsuarioCod ,
                                             IGxCollection AV12Usuarios ,
                                             bool A1135ContratoGestor_UsuarioEhContratante ,
                                             int A1603ContagemResultado_CntCod ,
                                             int A1078ContratoGestor_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [1] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV12Usuarios, "T1.[ContratoGestor_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_ContratoCod]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_P008L5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
               case 5 :
                     return conditional_P008L7(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 6 :
                     return conditional_P008L8(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008L2 ;
          prmP008L2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L3 ;
          prmP008L3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L4 ;
          prmP008L4 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L6 ;
          prmP008L6 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L9 ;
          prmP008L9 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L5 ;
          prmP008L5 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L7 ;
          prmP008L7 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10OsVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008L8 ;
          prmP008L8 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008L2", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T3.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_Demanda], T2.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_StatusDmn], T4.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Responsavel] FROM ((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV8Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008L2,1,0,true,true )
             ,new CursorDef("P008L3", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008L3,100,0,true,false )
             ,new CursorDef("P008L4", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @ContagemResultado_CntCod ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008L4,100,0,true,false )
             ,new CursorDef("P008L5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008L5,100,0,true,false )
             ,new CursorDef("P008L6", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008L6)
             ,new CursorDef("P008L7", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008L7,1,0,true,false )
             ,new CursorDef("P008L8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008L8,100,0,true,false )
             ,new CursorDef("P008L9", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008L9)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[1] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[2]);
                   }
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[1] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[2]);
                   }
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
