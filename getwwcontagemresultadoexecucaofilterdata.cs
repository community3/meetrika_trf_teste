/*
               File: GetWWContagemResultadoExecucaoFilterData
        Description: Get WWContagem Resultado Execucao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:35.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemresultadoexecucaofilterdata : GXProcedure
   {
      public getwwcontagemresultadoexecucaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemresultadoexecucaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemresultadoexecucaofilterdata objgetwwcontagemresultadoexecucaofilterdata;
         objgetwwcontagemresultadoexecucaofilterdata = new getwwcontagemresultadoexecucaofilterdata();
         objgetwwcontagemresultadoexecucaofilterdata.AV24DDOName = aP0_DDOName;
         objgetwwcontagemresultadoexecucaofilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwcontagemresultadoexecucaofilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemresultadoexecucaofilterdata.AV28OptionsJson = "" ;
         objgetwwcontagemresultadoexecucaofilterdata.AV31OptionsDescJson = "" ;
         objgetwwcontagemresultadoexecucaofilterdata.AV33OptionIndexesJson = "" ;
         objgetwwcontagemresultadoexecucaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemresultadoexecucaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemresultadoexecucaofilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemresultadoexecucaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTAGEMRESULTADO_OSFSOSFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_OSFSOSFMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWContagemResultadoExecucaoGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemResultadoExecucaoGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWContagemResultadoExecucaoGridState"), "");
         }
         AV75GXV1 = 1;
         while ( AV75GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV75GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "CONTRATADA_CODIGO") == 0 )
            {
               AV72Contratada_Codigo = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV10TFContagemResultado_OsFsOsFm = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_OSFSOSFM_SEL") == 0 )
            {
               AV11TFContagemResultado_OsFsOsFm_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
            {
               AV12TFContagemResultadoExecucao_Inicio = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContagemResultadoExecucao_Inicio_To = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
            {
               AV14TFContagemResultadoExecucao_Prevista = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContagemResultadoExecucao_Prevista_To = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS") == 0 )
            {
               AV16TFContagemResultadoExecucao_PrazoDias = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContagemResultadoExecucao_PrazoDias_To = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
            {
               AV18TFContagemResultadoExecucao_Fim = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContagemResultadoExecucao_Fim_To = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEXECUCAO_DIAS") == 0 )
            {
               AV20TFContagemResultadoExecucao_Dias = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV21TFContagemResultadoExecucao_Dias_To = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV75GXV1 = (int)(AV75GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
            {
               AV41ContagemResultadoExecucao_Inicio1 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
               AV42ContagemResultadoExecucao_Inicio_To1 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
            {
               AV43ContagemResultadoExecucao_Fim1 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
               AV44ContagemResultadoExecucao_Fim_To1 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
            {
               AV45ContagemResultadoExecucao_Prevista1 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
               AV46ContagemResultadoExecucao_Prevista_To1 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV47ContagemResultado_OsFsOsFm1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV66ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
            {
               AV67ContagemResultadoExecucao_OSCod1 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV48DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV49DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
               {
                  AV50ContagemResultadoExecucao_Inicio2 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                  AV51ContagemResultadoExecucao_Inicio_To2 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
               {
                  AV52ContagemResultadoExecucao_Fim2 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                  AV53ContagemResultadoExecucao_Fim_To2 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
               {
                  AV54ContagemResultadoExecucao_Prevista2 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                  AV55ContagemResultadoExecucao_Prevista_To2 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV56ContagemResultado_OsFsOsFm2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV68ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
               {
                  AV69ContagemResultadoExecucao_OSCod2 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV57DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV58DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
                  {
                     AV59ContagemResultadoExecucao_Inicio3 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                     AV60ContagemResultadoExecucao_Inicio_To3 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 )
                  {
                     AV61ContagemResultadoExecucao_Fim3 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                     AV62ContagemResultadoExecucao_Fim_To3 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 )
                  {
                     AV63ContagemResultadoExecucao_Prevista3 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                     AV64ContagemResultadoExecucao_Prevista_To3 = context.localUtil.CToT( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV65ContagemResultado_OsFsOsFm3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV70ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 )
                  {
                     AV71ContagemResultadoExecucao_OSCod3 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_OSFSOSFMOPTIONS' Routine */
         AV10TFContagemResultado_OsFsOsFm = AV22SearchTxt;
         AV11TFContagemResultado_OsFsOsFm_Sel = "";
         AV77WWContagemResultadoExecucaoDS_1_Contratada_codigo = AV72Contratada_Codigo;
         AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = AV41ContagemResultadoExecucao_Inicio1;
         AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = AV42ContagemResultadoExecucao_Inicio_To1;
         AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = AV43ContagemResultadoExecucao_Fim1;
         AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = AV44ContagemResultadoExecucao_Fim_To1;
         AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = AV45ContagemResultadoExecucao_Prevista1;
         AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = AV46ContagemResultadoExecucao_Prevista_To1;
         AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = AV47ContagemResultado_OsFsOsFm1;
         AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 = AV66ContagemResultado_ContratadaCod1;
         AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 = AV67ContagemResultadoExecucao_OSCod1;
         AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 = AV48DynamicFiltersEnabled2;
         AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = AV49DynamicFiltersSelector2;
         AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = AV50ContagemResultadoExecucao_Inicio2;
         AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = AV51ContagemResultadoExecucao_Inicio_To2;
         AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = AV52ContagemResultadoExecucao_Fim2;
         AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = AV53ContagemResultadoExecucao_Fim_To2;
         AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = AV54ContagemResultadoExecucao_Prevista2;
         AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = AV55ContagemResultadoExecucao_Prevista_To2;
         AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = AV56ContagemResultado_OsFsOsFm2;
         AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 = AV68ContagemResultado_ContratadaCod2;
         AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 = AV69ContagemResultadoExecucao_OSCod2;
         AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 = AV57DynamicFiltersEnabled3;
         AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = AV58DynamicFiltersSelector3;
         AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = AV59ContagemResultadoExecucao_Inicio3;
         AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = AV60ContagemResultadoExecucao_Inicio_To3;
         AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = AV61ContagemResultadoExecucao_Fim3;
         AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = AV62ContagemResultadoExecucao_Fim_To3;
         AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = AV63ContagemResultadoExecucao_Prevista3;
         AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = AV64ContagemResultadoExecucao_Prevista_To3;
         AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = AV65ContagemResultado_OsFsOsFm3;
         AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 = AV70ContagemResultado_ContratadaCod3;
         AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 = AV71ContagemResultadoExecucao_OSCod3;
         AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = AV10TFContagemResultado_OsFsOsFm;
         AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = AV11TFContagemResultado_OsFsOsFm_Sel;
         AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = AV12TFContagemResultadoExecucao_Inicio;
         AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = AV13TFContagemResultadoExecucao_Inicio_To;
         AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = AV14TFContagemResultadoExecucao_Prevista;
         AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = AV15TFContagemResultadoExecucao_Prevista_To;
         AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias = AV16TFContagemResultadoExecucao_PrazoDias;
         AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to = AV17TFContagemResultadoExecucao_PrazoDias_To;
         AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = AV18TFContagemResultadoExecucao_Fim;
         AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = AV19TFContagemResultadoExecucao_Fim_To;
         AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias = AV20TFContagemResultadoExecucao_Dias;
         AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to = AV21TFContagemResultadoExecucao_Dias_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ,
                                              AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ,
                                              AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ,
                                              AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ,
                                              AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ,
                                              AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ,
                                              AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ,
                                              AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ,
                                              AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ,
                                              AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ,
                                              AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ,
                                              AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ,
                                              AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ,
                                              AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ,
                                              AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ,
                                              AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ,
                                              AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ,
                                              AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ,
                                              AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ,
                                              AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ,
                                              AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ,
                                              AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ,
                                              AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ,
                                              AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ,
                                              AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ,
                                              AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ,
                                              AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ,
                                              AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ,
                                              AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ,
                                              AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ,
                                              AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ,
                                              AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ,
                                              AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ,
                                              AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ,
                                              AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ,
                                              AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ,
                                              AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ,
                                              AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ,
                                              AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ,
                                              AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ,
                                              AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ,
                                              AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ,
                                              AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ,
                                              AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1406ContagemResultadoExecucao_Inicio ,
                                              A1407ContagemResultadoExecucao_Fim ,
                                              A1408ContagemResultadoExecucao_Prevista ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A1404ContagemResultadoExecucao_OSCod ,
                                              A1411ContagemResultadoExecucao_PrazoDias ,
                                              A1410ContagemResultadoExecucao_Dias },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3), "%", "");
         lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3), "%", "");
         lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm), "%", "");
         /* Using cursor P00QS2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Contratada_codigo, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1, AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1, AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1, AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1, AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1, AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1, lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1, lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1, AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1, AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1, AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2, AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2, AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2, AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2, AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2, AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2, lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2, lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2, AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2, AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2, AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3, AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3, AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3, AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3, AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3, AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3, lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3, lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3, AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3, AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3, lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm, AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel, AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio, AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to, AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista, AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to, AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias, AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to, AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim, AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to, AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias, AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1410ContagemResultadoExecucao_Dias = P00QS2_A1410ContagemResultadoExecucao_Dias[0];
            n1410ContagemResultadoExecucao_Dias = P00QS2_n1410ContagemResultadoExecucao_Dias[0];
            A1411ContagemResultadoExecucao_PrazoDias = P00QS2_A1411ContagemResultadoExecucao_PrazoDias[0];
            n1411ContagemResultadoExecucao_PrazoDias = P00QS2_n1411ContagemResultadoExecucao_PrazoDias[0];
            A1404ContagemResultadoExecucao_OSCod = P00QS2_A1404ContagemResultadoExecucao_OSCod[0];
            A1408ContagemResultadoExecucao_Prevista = P00QS2_A1408ContagemResultadoExecucao_Prevista[0];
            n1408ContagemResultadoExecucao_Prevista = P00QS2_n1408ContagemResultadoExecucao_Prevista[0];
            A1407ContagemResultadoExecucao_Fim = P00QS2_A1407ContagemResultadoExecucao_Fim[0];
            n1407ContagemResultadoExecucao_Fim = P00QS2_n1407ContagemResultadoExecucao_Fim[0];
            A1406ContagemResultadoExecucao_Inicio = P00QS2_A1406ContagemResultadoExecucao_Inicio[0];
            A52Contratada_AreaTrabalhoCod = P00QS2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00QS2_n52Contratada_AreaTrabalhoCod[0];
            A490ContagemResultado_ContratadaCod = P00QS2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00QS2_n490ContagemResultado_ContratadaCod[0];
            A493ContagemResultado_DemandaFM = P00QS2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00QS2_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00QS2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00QS2_n457ContagemResultado_Demanda[0];
            A1405ContagemResultadoExecucao_Codigo = P00QS2_A1405ContagemResultadoExecucao_Codigo[0];
            A490ContagemResultado_ContratadaCod = P00QS2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00QS2_n490ContagemResultado_ContratadaCod[0];
            A493ContagemResultado_DemandaFM = P00QS2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00QS2_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00QS2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00QS2_n457ContagemResultado_Demanda[0];
            A52Contratada_AreaTrabalhoCod = P00QS2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00QS2_n52Contratada_AreaTrabalhoCod[0];
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A501ContagemResultado_OsFsOsFm)) )
            {
               AV26Option = A501ContagemResultado_OsFsOsFm;
               AV25InsertIndex = 1;
               while ( ( AV25InsertIndex <= AV27Options.Count ) && ( StringUtil.StrCmp(((String)AV27Options.Item(AV25InsertIndex)), AV26Option) < 0 ) )
               {
                  AV25InsertIndex = (int)(AV25InsertIndex+1);
               }
               if ( ( AV25InsertIndex <= AV27Options.Count ) && ( StringUtil.StrCmp(((String)AV27Options.Item(AV25InsertIndex)), AV26Option) == 0 ) )
               {
                  AV34count = (long)(NumberUtil.Val( ((String)AV32OptionIndexes.Item(AV25InsertIndex)), "."));
                  AV34count = (long)(AV34count+1);
                  AV32OptionIndexes.RemoveItem(AV25InsertIndex);
                  AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), AV25InsertIndex);
               }
               else
               {
                  AV27Options.Add(AV26Option, AV25InsertIndex);
                  AV32OptionIndexes.Add("1", AV25InsertIndex);
               }
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_OsFsOsFm = "";
         AV11TFContagemResultado_OsFsOsFm_Sel = "";
         AV12TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         AV13TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
         AV14TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         AV15TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
         AV18TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         AV19TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV41ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
         AV42ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
         AV43ContagemResultadoExecucao_Fim1 = (DateTime)(DateTime.MinValue);
         AV44ContagemResultadoExecucao_Fim_To1 = (DateTime)(DateTime.MinValue);
         AV45ContagemResultadoExecucao_Prevista1 = (DateTime)(DateTime.MinValue);
         AV46ContagemResultadoExecucao_Prevista_To1 = (DateTime)(DateTime.MinValue);
         AV47ContagemResultado_OsFsOsFm1 = "";
         AV49DynamicFiltersSelector2 = "";
         AV50ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
         AV51ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
         AV52ContagemResultadoExecucao_Fim2 = (DateTime)(DateTime.MinValue);
         AV53ContagemResultadoExecucao_Fim_To2 = (DateTime)(DateTime.MinValue);
         AV54ContagemResultadoExecucao_Prevista2 = (DateTime)(DateTime.MinValue);
         AV55ContagemResultadoExecucao_Prevista_To2 = (DateTime)(DateTime.MinValue);
         AV56ContagemResultado_OsFsOsFm2 = "";
         AV58DynamicFiltersSelector3 = "";
         AV59ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
         AV60ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
         AV61ContagemResultadoExecucao_Fim3 = (DateTime)(DateTime.MinValue);
         AV62ContagemResultadoExecucao_Fim_To3 = (DateTime)(DateTime.MinValue);
         AV63ContagemResultadoExecucao_Prevista3 = (DateTime)(DateTime.MinValue);
         AV64ContagemResultadoExecucao_Prevista_To3 = (DateTime)(DateTime.MinValue);
         AV65ContagemResultado_OsFsOsFm3 = "";
         AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 = "";
         AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 = (DateTime)(DateTime.MinValue);
         AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 = (DateTime)(DateTime.MinValue);
         AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 = (DateTime)(DateTime.MinValue);
         AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 = (DateTime)(DateTime.MinValue);
         AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 = (DateTime)(DateTime.MinValue);
         AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 = (DateTime)(DateTime.MinValue);
         AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = "";
         AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 = "";
         AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 = (DateTime)(DateTime.MinValue);
         AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 = (DateTime)(DateTime.MinValue);
         AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 = (DateTime)(DateTime.MinValue);
         AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 = (DateTime)(DateTime.MinValue);
         AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 = (DateTime)(DateTime.MinValue);
         AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 = (DateTime)(DateTime.MinValue);
         AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = "";
         AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 = "";
         AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 = (DateTime)(DateTime.MinValue);
         AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 = (DateTime)(DateTime.MinValue);
         AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 = (DateTime)(DateTime.MinValue);
         AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 = (DateTime)(DateTime.MinValue);
         AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 = (DateTime)(DateTime.MinValue);
         AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 = (DateTime)(DateTime.MinValue);
         AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = "";
         AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = "";
         AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel = "";
         AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio = (DateTime)(DateTime.MinValue);
         AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to = (DateTime)(DateTime.MinValue);
         AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista = (DateTime)(DateTime.MinValue);
         AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to = (DateTime)(DateTime.MinValue);
         AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim = (DateTime)(DateTime.MinValue);
         AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 = "";
         lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 = "";
         lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 = "";
         lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         P00QS2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00QS2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P00QS2_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         P00QS2_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         P00QS2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00QS2_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         P00QS2_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         P00QS2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P00QS2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P00QS2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00QS2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00QS2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00QS2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00QS2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00QS2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00QS2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00QS2_A457ContagemResultado_Demanda = new String[] {""} ;
         P00QS2_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00QS2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A501ContagemResultado_OsFsOsFm = "";
         AV26Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemresultadoexecucaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QS2_A1410ContagemResultadoExecucao_Dias, P00QS2_n1410ContagemResultadoExecucao_Dias, P00QS2_A1411ContagemResultadoExecucao_PrazoDias, P00QS2_n1411ContagemResultadoExecucao_PrazoDias, P00QS2_A1404ContagemResultadoExecucao_OSCod, P00QS2_A1408ContagemResultadoExecucao_Prevista, P00QS2_n1408ContagemResultadoExecucao_Prevista, P00QS2_A1407ContagemResultadoExecucao_Fim, P00QS2_n1407ContagemResultadoExecucao_Fim, P00QS2_A1406ContagemResultadoExecucao_Inicio,
               P00QS2_A52Contratada_AreaTrabalhoCod, P00QS2_n52Contratada_AreaTrabalhoCod, P00QS2_A490ContagemResultado_ContratadaCod, P00QS2_n490ContagemResultado_ContratadaCod, P00QS2_A493ContagemResultado_DemandaFM, P00QS2_n493ContagemResultado_DemandaFM, P00QS2_A457ContagemResultado_Demanda, P00QS2_n457ContagemResultado_Demanda, P00QS2_A1405ContagemResultadoExecucao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFContagemResultadoExecucao_PrazoDias ;
      private short AV17TFContagemResultadoExecucao_PrazoDias_To ;
      private short AV20TFContagemResultadoExecucao_Dias ;
      private short AV21TFContagemResultadoExecucao_Dias_To ;
      private short AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ;
      private short AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ;
      private short AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ;
      private short AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private int AV75GXV1 ;
      private int AV72Contratada_Codigo ;
      private int AV66ContagemResultado_ContratadaCod1 ;
      private int AV67ContagemResultadoExecucao_OSCod1 ;
      private int AV68ContagemResultado_ContratadaCod2 ;
      private int AV69ContagemResultadoExecucao_OSCod2 ;
      private int AV70ContagemResultado_ContratadaCod3 ;
      private int AV71ContagemResultadoExecucao_OSCod3 ;
      private int AV77WWContagemResultadoExecucaoDS_1_Contratada_codigo ;
      private int AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ;
      private int AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ;
      private int AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ;
      private int AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ;
      private int AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ;
      private int AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int AV25InsertIndex ;
      private long AV34count ;
      private String scmdbuf ;
      private DateTime AV12TFContagemResultadoExecucao_Inicio ;
      private DateTime AV13TFContagemResultadoExecucao_Inicio_To ;
      private DateTime AV14TFContagemResultadoExecucao_Prevista ;
      private DateTime AV15TFContagemResultadoExecucao_Prevista_To ;
      private DateTime AV18TFContagemResultadoExecucao_Fim ;
      private DateTime AV19TFContagemResultadoExecucao_Fim_To ;
      private DateTime AV41ContagemResultadoExecucao_Inicio1 ;
      private DateTime AV42ContagemResultadoExecucao_Inicio_To1 ;
      private DateTime AV43ContagemResultadoExecucao_Fim1 ;
      private DateTime AV44ContagemResultadoExecucao_Fim_To1 ;
      private DateTime AV45ContagemResultadoExecucao_Prevista1 ;
      private DateTime AV46ContagemResultadoExecucao_Prevista_To1 ;
      private DateTime AV50ContagemResultadoExecucao_Inicio2 ;
      private DateTime AV51ContagemResultadoExecucao_Inicio_To2 ;
      private DateTime AV52ContagemResultadoExecucao_Fim2 ;
      private DateTime AV53ContagemResultadoExecucao_Fim_To2 ;
      private DateTime AV54ContagemResultadoExecucao_Prevista2 ;
      private DateTime AV55ContagemResultadoExecucao_Prevista_To2 ;
      private DateTime AV59ContagemResultadoExecucao_Inicio3 ;
      private DateTime AV60ContagemResultadoExecucao_Inicio_To3 ;
      private DateTime AV61ContagemResultadoExecucao_Fim3 ;
      private DateTime AV62ContagemResultadoExecucao_Fim_To3 ;
      private DateTime AV63ContagemResultadoExecucao_Prevista3 ;
      private DateTime AV64ContagemResultadoExecucao_Prevista_To3 ;
      private DateTime AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ;
      private DateTime AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ;
      private DateTime AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ;
      private DateTime AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ;
      private DateTime AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ;
      private DateTime AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ;
      private DateTime AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ;
      private DateTime AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ;
      private DateTime AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ;
      private DateTime AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ;
      private DateTime AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ;
      private DateTime AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ;
      private DateTime AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ;
      private DateTime AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ;
      private DateTime AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ;
      private DateTime AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ;
      private DateTime AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ;
      private DateTime AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ;
      private DateTime AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ;
      private DateTime AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ;
      private DateTime AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ;
      private DateTime AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ;
      private DateTime AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ;
      private DateTime AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private bool returnInSub ;
      private bool AV48DynamicFiltersEnabled2 ;
      private bool AV57DynamicFiltersEnabled3 ;
      private bool AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ;
      private bool AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV10TFContagemResultado_OsFsOsFm ;
      private String AV11TFContagemResultado_OsFsOsFm_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV47ContagemResultado_OsFsOsFm1 ;
      private String AV49DynamicFiltersSelector2 ;
      private String AV56ContagemResultado_OsFsOsFm2 ;
      private String AV58DynamicFiltersSelector3 ;
      private String AV65ContagemResultado_OsFsOsFm3 ;
      private String AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ;
      private String AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ;
      private String AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ;
      private String AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ;
      private String AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ;
      private String AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ;
      private String AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ;
      private String AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ;
      private String lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ;
      private String lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ;
      private String lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ;
      private String lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P00QS2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00QS2_n1410ContagemResultadoExecucao_Dias ;
      private short[] P00QS2_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] P00QS2_n1411ContagemResultadoExecucao_PrazoDias ;
      private int[] P00QS2_A1404ContagemResultadoExecucao_OSCod ;
      private DateTime[] P00QS2_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] P00QS2_n1408ContagemResultadoExecucao_Prevista ;
      private DateTime[] P00QS2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P00QS2_n1407ContagemResultadoExecucao_Fim ;
      private DateTime[] P00QS2_A1406ContagemResultadoExecucao_Inicio ;
      private int[] P00QS2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00QS2_n52Contratada_AreaTrabalhoCod ;
      private int[] P00QS2_A490ContagemResultado_ContratadaCod ;
      private bool[] P00QS2_n490ContagemResultado_ContratadaCod ;
      private String[] P00QS2_A493ContagemResultado_DemandaFM ;
      private bool[] P00QS2_n493ContagemResultado_DemandaFM ;
      private String[] P00QS2_A457ContagemResultado_Demanda ;
      private bool[] P00QS2_n457ContagemResultado_Demanda ;
      private int[] P00QS2_A1405ContagemResultadoExecucao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwcontagemresultadoexecucaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QS2( IGxContext context ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1 ,
                                             DateTime AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1 ,
                                             DateTime AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1 ,
                                             DateTime AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1 ,
                                             DateTime AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1 ,
                                             DateTime AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1 ,
                                             String AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 ,
                                             int AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 ,
                                             int AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 ,
                                             bool AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 ,
                                             String AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2 ,
                                             DateTime AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2 ,
                                             DateTime AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2 ,
                                             DateTime AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2 ,
                                             DateTime AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2 ,
                                             DateTime AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2 ,
                                             DateTime AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2 ,
                                             String AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 ,
                                             int AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 ,
                                             int AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 ,
                                             bool AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 ,
                                             String AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3 ,
                                             DateTime AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3 ,
                                             DateTime AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3 ,
                                             DateTime AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3 ,
                                             DateTime AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3 ,
                                             DateTime AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3 ,
                                             DateTime AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3 ,
                                             String AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 ,
                                             int AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 ,
                                             int AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 ,
                                             String AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio ,
                                             DateTime AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to ,
                                             DateTime AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista ,
                                             DateTime AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to ,
                                             short AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias ,
                                             short AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to ,
                                             DateTime AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim ,
                                             DateTime AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to ,
                                             short AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias ,
                                             short AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime A1406ContagemResultadoExecucao_Inicio ,
                                             DateTime A1407ContagemResultadoExecucao_Fim ,
                                             DateTime A1408ContagemResultadoExecucao_Prevista ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             int A1404ContagemResultadoExecucao_OSCod ,
                                             short A1411ContagemResultadoExecucao_PrazoDias ,
                                             short A1410ContagemResultadoExecucao_Dias )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [44] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoExecucao_Dias], T1.[ContagemResultadoExecucao_PrazoDias], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T1.[ContagemResultadoExecucao_Prevista], T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_Inicio], T3.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda], T1.[ContagemResultadoExecucao_Codigo] FROM (([ContagemResultadoExecucao] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV9WWPCo_1Contratada_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV9WWPCo_1Contratada_codigo)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( (0==AV9WWPContext_gxTpr_Contratada_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_2Areatrabalho_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_2Areatrabalho_codigo)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%' or T2.[ContagemResultado_DemandaFM] like @lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%' or T2.[ContagemResultado_DemandaFM] like @lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1 + '%')";
            }
         }
         else
         {
            GXv_int1[8] = 1;
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV78WWContagemResultadoExecucaoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%' or T2.[ContagemResultado_DemandaFM] like @lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%' or T2.[ContagemResultado_DemandaFM] like @lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2 + '%')";
            }
         }
         else
         {
            GXv_int1[18] = 1;
            GXv_int1[19] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV88WWContagemResultadoExecucaoDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContagemResultadoExecucaoDS_13_Dynamicfiltersselector2, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_FIM") == 0 ) && ( ! (DateTime.MinValue==AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_PREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%' or T2.[ContagemResultado_DemandaFM] like @lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%' or T2.[ContagemResultado_DemandaFM] like @lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3 + '%')";
            }
         }
         else
         {
            GXv_int1[28] = 1;
            GXv_int1[29] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_ContratadaCod] = @AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV99WWContagemResultadoExecucaoDS_23_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV100WWContagemResultadoExecucaoDS_24_Dynamicfiltersselector3, "CONTAGEMRESULTADOEXECUCAO_OSCOD") == 0 ) && ( ( AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3 > 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_OSCod] = @AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_OSCod] = @AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)";
            }
            else
            {
               sWhereString = sWhereString + " (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] >= @AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] >= @AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Inicio] <= @AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Inicio] <= @AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] >= @AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] >= @AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista)";
            }
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( ! (DateTime.MinValue==AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Prevista] <= @AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Prevista] <= @AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to)";
            }
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( ! (0==AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_PrazoDias] >= @AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_PrazoDias] >= @AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias)";
            }
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( ! (0==AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_PrazoDias] <= @AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_PrazoDias] <= @AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to)";
            }
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( ! (DateTime.MinValue==AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] >= @AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] >= @AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim)";
            }
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( ! (DateTime.MinValue==AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Fim] <= @AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Fim] <= @AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to)";
            }
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( ! (0==AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Dias] >= @AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Dias] >= @AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias)";
            }
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( ! (0==AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultadoExecucao_Dias] <= @AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultadoExecucao_Dias] <= @AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to)";
            }
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultadoExecucao_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QS2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (DateTime)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (short)dynConstraints[39] , (short)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (int)dynConstraints[53] , (short)dynConstraints[54] , (short)dynConstraints[55] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QS2 ;
          prmP00QS2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWContagemResultadoExecucaoDS_3_Contagemresultadoexecucao_inicio1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV80WWContagemResultadoExecucaoDS_4_Contagemresultadoexecucao_inicio_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81WWContagemResultadoExecucaoDS_5_Contagemresultadoexecucao_fim1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82WWContagemResultadoExecucaoDS_6_Contagemresultadoexecucao_fim_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV83WWContagemResultadoExecucaoDS_7_Contagemresultadoexecucao_prevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV84WWContagemResultadoExecucaoDS_8_Contagemresultadoexecucao_prevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV85WWContagemResultadoExecucaoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV86WWContagemResultadoExecucaoDS_10_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WWContagemResultadoExecucaoDS_11_Contagemresultadoexecucao_oscod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV90WWContagemResultadoExecucaoDS_14_Contagemresultadoexecucao_inicio2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV91WWContagemResultadoExecucaoDS_15_Contagemresultadoexecucao_inicio_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV92WWContagemResultadoExecucaoDS_16_Contagemresultadoexecucao_fim2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV93WWContagemResultadoExecucaoDS_17_Contagemresultadoexecucao_fim_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV94WWContagemResultadoExecucaoDS_18_Contagemresultadoexecucao_prevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95WWContagemResultadoExecucaoDS_19_Contagemresultadoexecucao_prevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV96WWContagemResultadoExecucaoDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV97WWContagemResultadoExecucaoDS_21_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV98WWContagemResultadoExecucaoDS_22_Contagemresultadoexecucao_oscod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWContagemResultadoExecucaoDS_25_Contagemresultadoexecucao_inicio3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV102WWContagemResultadoExecucaoDS_26_Contagemresultadoexecucao_inicio_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV103WWContagemResultadoExecucaoDS_27_Contagemresultadoexecucao_fim3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV104WWContagemResultadoExecucaoDS_28_Contagemresultadoexecucao_fim_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV105WWContagemResultadoExecucaoDS_29_Contagemresultadoexecucao_prevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV106WWContagemResultadoExecucaoDS_30_Contagemresultadoexecucao_prevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV107WWContagemResultadoExecucaoDS_31_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV108WWContagemResultadoExecucaoDS_32_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV109WWContagemResultadoExecucaoDS_33_Contagemresultadoexecucao_oscod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV110WWContagemResultadoExecucaoDS_34_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV111WWContagemResultadoExecucaoDS_35_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV112WWContagemResultadoExecucaoDS_36_Tfcontagemresultadoexecucao_inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV113WWContagemResultadoExecucaoDS_37_Tfcontagemresultadoexecucao_inicio_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV114WWContagemResultadoExecucaoDS_38_Tfcontagemresultadoexecucao_prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV115WWContagemResultadoExecucaoDS_39_Tfcontagemresultadoexecucao_prevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV116WWContagemResultadoExecucaoDS_40_Tfcontagemresultadoexecucao_prazodias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV117WWContagemResultadoExecucaoDS_41_Tfcontagemresultadoexecucao_prazodias_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV118WWContagemResultadoExecucaoDS_42_Tfcontagemresultadoexecucao_fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV119WWContagemResultadoExecucaoDS_43_Tfcontagemresultadoexecucao_fim_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV120WWContagemResultadoExecucaoDS_44_Tfcontagemresultadoexecucao_dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV121WWContagemResultadoExecucaoDS_45_Tfcontagemresultadoexecucao_dias_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QS2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[74]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[80]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[81]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[82]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[83]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[86]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[87]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemresultadoexecucaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemresultadoexecucaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemresultadoexecucaofilterdata") )
          {
             return  ;
          }
          getwwcontagemresultadoexecucaofilterdata worker = new getwwcontagemresultadoexecucaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
