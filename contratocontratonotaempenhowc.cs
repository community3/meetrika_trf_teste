/*
               File: ContratoContratoNotaEmpenhoWC
        Description: Contrato Contrato Nota Empenho WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:55:25.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratocontratonotaempenhowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratocontratonotaempenhowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratocontratonotaempenhowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV7Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynSaldoContrato_Codigo = new GXCombobox();
         chkNotaEmpenho_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_19 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_19_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_19_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV18TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0)));
                  AV19TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0)));
                  AV22TFSaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0)));
                  AV23TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0)));
                  AV26TFSaldoContrato_UnidadeMedicao_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFSaldoContrato_UnidadeMedicao_Sigla", AV26TFSaldoContrato_UnidadeMedicao_Sigla);
                  AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel", AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel);
                  AV30TFNotaEmpenho_Itentificador = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFNotaEmpenho_Itentificador", AV30TFNotaEmpenho_Itentificador);
                  AV31TFNotaEmpenho_Itentificador_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFNotaEmpenho_Itentificador_Sel", AV31TFNotaEmpenho_Itentificador_Sel);
                  AV34TFNotaEmpenho_DEmissao = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
                  AV35TFNotaEmpenho_DEmissao_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
                  AV40TFNotaEmpenho_Qtd = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5)));
                  AV41TFNotaEmpenho_Qtd_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5)));
                  AV44TFNotaEmpenho_Valor = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5)));
                  AV45TFNotaEmpenho_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5)));
                  AV48TFNotaEmpenho_SaldoAnt = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5)));
                  AV49TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5)));
                  AV52TFNotaEmpenho_SaldoPos = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5)));
                  AV53TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5)));
                  AV56TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0));
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace", AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace);
                  AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace);
                  AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace", AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace);
                  AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace", AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace);
                  AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace", AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace);
                  AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace", AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace);
                  AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace", AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace);
                  AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace", AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace);
                  AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace", AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace);
                  AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace", AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV65Pgmname = GetNextPar( );
                  A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, AV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, AV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, AV56TFNotaEmpenho_Ativo_Sel, AV7Contrato_Codigo, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV6WWPContext, AV65Pgmname, A1560NotaEmpenho_Codigo, A74Contrato_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAMH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV65Pgmname = "ContratoContratoNotaEmpenhoWC";
               context.Gx_err = 0;
               WSMH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Contrato Nota Empenho WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812552617");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratocontratonotaempenhowc.aspx") + "?" + UrlEncode("" +AV7Contrato_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA", StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL", StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_ITENTIFICADOR", StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_ITENTIFICADOR_SEL", StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_DEMISSAO", context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_DEMISSAO_TO", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_QTD", StringUtil.LTrim( StringUtil.NToC( AV40TFNotaEmpenho_Qtd, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_QTD_TO", StringUtil.LTrim( StringUtil.NToC( AV41TFNotaEmpenho_Qtd_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_VALOR", StringUtil.LTrim( StringUtil.NToC( AV44TFNotaEmpenho_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV45TFNotaEmpenho_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_SALDOANT", StringUtil.LTrim( StringUtil.NToC( AV48TFNotaEmpenho_SaldoAnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_SALDOANT_TO", StringUtil.LTrim( StringUtil.NToC( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_SALDOPOS", StringUtil.LTrim( StringUtil.NToC( AV52TFNotaEmpenho_SaldoPos, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_SALDOPOS_TO", StringUtil.LTrim( StringUtil.NToC( AV53TFNotaEmpenho_SaldoPos_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFNOTAEMPENHO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV58DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV58DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_CODIGOTITLEFILTERDATA", AV17NotaEmpenho_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_CODIGOTITLEFILTERDATA", AV17NotaEmpenho_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_CODIGOTITLEFILTERDATA", AV21SaldoContrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_CODIGOTITLEFILTERDATA", AV21SaldoContrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLEFILTERDATA", AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLEFILTERDATA", AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA", AV29NotaEmpenho_ItentificadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA", AV29NotaEmpenho_ItentificadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA", AV33NotaEmpenho_DEmissaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA", AV33NotaEmpenho_DEmissaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_QTDTITLEFILTERDATA", AV39NotaEmpenho_QtdTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_QTDTITLEFILTERDATA", AV39NotaEmpenho_QtdTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_VALORTITLEFILTERDATA", AV43NotaEmpenho_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_VALORTITLEFILTERDATA", AV43NotaEmpenho_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_SALDOANTTITLEFILTERDATA", AV47NotaEmpenho_SaldoAntTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_SALDOANTTITLEFILTERDATA", AV47NotaEmpenho_SaldoAntTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA", AV51NotaEmpenho_SaldoPosTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA", AV51NotaEmpenho_SaldoPosTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vNOTAEMPENHO_ATIVOTITLEFILTERDATA", AV55NotaEmpenho_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vNOTAEMPENHO_ATIVOTITLEFILTERDATA", AV55NotaEmpenho_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV65Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Caption", StringUtil.RTrim( Ddo_notaempenho_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_notaempenho_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Cls", StringUtil.RTrim( Ddo_notaempenho_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_notaempenho_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_notaempenho_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_saldocontrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_saldocontrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Caption", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Cls", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_unidademedicao_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_saldocontrato_unidademedicao_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Caption", StringUtil.RTrim( Ddo_notaempenho_itentificador_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Tooltip", StringUtil.RTrim( Ddo_notaempenho_itentificador_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Cls", StringUtil.RTrim( Ddo_notaempenho_itentificador_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_itentificador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_notaempenho_itentificador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_itentificador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_itentificador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_itentificador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filtertype", StringUtil.RTrim( Ddo_notaempenho_itentificador_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Datalisttype", StringUtil.RTrim( Ddo_notaempenho_itentificador_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistproc", StringUtil.RTrim( Ddo_notaempenho_itentificador_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Sortasc", StringUtil.RTrim( Ddo_notaempenho_itentificador_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_itentificador_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Loadingdata", StringUtil.RTrim( Ddo_notaempenho_itentificador_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_itentificador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Noresultsfound", StringUtil.RTrim( Ddo_notaempenho_itentificador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_itentificador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Caption", StringUtil.RTrim( Ddo_notaempenho_demissao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Tooltip", StringUtil.RTrim( Ddo_notaempenho_demissao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Cls", StringUtil.RTrim( Ddo_notaempenho_demissao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_demissao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_demissao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_demissao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filtertype", StringUtil.RTrim( Ddo_notaempenho_demissao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Sortasc", StringUtil.RTrim( Ddo_notaempenho_demissao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_demissao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_demissao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_demissao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_demissao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_demissao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Caption", StringUtil.RTrim( Ddo_notaempenho_qtd_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Tooltip", StringUtil.RTrim( Ddo_notaempenho_qtd_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Cls", StringUtil.RTrim( Ddo_notaempenho_qtd_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_qtd_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_qtd_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_qtd_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Filtertype", StringUtil.RTrim( Ddo_notaempenho_qtd_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Sortasc", StringUtil.RTrim( Ddo_notaempenho_qtd_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_qtd_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_qtd_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_qtd_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_qtd_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_qtd_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Caption", StringUtil.RTrim( Ddo_notaempenho_valor_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Tooltip", StringUtil.RTrim( Ddo_notaempenho_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Cls", StringUtil.RTrim( Ddo_notaempenho_valor_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Filtertype", StringUtil.RTrim( Ddo_notaempenho_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Sortasc", StringUtil.RTrim( Ddo_notaempenho_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Caption", StringUtil.RTrim( Ddo_notaempenho_saldoant_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Tooltip", StringUtil.RTrim( Ddo_notaempenho_saldoant_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Cls", StringUtil.RTrim( Ddo_notaempenho_saldoant_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_saldoant_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_saldoant_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_saldoant_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filtertype", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Sortasc", StringUtil.RTrim( Ddo_notaempenho_saldoant_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_saldoant_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_saldoant_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_saldoant_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_saldoant_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_saldoant_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Caption", StringUtil.RTrim( Ddo_notaempenho_saldopos_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Tooltip", StringUtil.RTrim( Ddo_notaempenho_saldopos_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Cls", StringUtil.RTrim( Ddo_notaempenho_saldopos_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_saldopos_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_saldopos_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_saldopos_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filtertype", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Sortasc", StringUtil.RTrim( Ddo_notaempenho_saldopos_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_saldopos_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_saldopos_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_saldopos_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_saldopos_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_saldopos_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Caption", StringUtil.RTrim( Ddo_notaempenho_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_notaempenho_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Cls", StringUtil.RTrim( Ddo_notaempenho_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_notaempenho_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_notaempenho_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_notaempenho_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_notaempenho_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_itentificador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_itentificador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_notaempenho_itentificador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_demissao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_qtd_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_saldoant_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_saldopos_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_NOTAEMPENHO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_notaempenho_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMH2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratocontratonotaempenhowc.js", "?202051812553067");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoContratoNotaEmpenhoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Contrato Nota Empenho WC" ;
      }

      protected void WBMH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratocontratonotaempenhowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_MH2( true) ;
         }
         else
         {
            wb_table1_2_MH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18TFNotaEmpenho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19TFNotaEmpenho_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22TFSaldoContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23TFSaldoContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_unidademedicao_sigla_Internalname, StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla), StringUtil.RTrim( context.localUtil.Format( AV26TFSaldoContrato_UnidadeMedicao_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_unidademedicao_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_unidademedicao_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_unidademedicao_sigla_sel_Internalname, StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_unidademedicao_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_unidademedicao_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_itentificador_Internalname, StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador), StringUtil.RTrim( context.localUtil.Format( AV30TFNotaEmpenho_Itentificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_itentificador_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_itentificador_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_itentificador_sel_Internalname, StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel), StringUtil.RTrim( context.localUtil.Format( AV31TFNotaEmpenho_Itentificador_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_itentificador_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_itentificador_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfnotaempenho_demissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_demissao_Internalname, context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV34TFNotaEmpenho_DEmissao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,48);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_demissao_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_demissao_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfnotaempenho_demissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfnotaempenho_demissao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfnotaempenho_demissao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_demissao_to_Internalname, context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV35TFNotaEmpenho_DEmissao_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_demissao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_demissao_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfnotaempenho_demissao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfnotaempenho_demissao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_notaempenho_demissaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_notaempenho_demissaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_notaempenho_demissaoauxdate_Internalname, context.localUtil.Format(AV36DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"), context.localUtil.Format( AV36DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_notaempenho_demissaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_notaempenho_demissaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_notaempenho_demissaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_notaempenho_demissaoauxdateto_Internalname, context.localUtil.Format(AV37DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV37DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,52);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_notaempenho_demissaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_notaempenho_demissaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_qtd_Internalname, StringUtil.LTrim( StringUtil.NToC( AV40TFNotaEmpenho_Qtd, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV40TFNotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,53);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_qtd_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_qtd_Visible, 1, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_qtd_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV41TFNotaEmpenho_Qtd_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV41TFNotaEmpenho_Qtd_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,54);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_qtd_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_qtd_to_Visible, 1, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV44TFNotaEmpenho_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV44TFNotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,55);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV45TFNotaEmpenho_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV45TFNotaEmpenho_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldoant_Internalname, StringUtil.LTrim( StringUtil.NToC( AV48TFNotaEmpenho_SaldoAnt, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV48TFNotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldoant_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldoant_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldoant_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV49TFNotaEmpenho_SaldoAnt_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldoant_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldoant_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldopos_Internalname, StringUtil.LTrim( StringUtil.NToC( AV52TFNotaEmpenho_SaldoPos, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV52TFNotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldopos_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldopos_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldopos_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV53TFNotaEmpenho_SaldoPos_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV53TFNotaEmpenho_SaldoPos_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,60);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldopos_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldopos_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", 0, edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", 0, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Internalname, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_DEMISSAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_QTDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_SALDOANTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", 0, edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_SALDOPOSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", 0, edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_NOTAEMPENHO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoContratoNotaEmpenhoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTMH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Contrato Nota Empenho WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPMH0( ) ;
            }
         }
      }

      protected void WSMH2( )
      {
         STARTMH2( ) ;
         EVTMH2( ) ;
      }

      protected void EVTMH2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11MH2 */
                                    E11MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12MH2 */
                                    E12MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13MH2 */
                                    E13MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14MH2 */
                                    E14MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15MH2 */
                                    E15MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16MH2 */
                                    E16MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17MH2 */
                                    E17MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18MH2 */
                                    E18MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19MH2 */
                                    E19MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20MH2 */
                                    E20MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21MH2 */
                                    E21MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22MH2 */
                                    E22MH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMH0( ) ;
                              }
                              nGXsfl_19_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
                              SubsflControlProps_192( ) ;
                              AV16Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)) ? AV64Update_GXI : context.convertURL( context.PathToRelativeUrl( AV16Update))));
                              A1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNotaEmpenho_Codigo_Internalname), ",", "."));
                              dynSaldoContrato_Codigo.Name = dynSaldoContrato_Codigo_Internalname;
                              dynSaldoContrato_Codigo.CurrentValue = cgiGet( dynSaldoContrato_Codigo_Internalname);
                              A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( cgiGet( dynSaldoContrato_Codigo_Internalname), "."));
                              A1785SaldoContrato_UnidadeMedicao_Sigla = StringUtil.Upper( cgiGet( edtSaldoContrato_UnidadeMedicao_Sigla_Internalname));
                              n1785SaldoContrato_UnidadeMedicao_Sigla = false;
                              A1564NotaEmpenho_Itentificador = cgiGet( edtNotaEmpenho_Itentificador_Internalname);
                              n1564NotaEmpenho_Itentificador = false;
                              A1565NotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtNotaEmpenho_DEmissao_Internalname), 0);
                              n1565NotaEmpenho_DEmissao = false;
                              A1567NotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Qtd_Internalname), ",", ".");
                              n1567NotaEmpenho_Qtd = false;
                              A1566NotaEmpenho_Valor = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Valor_Internalname), ",", ".");
                              n1566NotaEmpenho_Valor = false;
                              A1568NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( edtNotaEmpenho_SaldoAnt_Internalname), ",", ".");
                              n1568NotaEmpenho_SaldoAnt = false;
                              A1569NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( edtNotaEmpenho_SaldoPos_Internalname), ",", ".");
                              n1569NotaEmpenho_SaldoPos = false;
                              A1570NotaEmpenho_Ativo = StringUtil.StrToBool( cgiGet( chkNotaEmpenho_Ativo_Internalname));
                              n1570NotaEmpenho_Ativo = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23MH2 */
                                          E23MH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24MH2 */
                                          E24MH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25MH2 */
                                          E25MH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_CODIGO"), ",", ".") != Convert.ToDecimal( AV18TFNotaEmpenho_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV19TFNotaEmpenho_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV22TFSaldoContrato_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV23TFSaldoContrato_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_unidademedicao_sigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA"), AV26TFSaldoContrato_UnidadeMedicao_Sigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsaldocontrato_unidademedicao_sigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL"), AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_itentificador Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_ITENTIFICADOR"), AV30TFNotaEmpenho_Itentificador) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_itentificador_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_ITENTIFICADOR_SEL"), AV31TFNotaEmpenho_Itentificador_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_demissao Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_DEMISSAO"), 0) != AV34TFNotaEmpenho_DEmissao )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_demissao_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_DEMISSAO_TO"), 0) != AV35TFNotaEmpenho_DEmissao_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_qtd Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_QTD"), ",", ".") != AV40TFNotaEmpenho_Qtd )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_qtd_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_QTD_TO"), ",", ".") != AV41TFNotaEmpenho_Qtd_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_valor Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_VALOR"), ",", ".") != AV44TFNotaEmpenho_Valor )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_valor_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_VALOR_TO"), ",", ".") != AV45TFNotaEmpenho_Valor_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_saldoant Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOANT"), ",", ".") != AV48TFNotaEmpenho_SaldoAnt )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_saldoant_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOANT_TO"), ",", ".") != AV49TFNotaEmpenho_SaldoAnt_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_saldopos Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOPOS"), ",", ".") != AV52TFNotaEmpenho_SaldoPos )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_saldopos_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOPOS_TO"), ",", ".") != AV53TFNotaEmpenho_SaldoPos_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfnotaempenho_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV56TFNotaEmpenho_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPMH0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMH2( ) ;
            }
         }
      }

      protected void PAMH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "SALDOCONTRATO_CODIGO_" + sGXsfl_19_idx;
            dynSaldoContrato_Codigo.Name = GXCCtl;
            dynSaldoContrato_Codigo.WebTags = "";
            dynSaldoContrato_Codigo.removeAllItems();
            /* Using cursor H00MH2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynSaldoContrato_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00MH2_A1561SaldoContrato_Codigo[0]), 6, 0)), StringUtil.Str( (decimal)(H00MH2_A1561SaldoContrato_Codigo[0]), 6, 0), 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynSaldoContrato_Codigo.ItemCount > 0 )
            {
               A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( dynSaldoContrato_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0))), "."));
            }
            GXCCtl = "NOTAEMPENHO_ATIVO_" + sGXsfl_19_idx;
            chkNotaEmpenho_Ativo.Name = GXCCtl;
            chkNotaEmpenho_Ativo.WebTags = "";
            chkNotaEmpenho_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkNotaEmpenho_Ativo_Internalname, "TitleCaption", chkNotaEmpenho_Ativo.Caption);
            chkNotaEmpenho_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV18TFNotaEmpenho_Codigo ,
                                       int AV19TFNotaEmpenho_Codigo_To ,
                                       int AV22TFSaldoContrato_Codigo ,
                                       int AV23TFSaldoContrato_Codigo_To ,
                                       String AV26TFSaldoContrato_UnidadeMedicao_Sigla ,
                                       String AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                       String AV30TFNotaEmpenho_Itentificador ,
                                       String AV31TFNotaEmpenho_Itentificador_Sel ,
                                       DateTime AV34TFNotaEmpenho_DEmissao ,
                                       DateTime AV35TFNotaEmpenho_DEmissao_To ,
                                       decimal AV40TFNotaEmpenho_Qtd ,
                                       decimal AV41TFNotaEmpenho_Qtd_To ,
                                       decimal AV44TFNotaEmpenho_Valor ,
                                       decimal AV45TFNotaEmpenho_Valor_To ,
                                       decimal AV48TFNotaEmpenho_SaldoAnt ,
                                       decimal AV49TFNotaEmpenho_SaldoAnt_To ,
                                       decimal AV52TFNotaEmpenho_SaldoPos ,
                                       decimal AV53TFNotaEmpenho_SaldoPos_To ,
                                       short AV56TFNotaEmpenho_Ativo_Sel ,
                                       int AV7Contrato_Codigo ,
                                       String AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace ,
                                       String AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace ,
                                       String AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace ,
                                       String AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace ,
                                       String AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace ,
                                       String AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace ,
                                       String AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace ,
                                       String AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace ,
                                       String AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace ,
                                       String AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV65Pgmname ,
                                       int A1560NotaEmpenho_Codigo ,
                                       int A74Contrato_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFMH2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_ITENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_ITENTIFICADOR", StringUtil.RTrim( A1564NotaEmpenho_Itentificador));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_DEMISSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_DEMISSAO", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_QTD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_QTD", StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_VALOR", StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_SALDOANT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_SALDOANT", StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_SALDOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_SALDOPOS", StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_ATIVO", GetSecureSignedToken( sPrefix, A1570NotaEmpenho_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_ATIVO", StringUtil.BoolToStr( A1570NotaEmpenho_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV65Pgmname = "ContratoContratoNotaEmpenhoWC";
         context.Gx_err = 0;
      }

      protected void RFMH2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: E24MH2 */
         E24MH2 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         nGXsfl_19_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV18TFNotaEmpenho_Codigo ,
                                                 AV19TFNotaEmpenho_Codigo_To ,
                                                 AV22TFSaldoContrato_Codigo ,
                                                 AV23TFSaldoContrato_Codigo_To ,
                                                 AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                                 AV26TFSaldoContrato_UnidadeMedicao_Sigla ,
                                                 AV31TFNotaEmpenho_Itentificador_Sel ,
                                                 AV30TFNotaEmpenho_Itentificador ,
                                                 AV34TFNotaEmpenho_DEmissao ,
                                                 AV35TFNotaEmpenho_DEmissao_To ,
                                                 AV40TFNotaEmpenho_Qtd ,
                                                 AV41TFNotaEmpenho_Qtd_To ,
                                                 AV44TFNotaEmpenho_Valor ,
                                                 AV45TFNotaEmpenho_Valor_To ,
                                                 AV48TFNotaEmpenho_SaldoAnt ,
                                                 AV49TFNotaEmpenho_SaldoAnt_To ,
                                                 AV52TFNotaEmpenho_SaldoPos ,
                                                 AV53TFNotaEmpenho_SaldoPos_To ,
                                                 AV56TFNotaEmpenho_Ativo_Sel ,
                                                 A1560NotaEmpenho_Codigo ,
                                                 A1561SaldoContrato_Codigo ,
                                                 A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                                 A1564NotaEmpenho_Itentificador ,
                                                 A1565NotaEmpenho_DEmissao ,
                                                 A1567NotaEmpenho_Qtd ,
                                                 A1566NotaEmpenho_Valor ,
                                                 A1568NotaEmpenho_SaldoAnt ,
                                                 A1569NotaEmpenho_SaldoPos ,
                                                 A1570NotaEmpenho_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A74Contrato_Codigo ,
                                                 AV7Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                                 TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV26TFSaldoContrato_UnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFSaldoContrato_UnidadeMedicao_Sigla", AV26TFSaldoContrato_UnidadeMedicao_Sigla);
            lV30TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFNotaEmpenho_Itentificador", AV30TFNotaEmpenho_Itentificador);
            /* Using cursor H00MH3 */
            pr_default.execute(1, new Object[] {AV7Contrato_Codigo, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, lV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, lV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_19_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1783SaldoContrato_UnidadeMedicao_Codigo = H00MH3_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
               A74Contrato_Codigo = H00MH3_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A1570NotaEmpenho_Ativo = H00MH3_A1570NotaEmpenho_Ativo[0];
               n1570NotaEmpenho_Ativo = H00MH3_n1570NotaEmpenho_Ativo[0];
               A1569NotaEmpenho_SaldoPos = H00MH3_A1569NotaEmpenho_SaldoPos[0];
               n1569NotaEmpenho_SaldoPos = H00MH3_n1569NotaEmpenho_SaldoPos[0];
               A1568NotaEmpenho_SaldoAnt = H00MH3_A1568NotaEmpenho_SaldoAnt[0];
               n1568NotaEmpenho_SaldoAnt = H00MH3_n1568NotaEmpenho_SaldoAnt[0];
               A1566NotaEmpenho_Valor = H00MH3_A1566NotaEmpenho_Valor[0];
               n1566NotaEmpenho_Valor = H00MH3_n1566NotaEmpenho_Valor[0];
               A1567NotaEmpenho_Qtd = H00MH3_A1567NotaEmpenho_Qtd[0];
               n1567NotaEmpenho_Qtd = H00MH3_n1567NotaEmpenho_Qtd[0];
               A1565NotaEmpenho_DEmissao = H00MH3_A1565NotaEmpenho_DEmissao[0];
               n1565NotaEmpenho_DEmissao = H00MH3_n1565NotaEmpenho_DEmissao[0];
               A1564NotaEmpenho_Itentificador = H00MH3_A1564NotaEmpenho_Itentificador[0];
               n1564NotaEmpenho_Itentificador = H00MH3_n1564NotaEmpenho_Itentificador[0];
               A1785SaldoContrato_UnidadeMedicao_Sigla = H00MH3_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
               n1785SaldoContrato_UnidadeMedicao_Sigla = H00MH3_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
               A1561SaldoContrato_Codigo = H00MH3_A1561SaldoContrato_Codigo[0];
               A1560NotaEmpenho_Codigo = H00MH3_A1560NotaEmpenho_Codigo[0];
               A1783SaldoContrato_UnidadeMedicao_Codigo = H00MH3_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
               A74Contrato_Codigo = H00MH3_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A1785SaldoContrato_UnidadeMedicao_Sigla = H00MH3_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
               n1785SaldoContrato_UnidadeMedicao_Sigla = H00MH3_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
               /* Execute user event: E25MH2 */
               E25MH2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 19;
            WBMH0( ) ;
         }
         nGXsfl_19_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV18TFNotaEmpenho_Codigo ,
                                              AV19TFNotaEmpenho_Codigo_To ,
                                              AV22TFSaldoContrato_Codigo ,
                                              AV23TFSaldoContrato_Codigo_To ,
                                              AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                              AV26TFSaldoContrato_UnidadeMedicao_Sigla ,
                                              AV31TFNotaEmpenho_Itentificador_Sel ,
                                              AV30TFNotaEmpenho_Itentificador ,
                                              AV34TFNotaEmpenho_DEmissao ,
                                              AV35TFNotaEmpenho_DEmissao_To ,
                                              AV40TFNotaEmpenho_Qtd ,
                                              AV41TFNotaEmpenho_Qtd_To ,
                                              AV44TFNotaEmpenho_Valor ,
                                              AV45TFNotaEmpenho_Valor_To ,
                                              AV48TFNotaEmpenho_SaldoAnt ,
                                              AV49TFNotaEmpenho_SaldoAnt_To ,
                                              AV52TFNotaEmpenho_SaldoPos ,
                                              AV53TFNotaEmpenho_SaldoPos_To ,
                                              AV56TFNotaEmpenho_Ativo_Sel ,
                                              A1560NotaEmpenho_Codigo ,
                                              A1561SaldoContrato_Codigo ,
                                              A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                              A1564NotaEmpenho_Itentificador ,
                                              A1565NotaEmpenho_DEmissao ,
                                              A1567NotaEmpenho_Qtd ,
                                              A1566NotaEmpenho_Valor ,
                                              A1568NotaEmpenho_SaldoAnt ,
                                              A1569NotaEmpenho_SaldoPos ,
                                              A1570NotaEmpenho_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A74Contrato_Codigo ,
                                              AV7Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV26TFSaldoContrato_UnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFSaldoContrato_UnidadeMedicao_Sigla", AV26TFSaldoContrato_UnidadeMedicao_Sigla);
         lV30TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFNotaEmpenho_Itentificador", AV30TFNotaEmpenho_Itentificador);
         /* Using cursor H00MH4 */
         pr_default.execute(2, new Object[] {AV7Contrato_Codigo, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, lV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, lV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To});
         GRID_nRecordCount = H00MH4_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, AV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, AV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, AV56TFNotaEmpenho_Ativo_Sel, AV7Contrato_Codigo, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV6WWPContext, AV65Pgmname, A1560NotaEmpenho_Codigo, A74Contrato_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, AV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, AV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, AV56TFNotaEmpenho_Ativo_Sel, AV7Contrato_Codigo, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV6WWPContext, AV65Pgmname, A1560NotaEmpenho_Codigo, A74Contrato_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, AV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, AV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, AV56TFNotaEmpenho_Ativo_Sel, AV7Contrato_Codigo, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV6WWPContext, AV65Pgmname, A1560NotaEmpenho_Codigo, A74Contrato_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, AV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, AV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, AV56TFNotaEmpenho_Ativo_Sel, AV7Contrato_Codigo, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV6WWPContext, AV65Pgmname, A1560NotaEmpenho_Codigo, A74Contrato_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFNotaEmpenho_Codigo, AV19TFNotaEmpenho_Codigo_To, AV22TFSaldoContrato_Codigo, AV23TFSaldoContrato_Codigo_To, AV26TFSaldoContrato_UnidadeMedicao_Sigla, AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel, AV30TFNotaEmpenho_Itentificador, AV31TFNotaEmpenho_Itentificador_Sel, AV34TFNotaEmpenho_DEmissao, AV35TFNotaEmpenho_DEmissao_To, AV40TFNotaEmpenho_Qtd, AV41TFNotaEmpenho_Qtd_To, AV44TFNotaEmpenho_Valor, AV45TFNotaEmpenho_Valor_To, AV48TFNotaEmpenho_SaldoAnt, AV49TFNotaEmpenho_SaldoAnt_To, AV52TFNotaEmpenho_SaldoPos, AV53TFNotaEmpenho_SaldoPos_To, AV56TFNotaEmpenho_Ativo_Sel, AV7Contrato_Codigo, AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV6WWPContext, AV65Pgmname, A1560NotaEmpenho_Codigo, A74Contrato_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPMH0( )
      {
         /* Before Start, stand alone formulas. */
         AV65Pgmname = "ContratoContratoNotaEmpenhoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23MH2 */
         E23MH2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV58DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_CODIGOTITLEFILTERDATA"), AV17NotaEmpenho_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_CODIGOTITLEFILTERDATA"), AV21SaldoContrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLEFILTERDATA"), AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA"), AV29NotaEmpenho_ItentificadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA"), AV33NotaEmpenho_DEmissaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_QTDTITLEFILTERDATA"), AV39NotaEmpenho_QtdTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_VALORTITLEFILTERDATA"), AV43NotaEmpenho_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_SALDOANTTITLEFILTERDATA"), AV47NotaEmpenho_SaldoAntTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA"), AV51NotaEmpenho_SaldoPosTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vNOTAEMPENHO_ATIVOTITLEFILTERDATA"), AV55NotaEmpenho_AtivoTitleFilterData);
            /* Read variables values. */
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_CODIGO");
               GX_FocusControl = edtavTfnotaempenho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFNotaEmpenho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0)));
            }
            else
            {
               AV18TFNotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_CODIGO_TO");
               GX_FocusControl = edtavTfnotaempenho_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19TFNotaEmpenho_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0)));
            }
            else
            {
               AV19TFNotaEmpenho_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CODIGO");
               GX_FocusControl = edtavTfsaldocontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFSaldoContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0)));
            }
            else
            {
               AV22TFSaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfsaldocontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFSaldoContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV23TFSaldoContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0)));
            }
            AV26TFSaldoContrato_UnidadeMedicao_Sigla = StringUtil.Upper( cgiGet( edtavTfsaldocontrato_unidademedicao_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFSaldoContrato_UnidadeMedicao_Sigla", AV26TFSaldoContrato_UnidadeMedicao_Sigla);
            AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfsaldocontrato_unidademedicao_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel", AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel);
            AV30TFNotaEmpenho_Itentificador = cgiGet( edtavTfnotaempenho_itentificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFNotaEmpenho_Itentificador", AV30TFNotaEmpenho_Itentificador);
            AV31TFNotaEmpenho_Itentificador_Sel = cgiGet( edtavTfnotaempenho_itentificador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFNotaEmpenho_Itentificador_Sel", AV31TFNotaEmpenho_Itentificador_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfnotaempenho_demissao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFNota Empenho_DEmissao"}), 1, "vTFNOTAEMPENHO_DEMISSAO");
               GX_FocusControl = edtavTfnotaempenho_demissao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV34TFNotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtavTfnotaempenho_demissao_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfnotaempenho_demissao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFNota Empenho_DEmissao_To"}), 1, "vTFNOTAEMPENHO_DEMISSAO_TO");
               GX_FocusControl = edtavTfnotaempenho_demissao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV35TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( cgiGet( edtavTfnotaempenho_demissao_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_notaempenho_demissaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Nota Empenho_DEmissao Aux Date"}), 1, "vDDO_NOTAEMPENHO_DEMISSAOAUXDATE");
               GX_FocusControl = edtavDdo_notaempenho_demissaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36DDO_NotaEmpenho_DEmissaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_NotaEmpenho_DEmissaoAuxDate", context.localUtil.Format(AV36DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"));
            }
            else
            {
               AV36DDO_NotaEmpenho_DEmissaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_notaempenho_demissaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_NotaEmpenho_DEmissaoAuxDate", context.localUtil.Format(AV36DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_notaempenho_demissaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Nota Empenho_DEmissao Aux Date To"}), 1, "vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO");
               GX_FocusControl = edtavDdo_notaempenho_demissaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37DDO_NotaEmpenho_DEmissaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DDO_NotaEmpenho_DEmissaoAuxDateTo", context.localUtil.Format(AV37DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV37DDO_NotaEmpenho_DEmissaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_notaempenho_demissaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DDO_NotaEmpenho_DEmissaoAuxDateTo", context.localUtil.Format(AV37DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_QTD");
               GX_FocusControl = edtavTfnotaempenho_qtd_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFNotaEmpenho_Qtd = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5)));
            }
            else
            {
               AV40TFNotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_QTD_TO");
               GX_FocusControl = edtavTfnotaempenho_qtd_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFNotaEmpenho_Qtd_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5)));
            }
            else
            {
               AV41TFNotaEmpenho_Qtd_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_VALOR");
               GX_FocusControl = edtavTfnotaempenho_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFNotaEmpenho_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5)));
            }
            else
            {
               AV44TFNotaEmpenho_Valor = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_VALOR_TO");
               GX_FocusControl = edtavTfnotaempenho_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFNotaEmpenho_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5)));
            }
            else
            {
               AV45TFNotaEmpenho_Valor_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOANT");
               GX_FocusControl = edtavTfnotaempenho_saldoant_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFNotaEmpenho_SaldoAnt = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5)));
            }
            else
            {
               AV48TFNotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOANT_TO");
               GX_FocusControl = edtavTfnotaempenho_saldoant_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFNotaEmpenho_SaldoAnt_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5)));
            }
            else
            {
               AV49TFNotaEmpenho_SaldoAnt_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOPOS");
               GX_FocusControl = edtavTfnotaempenho_saldopos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFNotaEmpenho_SaldoPos = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5)));
            }
            else
            {
               AV52TFNotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOPOS_TO");
               GX_FocusControl = edtavTfnotaempenho_saldopos_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFNotaEmpenho_SaldoPos_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5)));
            }
            else
            {
               AV53TFNotaEmpenho_SaldoPos_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_ATIVO_SEL");
               GX_FocusControl = edtavTfnotaempenho_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFNotaEmpenho_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0));
            }
            else
            {
               AV56TFNotaEmpenho_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfnotaempenho_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0));
            }
            AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace", AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace);
            AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace);
            AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace", AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace);
            AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace", AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace);
            AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace", AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace);
            AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace", AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace);
            AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace", AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace);
            AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace", AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace);
            AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace", AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace);
            AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace", AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_19"), ",", "."));
            AV60GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV61GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_notaempenho_codigo_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Caption");
            Ddo_notaempenho_codigo_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Tooltip");
            Ddo_notaempenho_codigo_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Cls");
            Ddo_notaempenho_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtext_set");
            Ddo_notaempenho_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtextto_set");
            Ddo_notaempenho_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Dropdownoptionstype");
            Ddo_notaempenho_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Titlecontrolidtoreplace");
            Ddo_notaempenho_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includesortasc"));
            Ddo_notaempenho_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includesortdsc"));
            Ddo_notaempenho_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Sortedstatus");
            Ddo_notaempenho_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includefilter"));
            Ddo_notaempenho_codigo_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filtertype");
            Ddo_notaempenho_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filterisrange"));
            Ddo_notaempenho_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Includedatalist"));
            Ddo_notaempenho_codigo_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Sortasc");
            Ddo_notaempenho_codigo_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Sortdsc");
            Ddo_notaempenho_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Cleanfilter");
            Ddo_notaempenho_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Rangefilterfrom");
            Ddo_notaempenho_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Rangefilterto");
            Ddo_notaempenho_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Searchbuttontext");
            Ddo_saldocontrato_codigo_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Caption");
            Ddo_saldocontrato_codigo_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Tooltip");
            Ddo_saldocontrato_codigo_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Cls");
            Ddo_saldocontrato_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtext_set");
            Ddo_saldocontrato_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtextto_set");
            Ddo_saldocontrato_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includesortasc"));
            Ddo_saldocontrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includesortdsc"));
            Ddo_saldocontrato_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Sortedstatus");
            Ddo_saldocontrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includefilter"));
            Ddo_saldocontrato_codigo_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filtertype");
            Ddo_saldocontrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filterisrange"));
            Ddo_saldocontrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Includedatalist"));
            Ddo_saldocontrato_codigo_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Sortasc");
            Ddo_saldocontrato_codigo_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Sortdsc");
            Ddo_saldocontrato_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Cleanfilter");
            Ddo_saldocontrato_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Rangefilterfrom");
            Ddo_saldocontrato_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Rangefilterto");
            Ddo_saldocontrato_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Searchbuttontext");
            Ddo_saldocontrato_unidademedicao_sigla_Caption = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Caption");
            Ddo_saldocontrato_unidademedicao_sigla_Tooltip = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Tooltip");
            Ddo_saldocontrato_unidademedicao_sigla_Cls = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Cls");
            Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filteredtext_set");
            Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Selectedvalue_set");
            Ddo_saldocontrato_unidademedicao_sigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Dropdownoptionstype");
            Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Titlecontrolidtoreplace");
            Ddo_saldocontrato_unidademedicao_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includesortasc"));
            Ddo_saldocontrato_unidademedicao_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includesortdsc"));
            Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Sortedstatus");
            Ddo_saldocontrato_unidademedicao_sigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includefilter"));
            Ddo_saldocontrato_unidademedicao_sigla_Filtertype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filtertype");
            Ddo_saldocontrato_unidademedicao_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filterisrange"));
            Ddo_saldocontrato_unidademedicao_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Includedatalist"));
            Ddo_saldocontrato_unidademedicao_sigla_Datalisttype = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Datalisttype");
            Ddo_saldocontrato_unidademedicao_sigla_Datalistproc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Datalistproc");
            Ddo_saldocontrato_unidademedicao_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_saldocontrato_unidademedicao_sigla_Sortasc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Sortasc");
            Ddo_saldocontrato_unidademedicao_sigla_Sortdsc = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Sortdsc");
            Ddo_saldocontrato_unidademedicao_sigla_Loadingdata = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Loadingdata");
            Ddo_saldocontrato_unidademedicao_sigla_Cleanfilter = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Cleanfilter");
            Ddo_saldocontrato_unidademedicao_sigla_Noresultsfound = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Noresultsfound");
            Ddo_saldocontrato_unidademedicao_sigla_Searchbuttontext = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Searchbuttontext");
            Ddo_notaempenho_itentificador_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Caption");
            Ddo_notaempenho_itentificador_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Tooltip");
            Ddo_notaempenho_itentificador_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Cls");
            Ddo_notaempenho_itentificador_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_set");
            Ddo_notaempenho_itentificador_Selectedvalue_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_set");
            Ddo_notaempenho_itentificador_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Dropdownoptionstype");
            Ddo_notaempenho_itentificador_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Titlecontrolidtoreplace");
            Ddo_notaempenho_itentificador_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortasc"));
            Ddo_notaempenho_itentificador_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortdsc"));
            Ddo_notaempenho_itentificador_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Sortedstatus");
            Ddo_notaempenho_itentificador_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includefilter"));
            Ddo_notaempenho_itentificador_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filtertype");
            Ddo_notaempenho_itentificador_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filterisrange"));
            Ddo_notaempenho_itentificador_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Includedatalist"));
            Ddo_notaempenho_itentificador_Datalisttype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Datalisttype");
            Ddo_notaempenho_itentificador_Datalistproc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistproc");
            Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_notaempenho_itentificador_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Sortasc");
            Ddo_notaempenho_itentificador_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Sortdsc");
            Ddo_notaempenho_itentificador_Loadingdata = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Loadingdata");
            Ddo_notaempenho_itentificador_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Cleanfilter");
            Ddo_notaempenho_itentificador_Noresultsfound = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Noresultsfound");
            Ddo_notaempenho_itentificador_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Searchbuttontext");
            Ddo_notaempenho_demissao_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Caption");
            Ddo_notaempenho_demissao_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Tooltip");
            Ddo_notaempenho_demissao_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Cls");
            Ddo_notaempenho_demissao_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_set");
            Ddo_notaempenho_demissao_Filteredtextto_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_set");
            Ddo_notaempenho_demissao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Dropdownoptionstype");
            Ddo_notaempenho_demissao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Titlecontrolidtoreplace");
            Ddo_notaempenho_demissao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includesortasc"));
            Ddo_notaempenho_demissao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includesortdsc"));
            Ddo_notaempenho_demissao_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Sortedstatus");
            Ddo_notaempenho_demissao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includefilter"));
            Ddo_notaempenho_demissao_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filtertype");
            Ddo_notaempenho_demissao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filterisrange"));
            Ddo_notaempenho_demissao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Includedatalist"));
            Ddo_notaempenho_demissao_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Sortasc");
            Ddo_notaempenho_demissao_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Sortdsc");
            Ddo_notaempenho_demissao_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Cleanfilter");
            Ddo_notaempenho_demissao_Rangefilterfrom = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Rangefilterfrom");
            Ddo_notaempenho_demissao_Rangefilterto = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Rangefilterto");
            Ddo_notaempenho_demissao_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Searchbuttontext");
            Ddo_notaempenho_qtd_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Caption");
            Ddo_notaempenho_qtd_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Tooltip");
            Ddo_notaempenho_qtd_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Cls");
            Ddo_notaempenho_qtd_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtext_set");
            Ddo_notaempenho_qtd_Filteredtextto_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtextto_set");
            Ddo_notaempenho_qtd_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Dropdownoptionstype");
            Ddo_notaempenho_qtd_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Titlecontrolidtoreplace");
            Ddo_notaempenho_qtd_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Includesortasc"));
            Ddo_notaempenho_qtd_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Includesortdsc"));
            Ddo_notaempenho_qtd_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Sortedstatus");
            Ddo_notaempenho_qtd_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Includefilter"));
            Ddo_notaempenho_qtd_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Filtertype");
            Ddo_notaempenho_qtd_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Filterisrange"));
            Ddo_notaempenho_qtd_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Includedatalist"));
            Ddo_notaempenho_qtd_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Sortasc");
            Ddo_notaempenho_qtd_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Sortdsc");
            Ddo_notaempenho_qtd_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Cleanfilter");
            Ddo_notaempenho_qtd_Rangefilterfrom = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Rangefilterfrom");
            Ddo_notaempenho_qtd_Rangefilterto = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Rangefilterto");
            Ddo_notaempenho_qtd_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Searchbuttontext");
            Ddo_notaempenho_valor_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Caption");
            Ddo_notaempenho_valor_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Tooltip");
            Ddo_notaempenho_valor_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Cls");
            Ddo_notaempenho_valor_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtext_set");
            Ddo_notaempenho_valor_Filteredtextto_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtextto_set");
            Ddo_notaempenho_valor_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Dropdownoptionstype");
            Ddo_notaempenho_valor_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Titlecontrolidtoreplace");
            Ddo_notaempenho_valor_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Includesortasc"));
            Ddo_notaempenho_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Includesortdsc"));
            Ddo_notaempenho_valor_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Sortedstatus");
            Ddo_notaempenho_valor_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Includefilter"));
            Ddo_notaempenho_valor_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Filtertype");
            Ddo_notaempenho_valor_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Filterisrange"));
            Ddo_notaempenho_valor_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Includedatalist"));
            Ddo_notaempenho_valor_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Sortasc");
            Ddo_notaempenho_valor_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Sortdsc");
            Ddo_notaempenho_valor_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Cleanfilter");
            Ddo_notaempenho_valor_Rangefilterfrom = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Rangefilterfrom");
            Ddo_notaempenho_valor_Rangefilterto = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Rangefilterto");
            Ddo_notaempenho_valor_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Searchbuttontext");
            Ddo_notaempenho_saldoant_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Caption");
            Ddo_notaempenho_saldoant_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Tooltip");
            Ddo_notaempenho_saldoant_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Cls");
            Ddo_notaempenho_saldoant_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtext_set");
            Ddo_notaempenho_saldoant_Filteredtextto_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_set");
            Ddo_notaempenho_saldoant_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Dropdownoptionstype");
            Ddo_notaempenho_saldoant_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Titlecontrolidtoreplace");
            Ddo_notaempenho_saldoant_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includesortasc"));
            Ddo_notaempenho_saldoant_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includesortdsc"));
            Ddo_notaempenho_saldoant_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Sortedstatus");
            Ddo_notaempenho_saldoant_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includefilter"));
            Ddo_notaempenho_saldoant_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filtertype");
            Ddo_notaempenho_saldoant_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filterisrange"));
            Ddo_notaempenho_saldoant_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Includedatalist"));
            Ddo_notaempenho_saldoant_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Sortasc");
            Ddo_notaempenho_saldoant_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Sortdsc");
            Ddo_notaempenho_saldoant_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Cleanfilter");
            Ddo_notaempenho_saldoant_Rangefilterfrom = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Rangefilterfrom");
            Ddo_notaempenho_saldoant_Rangefilterto = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Rangefilterto");
            Ddo_notaempenho_saldoant_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Searchbuttontext");
            Ddo_notaempenho_saldopos_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Caption");
            Ddo_notaempenho_saldopos_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Tooltip");
            Ddo_notaempenho_saldopos_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Cls");
            Ddo_notaempenho_saldopos_Filteredtext_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_set");
            Ddo_notaempenho_saldopos_Filteredtextto_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_set");
            Ddo_notaempenho_saldopos_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Dropdownoptionstype");
            Ddo_notaempenho_saldopos_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Titlecontrolidtoreplace");
            Ddo_notaempenho_saldopos_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includesortasc"));
            Ddo_notaempenho_saldopos_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includesortdsc"));
            Ddo_notaempenho_saldopos_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Sortedstatus");
            Ddo_notaempenho_saldopos_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includefilter"));
            Ddo_notaempenho_saldopos_Filtertype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filtertype");
            Ddo_notaempenho_saldopos_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filterisrange"));
            Ddo_notaempenho_saldopos_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Includedatalist"));
            Ddo_notaempenho_saldopos_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Sortasc");
            Ddo_notaempenho_saldopos_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Sortdsc");
            Ddo_notaempenho_saldopos_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Cleanfilter");
            Ddo_notaempenho_saldopos_Rangefilterfrom = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Rangefilterfrom");
            Ddo_notaempenho_saldopos_Rangefilterto = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Rangefilterto");
            Ddo_notaempenho_saldopos_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Searchbuttontext");
            Ddo_notaempenho_ativo_Caption = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Caption");
            Ddo_notaempenho_ativo_Tooltip = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Tooltip");
            Ddo_notaempenho_ativo_Cls = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Cls");
            Ddo_notaempenho_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Selectedvalue_set");
            Ddo_notaempenho_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Dropdownoptionstype");
            Ddo_notaempenho_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Titlecontrolidtoreplace");
            Ddo_notaempenho_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includesortasc"));
            Ddo_notaempenho_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includesortdsc"));
            Ddo_notaempenho_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Sortedstatus");
            Ddo_notaempenho_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includefilter"));
            Ddo_notaempenho_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Includedatalist"));
            Ddo_notaempenho_ativo_Datalisttype = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Datalisttype");
            Ddo_notaempenho_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Datalistfixedvalues");
            Ddo_notaempenho_ativo_Sortasc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Sortasc");
            Ddo_notaempenho_ativo_Sortdsc = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Sortdsc");
            Ddo_notaempenho_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Cleanfilter");
            Ddo_notaempenho_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_notaempenho_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Activeeventkey");
            Ddo_notaempenho_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtext_get");
            Ddo_notaempenho_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_CODIGO_Filteredtextto_get");
            Ddo_saldocontrato_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Activeeventkey");
            Ddo_saldocontrato_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtext_get");
            Ddo_saldocontrato_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_CODIGO_Filteredtextto_get");
            Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Activeeventkey");
            Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Filteredtext_get");
            Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_Selectedvalue_get");
            Ddo_notaempenho_itentificador_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Activeeventkey");
            Ddo_notaempenho_itentificador_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_get");
            Ddo_notaempenho_itentificador_Selectedvalue_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_get");
            Ddo_notaempenho_demissao_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Activeeventkey");
            Ddo_notaempenho_demissao_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_get");
            Ddo_notaempenho_demissao_Filteredtextto_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_get");
            Ddo_notaempenho_qtd_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Activeeventkey");
            Ddo_notaempenho_qtd_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtext_get");
            Ddo_notaempenho_qtd_Filteredtextto_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_QTD_Filteredtextto_get");
            Ddo_notaempenho_valor_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Activeeventkey");
            Ddo_notaempenho_valor_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtext_get");
            Ddo_notaempenho_valor_Filteredtextto_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_VALOR_Filteredtextto_get");
            Ddo_notaempenho_saldoant_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Activeeventkey");
            Ddo_notaempenho_saldoant_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtext_get");
            Ddo_notaempenho_saldoant_Filteredtextto_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_get");
            Ddo_notaempenho_saldopos_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Activeeventkey");
            Ddo_notaempenho_saldopos_Filteredtext_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_get");
            Ddo_notaempenho_saldopos_Filteredtextto_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_get");
            Ddo_notaempenho_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Activeeventkey");
            Ddo_notaempenho_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_NOTAEMPENHO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_CODIGO"), ",", ".") != Convert.ToDecimal( AV18TFNotaEmpenho_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV19TFNotaEmpenho_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV22TFSaldoContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV23TFSaldoContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA"), AV26TFSaldoContrato_UnidadeMedicao_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL"), AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_ITENTIFICADOR"), AV30TFNotaEmpenho_Itentificador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_ITENTIFICADOR_SEL"), AV31TFNotaEmpenho_Itentificador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_DEMISSAO"), 0) != AV34TFNotaEmpenho_DEmissao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_DEMISSAO_TO"), 0) != AV35TFNotaEmpenho_DEmissao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_QTD"), ",", ".") != AV40TFNotaEmpenho_Qtd )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_QTD_TO"), ",", ".") != AV41TFNotaEmpenho_Qtd_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_VALOR"), ",", ".") != AV44TFNotaEmpenho_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_VALOR_TO"), ",", ".") != AV45TFNotaEmpenho_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOANT"), ",", ".") != AV48TFNotaEmpenho_SaldoAnt )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOANT_TO"), ",", ".") != AV49TFNotaEmpenho_SaldoAnt_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOPOS"), ",", ".") != AV52TFNotaEmpenho_SaldoPos )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_SALDOPOS_TO"), ",", ".") != AV53TFNotaEmpenho_SaldoPos_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFNOTAEMPENHO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV56TFNotaEmpenho_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23MH2 */
         E23MH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23MH2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfnotaempenho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_codigo_Visible), 5, 0)));
         edtavTfnotaempenho_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_codigo_to_Visible), 5, 0)));
         edtavTfsaldocontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_codigo_Visible), 5, 0)));
         edtavTfsaldocontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_codigo_to_Visible), 5, 0)));
         edtavTfsaldocontrato_unidademedicao_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_unidademedicao_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_unidademedicao_sigla_Visible), 5, 0)));
         edtavTfsaldocontrato_unidademedicao_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsaldocontrato_unidademedicao_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_unidademedicao_sigla_sel_Visible), 5, 0)));
         edtavTfnotaempenho_itentificador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_itentificador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_itentificador_Visible), 5, 0)));
         edtavTfnotaempenho_itentificador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_itentificador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_itentificador_sel_Visible), 5, 0)));
         edtavTfnotaempenho_demissao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_demissao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_demissao_Visible), 5, 0)));
         edtavTfnotaempenho_demissao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_demissao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_demissao_to_Visible), 5, 0)));
         edtavTfnotaempenho_qtd_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_qtd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_qtd_Visible), 5, 0)));
         edtavTfnotaempenho_qtd_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_qtd_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_qtd_to_Visible), 5, 0)));
         edtavTfnotaempenho_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_valor_Visible), 5, 0)));
         edtavTfnotaempenho_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_valor_to_Visible), 5, 0)));
         edtavTfnotaempenho_saldoant_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_saldoant_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldoant_Visible), 5, 0)));
         edtavTfnotaempenho_saldoant_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_saldoant_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldoant_to_Visible), 5, 0)));
         edtavTfnotaempenho_saldopos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_saldopos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldopos_Visible), 5, 0)));
         edtavTfnotaempenho_saldopos_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_saldopos_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldopos_to_Visible), 5, 0)));
         edtavTfnotaempenho_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfnotaempenho_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_ativo_sel_Visible), 5, 0)));
         Ddo_notaempenho_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_codigo_Titlecontrolidtoreplace);
         AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace = Ddo_notaempenho_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace", AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace);
         edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_codigo_Titlecontrolidtoreplace);
         AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace = Ddo_saldocontrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace);
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_UnidadeMedicao_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace);
         AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace = Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace", AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace);
         edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_itentificador_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Itentificador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_itentificador_Titlecontrolidtoreplace);
         AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = Ddo_notaempenho_itentificador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace", AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace);
         edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_demissao_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_DEmissao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_demissao_Titlecontrolidtoreplace);
         AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = Ddo_notaempenho_demissao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace", AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace);
         edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_qtd_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Qtd";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_qtd_Titlecontrolidtoreplace);
         AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace = Ddo_notaempenho_qtd_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace", AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace);
         edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_valor_Titlecontrolidtoreplace);
         AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace = Ddo_notaempenho_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace", AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace);
         edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_saldoant_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_SaldoAnt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_saldoant_Titlecontrolidtoreplace);
         AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = Ddo_notaempenho_saldoant_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace", AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace);
         edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_saldopos_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_SaldoPos";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_saldopos_Titlecontrolidtoreplace);
         AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = Ddo_notaempenho_saldopos_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace", AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace);
         edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_ativo_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_ativo_Titlecontrolidtoreplace);
         AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace = Ddo_notaempenho_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace", AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace);
         edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV58DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV58DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24MH2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV17NotaEmpenho_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV21SaldoContrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV29NotaEmpenho_ItentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33NotaEmpenho_DEmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39NotaEmpenho_QtdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43NotaEmpenho_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47NotaEmpenho_SaldoAntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51NotaEmpenho_SaldoPosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55NotaEmpenho_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtNotaEmpenho_Codigo_Titleformat = 2;
         edtNotaEmpenho_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_Codigo_Internalname, "Title", edtNotaEmpenho_Codigo_Title);
         dynSaldoContrato_Codigo_Titleformat = 2;
         dynSaldoContrato_Codigo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo Contrato", AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynSaldoContrato_Codigo_Internalname, "Title", dynSaldoContrato_Codigo.Title.Text);
         edtSaldoContrato_UnidadeMedicao_Sigla_Titleformat = 2;
         edtSaldoContrato_UnidadeMedicao_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade de Medi��o", AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_UnidadeMedicao_Sigla_Internalname, "Title", edtSaldoContrato_UnidadeMedicao_Sigla_Title);
         edtNotaEmpenho_Itentificador_Titleformat = 2;
         edtNotaEmpenho_Itentificador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Identificador", AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_Itentificador_Internalname, "Title", edtNotaEmpenho_Itentificador_Title);
         edtNotaEmpenho_DEmissao_Titleformat = 2;
         edtNotaEmpenho_DEmissao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data Emiss�o", AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_DEmissao_Internalname, "Title", edtNotaEmpenho_DEmissao_Title);
         edtNotaEmpenho_Qtd_Titleformat = 2;
         edtNotaEmpenho_Qtd_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Quantidade", AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_Qtd_Internalname, "Title", edtNotaEmpenho_Qtd_Title);
         edtNotaEmpenho_Valor_Titleformat = 2;
         edtNotaEmpenho_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_Valor_Internalname, "Title", edtNotaEmpenho_Valor_Title);
         edtNotaEmpenho_SaldoAnt_Titleformat = 2;
         edtNotaEmpenho_SaldoAnt_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo Anterior", AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_SaldoAnt_Internalname, "Title", edtNotaEmpenho_SaldoAnt_Title);
         edtNotaEmpenho_SaldoPos_Titleformat = 2;
         edtNotaEmpenho_SaldoPos_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo Posterior", AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNotaEmpenho_SaldoPos_Internalname, "Title", edtNotaEmpenho_SaldoPos_Title);
         chkNotaEmpenho_Ativo_Titleformat = 2;
         chkNotaEmpenho_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkNotaEmpenho_Ativo_Internalname, "Title", chkNotaEmpenho_Ativo.Title.Text);
         AV60GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60GridCurrentPage), 10, 0)));
         AV61GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17NotaEmpenho_CodigoTitleFilterData", AV17NotaEmpenho_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV21SaldoContrato_CodigoTitleFilterData", AV21SaldoContrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData", AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV29NotaEmpenho_ItentificadorTitleFilterData", AV29NotaEmpenho_ItentificadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV33NotaEmpenho_DEmissaoTitleFilterData", AV33NotaEmpenho_DEmissaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV39NotaEmpenho_QtdTitleFilterData", AV39NotaEmpenho_QtdTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV43NotaEmpenho_ValorTitleFilterData", AV43NotaEmpenho_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47NotaEmpenho_SaldoAntTitleFilterData", AV47NotaEmpenho_SaldoAntTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV51NotaEmpenho_SaldoPosTitleFilterData", AV51NotaEmpenho_SaldoPosTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55NotaEmpenho_AtivoTitleFilterData", AV55NotaEmpenho_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11MH2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV59PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV59PageToGo) ;
         }
      }

      protected void E12MH2( )
      {
         /* Ddo_notaempenho_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV18TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( Ddo_notaempenho_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0)));
            AV19TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( Ddo_notaempenho_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13MH2( )
      {
         /* Ddo_saldocontrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV22TFSaldoContrato_Codigo = (int)(NumberUtil.Val( Ddo_saldocontrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0)));
            AV23TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_saldocontrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14MH2( )
      {
         /* Ddo_saldocontrato_unidademedicao_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV26TFSaldoContrato_UnidadeMedicao_Sigla = Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFSaldoContrato_UnidadeMedicao_Sigla", AV26TFSaldoContrato_UnidadeMedicao_Sigla);
            AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel = Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel", AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15MH2( )
      {
         /* Ddo_notaempenho_itentificador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_itentificador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_itentificador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_itentificador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_itentificador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_itentificador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV30TFNotaEmpenho_Itentificador = Ddo_notaempenho_itentificador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFNotaEmpenho_Itentificador", AV30TFNotaEmpenho_Itentificador);
            AV31TFNotaEmpenho_Itentificador_Sel = Ddo_notaempenho_itentificador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFNotaEmpenho_Itentificador_Sel", AV31TFNotaEmpenho_Itentificador_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E16MH2( )
      {
         /* Ddo_notaempenho_demissao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_demissao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_demissao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_demissao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_demissao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_demissao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFNotaEmpenho_DEmissao = context.localUtil.CToT( Ddo_notaempenho_demissao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            AV35TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( Ddo_notaempenho_demissao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV35TFNotaEmpenho_DEmissao_To) )
            {
               AV35TFNotaEmpenho_DEmissao_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV35TFNotaEmpenho_DEmissao_To)), (short)(DateTimeUtil.Month( AV35TFNotaEmpenho_DEmissao_To)), (short)(DateTimeUtil.Day( AV35TFNotaEmpenho_DEmissao_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E17MH2( )
      {
         /* Ddo_notaempenho_qtd_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_qtd_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_qtd_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_qtd_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_qtd_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_qtd_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFNotaEmpenho_Qtd = NumberUtil.Val( Ddo_notaempenho_qtd_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5)));
            AV41TFNotaEmpenho_Qtd_To = NumberUtil.Val( Ddo_notaempenho_qtd_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E18MH2( )
      {
         /* Ddo_notaempenho_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFNotaEmpenho_Valor = NumberUtil.Val( Ddo_notaempenho_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5)));
            AV45TFNotaEmpenho_Valor_To = NumberUtil.Val( Ddo_notaempenho_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E19MH2( )
      {
         /* Ddo_notaempenho_saldoant_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_saldoant_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldoant_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldoant_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldoant_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldoant_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFNotaEmpenho_SaldoAnt = NumberUtil.Val( Ddo_notaempenho_saldoant_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5)));
            AV49TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( Ddo_notaempenho_saldoant_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E20MH2( )
      {
         /* Ddo_notaempenho_saldopos_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_saldopos_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldopos_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldopos_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldopos_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldopos_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFNotaEmpenho_SaldoPos = NumberUtil.Val( Ddo_notaempenho_saldopos_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5)));
            AV53TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( Ddo_notaempenho_saldopos_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E21MH2( )
      {
         /* Ddo_notaempenho_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( Ddo_notaempenho_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E25MH2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("notaempenho.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1560NotaEmpenho_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
            AV16Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV16Update);
            AV64Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV16Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV16Update);
            AV64Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 19;
         }
         sendrow_192( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_19_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(19, GridRow);
         }
      }

      protected void E22MH2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("notaempenho.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_notaempenho_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
         Ddo_saldocontrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
         Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus);
         Ddo_notaempenho_itentificador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
         Ddo_notaempenho_demissao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
         Ddo_notaempenho_qtd_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
         Ddo_notaempenho_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
         Ddo_notaempenho_saldoant_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
         Ddo_notaempenho_saldopos_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
         Ddo_notaempenho_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_notaempenho_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_saldocontrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "SortedStatus", Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_notaempenho_itentificador_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_notaempenho_demissao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_notaempenho_qtd_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_notaempenho_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_notaempenho_saldoant_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_notaempenho_saldopos_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_notaempenho_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV65Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV65Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV65Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_CODIGO") == 0 )
            {
               AV18TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0)));
               AV19TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0)));
               if ( ! (0==AV18TFNotaEmpenho_Codigo) )
               {
                  Ddo_notaempenho_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "FilteredText_set", Ddo_notaempenho_codigo_Filteredtext_set);
               }
               if ( ! (0==AV19TFNotaEmpenho_Codigo_To) )
               {
                  Ddo_notaempenho_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_codigo_Internalname, "FilteredTextTo_set", Ddo_notaempenho_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CODIGO") == 0 )
            {
               AV22TFSaldoContrato_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0)));
               AV23TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0)));
               if ( ! (0==AV22TFSaldoContrato_Codigo) )
               {
                  Ddo_saldocontrato_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "FilteredText_set", Ddo_saldocontrato_codigo_Filteredtext_set);
               }
               if ( ! (0==AV23TFSaldoContrato_Codigo_To) )
               {
                  Ddo_saldocontrato_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_codigo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA") == 0 )
            {
               AV26TFSaldoContrato_UnidadeMedicao_Sigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFSaldoContrato_UnidadeMedicao_Sigla", AV26TFSaldoContrato_UnidadeMedicao_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla)) )
               {
                  Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_set = AV26TFSaldoContrato_UnidadeMedicao_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "FilteredText_set", Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL") == 0 )
            {
               AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel", AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) )
               {
                  Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_set = AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_saldocontrato_unidademedicao_sigla_Internalname, "SelectedValue_set", Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR") == 0 )
            {
               AV30TFNotaEmpenho_Itentificador = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFNotaEmpenho_Itentificador", AV30TFNotaEmpenho_Itentificador);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador)) )
               {
                  Ddo_notaempenho_itentificador_Filteredtext_set = AV30TFNotaEmpenho_Itentificador;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "FilteredText_set", Ddo_notaempenho_itentificador_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR_SEL") == 0 )
            {
               AV31TFNotaEmpenho_Itentificador_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFNotaEmpenho_Itentificador_Sel", AV31TFNotaEmpenho_Itentificador_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel)) )
               {
                  Ddo_notaempenho_itentificador_Selectedvalue_set = AV31TFNotaEmpenho_Itentificador_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_itentificador_Internalname, "SelectedValue_set", Ddo_notaempenho_itentificador_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_DEMISSAO") == 0 )
            {
               AV34TFNotaEmpenho_DEmissao = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
               AV35TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV34TFNotaEmpenho_DEmissao) )
               {
                  AV36DDO_NotaEmpenho_DEmissaoAuxDate = DateTimeUtil.ResetTime(AV34TFNotaEmpenho_DEmissao);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_NotaEmpenho_DEmissaoAuxDate", context.localUtil.Format(AV36DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"));
                  Ddo_notaempenho_demissao_Filteredtext_set = context.localUtil.DToC( AV36DDO_NotaEmpenho_DEmissaoAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "FilteredText_set", Ddo_notaempenho_demissao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV35TFNotaEmpenho_DEmissao_To) )
               {
                  AV37DDO_NotaEmpenho_DEmissaoAuxDateTo = DateTimeUtil.ResetTime(AV35TFNotaEmpenho_DEmissao_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DDO_NotaEmpenho_DEmissaoAuxDateTo", context.localUtil.Format(AV37DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"));
                  Ddo_notaempenho_demissao_Filteredtextto_set = context.localUtil.DToC( AV37DDO_NotaEmpenho_DEmissaoAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_demissao_Internalname, "FilteredTextTo_set", Ddo_notaempenho_demissao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_QTD") == 0 )
            {
               AV40TFNotaEmpenho_Qtd = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5)));
               AV41TFNotaEmpenho_Qtd_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV40TFNotaEmpenho_Qtd) )
               {
                  Ddo_notaempenho_qtd_Filteredtext_set = StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "FilteredText_set", Ddo_notaempenho_qtd_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV41TFNotaEmpenho_Qtd_To) )
               {
                  Ddo_notaempenho_qtd_Filteredtextto_set = StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_qtd_Internalname, "FilteredTextTo_set", Ddo_notaempenho_qtd_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_VALOR") == 0 )
            {
               AV44TFNotaEmpenho_Valor = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5)));
               AV45TFNotaEmpenho_Valor_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV44TFNotaEmpenho_Valor) )
               {
                  Ddo_notaempenho_valor_Filteredtext_set = StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "FilteredText_set", Ddo_notaempenho_valor_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV45TFNotaEmpenho_Valor_To) )
               {
                  Ddo_notaempenho_valor_Filteredtextto_set = StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_valor_Internalname, "FilteredTextTo_set", Ddo_notaempenho_valor_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOANT") == 0 )
            {
               AV48TFNotaEmpenho_SaldoAnt = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5)));
               AV49TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV48TFNotaEmpenho_SaldoAnt) )
               {
                  Ddo_notaempenho_saldoant_Filteredtext_set = StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "FilteredText_set", Ddo_notaempenho_saldoant_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV49TFNotaEmpenho_SaldoAnt_To) )
               {
                  Ddo_notaempenho_saldoant_Filteredtextto_set = StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldoant_Internalname, "FilteredTextTo_set", Ddo_notaempenho_saldoant_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOPOS") == 0 )
            {
               AV52TFNotaEmpenho_SaldoPos = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5)));
               AV53TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV52TFNotaEmpenho_SaldoPos) )
               {
                  Ddo_notaempenho_saldopos_Filteredtext_set = StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "FilteredText_set", Ddo_notaempenho_saldopos_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV53TFNotaEmpenho_SaldoPos_To) )
               {
                  Ddo_notaempenho_saldopos_Filteredtextto_set = StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_saldopos_Internalname, "FilteredTextTo_set", Ddo_notaempenho_saldopos_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ATIVO_SEL") == 0 )
            {
               AV56TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0));
               if ( ! (0==AV56TFNotaEmpenho_Ativo_Sel) )
               {
                  Ddo_notaempenho_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_notaempenho_ativo_Internalname, "SelectedValue_set", Ddo_notaempenho_ativo_Selectedvalue_set);
               }
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV65Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV18TFNotaEmpenho_Codigo) && (0==AV19TFNotaEmpenho_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV18TFNotaEmpenho_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV19TFNotaEmpenho_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV22TFSaldoContrato_Codigo) && (0==AV23TFSaldoContrato_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV22TFSaldoContrato_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV23TFSaldoContrato_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV26TFSaldoContrato_UnidadeMedicao_Sigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_ITENTIFICADOR";
            AV12GridStateFilterValue.gxTpr_Value = AV30TFNotaEmpenho_Itentificador;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_ITENTIFICADOR_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV31TFNotaEmpenho_Itentificador_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV34TFNotaEmpenho_DEmissao) && (DateTime.MinValue==AV35TFNotaEmpenho_DEmissao_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_DEMISSAO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV34TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV35TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV40TFNotaEmpenho_Qtd) && (Convert.ToDecimal(0)==AV41TFNotaEmpenho_Qtd_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_QTD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV40TFNotaEmpenho_Qtd, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV41TFNotaEmpenho_Qtd_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV44TFNotaEmpenho_Valor) && (Convert.ToDecimal(0)==AV45TFNotaEmpenho_Valor_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_VALOR";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV44TFNotaEmpenho_Valor, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV45TFNotaEmpenho_Valor_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV48TFNotaEmpenho_SaldoAnt) && (Convert.ToDecimal(0)==AV49TFNotaEmpenho_SaldoAnt_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_SALDOANT";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV48TFNotaEmpenho_SaldoAnt, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV49TFNotaEmpenho_SaldoAnt_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV52TFNotaEmpenho_SaldoPos) && (Convert.ToDecimal(0)==AV53TFNotaEmpenho_SaldoPos_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_SALDOPOS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV52TFNotaEmpenho_SaldoPos, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV53TFNotaEmpenho_SaldoPos_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV56TFNotaEmpenho_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV56TFNotaEmpenho_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Contrato_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV65Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV65Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "NotaEmpenho";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contrato_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_MH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_MH2( true) ;
         }
         else
         {
            wb_table2_8_MH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_13_MH2( true) ;
         }
         else
         {
            wb_table3_13_MH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_13_MH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MH2e( true) ;
         }
         else
         {
            wb_table1_2_MH2e( false) ;
         }
      }

      protected void wb_table3_13_MH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedgrid_Internalname, tblTablemergedgrid_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_16_MH2( true) ;
         }
         else
         {
            wb_table4_16_MH2( false) ;
         }
         return  ;
      }

      protected void wb_table4_16_MH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), imgInsert_Visible, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoContratoNotaEmpenhoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_13_MH2e( true) ;
         }
         else
         {
            wb_table3_13_MH2e( false) ;
         }
      }

      protected void wb_table4_16_MH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynSaldoContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( dynSaldoContrato_Codigo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynSaldoContrato_Codigo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_UnidadeMedicao_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_UnidadeMedicao_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_UnidadeMedicao_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Itentificador_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Itentificador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Itentificador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_DEmissao_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_DEmissao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_DEmissao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Qtd_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Qtd_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Qtd_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_SaldoAnt_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_SaldoAnt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_SaldoAnt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_SaldoPos_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_SaldoPos_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_SaldoPos_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkNotaEmpenho_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkNotaEmpenho_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkNotaEmpenho_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynSaldoContrato_Codigo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynSaldoContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1785SaldoContrato_UnidadeMedicao_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_UnidadeMedicao_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_UnidadeMedicao_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1564NotaEmpenho_Itentificador));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Itentificador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Itentificador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_DEmissao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_DEmissao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Qtd_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Qtd_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_SaldoAnt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_SaldoAnt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_SaldoPos_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_SaldoPos_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1570NotaEmpenho_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkNotaEmpenho_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkNotaEmpenho_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (short)(nGXsfl_19_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_MH2e( true) ;
         }
         else
         {
            wb_table4_16_MH2e( false) ;
         }
      }

      protected void wb_table2_8_MH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MH2e( true) ;
         }
         else
         {
            wb_table2_8_MH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMH2( ) ;
         WSMH2( ) ;
         WEMH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAMH2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratocontratonotaempenhowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAMH2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contrato_Codigo != wcpOAV7Contrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contrato_Codigo = AV7Contrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contrato_Codigo = cgiGet( sPrefix+"AV7Contrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contrato_Codigo) > 0 )
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contrato_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         else
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contrato_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAMH2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSMH2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSMH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEMH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812554555");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratocontratonotaempenhowc.js", "?202051812554556");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_19_idx;
         edtNotaEmpenho_Codigo_Internalname = sPrefix+"NOTAEMPENHO_CODIGO_"+sGXsfl_19_idx;
         dynSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO_"+sGXsfl_19_idx;
         edtSaldoContrato_UnidadeMedicao_Sigla_Internalname = sPrefix+"SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_"+sGXsfl_19_idx;
         edtNotaEmpenho_Itentificador_Internalname = sPrefix+"NOTAEMPENHO_ITENTIFICADOR_"+sGXsfl_19_idx;
         edtNotaEmpenho_DEmissao_Internalname = sPrefix+"NOTAEMPENHO_DEMISSAO_"+sGXsfl_19_idx;
         edtNotaEmpenho_Qtd_Internalname = sPrefix+"NOTAEMPENHO_QTD_"+sGXsfl_19_idx;
         edtNotaEmpenho_Valor_Internalname = sPrefix+"NOTAEMPENHO_VALOR_"+sGXsfl_19_idx;
         edtNotaEmpenho_SaldoAnt_Internalname = sPrefix+"NOTAEMPENHO_SALDOANT_"+sGXsfl_19_idx;
         edtNotaEmpenho_SaldoPos_Internalname = sPrefix+"NOTAEMPENHO_SALDOPOS_"+sGXsfl_19_idx;
         chkNotaEmpenho_Ativo_Internalname = sPrefix+"NOTAEMPENHO_ATIVO_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_Codigo_Internalname = sPrefix+"NOTAEMPENHO_CODIGO_"+sGXsfl_19_fel_idx;
         dynSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO_"+sGXsfl_19_fel_idx;
         edtSaldoContrato_UnidadeMedicao_Sigla_Internalname = sPrefix+"SALDOCONTRATO_UNIDADEMEDICAO_SIGLA_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_Itentificador_Internalname = sPrefix+"NOTAEMPENHO_ITENTIFICADOR_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_DEmissao_Internalname = sPrefix+"NOTAEMPENHO_DEMISSAO_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_Qtd_Internalname = sPrefix+"NOTAEMPENHO_QTD_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_Valor_Internalname = sPrefix+"NOTAEMPENHO_VALOR_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_SaldoAnt_Internalname = sPrefix+"NOTAEMPENHO_SALDOANT_"+sGXsfl_19_fel_idx;
         edtNotaEmpenho_SaldoPos_Internalname = sPrefix+"NOTAEMPENHO_SALDOPOS_"+sGXsfl_19_fel_idx;
         chkNotaEmpenho_Ativo_Internalname = sPrefix+"NOTAEMPENHO_ATIVO_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WBMH0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_19_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)) ? AV64Update_GXI : context.PathToRelativeUrl( AV16Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_19_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SALDOCONTRATO_CODIGO_" + sGXsfl_19_idx;
               dynSaldoContrato_Codigo.Name = GXCCtl;
               dynSaldoContrato_Codigo.WebTags = "";
               dynSaldoContrato_Codigo.removeAllItems();
               /* Using cursor H00MH5 */
               pr_default.execute(3);
               while ( (pr_default.getStatus(3) != 101) )
               {
                  dynSaldoContrato_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00MH5_A1561SaldoContrato_Codigo[0]), 6, 0)), StringUtil.Str( (decimal)(H00MH5_A1561SaldoContrato_Codigo[0]), 6, 0), 0);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               if ( dynSaldoContrato_Codigo.ItemCount > 0 )
               {
                  A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( dynSaldoContrato_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynSaldoContrato_Codigo,(String)dynSaldoContrato_Codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)),(short)1,(String)dynSaldoContrato_Codigo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynSaldoContrato_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynSaldoContrato_Codigo_Internalname, "Values", (String)(dynSaldoContrato_Codigo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_UnidadeMedicao_Sigla_Internalname,StringUtil.RTrim( A1785SaldoContrato_UnidadeMedicao_Sigla),StringUtil.RTrim( context.localUtil.Format( A1785SaldoContrato_UnidadeMedicao_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_UnidadeMedicao_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Itentificador_Internalname,StringUtil.RTrim( A1564NotaEmpenho_Itentificador),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Itentificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_DEmissao_Internalname,context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_DEmissao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Qtd_Internalname,StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ",", "")),context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Qtd_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ",", "")),context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_SaldoAnt_Internalname,StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ",", "")),context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_SaldoAnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_SaldoPos_Internalname,StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ",", "")),context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_SaldoPos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkNotaEmpenho_Ativo_Internalname,StringUtil.BoolToStr( A1570NotaEmpenho_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SALDOCONTRATO_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_ITENTIFICADOR"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_DEMISSAO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_QTD"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_VALOR"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_SALDOANT"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_SALDOPOS"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NOTAEMPENHO_ATIVO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, A1570NotaEmpenho_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         /* End function sendrow_192 */
      }

      protected void init_default_properties( )
      {
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtNotaEmpenho_Codigo_Internalname = sPrefix+"NOTAEMPENHO_CODIGO";
         dynSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO";
         edtSaldoContrato_UnidadeMedicao_Sigla_Internalname = sPrefix+"SALDOCONTRATO_UNIDADEMEDICAO_SIGLA";
         edtNotaEmpenho_Itentificador_Internalname = sPrefix+"NOTAEMPENHO_ITENTIFICADOR";
         edtNotaEmpenho_DEmissao_Internalname = sPrefix+"NOTAEMPENHO_DEMISSAO";
         edtNotaEmpenho_Qtd_Internalname = sPrefix+"NOTAEMPENHO_QTD";
         edtNotaEmpenho_Valor_Internalname = sPrefix+"NOTAEMPENHO_VALOR";
         edtNotaEmpenho_SaldoAnt_Internalname = sPrefix+"NOTAEMPENHO_SALDOANT";
         edtNotaEmpenho_SaldoPos_Internalname = sPrefix+"NOTAEMPENHO_SALDOPOS";
         chkNotaEmpenho_Ativo_Internalname = sPrefix+"NOTAEMPENHO_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablemergedgrid_Internalname = sPrefix+"TABLEMERGEDGRID";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfnotaempenho_codigo_Internalname = sPrefix+"vTFNOTAEMPENHO_CODIGO";
         edtavTfnotaempenho_codigo_to_Internalname = sPrefix+"vTFNOTAEMPENHO_CODIGO_TO";
         edtavTfsaldocontrato_codigo_Internalname = sPrefix+"vTFSALDOCONTRATO_CODIGO";
         edtavTfsaldocontrato_codigo_to_Internalname = sPrefix+"vTFSALDOCONTRATO_CODIGO_TO";
         edtavTfsaldocontrato_unidademedicao_sigla_Internalname = sPrefix+"vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA";
         edtavTfsaldocontrato_unidademedicao_sigla_sel_Internalname = sPrefix+"vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL";
         edtavTfnotaempenho_itentificador_Internalname = sPrefix+"vTFNOTAEMPENHO_ITENTIFICADOR";
         edtavTfnotaempenho_itentificador_sel_Internalname = sPrefix+"vTFNOTAEMPENHO_ITENTIFICADOR_SEL";
         edtavTfnotaempenho_demissao_Internalname = sPrefix+"vTFNOTAEMPENHO_DEMISSAO";
         edtavTfnotaempenho_demissao_to_Internalname = sPrefix+"vTFNOTAEMPENHO_DEMISSAO_TO";
         edtavDdo_notaempenho_demissaoauxdate_Internalname = sPrefix+"vDDO_NOTAEMPENHO_DEMISSAOAUXDATE";
         edtavDdo_notaempenho_demissaoauxdateto_Internalname = sPrefix+"vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO";
         divDdo_notaempenho_demissaoauxdates_Internalname = sPrefix+"DDO_NOTAEMPENHO_DEMISSAOAUXDATES";
         edtavTfnotaempenho_qtd_Internalname = sPrefix+"vTFNOTAEMPENHO_QTD";
         edtavTfnotaempenho_qtd_to_Internalname = sPrefix+"vTFNOTAEMPENHO_QTD_TO";
         edtavTfnotaempenho_valor_Internalname = sPrefix+"vTFNOTAEMPENHO_VALOR";
         edtavTfnotaempenho_valor_to_Internalname = sPrefix+"vTFNOTAEMPENHO_VALOR_TO";
         edtavTfnotaempenho_saldoant_Internalname = sPrefix+"vTFNOTAEMPENHO_SALDOANT";
         edtavTfnotaempenho_saldoant_to_Internalname = sPrefix+"vTFNOTAEMPENHO_SALDOANT_TO";
         edtavTfnotaempenho_saldopos_Internalname = sPrefix+"vTFNOTAEMPENHO_SALDOPOS";
         edtavTfnotaempenho_saldopos_to_Internalname = sPrefix+"vTFNOTAEMPENHO_SALDOPOS_TO";
         edtavTfnotaempenho_ativo_sel_Internalname = sPrefix+"vTFNOTAEMPENHO_ATIVO_SEL";
         Ddo_notaempenho_codigo_Internalname = sPrefix+"DDO_NOTAEMPENHO_CODIGO";
         edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_codigo_Internalname = sPrefix+"DDO_SALDOCONTRATO_CODIGO";
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_unidademedicao_sigla_Internalname = sPrefix+"DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA";
         edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_itentificador_Internalname = sPrefix+"DDO_NOTAEMPENHO_ITENTIFICADOR";
         edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_demissao_Internalname = sPrefix+"DDO_NOTAEMPENHO_DEMISSAO";
         edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_qtd_Internalname = sPrefix+"DDO_NOTAEMPENHO_QTD";
         edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_valor_Internalname = sPrefix+"DDO_NOTAEMPENHO_VALOR";
         edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_saldoant_Internalname = sPrefix+"DDO_NOTAEMPENHO_SALDOANT";
         edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_saldopos_Internalname = sPrefix+"DDO_NOTAEMPENHO_SALDOPOS";
         edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_ativo_Internalname = sPrefix+"DDO_NOTAEMPENHO_ATIVO";
         edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtNotaEmpenho_SaldoPos_Jsonclick = "";
         edtNotaEmpenho_SaldoAnt_Jsonclick = "";
         edtNotaEmpenho_Valor_Jsonclick = "";
         edtNotaEmpenho_Qtd_Jsonclick = "";
         edtNotaEmpenho_DEmissao_Jsonclick = "";
         edtNotaEmpenho_Itentificador_Jsonclick = "";
         edtSaldoContrato_UnidadeMedicao_Sigla_Jsonclick = "";
         dynSaldoContrato_Codigo_Jsonclick = "";
         edtNotaEmpenho_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkNotaEmpenho_Ativo_Titleformat = 0;
         edtNotaEmpenho_SaldoPos_Titleformat = 0;
         edtNotaEmpenho_SaldoAnt_Titleformat = 0;
         edtNotaEmpenho_Valor_Titleformat = 0;
         edtNotaEmpenho_Qtd_Titleformat = 0;
         edtNotaEmpenho_DEmissao_Titleformat = 0;
         edtNotaEmpenho_Itentificador_Titleformat = 0;
         edtSaldoContrato_UnidadeMedicao_Sigla_Titleformat = 0;
         dynSaldoContrato_Codigo_Titleformat = 0;
         edtNotaEmpenho_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Enabled = 1;
         imgInsert_Visible = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavUpdate_Visible = -1;
         chkNotaEmpenho_Ativo.Title.Text = "Ativo";
         edtNotaEmpenho_SaldoPos_Title = "Saldo Posterior";
         edtNotaEmpenho_SaldoAnt_Title = "Saldo Anterior";
         edtNotaEmpenho_Valor_Title = "Valor";
         edtNotaEmpenho_Qtd_Title = "Quantidade";
         edtNotaEmpenho_DEmissao_Title = "Data Emiss�o";
         edtNotaEmpenho_Itentificador_Title = "Identificador";
         edtSaldoContrato_UnidadeMedicao_Sigla_Title = "Unidade de Medi��o";
         dynSaldoContrato_Codigo.Title.Text = "Saldo Contrato";
         edtNotaEmpenho_Codigo_Title = "C�digo";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkNotaEmpenho_Ativo.Caption = "";
         edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfnotaempenho_ativo_sel_Jsonclick = "";
         edtavTfnotaempenho_ativo_sel_Visible = 1;
         edtavTfnotaempenho_saldopos_to_Jsonclick = "";
         edtavTfnotaempenho_saldopos_to_Visible = 1;
         edtavTfnotaempenho_saldopos_Jsonclick = "";
         edtavTfnotaempenho_saldopos_Visible = 1;
         edtavTfnotaempenho_saldoant_to_Jsonclick = "";
         edtavTfnotaempenho_saldoant_to_Visible = 1;
         edtavTfnotaempenho_saldoant_Jsonclick = "";
         edtavTfnotaempenho_saldoant_Visible = 1;
         edtavTfnotaempenho_valor_to_Jsonclick = "";
         edtavTfnotaempenho_valor_to_Visible = 1;
         edtavTfnotaempenho_valor_Jsonclick = "";
         edtavTfnotaempenho_valor_Visible = 1;
         edtavTfnotaempenho_qtd_to_Jsonclick = "";
         edtavTfnotaempenho_qtd_to_Visible = 1;
         edtavTfnotaempenho_qtd_Jsonclick = "";
         edtavTfnotaempenho_qtd_Visible = 1;
         edtavDdo_notaempenho_demissaoauxdateto_Jsonclick = "";
         edtavDdo_notaempenho_demissaoauxdate_Jsonclick = "";
         edtavTfnotaempenho_demissao_to_Jsonclick = "";
         edtavTfnotaempenho_demissao_to_Visible = 1;
         edtavTfnotaempenho_demissao_Jsonclick = "";
         edtavTfnotaempenho_demissao_Visible = 1;
         edtavTfnotaempenho_itentificador_sel_Jsonclick = "";
         edtavTfnotaempenho_itentificador_sel_Visible = 1;
         edtavTfnotaempenho_itentificador_Jsonclick = "";
         edtavTfnotaempenho_itentificador_Visible = 1;
         edtavTfsaldocontrato_unidademedicao_sigla_sel_Jsonclick = "";
         edtavTfsaldocontrato_unidademedicao_sigla_sel_Visible = 1;
         edtavTfsaldocontrato_unidademedicao_sigla_Jsonclick = "";
         edtavTfsaldocontrato_unidademedicao_sigla_Visible = 1;
         edtavTfsaldocontrato_codigo_to_Jsonclick = "";
         edtavTfsaldocontrato_codigo_to_Visible = 1;
         edtavTfsaldocontrato_codigo_Jsonclick = "";
         edtavTfsaldocontrato_codigo_Visible = 1;
         edtavTfnotaempenho_codigo_to_Jsonclick = "";
         edtavTfnotaempenho_codigo_to_Visible = 1;
         edtavTfnotaempenho_codigo_Jsonclick = "";
         edtavTfnotaempenho_codigo_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Visible = 1;
         Ddo_notaempenho_ativo_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_notaempenho_ativo_Datalisttype = "FixedValues";
         Ddo_notaempenho_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_notaempenho_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_notaempenho_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_ativo_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_ativo_Cls = "ColumnSettings";
         Ddo_notaempenho_ativo_Tooltip = "Op��es";
         Ddo_notaempenho_ativo_Caption = "";
         Ddo_notaempenho_saldopos_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_saldopos_Rangefilterto = "At�";
         Ddo_notaempenho_saldopos_Rangefilterfrom = "Desde";
         Ddo_notaempenho_saldopos_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_saldopos_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_saldopos_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_saldopos_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_saldopos_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Filtertype = "Numeric";
         Ddo_notaempenho_saldopos_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_saldopos_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_saldopos_Cls = "ColumnSettings";
         Ddo_notaempenho_saldopos_Tooltip = "Op��es";
         Ddo_notaempenho_saldopos_Caption = "";
         Ddo_notaempenho_saldoant_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_saldoant_Rangefilterto = "At�";
         Ddo_notaempenho_saldoant_Rangefilterfrom = "Desde";
         Ddo_notaempenho_saldoant_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_saldoant_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_saldoant_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_saldoant_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_saldoant_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Filtertype = "Numeric";
         Ddo_notaempenho_saldoant_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_saldoant_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_saldoant_Cls = "ColumnSettings";
         Ddo_notaempenho_saldoant_Tooltip = "Op��es";
         Ddo_notaempenho_saldoant_Caption = "";
         Ddo_notaempenho_valor_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_valor_Rangefilterto = "At�";
         Ddo_notaempenho_valor_Rangefilterfrom = "Desde";
         Ddo_notaempenho_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_valor_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Filtertype = "Numeric";
         Ddo_notaempenho_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_valor_Cls = "ColumnSettings";
         Ddo_notaempenho_valor_Tooltip = "Op��es";
         Ddo_notaempenho_valor_Caption = "";
         Ddo_notaempenho_qtd_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_qtd_Rangefilterto = "At�";
         Ddo_notaempenho_qtd_Rangefilterfrom = "Desde";
         Ddo_notaempenho_qtd_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_qtd_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_qtd_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_qtd_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_qtd_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Filtertype = "Numeric";
         Ddo_notaempenho_qtd_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_qtd_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_qtd_Cls = "ColumnSettings";
         Ddo_notaempenho_qtd_Tooltip = "Op��es";
         Ddo_notaempenho_qtd_Caption = "";
         Ddo_notaempenho_demissao_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_demissao_Rangefilterto = "At�";
         Ddo_notaempenho_demissao_Rangefilterfrom = "Desde";
         Ddo_notaempenho_demissao_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_demissao_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_demissao_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_demissao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_demissao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Filtertype = "Date";
         Ddo_notaempenho_demissao_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_demissao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_demissao_Cls = "ColumnSettings";
         Ddo_notaempenho_demissao_Tooltip = "Op��es";
         Ddo_notaempenho_demissao_Caption = "";
         Ddo_notaempenho_itentificador_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_itentificador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_notaempenho_itentificador_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_itentificador_Loadingdata = "Carregando dados...";
         Ddo_notaempenho_itentificador_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_itentificador_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters = 0;
         Ddo_notaempenho_itentificador_Datalistproc = "GetContratoContratoNotaEmpenhoWCFilterData";
         Ddo_notaempenho_itentificador_Datalisttype = "Dynamic";
         Ddo_notaempenho_itentificador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_notaempenho_itentificador_Filtertype = "Character";
         Ddo_notaempenho_itentificador_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_itentificador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_itentificador_Cls = "ColumnSettings";
         Ddo_notaempenho_itentificador_Tooltip = "Op��es";
         Ddo_notaempenho_itentificador_Caption = "";
         Ddo_saldocontrato_unidademedicao_sigla_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_unidademedicao_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_saldocontrato_unidademedicao_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_unidademedicao_sigla_Loadingdata = "Carregando dados...";
         Ddo_saldocontrato_unidademedicao_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_unidademedicao_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_unidademedicao_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_saldocontrato_unidademedicao_sigla_Datalistproc = "GetContratoContratoNotaEmpenhoWCFilterData";
         Ddo_saldocontrato_unidademedicao_sigla_Datalisttype = "Dynamic";
         Ddo_saldocontrato_unidademedicao_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_saldocontrato_unidademedicao_sigla_Filtertype = "Character";
         Ddo_saldocontrato_unidademedicao_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_unidademedicao_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_unidademedicao_sigla_Cls = "ColumnSettings";
         Ddo_saldocontrato_unidademedicao_sigla_Tooltip = "Op��es";
         Ddo_saldocontrato_unidademedicao_sigla_Caption = "";
         Ddo_saldocontrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_codigo_Rangefilterto = "At�";
         Ddo_saldocontrato_codigo_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Filtertype = "Numeric";
         Ddo_saldocontrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_codigo_Cls = "ColumnSettings";
         Ddo_saldocontrato_codigo_Tooltip = "Op��es";
         Ddo_saldocontrato_codigo_Caption = "";
         Ddo_notaempenho_codigo_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_codigo_Rangefilterto = "At�";
         Ddo_notaempenho_codigo_Rangefilterfrom = "Desde";
         Ddo_notaempenho_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Filtertype = "Numeric";
         Ddo_notaempenho_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_codigo_Cls = "ColumnSettings";
         Ddo_notaempenho_codigo_Tooltip = "Op��es";
         Ddo_notaempenho_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17NotaEmpenho_CodigoTitleFilterData',fld:'vNOTAEMPENHO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV21SaldoContrato_CodigoTitleFilterData',fld:'vSALDOCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData',fld:'vSALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV29NotaEmpenho_ItentificadorTitleFilterData',fld:'vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV33NotaEmpenho_DEmissaoTitleFilterData',fld:'vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV39NotaEmpenho_QtdTitleFilterData',fld:'vNOTAEMPENHO_QTDTITLEFILTERDATA',pic:'',nv:null},{av:'AV43NotaEmpenho_ValorTitleFilterData',fld:'vNOTAEMPENHO_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV47NotaEmpenho_SaldoAntTitleFilterData',fld:'vNOTAEMPENHO_SALDOANTTITLEFILTERDATA',pic:'',nv:null},{av:'AV51NotaEmpenho_SaldoPosTitleFilterData',fld:'vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA',pic:'',nv:null},{av:'AV55NotaEmpenho_AtivoTitleFilterData',fld:'vNOTAEMPENHO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtNotaEmpenho_Codigo_Titleformat',ctrl:'NOTAEMPENHO_CODIGO',prop:'Titleformat'},{av:'edtNotaEmpenho_Codigo_Title',ctrl:'NOTAEMPENHO_CODIGO',prop:'Title'},{av:'dynSaldoContrato_Codigo'},{av:'edtSaldoContrato_UnidadeMedicao_Sigla_Titleformat',ctrl:'SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'Titleformat'},{av:'edtSaldoContrato_UnidadeMedicao_Sigla_Title',ctrl:'SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'Title'},{av:'edtNotaEmpenho_Itentificador_Titleformat',ctrl:'NOTAEMPENHO_ITENTIFICADOR',prop:'Titleformat'},{av:'edtNotaEmpenho_Itentificador_Title',ctrl:'NOTAEMPENHO_ITENTIFICADOR',prop:'Title'},{av:'edtNotaEmpenho_DEmissao_Titleformat',ctrl:'NOTAEMPENHO_DEMISSAO',prop:'Titleformat'},{av:'edtNotaEmpenho_DEmissao_Title',ctrl:'NOTAEMPENHO_DEMISSAO',prop:'Title'},{av:'edtNotaEmpenho_Qtd_Titleformat',ctrl:'NOTAEMPENHO_QTD',prop:'Titleformat'},{av:'edtNotaEmpenho_Qtd_Title',ctrl:'NOTAEMPENHO_QTD',prop:'Title'},{av:'edtNotaEmpenho_Valor_Titleformat',ctrl:'NOTAEMPENHO_VALOR',prop:'Titleformat'},{av:'edtNotaEmpenho_Valor_Title',ctrl:'NOTAEMPENHO_VALOR',prop:'Title'},{av:'edtNotaEmpenho_SaldoAnt_Titleformat',ctrl:'NOTAEMPENHO_SALDOANT',prop:'Titleformat'},{av:'edtNotaEmpenho_SaldoAnt_Title',ctrl:'NOTAEMPENHO_SALDOANT',prop:'Title'},{av:'edtNotaEmpenho_SaldoPos_Titleformat',ctrl:'NOTAEMPENHO_SALDOPOS',prop:'Titleformat'},{av:'edtNotaEmpenho_SaldoPos_Title',ctrl:'NOTAEMPENHO_SALDOPOS',prop:'Title'},{av:'chkNotaEmpenho_Ativo_Titleformat',ctrl:'NOTAEMPENHO_ATIVO',prop:'Titleformat'},{av:'chkNotaEmpenho_Ativo.Title.Text',ctrl:'NOTAEMPENHO_ATIVO',prop:'Title'},{av:'AV60GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV61GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED","{handler:'E12MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_codigo_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_codigo_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_notaempenho_codigo_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E13MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_codigo_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_codigo_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_codigo_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA.ONOPTIONCLICKED","{handler:'E14MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_get',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED","{handler:'E15MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_itentificador_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_itentificador_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'FilteredText_get'},{av:'Ddo_notaempenho_itentificador_Selectedvalue_get',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED","{handler:'E16MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_demissao_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_demissao_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredText_get'},{av:'Ddo_notaempenho_demissao_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED","{handler:'E17MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_qtd_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_qtd_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredText_get'},{av:'Ddo_notaempenho_qtd_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED","{handler:'E18MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_valor_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_valor_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredText_get'},{av:'Ddo_notaempenho_valor_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED","{handler:'E19MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_saldoant_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_saldoant_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredText_get'},{av:'Ddo_notaempenho_saldoant_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED","{handler:'E20MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_saldopos_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_saldopos_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredText_get'},{av:'Ddo_notaempenho_saldopos_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED","{handler:'E21MH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV22TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV26TFSaldoContrato_UnidadeMedicao_Sigla',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA',pic:'@!',nv:''},{av:'AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel',fld:'vTFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV30TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV31TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV34TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV35TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV45TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV49TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV65Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_notaempenho_ativo_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_ativo_Selectedvalue_get',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'},{av:'AV56TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25MH2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E22MH2',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_notaempenho_codigo_Activeeventkey = "";
         Ddo_notaempenho_codigo_Filteredtext_get = "";
         Ddo_notaempenho_codigo_Filteredtextto_get = "";
         Ddo_saldocontrato_codigo_Activeeventkey = "";
         Ddo_saldocontrato_codigo_Filteredtext_get = "";
         Ddo_saldocontrato_codigo_Filteredtextto_get = "";
         Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey = "";
         Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_get = "";
         Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_get = "";
         Ddo_notaempenho_itentificador_Activeeventkey = "";
         Ddo_notaempenho_itentificador_Filteredtext_get = "";
         Ddo_notaempenho_itentificador_Selectedvalue_get = "";
         Ddo_notaempenho_demissao_Activeeventkey = "";
         Ddo_notaempenho_demissao_Filteredtext_get = "";
         Ddo_notaempenho_demissao_Filteredtextto_get = "";
         Ddo_notaempenho_qtd_Activeeventkey = "";
         Ddo_notaempenho_qtd_Filteredtext_get = "";
         Ddo_notaempenho_qtd_Filteredtextto_get = "";
         Ddo_notaempenho_valor_Activeeventkey = "";
         Ddo_notaempenho_valor_Filteredtext_get = "";
         Ddo_notaempenho_valor_Filteredtextto_get = "";
         Ddo_notaempenho_saldoant_Activeeventkey = "";
         Ddo_notaempenho_saldoant_Filteredtext_get = "";
         Ddo_notaempenho_saldoant_Filteredtextto_get = "";
         Ddo_notaempenho_saldopos_Activeeventkey = "";
         Ddo_notaempenho_saldopos_Filteredtext_get = "";
         Ddo_notaempenho_saldopos_Filteredtextto_get = "";
         Ddo_notaempenho_ativo_Activeeventkey = "";
         Ddo_notaempenho_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV26TFSaldoContrato_UnidadeMedicao_Sigla = "";
         AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel = "";
         AV30TFNotaEmpenho_Itentificador = "";
         AV31TFNotaEmpenho_Itentificador_Sel = "";
         AV34TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         AV35TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
         AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace = "";
         AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace = "";
         AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace = "";
         AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = "";
         AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = "";
         AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace = "";
         AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace = "";
         AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = "";
         AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = "";
         AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV65Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV58DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV17NotaEmpenho_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV21SaldoContrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV29NotaEmpenho_ItentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33NotaEmpenho_DEmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39NotaEmpenho_QtdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43NotaEmpenho_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47NotaEmpenho_SaldoAntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51NotaEmpenho_SaldoPosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55NotaEmpenho_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_notaempenho_codigo_Filteredtext_set = "";
         Ddo_notaempenho_codigo_Filteredtextto_set = "";
         Ddo_notaempenho_codigo_Sortedstatus = "";
         Ddo_saldocontrato_codigo_Filteredtext_set = "";
         Ddo_saldocontrato_codigo_Filteredtextto_set = "";
         Ddo_saldocontrato_codigo_Sortedstatus = "";
         Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_set = "";
         Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_set = "";
         Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus = "";
         Ddo_notaempenho_itentificador_Filteredtext_set = "";
         Ddo_notaempenho_itentificador_Selectedvalue_set = "";
         Ddo_notaempenho_itentificador_Sortedstatus = "";
         Ddo_notaempenho_demissao_Filteredtext_set = "";
         Ddo_notaempenho_demissao_Filteredtextto_set = "";
         Ddo_notaempenho_demissao_Sortedstatus = "";
         Ddo_notaempenho_qtd_Filteredtext_set = "";
         Ddo_notaempenho_qtd_Filteredtextto_set = "";
         Ddo_notaempenho_qtd_Sortedstatus = "";
         Ddo_notaempenho_valor_Filteredtext_set = "";
         Ddo_notaempenho_valor_Filteredtextto_set = "";
         Ddo_notaempenho_valor_Sortedstatus = "";
         Ddo_notaempenho_saldoant_Filteredtext_set = "";
         Ddo_notaempenho_saldoant_Filteredtextto_set = "";
         Ddo_notaempenho_saldoant_Sortedstatus = "";
         Ddo_notaempenho_saldopos_Filteredtext_set = "";
         Ddo_notaempenho_saldopos_Filteredtextto_set = "";
         Ddo_notaempenho_saldopos_Sortedstatus = "";
         Ddo_notaempenho_ativo_Selectedvalue_set = "";
         Ddo_notaempenho_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV36DDO_NotaEmpenho_DEmissaoAuxDate = DateTime.MinValue;
         AV37DDO_NotaEmpenho_DEmissaoAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Update = "";
         AV64Update_GXI = "";
         A1785SaldoContrato_UnidadeMedicao_Sigla = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         GXCCtl = "";
         scmdbuf = "";
         H00MH2_A1561SaldoContrato_Codigo = new int[1] ;
         GridContainer = new GXWebGrid( context);
         lV26TFSaldoContrato_UnidadeMedicao_Sigla = "";
         lV30TFNotaEmpenho_Itentificador = "";
         H00MH3_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         H00MH3_A74Contrato_Codigo = new int[1] ;
         H00MH3_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         H00MH3_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         H00MH3_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         H00MH3_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         H00MH3_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         H00MH3_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         H00MH3_A1566NotaEmpenho_Valor = new decimal[1] ;
         H00MH3_n1566NotaEmpenho_Valor = new bool[] {false} ;
         H00MH3_A1567NotaEmpenho_Qtd = new decimal[1] ;
         H00MH3_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         H00MH3_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         H00MH3_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         H00MH3_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         H00MH3_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         H00MH3_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         H00MH3_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         H00MH3_A1561SaldoContrato_Codigo = new int[1] ;
         H00MH3_A1560NotaEmpenho_Codigo = new int[1] ;
         H00MH4_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contrato_Codigo = "";
         ROClassString = "";
         H00MH5_A1561SaldoContrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratocontratonotaempenhowc__default(),
            new Object[][] {
                new Object[] {
               H00MH2_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               H00MH3_A1783SaldoContrato_UnidadeMedicao_Codigo, H00MH3_A74Contrato_Codigo, H00MH3_A1570NotaEmpenho_Ativo, H00MH3_n1570NotaEmpenho_Ativo, H00MH3_A1569NotaEmpenho_SaldoPos, H00MH3_n1569NotaEmpenho_SaldoPos, H00MH3_A1568NotaEmpenho_SaldoAnt, H00MH3_n1568NotaEmpenho_SaldoAnt, H00MH3_A1566NotaEmpenho_Valor, H00MH3_n1566NotaEmpenho_Valor,
               H00MH3_A1567NotaEmpenho_Qtd, H00MH3_n1567NotaEmpenho_Qtd, H00MH3_A1565NotaEmpenho_DEmissao, H00MH3_n1565NotaEmpenho_DEmissao, H00MH3_A1564NotaEmpenho_Itentificador, H00MH3_n1564NotaEmpenho_Itentificador, H00MH3_A1785SaldoContrato_UnidadeMedicao_Sigla, H00MH3_n1785SaldoContrato_UnidadeMedicao_Sigla, H00MH3_A1561SaldoContrato_Codigo, H00MH3_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               H00MH4_AGRID_nRecordCount
               }
               , new Object[] {
               H00MH5_A1561SaldoContrato_Codigo
               }
            }
         );
         AV65Pgmname = "ContratoContratoNotaEmpenhoWC";
         /* GeneXus formulas. */
         AV65Pgmname = "ContratoContratoNotaEmpenhoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_19 ;
      private short nGXsfl_19_idx=1 ;
      private short AV13OrderedBy ;
      private short AV56TFNotaEmpenho_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_19_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtNotaEmpenho_Codigo_Titleformat ;
      private short dynSaldoContrato_Codigo_Titleformat ;
      private short edtSaldoContrato_UnidadeMedicao_Sigla_Titleformat ;
      private short edtNotaEmpenho_Itentificador_Titleformat ;
      private short edtNotaEmpenho_DEmissao_Titleformat ;
      private short edtNotaEmpenho_Qtd_Titleformat ;
      private short edtNotaEmpenho_Valor_Titleformat ;
      private short edtNotaEmpenho_SaldoAnt_Titleformat ;
      private short edtNotaEmpenho_SaldoPos_Titleformat ;
      private short chkNotaEmpenho_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contrato_Codigo ;
      private int wcpOAV7Contrato_Codigo ;
      private int subGrid_Rows ;
      private int AV18TFNotaEmpenho_Codigo ;
      private int AV19TFNotaEmpenho_Codigo_To ;
      private int AV22TFSaldoContrato_Codigo ;
      private int AV23TFSaldoContrato_Codigo_To ;
      private int A1560NotaEmpenho_Codigo ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_saldocontrato_unidademedicao_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters ;
      private int edtContrato_Codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfnotaempenho_codigo_Visible ;
      private int edtavTfnotaempenho_codigo_to_Visible ;
      private int edtavTfsaldocontrato_codigo_Visible ;
      private int edtavTfsaldocontrato_codigo_to_Visible ;
      private int edtavTfsaldocontrato_unidademedicao_sigla_Visible ;
      private int edtavTfsaldocontrato_unidademedicao_sigla_sel_Visible ;
      private int edtavTfnotaempenho_itentificador_Visible ;
      private int edtavTfnotaempenho_itentificador_sel_Visible ;
      private int edtavTfnotaempenho_demissao_Visible ;
      private int edtavTfnotaempenho_demissao_to_Visible ;
      private int edtavTfnotaempenho_qtd_Visible ;
      private int edtavTfnotaempenho_qtd_to_Visible ;
      private int edtavTfnotaempenho_valor_Visible ;
      private int edtavTfnotaempenho_valor_to_Visible ;
      private int edtavTfnotaempenho_saldoant_Visible ;
      private int edtavTfnotaempenho_saldoant_to_Visible ;
      private int edtavTfnotaempenho_saldopos_Visible ;
      private int edtavTfnotaempenho_saldopos_to_Visible ;
      private int edtavTfnotaempenho_ativo_sel_Visible ;
      private int edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible ;
      private int A1561SaldoContrato_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int AV59PageToGo ;
      private int edtavUpdate_Enabled ;
      private int imgInsert_Enabled ;
      private int AV66GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV60GridCurrentPage ;
      private long AV61GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV40TFNotaEmpenho_Qtd ;
      private decimal AV41TFNotaEmpenho_Qtd_To ;
      private decimal AV44TFNotaEmpenho_Valor ;
      private decimal AV45TFNotaEmpenho_Valor_To ;
      private decimal AV48TFNotaEmpenho_SaldoAnt ;
      private decimal AV49TFNotaEmpenho_SaldoAnt_To ;
      private decimal AV52TFNotaEmpenho_SaldoPos ;
      private decimal AV53TFNotaEmpenho_SaldoPos_To ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_notaempenho_codigo_Activeeventkey ;
      private String Ddo_notaempenho_codigo_Filteredtext_get ;
      private String Ddo_notaempenho_codigo_Filteredtextto_get ;
      private String Ddo_saldocontrato_codigo_Activeeventkey ;
      private String Ddo_saldocontrato_codigo_Filteredtext_get ;
      private String Ddo_saldocontrato_codigo_Filteredtextto_get ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Activeeventkey ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_get ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_get ;
      private String Ddo_notaempenho_itentificador_Activeeventkey ;
      private String Ddo_notaempenho_itentificador_Filteredtext_get ;
      private String Ddo_notaempenho_itentificador_Selectedvalue_get ;
      private String Ddo_notaempenho_demissao_Activeeventkey ;
      private String Ddo_notaempenho_demissao_Filteredtext_get ;
      private String Ddo_notaempenho_demissao_Filteredtextto_get ;
      private String Ddo_notaempenho_qtd_Activeeventkey ;
      private String Ddo_notaempenho_qtd_Filteredtext_get ;
      private String Ddo_notaempenho_qtd_Filteredtextto_get ;
      private String Ddo_notaempenho_valor_Activeeventkey ;
      private String Ddo_notaempenho_valor_Filteredtext_get ;
      private String Ddo_notaempenho_valor_Filteredtextto_get ;
      private String Ddo_notaempenho_saldoant_Activeeventkey ;
      private String Ddo_notaempenho_saldoant_Filteredtext_get ;
      private String Ddo_notaempenho_saldoant_Filteredtextto_get ;
      private String Ddo_notaempenho_saldopos_Activeeventkey ;
      private String Ddo_notaempenho_saldopos_Filteredtext_get ;
      private String Ddo_notaempenho_saldopos_Filteredtextto_get ;
      private String Ddo_notaempenho_ativo_Activeeventkey ;
      private String Ddo_notaempenho_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_19_idx="0001" ;
      private String AV26TFSaldoContrato_UnidadeMedicao_Sigla ;
      private String AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel ;
      private String AV30TFNotaEmpenho_Itentificador ;
      private String AV31TFNotaEmpenho_Itentificador_Sel ;
      private String AV65Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_notaempenho_codigo_Caption ;
      private String Ddo_notaempenho_codigo_Tooltip ;
      private String Ddo_notaempenho_codigo_Cls ;
      private String Ddo_notaempenho_codigo_Filteredtext_set ;
      private String Ddo_notaempenho_codigo_Filteredtextto_set ;
      private String Ddo_notaempenho_codigo_Dropdownoptionstype ;
      private String Ddo_notaempenho_codigo_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_codigo_Sortedstatus ;
      private String Ddo_notaempenho_codigo_Filtertype ;
      private String Ddo_notaempenho_codigo_Sortasc ;
      private String Ddo_notaempenho_codigo_Sortdsc ;
      private String Ddo_notaempenho_codigo_Cleanfilter ;
      private String Ddo_notaempenho_codigo_Rangefilterfrom ;
      private String Ddo_notaempenho_codigo_Rangefilterto ;
      private String Ddo_notaempenho_codigo_Searchbuttontext ;
      private String Ddo_saldocontrato_codigo_Caption ;
      private String Ddo_saldocontrato_codigo_Tooltip ;
      private String Ddo_saldocontrato_codigo_Cls ;
      private String Ddo_saldocontrato_codigo_Filteredtext_set ;
      private String Ddo_saldocontrato_codigo_Filteredtextto_set ;
      private String Ddo_saldocontrato_codigo_Dropdownoptionstype ;
      private String Ddo_saldocontrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_codigo_Sortedstatus ;
      private String Ddo_saldocontrato_codigo_Filtertype ;
      private String Ddo_saldocontrato_codigo_Sortasc ;
      private String Ddo_saldocontrato_codigo_Sortdsc ;
      private String Ddo_saldocontrato_codigo_Cleanfilter ;
      private String Ddo_saldocontrato_codigo_Rangefilterfrom ;
      private String Ddo_saldocontrato_codigo_Rangefilterto ;
      private String Ddo_saldocontrato_codigo_Searchbuttontext ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Caption ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Tooltip ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Cls ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Filteredtext_set ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Selectedvalue_set ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Dropdownoptionstype ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Sortedstatus ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Filtertype ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Datalisttype ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Datalistproc ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Sortasc ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Sortdsc ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Loadingdata ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Cleanfilter ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Noresultsfound ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Searchbuttontext ;
      private String Ddo_notaempenho_itentificador_Caption ;
      private String Ddo_notaempenho_itentificador_Tooltip ;
      private String Ddo_notaempenho_itentificador_Cls ;
      private String Ddo_notaempenho_itentificador_Filteredtext_set ;
      private String Ddo_notaempenho_itentificador_Selectedvalue_set ;
      private String Ddo_notaempenho_itentificador_Dropdownoptionstype ;
      private String Ddo_notaempenho_itentificador_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_itentificador_Sortedstatus ;
      private String Ddo_notaempenho_itentificador_Filtertype ;
      private String Ddo_notaempenho_itentificador_Datalisttype ;
      private String Ddo_notaempenho_itentificador_Datalistproc ;
      private String Ddo_notaempenho_itentificador_Sortasc ;
      private String Ddo_notaempenho_itentificador_Sortdsc ;
      private String Ddo_notaempenho_itentificador_Loadingdata ;
      private String Ddo_notaempenho_itentificador_Cleanfilter ;
      private String Ddo_notaempenho_itentificador_Noresultsfound ;
      private String Ddo_notaempenho_itentificador_Searchbuttontext ;
      private String Ddo_notaempenho_demissao_Caption ;
      private String Ddo_notaempenho_demissao_Tooltip ;
      private String Ddo_notaempenho_demissao_Cls ;
      private String Ddo_notaempenho_demissao_Filteredtext_set ;
      private String Ddo_notaempenho_demissao_Filteredtextto_set ;
      private String Ddo_notaempenho_demissao_Dropdownoptionstype ;
      private String Ddo_notaempenho_demissao_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_demissao_Sortedstatus ;
      private String Ddo_notaempenho_demissao_Filtertype ;
      private String Ddo_notaempenho_demissao_Sortasc ;
      private String Ddo_notaempenho_demissao_Sortdsc ;
      private String Ddo_notaempenho_demissao_Cleanfilter ;
      private String Ddo_notaempenho_demissao_Rangefilterfrom ;
      private String Ddo_notaempenho_demissao_Rangefilterto ;
      private String Ddo_notaempenho_demissao_Searchbuttontext ;
      private String Ddo_notaempenho_qtd_Caption ;
      private String Ddo_notaempenho_qtd_Tooltip ;
      private String Ddo_notaempenho_qtd_Cls ;
      private String Ddo_notaempenho_qtd_Filteredtext_set ;
      private String Ddo_notaempenho_qtd_Filteredtextto_set ;
      private String Ddo_notaempenho_qtd_Dropdownoptionstype ;
      private String Ddo_notaempenho_qtd_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_qtd_Sortedstatus ;
      private String Ddo_notaempenho_qtd_Filtertype ;
      private String Ddo_notaempenho_qtd_Sortasc ;
      private String Ddo_notaempenho_qtd_Sortdsc ;
      private String Ddo_notaempenho_qtd_Cleanfilter ;
      private String Ddo_notaempenho_qtd_Rangefilterfrom ;
      private String Ddo_notaempenho_qtd_Rangefilterto ;
      private String Ddo_notaempenho_qtd_Searchbuttontext ;
      private String Ddo_notaempenho_valor_Caption ;
      private String Ddo_notaempenho_valor_Tooltip ;
      private String Ddo_notaempenho_valor_Cls ;
      private String Ddo_notaempenho_valor_Filteredtext_set ;
      private String Ddo_notaempenho_valor_Filteredtextto_set ;
      private String Ddo_notaempenho_valor_Dropdownoptionstype ;
      private String Ddo_notaempenho_valor_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_valor_Sortedstatus ;
      private String Ddo_notaempenho_valor_Filtertype ;
      private String Ddo_notaempenho_valor_Sortasc ;
      private String Ddo_notaempenho_valor_Sortdsc ;
      private String Ddo_notaempenho_valor_Cleanfilter ;
      private String Ddo_notaempenho_valor_Rangefilterfrom ;
      private String Ddo_notaempenho_valor_Rangefilterto ;
      private String Ddo_notaempenho_valor_Searchbuttontext ;
      private String Ddo_notaempenho_saldoant_Caption ;
      private String Ddo_notaempenho_saldoant_Tooltip ;
      private String Ddo_notaempenho_saldoant_Cls ;
      private String Ddo_notaempenho_saldoant_Filteredtext_set ;
      private String Ddo_notaempenho_saldoant_Filteredtextto_set ;
      private String Ddo_notaempenho_saldoant_Dropdownoptionstype ;
      private String Ddo_notaempenho_saldoant_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_saldoant_Sortedstatus ;
      private String Ddo_notaempenho_saldoant_Filtertype ;
      private String Ddo_notaempenho_saldoant_Sortasc ;
      private String Ddo_notaempenho_saldoant_Sortdsc ;
      private String Ddo_notaempenho_saldoant_Cleanfilter ;
      private String Ddo_notaempenho_saldoant_Rangefilterfrom ;
      private String Ddo_notaempenho_saldoant_Rangefilterto ;
      private String Ddo_notaempenho_saldoant_Searchbuttontext ;
      private String Ddo_notaempenho_saldopos_Caption ;
      private String Ddo_notaempenho_saldopos_Tooltip ;
      private String Ddo_notaempenho_saldopos_Cls ;
      private String Ddo_notaempenho_saldopos_Filteredtext_set ;
      private String Ddo_notaempenho_saldopos_Filteredtextto_set ;
      private String Ddo_notaempenho_saldopos_Dropdownoptionstype ;
      private String Ddo_notaempenho_saldopos_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_saldopos_Sortedstatus ;
      private String Ddo_notaempenho_saldopos_Filtertype ;
      private String Ddo_notaempenho_saldopos_Sortasc ;
      private String Ddo_notaempenho_saldopos_Sortdsc ;
      private String Ddo_notaempenho_saldopos_Cleanfilter ;
      private String Ddo_notaempenho_saldopos_Rangefilterfrom ;
      private String Ddo_notaempenho_saldopos_Rangefilterto ;
      private String Ddo_notaempenho_saldopos_Searchbuttontext ;
      private String Ddo_notaempenho_ativo_Caption ;
      private String Ddo_notaempenho_ativo_Tooltip ;
      private String Ddo_notaempenho_ativo_Cls ;
      private String Ddo_notaempenho_ativo_Selectedvalue_set ;
      private String Ddo_notaempenho_ativo_Dropdownoptionstype ;
      private String Ddo_notaempenho_ativo_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_ativo_Sortedstatus ;
      private String Ddo_notaempenho_ativo_Datalisttype ;
      private String Ddo_notaempenho_ativo_Datalistfixedvalues ;
      private String Ddo_notaempenho_ativo_Sortasc ;
      private String Ddo_notaempenho_ativo_Sortdsc ;
      private String Ddo_notaempenho_ativo_Cleanfilter ;
      private String Ddo_notaempenho_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfnotaempenho_codigo_Internalname ;
      private String edtavTfnotaempenho_codigo_Jsonclick ;
      private String edtavTfnotaempenho_codigo_to_Internalname ;
      private String edtavTfnotaempenho_codigo_to_Jsonclick ;
      private String edtavTfsaldocontrato_codigo_Internalname ;
      private String edtavTfsaldocontrato_codigo_Jsonclick ;
      private String edtavTfsaldocontrato_codigo_to_Internalname ;
      private String edtavTfsaldocontrato_codigo_to_Jsonclick ;
      private String edtavTfsaldocontrato_unidademedicao_sigla_Internalname ;
      private String edtavTfsaldocontrato_unidademedicao_sigla_Jsonclick ;
      private String edtavTfsaldocontrato_unidademedicao_sigla_sel_Internalname ;
      private String edtavTfsaldocontrato_unidademedicao_sigla_sel_Jsonclick ;
      private String edtavTfnotaempenho_itentificador_Internalname ;
      private String edtavTfnotaempenho_itentificador_Jsonclick ;
      private String edtavTfnotaempenho_itentificador_sel_Internalname ;
      private String edtavTfnotaempenho_itentificador_sel_Jsonclick ;
      private String edtavTfnotaempenho_demissao_Internalname ;
      private String edtavTfnotaempenho_demissao_Jsonclick ;
      private String edtavTfnotaempenho_demissao_to_Internalname ;
      private String edtavTfnotaempenho_demissao_to_Jsonclick ;
      private String divDdo_notaempenho_demissaoauxdates_Internalname ;
      private String edtavDdo_notaempenho_demissaoauxdate_Internalname ;
      private String edtavDdo_notaempenho_demissaoauxdate_Jsonclick ;
      private String edtavDdo_notaempenho_demissaoauxdateto_Internalname ;
      private String edtavDdo_notaempenho_demissaoauxdateto_Jsonclick ;
      private String edtavTfnotaempenho_qtd_Internalname ;
      private String edtavTfnotaempenho_qtd_Jsonclick ;
      private String edtavTfnotaempenho_qtd_to_Internalname ;
      private String edtavTfnotaempenho_qtd_to_Jsonclick ;
      private String edtavTfnotaempenho_valor_Internalname ;
      private String edtavTfnotaempenho_valor_Jsonclick ;
      private String edtavTfnotaempenho_valor_to_Internalname ;
      private String edtavTfnotaempenho_valor_to_Jsonclick ;
      private String edtavTfnotaempenho_saldoant_Internalname ;
      private String edtavTfnotaempenho_saldoant_Jsonclick ;
      private String edtavTfnotaempenho_saldoant_to_Internalname ;
      private String edtavTfnotaempenho_saldoant_to_Jsonclick ;
      private String edtavTfnotaempenho_saldopos_Internalname ;
      private String edtavTfnotaempenho_saldopos_Jsonclick ;
      private String edtavTfnotaempenho_saldopos_to_Internalname ;
      private String edtavTfnotaempenho_saldopos_to_Jsonclick ;
      private String edtavTfnotaempenho_ativo_sel_Internalname ;
      private String edtavTfnotaempenho_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_unidademedicao_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtNotaEmpenho_Codigo_Internalname ;
      private String dynSaldoContrato_Codigo_Internalname ;
      private String A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private String edtSaldoContrato_UnidadeMedicao_Sigla_Internalname ;
      private String A1564NotaEmpenho_Itentificador ;
      private String edtNotaEmpenho_Itentificador_Internalname ;
      private String edtNotaEmpenho_DEmissao_Internalname ;
      private String edtNotaEmpenho_Qtd_Internalname ;
      private String edtNotaEmpenho_Valor_Internalname ;
      private String edtNotaEmpenho_SaldoAnt_Internalname ;
      private String edtNotaEmpenho_SaldoPos_Internalname ;
      private String chkNotaEmpenho_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV26TFSaldoContrato_UnidadeMedicao_Sigla ;
      private String lV30TFNotaEmpenho_Itentificador ;
      private String subGrid_Internalname ;
      private String Ddo_notaempenho_codigo_Internalname ;
      private String Ddo_saldocontrato_codigo_Internalname ;
      private String Ddo_saldocontrato_unidademedicao_sigla_Internalname ;
      private String Ddo_notaempenho_itentificador_Internalname ;
      private String Ddo_notaempenho_demissao_Internalname ;
      private String Ddo_notaempenho_qtd_Internalname ;
      private String Ddo_notaempenho_valor_Internalname ;
      private String Ddo_notaempenho_saldoant_Internalname ;
      private String Ddo_notaempenho_saldopos_Internalname ;
      private String Ddo_notaempenho_ativo_Internalname ;
      private String edtNotaEmpenho_Codigo_Title ;
      private String edtSaldoContrato_UnidadeMedicao_Sigla_Title ;
      private String edtNotaEmpenho_Itentificador_Title ;
      private String edtNotaEmpenho_DEmissao_Title ;
      private String edtNotaEmpenho_Qtd_Title ;
      private String edtNotaEmpenho_Valor_Title ;
      private String edtNotaEmpenho_SaldoAnt_Title ;
      private String edtNotaEmpenho_SaldoPos_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String imgInsert_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblTablemergedgrid_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String sCtrlAV7Contrato_Codigo ;
      private String sGXsfl_19_fel_idx="0001" ;
      private String ROClassString ;
      private String edtNotaEmpenho_Codigo_Jsonclick ;
      private String dynSaldoContrato_Codigo_Jsonclick ;
      private String edtSaldoContrato_UnidadeMedicao_Sigla_Jsonclick ;
      private String edtNotaEmpenho_Itentificador_Jsonclick ;
      private String edtNotaEmpenho_DEmissao_Jsonclick ;
      private String edtNotaEmpenho_Qtd_Jsonclick ;
      private String edtNotaEmpenho_Valor_Jsonclick ;
      private String edtNotaEmpenho_SaldoAnt_Jsonclick ;
      private String edtNotaEmpenho_SaldoPos_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV34TFNotaEmpenho_DEmissao ;
      private DateTime AV35TFNotaEmpenho_DEmissao_To ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private DateTime AV36DDO_NotaEmpenho_DEmissaoAuxDate ;
      private DateTime AV37DDO_NotaEmpenho_DEmissaoAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_notaempenho_codigo_Includesortasc ;
      private bool Ddo_notaempenho_codigo_Includesortdsc ;
      private bool Ddo_notaempenho_codigo_Includefilter ;
      private bool Ddo_notaempenho_codigo_Filterisrange ;
      private bool Ddo_notaempenho_codigo_Includedatalist ;
      private bool Ddo_saldocontrato_codigo_Includesortasc ;
      private bool Ddo_saldocontrato_codigo_Includesortdsc ;
      private bool Ddo_saldocontrato_codigo_Includefilter ;
      private bool Ddo_saldocontrato_codigo_Filterisrange ;
      private bool Ddo_saldocontrato_codigo_Includedatalist ;
      private bool Ddo_saldocontrato_unidademedicao_sigla_Includesortasc ;
      private bool Ddo_saldocontrato_unidademedicao_sigla_Includesortdsc ;
      private bool Ddo_saldocontrato_unidademedicao_sigla_Includefilter ;
      private bool Ddo_saldocontrato_unidademedicao_sigla_Filterisrange ;
      private bool Ddo_saldocontrato_unidademedicao_sigla_Includedatalist ;
      private bool Ddo_notaempenho_itentificador_Includesortasc ;
      private bool Ddo_notaempenho_itentificador_Includesortdsc ;
      private bool Ddo_notaempenho_itentificador_Includefilter ;
      private bool Ddo_notaempenho_itentificador_Filterisrange ;
      private bool Ddo_notaempenho_itentificador_Includedatalist ;
      private bool Ddo_notaempenho_demissao_Includesortasc ;
      private bool Ddo_notaempenho_demissao_Includesortdsc ;
      private bool Ddo_notaempenho_demissao_Includefilter ;
      private bool Ddo_notaempenho_demissao_Filterisrange ;
      private bool Ddo_notaempenho_demissao_Includedatalist ;
      private bool Ddo_notaempenho_qtd_Includesortasc ;
      private bool Ddo_notaempenho_qtd_Includesortdsc ;
      private bool Ddo_notaempenho_qtd_Includefilter ;
      private bool Ddo_notaempenho_qtd_Filterisrange ;
      private bool Ddo_notaempenho_qtd_Includedatalist ;
      private bool Ddo_notaempenho_valor_Includesortasc ;
      private bool Ddo_notaempenho_valor_Includesortdsc ;
      private bool Ddo_notaempenho_valor_Includefilter ;
      private bool Ddo_notaempenho_valor_Filterisrange ;
      private bool Ddo_notaempenho_valor_Includedatalist ;
      private bool Ddo_notaempenho_saldoant_Includesortasc ;
      private bool Ddo_notaempenho_saldoant_Includesortdsc ;
      private bool Ddo_notaempenho_saldoant_Includefilter ;
      private bool Ddo_notaempenho_saldoant_Filterisrange ;
      private bool Ddo_notaempenho_saldoant_Includedatalist ;
      private bool Ddo_notaempenho_saldopos_Includesortasc ;
      private bool Ddo_notaempenho_saldopos_Includesortdsc ;
      private bool Ddo_notaempenho_saldopos_Includefilter ;
      private bool Ddo_notaempenho_saldopos_Filterisrange ;
      private bool Ddo_notaempenho_saldopos_Includedatalist ;
      private bool Ddo_notaempenho_ativo_Includesortasc ;
      private bool Ddo_notaempenho_ativo_Includesortdsc ;
      private bool Ddo_notaempenho_ativo_Includefilter ;
      private bool Ddo_notaempenho_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1565NotaEmpenho_DEmissao ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1566NotaEmpenho_Valor ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV16Update_IsBlob ;
      private String AV20ddo_NotaEmpenho_CodigoTitleControlIdToReplace ;
      private String AV24ddo_SaldoContrato_CodigoTitleControlIdToReplace ;
      private String AV28ddo_SaldoContrato_UnidadeMedicao_SiglaTitleControlIdToReplace ;
      private String AV32ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace ;
      private String AV38ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace ;
      private String AV42ddo_NotaEmpenho_QtdTitleControlIdToReplace ;
      private String AV46ddo_NotaEmpenho_ValorTitleControlIdToReplace ;
      private String AV50ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace ;
      private String AV54ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace ;
      private String AV57ddo_NotaEmpenho_AtivoTitleControlIdToReplace ;
      private String AV64Update_GXI ;
      private String AV16Update ;
      private String imgInsert_Bitmap ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynSaldoContrato_Codigo ;
      private GXCheckbox chkNotaEmpenho_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00MH2_A1561SaldoContrato_Codigo ;
      private int[] H00MH3_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] H00MH3_A74Contrato_Codigo ;
      private bool[] H00MH3_A1570NotaEmpenho_Ativo ;
      private bool[] H00MH3_n1570NotaEmpenho_Ativo ;
      private decimal[] H00MH3_A1569NotaEmpenho_SaldoPos ;
      private bool[] H00MH3_n1569NotaEmpenho_SaldoPos ;
      private decimal[] H00MH3_A1568NotaEmpenho_SaldoAnt ;
      private bool[] H00MH3_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] H00MH3_A1566NotaEmpenho_Valor ;
      private bool[] H00MH3_n1566NotaEmpenho_Valor ;
      private decimal[] H00MH3_A1567NotaEmpenho_Qtd ;
      private bool[] H00MH3_n1567NotaEmpenho_Qtd ;
      private DateTime[] H00MH3_A1565NotaEmpenho_DEmissao ;
      private bool[] H00MH3_n1565NotaEmpenho_DEmissao ;
      private String[] H00MH3_A1564NotaEmpenho_Itentificador ;
      private bool[] H00MH3_n1564NotaEmpenho_Itentificador ;
      private String[] H00MH3_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] H00MH3_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private int[] H00MH3_A1561SaldoContrato_Codigo ;
      private int[] H00MH3_A1560NotaEmpenho_Codigo ;
      private long[] H00MH4_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] H00MH5_A1561SaldoContrato_Codigo ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV17NotaEmpenho_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV21SaldoContrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV25SaldoContrato_UnidadeMedicao_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV29NotaEmpenho_ItentificadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33NotaEmpenho_DEmissaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39NotaEmpenho_QtdTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43NotaEmpenho_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47NotaEmpenho_SaldoAntTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51NotaEmpenho_SaldoPosTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55NotaEmpenho_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV58DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratocontratonotaempenhowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MH3( IGxContext context ,
                                             int AV18TFNotaEmpenho_Codigo ,
                                             int AV19TFNotaEmpenho_Codigo_To ,
                                             int AV22TFSaldoContrato_Codigo ,
                                             int AV23TFSaldoContrato_Codigo_To ,
                                             String AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                             String AV26TFSaldoContrato_UnidadeMedicao_Sigla ,
                                             String AV31TFNotaEmpenho_Itentificador_Sel ,
                                             String AV30TFNotaEmpenho_Itentificador ,
                                             DateTime AV34TFNotaEmpenho_DEmissao ,
                                             DateTime AV35TFNotaEmpenho_DEmissao_To ,
                                             decimal AV40TFNotaEmpenho_Qtd ,
                                             decimal AV41TFNotaEmpenho_Qtd_To ,
                                             decimal AV44TFNotaEmpenho_Valor ,
                                             decimal AV45TFNotaEmpenho_Valor_To ,
                                             decimal AV48TFNotaEmpenho_SaldoAnt ,
                                             decimal AV49TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV52TFNotaEmpenho_SaldoPos ,
                                             decimal AV53TFNotaEmpenho_SaldoPos_To ,
                                             short AV56TFNotaEmpenho_Ativo_Sel ,
                                             int A1560NotaEmpenho_Codigo ,
                                             int A1561SaldoContrato_Codigo ,
                                             String A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [24] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo, T2.[Contrato_Codigo], T1.[NotaEmpenho_Ativo], T1.[NotaEmpenho_SaldoPos], T1.[NotaEmpenho_SaldoAnt], T1.[NotaEmpenho_Valor], T1.[NotaEmpenho_Qtd], T1.[NotaEmpenho_DEmissao], T1.[NotaEmpenho_Itentificador], T3.[UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla, T1.[SaldoContrato_Codigo], T1.[NotaEmpenho_Codigo]";
         sFromString = " FROM (([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[SaldoContrato_UnidadeMedicao_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T2.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         if ( ! (0==AV18TFNotaEmpenho_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] >= @AV18TFNotaEmpenho_Codigo)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV19TFNotaEmpenho_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] <= @AV19TFNotaEmpenho_Codigo_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV22TFSaldoContrato_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] >= @AV22TFSaldoContrato_Codigo)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV23TFSaldoContrato_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] <= @AV23TFSaldoContrato_Codigo_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] like @lV26TFSaldoContrato_UnidadeMedicao_Sigla)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] = @AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] like @lV30TFNotaEmpenho_Itentificador)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] = @AV31TFNotaEmpenho_Itentificador_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV34TFNotaEmpenho_DEmissao) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] >= @AV34TFNotaEmpenho_DEmissao)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFNotaEmpenho_DEmissao_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] <= @AV35TFNotaEmpenho_DEmissao_To)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV40TFNotaEmpenho_Qtd) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] >= @AV40TFNotaEmpenho_Qtd)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV41TFNotaEmpenho_Qtd_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] <= @AV41TFNotaEmpenho_Qtd_To)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFNotaEmpenho_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] >= @AV44TFNotaEmpenho_Valor)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV45TFNotaEmpenho_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] <= @AV45TFNotaEmpenho_Valor_To)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFNotaEmpenho_SaldoAnt) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] >= @AV48TFNotaEmpenho_SaldoAnt)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV49TFNotaEmpenho_SaldoAnt_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] <= @AV49TFNotaEmpenho_SaldoAnt_To)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV52TFNotaEmpenho_SaldoPos) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] >= @AV52TFNotaEmpenho_SaldoPos)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV53TFNotaEmpenho_SaldoPos_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] <= @AV53TFNotaEmpenho_SaldoPos_To)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV56TFNotaEmpenho_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 1)";
         }
         if ( AV56TFNotaEmpenho_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[SaldoContrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[SaldoContrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T3.[UnidadeMedicao_Sigla]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T3.[UnidadeMedicao_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_Itentificador]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_Itentificador] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_DEmissao]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_DEmissao] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_Qtd]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_Qtd] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_Valor]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_Valor] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_SaldoAnt]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_SaldoAnt] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_SaldoPos]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_SaldoPos] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_Ativo]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Codigo] DESC, T1.[NotaEmpenho_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[NotaEmpenho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00MH4( IGxContext context ,
                                             int AV18TFNotaEmpenho_Codigo ,
                                             int AV19TFNotaEmpenho_Codigo_To ,
                                             int AV22TFSaldoContrato_Codigo ,
                                             int AV23TFSaldoContrato_Codigo_To ,
                                             String AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                             String AV26TFSaldoContrato_UnidadeMedicao_Sigla ,
                                             String AV31TFNotaEmpenho_Itentificador_Sel ,
                                             String AV30TFNotaEmpenho_Itentificador ,
                                             DateTime AV34TFNotaEmpenho_DEmissao ,
                                             DateTime AV35TFNotaEmpenho_DEmissao_To ,
                                             decimal AV40TFNotaEmpenho_Qtd ,
                                             decimal AV41TFNotaEmpenho_Qtd_To ,
                                             decimal AV44TFNotaEmpenho_Valor ,
                                             decimal AV45TFNotaEmpenho_Valor_To ,
                                             decimal AV48TFNotaEmpenho_SaldoAnt ,
                                             decimal AV49TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV52TFNotaEmpenho_SaldoPos ,
                                             decimal AV53TFNotaEmpenho_SaldoPos_To ,
                                             short AV56TFNotaEmpenho_Ativo_Sel ,
                                             int A1560NotaEmpenho_Codigo ,
                                             int A1561SaldoContrato_Codigo ,
                                             String A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [19] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[SaldoContrato_UnidadeMedicao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         if ( ! (0==AV18TFNotaEmpenho_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] >= @AV18TFNotaEmpenho_Codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV19TFNotaEmpenho_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] <= @AV19TFNotaEmpenho_Codigo_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV22TFSaldoContrato_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] >= @AV22TFSaldoContrato_Codigo)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV23TFSaldoContrato_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] <= @AV23TFSaldoContrato_Codigo_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFSaldoContrato_UnidadeMedicao_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] like @lV26TFSaldoContrato_UnidadeMedicao_Sigla)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] = @AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFNotaEmpenho_Itentificador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] like @lV30TFNotaEmpenho_Itentificador)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFNotaEmpenho_Itentificador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] = @AV31TFNotaEmpenho_Itentificador_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV34TFNotaEmpenho_DEmissao) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] >= @AV34TFNotaEmpenho_DEmissao)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFNotaEmpenho_DEmissao_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] <= @AV35TFNotaEmpenho_DEmissao_To)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV40TFNotaEmpenho_Qtd) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] >= @AV40TFNotaEmpenho_Qtd)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV41TFNotaEmpenho_Qtd_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] <= @AV41TFNotaEmpenho_Qtd_To)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFNotaEmpenho_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] >= @AV44TFNotaEmpenho_Valor)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV45TFNotaEmpenho_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] <= @AV45TFNotaEmpenho_Valor_To)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFNotaEmpenho_SaldoAnt) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] >= @AV48TFNotaEmpenho_SaldoAnt)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV49TFNotaEmpenho_SaldoAnt_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] <= @AV49TFNotaEmpenho_SaldoAnt_To)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV52TFNotaEmpenho_SaldoPos) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] >= @AV52TFNotaEmpenho_SaldoPos)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV53TFNotaEmpenho_SaldoPos_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] <= @AV53TFNotaEmpenho_SaldoPos_To)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV56TFNotaEmpenho_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 1)";
         }
         if ( AV56TFNotaEmpenho_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00MH3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (decimal)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (bool)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] );
               case 2 :
                     return conditional_H00MH4(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (decimal)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (bool)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MH2 ;
          prmH00MH2 = new Object[] {
          } ;
          Object[] prmH00MH5 ;
          prmH00MH5 = new Object[] {
          } ;
          Object[] prmH00MH3 ;
          prmH00MH3 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV26TFSaldoContrato_UnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV30TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV31TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV34TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV35TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV41TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV44TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV45TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV49TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV52TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV53TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00MH4 ;
          prmH00MH4 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV26TFSaldoContrato_UnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV27TFSaldoContrato_UnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV30TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV31TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV34TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV35TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV41TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV44TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV45TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV49TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV52TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV53TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MH2", "SELECT [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) ORDER BY [SaldoContrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MH2,0,0,true,false )
             ,new CursorDef("H00MH3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MH3,11,0,true,false )
             ,new CursorDef("H00MH4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MH4,1,0,true,false )
             ,new CursorDef("H00MH5", "SELECT [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) ORDER BY [SaldoContrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MH5,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                return;
       }
    }

 }

}
