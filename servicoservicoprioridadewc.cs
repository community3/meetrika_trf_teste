/*
               File: ServicoServicoPrioridadeWC
        Description: Servico Servico Prioridade WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:54:47.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoservicoprioridadewc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicoservicoprioridadewc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicoservicoprioridadewc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ServicoPrioridade_SrvCod )
      {
         this.AV7ServicoPrioridade_SrvCod = aP0_ServicoPrioridade_SrvCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ServicoPrioridade_SrvCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV19TFServicoPrioridade_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoPrioridade_Nome", AV19TFServicoPrioridade_Nome);
                  AV20TFServicoPrioridade_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFServicoPrioridade_Nome_Sel", AV20TFServicoPrioridade_Nome_Sel);
                  AV23TFServicoPrioridade_Finalidade = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFServicoPrioridade_Finalidade", AV23TFServicoPrioridade_Finalidade);
                  AV24TFServicoPrioridade_Finalidade_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFServicoPrioridade_Finalidade_Sel", AV24TFServicoPrioridade_Finalidade_Sel);
                  AV7ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0)));
                  AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace", AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace);
                  AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace", AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace);
                  AV34Pgmname = GetNextPar( );
                  A1440ServicoPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A1439ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, AV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, AV7ServicoPrioridade_SrvCod, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV34Pgmname, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PALK2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV34Pgmname = "ServicoServicoPrioridadeWC";
               context.Gx_err = 0;
               WSLK2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Servico Prioridade WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812544736");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoservicoprioridadewc.aspx") + "?" + UrlEncode("" +AV7ServicoPrioridade_SrvCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOPRIORIDADE_NOME", StringUtil.RTrim( AV19TFServicoPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOPRIORIDADE_NOME_SEL", StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOPRIORIDADE_FINALIDADE", AV23TFServicoPrioridade_Finalidade);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOPRIORIDADE_FINALIDADE_SEL", AV24TFServicoPrioridade_Finalidade_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV26DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV26DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOPRIORIDADE_NOMETITLEFILTERDATA", AV18ServicoPrioridade_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOPRIORIDADE_NOMETITLEFILTERDATA", AV18ServicoPrioridade_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA", AV22ServicoPrioridade_FinalidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA", AV22ServicoPrioridade_FinalidadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ServicoPrioridade_SrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOPRIORIDADE_SRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV34Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Caption", StringUtil.RTrim( Ddo_servicoprioridade_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Tooltip", StringUtil.RTrim( Ddo_servicoprioridade_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Cls", StringUtil.RTrim( Ddo_servicoprioridade_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servicoprioridade_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servicoprioridade_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicoprioridade_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicoprioridade_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servicoprioridade_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filtertype", StringUtil.RTrim( Ddo_servicoprioridade_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servicoprioridade_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Datalisttype", StringUtil.RTrim( Ddo_servicoprioridade_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Datalistproc", StringUtil.RTrim( Ddo_servicoprioridade_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Sortasc", StringUtil.RTrim( Ddo_servicoprioridade_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Sortdsc", StringUtil.RTrim( Ddo_servicoprioridade_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Loadingdata", StringUtil.RTrim( Ddo_servicoprioridade_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servicoprioridade_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servicoprioridade_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servicoprioridade_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Caption", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Tooltip", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Cls", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filtertype", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_servicoprioridade_finalidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Datalisttype", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistproc", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Sortasc", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Sortdsc", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Loadingdata", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Noresultsfound", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servicoprioridade_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servicoprioridade_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servicoprioridade_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_servicoprioridade_finalidade_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormLK2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicoservicoprioridadewc.js", "?20205181254485");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoServicoPrioridadeWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Servico Prioridade WC" ;
      }

      protected void WBLK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicoservicoprioridadewc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_LK2( true) ;
         }
         else
         {
            wb_table1_2_LK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LK2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoServicoPrioridadeWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ServicoServicoPrioridadeWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoprioridade_nome_Internalname, StringUtil.RTrim( AV19TFServicoPrioridade_Nome), StringUtil.RTrim( context.localUtil.Format( AV19TFServicoPrioridade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoprioridade_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoprioridade_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoServicoPrioridadeWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoprioridade_nome_sel_Internalname, StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV20TFServicoPrioridade_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoprioridade_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoprioridade_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoServicoPrioridadeWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfservicoprioridade_finalidade_Internalname, AV23TFServicoPrioridade_Finalidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", 0, edtavTfservicoprioridade_finalidade_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ServicoServicoPrioridadeWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfservicoprioridade_finalidade_sel_Internalname, AV24TFServicoPrioridade_Finalidade_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", 0, edtavTfservicoprioridade_finalidade_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ServicoServicoPrioridadeWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOPRIORIDADE_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoPrioridadeWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", 0, edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoServicoPrioridadeWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTLK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Servico Prioridade WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPLK0( ) ;
            }
         }
      }

      protected void WSLK2( )
      {
         STARTLK2( ) ;
         EVTLK2( ) ;
      }

      protected void EVTLK2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11LK2 */
                                    E11LK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOPRIORIDADE_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12LK2 */
                                    E12LK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOPRIORIDADE_FINALIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13LK2 */
                                    E13LK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14LK2 */
                                    E14LK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLK0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV32Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV16Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV33Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV16Delete))));
                              A1440ServicoPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoPrioridade_Codigo_Internalname), ",", "."));
                              A1439ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( edtServicoPrioridade_SrvCod_Internalname), ",", "."));
                              A1441ServicoPrioridade_Nome = StringUtil.Upper( cgiGet( edtServicoPrioridade_Nome_Internalname));
                              A1442ServicoPrioridade_Finalidade = cgiGet( edtServicoPrioridade_Finalidade_Internalname);
                              n1442ServicoPrioridade_Finalidade = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15LK2 */
                                          E15LK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16LK2 */
                                          E16LK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17LK2 */
                                          E17LK2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicoprioridade_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_NOME"), AV19TFServicoPrioridade_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicoprioridade_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_NOME_SEL"), AV20TFServicoPrioridade_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicoprioridade_finalidade Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_FINALIDADE"), AV23TFServicoPrioridade_Finalidade) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicoprioridade_finalidade_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_FINALIDADE_SEL"), AV24TFServicoPrioridade_Finalidade_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPLK0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormLK2( ) ;
            }
         }
      }

      protected void PALK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV19TFServicoPrioridade_Nome ,
                                       String AV20TFServicoPrioridade_Nome_Sel ,
                                       String AV23TFServicoPrioridade_Finalidade ,
                                       String AV24TFServicoPrioridade_Finalidade_Sel ,
                                       int AV7ServicoPrioridade_SrvCod ,
                                       String AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace ,
                                       String AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace ,
                                       String AV34Pgmname ,
                                       int A1440ServicoPrioridade_Codigo ,
                                       int A1439ServicoPrioridade_SrvCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFLK2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_SRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1439ServicoPrioridade_SrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOPRIORIDADE_SRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOPRIORIDADE_NOME", StringUtil.RTrim( A1441ServicoPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_FINALIDADE", GetSecureSignedToken( sPrefix, A1442ServicoPrioridade_Finalidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOPRIORIDADE_FINALIDADE", A1442ServicoPrioridade_Finalidade);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV34Pgmname = "ServicoServicoPrioridadeWC";
         context.Gx_err = 0;
      }

      protected void RFLK2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E16LK2 */
         E16LK2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV20TFServicoPrioridade_Nome_Sel ,
                                                 AV19TFServicoPrioridade_Nome ,
                                                 AV24TFServicoPrioridade_Finalidade_Sel ,
                                                 AV23TFServicoPrioridade_Finalidade ,
                                                 A1441ServicoPrioridade_Nome ,
                                                 A1442ServicoPrioridade_Finalidade ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1439ServicoPrioridade_SrvCod ,
                                                 AV7ServicoPrioridade_SrvCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV19TFServicoPrioridade_Nome = StringUtil.PadR( StringUtil.RTrim( AV19TFServicoPrioridade_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoPrioridade_Nome", AV19TFServicoPrioridade_Nome);
            lV23TFServicoPrioridade_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV23TFServicoPrioridade_Finalidade), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFServicoPrioridade_Finalidade", AV23TFServicoPrioridade_Finalidade);
            /* Using cursor H00LK2 */
            pr_default.execute(0, new Object[] {AV7ServicoPrioridade_SrvCod, lV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, lV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1442ServicoPrioridade_Finalidade = H00LK2_A1442ServicoPrioridade_Finalidade[0];
               n1442ServicoPrioridade_Finalidade = H00LK2_n1442ServicoPrioridade_Finalidade[0];
               A1441ServicoPrioridade_Nome = H00LK2_A1441ServicoPrioridade_Nome[0];
               A1439ServicoPrioridade_SrvCod = H00LK2_A1439ServicoPrioridade_SrvCod[0];
               A1440ServicoPrioridade_Codigo = H00LK2_A1440ServicoPrioridade_Codigo[0];
               /* Execute user event: E17LK2 */
               E17LK2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBLK0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV20TFServicoPrioridade_Nome_Sel ,
                                              AV19TFServicoPrioridade_Nome ,
                                              AV24TFServicoPrioridade_Finalidade_Sel ,
                                              AV23TFServicoPrioridade_Finalidade ,
                                              A1441ServicoPrioridade_Nome ,
                                              A1442ServicoPrioridade_Finalidade ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1439ServicoPrioridade_SrvCod ,
                                              AV7ServicoPrioridade_SrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV19TFServicoPrioridade_Nome = StringUtil.PadR( StringUtil.RTrim( AV19TFServicoPrioridade_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoPrioridade_Nome", AV19TFServicoPrioridade_Nome);
         lV23TFServicoPrioridade_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV23TFServicoPrioridade_Finalidade), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFServicoPrioridade_Finalidade", AV23TFServicoPrioridade_Finalidade);
         /* Using cursor H00LK3 */
         pr_default.execute(1, new Object[] {AV7ServicoPrioridade_SrvCod, lV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, lV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel});
         GRID_nRecordCount = H00LK3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, AV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, AV7ServicoPrioridade_SrvCod, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV34Pgmname, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, AV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, AV7ServicoPrioridade_SrvCod, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV34Pgmname, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, AV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, AV7ServicoPrioridade_SrvCod, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV34Pgmname, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, AV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, AV7ServicoPrioridade_SrvCod, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV34Pgmname, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFServicoPrioridade_Nome, AV20TFServicoPrioridade_Nome_Sel, AV23TFServicoPrioridade_Finalidade, AV24TFServicoPrioridade_Finalidade_Sel, AV7ServicoPrioridade_SrvCod, AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, AV34Pgmname, A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPLK0( )
      {
         /* Before Start, stand alone formulas. */
         AV34Pgmname = "ServicoServicoPrioridadeWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15LK2 */
         E15LK2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV26DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOPRIORIDADE_NOMETITLEFILTERDATA"), AV18ServicoPrioridade_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA"), AV22ServicoPrioridade_FinalidadeTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV19TFServicoPrioridade_Nome = StringUtil.Upper( cgiGet( edtavTfservicoprioridade_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoPrioridade_Nome", AV19TFServicoPrioridade_Nome);
            AV20TFServicoPrioridade_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservicoprioridade_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFServicoPrioridade_Nome_Sel", AV20TFServicoPrioridade_Nome_Sel);
            AV23TFServicoPrioridade_Finalidade = cgiGet( edtavTfservicoprioridade_finalidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFServicoPrioridade_Finalidade", AV23TFServicoPrioridade_Finalidade);
            AV24TFServicoPrioridade_Finalidade_Sel = cgiGet( edtavTfservicoprioridade_finalidade_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFServicoPrioridade_Finalidade_Sel", AV24TFServicoPrioridade_Finalidade_Sel);
            AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace", AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace);
            AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = cgiGet( edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace", AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV28GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV29GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoPrioridade_SrvCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servicoprioridade_nome_Caption = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Caption");
            Ddo_servicoprioridade_nome_Tooltip = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Tooltip");
            Ddo_servicoprioridade_nome_Cls = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Cls");
            Ddo_servicoprioridade_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filteredtext_set");
            Ddo_servicoprioridade_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_set");
            Ddo_servicoprioridade_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Dropdownoptionstype");
            Ddo_servicoprioridade_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Titlecontrolidtoreplace");
            Ddo_servicoprioridade_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includesortasc"));
            Ddo_servicoprioridade_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includesortdsc"));
            Ddo_servicoprioridade_nome_Sortedstatus = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Sortedstatus");
            Ddo_servicoprioridade_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includefilter"));
            Ddo_servicoprioridade_nome_Filtertype = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filtertype");
            Ddo_servicoprioridade_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filterisrange"));
            Ddo_servicoprioridade_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Includedatalist"));
            Ddo_servicoprioridade_nome_Datalisttype = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Datalisttype");
            Ddo_servicoprioridade_nome_Datalistproc = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Datalistproc");
            Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicoprioridade_nome_Sortasc = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Sortasc");
            Ddo_servicoprioridade_nome_Sortdsc = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Sortdsc");
            Ddo_servicoprioridade_nome_Loadingdata = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Loadingdata");
            Ddo_servicoprioridade_nome_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Cleanfilter");
            Ddo_servicoprioridade_nome_Noresultsfound = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Noresultsfound");
            Ddo_servicoprioridade_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Searchbuttontext");
            Ddo_servicoprioridade_finalidade_Caption = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Caption");
            Ddo_servicoprioridade_finalidade_Tooltip = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Tooltip");
            Ddo_servicoprioridade_finalidade_Cls = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Cls");
            Ddo_servicoprioridade_finalidade_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_set");
            Ddo_servicoprioridade_finalidade_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_set");
            Ddo_servicoprioridade_finalidade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Dropdownoptionstype");
            Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Titlecontrolidtoreplace");
            Ddo_servicoprioridade_finalidade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortasc"));
            Ddo_servicoprioridade_finalidade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includesortdsc"));
            Ddo_servicoprioridade_finalidade_Sortedstatus = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Sortedstatus");
            Ddo_servicoprioridade_finalidade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includefilter"));
            Ddo_servicoprioridade_finalidade_Filtertype = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filtertype");
            Ddo_servicoprioridade_finalidade_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filterisrange"));
            Ddo_servicoprioridade_finalidade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Includedatalist"));
            Ddo_servicoprioridade_finalidade_Datalisttype = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Datalisttype");
            Ddo_servicoprioridade_finalidade_Datalistproc = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistproc");
            Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicoprioridade_finalidade_Sortasc = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Sortasc");
            Ddo_servicoprioridade_finalidade_Sortdsc = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Sortdsc");
            Ddo_servicoprioridade_finalidade_Loadingdata = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Loadingdata");
            Ddo_servicoprioridade_finalidade_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Cleanfilter");
            Ddo_servicoprioridade_finalidade_Noresultsfound = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Noresultsfound");
            Ddo_servicoprioridade_finalidade_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servicoprioridade_nome_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Activeeventkey");
            Ddo_servicoprioridade_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Filteredtext_get");
            Ddo_servicoprioridade_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_NOME_Selectedvalue_get");
            Ddo_servicoprioridade_finalidade_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Activeeventkey");
            Ddo_servicoprioridade_finalidade_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Filteredtext_get");
            Ddo_servicoprioridade_finalidade_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_NOME"), AV19TFServicoPrioridade_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_NOME_SEL"), AV20TFServicoPrioridade_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_FINALIDADE"), AV23TFServicoPrioridade_Finalidade) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOPRIORIDADE_FINALIDADE_SEL"), AV24TFServicoPrioridade_Finalidade_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15LK2 */
         E15LK2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15LK2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfservicoprioridade_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicoprioridade_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_nome_Visible), 5, 0)));
         edtavTfservicoprioridade_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicoprioridade_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_nome_sel_Visible), 5, 0)));
         edtavTfservicoprioridade_finalidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicoprioridade_finalidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_finalidade_Visible), 5, 0)));
         edtavTfservicoprioridade_finalidade_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicoprioridade_finalidade_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoprioridade_finalidade_sel_Visible), 5, 0)));
         Ddo_servicoprioridade_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoPrioridade_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "TitleControlIdToReplace", Ddo_servicoprioridade_nome_Titlecontrolidtoreplace);
         AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace = Ddo_servicoprioridade_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace", AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace);
         edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoPrioridade_Finalidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "TitleControlIdToReplace", Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace);
         AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace", AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace);
         edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV26DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV26DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E16LK2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV18ServicoPrioridade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22ServicoPrioridade_FinalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoPrioridade_Nome_Titleformat = 2;
         edtServicoPrioridade_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoPrioridade_Nome_Internalname, "Title", edtServicoPrioridade_Nome_Title);
         edtServicoPrioridade_Finalidade_Titleformat = 2;
         edtServicoPrioridade_Finalidade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Finalidade", AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoPrioridade_Finalidade_Internalname, "Title", edtServicoPrioridade_Finalidade_Title);
         AV28GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridCurrentPage), 10, 0)));
         AV29GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV18ServicoPrioridade_NomeTitleFilterData", AV18ServicoPrioridade_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22ServicoPrioridade_FinalidadeTitleFilterData", AV22ServicoPrioridade_FinalidadeTitleFilterData);
      }

      protected void E11LK2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV27PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV27PageToGo) ;
         }
      }

      protected void E12LK2( )
      {
         /* Ddo_servicoprioridade_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicoprioridade_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV19TFServicoPrioridade_Nome = Ddo_servicoprioridade_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoPrioridade_Nome", AV19TFServicoPrioridade_Nome);
            AV20TFServicoPrioridade_Nome_Sel = Ddo_servicoprioridade_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFServicoPrioridade_Nome_Sel", AV20TFServicoPrioridade_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13LK2( )
      {
         /* Ddo_servicoprioridade_finalidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicoprioridade_finalidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_finalidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_finalidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoprioridade_finalidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_servicoprioridade_finalidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFServicoPrioridade_Finalidade = Ddo_servicoprioridade_finalidade_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFServicoPrioridade_Finalidade", AV23TFServicoPrioridade_Finalidade);
            AV24TFServicoPrioridade_Finalidade_Sel = Ddo_servicoprioridade_finalidade_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFServicoPrioridade_Finalidade_Sel", AV24TFServicoPrioridade_Finalidade_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E17LK2( )
      {
         /* Grid_Load Routine */
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV32Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1440ServicoPrioridade_Codigo) + "," + UrlEncode("" +A1439ServicoPrioridade_SrvCod);
         AV16Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV16Delete);
         AV33Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1440ServicoPrioridade_Codigo) + "," + UrlEncode("" +A1439ServicoPrioridade_SrvCod);
         edtServicoPrioridade_Nome_Link = formatLink("viewservicoprioridade.aspx") + "?" + UrlEncode("" +A1440ServicoPrioridade_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E14LK2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7ServicoPrioridade_SrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servicoprioridade_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
         Ddo_servicoprioridade_finalidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_servicoprioridade_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "SortedStatus", Ddo_servicoprioridade_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_servicoprioridade_finalidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "SortedStatus", Ddo_servicoprioridade_finalidade_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV17Session.Get(AV34Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV34Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV17Session.Get(AV34Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV35GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_NOME") == 0 )
            {
               AV19TFServicoPrioridade_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoPrioridade_Nome", AV19TFServicoPrioridade_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoPrioridade_Nome)) )
               {
                  Ddo_servicoprioridade_nome_Filteredtext_set = AV19TFServicoPrioridade_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "FilteredText_set", Ddo_servicoprioridade_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_NOME_SEL") == 0 )
            {
               AV20TFServicoPrioridade_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFServicoPrioridade_Nome_Sel", AV20TFServicoPrioridade_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel)) )
               {
                  Ddo_servicoprioridade_nome_Selectedvalue_set = AV20TFServicoPrioridade_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_nome_Internalname, "SelectedValue_set", Ddo_servicoprioridade_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_FINALIDADE") == 0 )
            {
               AV23TFServicoPrioridade_Finalidade = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFServicoPrioridade_Finalidade", AV23TFServicoPrioridade_Finalidade);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoPrioridade_Finalidade)) )
               {
                  Ddo_servicoprioridade_finalidade_Filteredtext_set = AV23TFServicoPrioridade_Finalidade;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "FilteredText_set", Ddo_servicoprioridade_finalidade_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_FINALIDADE_SEL") == 0 )
            {
               AV24TFServicoPrioridade_Finalidade_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFServicoPrioridade_Finalidade_Sel", AV24TFServicoPrioridade_Finalidade_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFServicoPrioridade_Finalidade_Sel)) )
               {
                  Ddo_servicoprioridade_finalidade_Selectedvalue_set = AV24TFServicoPrioridade_Finalidade_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoprioridade_finalidade_Internalname, "SelectedValue_set", Ddo_servicoprioridade_finalidade_Selectedvalue_set);
               }
            }
            AV35GXV1 = (int)(AV35GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV17Session.Get(AV34Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoPrioridade_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV19TFServicoPrioridade_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV20TFServicoPrioridade_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoPrioridade_Finalidade)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_FINALIDADE";
            AV12GridStateFilterValue.gxTpr_Value = AV23TFServicoPrioridade_Finalidade;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFServicoPrioridade_Finalidade_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOPRIORIDADE_FINALIDADE_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV24TFServicoPrioridade_Finalidade_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ServicoPrioridade_SrvCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SERVICOPRIORIDADE_SRVCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV34Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV34Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ServicoPrioridade";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ServicoPrioridade_SrvCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV17Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_LK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_LK2( true) ;
         }
         else
         {
            wb_table2_5_LK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_LK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoServicoPrioridadeWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LK2e( true) ;
         }
         else
         {
            wb_table1_2_LK2e( false) ;
         }
      }

      protected void wb_table2_5_LK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servico Prioridade_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoPrioridade_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoPrioridade_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoPrioridade_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoPrioridade_Finalidade_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoPrioridade_Finalidade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoPrioridade_Finalidade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1441ServicoPrioridade_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoPrioridade_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoPrioridade_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServicoPrioridade_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1442ServicoPrioridade_Finalidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoPrioridade_Finalidade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoPrioridade_Finalidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_LK2e( true) ;
         }
         else
         {
            wb_table2_5_LK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ServicoPrioridade_SrvCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALK2( ) ;
         WSLK2( ) ;
         WELK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ServicoPrioridade_SrvCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PALK2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicoservicoprioridadewc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PALK2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ServicoPrioridade_SrvCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0)));
         }
         wcpOAV7ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoPrioridade_SrvCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ServicoPrioridade_SrvCod != wcpOAV7ServicoPrioridade_SrvCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ServicoPrioridade_SrvCod = AV7ServicoPrioridade_SrvCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ServicoPrioridade_SrvCod = cgiGet( sPrefix+"AV7ServicoPrioridade_SrvCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ServicoPrioridade_SrvCod) > 0 )
         {
            AV7ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ServicoPrioridade_SrvCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0)));
         }
         else
         {
            AV7ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ServicoPrioridade_SrvCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PALK2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSLK2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSLK2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoPrioridade_SrvCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoPrioridade_SrvCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ServicoPrioridade_SrvCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoPrioridade_SrvCod_CTRL", StringUtil.RTrim( sCtrlAV7ServicoPrioridade_SrvCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WELK2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181254500");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicoservicoprioridadewc.js", "?20205181254500");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_idx;
         edtServicoPrioridade_Codigo_Internalname = sPrefix+"SERVICOPRIORIDADE_CODIGO_"+sGXsfl_8_idx;
         edtServicoPrioridade_SrvCod_Internalname = sPrefix+"SERVICOPRIORIDADE_SRVCOD_"+sGXsfl_8_idx;
         edtServicoPrioridade_Nome_Internalname = sPrefix+"SERVICOPRIORIDADE_NOME_"+sGXsfl_8_idx;
         edtServicoPrioridade_Finalidade_Internalname = sPrefix+"SERVICOPRIORIDADE_FINALIDADE_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_fel_idx;
         edtServicoPrioridade_Codigo_Internalname = sPrefix+"SERVICOPRIORIDADE_CODIGO_"+sGXsfl_8_fel_idx;
         edtServicoPrioridade_SrvCod_Internalname = sPrefix+"SERVICOPRIORIDADE_SRVCOD_"+sGXsfl_8_fel_idx;
         edtServicoPrioridade_Nome_Internalname = sPrefix+"SERVICOPRIORIDADE_NOME_"+sGXsfl_8_fel_idx;
         edtServicoPrioridade_Finalidade_Internalname = sPrefix+"SERVICOPRIORIDADE_FINALIDADE_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBLK0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV32Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV32Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV33Delete_GXI : context.PathToRelativeUrl( AV16Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoPrioridade_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_SrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1439ServicoPrioridade_SrvCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoPrioridade_SrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_Nome_Internalname,StringUtil.RTrim( A1441ServicoPrioridade_Nome),StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtServicoPrioridade_Nome_Link,(String)"",(String)"",(String)"",(String)edtServicoPrioridade_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoPrioridade_Finalidade_Internalname,(String)A1442ServicoPrioridade_Finalidade,(String)A1442ServicoPrioridade_Finalidade,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoPrioridade_Finalidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)8,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_SRVCOD"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1439ServicoPrioridade_SrvCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_NOME"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOPRIORIDADE_FINALIDADE"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1442ServicoPrioridade_Finalidade));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtServicoPrioridade_Codigo_Internalname = sPrefix+"SERVICOPRIORIDADE_CODIGO";
         edtServicoPrioridade_SrvCod_Internalname = sPrefix+"SERVICOPRIORIDADE_SRVCOD";
         edtServicoPrioridade_Nome_Internalname = sPrefix+"SERVICOPRIORIDADE_NOME";
         edtServicoPrioridade_Finalidade_Internalname = sPrefix+"SERVICOPRIORIDADE_FINALIDADE";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfservicoprioridade_nome_Internalname = sPrefix+"vTFSERVICOPRIORIDADE_NOME";
         edtavTfservicoprioridade_nome_sel_Internalname = sPrefix+"vTFSERVICOPRIORIDADE_NOME_SEL";
         edtavTfservicoprioridade_finalidade_Internalname = sPrefix+"vTFSERVICOPRIORIDADE_FINALIDADE";
         edtavTfservicoprioridade_finalidade_sel_Internalname = sPrefix+"vTFSERVICOPRIORIDADE_FINALIDADE_SEL";
         Ddo_servicoprioridade_nome_Internalname = sPrefix+"DDO_SERVICOPRIORIDADE_NOME";
         edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE";
         Ddo_servicoprioridade_finalidade_Internalname = sPrefix+"DDO_SERVICOPRIORIDADE_FINALIDADE";
         edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtServicoPrioridade_Finalidade_Jsonclick = "";
         edtServicoPrioridade_Nome_Jsonclick = "";
         edtServicoPrioridade_SrvCod_Jsonclick = "";
         edtServicoPrioridade_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtServicoPrioridade_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtServicoPrioridade_Finalidade_Titleformat = 0;
         edtServicoPrioridade_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtServicoPrioridade_Finalidade_Title = "Finalidade";
         edtServicoPrioridade_Nome_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfservicoprioridade_finalidade_sel_Visible = 1;
         edtavTfservicoprioridade_finalidade_Visible = 1;
         edtavTfservicoprioridade_nome_sel_Jsonclick = "";
         edtavTfservicoprioridade_nome_sel_Visible = 1;
         edtavTfservicoprioridade_nome_Jsonclick = "";
         edtavTfservicoprioridade_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_servicoprioridade_finalidade_Searchbuttontext = "Pesquisar";
         Ddo_servicoprioridade_finalidade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicoprioridade_finalidade_Cleanfilter = "Limpar pesquisa";
         Ddo_servicoprioridade_finalidade_Loadingdata = "Carregando dados...";
         Ddo_servicoprioridade_finalidade_Sortdsc = "Ordenar de Z � A";
         Ddo_servicoprioridade_finalidade_Sortasc = "Ordenar de A � Z";
         Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters = 0;
         Ddo_servicoprioridade_finalidade_Datalistproc = "GetServicoServicoPrioridadeWCFilterData";
         Ddo_servicoprioridade_finalidade_Datalisttype = "Dynamic";
         Ddo_servicoprioridade_finalidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicoprioridade_finalidade_Filtertype = "Character";
         Ddo_servicoprioridade_finalidade_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace = "";
         Ddo_servicoprioridade_finalidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicoprioridade_finalidade_Cls = "ColumnSettings";
         Ddo_servicoprioridade_finalidade_Tooltip = "Op��es";
         Ddo_servicoprioridade_finalidade_Caption = "";
         Ddo_servicoprioridade_nome_Searchbuttontext = "Pesquisar";
         Ddo_servicoprioridade_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicoprioridade_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servicoprioridade_nome_Loadingdata = "Carregando dados...";
         Ddo_servicoprioridade_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servicoprioridade_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servicoprioridade_nome_Datalistproc = "GetServicoServicoPrioridadeWCFilterData";
         Ddo_servicoprioridade_nome_Datalisttype = "Dynamic";
         Ddo_servicoprioridade_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicoprioridade_nome_Filtertype = "Character";
         Ddo_servicoprioridade_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicoprioridade_nome_Titlecontrolidtoreplace = "";
         Ddo_servicoprioridade_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicoprioridade_nome_Cls = "ColumnSettings";
         Ddo_servicoprioridade_nome_Tooltip = "Op��es";
         Ddo_servicoprioridade_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV20TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV23TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV24TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV7ServicoPrioridade_SrvCod',fld:'vSERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV18ServicoPrioridade_NomeTitleFilterData',fld:'vSERVICOPRIORIDADE_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV22ServicoPrioridade_FinalidadeTitleFilterData',fld:'vSERVICOPRIORIDADE_FINALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoPrioridade_Nome_Titleformat',ctrl:'SERVICOPRIORIDADE_NOME',prop:'Titleformat'},{av:'edtServicoPrioridade_Nome_Title',ctrl:'SERVICOPRIORIDADE_NOME',prop:'Title'},{av:'edtServicoPrioridade_Finalidade_Titleformat',ctrl:'SERVICOPRIORIDADE_FINALIDADE',prop:'Titleformat'},{av:'edtServicoPrioridade_Finalidade_Title',ctrl:'SERVICOPRIORIDADE_FINALIDADE',prop:'Title'},{av:'AV28GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV29GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11LK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV20TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV23TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV24TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV7ServicoPrioridade_SrvCod',fld:'vSERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICOPRIORIDADE_NOME.ONOPTIONCLICKED","{handler:'E12LK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV20TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV23TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV24TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV7ServicoPrioridade_SrvCod',fld:'vSERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_servicoprioridade_nome_Activeeventkey',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'ActiveEventKey'},{av:'Ddo_servicoprioridade_nome_Filteredtext_get',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'FilteredText_get'},{av:'Ddo_servicoprioridade_nome_Selectedvalue_get',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicoprioridade_nome_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SortedStatus'},{av:'AV19TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV20TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servicoprioridade_finalidade_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOPRIORIDADE_FINALIDADE.ONOPTIONCLICKED","{handler:'E13LK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFServicoPrioridade_Nome',fld:'vTFSERVICOPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV20TFServicoPrioridade_Nome_Sel',fld:'vTFSERVICOPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV23TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV24TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'AV7ServicoPrioridade_SrvCod',fld:'vSERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace',fld:'vDDO_SERVICOPRIORIDADE_FINALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_servicoprioridade_finalidade_Activeeventkey',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'ActiveEventKey'},{av:'Ddo_servicoprioridade_finalidade_Filteredtext_get',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'FilteredText_get'},{av:'Ddo_servicoprioridade_finalidade_Selectedvalue_get',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicoprioridade_finalidade_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_FINALIDADE',prop:'SortedStatus'},{av:'AV23TFServicoPrioridade_Finalidade',fld:'vTFSERVICOPRIORIDADE_FINALIDADE',pic:'',nv:''},{av:'AV24TFServicoPrioridade_Finalidade_Sel',fld:'vTFSERVICOPRIORIDADE_FINALIDADE_SEL',pic:'',nv:''},{av:'Ddo_servicoprioridade_nome_Sortedstatus',ctrl:'DDO_SERVICOPRIORIDADE_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17LK2',iparms:[{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtServicoPrioridade_Nome_Link',ctrl:'SERVICOPRIORIDADE_NOME',prop:'Link'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E14LK2',iparms:[{av:'A1440ServicoPrioridade_Codigo',fld:'SERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7ServicoPrioridade_SrvCod',fld:'vSERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7ServicoPrioridade_SrvCod',fld:'vSERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_servicoprioridade_nome_Activeeventkey = "";
         Ddo_servicoprioridade_nome_Filteredtext_get = "";
         Ddo_servicoprioridade_nome_Selectedvalue_get = "";
         Ddo_servicoprioridade_finalidade_Activeeventkey = "";
         Ddo_servicoprioridade_finalidade_Filteredtext_get = "";
         Ddo_servicoprioridade_finalidade_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV19TFServicoPrioridade_Nome = "";
         AV20TFServicoPrioridade_Nome_Sel = "";
         AV23TFServicoPrioridade_Finalidade = "";
         AV24TFServicoPrioridade_Finalidade_Sel = "";
         AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace = "";
         AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace = "";
         AV34Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV26DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV18ServicoPrioridade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22ServicoPrioridade_FinalidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servicoprioridade_nome_Filteredtext_set = "";
         Ddo_servicoprioridade_nome_Selectedvalue_set = "";
         Ddo_servicoprioridade_nome_Sortedstatus = "";
         Ddo_servicoprioridade_finalidade_Filteredtext_set = "";
         Ddo_servicoprioridade_finalidade_Selectedvalue_set = "";
         Ddo_servicoprioridade_finalidade_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Update = "";
         AV32Update_GXI = "";
         AV16Delete = "";
         AV33Delete_GXI = "";
         A1441ServicoPrioridade_Nome = "";
         A1442ServicoPrioridade_Finalidade = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19TFServicoPrioridade_Nome = "";
         lV23TFServicoPrioridade_Finalidade = "";
         H00LK2_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         H00LK2_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         H00LK2_A1441ServicoPrioridade_Nome = new String[] {""} ;
         H00LK2_A1439ServicoPrioridade_SrvCod = new int[1] ;
         H00LK2_A1440ServicoPrioridade_Codigo = new int[1] ;
         H00LK3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV17Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ServicoPrioridade_SrvCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoservicoprioridadewc__default(),
            new Object[][] {
                new Object[] {
               H00LK2_A1442ServicoPrioridade_Finalidade, H00LK2_n1442ServicoPrioridade_Finalidade, H00LK2_A1441ServicoPrioridade_Nome, H00LK2_A1439ServicoPrioridade_SrvCod, H00LK2_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               H00LK3_AGRID_nRecordCount
               }
            }
         );
         AV34Pgmname = "ServicoServicoPrioridadeWC";
         /* GeneXus formulas. */
         AV34Pgmname = "ServicoServicoPrioridadeWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServicoPrioridade_Nome_Titleformat ;
      private short edtServicoPrioridade_Finalidade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ServicoPrioridade_SrvCod ;
      private int wcpOAV7ServicoPrioridade_SrvCod ;
      private int subGrid_Rows ;
      private int A1440ServicoPrioridade_Codigo ;
      private int A1439ServicoPrioridade_SrvCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servicoprioridade_nome_Datalistupdateminimumcharacters ;
      private int Ddo_servicoprioridade_finalidade_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfservicoprioridade_nome_Visible ;
      private int edtavTfservicoprioridade_nome_sel_Visible ;
      private int edtavTfservicoprioridade_finalidade_Visible ;
      private int edtavTfservicoprioridade_finalidade_sel_Visible ;
      private int edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV27PageToGo ;
      private int AV35GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV28GridCurrentPage ;
      private long AV29GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servicoprioridade_nome_Activeeventkey ;
      private String Ddo_servicoprioridade_nome_Filteredtext_get ;
      private String Ddo_servicoprioridade_nome_Selectedvalue_get ;
      private String Ddo_servicoprioridade_finalidade_Activeeventkey ;
      private String Ddo_servicoprioridade_finalidade_Filteredtext_get ;
      private String Ddo_servicoprioridade_finalidade_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV19TFServicoPrioridade_Nome ;
      private String AV20TFServicoPrioridade_Nome_Sel ;
      private String AV34Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servicoprioridade_nome_Caption ;
      private String Ddo_servicoprioridade_nome_Tooltip ;
      private String Ddo_servicoprioridade_nome_Cls ;
      private String Ddo_servicoprioridade_nome_Filteredtext_set ;
      private String Ddo_servicoprioridade_nome_Selectedvalue_set ;
      private String Ddo_servicoprioridade_nome_Dropdownoptionstype ;
      private String Ddo_servicoprioridade_nome_Titlecontrolidtoreplace ;
      private String Ddo_servicoprioridade_nome_Sortedstatus ;
      private String Ddo_servicoprioridade_nome_Filtertype ;
      private String Ddo_servicoprioridade_nome_Datalisttype ;
      private String Ddo_servicoprioridade_nome_Datalistproc ;
      private String Ddo_servicoprioridade_nome_Sortasc ;
      private String Ddo_servicoprioridade_nome_Sortdsc ;
      private String Ddo_servicoprioridade_nome_Loadingdata ;
      private String Ddo_servicoprioridade_nome_Cleanfilter ;
      private String Ddo_servicoprioridade_nome_Noresultsfound ;
      private String Ddo_servicoprioridade_nome_Searchbuttontext ;
      private String Ddo_servicoprioridade_finalidade_Caption ;
      private String Ddo_servicoprioridade_finalidade_Tooltip ;
      private String Ddo_servicoprioridade_finalidade_Cls ;
      private String Ddo_servicoprioridade_finalidade_Filteredtext_set ;
      private String Ddo_servicoprioridade_finalidade_Selectedvalue_set ;
      private String Ddo_servicoprioridade_finalidade_Dropdownoptionstype ;
      private String Ddo_servicoprioridade_finalidade_Titlecontrolidtoreplace ;
      private String Ddo_servicoprioridade_finalidade_Sortedstatus ;
      private String Ddo_servicoprioridade_finalidade_Filtertype ;
      private String Ddo_servicoprioridade_finalidade_Datalisttype ;
      private String Ddo_servicoprioridade_finalidade_Datalistproc ;
      private String Ddo_servicoprioridade_finalidade_Sortasc ;
      private String Ddo_servicoprioridade_finalidade_Sortdsc ;
      private String Ddo_servicoprioridade_finalidade_Loadingdata ;
      private String Ddo_servicoprioridade_finalidade_Cleanfilter ;
      private String Ddo_servicoprioridade_finalidade_Noresultsfound ;
      private String Ddo_servicoprioridade_finalidade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfservicoprioridade_nome_Internalname ;
      private String edtavTfservicoprioridade_nome_Jsonclick ;
      private String edtavTfservicoprioridade_nome_sel_Internalname ;
      private String edtavTfservicoprioridade_nome_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfservicoprioridade_finalidade_Internalname ;
      private String edtavTfservicoprioridade_finalidade_sel_Internalname ;
      private String edtavDdo_servicoprioridade_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicoprioridade_finalidadetitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServicoPrioridade_Codigo_Internalname ;
      private String edtServicoPrioridade_SrvCod_Internalname ;
      private String A1441ServicoPrioridade_Nome ;
      private String edtServicoPrioridade_Nome_Internalname ;
      private String edtServicoPrioridade_Finalidade_Internalname ;
      private String scmdbuf ;
      private String lV19TFServicoPrioridade_Nome ;
      private String subGrid_Internalname ;
      private String Ddo_servicoprioridade_nome_Internalname ;
      private String Ddo_servicoprioridade_finalidade_Internalname ;
      private String edtServicoPrioridade_Nome_Title ;
      private String edtServicoPrioridade_Finalidade_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtServicoPrioridade_Nome_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ServicoPrioridade_SrvCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServicoPrioridade_Codigo_Jsonclick ;
      private String edtServicoPrioridade_SrvCod_Jsonclick ;
      private String edtServicoPrioridade_Nome_Jsonclick ;
      private String edtServicoPrioridade_Finalidade_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servicoprioridade_nome_Includesortasc ;
      private bool Ddo_servicoprioridade_nome_Includesortdsc ;
      private bool Ddo_servicoprioridade_nome_Includefilter ;
      private bool Ddo_servicoprioridade_nome_Filterisrange ;
      private bool Ddo_servicoprioridade_nome_Includedatalist ;
      private bool Ddo_servicoprioridade_finalidade_Includesortasc ;
      private bool Ddo_servicoprioridade_finalidade_Includesortdsc ;
      private bool Ddo_servicoprioridade_finalidade_Includefilter ;
      private bool Ddo_servicoprioridade_finalidade_Filterisrange ;
      private bool Ddo_servicoprioridade_finalidade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1442ServicoPrioridade_Finalidade ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV16Delete_IsBlob ;
      private String A1442ServicoPrioridade_Finalidade ;
      private String AV23TFServicoPrioridade_Finalidade ;
      private String AV24TFServicoPrioridade_Finalidade_Sel ;
      private String AV21ddo_ServicoPrioridade_NomeTitleControlIdToReplace ;
      private String AV25ddo_ServicoPrioridade_FinalidadeTitleControlIdToReplace ;
      private String AV32Update_GXI ;
      private String AV33Delete_GXI ;
      private String lV23TFServicoPrioridade_Finalidade ;
      private String AV15Update ;
      private String AV16Delete ;
      private IGxSession AV17Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00LK2_A1442ServicoPrioridade_Finalidade ;
      private bool[] H00LK2_n1442ServicoPrioridade_Finalidade ;
      private String[] H00LK2_A1441ServicoPrioridade_Nome ;
      private int[] H00LK2_A1439ServicoPrioridade_SrvCod ;
      private int[] H00LK2_A1440ServicoPrioridade_Codigo ;
      private long[] H00LK3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV18ServicoPrioridade_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22ServicoPrioridade_FinalidadeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV26DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class servicoservicoprioridadewc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00LK2( IGxContext context ,
                                             String AV20TFServicoPrioridade_Nome_Sel ,
                                             String AV19TFServicoPrioridade_Nome ,
                                             String AV24TFServicoPrioridade_Finalidade_Sel ,
                                             String AV23TFServicoPrioridade_Finalidade ,
                                             String A1441ServicoPrioridade_Nome ,
                                             String A1442ServicoPrioridade_Finalidade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1439ServicoPrioridade_SrvCod ,
                                             int AV7ServicoPrioridade_SrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [10] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ServicoPrioridade_Finalidade], [ServicoPrioridade_Nome], [ServicoPrioridade_SrvCod], [ServicoPrioridade_Codigo]";
         sFromString = " FROM [ServicoPrioridade] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([ServicoPrioridade_SrvCod] = @AV7ServicoPrioridade_SrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoPrioridade_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like @lV19TFServicoPrioridade_Nome)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] = @AV20TFServicoPrioridade_Nome_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFServicoPrioridade_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoPrioridade_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] like @lV23TFServicoPrioridade_Finalidade)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFServicoPrioridade_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] = @AV24TFServicoPrioridade_Finalidade_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_SrvCod], [ServicoPrioridade_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_SrvCod] DESC, [ServicoPrioridade_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_SrvCod], [ServicoPrioridade_Finalidade]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_SrvCod] DESC, [ServicoPrioridade_Finalidade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ServicoPrioridade_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00LK3( IGxContext context ,
                                             String AV20TFServicoPrioridade_Nome_Sel ,
                                             String AV19TFServicoPrioridade_Nome ,
                                             String AV24TFServicoPrioridade_Finalidade_Sel ,
                                             String AV23TFServicoPrioridade_Finalidade ,
                                             String A1441ServicoPrioridade_Nome ,
                                             String A1442ServicoPrioridade_Finalidade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1439ServicoPrioridade_SrvCod ,
                                             int AV7ServicoPrioridade_SrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ServicoPrioridade] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoPrioridade_SrvCod] = @AV7ServicoPrioridade_SrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoPrioridade_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like @lV19TFServicoPrioridade_Nome)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFServicoPrioridade_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] = @AV20TFServicoPrioridade_Nome_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFServicoPrioridade_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoPrioridade_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] like @lV23TFServicoPrioridade_Finalidade)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFServicoPrioridade_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] = @AV24TFServicoPrioridade_Finalidade_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00LK2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
               case 1 :
                     return conditional_H00LK3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LK2 ;
          prmH00LK2 = new Object[] {
          new Object[] {"@AV7ServicoPrioridade_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19TFServicoPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV20TFServicoPrioridade_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23TFServicoPrioridade_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV24TFServicoPrioridade_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00LK3 ;
          prmH00LK3 = new Object[] {
          new Object[] {"@AV7ServicoPrioridade_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19TFServicoPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV20TFServicoPrioridade_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23TFServicoPrioridade_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV24TFServicoPrioridade_Finalidade_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LK2,11,0,true,false )
             ,new CursorDef("H00LK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LK3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

}
