/*
               File: DP_ServicosContratados
        Description: Servicos Contratados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:47:45.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_servicoscontratados : GXProcedure
   {
      public dp_servicoscontratados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_servicoscontratados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_Contratada_Codigo ,
                           out IGxCollection aP2_Gxm2rootcol )
      {
         this.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV6Contratada_Codigo = aP1_Contratada_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_AreaTrabalho_Codigo ,
                                       int aP1_Contratada_Codigo )
      {
         this.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV6Contratada_Codigo = aP1_Contratada_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 int aP1_Contratada_Codigo ,
                                 out IGxCollection aP2_Gxm2rootcol )
      {
         dp_servicoscontratados objdp_servicoscontratados;
         objdp_servicoscontratados = new dp_servicoscontratados();
         objdp_servicoscontratados.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objdp_servicoscontratados.AV6Contratada_Codigo = aP1_Contratada_Codigo;
         objdp_servicoscontratados.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         objdp_servicoscontratados.context.SetSubmitInitialConfig(context);
         objdp_servicoscontratados.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_servicoscontratados);
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_servicoscontratados)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV6Contratada_Codigo ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A39Contratada_Codigo ,
                                              A638ContratoServicos_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P000G2 */
         pr_default.execute(0, new Object[] {AV5AreaTrabalho_Codigo, AV6Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P000G2_A74Contrato_Codigo[0];
            A638ContratoServicos_Ativo = P000G2_A638ContratoServicos_Ativo[0];
            A39Contratada_Codigo = P000G2_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P000G2_A75Contrato_AreaTrabalhoCod[0];
            A605Servico_Sigla = P000G2_A605Servico_Sigla[0];
            A155Servico_Codigo = P000G2_A155Servico_Codigo[0];
            A39Contratada_Codigo = P000G2_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P000G2_A75Contrato_AreaTrabalhoCod[0];
            A605Servico_Sigla = P000G2_A605Servico_Sigla[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A155Servico_Codigo;
            Gxm1sdt_codigos.gxTpr_Descricao = A605Servico_Sigla;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000G2_A74Contrato_Codigo = new int[1] ;
         P000G2_A638ContratoServicos_Ativo = new bool[] {false} ;
         P000G2_A39Contratada_Codigo = new int[1] ;
         P000G2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P000G2_A605Servico_Sigla = new String[] {""} ;
         P000G2_A155Servico_Codigo = new int[1] ;
         A605Servico_Sigla = "";
         Gxm1sdt_codigos = new SdtSDT_Codigos(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_servicoscontratados__default(),
            new Object[][] {
                new Object[] {
               P000G2_A74Contrato_Codigo, P000G2_A638ContratoServicos_Ativo, P000G2_A39Contratada_Codigo, P000G2_A75Contrato_AreaTrabalhoCod, P000G2_A605Servico_Sigla, P000G2_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5AreaTrabalho_Codigo ;
      private int AV6Contratada_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private String scmdbuf ;
      private String A605Servico_Sigla ;
      private bool A638ContratoServicos_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000G2_A74Contrato_Codigo ;
      private bool[] P000G2_A638ContratoServicos_Ativo ;
      private int[] P000G2_A39Contratada_Codigo ;
      private int[] P000G2_A75Contrato_AreaTrabalhoCod ;
      private String[] P000G2_A605Servico_Sigla ;
      private int[] P000G2_A155Servico_Codigo ;
      private IGxCollection aP2_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_Codigos Gxm1sdt_codigos ;
   }

   public class dp_servicoscontratados__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P000G2( IGxContext context ,
                                             int AV5AreaTrabalho_Codigo ,
                                             int AV6Contratada_Codigo ,
                                             int A75Contrato_AreaTrabalhoCod ,
                                             int A39Contratada_Codigo ,
                                             bool A638ContratoServicos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [Contrato_Codigo], NULL AS [ContratoServicos_Ativo], NULL AS [Contratada_Codigo], NULL AS [Contrato_AreaTrabalhoCod], [Servico_Sigla], [Servico_Codigo] FROM ( SELECT TOP(100) PERCENT T1.[Contrato_Codigo], T1.[ContratoServicos_Ativo], T2.[Contratada_Codigo], T2.[Contrato_AreaTrabalhoCod], T3.[Servico_Sigla], T1.[Servico_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicos_Ativo] = 1)";
         if ( AV5AreaTrabalho_Codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV6Contratada_Codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_Codigo] = @AV6Contratada_Codigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Codigo]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P000G2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000G2 ;
          prmP000G2 = new Object[] {
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000G2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
