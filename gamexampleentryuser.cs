/*
               File: GAMExampleEntryUser
        Description: User
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:11:48.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleentryuser : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleentryuser( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleentryuser( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref String aP1_UserId )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV53UserId = aP1_UserId;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_UserId=this.AV53UserId;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavAuthenticationtypename = new GXCombobox();
         cmbavGender = new GXCombobox();
         chkavIsactive = new GXCheckbox();
         chkavDontreceiveinformation = new GXCheckbox();
         chkavCannotchangepassword = new GXCheckbox();
         chkavMustchangepassword = new GXCheckbox();
         chkavPasswordneverexpires = new GXCheckbox();
         chkavIsblocked = new GXCheckbox();
         cmbavSecuritypolicyid = new GXCombobox();
         chkavIsenabledinrepository = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV53UserId = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserId", AV53UserId);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV53UserId, ""))));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA232( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START232( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823114981");
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleentryuser.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV53UserId))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vSTRING", AV50String);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "TBGO_Link", StringUtil.RTrim( lblTbgo_Link));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "GAMExampleEntryUser";
         AV25IsEnabledInRepository = StringUtil.StrToBool( StringUtil.BoolToStr( AV25IsEnabledInRepository));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsEnabledInRepository", AV25IsEnabledInRepository);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vISENABLEDINREPOSITORY", GetSecureSignedToken( "", AV25IsEnabledInRepository));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( AV25IsEnabledInRepository);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("gamexampleentryuser:[SendSecurityCheck value for]"+"IsEnabledInRepository:"+StringUtil.BoolToStr( AV25IsEnabledInRepository));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE232( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT232( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleentryuser.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV53UserId)) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleEntryUser" ;
      }

      public override String GetPgmdesc( )
      {
         return "User " ;
      }

      protected void WB230( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_232( true) ;
         }
         else
         {
            wb_table1_3_232( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_232e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_158_232( true) ;
         }
         else
         {
            wb_table2_158_232( false) ;
         }
         return  ;
      }

      protected void wb_table2_158_232e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_177_232( true) ;
         }
         else
         {
            wb_table3_177_232( false) ;
         }
         return  ;
      }

      protected void wb_table3_177_232e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START232( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "User ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP230( ) ;
      }

      protected void WS232( )
      {
         START232( ) ;
         EVT232( ) ;
      }

      protected void EVT232( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11232 */
                              E11232 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12232 */
                                    E12232 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENABLEDISABLE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13232 */
                              E13232 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAUTHENTICATIONTYPENAME.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14232 */
                              E14232 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15232 */
                              E15232 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE232( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA232( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavAuthenticationtypename.Name = "vAUTHENTICATIONTYPENAME";
            cmbavAuthenticationtypename.WebTags = "";
            cmbavAuthenticationtypename.addItem("0", "(select)", 0);
            if ( cmbavAuthenticationtypename.ItemCount > 0 )
            {
               AV7AuthenticationTypeName = cmbavAuthenticationtypename.getValidValue(AV7AuthenticationTypeName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AuthenticationTypeName", AV7AuthenticationTypeName);
            }
            cmbavGender.Name = "vGENDER";
            cmbavGender.WebTags = "";
            cmbavGender.addItem("N", "Not Specified", 0);
            cmbavGender.addItem("F", "Female", 0);
            cmbavGender.addItem("M", "Male", 0);
            if ( cmbavGender.ItemCount > 0 )
            {
               AV21Gender = cmbavGender.getValidValue(AV21Gender);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Gender", AV21Gender);
            }
            chkavIsactive.Name = "vISACTIVE";
            chkavIsactive.WebTags = "";
            chkavIsactive.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsactive_Internalname, "TitleCaption", chkavIsactive.Caption);
            chkavIsactive.CheckedValue = "false";
            chkavDontreceiveinformation.Name = "vDONTRECEIVEINFORMATION";
            chkavDontreceiveinformation.WebTags = "";
            chkavDontreceiveinformation.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDontreceiveinformation_Internalname, "TitleCaption", chkavDontreceiveinformation.Caption);
            chkavDontreceiveinformation.CheckedValue = "false";
            chkavCannotchangepassword.Name = "vCANNOTCHANGEPASSWORD";
            chkavCannotchangepassword.WebTags = "";
            chkavCannotchangepassword.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCannotchangepassword_Internalname, "TitleCaption", chkavCannotchangepassword.Caption);
            chkavCannotchangepassword.CheckedValue = "false";
            chkavMustchangepassword.Name = "vMUSTCHANGEPASSWORD";
            chkavMustchangepassword.WebTags = "";
            chkavMustchangepassword.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavMustchangepassword_Internalname, "TitleCaption", chkavMustchangepassword.Caption);
            chkavMustchangepassword.CheckedValue = "false";
            chkavPasswordneverexpires.Name = "vPASSWORDNEVEREXPIRES";
            chkavPasswordneverexpires.WebTags = "";
            chkavPasswordneverexpires.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPasswordneverexpires_Internalname, "TitleCaption", chkavPasswordneverexpires.Caption);
            chkavPasswordneverexpires.CheckedValue = "false";
            chkavIsblocked.Name = "vISBLOCKED";
            chkavIsblocked.WebTags = "";
            chkavIsblocked.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsblocked_Internalname, "TitleCaption", chkavIsblocked.Caption);
            chkavIsblocked.CheckedValue = "false";
            cmbavSecuritypolicyid.Name = "vSECURITYPOLICYID";
            cmbavSecuritypolicyid.WebTags = "";
            cmbavSecuritypolicyid.addItem("0", "(select)", 0);
            if ( cmbavSecuritypolicyid.ItemCount > 0 )
            {
               AV49SecurityPolicyId = (int)(NumberUtil.Val( cmbavSecuritypolicyid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49SecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0)));
            }
            chkavIsenabledinrepository.Name = "vISENABLEDINREPOSITORY";
            chkavIsenabledinrepository.WebTags = "";
            chkavIsenabledinrepository.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenabledinrepository_Internalname, "TitleCaption", chkavIsenabledinrepository.Caption);
            chkavIsenabledinrepository.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsernamespace_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAuthenticationtypename.ItemCount > 0 )
         {
            AV7AuthenticationTypeName = cmbavAuthenticationtypename.getValidValue(AV7AuthenticationTypeName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AuthenticationTypeName", AV7AuthenticationTypeName);
         }
         if ( cmbavGender.ItemCount > 0 )
         {
            AV21Gender = cmbavGender.getValidValue(AV21Gender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Gender", AV21Gender);
         }
         if ( cmbavSecuritypolicyid.ItemCount > 0 )
         {
            AV49SecurityPolicyId = (int)(NumberUtil.Val( cmbavSecuritypolicyid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49SecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF232( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUserid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUserid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUserid_Enabled), 5, 0)));
         edtavUsernamespace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsernamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsernamespace_Enabled), 5, 0)));
         edtavActivationdate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavActivationdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavActivationdate_Enabled), 5, 0)));
         chkavIsenabledinrepository.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenabledinrepository_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsenabledinrepository.Enabled), 5, 0)));
         edtavDatelastauthentication_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatelastauthentication_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatelastauthentication_Enabled), 5, 0)));
      }

      protected void RF232( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15232 */
            E15232 ();
            WB230( ) ;
         }
      }

      protected void STRUP230( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUserid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUserid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUserid_Enabled), 5, 0)));
         edtavUsernamespace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsernamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsernamespace_Enabled), 5, 0)));
         edtavActivationdate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavActivationdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavActivationdate_Enabled), 5, 0)));
         chkavIsenabledinrepository.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenabledinrepository_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsenabledinrepository.Enabled), 5, 0)));
         edtavDatelastauthentication_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatelastauthentication_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatelastauthentication_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11232 */
         E11232 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV53UserId = cgiGet( edtavUserid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserId", AV53UserId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV53UserId, ""))));
            AV54UserNameSpace = cgiGet( edtavUsernamespace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54UserNameSpace", AV54UserNameSpace);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERNAMESPACE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV54UserNameSpace, ""))));
            cmbavAuthenticationtypename.CurrentValue = cgiGet( cmbavAuthenticationtypename_Internalname);
            AV7AuthenticationTypeName = cgiGet( cmbavAuthenticationtypename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AuthenticationTypeName", AV7AuthenticationTypeName);
            AV45ReqName = cgiGet( imgavReqname_Internalname);
            AV33Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Name", AV33Name);
            AV40ReqEmail = cgiGet( imgavReqemail_Internalname);
            AV15EMail = cgiGet( edtavEmail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15EMail", AV15EMail);
            AV41ReqFirstName = cgiGet( imgavReqfirstname_Internalname);
            AV20FirstName = cgiGet( edtavFirstname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FirstName", AV20FirstName);
            AV44ReqLastName = cgiGet( imgavReqlastname_Internalname);
            AV28LastName = cgiGet( edtavLastname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LastName", AV28LastName);
            AV18ExternalId = cgiGet( edtavExternalid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ExternalId", AV18ExternalId);
            AV39ReqBirthday = cgiGet( imgavReqbirthday_Internalname);
            if ( context.localUtil.VCDate( cgiGet( edtavBirthday_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Birthday"}), 1, "vBIRTHDAY");
               GX_FocusControl = edtavBirthday_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11Birthday = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Birthday", context.localUtil.Format(AV11Birthday, "99/99/9999"));
            }
            else
            {
               AV11Birthday = context.localUtil.CToD( cgiGet( edtavBirthday_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Birthday", context.localUtil.Format(AV11Birthday, "99/99/9999"));
            }
            AV42ReqGender = cgiGet( imgavReqgender_Internalname);
            cmbavGender.CurrentValue = cgiGet( cmbavGender_Internalname);
            AV21Gender = cgiGet( cmbavGender_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Gender", AV21Gender);
            AV22Image = cgiGet( imgavImage_Internalname);
            AV51URLProfile = cgiGet( edtavUrlprofile_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51URLProfile", AV51URLProfile);
            AV23IsActive = StringUtil.StrToBool( cgiGet( chkavIsactive_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23IsActive", AV23IsActive);
            if ( context.localUtil.VCDateTime( cgiGet( edtavActivationdate_Internalname), 2, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Activation Date"}), 1, "vACTIVATIONDATE");
               GX_FocusControl = edtavActivationdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5ActivationDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ActivationDate", context.localUtil.TToC( AV5ActivationDate, 10, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vACTIVATIONDATE", GetSecureSignedToken( "", context.localUtil.Format( AV5ActivationDate, "99/99/9999 99:99")));
            }
            else
            {
               AV5ActivationDate = context.localUtil.CToT( cgiGet( edtavActivationdate_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ActivationDate", context.localUtil.TToC( AV5ActivationDate, 10, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vACTIVATIONDATE", GetSecureSignedToken( "", context.localUtil.Format( AV5ActivationDate, "99/99/9999 99:99")));
            }
            AV14DontReceiveInformation = StringUtil.StrToBool( cgiGet( chkavDontreceiveinformation_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DontReceiveInformation", AV14DontReceiveInformation);
            AV12CannotChangePassword = StringUtil.StrToBool( cgiGet( chkavCannotchangepassword_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12CannotChangePassword", AV12CannotChangePassword);
            AV32MustChangePassword = StringUtil.StrToBool( cgiGet( chkavMustchangepassword_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32MustChangePassword", AV32MustChangePassword);
            AV37PasswordNeverExpires = StringUtil.StrToBool( cgiGet( chkavPasswordneverexpires_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37PasswordNeverExpires", AV37PasswordNeverExpires);
            AV24IsBlocked = StringUtil.StrToBool( cgiGet( chkavIsblocked_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24IsBlocked", AV24IsBlocked);
            cmbavSecuritypolicyid.CurrentValue = cgiGet( cmbavSecuritypolicyid_Internalname);
            AV49SecurityPolicyId = (int)(NumberUtil.Val( cgiGet( cmbavSecuritypolicyid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49SecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0)));
            AV25IsEnabledInRepository = StringUtil.StrToBool( cgiGet( chkavIsenabledinrepository_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsEnabledInRepository", AV25IsEnabledInRepository);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vISENABLEDINREPOSITORY", GetSecureSignedToken( "", AV25IsEnabledInRepository));
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatelastauthentication_Internalname), 2, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Date Last Authentication"}), 1, "vDATELASTAUTHENTICATION");
               GX_FocusControl = edtavDatelastauthentication_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13DateLastAuthentication = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13DateLastAuthentication", context.localUtil.TToC( AV13DateLastAuthentication, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATELASTAUTHENTICATION", GetSecureSignedToken( "", context.localUtil.Format( AV13DateLastAuthentication, "99/99/99 99:99")));
            }
            else
            {
               AV13DateLastAuthentication = context.localUtil.CToT( cgiGet( edtavDatelastauthentication_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13DateLastAuthentication", context.localUtil.TToC( AV13DateLastAuthentication, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATELASTAUTHENTICATION", GetSecureSignedToken( "", context.localUtil.Format( AV13DateLastAuthentication, "99/99/99 99:99")));
            }
            AV46ReqPassword = cgiGet( imgavReqpassword_Internalname);
            AV34Password = cgiGet( edtavPassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Password", AV34Password);
            AV46ReqPassword = cgiGet( imgavReqpassword_Internalname);
            AV35PasswordConf = cgiGet( edtavPasswordconf_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35PasswordConf", AV35PasswordConf);
            AV43ReqIcon = cgiGet( imgavReqicon_Internalname);
            /* Read saved values. */
            AV50String = cgiGet( "vSTRING");
            lblTbgo_Link = cgiGet( "TBGO_Link");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "GAMExampleEntryUser";
            AV25IsEnabledInRepository = StringUtil.StrToBool( cgiGet( chkavIsenabledinrepository_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsEnabledInRepository", AV25IsEnabledInRepository);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vISENABLEDINREPOSITORY", GetSecureSignedToken( "", AV25IsEnabledInRepository));
            forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( AV25IsEnabledInRepository);
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("gamexampleentryuser:[SecurityCheckFailed value for]"+"IsEnabledInRepository:"+StringUtil.BoolToStr( AV25IsEnabledInRepository));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11232 */
         E11232 ();
         if (returnInSub) return;
      }

      protected void E11232( )
      {
         /* Start Routine */
         AV38Repository = new SdtGAMRepository(context).get();
         AV45ReqName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV45ReqName)) ? AV58Reqname_GXI : context.convertURL( context.PathToRelativeUrl( AV45ReqName))));
         AV58Reqname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV45ReqName)) ? AV58Reqname_GXI : context.convertURL( context.PathToRelativeUrl( AV45ReqName))));
         if ( AV38Repository.gxTpr_Requiredemail )
         {
            AV40ReqEmail = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV40ReqEmail)) ? AV59Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV40ReqEmail))));
            AV59Reqemail_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV40ReqEmail)) ? AV59Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV40ReqEmail))));
         }
         else
         {
            imgavReqemail_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqemail_Visible), 5, 0)));
         }
         if ( AV38Repository.gxTpr_Requiredfirstname )
         {
            AV41ReqFirstName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV41ReqFirstName)) ? AV60Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV41ReqFirstName))));
            AV60Reqfirstname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV41ReqFirstName)) ? AV60Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV41ReqFirstName))));
         }
         else
         {
            imgavReqfirstname_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqfirstname_Visible), 5, 0)));
         }
         if ( AV38Repository.gxTpr_Requiredlastname )
         {
            AV44ReqLastName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV44ReqLastName)) ? AV61Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV44ReqLastName))));
            AV61Reqlastname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV44ReqLastName)) ? AV61Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV44ReqLastName))));
         }
         else
         {
            imgavReqlastname_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqlastname_Visible), 5, 0)));
         }
         if ( AV38Repository.gxTpr_Requiredbirthday )
         {
            AV39ReqBirthday = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV39ReqBirthday)) ? AV62Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV39ReqBirthday))));
            AV62Reqbirthday_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV39ReqBirthday)) ? AV62Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV39ReqBirthday))));
         }
         else
         {
            imgavReqbirthday_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqbirthday_Visible), 5, 0)));
         }
         if ( AV38Repository.gxTpr_Requiredgender )
         {
            AV42ReqGender = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV42ReqGender)) ? AV63Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV42ReqGender))));
            AV63Reqgender_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV42ReqGender)) ? AV63Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV42ReqGender))));
         }
         else
         {
            imgavReqgender_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqgender_Visible), 5, 0)));
         }
         if ( AV38Repository.gxTpr_Requiredpassword )
         {
            AV46ReqPassword = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword)) ? AV64Reqpassword_GXI : context.convertURL( context.PathToRelativeUrl( AV46ReqPassword))));
            AV64Reqpassword_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword)) ? AV64Reqpassword_GXI : context.convertURL( context.PathToRelativeUrl( AV46ReqPassword))));
         }
         else
         {
            imgavReqpassword_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqpassword_Visible), 5, 0)));
         }
         AV43ReqIcon = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV43ReqIcon)) ? AV65Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV43ReqIcon))));
         AV65Reqicon_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV43ReqIcon)) ? AV65Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV43ReqIcon))));
         lblTbinficonreq_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinficonreq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinficonreq_Visible), 5, 0)));
         cmbavAuthenticationtypename.removeAllItems();
         AV8AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV27Language, out  AV17Errors);
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV8AuthenticationTypes.Count )
         {
            AV9AuthenticationTypesIns = ((SdtGAMAuthenticationTypeSimple)AV8AuthenticationTypes.Item(AV66GXV1));
            cmbavAuthenticationtypename.addItem(AV9AuthenticationTypesIns.gxTpr_Name, AV9AuthenticationTypesIns.gxTpr_Description, 0);
            AV66GXV1 = (int)(AV66GXV1+1);
         }
         AV47SecurityPolicies = new SdtGAMRepository(context).getsecuritypolicies(AV19FilterSecPol, out  AV17Errors);
         cmbavSecuritypolicyid.addItem("0", "(None)", 0);
         AV67GXV2 = 1;
         while ( AV67GXV2 <= AV47SecurityPolicies.Count )
         {
            AV48SecurityPolicy = ((SdtGAMSecurityPolicy)AV47SecurityPolicies.Item(AV67GXV2));
            cmbavSecuritypolicyid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV48SecurityPolicy.gxTpr_Id), 9, 0)), AV48SecurityPolicy.gxTpr_Name, 0);
            AV67GXV2 = (int)(AV67GXV2+1);
         }
         lblTbenabledrepo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbenabledrepo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbenabledrepo_Visible), 5, 0)));
         chkavIsenabledinrepository.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenabledinrepository_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsenabledinrepository.Visible), 5, 0)));
         bttBtnenableuserinrepo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenableuserinrepo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenableuserinrepo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            cmbavAuthenticationtypename.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAuthenticationtypename_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAuthenticationtypename.Enabled), 5, 0)));
            AV7AuthenticationTypeName = "local";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AuthenticationTypeName", AV7AuthenticationTypeName);
            lblTbgo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbgo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbgo_Visible), 5, 0)));
            imgTburlimage_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTburlimage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTburlimage_Visible), 5, 0)));
            lblTburlprofile_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTburlprofile_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTburlprofile_Visible), 5, 0)));
            imgavImage_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavImage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavImage_Visible), 5, 0)));
            edtavUrlprofile_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUrlprofile_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUrlprofile_Visible), 5, 0)));
            edtavDatelastauthentication_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatelastauthentication_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatelastauthentication_Visible), 5, 0)));
            lblTblastauthentication_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTblastauthentication_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTblastauthentication_Visible), 5, 0)));
            AV10AuthTypeId = AV6AuthenticationType.gettypebyname(AV7AuthenticationTypeName, out  AV17Errors);
            if ( StringUtil.StrCmp(AV10AuthTypeId, "GAMLocal") == 0 )
            {
               tblTablepassword_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablepassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablepassword_Visible), 5, 0)));
            }
            else
            {
               tblTablepassword_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablepassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablepassword_Visible), 5, 0)));
            }
            AV38Repository = new SdtGAMRepository(context).get();
            AV54UserNameSpace = AV38Repository.gxTpr_Namespace;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54UserNameSpace", AV54UserNameSpace);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERNAMESPACE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV54UserNameSpace, ""))));
         }
         else
         {
            AV52User.load( AV53UserId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserId", AV53UserId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV53UserId, ""))));
            cmbavAuthenticationtypename.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAuthenticationtypename_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAuthenticationtypename.Enabled), 5, 0)));
            AV7AuthenticationTypeName = AV52User.gxTpr_Authenticationtypename;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AuthenticationTypeName", AV7AuthenticationTypeName);
            lblTbenabledrepo_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbenabledrepo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbenabledrepo_Visible), 5, 0)));
            lblTbgo_Linktarget = "_blank";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbgo_Internalname, "Linktarget", lblTbgo_Linktarget);
            lblTbgo_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbgo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbgo_Visible), 5, 0)));
            imgTburlimage_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTburlimage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTburlimage_Visible), 5, 0)));
            lblTburlprofile_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTburlprofile_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTburlprofile_Visible), 5, 0)));
            imgavImage_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavImage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavImage_Visible), 5, 0)));
            edtavUrlprofile_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUrlprofile_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUrlprofile_Visible), 5, 0)));
            edtavDatelastauthentication_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatelastauthentication_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatelastauthentication_Visible), 5, 0)));
            lblTblastauthentication_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTblastauthentication_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTblastauthentication_Visible), 5, 0)));
            tblTablepassword_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablepassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablepassword_Visible), 5, 0)));
            AV10AuthTypeId = AV6AuthenticationType.gettypebyname(AV7AuthenticationTypeName, out  AV17Errors);
            if ( StringUtil.StrCmp(AV10AuthTypeId, "GAMLocal") == 0 )
            {
               edtavName_Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            }
            else
            {
               edtavName_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            }
            AV53UserId = AV52User.gxTpr_Guid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserId", AV53UserId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV53UserId, ""))));
            AV54UserNameSpace = AV52User.gxTpr_Namespace;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54UserNameSpace", AV54UserNameSpace);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERNAMESPACE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV54UserNameSpace, ""))));
            AV33Name = AV52User.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Name", AV33Name);
            AV15EMail = AV52User.gxTpr_Email;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15EMail", AV15EMail);
            AV20FirstName = AV52User.gxTpr_Firstname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FirstName", AV20FirstName);
            AV28LastName = AV52User.gxTpr_Lastname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LastName", AV28LastName);
            AV22Image = AV52User.gxTpr_Urlimage;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavImage_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV22Image)) ? AV68Image_GXI : context.convertURL( context.PathToRelativeUrl( AV22Image))));
            AV68Image_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV52User.gxTpr_Urlimage);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavImage_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV22Image)) ? AV68Image_GXI : context.convertURL( context.PathToRelativeUrl( AV22Image))));
            AV51URLProfile = AV52User.gxTpr_Urlprofile;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51URLProfile", AV51URLProfile);
            lblTbgo_Link = AV51URLProfile;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbgo_Internalname, "Link", lblTbgo_Link);
            AV18ExternalId = AV52User.gxTpr_Externalid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ExternalId", AV18ExternalId);
            AV11Birthday = AV52User.gxTpr_Birthday;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Birthday", context.localUtil.Format(AV11Birthday, "99/99/9999"));
            AV21Gender = AV52User.gxTpr_Gender;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Gender", AV21Gender);
            AV23IsActive = AV52User.gxTpr_Isactive;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23IsActive", AV23IsActive);
            AV5ActivationDate = AV52User.gxTpr_Activationdate;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ActivationDate", context.localUtil.TToC( AV5ActivationDate, 10, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vACTIVATIONDATE", GetSecureSignedToken( "", context.localUtil.Format( AV5ActivationDate, "99/99/9999 99:99")));
            AV14DontReceiveInformation = AV52User.gxTpr_Dontreceiveinformation;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DontReceiveInformation", AV14DontReceiveInformation);
            AV12CannotChangePassword = AV52User.gxTpr_Cannotchangepassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12CannotChangePassword", AV12CannotChangePassword);
            AV32MustChangePassword = AV52User.gxTpr_Mustchangepassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32MustChangePassword", AV32MustChangePassword);
            AV37PasswordNeverExpires = AV52User.gxTpr_Passwordneverexpires;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37PasswordNeverExpires", AV37PasswordNeverExpires);
            AV24IsBlocked = AV52User.gxTpr_Isblocked;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24IsBlocked", AV24IsBlocked);
            AV49SecurityPolicyId = AV52User.gxTpr_Securitypolicyid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49SecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0)));
            AV25IsEnabledInRepository = AV52User.gxTpr_Isenabledinrepository;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25IsEnabledInRepository", AV25IsEnabledInRepository);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vISENABLEDINREPOSITORY", GetSecureSignedToken( "", AV25IsEnabledInRepository));
            AV13DateLastAuthentication = AV52User.gxTpr_Datelastauthentication;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13DateLastAuthentication", context.localUtil.TToC( AV13DateLastAuthentication, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATELASTAUTHENTICATION", GetSecureSignedToken( "", context.localUtil.Format( AV13DateLastAuthentication, "99/99/99 99:99")));
            bttBtnenableuserinrepo_Caption = (AV25IsEnabledInRepository ? "Disable" : "Enable");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenableuserinrepo_Internalname, "Caption", bttBtnenableuserinrepo_Caption);
         }
         if ( AV23IsActive )
         {
            chkavIsactive.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsactive_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsactive.Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            lblTbactivated_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbactivated_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbactivated_Visible), 5, 0)));
            chkavIsactive.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsactive_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsactive.Visible), 5, 0)));
            edtavActivationdate_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavActivationdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavActivationdate_Visible), 5, 0)));
         }
         else
         {
            lblTbactivated_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbactivated_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbactivated_Visible), 5, 0)));
            chkavIsactive.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsactive_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsactive.Visible), 5, 0)));
            edtavActivationdate_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavActivationdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavActivationdate_Visible), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            bttBtnenableuserinrepo_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenableuserinrepo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenableuserinrepo_Visible), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            edtavEmail_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
            edtavFirstname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFirstname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFirstname_Enabled), 5, 0)));
            edtavLastname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLastname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLastname_Enabled), 5, 0)));
            edtavUrlprofile_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUrlprofile_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUrlprofile_Enabled), 5, 0)));
            edtavExternalid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavExternalid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavExternalid_Enabled), 5, 0)));
            edtavBirthday_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBirthday_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBirthday_Enabled), 5, 0)));
            cmbavGender.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavGender.Enabled), 5, 0)));
            chkavIsactive.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsactive_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsactive.Enabled), 5, 0)));
            chkavDontreceiveinformation.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDontreceiveinformation_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDontreceiveinformation.Enabled), 5, 0)));
            chkavCannotchangepassword.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCannotchangepassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCannotchangepassword.Enabled), 5, 0)));
            chkavMustchangepassword.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavMustchangepassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavMustchangepassword.Enabled), 5, 0)));
            chkavIsblocked.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsblocked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsblocked.Enabled), 5, 0)));
            chkavPasswordneverexpires.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPasswordneverexpires_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavPasswordneverexpires.Enabled), 5, 0)));
            cmbavSecuritypolicyid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSecuritypolicyid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSecuritypolicyid.Enabled), 5, 0)));
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12232 */
         E12232 ();
         if (returnInSub) return;
      }

      protected void E12232( )
      {
         /* Enter Routine */
         AV52User.gxTpr_Guid = AV53UserId;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
         AV36PasswordIsOK = true;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               AV10AuthTypeId = AV6AuthenticationType.gettypebyname(AV7AuthenticationTypeName, out  AV17Errors);
               if ( StringUtil.StrCmp(AV10AuthTypeId, "GAMLocal") == 0 )
               {
                  if ( StringUtil.StrCmp(AV34Password, AV35PasswordConf) != 0 )
                  {
                     AV36PasswordIsOK = false;
                     GX_msglist.addItem("The password and confirmation password do not match.");
                  }
               }
               else
               {
                  AV34Password = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Password", AV34Password);
               }
            }
            if ( AV36PasswordIsOK )
            {
               AV52User.gxTpr_Authenticationtypename = AV7AuthenticationTypeName;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Name = AV33Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Email = AV15EMail;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Firstname = AV20FirstName;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Lastname = AV28LastName;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Password = AV34Password;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Externalid = AV18ExternalId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Birthday = AV11Birthday;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Gender = AV21Gender;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Isactive = AV23IsActive;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Dontreceiveinformation = AV14DontReceiveInformation;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Cannotchangepassword = AV12CannotChangePassword;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Mustchangepassword = AV32MustChangePassword;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Isblocked = AV24IsBlocked;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Passwordneverexpires = AV37PasswordNeverExpires;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.gxTpr_Securitypolicyid = AV49SecurityPolicyId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52User", AV52User);
               AV52User.save();
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV52User.delete();
         }
         if ( AV36PasswordIsOK )
         {
            if ( AV52User.success() )
            {
               context.CommitDataStores( "GAMExampleEntryUser");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV53UserId});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
            else
            {
               AV17Errors = AV52User.geterrors();
               AV69GXV3 = 1;
               while ( AV69GXV3 <= AV17Errors.Count )
               {
                  AV16Error = ((SdtGAMError)AV17Errors.Item(AV69GXV3));
                  GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV16Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
                  AV69GXV3 = (int)(AV69GXV3+1);
               }
            }
         }
      }

      protected void E13232( )
      {
         /* 'EnableDisable' Routine */
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            if ( AV25IsEnabledInRepository )
            {
               AV26isOK = AV52User.repositorydisable(out  AV17Errors);
            }
            else
            {
               AV26isOK = AV52User.repositoryenable(out  AV17Errors);
            }
            if ( ! AV26isOK )
            {
               AV70GXV4 = 1;
               while ( AV70GXV4 <= AV17Errors.Count )
               {
                  AV16Error = ((SdtGAMError)AV17Errors.Item(AV70GXV4));
                  GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV16Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
                  AV70GXV4 = (int)(AV70GXV4+1);
               }
            }
            else
            {
               context.CommitDataStores( "GAMExampleEntryUser");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV53UserId});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E14232( )
      {
         /* Authenticationtypename_Isvalid Routine */
         AV10AuthTypeId = AV6AuthenticationType.gettypebyname(AV7AuthenticationTypeName, out  AV17Errors);
         if ( StringUtil.StrCmp(AV10AuthTypeId, "GAMLocal") == 0 )
         {
            tblTablepassword_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablepassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablepassword_Visible), 5, 0)));
         }
         else
         {
            tblTablepassword_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablepassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablepassword_Visible), 5, 0)));
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E15232( )
      {
         /* Load Routine */
      }

      protected void wb_table3_177_232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablebtns_Internalname, tblTablebtns_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:20px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqicon_Internalname, AV43ReqIcon);
            ClassString = "Image";
            StyleString = "";
            AV43ReqIcon_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV43ReqIcon))&&String.IsNullOrEmpty(StringUtil.RTrim( AV65Reqicon_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV43ReqIcon)));
            GxWebStd.gx_bitmap( context, imgavReqicon_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV43ReqIcon)) ? AV65Reqicon_GXI : context.PathToRelativeUrl( AV43ReqIcon)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV43ReqIcon_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:230px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinficonreq_Internalname, "Information required.", "", "", lblTbinficonreq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:8.0pt; font-weight:normal; font-style:normal;", "Label", 0, "", lblTbinficonreq_Visible, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:350px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_188_232( true) ;
         }
         else
         {
            wb_table4_188_232( false) ;
         }
         return  ;
      }

      protected void wb_table4_188_232e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_177_232e( true) ;
         }
         else
         {
            wb_table3_177_232e( false) ;
         }
      }

      protected void wb_table4_188_232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 191,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_188_232e( true) ;
         }
         else
         {
            wb_table4_188_232e( false) ;
         }
      }

      protected void wb_table2_158_232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablepassword_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablepassword_Internalname, tblTablepassword_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:20px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqpassword_Internalname, AV46ReqPassword);
            ClassString = "Image";
            StyleString = "";
            AV46ReqPassword_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Reqpassword_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword)));
            GxWebStd.gx_bitmap( context, imgavReqpassword_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword)) ? AV64Reqpassword_GXI : context.PathToRelativeUrl( AV46ReqPassword)), "", "", "", context.GetTheme( ), imgavReqpassword_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV46ReqPassword_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:230px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpwd_Internalname, "Password", "", "", lblTbpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:400px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPassword_Internalname, StringUtil.RTrim( AV34Password), StringUtil.RTrim( context.localUtil.Format( AV34Password, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqpassword_Internalname, AV46ReqPassword);
            ClassString = "Image";
            StyleString = "";
            AV46ReqPassword_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Reqpassword_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword)));
            GxWebStd.gx_bitmap( context, imgavReqpassword_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV46ReqPassword)) ? AV64Reqpassword_GXI : context.PathToRelativeUrl( AV46ReqPassword)), "", "", "", context.GetTheme( ), imgavReqpassword_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV46ReqPassword_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpwdconf_Internalname, "Password confirmation", "", "", lblTbpwdconf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:8px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPasswordconf_Internalname, StringUtil.RTrim( AV35PasswordConf), StringUtil.RTrim( context.localUtil.Format( AV35PasswordConf, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,172);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPasswordconf_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:3px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_158_232e( true) ;
         }
         else
         {
            wb_table2_158_232e( false) ;
         }
      }

      protected void wb_table1_3_232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:20px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "width:230px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbguid_Internalname, "GUID", "", "", lblTbguid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "width:400px")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavUserid_Internalname, StringUtil.RTrim( AV53UserId), StringUtil.RTrim( context.localUtil.Format( AV53UserId, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUserid_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnamespace_Internalname, "Namespace", "", "", lblTbnamespace_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsernamespace_Internalname, StringUtil.RTrim( AV54UserNameSpace), StringUtil.RTrim( context.localUtil.Format( AV54UserNameSpace, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsernamespace_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUsernamespace_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, 0, true, "GAMRepositoryNameSpace", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnauthenticationtype_Internalname, "Authentication type", "", "", lblTbnauthenticationtype_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAuthenticationtypename, cmbavAuthenticationtypename_Internalname, StringUtil.RTrim( AV7AuthenticationTypeName), 1, cmbavAuthenticationtypename_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavAuthenticationtypename.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_GAMExampleEntryUser.htm");
            cmbavAuthenticationtypename.CurrentValue = StringUtil.RTrim( AV7AuthenticationTypeName);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAuthenticationtypename_Internalname, "Values", (String)(cmbavAuthenticationtypename.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqname_Internalname, AV45ReqName);
            ClassString = "Image";
            StyleString = "";
            AV45ReqName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV45ReqName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV58Reqname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV45ReqName)));
            GxWebStd.gx_bitmap( context, imgavReqname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV45ReqName)) ? AV58Reqname_GXI : context.PathToRelativeUrl( AV45ReqName)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV45ReqName_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname2_Internalname, "User Name", "", "", lblTbname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, AV33Name, StringUtil.RTrim( context.localUtil.Format( AV33Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqemail_Internalname, AV40ReqEmail);
            ClassString = "Image";
            StyleString = "";
            AV40ReqEmail_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV40ReqEmail))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Reqemail_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV40ReqEmail)));
            GxWebStd.gx_bitmap( context, imgavReqemail_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV40ReqEmail)) ? AV59Reqemail_GXI : context.PathToRelativeUrl( AV40ReqEmail)), "", "", "", context.GetTheme( ), imgavReqemail_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV40ReqEmail_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbemail_Internalname, "Email", "", "", lblTbemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_Internalname, AV15EMail, StringUtil.RTrim( context.localUtil.Format( AV15EMail, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_Jsonclick, 0, "Attribute", "", "", "", 1, edtavEmail_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMEMail", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqfirstname_Internalname, AV41ReqFirstName);
            ClassString = "Image";
            StyleString = "";
            AV41ReqFirstName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV41ReqFirstName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV60Reqfirstname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV41ReqFirstName)));
            GxWebStd.gx_bitmap( context, imgavReqfirstname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV41ReqFirstName)) ? AV60Reqfirstname_GXI : context.PathToRelativeUrl( AV41ReqFirstName)), "", "", "", context.GetTheme( ), imgavReqfirstname_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV41ReqFirstName_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfirstname_Internalname, "First Name", "", "", lblTbfirstname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFirstname_Internalname, StringUtil.RTrim( AV20FirstName), StringUtil.RTrim( context.localUtil.Format( AV20FirstName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFirstname_Jsonclick, 0, "Attribute", "", "", "", 1, edtavFirstname_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqlastname_Internalname, AV44ReqLastName);
            ClassString = "Image";
            StyleString = "";
            AV44ReqLastName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV44ReqLastName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Reqlastname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV44ReqLastName)));
            GxWebStd.gx_bitmap( context, imgavReqlastname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV44ReqLastName)) ? AV61Reqlastname_GXI : context.PathToRelativeUrl( AV44ReqLastName)), "", "", "", context.GetTheme( ), imgavReqlastname_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV44ReqLastName_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblastname_Internalname, "Last Name", "", "", lblTblastname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLastname_Internalname, StringUtil.RTrim( AV28LastName), StringUtil.RTrim( context.localUtil.Format( AV28LastName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLastname_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLastname_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:8px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbexternalid_Internalname, "External Id.", "", "", lblTbexternalid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavExternalid_Internalname, AV18ExternalId, StringUtil.RTrim( context.localUtil.Format( AV18ExternalId, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavExternalid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavExternalid_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqbirthday_Internalname, AV39ReqBirthday);
            ClassString = "Image";
            StyleString = "";
            AV39ReqBirthday_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV39ReqBirthday))&&String.IsNullOrEmpty(StringUtil.RTrim( AV62Reqbirthday_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV39ReqBirthday)));
            GxWebStd.gx_bitmap( context, imgavReqbirthday_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV39ReqBirthday)) ? AV62Reqbirthday_GXI : context.PathToRelativeUrl( AV39ReqBirthday)), "", "", "", context.GetTheme( ), imgavReqbirthday_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV39ReqBirthday_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbbirthday_Internalname, "Birthday", "", "", lblTbbirthday_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavBirthday_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavBirthday_Internalname, context.localUtil.Format(AV11Birthday, "99/99/9999"), context.localUtil.Format( AV11Birthday, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBirthday_Jsonclick, 0, "Attribute", "", "", "", 1, edtavBirthday_Enabled, 1, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "GAMDate", "right", false, "HLP_GAMExampleEntryUser.htm");
            GxWebStd.gx_bitmap( context, edtavBirthday_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavBirthday_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqgender_Internalname, AV42ReqGender);
            ClassString = "Image";
            StyleString = "";
            AV42ReqGender_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV42ReqGender))&&String.IsNullOrEmpty(StringUtil.RTrim( AV63Reqgender_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV42ReqGender)));
            GxWebStd.gx_bitmap( context, imgavReqgender_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV42ReqGender)) ? AV63Reqgender_GXI : context.PathToRelativeUrl( AV42ReqGender)), "", "", "", context.GetTheme( ), imgavReqgender_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV42ReqGender_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgender_Internalname, "Gender", "", "", lblTbgender_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGender, cmbavGender_Internalname, StringUtil.RTrim( AV21Gender), 1, cmbavGender_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavGender.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_GAMExampleEntryUser.htm");
            cmbavGender.CurrentValue = StringUtil.RTrim( AV21Gender);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Values", (String)(cmbavGender.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:3px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, imgTburlimage_Internalname, "Image", "", "", imgTburlimage_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", imgTburlimage_Visible, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavImage_Internalname, AV22Image);
            ClassString = "Image";
            StyleString = "";
            AV22Image_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV22Image))&&String.IsNullOrEmpty(StringUtil.RTrim( AV68Image_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV22Image)));
            GxWebStd.gx_bitmap( context, imgavImage_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV22Image)) ? AV68Image_GXI : context.PathToRelativeUrl( AV22Image)), "", "", "", context.GetTheme( ), imgavImage_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV22Image_IsBlob, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTburlprofile_Internalname, "URL Profile", "", "", lblTburlprofile_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTburlprofile_Visible, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgo_Internalname, "Go", lblTbgo_Link, lblTbgo_Linktarget, lblTbgo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal; text-decoration:underline; color:#0000FF;", "TextBlock", 0, "", lblTbgo_Visible, 1, 1, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUrlprofile_Internalname, AV51URLProfile, StringUtil.RTrim( context.localUtil.Format( AV51URLProfile, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUrlprofile_Jsonclick, 0, "Attribute", "", "", "", edtavUrlprofile_Visible, edtavUrlprofile_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:3px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbactivated_Internalname, "Account is active?", "", "", lblTbactivated_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbactivated_Visible, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIsactive_Internalname, StringUtil.BoolToStr( AV23IsActive), "", "", chkavIsactive.Visible, chkavIsactive.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavActivationdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavActivationdate_Internalname, context.localUtil.TToC( AV5ActivationDate, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV5ActivationDate, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavActivationdate_Jsonclick, 0, "Attribute", "", "", "", edtavActivationdate_Visible, edtavActivationdate_Enabled, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "GAMDateTime", "right", false, "HLP_GAMExampleEntryUser.htm");
            GxWebStd.gx_bitmap( context, edtavActivationdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavActivationdate_Visible==0)||(edtavActivationdate_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnotrecibeinf_Internalname, "Don't want to receive information", "", "", lblTbnotrecibeinf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDontreceiveinformation_Internalname, StringUtil.BoolToStr( AV14DontReceiveInformation), "", "", 1, chkavDontreceiveinformation.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcannotchangepwd_Internalname, "Cannot change password", "", "", lblTbcannotchangepwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCannotchangepassword_Internalname, StringUtil.BoolToStr( AV12CannotChangePassword), "", "", 1, chkavCannotchangepassword.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(115, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbmustchangepwd_Internalname, "Must change password", "", "", lblTbmustchangepwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavMustchangepassword_Internalname, StringUtil.BoolToStr( AV32MustChangePassword), "", "", 1, chkavMustchangepassword.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(121, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpasswordneverexpires_Internalname, "Password never expires", "", "", lblTbpasswordneverexpires_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavPasswordneverexpires_Internalname, StringUtil.BoolToStr( AV37PasswordNeverExpires), "", "", 1, chkavPasswordneverexpires.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(127, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:24px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbisblocked_Internalname, "User is blocked", "", "", lblTbisblocked_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIsblocked_Internalname, StringUtil.BoolToStr( AV24IsBlocked), "", "", 1, chkavIsblocked.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(133, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsecuritypolicy_Internalname, "Security Policy", "", "", lblTbsecuritypolicy_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSecuritypolicyid, cmbavSecuritypolicyid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0)), 1, cmbavSecuritypolicyid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavSecuritypolicyid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", "", true, "HLP_GAMExampleEntryUser.htm");
            cmbavSecuritypolicyid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV49SecurityPolicyId), 9, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSecuritypolicyid_Internalname, "Values", (String)(cmbavSecuritypolicyid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbenabledrepo_Internalname, "Enabled in repository", "", "", lblTbenabledrepo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbenabledrepo_Visible, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIsenabledinrepository_Internalname, StringUtil.BoolToStr( AV25IsEnabledInRepository), "", "", chkavIsenabledinrepository.Visible, chkavIsenabledinrepository.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(145, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenableuserinrepo_Internalname, "", bttBtnenableuserinrepo_Caption, bttBtnenableuserinrepo_Jsonclick, 5, "Enable", "", StyleString, ClassString, bttBtnenableuserinrepo_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENABLEDISABLE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblastauthentication_Internalname, "Last authentication", "", "", lblTblastauthentication_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTblastauthentication_Visible, 1, 0, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatelastauthentication_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatelastauthentication_Internalname, context.localUtil.TToC( AV13DateLastAuthentication, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV13DateLastAuthentication, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatelastauthentication_Jsonclick, 0, "Attribute", "", "", "", edtavDatelastauthentication_Visible, edtavDatelastauthentication_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntryUser.htm");
            GxWebStd.gx_bitmap( context, edtavDatelastauthentication_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavDatelastauthentication_Visible==0)||(edtavDatelastauthentication_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_GAMExampleEntryUser.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:3px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_232e( true) ;
         }
         else
         {
            wb_table1_3_232e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV53UserId = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserId", AV53UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV53UserId, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA232( ) ;
         WS232( ) ;
         WE232( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823115476");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleentryuser.js", "?202042823115477");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbguid_Internalname = "TBGUID";
         edtavUserid_Internalname = "vUSERID";
         lblTbnamespace_Internalname = "TBNAMESPACE";
         edtavUsernamespace_Internalname = "vUSERNAMESPACE";
         lblTbnauthenticationtype_Internalname = "TBNAUTHENTICATIONTYPE";
         cmbavAuthenticationtypename_Internalname = "vAUTHENTICATIONTYPENAME";
         imgavReqname_Internalname = "vREQNAME";
         lblTbname2_Internalname = "TBNAME2";
         edtavName_Internalname = "vNAME";
         imgavReqemail_Internalname = "vREQEMAIL";
         lblTbemail_Internalname = "TBEMAIL";
         edtavEmail_Internalname = "vEMAIL";
         imgavReqfirstname_Internalname = "vREQFIRSTNAME";
         lblTbfirstname_Internalname = "TBFIRSTNAME";
         edtavFirstname_Internalname = "vFIRSTNAME";
         imgavReqlastname_Internalname = "vREQLASTNAME";
         lblTblastname_Internalname = "TBLASTNAME";
         edtavLastname_Internalname = "vLASTNAME";
         lblTbexternalid_Internalname = "TBEXTERNALID";
         edtavExternalid_Internalname = "vEXTERNALID";
         imgavReqbirthday_Internalname = "vREQBIRTHDAY";
         lblTbbirthday_Internalname = "TBBIRTHDAY";
         edtavBirthday_Internalname = "vBIRTHDAY";
         imgavReqgender_Internalname = "vREQGENDER";
         lblTbgender_Internalname = "TBGENDER";
         cmbavGender_Internalname = "vGENDER";
         imgTburlimage_Internalname = "TBURLIMAGE";
         imgavImage_Internalname = "vIMAGE";
         lblTburlprofile_Internalname = "TBURLPROFILE";
         lblTbgo_Internalname = "TBGO";
         edtavUrlprofile_Internalname = "vURLPROFILE";
         lblTbactivated_Internalname = "TBACTIVATED";
         chkavIsactive_Internalname = "vISACTIVE";
         edtavActivationdate_Internalname = "vACTIVATIONDATE";
         lblTbnotrecibeinf_Internalname = "TBNOTRECIBEINF";
         chkavDontreceiveinformation_Internalname = "vDONTRECEIVEINFORMATION";
         lblTbcannotchangepwd_Internalname = "TBCANNOTCHANGEPWD";
         chkavCannotchangepassword_Internalname = "vCANNOTCHANGEPASSWORD";
         lblTbmustchangepwd_Internalname = "TBMUSTCHANGEPWD";
         chkavMustchangepassword_Internalname = "vMUSTCHANGEPASSWORD";
         lblTbpasswordneverexpires_Internalname = "TBPASSWORDNEVEREXPIRES";
         chkavPasswordneverexpires_Internalname = "vPASSWORDNEVEREXPIRES";
         lblTbisblocked_Internalname = "TBISBLOCKED";
         chkavIsblocked_Internalname = "vISBLOCKED";
         lblTbsecuritypolicy_Internalname = "TBSECURITYPOLICY";
         cmbavSecuritypolicyid_Internalname = "vSECURITYPOLICYID";
         lblTbenabledrepo_Internalname = "TBENABLEDREPO";
         chkavIsenabledinrepository_Internalname = "vISENABLEDINREPOSITORY";
         bttBtnenableuserinrepo_Internalname = "BTNENABLEUSERINREPO";
         lblTblastauthentication_Internalname = "TBLASTAUTHENTICATION";
         edtavDatelastauthentication_Internalname = "vDATELASTAUTHENTICATION";
         tblTable1_Internalname = "TABLE1";
         imgavReqpassword_Internalname = "vREQPASSWORD";
         lblTbpwd_Internalname = "TBPWD";
         edtavPassword_Internalname = "vPASSWORD";
         imgavReqpassword_Internalname = "vREQPASSWORD";
         lblTbpwdconf_Internalname = "TBPWDCONF";
         edtavPasswordconf_Internalname = "vPASSWORDCONF";
         tblTablepassword_Internalname = "TABLEPASSWORD";
         imgavReqicon_Internalname = "vREQICON";
         lblTbinficonreq_Internalname = "TBINFICONREQ";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         tblTablebtns_Internalname = "TABLEBTNS";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavDatelastauthentication_Jsonclick = "";
         edtavDatelastauthentication_Enabled = 1;
         lblTblastauthentication_Visible = 1;
         bttBtnenableuserinrepo_Visible = 1;
         chkavIsenabledinrepository.Enabled = 1;
         lblTbenabledrepo_Visible = 1;
         cmbavSecuritypolicyid_Jsonclick = "";
         edtavActivationdate_Jsonclick = "";
         edtavActivationdate_Enabled = 1;
         lblTbactivated_Visible = 1;
         edtavUrlprofile_Jsonclick = "";
         lblTbgo_Linktarget = "";
         lblTbgo_Link = "";
         lblTbgo_Visible = 1;
         lblTburlprofile_Visible = 1;
         imgTburlimage_Visible = 1;
         cmbavGender_Jsonclick = "";
         edtavBirthday_Jsonclick = "";
         edtavExternalid_Jsonclick = "";
         edtavLastname_Jsonclick = "";
         edtavFirstname_Jsonclick = "";
         edtavEmail_Jsonclick = "";
         edtavName_Jsonclick = "";
         cmbavAuthenticationtypename_Jsonclick = "";
         edtavUsernamespace_Jsonclick = "";
         edtavUsernamespace_Enabled = 1;
         edtavUserid_Jsonclick = "";
         edtavUserid_Enabled = 0;
         edtavPasswordconf_Jsonclick = "";
         edtavPassword_Jsonclick = "";
         bttBtnconfirm_Visible = 1;
         lblTbinficonreq_Visible = 1;
         bttBtnconfirm_Caption = "Confirmar";
         cmbavSecuritypolicyid.Enabled = 1;
         chkavPasswordneverexpires.Enabled = 1;
         chkavIsblocked.Enabled = 1;
         chkavMustchangepassword.Enabled = 1;
         chkavCannotchangepassword.Enabled = 1;
         chkavDontreceiveinformation.Enabled = 1;
         cmbavGender.Enabled = 1;
         edtavBirthday_Enabled = 1;
         edtavExternalid_Enabled = 1;
         edtavUrlprofile_Enabled = 1;
         edtavLastname_Enabled = 1;
         edtavFirstname_Enabled = 1;
         edtavEmail_Enabled = 1;
         edtavActivationdate_Visible = 1;
         chkavIsactive.Visible = 1;
         chkavIsactive.Enabled = 1;
         bttBtnenableuserinrepo_Caption = "Enable";
         edtavName_Enabled = 1;
         tblTablepassword_Visible = 1;
         edtavDatelastauthentication_Visible = 1;
         edtavUrlprofile_Visible = 1;
         imgavImage_Visible = 1;
         cmbavAuthenticationtypename.Enabled = 1;
         chkavIsenabledinrepository.Visible = 1;
         imgavReqpassword_Visible = 1;
         imgavReqgender_Visible = 1;
         imgavReqbirthday_Visible = 1;
         imgavReqlastname_Visible = 1;
         imgavReqfirstname_Visible = 1;
         imgavReqemail_Visible = 1;
         chkavIsenabledinrepository.Caption = "";
         chkavIsblocked.Caption = "";
         chkavPasswordneverexpires.Caption = "";
         chkavMustchangepassword.Caption = "";
         chkavCannotchangepassword.Caption = "";
         chkavDontreceiveinformation.Caption = "";
         chkavIsactive.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "User ";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         wcpOAV53UserId = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV50String = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7AuthenticationTypeName = "";
         AV21Gender = "";
         AV54UserNameSpace = "";
         AV45ReqName = "";
         AV33Name = "";
         AV40ReqEmail = "";
         AV15EMail = "";
         AV41ReqFirstName = "";
         AV20FirstName = "";
         AV44ReqLastName = "";
         AV28LastName = "";
         AV18ExternalId = "";
         AV39ReqBirthday = "";
         AV11Birthday = DateTime.MinValue;
         AV42ReqGender = "";
         AV22Image = "";
         AV51URLProfile = "";
         AV5ActivationDate = (DateTime)(DateTime.MinValue);
         AV13DateLastAuthentication = (DateTime)(DateTime.MinValue);
         AV46ReqPassword = "";
         AV34Password = "";
         AV35PasswordConf = "";
         AV43ReqIcon = "";
         hsh = "";
         AV38Repository = new SdtGAMRepository(context);
         AV58Reqname_GXI = "";
         AV59Reqemail_GXI = "";
         AV60Reqfirstname_GXI = "";
         AV61Reqlastname_GXI = "";
         AV62Reqbirthday_GXI = "";
         AV63Reqgender_GXI = "";
         AV64Reqpassword_GXI = "";
         AV65Reqicon_GXI = "";
         AV8AuthenticationTypes = new GxExternalCollection( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV27Language = "";
         AV17Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV9AuthenticationTypesIns = new SdtGAMAuthenticationTypeSimple(context);
         AV47SecurityPolicies = new GxExternalCollection( context, "SdtGAMSecurityPolicy", "GeneXus.Programs");
         AV19FilterSecPol = new SdtGAMSecurityPolicyFilter(context);
         AV48SecurityPolicy = new SdtGAMSecurityPolicy(context);
         AV10AuthTypeId = "";
         AV6AuthenticationType = new SdtGAMAuthenticationType(context);
         AV52User = new SdtGAMUser(context);
         AV68Image_GXI = "";
         AV16Error = new SdtGAMError(context);
         sStyleString = "";
         lblTbinficonreq_Jsonclick = "";
         TempTags = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         lblTbpwd_Jsonclick = "";
         lblTbpwdconf_Jsonclick = "";
         lblTbguid_Jsonclick = "";
         lblTbnamespace_Jsonclick = "";
         lblTbnauthenticationtype_Jsonclick = "";
         lblTbname2_Jsonclick = "";
         lblTbemail_Jsonclick = "";
         lblTbfirstname_Jsonclick = "";
         lblTblastname_Jsonclick = "";
         lblTbexternalid_Jsonclick = "";
         lblTbbirthday_Jsonclick = "";
         lblTbgender_Jsonclick = "";
         imgTburlimage_Jsonclick = "";
         lblTburlprofile_Jsonclick = "";
         lblTbgo_Jsonclick = "";
         lblTbactivated_Jsonclick = "";
         lblTbnotrecibeinf_Jsonclick = "";
         lblTbcannotchangepwd_Jsonclick = "";
         lblTbmustchangepwd_Jsonclick = "";
         lblTbpasswordneverexpires_Jsonclick = "";
         lblTbisblocked_Jsonclick = "";
         lblTbsecuritypolicy_Jsonclick = "";
         lblTbenabledrepo_Jsonclick = "";
         bttBtnenableuserinrepo_Jsonclick = "";
         lblTblastauthentication_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleentryuser__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUserid_Enabled = 0;
         edtavUsernamespace_Enabled = 0;
         edtavActivationdate_Enabled = 0;
         chkavIsenabledinrepository.Enabled = 0;
         edtavDatelastauthentication_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV49SecurityPolicyId ;
      private int edtavUserid_Enabled ;
      private int edtavUsernamespace_Enabled ;
      private int edtavActivationdate_Enabled ;
      private int edtavDatelastauthentication_Enabled ;
      private int imgavReqemail_Visible ;
      private int imgavReqfirstname_Visible ;
      private int imgavReqlastname_Visible ;
      private int imgavReqbirthday_Visible ;
      private int imgavReqgender_Visible ;
      private int imgavReqpassword_Visible ;
      private int lblTbinficonreq_Visible ;
      private int AV66GXV1 ;
      private int AV67GXV2 ;
      private int lblTbenabledrepo_Visible ;
      private int bttBtnenableuserinrepo_Visible ;
      private int lblTbgo_Visible ;
      private int imgTburlimage_Visible ;
      private int lblTburlprofile_Visible ;
      private int imgavImage_Visible ;
      private int edtavUrlprofile_Visible ;
      private int edtavDatelastauthentication_Visible ;
      private int lblTblastauthentication_Visible ;
      private int tblTablepassword_Visible ;
      private int edtavName_Enabled ;
      private int lblTbactivated_Visible ;
      private int edtavActivationdate_Visible ;
      private int bttBtnconfirm_Visible ;
      private int edtavEmail_Enabled ;
      private int edtavFirstname_Enabled ;
      private int edtavLastname_Enabled ;
      private int edtavUrlprofile_Enabled ;
      private int edtavExternalid_Enabled ;
      private int edtavBirthday_Enabled ;
      private int AV69GXV3 ;
      private int AV70GXV4 ;
      private int idxLst ;
      private String Gx_mode ;
      private String AV53UserId ;
      private String wcpOGx_mode ;
      private String wcpOAV53UserId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String lblTbgo_Link ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV7AuthenticationTypeName ;
      private String AV21Gender ;
      private String chkavIsactive_Internalname ;
      private String chkavDontreceiveinformation_Internalname ;
      private String chkavCannotchangepassword_Internalname ;
      private String chkavMustchangepassword_Internalname ;
      private String chkavPasswordneverexpires_Internalname ;
      private String chkavIsblocked_Internalname ;
      private String chkavIsenabledinrepository_Internalname ;
      private String edtavUsernamespace_Internalname ;
      private String edtavUserid_Internalname ;
      private String edtavActivationdate_Internalname ;
      private String edtavDatelastauthentication_Internalname ;
      private String AV54UserNameSpace ;
      private String cmbavAuthenticationtypename_Internalname ;
      private String imgavReqname_Internalname ;
      private String edtavName_Internalname ;
      private String imgavReqemail_Internalname ;
      private String edtavEmail_Internalname ;
      private String imgavReqfirstname_Internalname ;
      private String AV20FirstName ;
      private String edtavFirstname_Internalname ;
      private String imgavReqlastname_Internalname ;
      private String AV28LastName ;
      private String edtavLastname_Internalname ;
      private String edtavExternalid_Internalname ;
      private String imgavReqbirthday_Internalname ;
      private String edtavBirthday_Internalname ;
      private String imgavReqgender_Internalname ;
      private String cmbavGender_Internalname ;
      private String imgavImage_Internalname ;
      private String edtavUrlprofile_Internalname ;
      private String cmbavSecuritypolicyid_Internalname ;
      private String imgavReqpassword_Internalname ;
      private String AV34Password ;
      private String edtavPassword_Internalname ;
      private String AV35PasswordConf ;
      private String edtavPasswordconf_Internalname ;
      private String imgavReqicon_Internalname ;
      private String hsh ;
      private String lblTbinficonreq_Internalname ;
      private String AV27Language ;
      private String lblTbenabledrepo_Internalname ;
      private String bttBtnenableuserinrepo_Internalname ;
      private String lblTbgo_Internalname ;
      private String imgTburlimage_Internalname ;
      private String lblTburlprofile_Internalname ;
      private String lblTblastauthentication_Internalname ;
      private String AV10AuthTypeId ;
      private String tblTablepassword_Internalname ;
      private String lblTbgo_Linktarget ;
      private String bttBtnenableuserinrepo_Caption ;
      private String lblTbactivated_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String sStyleString ;
      private String tblTablebtns_Internalname ;
      private String lblTbinficonreq_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String TempTags ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private String lblTbpwd_Internalname ;
      private String lblTbpwd_Jsonclick ;
      private String edtavPassword_Jsonclick ;
      private String lblTbpwdconf_Internalname ;
      private String lblTbpwdconf_Jsonclick ;
      private String edtavPasswordconf_Jsonclick ;
      private String tblTable1_Internalname ;
      private String lblTbguid_Internalname ;
      private String lblTbguid_Jsonclick ;
      private String edtavUserid_Jsonclick ;
      private String lblTbnamespace_Internalname ;
      private String lblTbnamespace_Jsonclick ;
      private String edtavUsernamespace_Jsonclick ;
      private String lblTbnauthenticationtype_Internalname ;
      private String lblTbnauthenticationtype_Jsonclick ;
      private String cmbavAuthenticationtypename_Jsonclick ;
      private String lblTbname2_Internalname ;
      private String lblTbname2_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbemail_Internalname ;
      private String lblTbemail_Jsonclick ;
      private String edtavEmail_Jsonclick ;
      private String lblTbfirstname_Internalname ;
      private String lblTbfirstname_Jsonclick ;
      private String edtavFirstname_Jsonclick ;
      private String lblTblastname_Internalname ;
      private String lblTblastname_Jsonclick ;
      private String edtavLastname_Jsonclick ;
      private String lblTbexternalid_Internalname ;
      private String lblTbexternalid_Jsonclick ;
      private String edtavExternalid_Jsonclick ;
      private String lblTbbirthday_Internalname ;
      private String lblTbbirthday_Jsonclick ;
      private String edtavBirthday_Jsonclick ;
      private String lblTbgender_Internalname ;
      private String lblTbgender_Jsonclick ;
      private String cmbavGender_Jsonclick ;
      private String imgTburlimage_Jsonclick ;
      private String lblTburlprofile_Jsonclick ;
      private String lblTbgo_Jsonclick ;
      private String edtavUrlprofile_Jsonclick ;
      private String lblTbactivated_Jsonclick ;
      private String edtavActivationdate_Jsonclick ;
      private String lblTbnotrecibeinf_Internalname ;
      private String lblTbnotrecibeinf_Jsonclick ;
      private String lblTbcannotchangepwd_Internalname ;
      private String lblTbcannotchangepwd_Jsonclick ;
      private String lblTbmustchangepwd_Internalname ;
      private String lblTbmustchangepwd_Jsonclick ;
      private String lblTbpasswordneverexpires_Internalname ;
      private String lblTbpasswordneverexpires_Jsonclick ;
      private String lblTbisblocked_Internalname ;
      private String lblTbisblocked_Jsonclick ;
      private String lblTbsecuritypolicy_Internalname ;
      private String lblTbsecuritypolicy_Jsonclick ;
      private String cmbavSecuritypolicyid_Jsonclick ;
      private String lblTbenabledrepo_Jsonclick ;
      private String bttBtnenableuserinrepo_Jsonclick ;
      private String lblTblastauthentication_Jsonclick ;
      private String edtavDatelastauthentication_Jsonclick ;
      private DateTime AV5ActivationDate ;
      private DateTime AV13DateLastAuthentication ;
      private DateTime AV11Birthday ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV25IsEnabledInRepository ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV23IsActive ;
      private bool AV14DontReceiveInformation ;
      private bool AV12CannotChangePassword ;
      private bool AV32MustChangePassword ;
      private bool AV37PasswordNeverExpires ;
      private bool AV24IsBlocked ;
      private bool returnInSub ;
      private bool AV36PasswordIsOK ;
      private bool AV26isOK ;
      private bool AV43ReqIcon_IsBlob ;
      private bool AV46ReqPassword_IsBlob ;
      private bool AV45ReqName_IsBlob ;
      private bool AV40ReqEmail_IsBlob ;
      private bool AV41ReqFirstName_IsBlob ;
      private bool AV44ReqLastName_IsBlob ;
      private bool AV39ReqBirthday_IsBlob ;
      private bool AV42ReqGender_IsBlob ;
      private bool AV22Image_IsBlob ;
      private String AV50String ;
      private String AV33Name ;
      private String AV15EMail ;
      private String AV18ExternalId ;
      private String AV51URLProfile ;
      private String AV58Reqname_GXI ;
      private String AV59Reqemail_GXI ;
      private String AV60Reqfirstname_GXI ;
      private String AV61Reqlastname_GXI ;
      private String AV62Reqbirthday_GXI ;
      private String AV63Reqgender_GXI ;
      private String AV64Reqpassword_GXI ;
      private String AV65Reqicon_GXI ;
      private String AV68Image_GXI ;
      private String AV45ReqName ;
      private String AV40ReqEmail ;
      private String AV41ReqFirstName ;
      private String AV44ReqLastName ;
      private String AV39ReqBirthday ;
      private String AV42ReqGender ;
      private String AV22Image ;
      private String AV46ReqPassword ;
      private String AV43ReqIcon ;
      private SdtGAMAuthenticationType AV6AuthenticationType ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private String aP1_UserId ;
      private GXCombobox cmbavAuthenticationtypename ;
      private GXCombobox cmbavGender ;
      private GXCheckbox chkavIsactive ;
      private GXCheckbox chkavDontreceiveinformation ;
      private GXCheckbox chkavCannotchangepassword ;
      private GXCheckbox chkavMustchangepassword ;
      private GXCheckbox chkavPasswordneverexpires ;
      private GXCheckbox chkavIsblocked ;
      private GXCombobox cmbavSecuritypolicyid ;
      private GXCheckbox chkavIsenabledinrepository ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV17Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMAuthenticationTypeSimple ))]
      private IGxCollection AV8AuthenticationTypes ;
      [ObjectCollection(ItemType=typeof( SdtGAMSecurityPolicy ))]
      private IGxCollection AV47SecurityPolicies ;
      private GXWebForm Form ;
      private SdtGAMError AV16Error ;
      private SdtGAMAuthenticationTypeSimple AV9AuthenticationTypesIns ;
      private SdtGAMSecurityPolicyFilter AV19FilterSecPol ;
      private SdtGAMRepository AV38Repository ;
      private SdtGAMSecurityPolicy AV48SecurityPolicy ;
      private SdtGAMUser AV52User ;
   }

   public class gamexampleentryuser__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
