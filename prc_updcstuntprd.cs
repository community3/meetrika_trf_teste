/*
               File: PRC_UPDCstUntPrd
        Description: Update Custo unitario de produ��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:57:46.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updcstuntprd : GXProcedure
   {
      public prc_updcstuntprd( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updcstuntprd( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratadaUsuario_ContratadaCod ,
                           ref int aP1_ContratadaUsuario_UsuarioCod ,
                           decimal aP2_ContratadaUsuario_CstUntPrdNrm ,
                           decimal aP3_ContratadaUsuario_CstUntPrdExt )
      {
         this.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         this.A69ContratadaUsuario_UsuarioCod = aP1_ContratadaUsuario_UsuarioCod;
         this.AV10ContratadaUsuario_CstUntPrdNrm = aP2_ContratadaUsuario_CstUntPrdNrm;
         this.AV9ContratadaUsuario_CstUntPrdExt = aP3_ContratadaUsuario_CstUntPrdExt;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_ContratadaCod=this.A66ContratadaUsuario_ContratadaCod;
         aP1_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
      }

      public void executeSubmit( ref int aP0_ContratadaUsuario_ContratadaCod ,
                                 ref int aP1_ContratadaUsuario_UsuarioCod ,
                                 decimal aP2_ContratadaUsuario_CstUntPrdNrm ,
                                 decimal aP3_ContratadaUsuario_CstUntPrdExt )
      {
         prc_updcstuntprd objprc_updcstuntprd;
         objprc_updcstuntprd = new prc_updcstuntprd();
         objprc_updcstuntprd.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         objprc_updcstuntprd.A69ContratadaUsuario_UsuarioCod = aP1_ContratadaUsuario_UsuarioCod;
         objprc_updcstuntprd.AV10ContratadaUsuario_CstUntPrdNrm = aP2_ContratadaUsuario_CstUntPrdNrm;
         objprc_updcstuntprd.AV9ContratadaUsuario_CstUntPrdExt = aP3_ContratadaUsuario_CstUntPrdExt;
         objprc_updcstuntprd.context.SetSubmitInitialConfig(context);
         objprc_updcstuntprd.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updcstuntprd);
         aP0_ContratadaUsuario_ContratadaCod=this.A66ContratadaUsuario_ContratadaCod;
         aP1_ContratadaUsuario_UsuarioCod=this.A69ContratadaUsuario_UsuarioCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updcstuntprd)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005S2 */
         pr_default.execute(0, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A576ContratadaUsuario_CstUntPrdNrm = P005S2_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = P005S2_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = P005S2_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = P005S2_n577ContratadaUsuario_CstUntPrdExt[0];
            A576ContratadaUsuario_CstUntPrdNrm = AV10ContratadaUsuario_CstUntPrdNrm;
            n576ContratadaUsuario_CstUntPrdNrm = false;
            A577ContratadaUsuario_CstUntPrdExt = AV9ContratadaUsuario_CstUntPrdExt;
            n577ContratadaUsuario_CstUntPrdExt = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P005S3 */
            pr_default.execute(1, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            if (true) break;
            /* Using cursor P005S4 */
            pr_default.execute(2, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UPDCstUntPrd");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005S2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P005S2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P005S2_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P005S2_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P005S2_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         P005S2_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updcstuntprd__default(),
            new Object[][] {
                new Object[] {
               P005S2_A66ContratadaUsuario_ContratadaCod, P005S2_A69ContratadaUsuario_UsuarioCod, P005S2_A576ContratadaUsuario_CstUntPrdNrm, P005S2_n576ContratadaUsuario_CstUntPrdNrm, P005S2_A577ContratadaUsuario_CstUntPrdExt, P005S2_n577ContratadaUsuario_CstUntPrdExt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private decimal AV10ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV9ContratadaUsuario_CstUntPrdExt ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private String scmdbuf ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratadaUsuario_ContratadaCod ;
      private int aP1_ContratadaUsuario_UsuarioCod ;
      private IDataStoreProvider pr_default ;
      private int[] P005S2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P005S2_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] P005S2_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P005S2_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] P005S2_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] P005S2_n577ContratadaUsuario_CstUntPrdExt ;
   }

   public class prc_updcstuntprd__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005S2 ;
          prmP005S2 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005S3 ;
          prmP005S3 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005S4 ;
          prmP005S4 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005S2", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt] FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005S2,1,0,true,true )
             ,new CursorDef("P005S3", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005S3)
             ,new CursorDef("P005S4", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005S4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
       }
    }

 }

}
