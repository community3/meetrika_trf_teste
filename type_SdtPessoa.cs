/*
               File: type_SdtPessoa
        Description: Pessoas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:27:37.50
       Program type: HTTP procedure
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Pessoa" )]
   [XmlType(TypeName =  "Pessoa" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtPessoa : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPessoa( )
      {
         /* Constructor for serialization */
         gxTv_SdtPessoa_Pessoa_nome = "";
         gxTv_SdtPessoa_Pessoa_tipopessoa = "";
         gxTv_SdtPessoa_Pessoa_docto = "";
         gxTv_SdtPessoa_Pessoa_ie = "";
         gxTv_SdtPessoa_Pessoa_endereco = "";
         gxTv_SdtPessoa_Pessoa_uf = "";
         gxTv_SdtPessoa_Pessoa_cep = "";
         gxTv_SdtPessoa_Pessoa_telefone = "";
         gxTv_SdtPessoa_Pessoa_fax = "";
         gxTv_SdtPessoa_Mode = "";
         gxTv_SdtPessoa_Pessoa_nome_Z = "";
         gxTv_SdtPessoa_Pessoa_tipopessoa_Z = "";
         gxTv_SdtPessoa_Pessoa_docto_Z = "";
         gxTv_SdtPessoa_Pessoa_ie_Z = "";
         gxTv_SdtPessoa_Pessoa_endereco_Z = "";
         gxTv_SdtPessoa_Pessoa_uf_Z = "";
         gxTv_SdtPessoa_Pessoa_cep_Z = "";
         gxTv_SdtPessoa_Pessoa_telefone_Z = "";
         gxTv_SdtPessoa_Pessoa_fax_Z = "";
      }

      public SdtPessoa( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV34Pessoa_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV34Pessoa_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Pessoa_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Pessoa");
         metadata.Set("BT", "Pessoa");
         metadata.Set("PK", "[ \"Pessoa_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Pessoa_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Municipio_Codigo\" ],\"FKMap\":[ \"Pessoa_MunicipioCod-Municipio_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_tipopessoa_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_docto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_ie_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_endereco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_municipiocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_uf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_cep_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_telefone_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_fax_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_ie_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_endereco_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_municipiocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_uf_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_cep_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_telefone_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Pessoa_fax_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtPessoa deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtPessoa)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtPessoa obj ;
         obj = this;
         obj.gxTpr_Pessoa_codigo = deserialized.gxTpr_Pessoa_codigo;
         obj.gxTpr_Pessoa_nome = deserialized.gxTpr_Pessoa_nome;
         obj.gxTpr_Pessoa_tipopessoa = deserialized.gxTpr_Pessoa_tipopessoa;
         obj.gxTpr_Pessoa_docto = deserialized.gxTpr_Pessoa_docto;
         obj.gxTpr_Pessoa_ie = deserialized.gxTpr_Pessoa_ie;
         obj.gxTpr_Pessoa_endereco = deserialized.gxTpr_Pessoa_endereco;
         obj.gxTpr_Pessoa_municipiocod = deserialized.gxTpr_Pessoa_municipiocod;
         obj.gxTpr_Pessoa_uf = deserialized.gxTpr_Pessoa_uf;
         obj.gxTpr_Pessoa_cep = deserialized.gxTpr_Pessoa_cep;
         obj.gxTpr_Pessoa_telefone = deserialized.gxTpr_Pessoa_telefone;
         obj.gxTpr_Pessoa_fax = deserialized.gxTpr_Pessoa_fax;
         obj.gxTpr_Pessoa_ativo = deserialized.gxTpr_Pessoa_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Pessoa_codigo_Z = deserialized.gxTpr_Pessoa_codigo_Z;
         obj.gxTpr_Pessoa_nome_Z = deserialized.gxTpr_Pessoa_nome_Z;
         obj.gxTpr_Pessoa_tipopessoa_Z = deserialized.gxTpr_Pessoa_tipopessoa_Z;
         obj.gxTpr_Pessoa_docto_Z = deserialized.gxTpr_Pessoa_docto_Z;
         obj.gxTpr_Pessoa_ie_Z = deserialized.gxTpr_Pessoa_ie_Z;
         obj.gxTpr_Pessoa_endereco_Z = deserialized.gxTpr_Pessoa_endereco_Z;
         obj.gxTpr_Pessoa_municipiocod_Z = deserialized.gxTpr_Pessoa_municipiocod_Z;
         obj.gxTpr_Pessoa_uf_Z = deserialized.gxTpr_Pessoa_uf_Z;
         obj.gxTpr_Pessoa_cep_Z = deserialized.gxTpr_Pessoa_cep_Z;
         obj.gxTpr_Pessoa_telefone_Z = deserialized.gxTpr_Pessoa_telefone_Z;
         obj.gxTpr_Pessoa_fax_Z = deserialized.gxTpr_Pessoa_fax_Z;
         obj.gxTpr_Pessoa_ativo_Z = deserialized.gxTpr_Pessoa_ativo_Z;
         obj.gxTpr_Pessoa_ie_N = deserialized.gxTpr_Pessoa_ie_N;
         obj.gxTpr_Pessoa_endereco_N = deserialized.gxTpr_Pessoa_endereco_N;
         obj.gxTpr_Pessoa_municipiocod_N = deserialized.gxTpr_Pessoa_municipiocod_N;
         obj.gxTpr_Pessoa_uf_N = deserialized.gxTpr_Pessoa_uf_N;
         obj.gxTpr_Pessoa_cep_N = deserialized.gxTpr_Pessoa_cep_N;
         obj.gxTpr_Pessoa_telefone_N = deserialized.gxTpr_Pessoa_telefone_N;
         obj.gxTpr_Pessoa_fax_N = deserialized.gxTpr_Pessoa_fax_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Codigo") )
               {
                  gxTv_SdtPessoa_Pessoa_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Nome") )
               {
                  gxTv_SdtPessoa_Pessoa_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_TipoPessoa") )
               {
                  gxTv_SdtPessoa_Pessoa_tipopessoa = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Docto") )
               {
                  gxTv_SdtPessoa_Pessoa_docto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_IE") )
               {
                  gxTv_SdtPessoa_Pessoa_ie = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Endereco") )
               {
                  gxTv_SdtPessoa_Pessoa_endereco = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_MunicipioCod") )
               {
                  gxTv_SdtPessoa_Pessoa_municipiocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_UF") )
               {
                  gxTv_SdtPessoa_Pessoa_uf = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_CEP") )
               {
                  gxTv_SdtPessoa_Pessoa_cep = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Telefone") )
               {
                  gxTv_SdtPessoa_Pessoa_telefone = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Fax") )
               {
                  gxTv_SdtPessoa_Pessoa_fax = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Ativo") )
               {
                  gxTv_SdtPessoa_Pessoa_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtPessoa_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtPessoa_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Codigo_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Nome_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_TipoPessoa_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_tipopessoa_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Docto_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_docto_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_IE_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_ie_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Endereco_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_endereco_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_MunicipioCod_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_municipiocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_UF_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_uf_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_CEP_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_cep_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Telefone_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_telefone_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Fax_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_fax_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Ativo_Z") )
               {
                  gxTv_SdtPessoa_Pessoa_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_IE_N") )
               {
                  gxTv_SdtPessoa_Pessoa_ie_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Endereco_N") )
               {
                  gxTv_SdtPessoa_Pessoa_endereco_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_MunicipioCod_N") )
               {
                  gxTv_SdtPessoa_Pessoa_municipiocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_UF_N") )
               {
                  gxTv_SdtPessoa_Pessoa_uf_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_CEP_N") )
               {
                  gxTv_SdtPessoa_Pessoa_cep_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Telefone_N") )
               {
                  gxTv_SdtPessoa_Pessoa_telefone_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Fax_N") )
               {
                  gxTv_SdtPessoa_Pessoa_fax_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Pessoa";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Pessoa_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_Nome", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_TipoPessoa", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_tipopessoa));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_Docto", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_docto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_IE", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_ie));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_Endereco", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_endereco));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_MunicipioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_municipiocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_UF", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_uf));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_CEP", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_cep));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_Telefone", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_telefone));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_Fax", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_fax));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Pessoa_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtPessoa_Pessoa_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtPessoa_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Nome_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_TipoPessoa_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_tipopessoa_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Docto_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_docto_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_IE_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_ie_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Endereco_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_endereco_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_MunicipioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_municipiocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_UF_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_uf_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_CEP_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_cep_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Telefone_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_telefone_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Fax_Z", StringUtil.RTrim( gxTv_SdtPessoa_Pessoa_fax_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtPessoa_Pessoa_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_IE_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_ie_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Endereco_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_endereco_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_MunicipioCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_municipiocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_UF_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_uf_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_CEP_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_cep_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Telefone_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_telefone_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Pessoa_Fax_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPessoa_Pessoa_fax_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Pessoa_Codigo", gxTv_SdtPessoa_Pessoa_codigo, false);
         AddObjectProperty("Pessoa_Nome", gxTv_SdtPessoa_Pessoa_nome, false);
         AddObjectProperty("Pessoa_TipoPessoa", gxTv_SdtPessoa_Pessoa_tipopessoa, false);
         AddObjectProperty("Pessoa_Docto", gxTv_SdtPessoa_Pessoa_docto, false);
         AddObjectProperty("Pessoa_IE", gxTv_SdtPessoa_Pessoa_ie, false);
         AddObjectProperty("Pessoa_Endereco", gxTv_SdtPessoa_Pessoa_endereco, false);
         AddObjectProperty("Pessoa_MunicipioCod", gxTv_SdtPessoa_Pessoa_municipiocod, false);
         AddObjectProperty("Pessoa_UF", gxTv_SdtPessoa_Pessoa_uf, false);
         AddObjectProperty("Pessoa_CEP", gxTv_SdtPessoa_Pessoa_cep, false);
         AddObjectProperty("Pessoa_Telefone", gxTv_SdtPessoa_Pessoa_telefone, false);
         AddObjectProperty("Pessoa_Fax", gxTv_SdtPessoa_Pessoa_fax, false);
         AddObjectProperty("Pessoa_Ativo", gxTv_SdtPessoa_Pessoa_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtPessoa_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtPessoa_Initialized, false);
            AddObjectProperty("Pessoa_Codigo_Z", gxTv_SdtPessoa_Pessoa_codigo_Z, false);
            AddObjectProperty("Pessoa_Nome_Z", gxTv_SdtPessoa_Pessoa_nome_Z, false);
            AddObjectProperty("Pessoa_TipoPessoa_Z", gxTv_SdtPessoa_Pessoa_tipopessoa_Z, false);
            AddObjectProperty("Pessoa_Docto_Z", gxTv_SdtPessoa_Pessoa_docto_Z, false);
            AddObjectProperty("Pessoa_IE_Z", gxTv_SdtPessoa_Pessoa_ie_Z, false);
            AddObjectProperty("Pessoa_Endereco_Z", gxTv_SdtPessoa_Pessoa_endereco_Z, false);
            AddObjectProperty("Pessoa_MunicipioCod_Z", gxTv_SdtPessoa_Pessoa_municipiocod_Z, false);
            AddObjectProperty("Pessoa_UF_Z", gxTv_SdtPessoa_Pessoa_uf_Z, false);
            AddObjectProperty("Pessoa_CEP_Z", gxTv_SdtPessoa_Pessoa_cep_Z, false);
            AddObjectProperty("Pessoa_Telefone_Z", gxTv_SdtPessoa_Pessoa_telefone_Z, false);
            AddObjectProperty("Pessoa_Fax_Z", gxTv_SdtPessoa_Pessoa_fax_Z, false);
            AddObjectProperty("Pessoa_Ativo_Z", gxTv_SdtPessoa_Pessoa_ativo_Z, false);
            AddObjectProperty("Pessoa_IE_N", gxTv_SdtPessoa_Pessoa_ie_N, false);
            AddObjectProperty("Pessoa_Endereco_N", gxTv_SdtPessoa_Pessoa_endereco_N, false);
            AddObjectProperty("Pessoa_MunicipioCod_N", gxTv_SdtPessoa_Pessoa_municipiocod_N, false);
            AddObjectProperty("Pessoa_UF_N", gxTv_SdtPessoa_Pessoa_uf_N, false);
            AddObjectProperty("Pessoa_CEP_N", gxTv_SdtPessoa_Pessoa_cep_N, false);
            AddObjectProperty("Pessoa_Telefone_N", gxTv_SdtPessoa_Pessoa_telefone_N, false);
            AddObjectProperty("Pessoa_Fax_N", gxTv_SdtPessoa_Pessoa_fax_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Pessoa_Codigo" )]
      [  XmlElement( ElementName = "Pessoa_Codigo"   )]
      public int gxTpr_Pessoa_codigo
      {
         get {
            return gxTv_SdtPessoa_Pessoa_codigo ;
         }

         set {
            if ( gxTv_SdtPessoa_Pessoa_codigo != value )
            {
               gxTv_SdtPessoa_Mode = "INS";
               this.gxTv_SdtPessoa_Pessoa_codigo_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_nome_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_tipopessoa_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_docto_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_ie_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_endereco_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_municipiocod_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_uf_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_cep_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_telefone_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_fax_Z_SetNull( );
               this.gxTv_SdtPessoa_Pessoa_ativo_Z_SetNull( );
            }
            gxTv_SdtPessoa_Pessoa_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Pessoa_Nome" )]
      [  XmlElement( ElementName = "Pessoa_Nome"   )]
      public String gxTpr_Pessoa_nome
      {
         get {
            return gxTv_SdtPessoa_Pessoa_nome ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Pessoa_TipoPessoa" )]
      [  XmlElement( ElementName = "Pessoa_TipoPessoa"   )]
      public String gxTpr_Pessoa_tipopessoa
      {
         get {
            return gxTv_SdtPessoa_Pessoa_tipopessoa ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_tipopessoa = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Pessoa_Docto" )]
      [  XmlElement( ElementName = "Pessoa_Docto"   )]
      public String gxTpr_Pessoa_docto
      {
         get {
            return gxTv_SdtPessoa_Pessoa_docto ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_docto = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Pessoa_IE" )]
      [  XmlElement( ElementName = "Pessoa_IE"   )]
      public String gxTpr_Pessoa_ie
      {
         get {
            return gxTv_SdtPessoa_Pessoa_ie ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_ie_N = 0;
            gxTv_SdtPessoa_Pessoa_ie = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_ie_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_ie_N = 1;
         gxTv_SdtPessoa_Pessoa_ie = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_ie_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Endereco" )]
      [  XmlElement( ElementName = "Pessoa_Endereco"   )]
      public String gxTpr_Pessoa_endereco
      {
         get {
            return gxTv_SdtPessoa_Pessoa_endereco ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_endereco_N = 0;
            gxTv_SdtPessoa_Pessoa_endereco = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_endereco_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_endereco_N = 1;
         gxTv_SdtPessoa_Pessoa_endereco = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_endereco_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_MunicipioCod" )]
      [  XmlElement( ElementName = "Pessoa_MunicipioCod"   )]
      public int gxTpr_Pessoa_municipiocod
      {
         get {
            return gxTv_SdtPessoa_Pessoa_municipiocod ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_municipiocod_N = 0;
            gxTv_SdtPessoa_Pessoa_municipiocod = (int)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_municipiocod_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_municipiocod_N = 1;
         gxTv_SdtPessoa_Pessoa_municipiocod = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_municipiocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_UF" )]
      [  XmlElement( ElementName = "Pessoa_UF"   )]
      public String gxTpr_Pessoa_uf
      {
         get {
            return gxTv_SdtPessoa_Pessoa_uf ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_uf_N = 0;
            gxTv_SdtPessoa_Pessoa_uf = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_uf_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_uf_N = 1;
         gxTv_SdtPessoa_Pessoa_uf = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_uf_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_CEP" )]
      [  XmlElement( ElementName = "Pessoa_CEP"   )]
      public String gxTpr_Pessoa_cep
      {
         get {
            return gxTv_SdtPessoa_Pessoa_cep ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_cep_N = 0;
            gxTv_SdtPessoa_Pessoa_cep = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_cep_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_cep_N = 1;
         gxTv_SdtPessoa_Pessoa_cep = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_cep_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Telefone" )]
      [  XmlElement( ElementName = "Pessoa_Telefone"   )]
      public String gxTpr_Pessoa_telefone
      {
         get {
            return gxTv_SdtPessoa_Pessoa_telefone ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_telefone_N = 0;
            gxTv_SdtPessoa_Pessoa_telefone = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_telefone_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_telefone_N = 1;
         gxTv_SdtPessoa_Pessoa_telefone = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_telefone_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Fax" )]
      [  XmlElement( ElementName = "Pessoa_Fax"   )]
      public String gxTpr_Pessoa_fax
      {
         get {
            return gxTv_SdtPessoa_Pessoa_fax ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_fax_N = 0;
            gxTv_SdtPessoa_Pessoa_fax = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_fax_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_fax_N = 1;
         gxTv_SdtPessoa_Pessoa_fax = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_fax_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Ativo" )]
      [  XmlElement( ElementName = "Pessoa_Ativo"   )]
      public bool gxTpr_Pessoa_ativo
      {
         get {
            return gxTv_SdtPessoa_Pessoa_ativo ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtPessoa_Mode ;
         }

         set {
            gxTv_SdtPessoa_Mode = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Mode_SetNull( )
      {
         gxTv_SdtPessoa_Mode = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtPessoa_Initialized ;
         }

         set {
            gxTv_SdtPessoa_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Initialized_SetNull( )
      {
         gxTv_SdtPessoa_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Codigo_Z" )]
      [  XmlElement( ElementName = "Pessoa_Codigo_Z"   )]
      public int gxTpr_Pessoa_codigo_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_codigo_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_codigo_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Nome_Z" )]
      [  XmlElement( ElementName = "Pessoa_Nome_Z"   )]
      public String gxTpr_Pessoa_nome_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_nome_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_nome_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_TipoPessoa_Z" )]
      [  XmlElement( ElementName = "Pessoa_TipoPessoa_Z"   )]
      public String gxTpr_Pessoa_tipopessoa_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_tipopessoa_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_tipopessoa_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_tipopessoa_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_tipopessoa_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_tipopessoa_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Docto_Z" )]
      [  XmlElement( ElementName = "Pessoa_Docto_Z"   )]
      public String gxTpr_Pessoa_docto_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_docto_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_docto_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_docto_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_docto_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_docto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_IE_Z" )]
      [  XmlElement( ElementName = "Pessoa_IE_Z"   )]
      public String gxTpr_Pessoa_ie_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_ie_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_ie_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_ie_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_ie_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_ie_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Endereco_Z" )]
      [  XmlElement( ElementName = "Pessoa_Endereco_Z"   )]
      public String gxTpr_Pessoa_endereco_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_endereco_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_endereco_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_endereco_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_endereco_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_endereco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_MunicipioCod_Z" )]
      [  XmlElement( ElementName = "Pessoa_MunicipioCod_Z"   )]
      public int gxTpr_Pessoa_municipiocod_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_municipiocod_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_municipiocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_municipiocod_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_municipiocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_municipiocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_UF_Z" )]
      [  XmlElement( ElementName = "Pessoa_UF_Z"   )]
      public String gxTpr_Pessoa_uf_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_uf_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_uf_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_uf_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_uf_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_uf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_CEP_Z" )]
      [  XmlElement( ElementName = "Pessoa_CEP_Z"   )]
      public String gxTpr_Pessoa_cep_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_cep_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_cep_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_cep_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_cep_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_cep_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Telefone_Z" )]
      [  XmlElement( ElementName = "Pessoa_Telefone_Z"   )]
      public String gxTpr_Pessoa_telefone_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_telefone_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_telefone_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_telefone_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_telefone_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_telefone_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Fax_Z" )]
      [  XmlElement( ElementName = "Pessoa_Fax_Z"   )]
      public String gxTpr_Pessoa_fax_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_fax_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_fax_Z = (String)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_fax_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_fax_Z = "";
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_fax_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Ativo_Z" )]
      [  XmlElement( ElementName = "Pessoa_Ativo_Z"   )]
      public bool gxTpr_Pessoa_ativo_Z
      {
         get {
            return gxTv_SdtPessoa_Pessoa_ativo_Z ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_ativo_Z = value;
         }

      }

      public void gxTv_SdtPessoa_Pessoa_ativo_Z_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_IE_N" )]
      [  XmlElement( ElementName = "Pessoa_IE_N"   )]
      public short gxTpr_Pessoa_ie_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_ie_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_ie_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_ie_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_ie_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_ie_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Endereco_N" )]
      [  XmlElement( ElementName = "Pessoa_Endereco_N"   )]
      public short gxTpr_Pessoa_endereco_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_endereco_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_endereco_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_endereco_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_endereco_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_endereco_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_MunicipioCod_N" )]
      [  XmlElement( ElementName = "Pessoa_MunicipioCod_N"   )]
      public short gxTpr_Pessoa_municipiocod_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_municipiocod_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_municipiocod_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_municipiocod_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_municipiocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_municipiocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_UF_N" )]
      [  XmlElement( ElementName = "Pessoa_UF_N"   )]
      public short gxTpr_Pessoa_uf_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_uf_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_uf_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_uf_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_uf_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_uf_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_CEP_N" )]
      [  XmlElement( ElementName = "Pessoa_CEP_N"   )]
      public short gxTpr_Pessoa_cep_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_cep_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_cep_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_cep_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_cep_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_cep_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Telefone_N" )]
      [  XmlElement( ElementName = "Pessoa_Telefone_N"   )]
      public short gxTpr_Pessoa_telefone_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_telefone_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_telefone_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_telefone_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_telefone_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_telefone_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Pessoa_Fax_N" )]
      [  XmlElement( ElementName = "Pessoa_Fax_N"   )]
      public short gxTpr_Pessoa_fax_N
      {
         get {
            return gxTv_SdtPessoa_Pessoa_fax_N ;
         }

         set {
            gxTv_SdtPessoa_Pessoa_fax_N = (short)(value);
         }

      }

      public void gxTv_SdtPessoa_Pessoa_fax_N_SetNull( )
      {
         gxTv_SdtPessoa_Pessoa_fax_N = 0;
         return  ;
      }

      public bool gxTv_SdtPessoa_Pessoa_fax_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtPessoa_Pessoa_nome = "";
         gxTv_SdtPessoa_Pessoa_tipopessoa = "";
         gxTv_SdtPessoa_Pessoa_docto = "";
         gxTv_SdtPessoa_Pessoa_ie = "";
         gxTv_SdtPessoa_Pessoa_endereco = "";
         gxTv_SdtPessoa_Pessoa_uf = "";
         gxTv_SdtPessoa_Pessoa_cep = "";
         gxTv_SdtPessoa_Pessoa_telefone = "";
         gxTv_SdtPessoa_Pessoa_fax = "";
         gxTv_SdtPessoa_Mode = "";
         gxTv_SdtPessoa_Pessoa_nome_Z = "";
         gxTv_SdtPessoa_Pessoa_tipopessoa_Z = "";
         gxTv_SdtPessoa_Pessoa_docto_Z = "";
         gxTv_SdtPessoa_Pessoa_ie_Z = "";
         gxTv_SdtPessoa_Pessoa_endereco_Z = "";
         gxTv_SdtPessoa_Pessoa_uf_Z = "";
         gxTv_SdtPessoa_Pessoa_cep_Z = "";
         gxTv_SdtPessoa_Pessoa_telefone_Z = "";
         gxTv_SdtPessoa_Pessoa_fax_Z = "";
         gxTv_SdtPessoa_Pessoa_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "pessoa", "GeneXus.Programs.pessoa_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtPessoa_Initialized ;
      private short gxTv_SdtPessoa_Pessoa_ie_N ;
      private short gxTv_SdtPessoa_Pessoa_endereco_N ;
      private short gxTv_SdtPessoa_Pessoa_municipiocod_N ;
      private short gxTv_SdtPessoa_Pessoa_uf_N ;
      private short gxTv_SdtPessoa_Pessoa_cep_N ;
      private short gxTv_SdtPessoa_Pessoa_telefone_N ;
      private short gxTv_SdtPessoa_Pessoa_fax_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtPessoa_Pessoa_codigo ;
      private int gxTv_SdtPessoa_Pessoa_municipiocod ;
      private int gxTv_SdtPessoa_Pessoa_codigo_Z ;
      private int gxTv_SdtPessoa_Pessoa_municipiocod_Z ;
      private String gxTv_SdtPessoa_Pessoa_nome ;
      private String gxTv_SdtPessoa_Pessoa_tipopessoa ;
      private String gxTv_SdtPessoa_Pessoa_ie ;
      private String gxTv_SdtPessoa_Pessoa_uf ;
      private String gxTv_SdtPessoa_Pessoa_cep ;
      private String gxTv_SdtPessoa_Pessoa_telefone ;
      private String gxTv_SdtPessoa_Pessoa_fax ;
      private String gxTv_SdtPessoa_Mode ;
      private String gxTv_SdtPessoa_Pessoa_nome_Z ;
      private String gxTv_SdtPessoa_Pessoa_tipopessoa_Z ;
      private String gxTv_SdtPessoa_Pessoa_ie_Z ;
      private String gxTv_SdtPessoa_Pessoa_uf_Z ;
      private String gxTv_SdtPessoa_Pessoa_cep_Z ;
      private String gxTv_SdtPessoa_Pessoa_telefone_Z ;
      private String gxTv_SdtPessoa_Pessoa_fax_Z ;
      private String sTagName ;
      private bool gxTv_SdtPessoa_Pessoa_ativo ;
      private bool gxTv_SdtPessoa_Pessoa_ativo_Z ;
      private String gxTv_SdtPessoa_Pessoa_docto ;
      private String gxTv_SdtPessoa_Pessoa_endereco ;
      private String gxTv_SdtPessoa_Pessoa_docto_Z ;
      private String gxTv_SdtPessoa_Pessoa_endereco_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Pessoa", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtPessoa_RESTInterface : GxGenericCollectionItem<SdtPessoa>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPessoa_RESTInterface( ) : base()
      {
      }

      public SdtPessoa_RESTInterface( SdtPessoa psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Pessoa_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Pessoa_codigo
      {
         get {
            return sdt.gxTpr_Pessoa_codigo ;
         }

         set {
            sdt.gxTpr_Pessoa_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Pessoa_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_nome) ;
         }

         set {
            sdt.gxTpr_Pessoa_nome = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_TipoPessoa" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_tipopessoa
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_tipopessoa) ;
         }

         set {
            sdt.gxTpr_Pessoa_tipopessoa = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Docto" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_docto
      {
         get {
            return sdt.gxTpr_Pessoa_docto ;
         }

         set {
            sdt.gxTpr_Pessoa_docto = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_IE" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_ie
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_ie) ;
         }

         set {
            sdt.gxTpr_Pessoa_ie = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Endereco" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_endereco
      {
         get {
            return sdt.gxTpr_Pessoa_endereco ;
         }

         set {
            sdt.gxTpr_Pessoa_endereco = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_MunicipioCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Pessoa_municipiocod
      {
         get {
            return sdt.gxTpr_Pessoa_municipiocod ;
         }

         set {
            sdt.gxTpr_Pessoa_municipiocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Pessoa_UF" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_uf
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_uf) ;
         }

         set {
            sdt.gxTpr_Pessoa_uf = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_CEP" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_cep
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_cep) ;
         }

         set {
            sdt.gxTpr_Pessoa_cep = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Telefone" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_telefone
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_telefone) ;
         }

         set {
            sdt.gxTpr_Pessoa_telefone = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Fax" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Pessoa_fax
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_fax) ;
         }

         set {
            sdt.gxTpr_Pessoa_fax = (String)(value);
         }

      }

      [DataMember( Name = "Pessoa_Ativo" , Order = 11 )]
      [GxSeudo()]
      public bool gxTpr_Pessoa_ativo
      {
         get {
            return sdt.gxTpr_Pessoa_ativo ;
         }

         set {
            sdt.gxTpr_Pessoa_ativo = value;
         }

      }

      public SdtPessoa sdt
      {
         get {
            return (SdtPessoa)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtPessoa() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 33 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
