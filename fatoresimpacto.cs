/*
               File: FatoresImpacto
        Description: Configuração de Fatores de Impacto na Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:10:2.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class fatoresimpacto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A5AreaTrabalho_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV8AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8AreaTrabalho_Codigo), "ZZZZZ9")));
               AV11FatoresImpacto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11FatoresImpacto_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFATORESIMPACTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11FatoresImpacto_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Configuração de Fatores de Impacto na Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public fatoresimpacto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public fatoresimpacto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_AreaTrabalho_Codigo ,
                           int aP2_FatoresImpacto_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         this.AV11FatoresImpacto_Codigo = aP2_FatoresImpacto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_5B232( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_5B232e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAreaTrabalho_Codigo_Visible, edtAreaTrabalho_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FatoresImpacto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2127FatoresImpacto_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A2127FatoresImpacto_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFatoresImpacto_Codigo_Visible, edtFatoresImpacto_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FatoresImpacto.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_5B232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFatoresimpactotitle_Internalname, "Configuração de Fatores de Impacto na Contagem", "", "", lblFatoresimpactotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5B232( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_5B232e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_5B232( true) ;
         }
         return  ;
      }

      protected void wb_table3_67_5B232e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5B232e( true) ;
         }
         else
         {
            wb_table1_2_5B232e( false) ;
         }
      }

      protected void wb_table3_67_5B232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_5B232e( true) ;
         }
         else
         {
            wb_table3_67_5B232e( false) ;
         }
      }

      protected void wb_table2_8_5B232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_5B232( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_5B232e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5B232e( true) ;
         }
         else
         {
            wb_table2_8_5B232e( false) ;
         }
      }

      protected void wb_table4_16_5B232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup2_Internalname, "Projetos de Melhoria sem Garantia (Evolutiva)", 1, 0, "px", 0, "px", "Group", "", "HLP_FatoresImpacto.htm");
            wb_table5_20_5B232( true) ;
         }
         return  ;
      }

      protected void wb_table5_20_5B232e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "Projetos de Melhoria com Garantia (Corretiva)", 1, 0, "px", 0, "px", "Group", "", "HLP_FatoresImpacto.htm");
            wb_table6_44_5B232( true) ;
         }
         return  ;
      }

      protected void wb_table6_44_5B232e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_5B232e( true) ;
         }
         else
         {
            wb_table4_16_5B232e( false) ;
         }
      }

      protected void wb_table6_44_5B232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcmcsd_Internalname, "Funcionalidade Mantida pela Contratada atual S/Redocumentação - CMCSD", "", "", lblTextblockfatoresimpacto_valorcmcsd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCMCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2123FatoresImpacto_ValorCMCSD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorCMCSD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")) : context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCMCSD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorCMCSD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcmccd_Internalname, "Funcionalidade Mantida pela Contratada atual C/Redocumentação - CMCCD", "", "", lblTextblockfatoresimpacto_valorcmccd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCMCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2124FatoresImpacto_ValorCMCCD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorCMCCD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")) : context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCMCCD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorCMCCD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcncsd_Internalname, "Funcionalidade Não Mantida pela Contratada atual S/Redocumentação - CNCSD", "", "", lblTextblockfatoresimpacto_valorcncsd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCNCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2125FatoresImpacto_ValorCNCSD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorCNCSD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")) : context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCNCSD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorCNCSD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcnccd_Internalname, "Funcionalidade Não Mantida pela Contratada atual C/Redocumentação - CNCCD", "", "", lblTextblockfatoresimpacto_valorcnccd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCNCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2126FatoresImpacto_ValorCNCCD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorCNCCD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")) : context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCNCCD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorCNCCD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_44_5B232e( true) ;
         }
         else
         {
            wb_table6_44_5B232e( false) ;
         }
      }

      protected void wb_table5_20_5B232( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valoremcsd_Internalname, "Funcionalidade Mantida pela Contratada atual S/Redocumentação - EMCSD", "", "", lblTextblockfatoresimpacto_valoremcsd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorEMCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2119FatoresImpacto_ValorEMCSD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorEMCSD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")) : context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorEMCSD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorEMCSD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valoremccd_Internalname, "Funcionalidade Mantida pela Contratada atual C/Redocumentação - EMCCD", "", "", lblTextblockfatoresimpacto_valoremccd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorEMCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2120FatoresImpacto_ValorEMCCD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorEMCCD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")) : context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorEMCCD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorEMCCD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorencsd_Internalname, "Funcionalidade Não Mantida pela Contratada atual S/Redocumentação - ENCSD", "", "", lblTextblockfatoresimpacto_valorencsd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorENCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2121FatoresImpacto_ValorENCSD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorENCSD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")) : context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorENCSD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorENCSD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorenccd_Internalname, "Funcionalidade Não Mantida pela Contratada atual C/Redocumentação - ENCCD", "", "", lblTextblockfatoresimpacto_valorenccd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorENCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2122FatoresImpacto_ValorENCCD, 5, 2, ",", "")), ((edtFatoresImpacto_ValorENCCD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")) : context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorENCCD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFatoresImpacto_ValorENCCD_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_5B232e( true) ;
         }
         else
         {
            wb_table5_20_5B232e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E115B2 */
         E115B2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCSD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCSD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALOREMCSD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2119FatoresImpacto_ValorEMCSD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
               }
               else
               {
                  A2119FatoresImpacto_ValorEMCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCSD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCCD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCCD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALOREMCCD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorEMCCD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2120FatoresImpacto_ValorEMCCD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
               }
               else
               {
                  A2120FatoresImpacto_ValorEMCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCCD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCSD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCSD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALORENCSD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorENCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2121FatoresImpacto_ValorENCSD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
               }
               else
               {
                  A2121FatoresImpacto_ValorENCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCSD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCCD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCCD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALORENCCD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorENCCD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2122FatoresImpacto_ValorENCCD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
               }
               else
               {
                  A2122FatoresImpacto_ValorENCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCCD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCSD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCSD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALORCMCSD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorCMCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2123FatoresImpacto_ValorCMCSD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
               }
               else
               {
                  A2123FatoresImpacto_ValorCMCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCSD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCCD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCCD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALORCMCCD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorCMCCD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2124FatoresImpacto_ValorCMCCD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
               }
               else
               {
                  A2124FatoresImpacto_ValorCMCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCCD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCSD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCSD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALORCNCSD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorCNCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2125FatoresImpacto_ValorCNCSD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
               }
               else
               {
                  A2125FatoresImpacto_ValorCNCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCSD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCCD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCCD_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_VALORCNCCD");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_ValorCNCCD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2126FatoresImpacto_ValorCNCCD = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
               }
               else
               {
                  A2126FatoresImpacto_ValorCNCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCCD_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AREATRABALHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A5AreaTrabalho_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               }
               else
               {
                  A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFatoresImpacto_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FATORESIMPACTO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFatoresImpacto_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2127FatoresImpacto_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
               }
               else
               {
                  A2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFatoresImpacto_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z5AreaTrabalho_Codigo"), ",", "."));
               Z2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2127FatoresImpacto_Codigo"), ",", "."));
               Z2119FatoresImpacto_ValorEMCSD = context.localUtil.CToN( cgiGet( "Z2119FatoresImpacto_ValorEMCSD"), ",", ".");
               Z2120FatoresImpacto_ValorEMCCD = context.localUtil.CToN( cgiGet( "Z2120FatoresImpacto_ValorEMCCD"), ",", ".");
               Z2121FatoresImpacto_ValorENCSD = context.localUtil.CToN( cgiGet( "Z2121FatoresImpacto_ValorENCSD"), ",", ".");
               Z2122FatoresImpacto_ValorENCCD = context.localUtil.CToN( cgiGet( "Z2122FatoresImpacto_ValorENCCD"), ",", ".");
               Z2123FatoresImpacto_ValorCMCSD = context.localUtil.CToN( cgiGet( "Z2123FatoresImpacto_ValorCMCSD"), ",", ".");
               Z2124FatoresImpacto_ValorCMCCD = context.localUtil.CToN( cgiGet( "Z2124FatoresImpacto_ValorCMCCD"), ",", ".");
               Z2125FatoresImpacto_ValorCNCSD = context.localUtil.CToN( cgiGet( "Z2125FatoresImpacto_ValorCNCSD"), ",", ".");
               Z2126FatoresImpacto_ValorCNCCD = context.localUtil.CToN( cgiGet( "Z2126FatoresImpacto_ValorCNCCD"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV8AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( "vAREATRABALHO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV11FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFATORESIMPACTO_CODIGO"), ",", "."));
               A6AreaTrabalho_Descricao = cgiGet( "AREATRABALHO_DESCRICAO");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "FatoresImpacto";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A2127FatoresImpacto_Codigo != Z2127FatoresImpacto_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("fatoresimpacto:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A2127FatoresImpacto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  if ( ! (0==AV11FatoresImpacto_Codigo) )
                  {
                     A2127FatoresImpacto_Codigo = AV11FatoresImpacto_Codigo;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                  }
                  else
                  {
                     if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2127FatoresImpacto_Codigo) && ( Gx_BScreen == 0 ) )
                     {
                        A2127FatoresImpacto_Codigo = 1;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                     }
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode232 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     if ( ! (0==AV11FatoresImpacto_Codigo) )
                     {
                        A2127FatoresImpacto_Codigo = AV11FatoresImpacto_Codigo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                     }
                     else
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2127FatoresImpacto_Codigo) && ( Gx_BScreen == 0 ) )
                        {
                           A2127FatoresImpacto_Codigo = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                        }
                     }
                     Gx_mode = sMode232;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound232 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_5B0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "AREATRABALHO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E115B2 */
                           E115B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E125B2 */
                           E125B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E125B2 */
            E125B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll5B232( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes5B232( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_5B0( )
      {
         BeforeValidate5B232( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls5B232( ) ;
            }
            else
            {
               CheckExtendedTable5B232( ) ;
               CloseExtendedTableCursors5B232( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption5B0( )
      {
      }

      protected void E115B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtAreaTrabalho_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Visible), 5, 0)));
         edtFatoresImpacto_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_Codigo_Visible), 5, 0)));
      }

      protected void E125B2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwfatoresimpacto.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM5B232( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2119FatoresImpacto_ValorEMCSD = T005B3_A2119FatoresImpacto_ValorEMCSD[0];
               Z2120FatoresImpacto_ValorEMCCD = T005B3_A2120FatoresImpacto_ValorEMCCD[0];
               Z2121FatoresImpacto_ValorENCSD = T005B3_A2121FatoresImpacto_ValorENCSD[0];
               Z2122FatoresImpacto_ValorENCCD = T005B3_A2122FatoresImpacto_ValorENCCD[0];
               Z2123FatoresImpacto_ValorCMCSD = T005B3_A2123FatoresImpacto_ValorCMCSD[0];
               Z2124FatoresImpacto_ValorCMCCD = T005B3_A2124FatoresImpacto_ValorCMCCD[0];
               Z2125FatoresImpacto_ValorCNCSD = T005B3_A2125FatoresImpacto_ValorCNCSD[0];
               Z2126FatoresImpacto_ValorCNCCD = T005B3_A2126FatoresImpacto_ValorCNCCD[0];
            }
            else
            {
               Z2119FatoresImpacto_ValorEMCSD = A2119FatoresImpacto_ValorEMCSD;
               Z2120FatoresImpacto_ValorEMCCD = A2120FatoresImpacto_ValorEMCCD;
               Z2121FatoresImpacto_ValorENCSD = A2121FatoresImpacto_ValorENCSD;
               Z2122FatoresImpacto_ValorENCCD = A2122FatoresImpacto_ValorENCCD;
               Z2123FatoresImpacto_ValorCMCSD = A2123FatoresImpacto_ValorCMCSD;
               Z2124FatoresImpacto_ValorCMCCD = A2124FatoresImpacto_ValorCMCCD;
               Z2125FatoresImpacto_ValorCNCSD = A2125FatoresImpacto_ValorCNCSD;
               Z2126FatoresImpacto_ValorCNCCD = A2126FatoresImpacto_ValorCNCCD;
            }
         }
         if ( GX_JID == -7 )
         {
            Z2127FatoresImpacto_Codigo = A2127FatoresImpacto_Codigo;
            Z2119FatoresImpacto_ValorEMCSD = A2119FatoresImpacto_ValorEMCSD;
            Z2120FatoresImpacto_ValorEMCCD = A2120FatoresImpacto_ValorEMCCD;
            Z2121FatoresImpacto_ValorENCSD = A2121FatoresImpacto_ValorENCSD;
            Z2122FatoresImpacto_ValorENCCD = A2122FatoresImpacto_ValorENCCD;
            Z2123FatoresImpacto_ValorCMCSD = A2123FatoresImpacto_ValorCMCSD;
            Z2124FatoresImpacto_ValorCMCCD = A2124FatoresImpacto_ValorCMCCD;
            Z2125FatoresImpacto_ValorCNCSD = A2125FatoresImpacto_ValorCNCSD;
            Z2126FatoresImpacto_ValorCNCCD = A2126FatoresImpacto_ValorCNCCD;
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV8AreaTrabalho_Codigo) )
         {
            edtAreaTrabalho_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtAreaTrabalho_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8AreaTrabalho_Codigo) )
         {
            edtAreaTrabalho_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV11FatoresImpacto_Codigo) )
         {
            edtFatoresImpacto_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtFatoresImpacto_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV11FatoresImpacto_Codigo) )
         {
            edtFatoresImpacto_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ! (0==AV8AreaTrabalho_Codigo) )
         {
            A5AreaTrabalho_Codigo = AV8AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A5AreaTrabalho_Codigo) && ( Gx_BScreen == 0 ) )
            {
               A5AreaTrabalho_Codigo = AV7WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            }
         }
         if ( ! (0==AV11FatoresImpacto_Codigo) )
         {
            A2127FatoresImpacto_Codigo = AV11FatoresImpacto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2127FatoresImpacto_Codigo) && ( Gx_BScreen == 0 ) )
            {
               A2127FatoresImpacto_Codigo = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T005B4 */
            pr_default.execute(2, new Object[] {A5AreaTrabalho_Codigo});
            A6AreaTrabalho_Descricao = T005B4_A6AreaTrabalho_Descricao[0];
            pr_default.close(2);
         }
      }

      protected void Load5B232( )
      {
         /* Using cursor T005B5 */
         pr_default.execute(3, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound232 = 1;
            A6AreaTrabalho_Descricao = T005B5_A6AreaTrabalho_Descricao[0];
            A2119FatoresImpacto_ValorEMCSD = T005B5_A2119FatoresImpacto_ValorEMCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
            A2120FatoresImpacto_ValorEMCCD = T005B5_A2120FatoresImpacto_ValorEMCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
            A2121FatoresImpacto_ValorENCSD = T005B5_A2121FatoresImpacto_ValorENCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
            A2122FatoresImpacto_ValorENCCD = T005B5_A2122FatoresImpacto_ValorENCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
            A2123FatoresImpacto_ValorCMCSD = T005B5_A2123FatoresImpacto_ValorCMCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
            A2124FatoresImpacto_ValorCMCCD = T005B5_A2124FatoresImpacto_ValorCMCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
            A2125FatoresImpacto_ValorCNCSD = T005B5_A2125FatoresImpacto_ValorCNCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
            A2126FatoresImpacto_ValorCNCCD = T005B5_A2126FatoresImpacto_ValorCNCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
            ZM5B232( -7) ;
         }
         pr_default.close(3);
         OnLoadActions5B232( ) ;
      }

      protected void OnLoadActions5B232( )
      {
      }

      protected void CheckExtendedTable5B232( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T005B4 */
         pr_default.execute(2, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("Não existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A6AreaTrabalho_Descricao = T005B4_A6AreaTrabalho_Descricao[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors5B232( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A5AreaTrabalho_Codigo )
      {
         /* Using cursor T005B6 */
         pr_default.execute(4, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("Não existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A6AreaTrabalho_Descricao = T005B6_A6AreaTrabalho_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A6AreaTrabalho_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey5B232( )
      {
         /* Using cursor T005B7 */
         pr_default.execute(5, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound232 = 1;
         }
         else
         {
            RcdFound232 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T005B3 */
         pr_default.execute(1, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM5B232( 7) ;
            RcdFound232 = 1;
            A2127FatoresImpacto_Codigo = T005B3_A2127FatoresImpacto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
            A2119FatoresImpacto_ValorEMCSD = T005B3_A2119FatoresImpacto_ValorEMCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
            A2120FatoresImpacto_ValorEMCCD = T005B3_A2120FatoresImpacto_ValorEMCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
            A2121FatoresImpacto_ValorENCSD = T005B3_A2121FatoresImpacto_ValorENCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
            A2122FatoresImpacto_ValorENCCD = T005B3_A2122FatoresImpacto_ValorENCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
            A2123FatoresImpacto_ValorCMCSD = T005B3_A2123FatoresImpacto_ValorCMCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
            A2124FatoresImpacto_ValorCMCCD = T005B3_A2124FatoresImpacto_ValorCMCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
            A2125FatoresImpacto_ValorCNCSD = T005B3_A2125FatoresImpacto_ValorCNCSD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
            A2126FatoresImpacto_ValorCNCCD = T005B3_A2126FatoresImpacto_ValorCNCCD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
            A5AreaTrabalho_Codigo = T005B3_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z2127FatoresImpacto_Codigo = A2127FatoresImpacto_Codigo;
            sMode232 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load5B232( ) ;
            if ( AnyError == 1 )
            {
               RcdFound232 = 0;
               InitializeNonKey5B232( ) ;
            }
            Gx_mode = sMode232;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound232 = 0;
            InitializeNonKey5B232( ) ;
            sMode232 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode232;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey5B232( ) ;
         if ( RcdFound232 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound232 = 0;
         /* Using cursor T005B8 */
         pr_default.execute(6, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T005B8_A5AreaTrabalho_Codigo[0] < A5AreaTrabalho_Codigo ) || ( T005B8_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T005B8_A2127FatoresImpacto_Codigo[0] < A2127FatoresImpacto_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T005B8_A5AreaTrabalho_Codigo[0] > A5AreaTrabalho_Codigo ) || ( T005B8_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T005B8_A2127FatoresImpacto_Codigo[0] > A2127FatoresImpacto_Codigo ) ) )
            {
               A5AreaTrabalho_Codigo = T005B8_A5AreaTrabalho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A2127FatoresImpacto_Codigo = T005B8_A2127FatoresImpacto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
               RcdFound232 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound232 = 0;
         /* Using cursor T005B9 */
         pr_default.execute(7, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T005B9_A5AreaTrabalho_Codigo[0] > A5AreaTrabalho_Codigo ) || ( T005B9_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T005B9_A2127FatoresImpacto_Codigo[0] > A2127FatoresImpacto_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T005B9_A5AreaTrabalho_Codigo[0] < A5AreaTrabalho_Codigo ) || ( T005B9_A5AreaTrabalho_Codigo[0] == A5AreaTrabalho_Codigo ) && ( T005B9_A2127FatoresImpacto_Codigo[0] < A2127FatoresImpacto_Codigo ) ) )
            {
               A5AreaTrabalho_Codigo = T005B9_A5AreaTrabalho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A2127FatoresImpacto_Codigo = T005B9_A2127FatoresImpacto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
               RcdFound232 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey5B232( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert5B232( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound232 == 1 )
            {
               if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A2127FatoresImpacto_Codigo != Z2127FatoresImpacto_Codigo ) )
               {
                  A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A2127FatoresImpacto_Codigo = Z2127FatoresImpacto_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AREATRABALHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update5B232( ) ;
                  GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A2127FatoresImpacto_Codigo != Z2127FatoresImpacto_Codigo ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert5B232( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AREATRABALHO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert5B232( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A2127FatoresImpacto_Codigo != Z2127FatoresImpacto_Codigo ) )
         {
            A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A2127FatoresImpacto_Codigo = Z2127FatoresImpacto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFatoresImpacto_ValorEMCSD_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency5B232( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T005B2 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FatoresImpacto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z2119FatoresImpacto_ValorEMCSD != T005B2_A2119FatoresImpacto_ValorEMCSD[0] ) || ( Z2120FatoresImpacto_ValorEMCCD != T005B2_A2120FatoresImpacto_ValorEMCCD[0] ) || ( Z2121FatoresImpacto_ValorENCSD != T005B2_A2121FatoresImpacto_ValorENCSD[0] ) || ( Z2122FatoresImpacto_ValorENCCD != T005B2_A2122FatoresImpacto_ValorENCCD[0] ) || ( Z2123FatoresImpacto_ValorCMCSD != T005B2_A2123FatoresImpacto_ValorCMCSD[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2124FatoresImpacto_ValorCMCCD != T005B2_A2124FatoresImpacto_ValorCMCCD[0] ) || ( Z2125FatoresImpacto_ValorCNCSD != T005B2_A2125FatoresImpacto_ValorCNCSD[0] ) || ( Z2126FatoresImpacto_ValorCNCCD != T005B2_A2126FatoresImpacto_ValorCNCCD[0] ) )
            {
               if ( Z2119FatoresImpacto_ValorEMCSD != T005B2_A2119FatoresImpacto_ValorEMCSD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorEMCSD");
                  GXUtil.WriteLogRaw("Old: ",Z2119FatoresImpacto_ValorEMCSD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2119FatoresImpacto_ValorEMCSD[0]);
               }
               if ( Z2120FatoresImpacto_ValorEMCCD != T005B2_A2120FatoresImpacto_ValorEMCCD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorEMCCD");
                  GXUtil.WriteLogRaw("Old: ",Z2120FatoresImpacto_ValorEMCCD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2120FatoresImpacto_ValorEMCCD[0]);
               }
               if ( Z2121FatoresImpacto_ValorENCSD != T005B2_A2121FatoresImpacto_ValorENCSD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorENCSD");
                  GXUtil.WriteLogRaw("Old: ",Z2121FatoresImpacto_ValorENCSD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2121FatoresImpacto_ValorENCSD[0]);
               }
               if ( Z2122FatoresImpacto_ValorENCCD != T005B2_A2122FatoresImpacto_ValorENCCD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorENCCD");
                  GXUtil.WriteLogRaw("Old: ",Z2122FatoresImpacto_ValorENCCD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2122FatoresImpacto_ValorENCCD[0]);
               }
               if ( Z2123FatoresImpacto_ValorCMCSD != T005B2_A2123FatoresImpacto_ValorCMCSD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorCMCSD");
                  GXUtil.WriteLogRaw("Old: ",Z2123FatoresImpacto_ValorCMCSD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2123FatoresImpacto_ValorCMCSD[0]);
               }
               if ( Z2124FatoresImpacto_ValorCMCCD != T005B2_A2124FatoresImpacto_ValorCMCCD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorCMCCD");
                  GXUtil.WriteLogRaw("Old: ",Z2124FatoresImpacto_ValorCMCCD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2124FatoresImpacto_ValorCMCCD[0]);
               }
               if ( Z2125FatoresImpacto_ValorCNCSD != T005B2_A2125FatoresImpacto_ValorCNCSD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorCNCSD");
                  GXUtil.WriteLogRaw("Old: ",Z2125FatoresImpacto_ValorCNCSD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2125FatoresImpacto_ValorCNCSD[0]);
               }
               if ( Z2126FatoresImpacto_ValorCNCCD != T005B2_A2126FatoresImpacto_ValorCNCCD[0] )
               {
                  GXUtil.WriteLog("fatoresimpacto:[seudo value changed for attri]"+"FatoresImpacto_ValorCNCCD");
                  GXUtil.WriteLogRaw("Old: ",Z2126FatoresImpacto_ValorCNCCD);
                  GXUtil.WriteLogRaw("Current: ",T005B2_A2126FatoresImpacto_ValorCNCCD[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FatoresImpacto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert5B232( )
      {
         BeforeValidate5B232( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5B232( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM5B232( 0) ;
            CheckOptimisticConcurrency5B232( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5B232( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert5B232( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005B10 */
                     pr_default.execute(8, new Object[] {A2127FatoresImpacto_Codigo, A2119FatoresImpacto_ValorEMCSD, A2120FatoresImpacto_ValorEMCCD, A2121FatoresImpacto_ValorENCSD, A2122FatoresImpacto_ValorENCCD, A2123FatoresImpacto_ValorCMCSD, A2124FatoresImpacto_ValorCMCCD, A2125FatoresImpacto_ValorCNCSD, A2126FatoresImpacto_ValorCNCCD, A5AreaTrabalho_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("FatoresImpacto") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load5B232( ) ;
            }
            EndLevel5B232( ) ;
         }
         CloseExtendedTableCursors5B232( ) ;
      }

      protected void Update5B232( )
      {
         BeforeValidate5B232( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5B232( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5B232( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5B232( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate5B232( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005B11 */
                     pr_default.execute(9, new Object[] {A2119FatoresImpacto_ValorEMCSD, A2120FatoresImpacto_ValorEMCCD, A2121FatoresImpacto_ValorENCSD, A2122FatoresImpacto_ValorENCCD, A2123FatoresImpacto_ValorCMCSD, A2124FatoresImpacto_ValorCMCCD, A2125FatoresImpacto_ValorCNCSD, A2126FatoresImpacto_ValorCNCCD, A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("FatoresImpacto") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FatoresImpacto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate5B232( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel5B232( ) ;
         }
         CloseExtendedTableCursors5B232( ) ;
      }

      protected void DeferredUpdate5B232( )
      {
      }

      protected void delete( )
      {
         BeforeValidate5B232( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5B232( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls5B232( ) ;
            AfterConfirm5B232( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete5B232( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005B12 */
                  pr_default.execute(10, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("FatoresImpacto") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode232 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel5B232( ) ;
         Gx_mode = sMode232;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls5B232( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005B13 */
            pr_default.execute(11, new Object[] {A5AreaTrabalho_Codigo});
            A6AreaTrabalho_Descricao = T005B13_A6AreaTrabalho_Descricao[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel5B232( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete5B232( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "FatoresImpacto");
            if ( AnyError == 0 )
            {
               ConfirmValues5B0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "FatoresImpacto");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart5B232( )
      {
         /* Scan By routine */
         /* Using cursor T005B14 */
         pr_default.execute(12);
         RcdFound232 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound232 = 1;
            A5AreaTrabalho_Codigo = T005B14_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A2127FatoresImpacto_Codigo = T005B14_A2127FatoresImpacto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext5B232( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound232 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound232 = 1;
            A5AreaTrabalho_Codigo = T005B14_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A2127FatoresImpacto_Codigo = T005B14_A2127FatoresImpacto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd5B232( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm5B232( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert5B232( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate5B232( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete5B232( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete5B232( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate5B232( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes5B232( )
      {
         edtFatoresImpacto_ValorEMCSD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorEMCSD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorEMCSD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorEMCCD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorEMCCD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorEMCCD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorENCSD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorENCSD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorENCSD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorENCCD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorENCCD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorENCCD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorCMCSD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorCMCSD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorCMCSD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorCMCCD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorCMCCD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorCMCCD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorCNCSD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorCNCSD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorCNCSD_Enabled), 5, 0)));
         edtFatoresImpacto_ValorCNCCD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_ValorCNCCD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_ValorCNCCD_Enabled), 5, 0)));
         edtAreaTrabalho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         edtFatoresImpacto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFatoresImpacto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFatoresImpacto_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues5B0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282310387");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8AreaTrabalho_Codigo) + "," + UrlEncode("" +AV11FatoresImpacto_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2127FatoresImpacto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.NToC( Z2119FatoresImpacto_ValorEMCSD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.NToC( Z2120FatoresImpacto_ValorEMCCD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.NToC( Z2121FatoresImpacto_ValorENCSD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.NToC( Z2122FatoresImpacto_ValorENCCD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.NToC( Z2123FatoresImpacto_ValorCMCSD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.NToC( Z2124FatoresImpacto_ValorCMCCD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.NToC( Z2125FatoresImpacto_ValorCNCSD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.NToC( Z2126FatoresImpacto_ValorCNCCD, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFATORESIMPACTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11FatoresImpacto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFATORESIMPACTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11FatoresImpacto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "FatoresImpacto";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("fatoresimpacto:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8AreaTrabalho_Codigo) + "," + UrlEncode("" +AV11FatoresImpacto_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "FatoresImpacto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Configuração de Fatores de Impacto na Contagem" ;
      }

      protected void InitializeNonKey5B232( )
      {
         A6AreaTrabalho_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
         A2119FatoresImpacto_ValorEMCSD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
         A2120FatoresImpacto_ValorEMCCD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
         A2121FatoresImpacto_ValorENCSD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
         A2122FatoresImpacto_ValorENCCD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
         A2123FatoresImpacto_ValorCMCSD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
         A2124FatoresImpacto_ValorCMCCD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
         A2125FatoresImpacto_ValorCNCSD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
         A2126FatoresImpacto_ValorCNCCD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
         Z2119FatoresImpacto_ValorEMCSD = 0;
         Z2120FatoresImpacto_ValorEMCCD = 0;
         Z2121FatoresImpacto_ValorENCSD = 0;
         Z2122FatoresImpacto_ValorENCCD = 0;
         Z2123FatoresImpacto_ValorCMCSD = 0;
         Z2124FatoresImpacto_ValorCMCCD = 0;
         Z2125FatoresImpacto_ValorCNCSD = 0;
         Z2126FatoresImpacto_ValorCNCCD = 0;
      }

      protected void InitAll5B232( )
      {
         A5AreaTrabalho_Codigo = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         A2127FatoresImpacto_Codigo = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
         InitializeNonKey5B232( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428231045");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("fatoresimpacto.js", "?2020428231045");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblFatoresimpactotitle_Internalname = "FATORESIMPACTOTITLE";
         lblTextblockfatoresimpacto_valoremcsd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALOREMCSD";
         edtFatoresImpacto_ValorEMCSD_Internalname = "FATORESIMPACTO_VALOREMCSD";
         lblTextblockfatoresimpacto_valoremccd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALOREMCCD";
         edtFatoresImpacto_ValorEMCCD_Internalname = "FATORESIMPACTO_VALOREMCCD";
         lblTextblockfatoresimpacto_valorencsd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALORENCSD";
         edtFatoresImpacto_ValorENCSD_Internalname = "FATORESIMPACTO_VALORENCSD";
         lblTextblockfatoresimpacto_valorenccd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALORENCCD";
         edtFatoresImpacto_ValorENCCD_Internalname = "FATORESIMPACTO_VALORENCCD";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         grpUnnamedgroup2_Internalname = "UNNAMEDGROUP2";
         lblTextblockfatoresimpacto_valorcmcsd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALORCMCSD";
         edtFatoresImpacto_ValorCMCSD_Internalname = "FATORESIMPACTO_VALORCMCSD";
         lblTextblockfatoresimpacto_valorcmccd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALORCMCCD";
         edtFatoresImpacto_ValorCMCCD_Internalname = "FATORESIMPACTO_VALORCMCCD";
         lblTextblockfatoresimpacto_valorcncsd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALORCNCSD";
         edtFatoresImpacto_ValorCNCSD_Internalname = "FATORESIMPACTO_VALORCNCSD";
         lblTextblockfatoresimpacto_valorcnccd_Internalname = "TEXTBLOCKFATORESIMPACTO_VALORCNCCD";
         edtFatoresImpacto_ValorCNCCD_Internalname = "FATORESIMPACTO_VALORCNCCD";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         grpUnnamedgroup4_Internalname = "UNNAMEDGROUP4";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         edtFatoresImpacto_Codigo_Internalname = "FATORESIMPACTO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Fatores de Impacto";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Configuração de Fatores de Impacto na Contagem";
         edtFatoresImpacto_ValorENCCD_Jsonclick = "";
         edtFatoresImpacto_ValorENCCD_Enabled = 1;
         edtFatoresImpacto_ValorENCSD_Jsonclick = "";
         edtFatoresImpacto_ValorENCSD_Enabled = 1;
         edtFatoresImpacto_ValorEMCCD_Jsonclick = "";
         edtFatoresImpacto_ValorEMCCD_Enabled = 1;
         edtFatoresImpacto_ValorEMCSD_Jsonclick = "";
         edtFatoresImpacto_ValorEMCSD_Enabled = 1;
         edtFatoresImpacto_ValorCNCCD_Jsonclick = "";
         edtFatoresImpacto_ValorCNCCD_Enabled = 1;
         edtFatoresImpacto_ValorCNCSD_Jsonclick = "";
         edtFatoresImpacto_ValorCNCSD_Enabled = 1;
         edtFatoresImpacto_ValorCMCCD_Jsonclick = "";
         edtFatoresImpacto_ValorCMCCD_Enabled = 1;
         edtFatoresImpacto_ValorCMCSD_Jsonclick = "";
         edtFatoresImpacto_ValorCMCSD_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFatoresImpacto_Codigo_Jsonclick = "";
         edtFatoresImpacto_Codigo_Enabled = 1;
         edtFatoresImpacto_Codigo_Visible = 1;
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Enabled = 1;
         edtAreaTrabalho_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Areatrabalho_codigo( int GX_Parm1 ,
                                             String GX_Parm2 )
      {
         A5AreaTrabalho_Codigo = GX_Parm1;
         A6AreaTrabalho_Descricao = GX_Parm2;
         /* Using cursor T005B13 */
         pr_default.execute(11, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("Não existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
         }
         A6AreaTrabalho_Descricao = T005B13_A6AreaTrabalho_Descricao[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A6AreaTrabalho_Descricao = "";
         }
         isValidOutput.Add(A6AreaTrabalho_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV11FatoresImpacto_Codigo',fld:'vFATORESIMPACTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E125B2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         lblFatoresimpactotitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcmcsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcmccd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcncsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcnccd_Jsonclick = "";
         lblTextblockfatoresimpacto_valoremcsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valoremccd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorencsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorenccd_Jsonclick = "";
         A6AreaTrabalho_Descricao = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode232 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z6AreaTrabalho_Descricao = "";
         T005B4_A6AreaTrabalho_Descricao = new String[] {""} ;
         T005B5_A2127FatoresImpacto_Codigo = new int[1] ;
         T005B5_A6AreaTrabalho_Descricao = new String[] {""} ;
         T005B5_A2119FatoresImpacto_ValorEMCSD = new decimal[1] ;
         T005B5_A2120FatoresImpacto_ValorEMCCD = new decimal[1] ;
         T005B5_A2121FatoresImpacto_ValorENCSD = new decimal[1] ;
         T005B5_A2122FatoresImpacto_ValorENCCD = new decimal[1] ;
         T005B5_A2123FatoresImpacto_ValorCMCSD = new decimal[1] ;
         T005B5_A2124FatoresImpacto_ValorCMCCD = new decimal[1] ;
         T005B5_A2125FatoresImpacto_ValorCNCSD = new decimal[1] ;
         T005B5_A2126FatoresImpacto_ValorCNCCD = new decimal[1] ;
         T005B5_A5AreaTrabalho_Codigo = new int[1] ;
         T005B6_A6AreaTrabalho_Descricao = new String[] {""} ;
         T005B7_A5AreaTrabalho_Codigo = new int[1] ;
         T005B7_A2127FatoresImpacto_Codigo = new int[1] ;
         T005B3_A2127FatoresImpacto_Codigo = new int[1] ;
         T005B3_A2119FatoresImpacto_ValorEMCSD = new decimal[1] ;
         T005B3_A2120FatoresImpacto_ValorEMCCD = new decimal[1] ;
         T005B3_A2121FatoresImpacto_ValorENCSD = new decimal[1] ;
         T005B3_A2122FatoresImpacto_ValorENCCD = new decimal[1] ;
         T005B3_A2123FatoresImpacto_ValorCMCSD = new decimal[1] ;
         T005B3_A2124FatoresImpacto_ValorCMCCD = new decimal[1] ;
         T005B3_A2125FatoresImpacto_ValorCNCSD = new decimal[1] ;
         T005B3_A2126FatoresImpacto_ValorCNCCD = new decimal[1] ;
         T005B3_A5AreaTrabalho_Codigo = new int[1] ;
         T005B8_A5AreaTrabalho_Codigo = new int[1] ;
         T005B8_A2127FatoresImpacto_Codigo = new int[1] ;
         T005B9_A5AreaTrabalho_Codigo = new int[1] ;
         T005B9_A2127FatoresImpacto_Codigo = new int[1] ;
         T005B2_A2127FatoresImpacto_Codigo = new int[1] ;
         T005B2_A2119FatoresImpacto_ValorEMCSD = new decimal[1] ;
         T005B2_A2120FatoresImpacto_ValorEMCCD = new decimal[1] ;
         T005B2_A2121FatoresImpacto_ValorENCSD = new decimal[1] ;
         T005B2_A2122FatoresImpacto_ValorENCCD = new decimal[1] ;
         T005B2_A2123FatoresImpacto_ValorCMCSD = new decimal[1] ;
         T005B2_A2124FatoresImpacto_ValorCMCCD = new decimal[1] ;
         T005B2_A2125FatoresImpacto_ValorCNCSD = new decimal[1] ;
         T005B2_A2126FatoresImpacto_ValorCNCCD = new decimal[1] ;
         T005B2_A5AreaTrabalho_Codigo = new int[1] ;
         T005B13_A6AreaTrabalho_Descricao = new String[] {""} ;
         T005B14_A5AreaTrabalho_Codigo = new int[1] ;
         T005B14_A2127FatoresImpacto_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.fatoresimpacto__default(),
            new Object[][] {
                new Object[] {
               T005B2_A2127FatoresImpacto_Codigo, T005B2_A2119FatoresImpacto_ValorEMCSD, T005B2_A2120FatoresImpacto_ValorEMCCD, T005B2_A2121FatoresImpacto_ValorENCSD, T005B2_A2122FatoresImpacto_ValorENCCD, T005B2_A2123FatoresImpacto_ValorCMCSD, T005B2_A2124FatoresImpacto_ValorCMCCD, T005B2_A2125FatoresImpacto_ValorCNCSD, T005B2_A2126FatoresImpacto_ValorCNCCD, T005B2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T005B3_A2127FatoresImpacto_Codigo, T005B3_A2119FatoresImpacto_ValorEMCSD, T005B3_A2120FatoresImpacto_ValorEMCCD, T005B3_A2121FatoresImpacto_ValorENCSD, T005B3_A2122FatoresImpacto_ValorENCCD, T005B3_A2123FatoresImpacto_ValorCMCSD, T005B3_A2124FatoresImpacto_ValorCMCCD, T005B3_A2125FatoresImpacto_ValorCNCSD, T005B3_A2126FatoresImpacto_ValorCNCCD, T005B3_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T005B4_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               T005B5_A2127FatoresImpacto_Codigo, T005B5_A6AreaTrabalho_Descricao, T005B5_A2119FatoresImpacto_ValorEMCSD, T005B5_A2120FatoresImpacto_ValorEMCCD, T005B5_A2121FatoresImpacto_ValorENCSD, T005B5_A2122FatoresImpacto_ValorENCCD, T005B5_A2123FatoresImpacto_ValorCMCSD, T005B5_A2124FatoresImpacto_ValorCMCCD, T005B5_A2125FatoresImpacto_ValorCNCSD, T005B5_A2126FatoresImpacto_ValorCNCCD,
               T005B5_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T005B6_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               T005B7_A5AreaTrabalho_Codigo, T005B7_A2127FatoresImpacto_Codigo
               }
               , new Object[] {
               T005B8_A5AreaTrabalho_Codigo, T005B8_A2127FatoresImpacto_Codigo
               }
               , new Object[] {
               T005B9_A5AreaTrabalho_Codigo, T005B9_A2127FatoresImpacto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005B13_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               T005B14_A5AreaTrabalho_Codigo, T005B14_A2127FatoresImpacto_Codigo
               }
            }
         );
         Z2127FatoresImpacto_Codigo = 1;
         A2127FatoresImpacto_Codigo = 1;
         Z5AreaTrabalho_Codigo = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         A5AreaTrabalho_Codigo = AV7WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound232 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV8AreaTrabalho_Codigo ;
      private int wcpOAV11FatoresImpacto_Codigo ;
      private int Z5AreaTrabalho_Codigo ;
      private int Z2127FatoresImpacto_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV8AreaTrabalho_Codigo ;
      private int AV11FatoresImpacto_Codigo ;
      private int trnEnded ;
      private int edtAreaTrabalho_Codigo_Visible ;
      private int edtAreaTrabalho_Codigo_Enabled ;
      private int A2127FatoresImpacto_Codigo ;
      private int edtFatoresImpacto_Codigo_Visible ;
      private int edtFatoresImpacto_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtFatoresImpacto_ValorCMCSD_Enabled ;
      private int edtFatoresImpacto_ValorCMCCD_Enabled ;
      private int edtFatoresImpacto_ValorCNCSD_Enabled ;
      private int edtFatoresImpacto_ValorCNCCD_Enabled ;
      private int edtFatoresImpacto_ValorEMCSD_Enabled ;
      private int edtFatoresImpacto_ValorEMCCD_Enabled ;
      private int edtFatoresImpacto_ValorENCSD_Enabled ;
      private int edtFatoresImpacto_ValorENCCD_Enabled ;
      private int idxLst ;
      private decimal Z2119FatoresImpacto_ValorEMCSD ;
      private decimal Z2120FatoresImpacto_ValorEMCCD ;
      private decimal Z2121FatoresImpacto_ValorENCSD ;
      private decimal Z2122FatoresImpacto_ValorENCCD ;
      private decimal Z2123FatoresImpacto_ValorCMCSD ;
      private decimal Z2124FatoresImpacto_ValorCMCCD ;
      private decimal Z2125FatoresImpacto_ValorCNCSD ;
      private decimal Z2126FatoresImpacto_ValorCNCCD ;
      private decimal A2123FatoresImpacto_ValorCMCSD ;
      private decimal A2124FatoresImpacto_ValorCMCCD ;
      private decimal A2125FatoresImpacto_ValorCNCSD ;
      private decimal A2126FatoresImpacto_ValorCNCCD ;
      private decimal A2119FatoresImpacto_ValorEMCSD ;
      private decimal A2120FatoresImpacto_ValorEMCCD ;
      private decimal A2121FatoresImpacto_ValorENCSD ;
      private decimal A2122FatoresImpacto_ValorENCCD ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFatoresImpacto_ValorEMCSD_Internalname ;
      private String TempTags ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String edtFatoresImpacto_Codigo_Internalname ;
      private String edtFatoresImpacto_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblFatoresimpactotitle_Internalname ;
      private String lblFatoresimpactotitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup2_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockfatoresimpacto_valorcmcsd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcmcsd_Jsonclick ;
      private String edtFatoresImpacto_ValorCMCSD_Internalname ;
      private String edtFatoresImpacto_ValorCMCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorcmccd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcmccd_Jsonclick ;
      private String edtFatoresImpacto_ValorCMCCD_Internalname ;
      private String edtFatoresImpacto_ValorCMCCD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorcncsd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcncsd_Jsonclick ;
      private String edtFatoresImpacto_ValorCNCSD_Internalname ;
      private String edtFatoresImpacto_ValorCNCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorcnccd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcnccd_Jsonclick ;
      private String edtFatoresImpacto_ValorCNCCD_Internalname ;
      private String edtFatoresImpacto_ValorCNCCD_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockfatoresimpacto_valoremcsd_Internalname ;
      private String lblTextblockfatoresimpacto_valoremcsd_Jsonclick ;
      private String edtFatoresImpacto_ValorEMCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valoremccd_Internalname ;
      private String lblTextblockfatoresimpacto_valoremccd_Jsonclick ;
      private String edtFatoresImpacto_ValorEMCCD_Internalname ;
      private String edtFatoresImpacto_ValorEMCCD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorencsd_Internalname ;
      private String lblTextblockfatoresimpacto_valorencsd_Jsonclick ;
      private String edtFatoresImpacto_ValorENCSD_Internalname ;
      private String edtFatoresImpacto_ValorENCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorenccd_Internalname ;
      private String lblTextblockfatoresimpacto_valorenccd_Jsonclick ;
      private String edtFatoresImpacto_ValorENCCD_Internalname ;
      private String edtFatoresImpacto_ValorENCCD_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode232 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A6AreaTrabalho_Descricao ;
      private String Z6AreaTrabalho_Descricao ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T005B4_A6AreaTrabalho_Descricao ;
      private int[] T005B5_A2127FatoresImpacto_Codigo ;
      private String[] T005B5_A6AreaTrabalho_Descricao ;
      private decimal[] T005B5_A2119FatoresImpacto_ValorEMCSD ;
      private decimal[] T005B5_A2120FatoresImpacto_ValorEMCCD ;
      private decimal[] T005B5_A2121FatoresImpacto_ValorENCSD ;
      private decimal[] T005B5_A2122FatoresImpacto_ValorENCCD ;
      private decimal[] T005B5_A2123FatoresImpacto_ValorCMCSD ;
      private decimal[] T005B5_A2124FatoresImpacto_ValorCMCCD ;
      private decimal[] T005B5_A2125FatoresImpacto_ValorCNCSD ;
      private decimal[] T005B5_A2126FatoresImpacto_ValorCNCCD ;
      private int[] T005B5_A5AreaTrabalho_Codigo ;
      private String[] T005B6_A6AreaTrabalho_Descricao ;
      private int[] T005B7_A5AreaTrabalho_Codigo ;
      private int[] T005B7_A2127FatoresImpacto_Codigo ;
      private int[] T005B3_A2127FatoresImpacto_Codigo ;
      private decimal[] T005B3_A2119FatoresImpacto_ValorEMCSD ;
      private decimal[] T005B3_A2120FatoresImpacto_ValorEMCCD ;
      private decimal[] T005B3_A2121FatoresImpacto_ValorENCSD ;
      private decimal[] T005B3_A2122FatoresImpacto_ValorENCCD ;
      private decimal[] T005B3_A2123FatoresImpacto_ValorCMCSD ;
      private decimal[] T005B3_A2124FatoresImpacto_ValorCMCCD ;
      private decimal[] T005B3_A2125FatoresImpacto_ValorCNCSD ;
      private decimal[] T005B3_A2126FatoresImpacto_ValorCNCCD ;
      private int[] T005B3_A5AreaTrabalho_Codigo ;
      private int[] T005B8_A5AreaTrabalho_Codigo ;
      private int[] T005B8_A2127FatoresImpacto_Codigo ;
      private int[] T005B9_A5AreaTrabalho_Codigo ;
      private int[] T005B9_A2127FatoresImpacto_Codigo ;
      private int[] T005B2_A2127FatoresImpacto_Codigo ;
      private decimal[] T005B2_A2119FatoresImpacto_ValorEMCSD ;
      private decimal[] T005B2_A2120FatoresImpacto_ValorEMCCD ;
      private decimal[] T005B2_A2121FatoresImpacto_ValorENCSD ;
      private decimal[] T005B2_A2122FatoresImpacto_ValorENCCD ;
      private decimal[] T005B2_A2123FatoresImpacto_ValorCMCSD ;
      private decimal[] T005B2_A2124FatoresImpacto_ValorCMCCD ;
      private decimal[] T005B2_A2125FatoresImpacto_ValorCNCSD ;
      private decimal[] T005B2_A2126FatoresImpacto_ValorCNCCD ;
      private int[] T005B2_A5AreaTrabalho_Codigo ;
      private String[] T005B13_A6AreaTrabalho_Descricao ;
      private int[] T005B14_A5AreaTrabalho_Codigo ;
      private int[] T005B14_A2127FatoresImpacto_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class fatoresimpacto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT005B5 ;
          prmT005B5 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B4 ;
          prmT005B4 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B6 ;
          prmT005B6 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B7 ;
          prmT005B7 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B3 ;
          prmT005B3 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B8 ;
          prmT005B8 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B9 ;
          prmT005B9 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B2 ;
          prmT005B2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B10 ;
          prmT005B10 = new Object[] {
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_ValorEMCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorEMCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorENCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorENCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCMCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCMCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCNCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCNCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B11 ;
          prmT005B11 = new Object[] {
          new Object[] {"@FatoresImpacto_ValorEMCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorEMCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorENCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorENCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCMCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCMCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCNCSD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@FatoresImpacto_ValorCNCCD",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B12 ;
          prmT005B12 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005B14 ;
          prmT005B14 = new Object[] {
          } ;
          Object[] prmT005B13 ;
          prmT005B13 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T005B2", "SELECT [FatoresImpacto_Codigo], [FatoresImpacto_ValorEMCSD], [FatoresImpacto_ValorEMCCD], [FatoresImpacto_ValorENCSD], [FatoresImpacto_ValorENCCD], [FatoresImpacto_ValorCMCSD], [FatoresImpacto_ValorCMCCD], [FatoresImpacto_ValorCNCSD], [FatoresImpacto_ValorCNCCD], [AreaTrabalho_Codigo] FROM [FatoresImpacto] WITH (UPDLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [FatoresImpacto_Codigo] = @FatoresImpacto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005B2,1,0,true,false )
             ,new CursorDef("T005B3", "SELECT [FatoresImpacto_Codigo], [FatoresImpacto_ValorEMCSD], [FatoresImpacto_ValorEMCCD], [FatoresImpacto_ValorENCSD], [FatoresImpacto_ValorENCCD], [FatoresImpacto_ValorCMCSD], [FatoresImpacto_ValorCMCCD], [FatoresImpacto_ValorCNCSD], [FatoresImpacto_ValorCNCCD], [AreaTrabalho_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [FatoresImpacto_Codigo] = @FatoresImpacto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005B3,1,0,true,false )
             ,new CursorDef("T005B4", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005B4,1,0,true,false )
             ,new CursorDef("T005B5", "SELECT TM1.[FatoresImpacto_Codigo], T2.[AreaTrabalho_Descricao], TM1.[FatoresImpacto_ValorEMCSD], TM1.[FatoresImpacto_ValorEMCCD], TM1.[FatoresImpacto_ValorENCSD], TM1.[FatoresImpacto_ValorENCCD], TM1.[FatoresImpacto_ValorCMCSD], TM1.[FatoresImpacto_ValorCMCCD], TM1.[FatoresImpacto_ValorCNCSD], TM1.[FatoresImpacto_ValorCNCCD], TM1.[AreaTrabalho_Codigo] FROM ([FatoresImpacto] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[AreaTrabalho_Codigo]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and TM1.[FatoresImpacto_Codigo] = @FatoresImpacto_Codigo ORDER BY TM1.[AreaTrabalho_Codigo], TM1.[FatoresImpacto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005B5,100,0,true,false )
             ,new CursorDef("T005B6", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005B6,1,0,true,false )
             ,new CursorDef("T005B7", "SELECT [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [FatoresImpacto_Codigo] = @FatoresImpacto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005B7,1,0,true,false )
             ,new CursorDef("T005B8", "SELECT TOP 1 [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE ( [AreaTrabalho_Codigo] > @AreaTrabalho_Codigo or [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and [FatoresImpacto_Codigo] > @FatoresImpacto_Codigo) ORDER BY [AreaTrabalho_Codigo], [FatoresImpacto_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005B8,1,0,true,true )
             ,new CursorDef("T005B9", "SELECT TOP 1 [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE ( [AreaTrabalho_Codigo] < @AreaTrabalho_Codigo or [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and [FatoresImpacto_Codigo] < @FatoresImpacto_Codigo) ORDER BY [AreaTrabalho_Codigo] DESC, [FatoresImpacto_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005B9,1,0,true,true )
             ,new CursorDef("T005B10", "INSERT INTO [FatoresImpacto]([FatoresImpacto_Codigo], [FatoresImpacto_ValorEMCSD], [FatoresImpacto_ValorEMCCD], [FatoresImpacto_ValorENCSD], [FatoresImpacto_ValorENCCD], [FatoresImpacto_ValorCMCSD], [FatoresImpacto_ValorCMCCD], [FatoresImpacto_ValorCNCSD], [FatoresImpacto_ValorCNCCD], [AreaTrabalho_Codigo]) VALUES(@FatoresImpacto_Codigo, @FatoresImpacto_ValorEMCSD, @FatoresImpacto_ValorEMCCD, @FatoresImpacto_ValorENCSD, @FatoresImpacto_ValorENCCD, @FatoresImpacto_ValorCMCSD, @FatoresImpacto_ValorCMCCD, @FatoresImpacto_ValorCNCSD, @FatoresImpacto_ValorCNCCD, @AreaTrabalho_Codigo)", GxErrorMask.GX_NOMASK,prmT005B10)
             ,new CursorDef("T005B11", "UPDATE [FatoresImpacto] SET [FatoresImpacto_ValorEMCSD]=@FatoresImpacto_ValorEMCSD, [FatoresImpacto_ValorEMCCD]=@FatoresImpacto_ValorEMCCD, [FatoresImpacto_ValorENCSD]=@FatoresImpacto_ValorENCSD, [FatoresImpacto_ValorENCCD]=@FatoresImpacto_ValorENCCD, [FatoresImpacto_ValorCMCSD]=@FatoresImpacto_ValorCMCSD, [FatoresImpacto_ValorCMCCD]=@FatoresImpacto_ValorCMCCD, [FatoresImpacto_ValorCNCSD]=@FatoresImpacto_ValorCNCSD, [FatoresImpacto_ValorCNCCD]=@FatoresImpacto_ValorCNCCD  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [FatoresImpacto_Codigo] = @FatoresImpacto_Codigo", GxErrorMask.GX_NOMASK,prmT005B11)
             ,new CursorDef("T005B12", "DELETE FROM [FatoresImpacto]  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [FatoresImpacto_Codigo] = @FatoresImpacto_Codigo", GxErrorMask.GX_NOMASK,prmT005B12)
             ,new CursorDef("T005B13", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005B13,1,0,true,false )
             ,new CursorDef("T005B14", "SELECT [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) ORDER BY [AreaTrabalho_Codigo], [FatoresImpacto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005B14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(10) ;
                ((int[]) buf[10])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (decimal)parms[7]);
                stmt.SetParameter(9, (decimal)parms[8]);
                stmt.SetParameter(10, (int)parms[9]);
                return;
             case 9 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (decimal)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                stmt.SetParameter(10, (int)parms[9]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
