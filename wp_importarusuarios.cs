/*
               File: WP_ImportarUsuarios
        Description: Importar Usu�rios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:47.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_importarusuarios : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_importarusuarios( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_importarusuarios( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratante_Codigo ,
                           int aP1_Contratada_Codigo )
      {
         this.AV9Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV6Contratada_Codigo = aP1_Contratada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavAll = new GXCheckbox();
         chkavFlag = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_24 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_24_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_24_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               AV6Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Contratada_Codigo), "ZZZZZ9")));
               A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               AV9Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratante_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contratante_Codigo), "ZZZZZ9")));
               A61ContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n61ContratanteUsuario_UsuarioPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
               A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1228ContratadaUsuario_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV17WWPContext);
               A70ContratadaUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n70ContratadaUsuario_UsuarioPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A70ContratadaUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0)));
               A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A57Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV19UsuarioCadastrado);
               AV21Filtro = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro", AV21Filtro);
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A2Usuario_Nome = GetNextPar( );
               n2Usuario_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
               A54Usuario_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
               A487Selected_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A487Selected_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A487Selected_Codigo), 6, 0)));
               AV13Selected = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
               AV12Quantidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Quantidade), 3, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( AV6Contratada_Codigo, A63ContratanteUsuario_ContratanteCod, AV9Contratante_Codigo, A61ContratanteUsuario_UsuarioPessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, AV17WWPContext, A70ContratadaUsuario_UsuarioPessoaCod, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A66ContratadaUsuario_ContratadaCod, A58Usuario_PessoaNom, A57Usuario_PessoaCod, AV19UsuarioCadastrado, AV21Filtro, A1Usuario_Codigo, A2Usuario_Nome, A54Usuario_Ativo, A487Selected_Codigo, AV13Selected, AV12Quantidade) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9Contratante_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratante_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contratante_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV6Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contratada_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Contratada_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABB2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBB2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021294780");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_importarusuarios.aspx") + "?" + UrlEncode("" +AV9Contratante_Codigo) + "," + UrlEncode("" +AV6Contratada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_24", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_24), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV17WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV17WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOCADASTRADO", AV19UsuarioCadastrado);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOCADASTRADO", AV19UsuarioCadastrado);
         }
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_NOME", StringUtil.RTrim( A2Usuario_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_ATIVO", A54Usuario_Ativo);
         GxWebStd.gx_hidden_field( context, "SELECTED_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A487Selected_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Contratada_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBB2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBB2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_importarusuarios.aspx") + "?" + UrlEncode("" +AV9Contratante_Codigo) + "," + UrlEncode("" +AV6Contratada_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ImportarUsuarios" ;
      }

      public override String GetPgmdesc( )
      {
         return "Importar Usu�rios" ;
      }

      protected void WBBB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_BB2( true) ;
         }
         else
         {
            wb_table1_2_BB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BB2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTBB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Importar Usu�rios", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBB0( ) ;
      }

      protected void WSBB2( )
      {
         STARTBB2( ) ;
         EVTBB2( ) ;
      }

      protected void EVTBB2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VALL.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11BB2 */
                              E11BB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12BB2 */
                                    E12BB2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CANCEL'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13BB2 */
                              E13BB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "VFLAG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "VFLAG.CLICK") == 0 ) )
                           {
                              nGXsfl_24_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_24_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_idx), 4, 0)), 4, "0");
                              SubsflControlProps_242( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSUARIO_CODIGO");
                                 GX_FocusControl = edtavUsuario_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV15Usuario_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Usuario_Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV15Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Usuario_Codigo), 6, 0)));
                              }
                              AV10Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFlag_Internalname, AV10Flag);
                              AV16Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_pessoanom_Internalname, AV16Usuario_PessoaNom);
                              AV22Usuario_Nome = StringUtil.Upper( cgiGet( edtavUsuario_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_nome_Internalname, AV22Usuario_Nome);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14BB2 */
                                    E14BB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15BB2 */
                                    E15BB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16BB2 */
                                    E16BB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VFLAG.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17BB2 */
                                    E17BB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13BB2 */
                                    E13BB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavAll.Name = "vALL";
            chkavAll.WebTags = "";
            chkavAll.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAll_Internalname, "TitleCaption", chkavAll.Caption);
            chkavAll.CheckedValue = "false";
            GXCCtl = "vFLAG_" + sGXsfl_24_idx;
            chkavFlag.Name = GXCCtl;
            chkavFlag.WebTags = "";
            chkavFlag.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFlag_Internalname, "TitleCaption", chkavFlag.Caption);
            chkavFlag.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavAll_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_242( ) ;
         while ( nGXsfl_24_idx <= nRC_GXsfl_24 )
         {
            sendrow_242( ) ;
            nGXsfl_24_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_24_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_24_idx+1));
            sGXsfl_24_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_idx), 4, 0)), 4, "0");
            SubsflControlProps_242( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int AV6Contratada_Codigo ,
                                       int A63ContratanteUsuario_ContratanteCod ,
                                       int AV9Contratante_Codigo ,
                                       int A61ContratanteUsuario_UsuarioPessoaCod ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       wwpbaseobjects.SdtWWPContext AV17WWPContext ,
                                       int A70ContratadaUsuario_UsuarioPessoaCod ,
                                       int A5AreaTrabalho_Codigo ,
                                       int A29Contratante_Codigo ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       String A58Usuario_PessoaNom ,
                                       int A57Usuario_PessoaCod ,
                                       IGxCollection AV19UsuarioCadastrado ,
                                       String AV21Filtro ,
                                       int A1Usuario_Codigo ,
                                       String A2Usuario_Nome ,
                                       bool A54Usuario_Ativo ,
                                       int A487Selected_Codigo ,
                                       short AV13Selected ,
                                       short AV12Quantidade )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFBB2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSelected_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelected_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSelected_Enabled), 5, 0)));
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavUsuario_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_codigo_Enabled), 5, 0)));
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
         edtavUsuario_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome_Enabled), 5, 0)));
      }

      protected void RFBB2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 24;
         /* Execute user event: E15BB2 */
         E15BB2 ();
         nGXsfl_24_idx = 1;
         sGXsfl_24_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_idx), 4, 0)), 4, "0");
         SubsflControlProps_242( ) ;
         nGXsfl_24_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Width), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_242( ) ;
            /* Execute user event: E16BB2 */
            E16BB2 ();
            wbEnd = 24;
            WBBB0( ) ;
         }
         nGXsfl_24_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPBB0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSelected_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelected_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSelected_Enabled), 5, 0)));
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavUsuario_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_codigo_Enabled), 5, 0)));
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
         edtavUsuario_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14BB2 */
         E14BB2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5All = StringUtil.StrToBool( cgiGet( chkavAll_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5All", AV5All);
            AV21Filtro = StringUtil.Upper( cgiGet( edtavFiltro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro", AV21Filtro);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSelected_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSelected_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSELECTED");
               GX_FocusControl = edtavSelected_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13Selected = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
            }
            else
            {
               AV13Selected = (short)(context.localUtil.CToN( cgiGet( edtavSelected_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQUANTIDADE");
               GX_FocusControl = edtavQuantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Quantidade), 3, 0)));
            }
            else
            {
               AV12Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Quantidade), 3, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_24 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_24"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14BB2 */
         E14BB2 ();
         if (returnInSub) return;
      }

      protected void E14BB2( )
      {
         /* Start Routine */
         Gx_msg = "Importar usu�rios para a ";
         if ( (0==AV6Contratada_Codigo) )
         {
            /* Using cursor H00BB2 */
            pr_default.execute(0, new Object[] {AV9Contratante_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A29Contratante_Codigo = H00BB2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00BB2_n29Contratante_Codigo[0];
               A10Contratante_NomeFantasia = H00BB2_A10Contratante_NomeFantasia[0];
               Gx_msg = Gx_msg + "contratante " + A10Contratante_NomeFantasia;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         else
         {
            /* Using cursor H00BB3 */
            pr_default.execute(1, new Object[] {AV6Contratada_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A40Contratada_PessoaCod = H00BB3_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = H00BB3_A39Contratada_Codigo[0];
               A41Contratada_PessoaNom = H00BB3_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00BB3_n41Contratada_PessoaNom[0];
               A41Contratada_PessoaNom = H00BB3_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00BB3_n41Contratada_PessoaNom[0];
               Gx_msg = Gx_msg + "contratada " + A41Contratada_PessoaNom;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         lblTbtitulo_Caption = Gx_msg;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbtitulo_Internalname, "Caption", lblTbtitulo_Caption);
         GX_FocusControl = edtavFiltro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
      }

      protected void E15BB2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV17WWPContext) ;
         bttButton1_Visible = (AV17WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttButton1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttButton1_Visible), 5, 0)));
         GXt_int1 = AV17WWPContext.gxTpr_Userid;
         new prc_cleanselected(context ).execute( ref  GXt_int1) ;
         AV17WWPContext.gxTpr_Userid = (short)(GXt_int1);
         /* Execute user subroutine: 'USUARIOSCADASTRADOS' */
         S112 ();
         if (returnInSub) return;
         AV12Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Quantidade), 3, 0)));
         AV13Selected = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17WWPContext", AV17WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19UsuarioCadastrado", AV19UsuarioCadastrado);
      }

      private void E16BB2( )
      {
         /* Grid_Load Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A57Usuario_PessoaCod ,
                                              AV19UsuarioCadastrado ,
                                              AV21Filtro ,
                                              A58Usuario_PessoaNom },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV21Filtro = StringUtil.PadR( StringUtil.RTrim( AV21Filtro), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro", AV21Filtro);
         /* Using cursor H00BB4 */
         pr_default.execute(2, new Object[] {lV21Filtro});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A58Usuario_PessoaNom = H00BB4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00BB4_n58Usuario_PessoaNom[0];
            A57Usuario_PessoaCod = H00BB4_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00BB4_A1Usuario_Codigo[0];
            A54Usuario_Ativo = H00BB4_A54Usuario_Ativo[0];
            A2Usuario_Nome = H00BB4_A2Usuario_Nome[0];
            n2Usuario_Nome = H00BB4_n2Usuario_Nome[0];
            A58Usuario_PessoaNom = H00BB4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00BB4_n58Usuario_PessoaNom[0];
            AV15Usuario_Codigo = A1Usuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Usuario_Codigo), 6, 0)));
            AV22Usuario_Nome = A2Usuario_Nome + (A54Usuario_Ativo ? "" : " (Inativo)");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_nome_Internalname, AV22Usuario_Nome);
            AV16Usuario_PessoaNom = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_pessoanom_Internalname, AV16Usuario_PessoaNom);
            AV30GXLvl42 = 0;
            /* Using cursor H00BB5 */
            pr_default.execute(3, new Object[] {AV17WWPContext.gxTpr_Userid, A1Usuario_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A487Selected_Codigo = H00BB5_A487Selected_Codigo[0];
               A1Usuario_Codigo = H00BB5_A1Usuario_Codigo[0];
               AV30GXLvl42 = 1;
               AV13Selected = (short)(AV13Selected+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
               AV10Flag = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFlag_Internalname, AV10Flag);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            if ( AV30GXLvl42 == 0 )
            {
               AV10Flag = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFlag_Internalname, AV10Flag);
            }
            AV12Quantidade = (short)(AV12Quantidade+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Quantidade), 3, 0)));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 24;
            }
            sendrow_242( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_24_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(24, GridRow);
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void E17BB2( )
      {
         /* Flag_Click Routine */
         if ( AV10Flag )
         {
            AV13Selected = (short)(AV13Selected+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
         }
         else
         {
            AV13Selected = (short)(AV13Selected-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
         }
         GXt_int1 = AV17WWPContext.gxTpr_Userid;
         new prc_selectunselect(context ).execute( ref  GXt_int1, ref  AV15Usuario_Codigo, ref  AV10Flag) ;
         AV17WWPContext.gxTpr_Userid = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Usuario_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFlag_Internalname, AV10Flag);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17WWPContext", AV17WWPContext);
      }

      protected void E11BB2( )
      {
         /* All_Click Routine */
         if ( AV5All )
         {
            AV13Selected = AV12Quantidade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
         }
         else
         {
            AV13Selected = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Selected), 4, 0)));
         }
         /* Start For Each Line */
         nRC_GXsfl_24 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_24"), ",", "."));
         nGXsfl_24_fel_idx = 0;
         while ( nGXsfl_24_fel_idx < nRC_GXsfl_24 )
         {
            nGXsfl_24_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_24_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_24_fel_idx+1));
            sGXsfl_24_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_242( ) ;
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSUARIO_CODIGO");
               GX_FocusControl = edtavUsuario_codigo_Internalname;
               wbErr = true;
               AV15Usuario_Codigo = 0;
            }
            else
            {
               AV15Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", "."));
            }
            AV10Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
            AV16Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
            AV22Usuario_Nome = StringUtil.Upper( cgiGet( edtavUsuario_nome_Internalname));
            if ( AV10Flag != AV5All )
            {
               AV10Flag = AV5All;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFlag_Internalname, AV10Flag);
               GXt_int1 = AV17WWPContext.gxTpr_Userid;
               new prc_selectunselect(context ).execute( ref  GXt_int1, ref  AV15Usuario_Codigo, ref  AV10Flag) ;
               AV17WWPContext.gxTpr_Userid = (short)(GXt_int1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavFlag_Internalname, AV10Flag);
            }
            /* End For Each Line */
         }
         if ( nGXsfl_24_fel_idx == 0 )
         {
            nGXsfl_24_idx = 1;
            sGXsfl_24_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_idx), 4, 0)), 4, "0");
            SubsflControlProps_242( ) ;
         }
         nGXsfl_24_fel_idx = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17WWPContext", AV17WWPContext);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12BB2 */
         E12BB2 ();
         if (returnInSub) return;
      }

      protected void E12BB2( )
      {
         /* Enter Routine */
         /* Start For Each Line */
         nRC_GXsfl_24 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_24"), ",", "."));
         nGXsfl_24_fel_idx = 0;
         while ( nGXsfl_24_fel_idx < nRC_GXsfl_24 )
         {
            nGXsfl_24_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_24_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_24_fel_idx+1));
            sGXsfl_24_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_242( ) ;
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSUARIO_CODIGO");
               GX_FocusControl = edtavUsuario_codigo_Internalname;
               wbErr = true;
               AV15Usuario_Codigo = 0;
            }
            else
            {
               AV15Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavUsuario_codigo_Internalname), ",", "."));
            }
            AV10Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
            AV16Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
            AV22Usuario_Nome = StringUtil.Upper( cgiGet( edtavUsuario_nome_Internalname));
            if ( AV10Flag )
            {
               new prc_importarusuario(context ).execute(  AV9Contratante_Codigo,  AV6Contratada_Codigo,  AV15Usuario_Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratante_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contratante_Codigo), "ZZZZZ9")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Contratada_Codigo), "ZZZZZ9")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUsuario_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Usuario_Codigo), 6, 0)));
            }
            /* End For Each Line */
         }
         if ( nGXsfl_24_fel_idx == 0 )
         {
            nGXsfl_24_idx = 1;
            sGXsfl_24_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_idx), 4, 0)), 4, "0");
            SubsflControlProps_242( ) ;
         }
         nGXsfl_24_fel_idx = 1;
         GXt_int1 = AV17WWPContext.gxTpr_Userid;
         new prc_cleanselected(context ).execute( ref  GXt_int1) ;
         AV17WWPContext.gxTpr_Userid = (short)(GXt_int1);
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17WWPContext", AV17WWPContext);
      }

      protected void E13BB2( )
      {
         /* 'Cancel' Routine */
         GXt_int1 = AV17WWPContext.gxTpr_Userid;
         new prc_cleanselected(context ).execute( ref  GXt_int1) ;
         AV17WWPContext.gxTpr_Userid = (short)(GXt_int1);
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17WWPContext", AV17WWPContext);
      }

      protected void S112( )
      {
         /* 'USUARIOSCADASTRADOS' Routine */
         AV19UsuarioCadastrado.Clear();
         if ( (0==AV6Contratada_Codigo) )
         {
            /* Using cursor H00BB6 */
            pr_default.execute(4, new Object[] {AV9Contratante_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A60ContratanteUsuario_UsuarioCod = H00BB6_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H00BB6_A63ContratanteUsuario_ContratanteCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00BB6_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00BB6_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00BB6_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00BB6_n61ContratanteUsuario_UsuarioPessoaCod[0];
               AV19UsuarioCadastrado.Add(A61ContratanteUsuario_UsuarioPessoaCod, 0);
               pr_default.readNext(4);
            }
            pr_default.close(4);
            /* Using cursor H00BB7 */
            pr_default.execute(5, new Object[] {AV17WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = H00BB7_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00BB7_A69ContratadaUsuario_UsuarioCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00BB7_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00BB7_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00BB7_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00BB7_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00BB7_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00BB7_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00BB7_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00BB7_n70ContratadaUsuario_UsuarioPessoaCod[0];
               AV19UsuarioCadastrado.Add(A70ContratadaUsuario_UsuarioPessoaCod, 0);
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         else
         {
            /* Using cursor H00BB8 */
            pr_default.execute(6, new Object[] {AV17WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A5AreaTrabalho_Codigo = H00BB8_A5AreaTrabalho_Codigo[0];
               A29Contratante_Codigo = H00BB8_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00BB8_n29Contratante_Codigo[0];
               AV23Codigo = A29Contratante_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
            /* Using cursor H00BB9 */
            pr_default.execute(7, new Object[] {AV23Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A60ContratanteUsuario_UsuarioCod = H00BB9_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H00BB9_A63ContratanteUsuario_ContratanteCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00BB9_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00BB9_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00BB9_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00BB9_n61ContratanteUsuario_UsuarioPessoaCod[0];
               AV19UsuarioCadastrado.Add(A61ContratanteUsuario_UsuarioPessoaCod, 0);
               pr_default.readNext(7);
            }
            pr_default.close(7);
            /* Using cursor H00BB10 */
            pr_default.execute(8, new Object[] {AV6Contratada_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H00BB10_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = H00BB10_A66ContratadaUsuario_ContratadaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00BB10_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00BB10_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00BB10_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00BB10_n70ContratadaUsuario_UsuarioPessoaCod[0];
               AV19UsuarioCadastrado.Add(A70ContratadaUsuario_UsuarioPessoaCod, 0);
               pr_default.readNext(8);
            }
            pr_default.close(8);
         }
      }

      protected void wb_table1_2_BB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "left", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"left\" colspan=\"30\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\" class='Table'>") ;
            wb_table2_5_BB2( true) ;
         }
         else
         {
            wb_table2_5_BB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_BB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"30\"  style=\""+CSSHelper.Prettify( "height:20px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"24\">") ;
               sStyleString = "";
               sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(30), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Pessoa F�sica") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Width), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Usuario_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUsuario_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV10Flag));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV16Usuario_PessoaNom));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV22Usuario_Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUsuario_nome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 24 )
         {
            wbEnd = 0;
            nRC_GXsfl_24 = (short)(nGXsfl_24_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\" class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(24), 2, 0)+","+"null"+");", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttButton1_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportarUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(24), 2, 0)+","+"null"+");", "Fechar", bttButton2_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CANCEL\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportarUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BB2e( true) ;
         }
         else
         {
            wb_table1_2_BB2e( false) ;
         }
      }

      protected void wb_table2_5_BB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='TextBlockTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitulo_Internalname, lblTbtitulo_Caption, "", "", lblTbtitulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ImportarUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  style=\""+CSSHelper.Prettify( "height:20px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:10%")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'" + sGXsfl_24_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAll_Internalname, StringUtil.BoolToStr( AV5All), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(13, this, 'true', 'false');gx.ajax.executeCliEvent('e11bb2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;width:65%")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Filtrar por", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarUsuarios.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_24_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_Internalname, StringUtil.RTrim( AV21Filtro), StringUtil.RTrim( context.localUtil.Format( AV21Filtro, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_ImportarUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;width:25%")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_24_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSelected_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Selected), 4, 0, ",", "")), ((edtavSelected_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13Selected), "ZZZ9")) : context.localUtil.Format( (decimal)(AV13Selected), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSelected_Jsonclick, 0, "Attribute", "", "", "", 1, edtavSelected_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarUsuarios.htm");
            context.WriteHtmlText( "/") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_24_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Quantidade), 3, 0, ",", "")), ((edtavQuantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Quantidade), "ZZ9")) : context.localUtil.Format( (decimal)(AV12Quantidade), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQuantidade_Jsonclick, 0, "Attribute", "", "", "", 1, edtavQuantidade_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_WP_ImportarUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_BB2e( true) ;
         }
         else
         {
            wb_table2_5_BB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9Contratante_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contratante_Codigo), "ZZZZZ9")));
         AV6Contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6Contratada_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABB2( ) ;
         WSBB2( ) ;
         WEBB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021294858");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_importarusuarios.js", "?202053021294859");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_242( )
      {
         edtavUsuario_codigo_Internalname = "vUSUARIO_CODIGO_"+sGXsfl_24_idx;
         chkavFlag_Internalname = "vFLAG_"+sGXsfl_24_idx;
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM_"+sGXsfl_24_idx;
         edtavUsuario_nome_Internalname = "vUSUARIO_NOME_"+sGXsfl_24_idx;
      }

      protected void SubsflControlProps_fel_242( )
      {
         edtavUsuario_codigo_Internalname = "vUSUARIO_CODIGO_"+sGXsfl_24_fel_idx;
         chkavFlag_Internalname = "vFLAG_"+sGXsfl_24_fel_idx;
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM_"+sGXsfl_24_fel_idx;
         edtavUsuario_nome_Internalname = "vUSUARIO_NOME_"+sGXsfl_24_fel_idx;
      }

      protected void sendrow_242( )
      {
         SubsflControlProps_242( ) ;
         WBBB0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0x0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_24_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_24_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavUsuario_codigo_Enabled!=0)&&(edtavUsuario_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 25,'',false,'"+sGXsfl_24_idx+"',24)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavUsuario_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Usuario_Codigo), 6, 0, ",", "")),((edtavUsuario_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Usuario_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV15Usuario_Codigo), "ZZZZZ9")),TempTags+((edtavUsuario_codigo_Enabled!=0)&&(edtavUsuario_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavUsuario_codigo_Enabled!=0)&&(edtavUsuario_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,25);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavUsuario_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavUsuario_codigo_Enabled,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)24,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 26,'',false,'"+sGXsfl_24_idx+"',24)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavFlag_Internalname,StringUtil.BoolToStr( AV10Flag),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(26, this, 'true', 'false');gx.ajax.executeCliEvent('e17bb2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,26);\"" : " ")});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavUsuario_pessoanom_Enabled!=0)&&(edtavUsuario_pessoanom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'',false,'"+sGXsfl_24_idx+"',24)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavUsuario_pessoanom_Internalname,StringUtil.RTrim( AV16Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( AV16Usuario_PessoaNom, "@!")),TempTags+((edtavUsuario_pessoanom_Enabled!=0)&&(edtavUsuario_pessoanom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavUsuario_pessoanom_Enabled!=0)&&(edtavUsuario_pessoanom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavUsuario_pessoanom_Jsonclick,(short)0,(String)"BootstrapAttribute100",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavUsuario_pessoanom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)24,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavUsuario_nome_Enabled!=0)&&(edtavUsuario_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 28,'',false,'"+sGXsfl_24_idx+"',24)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavUsuario_nome_Internalname,StringUtil.RTrim( AV22Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( AV22Usuario_Nome, "@!")),TempTags+((edtavUsuario_nome_Enabled!=0)&&(edtavUsuario_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavUsuario_nome_Enabled!=0)&&(edtavUsuario_nome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavUsuario_nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavUsuario_nome_Enabled,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)24,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         GridContainer.AddRow(GridRow);
         nGXsfl_24_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_24_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_24_idx+1));
         sGXsfl_24_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_24_idx), 4, 0)), 4, "0");
         SubsflControlProps_242( ) ;
         /* End function sendrow_242 */
      }

      protected void init_default_properties( )
      {
         lblTbtitulo_Internalname = "TBTITULO";
         chkavAll_Internalname = "vALL";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavFiltro_Internalname = "vFILTRO";
         edtavSelected_Internalname = "vSELECTED";
         edtavQuantidade_Internalname = "vQUANTIDADE";
         tblTable2_Internalname = "TABLE2";
         edtavUsuario_codigo_Internalname = "vUSUARIO_CODIGO";
         chkavFlag_Internalname = "vFLAG";
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM";
         edtavUsuario_nome_Internalname = "vUSUARIO_NOME";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavUsuario_nome_Jsonclick = "";
         edtavUsuario_nome_Visible = -1;
         edtavUsuario_pessoanom_Jsonclick = "";
         edtavUsuario_pessoanom_Visible = -1;
         chkavFlag.Visible = -1;
         chkavFlag.Enabled = 1;
         edtavUsuario_codigo_Jsonclick = "";
         edtavUsuario_codigo_Visible = 0;
         edtavQuantidade_Jsonclick = "";
         edtavQuantidade_Enabled = 1;
         edtavSelected_Jsonclick = "";
         edtavSelected_Enabled = 1;
         edtavFiltro_Jsonclick = "";
         bttButton1_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavUsuario_nome_Enabled = 1;
         edtavUsuario_pessoanom_Enabled = 1;
         edtavUsuario_codigo_Enabled = 1;
         subGrid_Class = "Grid";
         lblTbtitulo_Caption = "Importar usu�rios para a contratada XXXXXXXXXX";
         subGrid_Width = 100;
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorstyle = 0;
         chkavFlag.Caption = "";
         chkavAll.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Importar Usu�rios";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19UsuarioCadastrado',fld:'vUSUARIOCADASTRADO',pic:'',nv:null},{av:'AV21Filtro',fld:'vFILTRO',pic:'@!',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2Usuario_Nome',fld:'USUARIO_NOME',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A487Selected_Codigo',fld:'SELECTED_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV12Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV6Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV9Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A61ContratanteUsuario_UsuarioPessoaCod',fld:'CONTRATANTEUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A70ContratadaUsuario_UsuarioPessoaCod',fld:'CONTRATADAUSUARIO_USUARIOPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'BUTTON1',prop:'Visible'},{av:'AV12Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV19UsuarioCadastrado',fld:'vUSUARIOCADASTRADO',pic:'',nv:null},{av:'AV23Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E16BB2',iparms:[{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A57Usuario_PessoaCod',fld:'USUARIO_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV19UsuarioCadastrado',fld:'vUSUARIOCADASTRADO',pic:'',nv:null},{av:'AV21Filtro',fld:'vFILTRO',pic:'@!',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2Usuario_Nome',fld:'USUARIO_NOME',pic:'@!',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A487Selected_Codigo',fld:'SELECTED_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV12Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0}],oparms:[{av:'AV15Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22Usuario_Nome',fld:'vUSUARIO_NOME',pic:'@!',nv:''},{av:'AV16Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV10Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV12Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0}]}");
         setEventMetadata("VFLAG.CLICK","{handler:'E17BB2',iparms:[{av:'AV10Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV15Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV10Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV15Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("VALL.CLICK","{handler:'E11BB2',iparms:[{av:'AV5All',fld:'vALL',pic:'',nv:false},{av:'AV12Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV10Flag',fld:'vFLAG',grid:24,pic:'',nv:false},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV15Usuario_Codigo',fld:'vUSUARIO_CODIGO',grid:24,pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV13Selected',fld:'vSELECTED',pic:'ZZZ9',nv:0},{av:'AV10Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV15Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E12BB2',iparms:[{av:'AV10Flag',fld:'vFLAG',grid:24,pic:'',nv:false},{av:'AV9Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15Usuario_Codigo',fld:'vUSUARIO_CODIGO',grid:24,pic:'ZZZZZ9',nv:0},{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("'CANCEL'","{handler:'E13BB2',iparms:[{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV17WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV17WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A58Usuario_PessoaNom = "";
         AV19UsuarioCadastrado = new GxSimpleCollection();
         AV21Filtro = "";
         A2Usuario_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Usuario_PessoaNom = "";
         AV22Usuario_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         Gx_msg = "";
         scmdbuf = "";
         H00BB2_A29Contratante_Codigo = new int[1] ;
         H00BB2_n29Contratante_Codigo = new bool[] {false} ;
         H00BB2_A10Contratante_NomeFantasia = new String[] {""} ;
         A10Contratante_NomeFantasia = "";
         H00BB3_A40Contratada_PessoaCod = new int[1] ;
         H00BB3_A39Contratada_Codigo = new int[1] ;
         H00BB3_A41Contratada_PessoaNom = new String[] {""} ;
         H00BB3_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         lV21Filtro = "";
         H00BB4_A58Usuario_PessoaNom = new String[] {""} ;
         H00BB4_n58Usuario_PessoaNom = new bool[] {false} ;
         H00BB4_A57Usuario_PessoaCod = new int[1] ;
         H00BB4_A1Usuario_Codigo = new int[1] ;
         H00BB4_A54Usuario_Ativo = new bool[] {false} ;
         H00BB4_A2Usuario_Nome = new String[] {""} ;
         H00BB4_n2Usuario_Nome = new bool[] {false} ;
         H00BB5_A488Selected_Flag = new bool[] {false} ;
         H00BB5_A487Selected_Codigo = new int[1] ;
         H00BB5_A1Usuario_Codigo = new int[1] ;
         GridRow = new GXWebRow();
         H00BB6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00BB6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00BB6_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00BB6_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00BB7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00BB7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00BB7_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00BB7_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00BB7_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00BB7_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00BB8_A5AreaTrabalho_Codigo = new int[1] ;
         H00BB8_A29Contratante_Codigo = new int[1] ;
         H00BB8_n29Contratante_Codigo = new bool[] {false} ;
         H00BB9_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00BB9_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00BB9_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00BB9_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00BB10_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00BB10_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00BB10_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00BB10_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         lblTbtitulo_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_importarusuarios__default(),
            new Object[][] {
                new Object[] {
               H00BB2_A29Contratante_Codigo, H00BB2_A10Contratante_NomeFantasia
               }
               , new Object[] {
               H00BB3_A40Contratada_PessoaCod, H00BB3_A39Contratada_Codigo, H00BB3_A41Contratada_PessoaNom, H00BB3_n41Contratada_PessoaNom
               }
               , new Object[] {
               H00BB4_A58Usuario_PessoaNom, H00BB4_n58Usuario_PessoaNom, H00BB4_A57Usuario_PessoaCod, H00BB4_A1Usuario_Codigo, H00BB4_A54Usuario_Ativo, H00BB4_A2Usuario_Nome, H00BB4_n2Usuario_Nome
               }
               , new Object[] {
               H00BB5_A488Selected_Flag, H00BB5_A487Selected_Codigo, H00BB5_A1Usuario_Codigo
               }
               , new Object[] {
               H00BB6_A60ContratanteUsuario_UsuarioCod, H00BB6_A63ContratanteUsuario_ContratanteCod, H00BB6_A61ContratanteUsuario_UsuarioPessoaCod, H00BB6_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               H00BB7_A66ContratadaUsuario_ContratadaCod, H00BB7_A69ContratadaUsuario_UsuarioCod, H00BB7_A1228ContratadaUsuario_AreaTrabalhoCod, H00BB7_n1228ContratadaUsuario_AreaTrabalhoCod, H00BB7_A70ContratadaUsuario_UsuarioPessoaCod, H00BB7_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               H00BB8_A5AreaTrabalho_Codigo, H00BB8_A29Contratante_Codigo, H00BB8_n29Contratante_Codigo
               }
               , new Object[] {
               H00BB9_A60ContratanteUsuario_UsuarioCod, H00BB9_A63ContratanteUsuario_ContratanteCod, H00BB9_A61ContratanteUsuario_UsuarioPessoaCod, H00BB9_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               H00BB10_A69ContratadaUsuario_UsuarioCod, H00BB10_A66ContratadaUsuario_ContratadaCod, H00BB10_A70ContratadaUsuario_UsuarioPessoaCod, H00BB10_n70ContratadaUsuario_UsuarioPessoaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSelected_Enabled = 0;
         edtavQuantidade_Enabled = 0;
         edtavUsuario_codigo_Enabled = 0;
         edtavUsuario_pessoanom_Enabled = 0;
         edtavUsuario_nome_Enabled = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_24 ;
      private short nGXsfl_24_idx=1 ;
      private short AV13Selected ;
      private short AV12Quantidade ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_24_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short GRID_nEOF ;
      private short AV30GXLvl42 ;
      private short nGXsfl_24_fel_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV9Contratante_Codigo ;
      private int AV6Contratada_Codigo ;
      private int wcpOAV9Contratante_Codigo ;
      private int wcpOAV6Contratada_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A487Selected_Codigo ;
      private int AV15Usuario_Codigo ;
      private int subGrid_Islastpage ;
      private int edtavSelected_Enabled ;
      private int edtavQuantidade_Enabled ;
      private int edtavUsuario_codigo_Enabled ;
      private int edtavUsuario_pessoanom_Enabled ;
      private int edtavUsuario_nome_Enabled ;
      private int subGrid_Titleforecolor ;
      private int subGrid_Width ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int bttButton1_Visible ;
      private int GXt_int1 ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV23Codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavUsuario_codigo_Visible ;
      private int edtavUsuario_pessoanom_Visible ;
      private int edtavUsuario_nome_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_24_idx="0001" ;
      private String A58Usuario_PessoaNom ;
      private String AV21Filtro ;
      private String A2Usuario_Nome ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUsuario_codigo_Internalname ;
      private String chkavFlag_Internalname ;
      private String AV16Usuario_PessoaNom ;
      private String edtavUsuario_pessoanom_Internalname ;
      private String AV22Usuario_Nome ;
      private String edtavUsuario_nome_Internalname ;
      private String chkavAll_Internalname ;
      private String GXCCtl ;
      private String edtavSelected_Internalname ;
      private String edtavQuantidade_Internalname ;
      private String edtavFiltro_Internalname ;
      private String Gx_msg ;
      private String scmdbuf ;
      private String A10Contratante_NomeFantasia ;
      private String A41Contratada_PessoaNom ;
      private String lblTbtitulo_Caption ;
      private String lblTbtitulo_Internalname ;
      private String bttButton1_Internalname ;
      private String lV21Filtro ;
      private String sGXsfl_24_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTbtitulo_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavFiltro_Jsonclick ;
      private String edtavSelected_Jsonclick ;
      private String edtavQuantidade_Jsonclick ;
      private String ROClassString ;
      private String edtavUsuario_codigo_Jsonclick ;
      private String edtavUsuario_pessoanom_Jsonclick ;
      private String edtavUsuario_nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n29Contratante_Codigo ;
      private bool n58Usuario_PessoaNom ;
      private bool n2Usuario_Nome ;
      private bool A54Usuario_Ativo ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV10Flag ;
      private bool AV5All ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool gx_refresh_fired ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavAll ;
      private GXCheckbox chkavFlag ;
      private IDataStoreProvider pr_default ;
      private int[] H00BB2_A29Contratante_Codigo ;
      private bool[] H00BB2_n29Contratante_Codigo ;
      private String[] H00BB2_A10Contratante_NomeFantasia ;
      private int[] H00BB3_A40Contratada_PessoaCod ;
      private int[] H00BB3_A39Contratada_Codigo ;
      private String[] H00BB3_A41Contratada_PessoaNom ;
      private bool[] H00BB3_n41Contratada_PessoaNom ;
      private String[] H00BB4_A58Usuario_PessoaNom ;
      private bool[] H00BB4_n58Usuario_PessoaNom ;
      private int[] H00BB4_A57Usuario_PessoaCod ;
      private int[] H00BB4_A1Usuario_Codigo ;
      private bool[] H00BB4_A54Usuario_Ativo ;
      private String[] H00BB4_A2Usuario_Nome ;
      private bool[] H00BB4_n2Usuario_Nome ;
      private bool[] H00BB5_A488Selected_Flag ;
      private int[] H00BB5_A487Selected_Codigo ;
      private int[] H00BB5_A1Usuario_Codigo ;
      private int[] H00BB6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00BB6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00BB6_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00BB6_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00BB7_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00BB7_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00BB7_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00BB7_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00BB7_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00BB7_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00BB8_A5AreaTrabalho_Codigo ;
      private int[] H00BB8_A29Contratante_Codigo ;
      private bool[] H00BB8_n29Contratante_Codigo ;
      private int[] H00BB9_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00BB9_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00BB9_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00BB9_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00BB10_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00BB10_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00BB10_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00BB10_n70ContratadaUsuario_UsuarioPessoaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV19UsuarioCadastrado ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV17WWPContext ;
   }

   public class wp_importarusuarios__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BB4( IGxContext context ,
                                             int A57Usuario_PessoaCod ,
                                             IGxCollection AV19UsuarioCadastrado ,
                                             String AV21Filtro ,
                                             String A58Usuario_PessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [1] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T1.[Usuario_Ativo], T1.[Usuario_Nome] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19UsuarioCadastrado, "T1.[Usuario_PessoaCod] IN (", ")") + ")";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Filtro)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV21Filtro + '%')";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Nome]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00BB4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BB2 ;
          prmH00BB2 = new Object[] {
          new Object[] {"@AV9Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB3 ;
          prmH00BB3 = new Object[] {
          new Object[] {"@AV6Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB5 ;
          prmH00BB5 = new Object[] {
          new Object[] {"@AV17WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB6 ;
          prmH00BB6 = new Object[] {
          new Object[] {"@AV9Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB7 ;
          prmH00BB7 = new Object[] {
          new Object[] {"@AV17WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB8 ;
          prmH00BB8 = new Object[] {
          new Object[] {"@AV17WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB9 ;
          prmH00BB9 = new Object[] {
          new Object[] {"@AV23Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB10 ;
          prmH00BB10 = new Object[] {
          new Object[] {"@AV6Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BB4 ;
          prmH00BB4 = new Object[] {
          new Object[] {"@lV21Filtro",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BB2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_NomeFantasia] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @AV9Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB2,1,0,false,true )
             ,new CursorDef("H00BB3", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV6Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB3,1,0,false,true )
             ,new CursorDef("H00BB4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB4,100,0,true,false )
             ,new CursorDef("H00BB5", "SELECT TOP 1 [Selected_Flag], [Selected_Codigo], [Usuario_Codigo] FROM [Selected] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV17WWPContext__Userid and [Selected_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo], [Selected_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB5,1,0,false,true )
             ,new CursorDef("H00BB6", "SELECT T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV9Contratante_Codigo ORDER BY T1.[ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB6,100,0,false,false )
             ,new CursorDef("H00BB7", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE T2.[Contratada_AreaTrabalhoCod] = @AV17WWPC_1Areatrabalho_codigo ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB7,100,0,false,false )
             ,new CursorDef("H00BB8", "SELECT TOP 1 [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV17WWPC_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB8,1,0,false,true )
             ,new CursorDef("H00BB9", "SELECT T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV23Codigo ORDER BY T1.[ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB9,100,0,false,false )
             ,new CursorDef("H00BB10", "SELECT T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV6Contratada_Codigo ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BB10,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
