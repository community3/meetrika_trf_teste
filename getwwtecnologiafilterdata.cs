/*
               File: GetWWTecnologiaFilterData
        Description: Get WWTecnologia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:17.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwtecnologiafilterdata : GXProcedure
   {
      public getwwtecnologiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwtecnologiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwtecnologiafilterdata objgetwwtecnologiafilterdata;
         objgetwwtecnologiafilterdata = new getwwtecnologiafilterdata();
         objgetwwtecnologiafilterdata.AV16DDOName = aP0_DDOName;
         objgetwwtecnologiafilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwtecnologiafilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwtecnologiafilterdata.AV20OptionsJson = "" ;
         objgetwwtecnologiafilterdata.AV23OptionsDescJson = "" ;
         objgetwwtecnologiafilterdata.AV25OptionIndexesJson = "" ;
         objgetwwtecnologiafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwtecnologiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwtecnologiafilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwtecnologiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_TECNOLOGIA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTECNOLOGIA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWTecnologiaGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWTecnologiaGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWTecnologiaGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_NOME") == 0 )
            {
               AV10TFTecnologia_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_NOME_SEL") == 0 )
            {
               AV11TFTecnologia_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_TIPOTECNOLOGIA_SEL") == 0 )
            {
               AV12TFTecnologia_TipoTecnologia_SelsJson = AV30GridStateFilterValue.gxTpr_Value;
               AV13TFTecnologia_TipoTecnologia_Sels.FromJSonString(AV12TFTecnologia_TipoTecnologia_SelsJson);
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 )
            {
               AV33Tecnologia_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
            {
               AV34Tecnologia_TipoTecnologia1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTECNOLOGIA_NOMEOPTIONS' Routine */
         AV10TFTecnologia_Nome = AV14SearchTxt;
         AV11TFTecnologia_Nome_Sel = "";
         AV39WWTecnologiaDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV40WWTecnologiaDS_2_Tecnologia_nome1 = AV33Tecnologia_Nome1;
         AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV34Tecnologia_TipoTecnologia1;
         AV42WWTecnologiaDS_4_Tftecnologia_nome = AV10TFTecnologia_Nome;
         AV43WWTecnologiaDS_5_Tftecnologia_nome_sel = AV11TFTecnologia_Nome_Sel;
         AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV13TFTecnologia_TipoTecnologia_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A355Tecnologia_TipoTecnologia ,
                                              AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ,
                                              AV39WWTecnologiaDS_1_Dynamicfiltersselector1 ,
                                              AV40WWTecnologiaDS_2_Tecnologia_nome1 ,
                                              AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ,
                                              AV43WWTecnologiaDS_5_Tftecnologia_nome_sel ,
                                              AV42WWTecnologiaDS_4_Tftecnologia_nome ,
                                              AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels.Count ,
                                              A132Tecnologia_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV40WWTecnologiaDS_2_Tecnologia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV40WWTecnologiaDS_2_Tecnologia_nome1), 50, "%");
         lV42WWTecnologiaDS_4_Tftecnologia_nome = StringUtil.PadR( StringUtil.RTrim( AV42WWTecnologiaDS_4_Tftecnologia_nome), 50, "%");
         /* Using cursor P00HA2 */
         pr_default.execute(0, new Object[] {lV40WWTecnologiaDS_2_Tecnologia_nome1, AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1, lV42WWTecnologiaDS_4_Tftecnologia_nome, AV43WWTecnologiaDS_5_Tftecnologia_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKHA2 = false;
            A132Tecnologia_Nome = P00HA2_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = P00HA2_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = P00HA2_n355Tecnologia_TipoTecnologia[0];
            A131Tecnologia_Codigo = P00HA2_A131Tecnologia_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00HA2_A132Tecnologia_Nome[0], A132Tecnologia_Nome) == 0 ) )
            {
               BRKHA2 = false;
               A131Tecnologia_Codigo = P00HA2_A131Tecnologia_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKHA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A132Tecnologia_Nome)) )
            {
               AV18Option = A132Tecnologia_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHA2 )
            {
               BRKHA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTecnologia_Nome = "";
         AV11TFTecnologia_Nome_Sel = "";
         AV12TFTecnologia_TipoTecnologia_SelsJson = "";
         AV13TFTecnologia_TipoTecnologia_Sels = new GxSimpleCollection();
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33Tecnologia_Nome1 = "";
         AV34Tecnologia_TipoTecnologia1 = "";
         AV39WWTecnologiaDS_1_Dynamicfiltersselector1 = "";
         AV40WWTecnologiaDS_2_Tecnologia_nome1 = "";
         AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = "";
         AV42WWTecnologiaDS_4_Tftecnologia_nome = "";
         AV43WWTecnologiaDS_5_Tftecnologia_nome_sel = "";
         AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV40WWTecnologiaDS_2_Tecnologia_nome1 = "";
         lV42WWTecnologiaDS_4_Tftecnologia_nome = "";
         A355Tecnologia_TipoTecnologia = "";
         A132Tecnologia_Nome = "";
         P00HA2_A132Tecnologia_Nome = new String[] {""} ;
         P00HA2_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         P00HA2_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         P00HA2_A131Tecnologia_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwtecnologiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00HA2_A132Tecnologia_Nome, P00HA2_A355Tecnologia_TipoTecnologia, P00HA2_n355Tecnologia_TipoTecnologia, P00HA2_A131Tecnologia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV37GXV1 ;
      private int AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count ;
      private int A131Tecnologia_Codigo ;
      private long AV26count ;
      private String AV10TFTecnologia_Nome ;
      private String AV11TFTecnologia_Nome_Sel ;
      private String AV33Tecnologia_Nome1 ;
      private String AV34Tecnologia_TipoTecnologia1 ;
      private String AV40WWTecnologiaDS_2_Tecnologia_nome1 ;
      private String AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ;
      private String AV42WWTecnologiaDS_4_Tftecnologia_nome ;
      private String AV43WWTecnologiaDS_5_Tftecnologia_nome_sel ;
      private String scmdbuf ;
      private String lV40WWTecnologiaDS_2_Tecnologia_nome1 ;
      private String lV42WWTecnologiaDS_4_Tftecnologia_nome ;
      private String A355Tecnologia_TipoTecnologia ;
      private String A132Tecnologia_Nome ;
      private bool returnInSub ;
      private bool BRKHA2 ;
      private bool n355Tecnologia_TipoTecnologia ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV12TFTecnologia_TipoTecnologia_SelsJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV39WWTecnologiaDS_1_Dynamicfiltersselector1 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00HA2_A132Tecnologia_Nome ;
      private String[] P00HA2_A355Tecnologia_TipoTecnologia ;
      private bool[] P00HA2_n355Tecnologia_TipoTecnologia ;
      private int[] P00HA2_A131Tecnologia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFTecnologia_TipoTecnologia_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwtecnologiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00HA2( IGxContext context ,
                                             String A355Tecnologia_TipoTecnologia ,
                                             IGxCollection AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ,
                                             String AV39WWTecnologiaDS_1_Dynamicfiltersselector1 ,
                                             String AV40WWTecnologiaDS_2_Tecnologia_nome1 ,
                                             String AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ,
                                             String AV43WWTecnologiaDS_5_Tftecnologia_nome_sel ,
                                             String AV42WWTecnologiaDS_4_Tftecnologia_nome ,
                                             int AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count ,
                                             String A132Tecnologia_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia], [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV39WWTecnologiaDS_1_Dynamicfiltersselector1, "TECNOLOGIA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWTecnologiaDS_2_Tecnologia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV40WWTecnologiaDS_2_Tecnologia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV40WWTecnologiaDS_2_Tecnologia_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39WWTecnologiaDS_1_Dynamicfiltersselector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43WWTecnologiaDS_5_Tftecnologia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42WWTecnologiaDS_4_Tftecnologia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV42WWTecnologiaDS_4_Tftecnologia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV42WWTecnologiaDS_4_Tftecnologia_nome)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43WWTecnologiaDS_5_Tftecnologia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] = @AV43WWTecnologiaDS_5_Tftecnologia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] = @AV43WWTecnologiaDS_5_Tftecnologia_nome_sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Tecnologia_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00HA2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00HA2 ;
          prmP00HA2 = new Object[] {
          new Object[] {"@lV40WWTecnologiaDS_2_Tecnologia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41WWTecnologiaDS_3_Tecnologia_tipotecnologia1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV42WWTecnologiaDS_4_Tftecnologia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43WWTecnologiaDS_5_Tftecnologia_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00HA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HA2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwtecnologiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwtecnologiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwtecnologiafilterdata") )
          {
             return  ;
          }
          getwwtecnologiafilterdata worker = new getwwtecnologiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
