/*
               File: PRC_FuncaoAPFAtributosDLT
        Description: Deleta um atributo da Fun��o APF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaoapfatributosdlt : GXProcedure
   {
      public prc_funcaoapfatributosdlt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaoapfatributosdlt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_Codigo ,
                           ref int aP1_FuncaoAPFAtributos_AtributosCod )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A364FuncaoAPFAtributos_AtributosCod = aP1_FuncaoAPFAtributos_AtributosCod;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPFAtributos_AtributosCod=this.A364FuncaoAPFAtributos_AtributosCod;
      }

      public int executeUdp( ref int aP0_FuncaoAPF_Codigo )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A364FuncaoAPFAtributos_AtributosCod = aP1_FuncaoAPFAtributos_AtributosCod;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPFAtributos_AtributosCod=this.A364FuncaoAPFAtributos_AtributosCod;
         return A364FuncaoAPFAtributos_AtributosCod ;
      }

      public void executeSubmit( ref int aP0_FuncaoAPF_Codigo ,
                                 ref int aP1_FuncaoAPFAtributos_AtributosCod )
      {
         prc_funcaoapfatributosdlt objprc_funcaoapfatributosdlt;
         objprc_funcaoapfatributosdlt = new prc_funcaoapfatributosdlt();
         objprc_funcaoapfatributosdlt.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         objprc_funcaoapfatributosdlt.A364FuncaoAPFAtributos_AtributosCod = aP1_FuncaoAPFAtributos_AtributosCod;
         objprc_funcaoapfatributosdlt.context.SetSubmitInitialConfig(context);
         objprc_funcaoapfatributosdlt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaoapfatributosdlt);
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPFAtributos_AtributosCod=this.A364FuncaoAPFAtributos_AtributosCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaoapfatributosdlt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P002F2 */
         pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
         /* End optimized DELETE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_FuncaoAPFAtributosDLT");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaoapfatributosdlt__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_Codigo ;
      private int aP1_FuncaoAPFAtributos_AtributosCod ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_funcaoapfatributosdlt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002F2 ;
          prmP002F2 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002F2", "DELETE FROM [FuncoesAPFAtributos]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002F2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
