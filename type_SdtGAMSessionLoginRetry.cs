/*
               File: type_SdtGAMSessionLoginRetry
        Description: GAMSessionLoginRetry
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:17.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSessionLoginRetry : GxUserType, IGxExternalObject
   {
      public SdtGAMSessionLoginRetry( )
      {
         initialize();
      }

      public SdtGAMSessionLoginRetry( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSessionLoginRetry_externalReference == null )
         {
            GAMSessionLoginRetry_externalReference = new Artech.Security.GAMSessionLoginRetry(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSessionLoginRetry_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Userlogin
      {
         get {
            if ( GAMSessionLoginRetry_externalReference == null )
            {
               GAMSessionLoginRetry_externalReference = new Artech.Security.GAMSessionLoginRetry(context);
            }
            return GAMSessionLoginRetry_externalReference.UserLogin ;
         }

         set {
            if ( GAMSessionLoginRetry_externalReference == null )
            {
               GAMSessionLoginRetry_externalReference = new Artech.Security.GAMSessionLoginRetry(context);
            }
            GAMSessionLoginRetry_externalReference.UserLogin = value;
         }

      }

      public short gxTpr_Number
      {
         get {
            if ( GAMSessionLoginRetry_externalReference == null )
            {
               GAMSessionLoginRetry_externalReference = new Artech.Security.GAMSessionLoginRetry(context);
            }
            return GAMSessionLoginRetry_externalReference.Number ;
         }

         set {
            if ( GAMSessionLoginRetry_externalReference == null )
            {
               GAMSessionLoginRetry_externalReference = new Artech.Security.GAMSessionLoginRetry(context);
            }
            GAMSessionLoginRetry_externalReference.Number = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSessionLoginRetry_externalReference == null )
            {
               GAMSessionLoginRetry_externalReference = new Artech.Security.GAMSessionLoginRetry(context);
            }
            return GAMSessionLoginRetry_externalReference ;
         }

         set {
            GAMSessionLoginRetry_externalReference = (Artech.Security.GAMSessionLoginRetry)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSessionLoginRetry GAMSessionLoginRetry_externalReference=null ;
   }

}
