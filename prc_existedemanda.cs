/*
               File: PRC_ExisteDemanda
        Description: Existe Demanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:22.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existedemanda : GXProcedure
   {
      public prc_existedemanda( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existedemanda( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_ContagemResultado_Demanda ,
                           int aP1_ContagemResultado_SistemaCod ,
                           String aP2_ContagemResultado_DemandaFM ,
                           int aP3_Contratada_AreaTrabalhoCod ,
                           bool aP4_AreaTrabalho_ValidaOSFM ,
                           ref int aP5_Codigo ,
                           int aP6_Servico ,
                           int aP7_ContagemResultado_ContratadaCod ,
                           out String aP8_QuemCadastrou ,
                           out bool aP9_Flag )
      {
         this.A457ContagemResultado_Demanda = aP0_ContagemResultado_Demanda;
         this.AV14ContagemResultado_SistemaCod = aP1_ContagemResultado_SistemaCod;
         this.AV16ContagemResultado_DemandaFM = aP2_ContagemResultado_DemandaFM;
         this.AV13Contratada_AreaTrabalhoCod = aP3_Contratada_AreaTrabalhoCod;
         this.AV15AreaTrabalho_ValidaOSFM = aP4_AreaTrabalho_ValidaOSFM;
         this.AV8Codigo = aP5_Codigo;
         this.AV18Servico = aP6_Servico;
         this.AV17ContagemResultado_ContratadaCod = aP7_ContagemResultado_ContratadaCod;
         this.AV10QuemCadastrou = "" ;
         this.AV9Flag = false ;
         initialize();
         executePrivate();
         aP5_Codigo=this.AV8Codigo;
         aP8_QuemCadastrou=this.AV10QuemCadastrou;
         aP9_Flag=this.AV9Flag;
      }

      public bool executeUdp( String aP0_ContagemResultado_Demanda ,
                              int aP1_ContagemResultado_SistemaCod ,
                              String aP2_ContagemResultado_DemandaFM ,
                              int aP3_Contratada_AreaTrabalhoCod ,
                              bool aP4_AreaTrabalho_ValidaOSFM ,
                              ref int aP5_Codigo ,
                              int aP6_Servico ,
                              int aP7_ContagemResultado_ContratadaCod ,
                              out String aP8_QuemCadastrou )
      {
         this.A457ContagemResultado_Demanda = aP0_ContagemResultado_Demanda;
         this.AV14ContagemResultado_SistemaCod = aP1_ContagemResultado_SistemaCod;
         this.AV16ContagemResultado_DemandaFM = aP2_ContagemResultado_DemandaFM;
         this.AV13Contratada_AreaTrabalhoCod = aP3_Contratada_AreaTrabalhoCod;
         this.AV15AreaTrabalho_ValidaOSFM = aP4_AreaTrabalho_ValidaOSFM;
         this.AV8Codigo = aP5_Codigo;
         this.AV18Servico = aP6_Servico;
         this.AV17ContagemResultado_ContratadaCod = aP7_ContagemResultado_ContratadaCod;
         this.AV10QuemCadastrou = "" ;
         this.AV9Flag = false ;
         initialize();
         executePrivate();
         aP5_Codigo=this.AV8Codigo;
         aP8_QuemCadastrou=this.AV10QuemCadastrou;
         aP9_Flag=this.AV9Flag;
         return AV9Flag ;
      }

      public void executeSubmit( String aP0_ContagemResultado_Demanda ,
                                 int aP1_ContagemResultado_SistemaCod ,
                                 String aP2_ContagemResultado_DemandaFM ,
                                 int aP3_Contratada_AreaTrabalhoCod ,
                                 bool aP4_AreaTrabalho_ValidaOSFM ,
                                 ref int aP5_Codigo ,
                                 int aP6_Servico ,
                                 int aP7_ContagemResultado_ContratadaCod ,
                                 out String aP8_QuemCadastrou ,
                                 out bool aP9_Flag )
      {
         prc_existedemanda objprc_existedemanda;
         objprc_existedemanda = new prc_existedemanda();
         objprc_existedemanda.A457ContagemResultado_Demanda = aP0_ContagemResultado_Demanda;
         objprc_existedemanda.AV14ContagemResultado_SistemaCod = aP1_ContagemResultado_SistemaCod;
         objprc_existedemanda.AV16ContagemResultado_DemandaFM = aP2_ContagemResultado_DemandaFM;
         objprc_existedemanda.AV13Contratada_AreaTrabalhoCod = aP3_Contratada_AreaTrabalhoCod;
         objprc_existedemanda.AV15AreaTrabalho_ValidaOSFM = aP4_AreaTrabalho_ValidaOSFM;
         objprc_existedemanda.AV8Codigo = aP5_Codigo;
         objprc_existedemanda.AV18Servico = aP6_Servico;
         objprc_existedemanda.AV17ContagemResultado_ContratadaCod = aP7_ContagemResultado_ContratadaCod;
         objprc_existedemanda.AV10QuemCadastrou = "" ;
         objprc_existedemanda.AV9Flag = false ;
         objprc_existedemanda.context.SetSubmitInitialConfig(context);
         objprc_existedemanda.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existedemanda);
         aP5_Codigo=this.AV8Codigo;
         aP8_QuemCadastrou=this.AV10QuemCadastrou;
         aP9_Flag=this.AV9Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existedemanda)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Flag = false;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV14ContagemResultado_SistemaCod ,
                                              AV15AreaTrabalho_ValidaOSFM ,
                                              A489ContagemResultado_SistemaCod ,
                                              A493ContagemResultado_DemandaFM ,
                                              AV16ContagemResultado_DemandaFM ,
                                              A456ContagemResultado_Codigo ,
                                              AV8Codigo ,
                                              A484ContagemResultado_StatusDmn ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV13Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV17ContagemResultado_ContratadaCod ,
                                              A601ContagemResultado_Servico ,
                                              AV18Servico ,
                                              A457ContagemResultado_Demanda },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00352 */
         pr_default.execute(0, new Object[] {n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, AV8Codigo, AV13Contratada_AreaTrabalhoCod, AV17ContagemResultado_ContratadaCod, AV18Servico, AV14ContagemResultado_SistemaCod, AV16ContagemResultado_DemandaFM});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00352_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00352_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P00352_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00352_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P00352_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00352_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00352_n601ContagemResultado_Servico[0];
            A493ContagemResultado_DemandaFM = P00352_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00352_n493ContagemResultado_DemandaFM[0];
            A489ContagemResultado_SistemaCod = P00352_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00352_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = P00352_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00352_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00352_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00352_n52Contratada_AreaTrabalhoCod[0];
            A508ContagemResultado_Owner = P00352_A508ContagemResultado_Owner[0];
            A509ContagemrResultado_SistemaSigla = P00352_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00352_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00352_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00352_n601ContagemResultado_Servico[0];
            A509ContagemrResultado_SistemaSigla = P00352_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00352_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00352_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00352_n52Contratada_AreaTrabalhoCod[0];
            AV9Flag = true;
            AV8Codigo = A456ContagemResultado_Codigo;
            AV11ContagemResultado_Owner = A508ContagemResultado_Owner;
            AV10QuemCadastrou = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( ! (0==AV11ContagemResultado_Owner) )
         {
            /* Using cursor P00353 */
            pr_default.execute(1, new Object[] {AV11ContagemResultado_Owner});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A57Usuario_PessoaCod = P00353_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = P00353_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = P00353_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00353_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = P00353_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00353_n58Usuario_PessoaNom[0];
               AV10QuemCadastrou = AV10QuemCadastrou + ", por " + StringUtil.Trim( A58Usuario_PessoaNom);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         A493ContagemResultado_DemandaFM = "";
         A484ContagemResultado_StatusDmn = "";
         P00352_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00352_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00352_A457ContagemResultado_Demanda = new String[] {""} ;
         P00352_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00352_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00352_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00352_A456ContagemResultado_Codigo = new int[1] ;
         P00352_A601ContagemResultado_Servico = new int[1] ;
         P00352_n601ContagemResultado_Servico = new bool[] {false} ;
         P00352_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00352_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00352_A489ContagemResultado_SistemaCod = new int[1] ;
         P00352_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00352_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00352_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00352_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00352_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00352_A508ContagemResultado_Owner = new int[1] ;
         P00352_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00352_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         A509ContagemrResultado_SistemaSigla = "";
         P00353_A57Usuario_PessoaCod = new int[1] ;
         P00353_A1Usuario_Codigo = new int[1] ;
         P00353_A58Usuario_PessoaNom = new String[] {""} ;
         P00353_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existedemanda__default(),
            new Object[][] {
                new Object[] {
               P00352_A1553ContagemResultado_CntSrvCod, P00352_n1553ContagemResultado_CntSrvCod, P00352_A457ContagemResultado_Demanda, P00352_n457ContagemResultado_Demanda, P00352_A484ContagemResultado_StatusDmn, P00352_n484ContagemResultado_StatusDmn, P00352_A456ContagemResultado_Codigo, P00352_A601ContagemResultado_Servico, P00352_n601ContagemResultado_Servico, P00352_A493ContagemResultado_DemandaFM,
               P00352_n493ContagemResultado_DemandaFM, P00352_A489ContagemResultado_SistemaCod, P00352_n489ContagemResultado_SistemaCod, P00352_A490ContagemResultado_ContratadaCod, P00352_n490ContagemResultado_ContratadaCod, P00352_A52Contratada_AreaTrabalhoCod, P00352_n52Contratada_AreaTrabalhoCod, P00352_A508ContagemResultado_Owner, P00352_A509ContagemrResultado_SistemaSigla, P00352_n509ContagemrResultado_SistemaSigla
               }
               , new Object[] {
               P00353_A57Usuario_PessoaCod, P00353_A1Usuario_Codigo, P00353_A58Usuario_PessoaNom, P00353_n58Usuario_PessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14ContagemResultado_SistemaCod ;
      private int AV13Contratada_AreaTrabalhoCod ;
      private int AV8Codigo ;
      private int AV18Servico ;
      private int AV17ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A508ContagemResultado_Owner ;
      private int AV11ContagemResultado_Owner ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV10QuemCadastrou ;
      private String A58Usuario_PessoaNom ;
      private bool AV15AreaTrabalho_ValidaOSFM ;
      private bool AV9Flag ;
      private bool n457ContagemResultado_Demanda ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n601ContagemResultado_Servico ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n58Usuario_PessoaNom ;
      private String A457ContagemResultado_Demanda ;
      private String AV16ContagemResultado_DemandaFM ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP5_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00352_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00352_n1553ContagemResultado_CntSrvCod ;
      private String[] P00352_A457ContagemResultado_Demanda ;
      private bool[] P00352_n457ContagemResultado_Demanda ;
      private String[] P00352_A484ContagemResultado_StatusDmn ;
      private bool[] P00352_n484ContagemResultado_StatusDmn ;
      private int[] P00352_A456ContagemResultado_Codigo ;
      private int[] P00352_A601ContagemResultado_Servico ;
      private bool[] P00352_n601ContagemResultado_Servico ;
      private String[] P00352_A493ContagemResultado_DemandaFM ;
      private bool[] P00352_n493ContagemResultado_DemandaFM ;
      private int[] P00352_A489ContagemResultado_SistemaCod ;
      private bool[] P00352_n489ContagemResultado_SistemaCod ;
      private int[] P00352_A490ContagemResultado_ContratadaCod ;
      private bool[] P00352_n490ContagemResultado_ContratadaCod ;
      private int[] P00352_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00352_n52Contratada_AreaTrabalhoCod ;
      private int[] P00352_A508ContagemResultado_Owner ;
      private String[] P00352_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00352_n509ContagemrResultado_SistemaSigla ;
      private int[] P00353_A57Usuario_PessoaCod ;
      private int[] P00353_A1Usuario_Codigo ;
      private String[] P00353_A58Usuario_PessoaNom ;
      private bool[] P00353_n58Usuario_PessoaNom ;
      private String aP8_QuemCadastrou ;
      private bool aP9_Flag ;
   }

   public class prc_existedemanda__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00352( IGxContext context ,
                                             int AV14ContagemResultado_SistemaCod ,
                                             bool AV15AreaTrabalho_ValidaOSFM ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String AV16ContagemResultado_DemandaFM ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV8Codigo ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV13Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int AV17ContagemResultado_ContratadaCod ,
                                             int A601ContagemResultado_Servico ,
                                             int AV18Servico ,
                                             String A457ContagemResultado_Demanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Owner], T3.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Demanda] = @ContagemResultado_Demanda)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Codigo] <> @AV8Codigo)";
         scmdbuf = scmdbuf + " and (Not T1.[ContagemResultado_StatusDmn] = 'X')";
         scmdbuf = scmdbuf + " and (T4.[Contratada_AreaTrabalhoCod] = @AV13Contratada_AreaTrabalhoCod)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContratadaCod] = @AV17ContagemResultado_ContratadaCod)";
         scmdbuf = scmdbuf + " and (T2.[Servico_Codigo] = @AV18Servico)";
         if ( ! (0==AV14ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV14ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV15AreaTrabalho_ValidaOSFM )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV16ContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00352(context, (int)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00353 ;
          prmP00353 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00352 ;
          prmP00352 = new Object[] {
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00352", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00352,1,0,false,true )
             ,new CursorDef("P00353", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV11ContagemResultado_Owner ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00353,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((String[]) buf[18])[0] = rslt.getString(11, 25) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[7] )
                   {
                      stmt.setNull( sIdx , SqlDbType.VarChar );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (String)parms[8]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
