/*
               File: PRC_UsuarioEhContratante
        Description: Usuario Eh Contratante na �rea de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:45.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarioehcontratante : GXProcedure
   {
      public prc_usuarioehcontratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarioehcontratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           int aP1_UserId ,
                           out bool aP2_EhContratante )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8UserId = aP1_UserId;
         this.AV9EhContratante = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_EhContratante=this.AV9EhContratante;
      }

      public bool executeUdp( ref int aP0_ContagemResultado_Codigo ,
                              int aP1_UserId )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8UserId = aP1_UserId;
         this.AV9EhContratante = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_EhContratante=this.AV9EhContratante;
         return AV9EhContratante ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 int aP1_UserId ,
                                 out bool aP2_EhContratante )
      {
         prc_usuarioehcontratante objprc_usuarioehcontratante;
         objprc_usuarioehcontratante = new prc_usuarioehcontratante();
         objprc_usuarioehcontratante.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_usuarioehcontratante.AV8UserId = aP1_UserId;
         objprc_usuarioehcontratante.AV9EhContratante = false ;
         objprc_usuarioehcontratante.context.SetSubmitInitialConfig(context);
         objprc_usuarioehcontratante.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarioehcontratante);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_EhContratante=this.AV9EhContratante;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarioehcontratante)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009S2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P009S2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P009S2_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P009S2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P009S2_n52Contratada_AreaTrabalhoCod[0];
            A489ContagemResultado_SistemaCod = P009S2_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P009S2_n489ContagemResultado_SistemaCod[0];
            A496Contagemresultado_SistemaAreaCod = P009S2_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P009S2_n496Contagemresultado_SistemaAreaCod[0];
            A52Contratada_AreaTrabalhoCod = P009S2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P009S2_n52Contratada_AreaTrabalhoCod[0];
            A496Contagemresultado_SistemaAreaCod = P009S2_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P009S2_n496Contagemresultado_SistemaAreaCod[0];
            if ( A490ContagemResultado_ContratadaCod > 0 )
            {
               AV11AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            }
            else if ( A489ContagemResultado_SistemaCod > 0 )
            {
               AV11AreaTrabalho_Codigo = A496Contagemresultado_SistemaAreaCod;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P009S4 */
         pr_default.execute(1, new Object[] {AV8UserId, AV11AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = P009S4_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = P009S4_A60ContratanteUsuario_UsuarioCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P009S4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P009S4_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P009S4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P009S4_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            AV9EhContratante = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009S2_A456ContagemResultado_Codigo = new int[1] ;
         P009S2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P009S2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P009S2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P009S2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P009S2_A489ContagemResultado_SistemaCod = new int[1] ;
         P009S2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P009S2_A496Contagemresultado_SistemaAreaCod = new int[1] ;
         P009S2_n496Contagemresultado_SistemaAreaCod = new bool[] {false} ;
         P009S4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P009S4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P009S4_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P009S4_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuarioehcontratante__default(),
            new Object[][] {
                new Object[] {
               P009S2_A456ContagemResultado_Codigo, P009S2_A490ContagemResultado_ContratadaCod, P009S2_n490ContagemResultado_ContratadaCod, P009S2_A52Contratada_AreaTrabalhoCod, P009S2_n52Contratada_AreaTrabalhoCod, P009S2_A489ContagemResultado_SistemaCod, P009S2_n489ContagemResultado_SistemaCod, P009S2_A496Contagemresultado_SistemaAreaCod, P009S2_n496Contagemresultado_SistemaAreaCod
               }
               , new Object[] {
               P009S4_A63ContratanteUsuario_ContratanteCod, P009S4_A60ContratanteUsuario_UsuarioCod, P009S4_A1020ContratanteUsuario_AreaTrabalhoCod, P009S4_n1020ContratanteUsuario_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV8UserId ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A496Contagemresultado_SistemaAreaCod ;
      private int AV11AreaTrabalho_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private String scmdbuf ;
      private bool AV9EhContratante ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n496Contagemresultado_SistemaAreaCod ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P009S2_A456ContagemResultado_Codigo ;
      private int[] P009S2_A490ContagemResultado_ContratadaCod ;
      private bool[] P009S2_n490ContagemResultado_ContratadaCod ;
      private int[] P009S2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P009S2_n52Contratada_AreaTrabalhoCod ;
      private int[] P009S2_A489ContagemResultado_SistemaCod ;
      private bool[] P009S2_n489ContagemResultado_SistemaCod ;
      private int[] P009S2_A496Contagemresultado_SistemaAreaCod ;
      private bool[] P009S2_n496Contagemresultado_SistemaAreaCod ;
      private int[] P009S4_A63ContratanteUsuario_ContratanteCod ;
      private int[] P009S4_A60ContratanteUsuario_UsuarioCod ;
      private int[] P009S4_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] P009S4_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool aP2_EhContratante ;
   }

   public class prc_usuarioehcontratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009S2 ;
          prmP009S2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009S4 ;
          prmP009S4 = new Object[] {
          new Object[] {"@AV8UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009S2", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T3.[Sistema_AreaTrabalhoCod] AS Contagemresultado_SistemaAreaCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009S2,1,0,false,true )
             ,new CursorDef("P009S4", "SELECT TOP 1 T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @AV8UserId) AND (COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV11AreaTrabalho_Codigo) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009S4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
