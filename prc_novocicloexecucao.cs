/*
               File: PRC_NovoCicloExecucao
        Description: Novo Ciclo de Execucao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:4.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_novocicloexecucao : GXProcedure
   {
      public prc_novocicloexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_novocicloexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_DataInicio ,
                           DateTime aP2_DataPrevista ,
                           String aP3_TipoDias ,
                           bool aP4_Glosavel )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12DataInicio = aP1_DataInicio;
         this.AV9DataPrevista = aP2_DataPrevista;
         this.AV10TipoDias = aP3_TipoDias;
         this.AV11Glosavel = aP4_Glosavel;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_DataInicio ,
                                 DateTime aP2_DataPrevista ,
                                 String aP3_TipoDias ,
                                 bool aP4_Glosavel )
      {
         prc_novocicloexecucao objprc_novocicloexecucao;
         objprc_novocicloexecucao = new prc_novocicloexecucao();
         objprc_novocicloexecucao.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_novocicloexecucao.AV12DataInicio = aP1_DataInicio;
         objprc_novocicloexecucao.AV9DataPrevista = aP2_DataPrevista;
         objprc_novocicloexecucao.AV10TipoDias = aP3_TipoDias;
         objprc_novocicloexecucao.AV11Glosavel = aP4_Glosavel;
         objprc_novocicloexecucao.context.SetSubmitInitialConfig(context);
         objprc_novocicloexecucao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_novocicloexecucao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_novocicloexecucao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV9DataPrevista > AV12DataInicio )
         {
            AV15GXLvl3 = 0;
            /* Using cursor P009W2 */
            pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo, AV12DataInicio});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1406ContagemResultadoExecucao_Inicio = P009W2_A1406ContagemResultadoExecucao_Inicio[0];
               A1404ContagemResultadoExecucao_OSCod = P009W2_A1404ContagemResultadoExecucao_OSCod[0];
               A1405ContagemResultadoExecucao_Codigo = P009W2_A1405ContagemResultadoExecucao_Codigo[0];
               AV15GXLvl3 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV15GXLvl3 == 0 )
            {
               /*
                  INSERT RECORD ON TABLE ContagemResultadoExecucao

               */
               A1404ContagemResultadoExecucao_OSCod = AV8ContagemResultado_Codigo;
               A1406ContagemResultadoExecucao_Inicio = AV12DataInicio;
               A1408ContagemResultadoExecucao_Prevista = AV9DataPrevista;
               n1408ContagemResultadoExecucao_Prevista = false;
               A2028ContagemResultadoExecucao_Glosavel = AV11Glosavel;
               n2028ContagemResultadoExecucao_Glosavel = false;
               GXt_int1 = A1411ContagemResultadoExecucao_PrazoDias;
               new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1408ContagemResultadoExecucao_Prevista),  AV10TipoDias, out  GXt_int1) ;
               A1411ContagemResultadoExecucao_PrazoDias = GXt_int1;
               n1411ContagemResultadoExecucao_PrazoDias = false;
               /* Using cursor P009W3 */
               pr_default.execute(1, new Object[] {A1404ContagemResultadoExecucao_OSCod, A1406ContagemResultadoExecucao_Inicio, n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, n2028ContagemResultadoExecucao_Glosavel, A2028ContagemResultadoExecucao_Glosavel});
               A1405ContagemResultadoExecucao_Codigo = P009W3_A1405ContagemResultadoExecucao_Codigo[0];
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
               if ( (pr_default.getStatus(1) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009W2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P009W2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P009W2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         P009W3_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_novocicloexecucao__default(),
            new Object[][] {
                new Object[] {
               P009W2_A1406ContagemResultadoExecucao_Inicio, P009W2_A1404ContagemResultadoExecucao_OSCod, P009W2_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               P009W3_A1405ContagemResultadoExecucao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15GXLvl3 ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short GXt_int1 ;
      private int AV8ContagemResultado_Codigo ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int GX_INS166 ;
      private String AV10TipoDias ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private DateTime AV12DataInicio ;
      private DateTime AV9DataPrevista ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private bool AV11Glosavel ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool A2028ContagemResultadoExecucao_Glosavel ;
      private bool n2028ContagemResultadoExecucao_Glosavel ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] P009W2_A1406ContagemResultadoExecucao_Inicio ;
      private int[] P009W2_A1404ContagemResultadoExecucao_OSCod ;
      private int[] P009W2_A1405ContagemResultadoExecucao_Codigo ;
      private int[] P009W3_A1405ContagemResultadoExecucao_Codigo ;
   }

   public class prc_novocicloexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009W2 ;
          prmP009W2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12DataInicio",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP009W3 ;
          prmP009W3 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoExecucao_Inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Glosavel",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009W2", "SELECT TOP 1 [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @AV8ContagemResultado_Codigo and [ContagemResultadoExecucao_Inicio] = @AV12DataInicio ORDER BY [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009W2,1,0,false,true )
             ,new CursorDef("P009W3", "INSERT INTO [ContagemResultadoExecucao]([ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Glosavel], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Dias]) VALUES(@ContagemResultadoExecucao_OSCod, @ContagemResultadoExecucao_Inicio, @ContagemResultadoExecucao_Prevista, @ContagemResultadoExecucao_PrazoDias, @ContagemResultadoExecucao_Glosavel, convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP009W3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[7]);
                }
                return;
       }
    }

 }

}
