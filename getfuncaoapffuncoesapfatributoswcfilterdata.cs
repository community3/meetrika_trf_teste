/*
               File: GetFuncaoAPFFuncoesAPFAtributosWCFilterData
        Description: Get Funcao APFFuncoes APFAtributos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:9.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getfuncaoapffuncoesapfatributoswcfilterdata : GXProcedure
   {
      public getfuncaoapffuncoesapfatributoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getfuncaoapffuncoesapfatributoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getfuncaoapffuncoesapfatributoswcfilterdata objgetfuncaoapffuncoesapfatributoswcfilterdata;
         objgetfuncaoapffuncoesapfatributoswcfilterdata = new getfuncaoapffuncoesapfatributoswcfilterdata();
         objgetfuncaoapffuncoesapfatributoswcfilterdata.AV18DDOName = aP0_DDOName;
         objgetfuncaoapffuncoesapfatributoswcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetfuncaoapffuncoesapfatributoswcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetfuncaoapffuncoesapfatributoswcfilterdata.AV22OptionsJson = "" ;
         objgetfuncaoapffuncoesapfatributoswcfilterdata.AV25OptionsDescJson = "" ;
         objgetfuncaoapffuncoesapfatributoswcfilterdata.AV27OptionIndexesJson = "" ;
         objgetfuncaoapffuncoesapfatributoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetfuncaoapffuncoesapfatributoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetfuncaoapffuncoesapfatributoswcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getfuncaoapffuncoesapfatributoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFATRIBUTOS_ATRTABELANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFATRIBUTOS_FUNCAODADOSNOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("FuncaoAPFFuncoesAPFAtributosWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "FuncaoAPFFuncoesAPFAtributosWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("FuncaoAPFFuncoesAPFAtributosWCGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV10TFFuncaoAPFAtributos_AtributosNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL") == 0 )
            {
               AV11TFFuncaoAPFAtributos_AtributosNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
            {
               AV12TFFuncaoAPFAtributos_AtrTabelaNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL") == 0 )
            {
               AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM") == 0 )
            {
               AV14TFFuncaoAPFAtributos_FuncaoDadosNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL") == 0 )
            {
               AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAOAPF_CODIGO") == 0 )
            {
               AV41FuncaoAPF_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAOAPF_SISTEMACOD") == 0 )
            {
               AV42FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV35FuncaoAPFAtributos_AtributosNom1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
            {
               AV36FuncaoAPFAtributos_AtrTabelaNom1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
               {
                  AV39FuncaoAPFAtributos_AtributosNom2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
               {
                  AV40FuncaoAPFAtributos_AtrTabelaNom2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMOPTIONS' Routine */
         AV10TFFuncaoAPFAtributos_AtributosNom = AV16SearchTxt;
         AV11TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35FuncaoAPFAtributos_AtributosNom1 ,
                                              AV36FuncaoAPFAtributos_AtrTabelaNom1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39FuncaoAPFAtributos_AtributosNom2 ,
                                              AV40FuncaoAPFAtributos_AtrTabelaNom2 ,
                                              AV11TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                              AV10TFFuncaoAPFAtributos_AtributosNom ,
                                              AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                              AV12TFFuncaoAPFAtributos_AtrTabelaNom ,
                                              AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                              AV14TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                              A180Atributos_Ativo ,
                                              AV41FuncaoAPF_Codigo ,
                                              A165FuncaoAPF_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV35FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV35FuncaoAPFAtributos_AtributosNom1), 50, "%");
         lV36FuncaoAPFAtributos_AtrTabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36FuncaoAPFAtributos_AtrTabelaNom1), 50, "%");
         lV39FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV39FuncaoAPFAtributos_AtributosNom2), 50, "%");
         lV40FuncaoAPFAtributos_AtrTabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV40FuncaoAPFAtributos_AtrTabelaNom2), 50, "%");
         lV10TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV10TFFuncaoAPFAtributos_AtributosNom), 50, "%");
         lV12TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
         lV14TFFuncaoAPFAtributos_FuncaoDadosNom = StringUtil.Concat( StringUtil.RTrim( AV14TFFuncaoAPFAtributos_FuncaoDadosNom), "%", "");
         /* Using cursor P00U82 */
         pr_default.execute(0, new Object[] {AV41FuncaoAPF_Codigo, lV35FuncaoAPFAtributos_AtributosNom1, lV36FuncaoAPFAtributos_AtrTabelaNom1, lV39FuncaoAPFAtributos_AtributosNom2, lV40FuncaoAPFAtributos_AtrTabelaNom2, lV10TFFuncaoAPFAtributos_AtributosNom, AV11TFFuncaoAPFAtributos_AtributosNom_Sel, lV12TFFuncaoAPFAtributos_AtrTabelaNom, AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV14TFFuncaoAPFAtributos_FuncaoDadosNom, AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKU82 = false;
            A366FuncaoAPFAtributos_AtrTabelaCod = P00U82_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00U82_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00U82_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00U82_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A364FuncaoAPFAtributos_AtributosCod = P00U82_A364FuncaoAPFAtributos_AtributosCod[0];
            A165FuncaoAPF_Codigo = P00U82_A165FuncaoAPF_Codigo[0];
            A180Atributos_Ativo = P00U82_A180Atributos_Ativo[0];
            n180Atributos_Ativo = P00U82_n180Atributos_Ativo[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = P00U82_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = P00U82_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00U82_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00U82_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A365FuncaoAPFAtributos_AtributosNom = P00U82_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00U82_n365FuncaoAPFAtributos_AtributosNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = P00U82_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = P00U82_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00U82_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00U82_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A180Atributos_Ativo = P00U82_A180Atributos_Ativo[0];
            n180Atributos_Ativo = P00U82_n180Atributos_Ativo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00U82_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00U82_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00U82_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00U82_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00U82_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( P00U82_A364FuncaoAPFAtributos_AtributosCod[0] == A364FuncaoAPFAtributos_AtributosCod ) )
            {
               BRKU82 = false;
               AV28count = (long)(AV28count+1);
               BRKU82 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom)) )
            {
               AV20Option = A365FuncaoAPFAtributos_AtributosNom;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU82 )
            {
               BRKU82 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAOAPFATRIBUTOS_ATRTABELANOMOPTIONS' Routine */
         AV12TFFuncaoAPFAtributos_AtrTabelaNom = AV16SearchTxt;
         AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35FuncaoAPFAtributos_AtributosNom1 ,
                                              AV36FuncaoAPFAtributos_AtrTabelaNom1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39FuncaoAPFAtributos_AtributosNom2 ,
                                              AV40FuncaoAPFAtributos_AtrTabelaNom2 ,
                                              AV11TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                              AV10TFFuncaoAPFAtributos_AtributosNom ,
                                              AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                              AV12TFFuncaoAPFAtributos_AtrTabelaNom ,
                                              AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                              AV14TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                              A180Atributos_Ativo ,
                                              AV41FuncaoAPF_Codigo ,
                                              A165FuncaoAPF_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV35FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV35FuncaoAPFAtributos_AtributosNom1), 50, "%");
         lV36FuncaoAPFAtributos_AtrTabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36FuncaoAPFAtributos_AtrTabelaNom1), 50, "%");
         lV39FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV39FuncaoAPFAtributos_AtributosNom2), 50, "%");
         lV40FuncaoAPFAtributos_AtrTabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV40FuncaoAPFAtributos_AtrTabelaNom2), 50, "%");
         lV10TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV10TFFuncaoAPFAtributos_AtributosNom), 50, "%");
         lV12TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
         lV14TFFuncaoAPFAtributos_FuncaoDadosNom = StringUtil.Concat( StringUtil.RTrim( AV14TFFuncaoAPFAtributos_FuncaoDadosNom), "%", "");
         /* Using cursor P00U83 */
         pr_default.execute(1, new Object[] {AV41FuncaoAPF_Codigo, lV35FuncaoAPFAtributos_AtributosNom1, lV36FuncaoAPFAtributos_AtrTabelaNom1, lV39FuncaoAPFAtributos_AtributosNom2, lV40FuncaoAPFAtributos_AtrTabelaNom2, lV10TFFuncaoAPFAtributos_AtributosNom, AV11TFFuncaoAPFAtributos_AtributosNom_Sel, lV12TFFuncaoAPFAtributos_AtrTabelaNom, AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV14TFFuncaoAPFAtributos_FuncaoDadosNom, AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKU84 = false;
            A364FuncaoAPFAtributos_AtributosCod = P00U83_A364FuncaoAPFAtributos_AtributosCod[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00U83_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00U83_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00U83_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00U83_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A165FuncaoAPF_Codigo = P00U83_A165FuncaoAPF_Codigo[0];
            A180Atributos_Ativo = P00U83_A180Atributos_Ativo[0];
            n180Atributos_Ativo = P00U83_n180Atributos_Ativo[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00U83_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00U83_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = P00U83_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = P00U83_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A365FuncaoAPFAtributos_AtributosNom = P00U83_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00U83_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00U83_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00U83_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A180Atributos_Ativo = P00U83_A180Atributos_Ativo[0];
            n180Atributos_Ativo = P00U83_n180Atributos_Ativo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00U83_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00U83_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00U83_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00U83_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = P00U83_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = P00U83_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00U83_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( StringUtil.StrCmp(P00U83_A367FuncaoAPFAtributos_AtrTabelaNom[0], A367FuncaoAPFAtributos_AtrTabelaNom) == 0 ) )
            {
               BRKU84 = false;
               A364FuncaoAPFAtributos_AtributosCod = P00U83_A364FuncaoAPFAtributos_AtributosCod[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = P00U83_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = P00U83_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = P00U83_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = P00U83_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               AV28count = (long)(AV28count+1);
               BRKU84 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom)) )
            {
               AV20Option = A367FuncaoAPFAtributos_AtrTabelaNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU84 )
            {
               BRKU84 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADFUNCAOAPFATRIBUTOS_FUNCAODADOSNOMOPTIONS' Routine */
         AV14TFFuncaoAPFAtributos_FuncaoDadosNom = AV16SearchTxt;
         AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35FuncaoAPFAtributos_AtributosNom1 ,
                                              AV36FuncaoAPFAtributos_AtrTabelaNom1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39FuncaoAPFAtributos_AtributosNom2 ,
                                              AV40FuncaoAPFAtributos_AtrTabelaNom2 ,
                                              AV11TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                              AV10TFFuncaoAPFAtributos_AtributosNom ,
                                              AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                              AV12TFFuncaoAPFAtributos_AtrTabelaNom ,
                                              AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                              AV14TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                              A180Atributos_Ativo ,
                                              AV41FuncaoAPF_Codigo ,
                                              A165FuncaoAPF_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV35FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV35FuncaoAPFAtributos_AtributosNom1), 50, "%");
         lV36FuncaoAPFAtributos_AtrTabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36FuncaoAPFAtributos_AtrTabelaNom1), 50, "%");
         lV39FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV39FuncaoAPFAtributos_AtributosNom2), 50, "%");
         lV40FuncaoAPFAtributos_AtrTabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV40FuncaoAPFAtributos_AtrTabelaNom2), 50, "%");
         lV10TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV10TFFuncaoAPFAtributos_AtributosNom), 50, "%");
         lV12TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
         lV14TFFuncaoAPFAtributos_FuncaoDadosNom = StringUtil.Concat( StringUtil.RTrim( AV14TFFuncaoAPFAtributos_FuncaoDadosNom), "%", "");
         /* Using cursor P00U84 */
         pr_default.execute(2, new Object[] {AV41FuncaoAPF_Codigo, lV35FuncaoAPFAtributos_AtributosNom1, lV36FuncaoAPFAtributos_AtrTabelaNom1, lV39FuncaoAPFAtributos_AtributosNom2, lV40FuncaoAPFAtributos_AtrTabelaNom2, lV10TFFuncaoAPFAtributos_AtributosNom, AV11TFFuncaoAPFAtributos_AtributosNom_Sel, lV12TFFuncaoAPFAtributos_AtrTabelaNom, AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV14TFFuncaoAPFAtributos_FuncaoDadosNom, AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKU86 = false;
            A364FuncaoAPFAtributos_AtributosCod = P00U84_A364FuncaoAPFAtributos_AtributosCod[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00U84_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00U84_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P00U84_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P00U84_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A165FuncaoAPF_Codigo = P00U84_A165FuncaoAPF_Codigo[0];
            A180Atributos_Ativo = P00U84_A180Atributos_Ativo[0];
            n180Atributos_Ativo = P00U84_n180Atributos_Ativo[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = P00U84_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = P00U84_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00U84_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00U84_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A365FuncaoAPFAtributos_AtributosNom = P00U84_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00U84_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = P00U84_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = P00U84_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A180Atributos_Ativo = P00U84_A180Atributos_Ativo[0];
            n180Atributos_Ativo = P00U84_n180Atributos_Ativo[0];
            A365FuncaoAPFAtributos_AtributosNom = P00U84_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = P00U84_n365FuncaoAPFAtributos_AtributosNom[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = P00U84_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = P00U84_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = P00U84_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = P00U84_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00U84_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( StringUtil.StrCmp(P00U84_A412FuncaoAPFAtributos_FuncaoDadosNom[0], A412FuncaoAPFAtributos_FuncaoDadosNom) == 0 ) )
            {
               BRKU86 = false;
               A364FuncaoAPFAtributos_AtributosCod = P00U84_A364FuncaoAPFAtributos_AtributosCod[0];
               A378FuncaoAPFAtributos_FuncaoDadosCod = P00U84_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
               n378FuncaoAPFAtributos_FuncaoDadosCod = P00U84_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
               AV28count = (long)(AV28count+1);
               BRKU86 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A412FuncaoAPFAtributos_FuncaoDadosNom)) )
            {
               AV20Option = A412FuncaoAPFAtributos_FuncaoDadosNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU86 )
            {
               BRKU86 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPFAtributos_AtributosNom = "";
         AV11TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         AV12TFFuncaoAPFAtributos_AtrTabelaNom = "";
         AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         AV14TFFuncaoAPFAtributos_FuncaoDadosNom = "";
         AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35FuncaoAPFAtributos_AtributosNom1 = "";
         AV36FuncaoAPFAtributos_AtrTabelaNom1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV39FuncaoAPFAtributos_AtributosNom2 = "";
         AV40FuncaoAPFAtributos_AtrTabelaNom2 = "";
         scmdbuf = "";
         lV35FuncaoAPFAtributos_AtributosNom1 = "";
         lV36FuncaoAPFAtributos_AtrTabelaNom1 = "";
         lV39FuncaoAPFAtributos_AtributosNom2 = "";
         lV40FuncaoAPFAtributos_AtrTabelaNom2 = "";
         lV10TFFuncaoAPFAtributos_AtributosNom = "";
         lV12TFFuncaoAPFAtributos_AtrTabelaNom = "";
         lV14TFFuncaoAPFAtributos_FuncaoDadosNom = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         P00U82_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00U82_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00U82_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00U82_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00U82_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00U82_A165FuncaoAPF_Codigo = new int[1] ;
         P00U82_A180Atributos_Ativo = new bool[] {false} ;
         P00U82_n180Atributos_Ativo = new bool[] {false} ;
         P00U82_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         P00U82_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         P00U82_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00U82_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00U82_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00U82_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         AV20Option = "";
         P00U83_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00U83_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00U83_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00U83_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00U83_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00U83_A165FuncaoAPF_Codigo = new int[1] ;
         P00U83_A180Atributos_Ativo = new bool[] {false} ;
         P00U83_n180Atributos_Ativo = new bool[] {false} ;
         P00U83_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00U83_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00U83_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         P00U83_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         P00U83_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00U83_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         P00U84_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00U84_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         P00U84_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         P00U84_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P00U84_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P00U84_A165FuncaoAPF_Codigo = new int[1] ;
         P00U84_A180Atributos_Ativo = new bool[] {false} ;
         P00U84_n180Atributos_Ativo = new bool[] {false} ;
         P00U84_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         P00U84_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         P00U84_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         P00U84_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         P00U84_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         P00U84_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getfuncaoapffuncoesapfatributoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00U82_A366FuncaoAPFAtributos_AtrTabelaCod, P00U82_n366FuncaoAPFAtributos_AtrTabelaCod, P00U82_A378FuncaoAPFAtributos_FuncaoDadosCod, P00U82_n378FuncaoAPFAtributos_FuncaoDadosCod, P00U82_A364FuncaoAPFAtributos_AtributosCod, P00U82_A165FuncaoAPF_Codigo, P00U82_A180Atributos_Ativo, P00U82_n180Atributos_Ativo, P00U82_A412FuncaoAPFAtributos_FuncaoDadosNom, P00U82_n412FuncaoAPFAtributos_FuncaoDadosNom,
               P00U82_A367FuncaoAPFAtributos_AtrTabelaNom, P00U82_n367FuncaoAPFAtributos_AtrTabelaNom, P00U82_A365FuncaoAPFAtributos_AtributosNom, P00U82_n365FuncaoAPFAtributos_AtributosNom
               }
               , new Object[] {
               P00U83_A364FuncaoAPFAtributos_AtributosCod, P00U83_A366FuncaoAPFAtributos_AtrTabelaCod, P00U83_n366FuncaoAPFAtributos_AtrTabelaCod, P00U83_A378FuncaoAPFAtributos_FuncaoDadosCod, P00U83_n378FuncaoAPFAtributos_FuncaoDadosCod, P00U83_A165FuncaoAPF_Codigo, P00U83_A180Atributos_Ativo, P00U83_n180Atributos_Ativo, P00U83_A367FuncaoAPFAtributos_AtrTabelaNom, P00U83_n367FuncaoAPFAtributos_AtrTabelaNom,
               P00U83_A412FuncaoAPFAtributos_FuncaoDadosNom, P00U83_n412FuncaoAPFAtributos_FuncaoDadosNom, P00U83_A365FuncaoAPFAtributos_AtributosNom, P00U83_n365FuncaoAPFAtributos_AtributosNom
               }
               , new Object[] {
               P00U84_A364FuncaoAPFAtributos_AtributosCod, P00U84_A366FuncaoAPFAtributos_AtrTabelaCod, P00U84_n366FuncaoAPFAtributos_AtrTabelaCod, P00U84_A378FuncaoAPFAtributos_FuncaoDadosCod, P00U84_n378FuncaoAPFAtributos_FuncaoDadosCod, P00U84_A165FuncaoAPF_Codigo, P00U84_A180Atributos_Ativo, P00U84_n180Atributos_Ativo, P00U84_A412FuncaoAPFAtributos_FuncaoDadosNom, P00U84_n412FuncaoAPFAtributos_FuncaoDadosNom,
               P00U84_A367FuncaoAPFAtributos_AtrTabelaNom, P00U84_n367FuncaoAPFAtributos_AtrTabelaNom, P00U84_A365FuncaoAPFAtributos_AtributosNom, P00U84_n365FuncaoAPFAtributos_AtributosNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV45GXV1 ;
      private int AV41FuncaoAPF_Codigo ;
      private int AV42FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private String AV10TFFuncaoAPFAtributos_AtributosNom ;
      private String AV11TFFuncaoAPFAtributos_AtributosNom_Sel ;
      private String AV12TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ;
      private String AV35FuncaoAPFAtributos_AtributosNom1 ;
      private String AV36FuncaoAPFAtributos_AtrTabelaNom1 ;
      private String AV39FuncaoAPFAtributos_AtributosNom2 ;
      private String AV40FuncaoAPFAtributos_AtrTabelaNom2 ;
      private String scmdbuf ;
      private String lV35FuncaoAPFAtributos_AtributosNom1 ;
      private String lV36FuncaoAPFAtributos_AtrTabelaNom1 ;
      private String lV39FuncaoAPFAtributos_AtributosNom2 ;
      private String lV40FuncaoAPFAtributos_AtrTabelaNom2 ;
      private String lV10TFFuncaoAPFAtributos_AtributosNom ;
      private String lV12TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool A180Atributos_Ativo ;
      private bool BRKU82 ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n180Atributos_Ativo ;
      private bool n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool BRKU84 ;
      private bool BRKU86 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFFuncaoAPFAtributos_FuncaoDadosNom ;
      private String AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String lV14TFFuncaoAPFAtributos_FuncaoDadosNom ;
      private String A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00U82_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00U82_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00U82_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00U82_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00U82_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00U82_A165FuncaoAPF_Codigo ;
      private bool[] P00U82_A180Atributos_Ativo ;
      private bool[] P00U82_n180Atributos_Ativo ;
      private String[] P00U82_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] P00U82_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] P00U82_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00U82_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private String[] P00U82_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00U82_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] P00U83_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00U83_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00U83_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00U83_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00U83_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00U83_A165FuncaoAPF_Codigo ;
      private bool[] P00U83_A180Atributos_Ativo ;
      private bool[] P00U83_n180Atributos_Ativo ;
      private String[] P00U83_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00U83_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private String[] P00U83_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] P00U83_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] P00U83_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00U83_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] P00U84_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00U84_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] P00U84_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] P00U84_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P00U84_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P00U84_A165FuncaoAPF_Codigo ;
      private bool[] P00U84_A180Atributos_Ativo ;
      private bool[] P00U84_n180Atributos_Ativo ;
      private String[] P00U84_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] P00U84_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] P00U84_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] P00U84_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private String[] P00U84_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] P00U84_n365FuncaoAPFAtributos_AtributosNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getfuncaoapffuncoesapfatributoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00U82( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35FuncaoAPFAtributos_AtributosNom1 ,
                                             String AV36FuncaoAPFAtributos_AtrTabelaNom1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39FuncaoAPFAtributos_AtributosNom2 ,
                                             String AV40FuncaoAPFAtributos_AtrTabelaNom2 ,
                                             String AV11TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV10TFFuncaoAPFAtributos_AtributosNom ,
                                             String AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV12TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             String AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                             String AV14TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             String A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                             bool A180Atributos_Ativo ,
                                             int AV41FuncaoAPF_Codigo ,
                                             int A165FuncaoAPF_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncaoAPF_Codigo], T3.[Atributos_Ativo], T2.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPF_Codigo] = @AV41FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and (T3.[Atributos_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV35FuncaoAPFAtributos_AtributosNom1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoAPFAtributos_AtrTabelaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV36FuncaoAPFAtributos_AtrTabelaNom1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV39FuncaoAPFAtributos_AtributosNom2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoAPFAtributos_AtrTabelaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV40FuncaoAPFAtributos_AtrTabelaNom2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV10TFFuncaoAPFAtributos_AtributosNom)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] = @AV11TFFuncaoAPFAtributos_AtributosNom_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV12TFFuncaoAPFAtributos_AtrTabelaNom)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] = @AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoAPFAtributos_FuncaoDadosNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoDados_Nome] like @lV14TFFuncaoAPFAtributos_FuncaoDadosNom)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoDados_Nome] = @AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00U83( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35FuncaoAPFAtributos_AtributosNom1 ,
                                             String AV36FuncaoAPFAtributos_AtrTabelaNom1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39FuncaoAPFAtributos_AtributosNom2 ,
                                             String AV40FuncaoAPFAtributos_AtrTabelaNom2 ,
                                             String AV11TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV10TFFuncaoAPFAtributos_AtributosNom ,
                                             String AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV12TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             String AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                             String AV14TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             String A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                             bool A180Atributos_Ativo ,
                                             int AV41FuncaoAPF_Codigo ,
                                             int A165FuncaoAPF_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Ativo], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T4.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) LEFT JOIN [FuncaoDados] T4 WITH (NOLOCK) ON T4.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPF_Codigo] = @AV41FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Atributos_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV35FuncaoAPFAtributos_AtributosNom1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoAPFAtributos_AtrTabelaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like '%' + @lV36FuncaoAPFAtributos_AtrTabelaNom1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV39FuncaoAPFAtributos_AtributosNom2)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoAPFAtributos_AtrTabelaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like '%' + @lV40FuncaoAPFAtributos_AtrTabelaNom2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV10TFFuncaoAPFAtributos_AtributosNom)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV11TFFuncaoAPFAtributos_AtributosNom_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV12TFFuncaoAPFAtributos_AtrTabelaNom)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoAPFAtributos_FuncaoDadosNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoDados_Nome] like @lV14TFFuncaoAPFAtributos_FuncaoDadosNom)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoDados_Nome] = @AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Codigo], T3.[Tabela_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00U84( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35FuncaoAPFAtributos_AtributosNom1 ,
                                             String AV36FuncaoAPFAtributos_AtrTabelaNom1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39FuncaoAPFAtributos_AtributosNom2 ,
                                             String AV40FuncaoAPFAtributos_AtrTabelaNom2 ,
                                             String AV11TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV10TFFuncaoAPFAtributos_AtributosNom ,
                                             String AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV12TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             String AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                             String AV14TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             String A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                             bool A180Atributos_Ativo ,
                                             int AV41FuncaoAPF_Codigo ,
                                             int A165FuncaoAPF_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [11] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Ativo], T4.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) LEFT JOIN [FuncaoDados] T4 WITH (NOLOCK) ON T4.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPF_Codigo] = @AV41FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Atributos_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV35FuncaoAPFAtributos_AtributosNom1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoAPFAtributos_AtrTabelaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like '%' + @lV36FuncaoAPFAtributos_AtrTabelaNom1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV39FuncaoAPFAtributos_AtributosNom2)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoAPFAtributos_AtrTabelaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like '%' + @lV40FuncaoAPFAtributos_AtrTabelaNom2)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV10TFFuncaoAPFAtributos_AtributosNom)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV11TFFuncaoAPFAtributos_AtributosNom_Sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV12TFFuncaoAPFAtributos_AtrTabelaNom)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoAPFAtributos_FuncaoDadosNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoDados_Nome] like @lV14TFFuncaoAPFAtributos_FuncaoDadosNom)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoDados_Nome] = @AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Codigo], T4.[FuncaoDados_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00U82(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
               case 1 :
                     return conditional_P00U83(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
               case 2 :
                     return conditional_P00U84(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00U82 ;
          prmP00U82 = new Object[] {
          new Object[] {"@AV41FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36FuncaoAPFAtributos_AtrTabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40FuncaoAPFAtributos_AtrTabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFFuncaoAPFAtributos_FuncaoDadosNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00U83 ;
          prmP00U83 = new Object[] {
          new Object[] {"@AV41FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36FuncaoAPFAtributos_AtrTabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40FuncaoAPFAtributos_AtrTabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFFuncaoAPFAtributos_FuncaoDadosNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00U84 ;
          prmP00U84 = new Object[] {
          new Object[] {"@AV41FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36FuncaoAPFAtributos_AtrTabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40FuncaoAPFAtributos_AtrTabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFFuncaoAPFAtributos_FuncaoDadosNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFFuncaoAPFAtributos_FuncaoDadosNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00U82", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U82,100,0,true,false )
             ,new CursorDef("P00U83", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U83,100,0,true,false )
             ,new CursorDef("P00U84", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U84,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getfuncaoapffuncoesapfatributoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getfuncaoapffuncoesapfatributoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getfuncaoapffuncoesapfatributoswcfilterdata") )
          {
             return  ;
          }
          getfuncaoapffuncoesapfatributoswcfilterdata worker = new getfuncaoapffuncoesapfatributoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
