/*
               File: type_SdtUsuarioServicos
        Description: Usuario Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:11.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "UsuarioServicos" )]
   [XmlType(TypeName =  "UsuarioServicos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtUsuarioServicos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUsuarioServicos( )
      {
         /* Constructor for serialization */
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla = "";
         gxTv_SdtUsuarioServicos_Mode = "";
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z = "";
      }

      public SdtUsuarioServicos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV828UsuarioServicos_UsuarioCod ,
                        int AV829UsuarioServicos_ServicoCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV828UsuarioServicos_UsuarioCod,(int)AV829UsuarioServicos_ServicoCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"UsuarioServicos_UsuarioCod", typeof(int)}, new Object[]{"UsuarioServicos_ServicoCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "UsuarioServicos");
         metadata.Set("BT", "UsuarioServicos");
         metadata.Set("PK", "[ \"UsuarioServicos_UsuarioCod\",\"UsuarioServicos_ServicoCod\" ]");
         metadata.Set("PKAssigned", "[ \"UsuarioServicos_ServicoCod\",\"UsuarioServicos_UsuarioCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"UsuarioServicos_ServicoCod-Servico_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"UsuarioServicos_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_servicocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_servicosigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_servicoativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_tmpestanl_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_tmpestexc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_tmpestcrr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_servicosigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_servicoativo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_tmpestanl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_tmpestexc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuarioservicos_tmpestcrr_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtUsuarioServicos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtUsuarioServicos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtUsuarioServicos obj ;
         obj = this;
         obj.gxTpr_Usuarioservicos_usuariocod = deserialized.gxTpr_Usuarioservicos_usuariocod;
         obj.gxTpr_Usuarioservicos_servicocod = deserialized.gxTpr_Usuarioservicos_servicocod;
         obj.gxTpr_Usuarioservicos_servicosigla = deserialized.gxTpr_Usuarioservicos_servicosigla;
         obj.gxTpr_Usuarioservicos_servicoativo = deserialized.gxTpr_Usuarioservicos_servicoativo;
         obj.gxTpr_Usuarioservicos_tmpestanl = deserialized.gxTpr_Usuarioservicos_tmpestanl;
         obj.gxTpr_Usuarioservicos_tmpestexc = deserialized.gxTpr_Usuarioservicos_tmpestexc;
         obj.gxTpr_Usuarioservicos_tmpestcrr = deserialized.gxTpr_Usuarioservicos_tmpestcrr;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Usuarioservicos_usuariocod_Z = deserialized.gxTpr_Usuarioservicos_usuariocod_Z;
         obj.gxTpr_Usuarioservicos_servicocod_Z = deserialized.gxTpr_Usuarioservicos_servicocod_Z;
         obj.gxTpr_Usuarioservicos_servicosigla_Z = deserialized.gxTpr_Usuarioservicos_servicosigla_Z;
         obj.gxTpr_Usuarioservicos_servicoativo_Z = deserialized.gxTpr_Usuarioservicos_servicoativo_Z;
         obj.gxTpr_Usuarioservicos_tmpestanl_Z = deserialized.gxTpr_Usuarioservicos_tmpestanl_Z;
         obj.gxTpr_Usuarioservicos_tmpestexc_Z = deserialized.gxTpr_Usuarioservicos_tmpestexc_Z;
         obj.gxTpr_Usuarioservicos_tmpestcrr_Z = deserialized.gxTpr_Usuarioservicos_tmpestcrr_Z;
         obj.gxTpr_Usuarioservicos_servicosigla_N = deserialized.gxTpr_Usuarioservicos_servicosigla_N;
         obj.gxTpr_Usuarioservicos_servicoativo_N = deserialized.gxTpr_Usuarioservicos_servicoativo_N;
         obj.gxTpr_Usuarioservicos_tmpestanl_N = deserialized.gxTpr_Usuarioservicos_tmpestanl_N;
         obj.gxTpr_Usuarioservicos_tmpestexc_N = deserialized.gxTpr_Usuarioservicos_tmpestexc_N;
         obj.gxTpr_Usuarioservicos_tmpestcrr_N = deserialized.gxTpr_Usuarioservicos_tmpestcrr_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_UsuarioCod") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoCod") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoSigla") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoAtivo") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstAnl") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstExc") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstCrr") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtUsuarioServicos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtUsuarioServicos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_UsuarioCod_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoCod_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoSigla_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoAtivo_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstAnl_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstExc_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstCrr_Z") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoSigla_N") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_ServicoAtivo_N") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstAnl_N") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstExc_N") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioServicos_TmpEstCrr_N") )
               {
                  gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "UsuarioServicos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("UsuarioServicos_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioServicos_ServicoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioServicos_ServicoSigla", StringUtil.RTrim( gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioServicos_ServicoAtivo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioServicos_TmpEstAnl", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioServicos_TmpEstExc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("UsuarioServicos_TmpEstCrr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtUsuarioServicos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_ServicoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_ServicoSigla_Z", StringUtil.RTrim( gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_ServicoAtivo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_TmpEstAnl_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_TmpEstExc_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_TmpEstCrr_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_ServicoSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_ServicoAtivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_TmpEstAnl_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_TmpEstExc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("UsuarioServicos_TmpEstCrr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("UsuarioServicos_UsuarioCod", gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod, false);
         AddObjectProperty("UsuarioServicos_ServicoCod", gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod, false);
         AddObjectProperty("UsuarioServicos_ServicoSigla", gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla, false);
         AddObjectProperty("UsuarioServicos_ServicoAtivo", gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo, false);
         AddObjectProperty("UsuarioServicos_TmpEstAnl", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl, false);
         AddObjectProperty("UsuarioServicos_TmpEstExc", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc, false);
         AddObjectProperty("UsuarioServicos_TmpEstCrr", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtUsuarioServicos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtUsuarioServicos_Initialized, false);
            AddObjectProperty("UsuarioServicos_UsuarioCod_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z, false);
            AddObjectProperty("UsuarioServicos_ServicoCod_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z, false);
            AddObjectProperty("UsuarioServicos_ServicoSigla_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z, false);
            AddObjectProperty("UsuarioServicos_ServicoAtivo_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z, false);
            AddObjectProperty("UsuarioServicos_TmpEstAnl_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z, false);
            AddObjectProperty("UsuarioServicos_TmpEstExc_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z, false);
            AddObjectProperty("UsuarioServicos_TmpEstCrr_Z", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z, false);
            AddObjectProperty("UsuarioServicos_ServicoSigla_N", gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N, false);
            AddObjectProperty("UsuarioServicos_ServicoAtivo_N", gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N, false);
            AddObjectProperty("UsuarioServicos_TmpEstAnl_N", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N, false);
            AddObjectProperty("UsuarioServicos_TmpEstExc_N", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N, false);
            AddObjectProperty("UsuarioServicos_TmpEstCrr_N", gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_UsuarioCod" )]
      [  XmlElement( ElementName = "UsuarioServicos_UsuarioCod"   )]
      public int gxTpr_Usuarioservicos_usuariocod
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod ;
         }

         set {
            if ( gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod != value )
            {
               gxTv_SdtUsuarioServicos_Mode = "INS";
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z_SetNull( );
            }
            gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoCod" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoCod"   )]
      public int gxTpr_Usuarioservicos_servicocod
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod ;
         }

         set {
            if ( gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod != value )
            {
               gxTv_SdtUsuarioServicos_Mode = "INS";
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z_SetNull( );
               this.gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z_SetNull( );
            }
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoSigla" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoSigla"   )]
      public String gxTpr_Usuarioservicos_servicosigla
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N = 0;
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N = 1;
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoAtivo" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoAtivo"   )]
      public bool gxTpr_Usuarioservicos_servicoativo
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N = 0;
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo = value;
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N = 1;
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstAnl" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstAnl"   )]
      public int gxTpr_Usuarioservicos_tmpestanl
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N = 0;
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N = 1;
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstExc" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstExc"   )]
      public int gxTpr_Usuarioservicos_tmpestexc
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N = 0;
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N = 1;
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstCrr" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstCrr"   )]
      public int gxTpr_Usuarioservicos_tmpestcrr
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N = 0;
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N = 1;
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtUsuarioServicos_Mode ;
         }

         set {
            gxTv_SdtUsuarioServicos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Mode_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtUsuarioServicos_Initialized ;
         }

         set {
            gxTv_SdtUsuarioServicos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Initialized_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_UsuarioCod_Z"   )]
      public int gxTpr_Usuarioservicos_usuariocod_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoCod_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoCod_Z"   )]
      public int gxTpr_Usuarioservicos_servicocod_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoSigla_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoSigla_Z"   )]
      public String gxTpr_Usuarioservicos_servicosigla_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoAtivo_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoAtivo_Z"   )]
      public bool gxTpr_Usuarioservicos_servicoativo_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z = value;
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstAnl_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstAnl_Z"   )]
      public int gxTpr_Usuarioservicos_tmpestanl_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstExc_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstExc_Z"   )]
      public int gxTpr_Usuarioservicos_tmpestexc_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstCrr_Z" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstCrr_Z"   )]
      public int gxTpr_Usuarioservicos_tmpestcrr_Z
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z = (int)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoSigla_N" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoSigla_N"   )]
      public short gxTpr_Usuarioservicos_servicosigla_N
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_ServicoAtivo_N" )]
      [  XmlElement( ElementName = "UsuarioServicos_ServicoAtivo_N"   )]
      public short gxTpr_Usuarioservicos_servicoativo_N
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstAnl_N" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstAnl_N"   )]
      public short gxTpr_Usuarioservicos_tmpestanl_N
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstExc_N" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstExc_N"   )]
      public short gxTpr_Usuarioservicos_tmpestexc_N
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "UsuarioServicos_TmpEstCrr_N" )]
      [  XmlElement( ElementName = "UsuarioServicos_TmpEstCrr_N"   )]
      public short gxTpr_Usuarioservicos_tmpestcrr_N
      {
         get {
            return gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N ;
         }

         set {
            gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N = (short)(value);
         }

      }

      public void gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N_SetNull( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N = 0;
         return  ;
      }

      public bool gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla = "";
         gxTv_SdtUsuarioServicos_Mode = "";
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z = "";
         gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "usuarioservicos", "GeneXus.Programs.usuarioservicos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtUsuarioServicos_Initialized ;
      private short gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_N ;
      private short gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_N ;
      private short gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_N ;
      private short gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_N ;
      private short gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_usuariocod_Z ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_servicocod_Z ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestanl_Z ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestexc_Z ;
      private int gxTv_SdtUsuarioServicos_Usuarioservicos_tmpestcrr_Z ;
      private String gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla ;
      private String gxTv_SdtUsuarioServicos_Mode ;
      private String gxTv_SdtUsuarioServicos_Usuarioservicos_servicosigla_Z ;
      private String sTagName ;
      private bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo ;
      private bool gxTv_SdtUsuarioServicos_Usuarioservicos_servicoativo_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"UsuarioServicos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtUsuarioServicos_RESTInterface : GxGenericCollectionItem<SdtUsuarioServicos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUsuarioServicos_RESTInterface( ) : base()
      {
      }

      public SdtUsuarioServicos_RESTInterface( SdtUsuarioServicos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "UsuarioServicos_UsuarioCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuarioservicos_usuariocod
      {
         get {
            return sdt.gxTpr_Usuarioservicos_usuariocod ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "UsuarioServicos_ServicoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuarioservicos_servicocod
      {
         get {
            return sdt.gxTpr_Usuarioservicos_servicocod ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_servicocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "UsuarioServicos_ServicoSigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Usuarioservicos_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuarioservicos_servicosigla) ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "UsuarioServicos_ServicoAtivo" , Order = 3 )]
      [GxSeudo()]
      public bool gxTpr_Usuarioservicos_servicoativo
      {
         get {
            return sdt.gxTpr_Usuarioservicos_servicoativo ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_servicoativo = value;
         }

      }

      [DataMember( Name = "UsuarioServicos_TmpEstAnl" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Usuarioservicos_tmpestanl
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Usuarioservicos_tmpestanl), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_tmpestanl = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "UsuarioServicos_TmpEstExc" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Usuarioservicos_tmpestexc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Usuarioservicos_tmpestexc), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_tmpestexc = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "UsuarioServicos_TmpEstCrr" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Usuarioservicos_tmpestcrr
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Usuarioservicos_tmpestcrr), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Usuarioservicos_tmpestcrr = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      public SdtUsuarioServicos sdt
      {
         get {
            return (SdtUsuarioServicos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtUsuarioServicos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 21 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
