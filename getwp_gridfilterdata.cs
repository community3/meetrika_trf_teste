/*
               File: GetWP_GridFilterData
        Description: Get WP_Grid Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:36.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwp_gridfilterdata : GXProcedure
   {
      public getwp_gridfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwp_gridfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwp_gridfilterdata objgetwp_gridfilterdata;
         objgetwp_gridfilterdata = new getwp_gridfilterdata();
         objgetwp_gridfilterdata.AV24DDOName = aP0_DDOName;
         objgetwp_gridfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwp_gridfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwp_gridfilterdata.AV28OptionsJson = "" ;
         objgetwp_gridfilterdata.AV31OptionsDescJson = "" ;
         objgetwp_gridfilterdata.AV33OptionIndexesJson = "" ;
         objgetwp_gridfilterdata.context.SetSubmitInitialConfig(context);
         objgetwp_gridfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwp_gridfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwp_gridfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_CONTRATADAPESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WP_GridGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WP_GridGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WP_GridGridState"), "");
         }
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV42GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATAENTREGA") == 0 )
            {
               AV10TFContagemResultado_DataEntrega = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContagemResultado_DataEntrega_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV12TFContagemResultado_DataDmn = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContagemResultado_DataDmn_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_HORAENTREGA") == 0 )
            {
               AV14TFContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2));
               AV15TFContagemResultado_HoraEntrega_To = DateTimeUtil.ResetDate(context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM") == 0 )
            {
               AV16TFContagemResultado_ContratadaPessoaNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL") == 0 )
            {
               AV17TFContagemResultado_ContratadaPessoaNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV18TFContagemResultado_DemandaFM = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV19TFContagemResultado_DemandaFM_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
            {
               AV20TFContagemResultado_ServicoSigla = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL") == 0 )
            {
               AV21TFContagemResultado_ServicoSigla_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_CONTRATADAPESSOANOMOPTIONS' Routine */
         AV16TFContagemResultado_ContratadaPessoaNom = AV22SearchTxt;
         AV17TFContagemResultado_ContratadaPessoaNom_Sel = "";
         AV44WP_GridDS_1_Tfcontagemresultado_dataentrega = AV10TFContagemResultado_DataEntrega;
         AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV11TFContagemResultado_DataEntrega_To;
         AV46WP_GridDS_3_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV48WP_GridDS_5_Tfcontagemresultado_horaentrega = AV14TFContagemResultado_HoraEntrega;
         AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV15TFContagemResultado_HoraEntrega_To;
         AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV16TFContagemResultado_ContratadaPessoaNom;
         AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV17TFContagemResultado_ContratadaPessoaNom_Sel;
         AV52WP_GridDS_9_Tfcontagemresultado_demandafm = AV18TFContagemResultado_DemandaFM;
         AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV19TFContagemResultado_DemandaFM_Sel;
         AV54WP_GridDS_11_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                              AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                              AV46WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                              AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                              AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                              AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                              AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                              AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                              AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                              AV52WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                              AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                              AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                              A472ContagemResultado_DataEntrega ,
                                              A471ContagemResultado_DataDmn ,
                                              A912ContagemResultado_HoraEntrega ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A493ContagemResultado_DemandaFM ,
                                              A801ContagemResultado_ServicoSigla },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom), 100, "%");
         lV52WP_GridDS_9_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV52WP_GridDS_9_Tfcontagemresultado_demandafm), "%", "");
         lV54WP_GridDS_11_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV54WP_GridDS_11_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00R02 */
         pr_default.execute(0, new Object[] {AV44WP_GridDS_1_Tfcontagemresultado_dataentrega, AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to, AV46WP_GridDS_3_Tfcontagemresultado_datadmn, AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to, AV48WP_GridDS_5_Tfcontagemresultado_horaentrega, AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to, lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom, AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel, lV52WP_GridDS_9_Tfcontagemresultado_demandafm, AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel, lV54WP_GridDS_11_Tfcontagemresultado_servicosigla, AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKR02 = false;
            A490ContagemResultado_ContratadaCod = P00R02_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00R02_n490ContagemResultado_ContratadaCod[0];
            A499ContagemResultado_ContratadaPessoaCod = P00R02_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00R02_n499ContagemResultado_ContratadaPessoaCod[0];
            A1553ContagemResultado_CntSrvCod = P00R02_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00R02_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00R02_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00R02_n601ContagemResultado_Servico[0];
            A500ContagemResultado_ContratadaPessoaNom = P00R02_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00R02_n500ContagemResultado_ContratadaPessoaNom[0];
            A801ContagemResultado_ServicoSigla = P00R02_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00R02_n801ContagemResultado_ServicoSigla[0];
            A493ContagemResultado_DemandaFM = P00R02_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00R02_n493ContagemResultado_DemandaFM[0];
            A912ContagemResultado_HoraEntrega = P00R02_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00R02_n912ContagemResultado_HoraEntrega[0];
            A471ContagemResultado_DataDmn = P00R02_A471ContagemResultado_DataDmn[0];
            A472ContagemResultado_DataEntrega = P00R02_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00R02_n472ContagemResultado_DataEntrega[0];
            A456ContagemResultado_Codigo = P00R02_A456ContagemResultado_Codigo[0];
            A499ContagemResultado_ContratadaPessoaCod = P00R02_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00R02_n499ContagemResultado_ContratadaPessoaCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00R02_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00R02_n500ContagemResultado_ContratadaPessoaNom[0];
            A601ContagemResultado_Servico = P00R02_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00R02_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00R02_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00R02_n801ContagemResultado_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00R02_A500ContagemResultado_ContratadaPessoaNom[0], A500ContagemResultado_ContratadaPessoaNom) == 0 ) )
            {
               BRKR02 = false;
               A490ContagemResultado_ContratadaCod = P00R02_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00R02_n490ContagemResultado_ContratadaCod[0];
               A499ContagemResultado_ContratadaPessoaCod = P00R02_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = P00R02_n499ContagemResultado_ContratadaPessoaCod[0];
               A456ContagemResultado_Codigo = P00R02_A456ContagemResultado_Codigo[0];
               A499ContagemResultado_ContratadaPessoaCod = P00R02_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = P00R02_n499ContagemResultado_ContratadaPessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKR02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom)) )
            {
               AV26Option = A500ContagemResultado_ContratadaPessoaNom;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKR02 )
            {
               BRKR02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' Routine */
         AV18TFContagemResultado_DemandaFM = AV22SearchTxt;
         AV19TFContagemResultado_DemandaFM_Sel = "";
         AV44WP_GridDS_1_Tfcontagemresultado_dataentrega = AV10TFContagemResultado_DataEntrega;
         AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV11TFContagemResultado_DataEntrega_To;
         AV46WP_GridDS_3_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV48WP_GridDS_5_Tfcontagemresultado_horaentrega = AV14TFContagemResultado_HoraEntrega;
         AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV15TFContagemResultado_HoraEntrega_To;
         AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV16TFContagemResultado_ContratadaPessoaNom;
         AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV17TFContagemResultado_ContratadaPessoaNom_Sel;
         AV52WP_GridDS_9_Tfcontagemresultado_demandafm = AV18TFContagemResultado_DemandaFM;
         AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV19TFContagemResultado_DemandaFM_Sel;
         AV54WP_GridDS_11_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                              AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                              AV46WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                              AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                              AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                              AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                              AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                              AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                              AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                              AV52WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                              AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                              AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                              A472ContagemResultado_DataEntrega ,
                                              A471ContagemResultado_DataDmn ,
                                              A912ContagemResultado_HoraEntrega ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A493ContagemResultado_DemandaFM ,
                                              A801ContagemResultado_ServicoSigla },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom), 100, "%");
         lV52WP_GridDS_9_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV52WP_GridDS_9_Tfcontagemresultado_demandafm), "%", "");
         lV54WP_GridDS_11_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV54WP_GridDS_11_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00R03 */
         pr_default.execute(1, new Object[] {AV44WP_GridDS_1_Tfcontagemresultado_dataentrega, AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to, AV46WP_GridDS_3_Tfcontagemresultado_datadmn, AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to, AV48WP_GridDS_5_Tfcontagemresultado_horaentrega, AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to, lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom, AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel, lV52WP_GridDS_9_Tfcontagemresultado_demandafm, AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel, lV54WP_GridDS_11_Tfcontagemresultado_servicosigla, AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKR04 = false;
            A490ContagemResultado_ContratadaCod = P00R03_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00R03_n490ContagemResultado_ContratadaCod[0];
            A499ContagemResultado_ContratadaPessoaCod = P00R03_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00R03_n499ContagemResultado_ContratadaPessoaCod[0];
            A1553ContagemResultado_CntSrvCod = P00R03_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00R03_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00R03_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00R03_n601ContagemResultado_Servico[0];
            A493ContagemResultado_DemandaFM = P00R03_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00R03_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = P00R03_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00R03_n801ContagemResultado_ServicoSigla[0];
            A500ContagemResultado_ContratadaPessoaNom = P00R03_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00R03_n500ContagemResultado_ContratadaPessoaNom[0];
            A912ContagemResultado_HoraEntrega = P00R03_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00R03_n912ContagemResultado_HoraEntrega[0];
            A471ContagemResultado_DataDmn = P00R03_A471ContagemResultado_DataDmn[0];
            A472ContagemResultado_DataEntrega = P00R03_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00R03_n472ContagemResultado_DataEntrega[0];
            A456ContagemResultado_Codigo = P00R03_A456ContagemResultado_Codigo[0];
            A499ContagemResultado_ContratadaPessoaCod = P00R03_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00R03_n499ContagemResultado_ContratadaPessoaCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00R03_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00R03_n500ContagemResultado_ContratadaPessoaNom[0];
            A601ContagemResultado_Servico = P00R03_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00R03_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00R03_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00R03_n801ContagemResultado_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00R03_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
            {
               BRKR04 = false;
               A456ContagemResultado_Codigo = P00R03_A456ContagemResultado_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKR04 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV26Option = A493ContagemResultado_DemandaFM;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKR04 )
            {
               BRKR04 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' Routine */
         AV20TFContagemResultado_ServicoSigla = AV22SearchTxt;
         AV21TFContagemResultado_ServicoSigla_Sel = "";
         AV44WP_GridDS_1_Tfcontagemresultado_dataentrega = AV10TFContagemResultado_DataEntrega;
         AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to = AV11TFContagemResultado_DataEntrega_To;
         AV46WP_GridDS_3_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV48WP_GridDS_5_Tfcontagemresultado_horaentrega = AV14TFContagemResultado_HoraEntrega;
         AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to = AV15TFContagemResultado_HoraEntrega_To;
         AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = AV16TFContagemResultado_ContratadaPessoaNom;
         AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = AV17TFContagemResultado_ContratadaPessoaNom_Sel;
         AV52WP_GridDS_9_Tfcontagemresultado_demandafm = AV18TFContagemResultado_DemandaFM;
         AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel = AV19TFContagemResultado_DemandaFM_Sel;
         AV54WP_GridDS_11_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                              AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                              AV46WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                              AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                              AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                              AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                              AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                              AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                              AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                              AV52WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                              AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                              AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                              A472ContagemResultado_DataEntrega ,
                                              A471ContagemResultado_DataDmn ,
                                              A912ContagemResultado_HoraEntrega ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A493ContagemResultado_DemandaFM ,
                                              A801ContagemResultado_ServicoSigla },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom), 100, "%");
         lV52WP_GridDS_9_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV52WP_GridDS_9_Tfcontagemresultado_demandafm), "%", "");
         lV54WP_GridDS_11_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV54WP_GridDS_11_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00R04 */
         pr_default.execute(2, new Object[] {AV44WP_GridDS_1_Tfcontagemresultado_dataentrega, AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to, AV46WP_GridDS_3_Tfcontagemresultado_datadmn, AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to, AV48WP_GridDS_5_Tfcontagemresultado_horaentrega, AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to, lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom, AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel, lV52WP_GridDS_9_Tfcontagemresultado_demandafm, AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel, lV54WP_GridDS_11_Tfcontagemresultado_servicosigla, AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKR06 = false;
            A490ContagemResultado_ContratadaCod = P00R04_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00R04_n490ContagemResultado_ContratadaCod[0];
            A499ContagemResultado_ContratadaPessoaCod = P00R04_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00R04_n499ContagemResultado_ContratadaPessoaCod[0];
            A1553ContagemResultado_CntSrvCod = P00R04_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00R04_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00R04_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00R04_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00R04_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00R04_n801ContagemResultado_ServicoSigla[0];
            A493ContagemResultado_DemandaFM = P00R04_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00R04_n493ContagemResultado_DemandaFM[0];
            A500ContagemResultado_ContratadaPessoaNom = P00R04_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00R04_n500ContagemResultado_ContratadaPessoaNom[0];
            A912ContagemResultado_HoraEntrega = P00R04_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00R04_n912ContagemResultado_HoraEntrega[0];
            A471ContagemResultado_DataDmn = P00R04_A471ContagemResultado_DataDmn[0];
            A472ContagemResultado_DataEntrega = P00R04_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00R04_n472ContagemResultado_DataEntrega[0];
            A456ContagemResultado_Codigo = P00R04_A456ContagemResultado_Codigo[0];
            A499ContagemResultado_ContratadaPessoaCod = P00R04_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00R04_n499ContagemResultado_ContratadaPessoaCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00R04_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00R04_n500ContagemResultado_ContratadaPessoaNom[0];
            A601ContagemResultado_Servico = P00R04_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00R04_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00R04_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00R04_n801ContagemResultado_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00R04_A801ContagemResultado_ServicoSigla[0], A801ContagemResultado_ServicoSigla) == 0 ) )
            {
               BRKR06 = false;
               A1553ContagemResultado_CntSrvCod = P00R04_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00R04_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = P00R04_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00R04_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = P00R04_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P00R04_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00R04_n601ContagemResultado_Servico[0];
               AV34count = (long)(AV34count+1);
               BRKR06 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) )
            {
               AV26Option = A801ContagemResultado_ServicoSigla;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKR06 )
            {
               BRKR06 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_DataEntrega = DateTime.MinValue;
         AV11TFContagemResultado_DataEntrega_To = DateTime.MinValue;
         AV12TFContagemResultado_DataDmn = DateTime.MinValue;
         AV13TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV14TFContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV15TFContagemResultado_HoraEntrega_To = (DateTime)(DateTime.MinValue);
         AV16TFContagemResultado_ContratadaPessoaNom = "";
         AV17TFContagemResultado_ContratadaPessoaNom_Sel = "";
         AV18TFContagemResultado_DemandaFM = "";
         AV19TFContagemResultado_DemandaFM_Sel = "";
         AV20TFContagemResultado_ServicoSigla = "";
         AV21TFContagemResultado_ServicoSigla_Sel = "";
         AV44WP_GridDS_1_Tfcontagemresultado_dataentrega = DateTime.MinValue;
         AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to = DateTime.MinValue;
         AV46WP_GridDS_3_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV48WP_GridDS_5_Tfcontagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to = (DateTime)(DateTime.MinValue);
         AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = "";
         AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel = "";
         AV52WP_GridDS_9_Tfcontagemresultado_demandafm = "";
         AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel = "";
         AV54WP_GridDS_11_Tfcontagemresultado_servicosigla = "";
         AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom = "";
         lV52WP_GridDS_9_Tfcontagemresultado_demandafm = "";
         lV54WP_GridDS_11_Tfcontagemresultado_servicosigla = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A500ContagemResultado_ContratadaPessoaNom = "";
         A493ContagemResultado_DemandaFM = "";
         A801ContagemResultado_ServicoSigla = "";
         P00R02_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00R02_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00R02_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00R02_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00R02_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00R02_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00R02_A601ContagemResultado_Servico = new int[1] ;
         P00R02_n601ContagemResultado_Servico = new bool[] {false} ;
         P00R02_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00R02_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00R02_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00R02_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00R02_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00R02_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00R02_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00R02_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00R02_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00R02_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00R02_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00R02_A456ContagemResultado_Codigo = new int[1] ;
         AV26Option = "";
         P00R03_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00R03_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00R03_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00R03_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00R03_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00R03_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00R03_A601ContagemResultado_Servico = new int[1] ;
         P00R03_n601ContagemResultado_Servico = new bool[] {false} ;
         P00R03_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00R03_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00R03_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00R03_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00R03_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00R03_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00R03_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00R03_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00R03_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00R03_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00R03_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00R03_A456ContagemResultado_Codigo = new int[1] ;
         P00R04_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00R04_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00R04_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00R04_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00R04_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00R04_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00R04_A601ContagemResultado_Servico = new int[1] ;
         P00R04_n601ContagemResultado_Servico = new bool[] {false} ;
         P00R04_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00R04_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00R04_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00R04_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00R04_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00R04_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00R04_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00R04_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00R04_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00R04_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00R04_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00R04_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwp_gridfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00R02_A490ContagemResultado_ContratadaCod, P00R02_n490ContagemResultado_ContratadaCod, P00R02_A499ContagemResultado_ContratadaPessoaCod, P00R02_n499ContagemResultado_ContratadaPessoaCod, P00R02_A1553ContagemResultado_CntSrvCod, P00R02_n1553ContagemResultado_CntSrvCod, P00R02_A601ContagemResultado_Servico, P00R02_n601ContagemResultado_Servico, P00R02_A500ContagemResultado_ContratadaPessoaNom, P00R02_n500ContagemResultado_ContratadaPessoaNom,
               P00R02_A801ContagemResultado_ServicoSigla, P00R02_n801ContagemResultado_ServicoSigla, P00R02_A493ContagemResultado_DemandaFM, P00R02_n493ContagemResultado_DemandaFM, P00R02_A912ContagemResultado_HoraEntrega, P00R02_n912ContagemResultado_HoraEntrega, P00R02_A471ContagemResultado_DataDmn, P00R02_A472ContagemResultado_DataEntrega, P00R02_n472ContagemResultado_DataEntrega, P00R02_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00R03_A490ContagemResultado_ContratadaCod, P00R03_n490ContagemResultado_ContratadaCod, P00R03_A499ContagemResultado_ContratadaPessoaCod, P00R03_n499ContagemResultado_ContratadaPessoaCod, P00R03_A1553ContagemResultado_CntSrvCod, P00R03_n1553ContagemResultado_CntSrvCod, P00R03_A601ContagemResultado_Servico, P00R03_n601ContagemResultado_Servico, P00R03_A493ContagemResultado_DemandaFM, P00R03_n493ContagemResultado_DemandaFM,
               P00R03_A801ContagemResultado_ServicoSigla, P00R03_n801ContagemResultado_ServicoSigla, P00R03_A500ContagemResultado_ContratadaPessoaNom, P00R03_n500ContagemResultado_ContratadaPessoaNom, P00R03_A912ContagemResultado_HoraEntrega, P00R03_n912ContagemResultado_HoraEntrega, P00R03_A471ContagemResultado_DataDmn, P00R03_A472ContagemResultado_DataEntrega, P00R03_n472ContagemResultado_DataEntrega, P00R03_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00R04_A490ContagemResultado_ContratadaCod, P00R04_n490ContagemResultado_ContratadaCod, P00R04_A499ContagemResultado_ContratadaPessoaCod, P00R04_n499ContagemResultado_ContratadaPessoaCod, P00R04_A1553ContagemResultado_CntSrvCod, P00R04_n1553ContagemResultado_CntSrvCod, P00R04_A601ContagemResultado_Servico, P00R04_n601ContagemResultado_Servico, P00R04_A801ContagemResultado_ServicoSigla, P00R04_n801ContagemResultado_ServicoSigla,
               P00R04_A493ContagemResultado_DemandaFM, P00R04_n493ContagemResultado_DemandaFM, P00R04_A500ContagemResultado_ContratadaPessoaNom, P00R04_n500ContagemResultado_ContratadaPessoaNom, P00R04_A912ContagemResultado_HoraEntrega, P00R04_n912ContagemResultado_HoraEntrega, P00R04_A471ContagemResultado_DataDmn, P00R04_A472ContagemResultado_DataEntrega, P00R04_n472ContagemResultado_DataEntrega, P00R04_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV42GXV1 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private long AV34count ;
      private String AV16TFContagemResultado_ContratadaPessoaNom ;
      private String AV17TFContagemResultado_ContratadaPessoaNom_Sel ;
      private String AV20TFContagemResultado_ServicoSigla ;
      private String AV21TFContagemResultado_ServicoSigla_Sel ;
      private String AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ;
      private String AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ;
      private String AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ;
      private String AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ;
      private String lV54WP_GridDS_11_Tfcontagemresultado_servicosigla ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime AV14TFContagemResultado_HoraEntrega ;
      private DateTime AV15TFContagemResultado_HoraEntrega_To ;
      private DateTime AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ;
      private DateTime AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV10TFContagemResultado_DataEntrega ;
      private DateTime AV11TFContagemResultado_DataEntrega_To ;
      private DateTime AV12TFContagemResultado_DataDmn ;
      private DateTime AV13TFContagemResultado_DataDmn_To ;
      private DateTime AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ;
      private DateTime AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ;
      private DateTime AV46WP_GridDS_3_Tfcontagemresultado_datadmn ;
      private DateTime AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool returnInSub ;
      private bool BRKR02 ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool BRKR04 ;
      private bool BRKR06 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV18TFContagemResultado_DemandaFM ;
      private String AV19TFContagemResultado_DemandaFM_Sel ;
      private String AV52WP_GridDS_9_Tfcontagemresultado_demandafm ;
      private String AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ;
      private String lV52WP_GridDS_9_Tfcontagemresultado_demandafm ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00R02_A490ContagemResultado_ContratadaCod ;
      private bool[] P00R02_n490ContagemResultado_ContratadaCod ;
      private int[] P00R02_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00R02_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00R02_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00R02_n1553ContagemResultado_CntSrvCod ;
      private int[] P00R02_A601ContagemResultado_Servico ;
      private bool[] P00R02_n601ContagemResultado_Servico ;
      private String[] P00R02_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00R02_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00R02_A801ContagemResultado_ServicoSigla ;
      private bool[] P00R02_n801ContagemResultado_ServicoSigla ;
      private String[] P00R02_A493ContagemResultado_DemandaFM ;
      private bool[] P00R02_n493ContagemResultado_DemandaFM ;
      private DateTime[] P00R02_A912ContagemResultado_HoraEntrega ;
      private bool[] P00R02_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00R02_A471ContagemResultado_DataDmn ;
      private DateTime[] P00R02_A472ContagemResultado_DataEntrega ;
      private bool[] P00R02_n472ContagemResultado_DataEntrega ;
      private int[] P00R02_A456ContagemResultado_Codigo ;
      private int[] P00R03_A490ContagemResultado_ContratadaCod ;
      private bool[] P00R03_n490ContagemResultado_ContratadaCod ;
      private int[] P00R03_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00R03_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00R03_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00R03_n1553ContagemResultado_CntSrvCod ;
      private int[] P00R03_A601ContagemResultado_Servico ;
      private bool[] P00R03_n601ContagemResultado_Servico ;
      private String[] P00R03_A493ContagemResultado_DemandaFM ;
      private bool[] P00R03_n493ContagemResultado_DemandaFM ;
      private String[] P00R03_A801ContagemResultado_ServicoSigla ;
      private bool[] P00R03_n801ContagemResultado_ServicoSigla ;
      private String[] P00R03_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00R03_n500ContagemResultado_ContratadaPessoaNom ;
      private DateTime[] P00R03_A912ContagemResultado_HoraEntrega ;
      private bool[] P00R03_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00R03_A471ContagemResultado_DataDmn ;
      private DateTime[] P00R03_A472ContagemResultado_DataEntrega ;
      private bool[] P00R03_n472ContagemResultado_DataEntrega ;
      private int[] P00R03_A456ContagemResultado_Codigo ;
      private int[] P00R04_A490ContagemResultado_ContratadaCod ;
      private bool[] P00R04_n490ContagemResultado_ContratadaCod ;
      private int[] P00R04_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00R04_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00R04_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00R04_n1553ContagemResultado_CntSrvCod ;
      private int[] P00R04_A601ContagemResultado_Servico ;
      private bool[] P00R04_n601ContagemResultado_Servico ;
      private String[] P00R04_A801ContagemResultado_ServicoSigla ;
      private bool[] P00R04_n801ContagemResultado_ServicoSigla ;
      private String[] P00R04_A493ContagemResultado_DemandaFM ;
      private bool[] P00R04_n493ContagemResultado_DemandaFM ;
      private String[] P00R04_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00R04_n500ContagemResultado_ContratadaPessoaNom ;
      private DateTime[] P00R04_A912ContagemResultado_HoraEntrega ;
      private bool[] P00R04_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00R04_A471ContagemResultado_DataDmn ;
      private DateTime[] P00R04_A472ContagemResultado_DataEntrega ;
      private bool[] P00R04_n472ContagemResultado_DataEntrega ;
      private int[] P00R04_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
   }

   public class getwp_gridfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00R02( IGxContext context ,
                                             DateTime AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                             DateTime AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                             DateTime AV46WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                             DateTime AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                             DateTime AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                             String AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                             String AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                             String AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                             String AV52WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                             String AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                             String AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A912ContagemResultado_HoraEntrega ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A801ContagemResultado_ServicoSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( ! (DateTime.MinValue==AV44WP_GridDS_1_Tfcontagemresultado_dataentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] >= @AV44WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] >= @AV44WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] <= @AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] <= @AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV46WP_GridDS_3_Tfcontagemresultado_datadmn) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV46WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV46WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV48WP_GridDS_5_Tfcontagemresultado_horaentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] >= @AV48WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] >= @AV48WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] <= @AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] <= @AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WP_GridDS_9_Tfcontagemresultado_demandafm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV52WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] like @lV52WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] = @AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WP_GridDS_11_Tfcontagemresultado_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV54WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV54WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00R03( IGxContext context ,
                                             DateTime AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                             DateTime AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                             DateTime AV46WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                             DateTime AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                             DateTime AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                             String AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                             String AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                             String AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                             String AV52WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                             String AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                             String AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A912ContagemResultado_HoraEntrega ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A801ContagemResultado_ServicoSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_DemandaFM], T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T3.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( ! (DateTime.MinValue==AV44WP_GridDS_1_Tfcontagemresultado_dataentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] >= @AV44WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] >= @AV44WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] <= @AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] <= @AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV46WP_GridDS_3_Tfcontagemresultado_datadmn) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV46WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV46WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV48WP_GridDS_5_Tfcontagemresultado_horaentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] >= @AV48WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] >= @AV48WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] <= @AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] <= @AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WP_GridDS_9_Tfcontagemresultado_demandafm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV52WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] like @lV52WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] = @AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WP_GridDS_11_Tfcontagemresultado_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV54WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV54WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00R04( IGxContext context ,
                                             DateTime AV44WP_GridDS_1_Tfcontagemresultado_dataentrega ,
                                             DateTime AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to ,
                                             DateTime AV46WP_GridDS_3_Tfcontagemresultado_datadmn ,
                                             DateTime AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV48WP_GridDS_5_Tfcontagemresultado_horaentrega ,
                                             DateTime AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to ,
                                             String AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel ,
                                             String AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom ,
                                             String AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel ,
                                             String AV52WP_GridDS_9_Tfcontagemresultado_demandafm ,
                                             String AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel ,
                                             String AV54WP_GridDS_11_Tfcontagemresultado_servicosigla ,
                                             DateTime A472ContagemResultado_DataEntrega ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A912ContagemResultado_HoraEntrega ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A801ContagemResultado_ServicoSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T3.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( ! (DateTime.MinValue==AV44WP_GridDS_1_Tfcontagemresultado_dataentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] >= @AV44WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] >= @AV44WP_GridDS_1_Tfcontagemresultado_dataentrega)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntrega] <= @AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataEntrega] <= @AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV46WP_GridDS_3_Tfcontagemresultado_datadmn) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV46WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] >= @AV46WP_GridDS_3_Tfcontagemresultado_datadmn)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DataDmn] <= @AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV48WP_GridDS_5_Tfcontagemresultado_horaentrega) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] >= @AV48WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] >= @AV48WP_GridDS_5_Tfcontagemresultado_horaentrega)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraEntrega] <= @AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_HoraEntrega] <= @AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WP_GridDS_9_Tfcontagemresultado_demandafm)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV52WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] like @lV52WP_GridDS_9_Tfcontagemresultado_demandafm)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_DemandaFM] = @AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WP_GridDS_11_Tfcontagemresultado_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV54WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV54WP_GridDS_11_Tfcontagemresultado_servicosigla)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Servico_Sigla]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00R02(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 1 :
                     return conditional_P00R03(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 2 :
                     return conditional_P00R04(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00R02 ;
          prmP00R02 = new Object[] {
          new Object[] {"@AV44WP_GridDS_1_Tfcontagemresultado_dataentrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46WP_GridDS_3_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48WP_GridDS_5_Tfcontagemresultado_horaentrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52WP_GridDS_9_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WP_GridDS_11_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00R03 ;
          prmP00R03 = new Object[] {
          new Object[] {"@AV44WP_GridDS_1_Tfcontagemresultado_dataentrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46WP_GridDS_3_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48WP_GridDS_5_Tfcontagemresultado_horaentrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52WP_GridDS_9_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WP_GridDS_11_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00R04 ;
          prmP00R04 = new Object[] {
          new Object[] {"@AV44WP_GridDS_1_Tfcontagemresultado_dataentrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45WP_GridDS_2_Tfcontagemresultado_dataentrega_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46WP_GridDS_3_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47WP_GridDS_4_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48WP_GridDS_5_Tfcontagemresultado_horaentrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV49WP_GridDS_6_Tfcontagemresultado_horaentrega_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@lV50WP_GridDS_7_Tfcontagemresultado_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV51WP_GridDS_8_Tfcontagemresultado_contratadapessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52WP_GridDS_9_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV53WP_GridDS_10_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WP_GridDS_11_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV55WP_GridDS_12_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00R02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00R02,100,0,true,false )
             ,new CursorDef("P00R03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00R03,100,0,true,false )
             ,new CursorDef("P00R04", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00R04,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwp_gridfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwp_gridfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwp_gridfilterdata") )
          {
             return  ;
          }
          getwp_gridfilterdata worker = new getwp_gridfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
