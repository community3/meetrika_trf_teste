/*
               File: GetWWContratanteFilterData
        Description: Get WWContratante Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/31/2020 1:13:45.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratantefilterdata : GXProcedure
   {
      public getwwcontratantefilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratantefilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV30DDOName = aP0_DDOName;
         this.AV28SearchTxt = aP1_SearchTxt;
         this.AV29SearchTxtTo = aP2_SearchTxtTo;
         this.AV34OptionsJson = "" ;
         this.AV37OptionsDescJson = "" ;
         this.AV39OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
         return AV39OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratantefilterdata objgetwwcontratantefilterdata;
         objgetwwcontratantefilterdata = new getwwcontratantefilterdata();
         objgetwwcontratantefilterdata.AV30DDOName = aP0_DDOName;
         objgetwwcontratantefilterdata.AV28SearchTxt = aP1_SearchTxt;
         objgetwwcontratantefilterdata.AV29SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratantefilterdata.AV34OptionsJson = "" ;
         objgetwwcontratantefilterdata.AV37OptionsDescJson = "" ;
         objgetwwcontratantefilterdata.AV39OptionIndexesJson = "" ;
         objgetwwcontratantefilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratantefilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratantefilterdata);
         aP3_OptionsJson=this.AV34OptionsJson;
         aP4_OptionsDescJson=this.AV37OptionsDescJson;
         aP5_OptionIndexesJson=this.AV39OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratantefilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV33Options = (IGxCollection)(new GxSimpleCollection());
         AV36OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV38OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_RAZAOSOCIALOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATANTE_NOMEFANTASIA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_NOMEFANTASIAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATANTE_CNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_CNPJOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATANTE_TELEFONE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_TELEFONEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_CONTRATANTE_RAMAL") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTE_RAMALOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_MUNICIPIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMUNICIPIO_NOMEOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV30DDOName), "DDO_ESTADO_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_UFOPTIONS' */
            S181 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV34OptionsJson = AV33Options.ToJSonString(false);
         AV37OptionsDescJson = AV36OptionsDesc.ToJSonString(false);
         AV39OptionIndexesJson = AV38OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV41Session.Get("WWContratanteGridState"), "") == 0 )
         {
            AV43GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratanteGridState"), "");
         }
         else
         {
            AV43GridState.FromXml(AV41Session.Get("WWContratanteGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV43GridState.gxTpr_Filtervalues.Count )
         {
            AV44GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV43GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "AREATRABALHO_CODIGO") == 0 )
            {
               AV46AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV44GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               AV10TFContratante_RazaoSocial = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL_SEL") == 0 )
            {
               AV11TFContratante_RazaoSocial_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_NOMEFANTASIA") == 0 )
            {
               AV12TFContratante_NomeFantasia = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_NOMEFANTASIA_SEL") == 0 )
            {
               AV13TFContratante_NomeFantasia_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ") == 0 )
            {
               AV14TFContratante_CNPJ = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ_SEL") == 0 )
            {
               AV15TFContratante_CNPJ_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE") == 0 )
            {
               AV16TFContratante_Telefone = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE_SEL") == 0 )
            {
               AV17TFContratante_Telefone_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAMAL") == 0 )
            {
               AV18TFContratante_Ramal = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAMAL_SEL") == 0 )
            {
               AV19TFContratante_Ramal_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV20TFMunicipio_Nome = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV21TFMunicipio_Nome_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV22TFEstado_UF = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV23TFEstado_UF_Sel = AV44GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_INICIODOEXPEDIENTE") == 0 )
            {
               AV24TFContratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( AV44GridStateFilterValue.gxTpr_Value, 2));
               AV25TFContratante_InicioDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( AV44GridStateFilterValue.gxTpr_Valueto, 2));
            }
            else if ( StringUtil.StrCmp(AV44GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_FIMDOEXPEDIENTE") == 0 )
            {
               AV26TFContratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( AV44GridStateFilterValue.gxTpr_Value, 2));
               AV27TFContratante_FimDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( AV44GridStateFilterValue.gxTpr_Valueto, 2));
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATANTE_RAZAOSOCIALOPTIONS' Routine */
         AV10TFContratante_RazaoSocial = AV28SearchTxt;
         AV11TFContratante_RazaoSocial_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKEB2 = false;
            A29Contratante_Codigo = P00EB2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB2_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00EB2_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB2_n25Municipio_Codigo[0];
            A5AreaTrabalho_Codigo = P00EB2_A5AreaTrabalho_Codigo[0];
            A335Contratante_PessoaCod = P00EB2_A335Contratante_PessoaCod[0];
            A1192Contratante_FimDoExpediente = P00EB2_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB2_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB2_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB2_n1448Contratante_InicioDoExpediente[0];
            A23Estado_UF = P00EB2_A23Estado_UF[0];
            A26Municipio_Nome = P00EB2_A26Municipio_Nome[0];
            A32Contratante_Ramal = P00EB2_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB2_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB2_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00EB2_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB2_n12Contratante_CNPJ[0];
            A10Contratante_NomeFantasia = P00EB2_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00EB2_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB2_n9Contratante_RazaoSocial[0];
            A25Municipio_Codigo = P00EB2_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB2_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB2_A335Contratante_PessoaCod[0];
            A1192Contratante_FimDoExpediente = P00EB2_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB2_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB2_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB2_n1448Contratante_InicioDoExpediente[0];
            A32Contratante_Ramal = P00EB2_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB2_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB2_A31Contratante_Telefone[0];
            A10Contratante_NomeFantasia = P00EB2_A10Contratante_NomeFantasia[0];
            A23Estado_UF = P00EB2_A23Estado_UF[0];
            A26Municipio_Nome = P00EB2_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00EB2_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB2_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB2_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB2_n9Contratante_RazaoSocial[0];
            AV40count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00EB2_A335Contratante_PessoaCod[0] == A335Contratante_PessoaCod ) )
            {
               BRKEB2 = false;
               A29Contratante_Codigo = P00EB2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB2_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00EB2_A5AreaTrabalho_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKEB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A9Contratante_RazaoSocial)) )
            {
               AV32Option = A9Contratante_RazaoSocial;
               AV31InsertIndex = 1;
               while ( ( AV31InsertIndex <= AV33Options.Count ) && ( StringUtil.StrCmp(((String)AV33Options.Item(AV31InsertIndex)), AV32Option) < 0 ) )
               {
                  AV31InsertIndex = (int)(AV31InsertIndex+1);
               }
               AV33Options.Add(AV32Option, AV31InsertIndex);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), AV31InsertIndex);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB2 )
            {
               BRKEB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATANTE_NOMEFANTASIAOPTIONS' Routine */
         AV12TFContratante_NomeFantasia = AV28SearchTxt;
         AV13TFContratante_NomeFantasia_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKEB4 = false;
            A29Contratante_Codigo = P00EB3_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB3_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00EB3_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB3_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB3_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00EB3_A5AreaTrabalho_Codigo[0];
            A10Contratante_NomeFantasia = P00EB3_A10Contratante_NomeFantasia[0];
            A1192Contratante_FimDoExpediente = P00EB3_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB3_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB3_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB3_n1448Contratante_InicioDoExpediente[0];
            A23Estado_UF = P00EB3_A23Estado_UF[0];
            A26Municipio_Nome = P00EB3_A26Municipio_Nome[0];
            A32Contratante_Ramal = P00EB3_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB3_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB3_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00EB3_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB3_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB3_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB3_n9Contratante_RazaoSocial[0];
            A25Municipio_Codigo = P00EB3_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB3_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB3_A335Contratante_PessoaCod[0];
            A10Contratante_NomeFantasia = P00EB3_A10Contratante_NomeFantasia[0];
            A1192Contratante_FimDoExpediente = P00EB3_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB3_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB3_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB3_n1448Contratante_InicioDoExpediente[0];
            A32Contratante_Ramal = P00EB3_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB3_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB3_A31Contratante_Telefone[0];
            A23Estado_UF = P00EB3_A23Estado_UF[0];
            A26Municipio_Nome = P00EB3_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00EB3_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB3_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB3_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB3_n9Contratante_RazaoSocial[0];
            AV40count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00EB3_A10Contratante_NomeFantasia[0], A10Contratante_NomeFantasia) == 0 ) )
            {
               BRKEB4 = false;
               A29Contratante_Codigo = P00EB3_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB3_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00EB3_A5AreaTrabalho_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKEB4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A10Contratante_NomeFantasia)) )
            {
               AV32Option = A10Contratante_NomeFantasia;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB4 )
            {
               BRKEB4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATANTE_CNPJOPTIONS' Routine */
         AV14TFContratante_CNPJ = AV28SearchTxt;
         AV15TFContratante_CNPJ_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB4 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKEB6 = false;
            A29Contratante_Codigo = P00EB4_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB4_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00EB4_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB4_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB4_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00EB4_A5AreaTrabalho_Codigo[0];
            A12Contratante_CNPJ = P00EB4_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB4_n12Contratante_CNPJ[0];
            A1192Contratante_FimDoExpediente = P00EB4_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB4_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB4_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB4_n1448Contratante_InicioDoExpediente[0];
            A23Estado_UF = P00EB4_A23Estado_UF[0];
            A26Municipio_Nome = P00EB4_A26Municipio_Nome[0];
            A32Contratante_Ramal = P00EB4_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB4_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB4_A31Contratante_Telefone[0];
            A10Contratante_NomeFantasia = P00EB4_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00EB4_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB4_n9Contratante_RazaoSocial[0];
            A25Municipio_Codigo = P00EB4_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB4_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB4_A335Contratante_PessoaCod[0];
            A1192Contratante_FimDoExpediente = P00EB4_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB4_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB4_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB4_n1448Contratante_InicioDoExpediente[0];
            A32Contratante_Ramal = P00EB4_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB4_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB4_A31Contratante_Telefone[0];
            A10Contratante_NomeFantasia = P00EB4_A10Contratante_NomeFantasia[0];
            A23Estado_UF = P00EB4_A23Estado_UF[0];
            A26Municipio_Nome = P00EB4_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00EB4_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB4_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB4_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB4_n9Contratante_RazaoSocial[0];
            AV40count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00EB4_A12Contratante_CNPJ[0], A12Contratante_CNPJ) == 0 ) )
            {
               BRKEB6 = false;
               A29Contratante_Codigo = P00EB4_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB4_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = P00EB4_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00EB4_A5AreaTrabalho_Codigo[0];
               A335Contratante_PessoaCod = P00EB4_A335Contratante_PessoaCod[0];
               AV40count = (long)(AV40count+1);
               BRKEB6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A12Contratante_CNPJ)) )
            {
               AV32Option = A12Contratante_CNPJ;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB6 )
            {
               BRKEB6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATANTE_TELEFONEOPTIONS' Routine */
         AV16TFContratante_Telefone = AV28SearchTxt;
         AV17TFContratante_Telefone_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB5 */
         pr_default.execute(3, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKEB8 = false;
            A29Contratante_Codigo = P00EB5_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB5_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00EB5_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB5_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB5_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00EB5_A5AreaTrabalho_Codigo[0];
            A31Contratante_Telefone = P00EB5_A31Contratante_Telefone[0];
            A1192Contratante_FimDoExpediente = P00EB5_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB5_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB5_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB5_n1448Contratante_InicioDoExpediente[0];
            A23Estado_UF = P00EB5_A23Estado_UF[0];
            A26Municipio_Nome = P00EB5_A26Municipio_Nome[0];
            A32Contratante_Ramal = P00EB5_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB5_n32Contratante_Ramal[0];
            A12Contratante_CNPJ = P00EB5_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB5_n12Contratante_CNPJ[0];
            A10Contratante_NomeFantasia = P00EB5_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00EB5_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB5_n9Contratante_RazaoSocial[0];
            A25Municipio_Codigo = P00EB5_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB5_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB5_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = P00EB5_A31Contratante_Telefone[0];
            A1192Contratante_FimDoExpediente = P00EB5_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB5_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB5_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB5_n1448Contratante_InicioDoExpediente[0];
            A32Contratante_Ramal = P00EB5_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB5_n32Contratante_Ramal[0];
            A10Contratante_NomeFantasia = P00EB5_A10Contratante_NomeFantasia[0];
            A23Estado_UF = P00EB5_A23Estado_UF[0];
            A26Municipio_Nome = P00EB5_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00EB5_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB5_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB5_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB5_n9Contratante_RazaoSocial[0];
            AV40count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00EB5_A31Contratante_Telefone[0], A31Contratante_Telefone) == 0 ) )
            {
               BRKEB8 = false;
               A29Contratante_Codigo = P00EB5_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB5_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00EB5_A5AreaTrabalho_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKEB8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A31Contratante_Telefone)) )
            {
               AV32Option = A31Contratante_Telefone;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB8 )
            {
               BRKEB8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATANTE_RAMALOPTIONS' Routine */
         AV18TFContratante_Ramal = AV28SearchTxt;
         AV19TFContratante_Ramal_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB6 */
         pr_default.execute(4, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKEB10 = false;
            A29Contratante_Codigo = P00EB6_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB6_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00EB6_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB6_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB6_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00EB6_A5AreaTrabalho_Codigo[0];
            A32Contratante_Ramal = P00EB6_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB6_n32Contratante_Ramal[0];
            A1192Contratante_FimDoExpediente = P00EB6_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB6_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB6_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB6_n1448Contratante_InicioDoExpediente[0];
            A23Estado_UF = P00EB6_A23Estado_UF[0];
            A26Municipio_Nome = P00EB6_A26Municipio_Nome[0];
            A31Contratante_Telefone = P00EB6_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00EB6_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB6_n12Contratante_CNPJ[0];
            A10Contratante_NomeFantasia = P00EB6_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00EB6_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB6_n9Contratante_RazaoSocial[0];
            A25Municipio_Codigo = P00EB6_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB6_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB6_A335Contratante_PessoaCod[0];
            A32Contratante_Ramal = P00EB6_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB6_n32Contratante_Ramal[0];
            A1192Contratante_FimDoExpediente = P00EB6_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB6_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB6_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB6_n1448Contratante_InicioDoExpediente[0];
            A31Contratante_Telefone = P00EB6_A31Contratante_Telefone[0];
            A10Contratante_NomeFantasia = P00EB6_A10Contratante_NomeFantasia[0];
            A23Estado_UF = P00EB6_A23Estado_UF[0];
            A26Municipio_Nome = P00EB6_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00EB6_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB6_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB6_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB6_n9Contratante_RazaoSocial[0];
            AV40count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00EB6_A32Contratante_Ramal[0], A32Contratante_Ramal) == 0 ) )
            {
               BRKEB10 = false;
               A29Contratante_Codigo = P00EB6_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB6_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00EB6_A5AreaTrabalho_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKEB10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A32Contratante_Ramal)) )
            {
               AV32Option = A32Contratante_Ramal;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB10 )
            {
               BRKEB10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADMUNICIPIO_NOMEOPTIONS' Routine */
         AV20TFMunicipio_Nome = AV28SearchTxt;
         AV21TFMunicipio_Nome_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB7 */
         pr_default.execute(5, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKEB12 = false;
            A29Contratante_Codigo = P00EB7_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB7_n29Contratante_Codigo[0];
            A335Contratante_PessoaCod = P00EB7_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00EB7_A5AreaTrabalho_Codigo[0];
            A25Municipio_Codigo = P00EB7_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB7_n25Municipio_Codigo[0];
            A1192Contratante_FimDoExpediente = P00EB7_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB7_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB7_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB7_n1448Contratante_InicioDoExpediente[0];
            A23Estado_UF = P00EB7_A23Estado_UF[0];
            A26Municipio_Nome = P00EB7_A26Municipio_Nome[0];
            A32Contratante_Ramal = P00EB7_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB7_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB7_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00EB7_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB7_n12Contratante_CNPJ[0];
            A10Contratante_NomeFantasia = P00EB7_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00EB7_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB7_n9Contratante_RazaoSocial[0];
            A335Contratante_PessoaCod = P00EB7_A335Contratante_PessoaCod[0];
            A25Municipio_Codigo = P00EB7_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB7_n25Municipio_Codigo[0];
            A1192Contratante_FimDoExpediente = P00EB7_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB7_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB7_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB7_n1448Contratante_InicioDoExpediente[0];
            A32Contratante_Ramal = P00EB7_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB7_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB7_A31Contratante_Telefone[0];
            A10Contratante_NomeFantasia = P00EB7_A10Contratante_NomeFantasia[0];
            A12Contratante_CNPJ = P00EB7_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB7_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB7_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB7_n9Contratante_RazaoSocial[0];
            A23Estado_UF = P00EB7_A23Estado_UF[0];
            A26Municipio_Nome = P00EB7_A26Municipio_Nome[0];
            AV40count = 0;
            while ( (pr_default.getStatus(5) != 101) && ( P00EB7_A25Municipio_Codigo[0] == A25Municipio_Codigo ) )
            {
               BRKEB12 = false;
               A29Contratante_Codigo = P00EB7_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB7_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00EB7_A5AreaTrabalho_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKEB12 = true;
               pr_default.readNext(5);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A26Municipio_Nome)) )
            {
               AV32Option = A26Municipio_Nome;
               AV31InsertIndex = 1;
               while ( ( AV31InsertIndex <= AV33Options.Count ) && ( StringUtil.StrCmp(((String)AV33Options.Item(AV31InsertIndex)), AV32Option) < 0 ) )
               {
                  AV31InsertIndex = (int)(AV31InsertIndex+1);
               }
               AV33Options.Add(AV32Option, AV31InsertIndex);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), AV31InsertIndex);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB12 )
            {
               BRKEB12 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      protected void S181( )
      {
         /* 'LOADESTADO_UFOPTIONS' Routine */
         AV22TFEstado_UF = AV28SearchTxt;
         AV23TFEstado_UF_Sel = "";
         AV60WWContratanteDS_1_Areatrabalho_codigo = AV46AreaTrabalho_Codigo;
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = AV10TFContratante_RazaoSocial;
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV11TFContratante_RazaoSocial_Sel;
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = AV12TFContratante_NomeFantasia;
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV13TFContratante_NomeFantasia_Sel;
         AV65WWContratanteDS_6_Tfcontratante_cnpj = AV14TFContratante_CNPJ;
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = AV15TFContratante_CNPJ_Sel;
         AV67WWContratanteDS_8_Tfcontratante_telefone = AV16TFContratante_Telefone;
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = AV17TFContratante_Telefone_Sel;
         AV69WWContratanteDS_10_Tfcontratante_ramal = AV18TFContratante_Ramal;
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = AV19TFContratante_Ramal_Sel;
         AV71WWContratanteDS_12_Tfmunicipio_nome = AV20TFMunicipio_Nome;
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = AV21TFMunicipio_Nome_Sel;
         AV73WWContratanteDS_14_Tfestado_uf = AV22TFEstado_UF;
         AV74WWContratanteDS_15_Tfestado_uf_sel = AV23TFEstado_UF_Sel;
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV24TFContratante_InicioDoExpediente;
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV25TFContratante_InicioDoExpediente_To;
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV26TFContratante_FimDoExpediente;
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV27TFContratante_FimDoExpediente_To;
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV73WWContratanteDS_14_Tfestado_uf ,
                                              AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              A5AreaTrabalho_Codigo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV65WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV67WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV69WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV71WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV73WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor P00EB8 */
         pr_default.execute(6, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV61WWContratanteDS_2_Tfcontratante_razaosocial, AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV63WWContratanteDS_4_Tfcontratante_nomefantasia, AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV65WWContratanteDS_6_Tfcontratante_cnpj, AV66WWContratanteDS_7_Tfcontratante_cnpj_sel, lV67WWContratanteDS_8_Tfcontratante_telefone, AV68WWContratanteDS_9_Tfcontratante_telefone_sel, lV69WWContratanteDS_10_Tfcontratante_ramal, AV70WWContratanteDS_11_Tfcontratante_ramal_sel, lV71WWContratanteDS_12_Tfmunicipio_nome, AV72WWContratanteDS_13_Tfmunicipio_nome_sel, lV73WWContratanteDS_14_Tfestado_uf, AV74WWContratanteDS_15_Tfestado_uf_sel, AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         while ( (pr_default.getStatus(6) != 101) )
         {
            BRKEB14 = false;
            A29Contratante_Codigo = P00EB8_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00EB8_n29Contratante_Codigo[0];
            A25Municipio_Codigo = P00EB8_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB8_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB8_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00EB8_A5AreaTrabalho_Codigo[0];
            A23Estado_UF = P00EB8_A23Estado_UF[0];
            A1192Contratante_FimDoExpediente = P00EB8_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB8_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB8_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB8_n1448Contratante_InicioDoExpediente[0];
            A26Municipio_Nome = P00EB8_A26Municipio_Nome[0];
            A32Contratante_Ramal = P00EB8_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB8_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB8_A31Contratante_Telefone[0];
            A12Contratante_CNPJ = P00EB8_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB8_n12Contratante_CNPJ[0];
            A10Contratante_NomeFantasia = P00EB8_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = P00EB8_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB8_n9Contratante_RazaoSocial[0];
            A25Municipio_Codigo = P00EB8_A25Municipio_Codigo[0];
            n25Municipio_Codigo = P00EB8_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = P00EB8_A335Contratante_PessoaCod[0];
            A1192Contratante_FimDoExpediente = P00EB8_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00EB8_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00EB8_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00EB8_n1448Contratante_InicioDoExpediente[0];
            A32Contratante_Ramal = P00EB8_A32Contratante_Ramal[0];
            n32Contratante_Ramal = P00EB8_n32Contratante_Ramal[0];
            A31Contratante_Telefone = P00EB8_A31Contratante_Telefone[0];
            A10Contratante_NomeFantasia = P00EB8_A10Contratante_NomeFantasia[0];
            A23Estado_UF = P00EB8_A23Estado_UF[0];
            A26Municipio_Nome = P00EB8_A26Municipio_Nome[0];
            A12Contratante_CNPJ = P00EB8_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = P00EB8_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = P00EB8_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00EB8_n9Contratante_RazaoSocial[0];
            AV40count = 0;
            while ( (pr_default.getStatus(6) != 101) && ( StringUtil.StrCmp(P00EB8_A23Estado_UF[0], A23Estado_UF) == 0 ) )
            {
               BRKEB14 = false;
               A29Contratante_Codigo = P00EB8_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00EB8_n29Contratante_Codigo[0];
               A25Municipio_Codigo = P00EB8_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00EB8_n25Municipio_Codigo[0];
               A5AreaTrabalho_Codigo = P00EB8_A5AreaTrabalho_Codigo[0];
               A25Municipio_Codigo = P00EB8_A25Municipio_Codigo[0];
               n25Municipio_Codigo = P00EB8_n25Municipio_Codigo[0];
               AV40count = (long)(AV40count+1);
               BRKEB14 = true;
               pr_default.readNext(6);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
            {
               AV32Option = A23Estado_UF;
               AV33Options.Add(AV32Option, 0);
               AV38OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV40count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV33Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEB14 )
            {
               BRKEB14 = true;
               pr_default.readNext(6);
            }
         }
         pr_default.close(6);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV33Options = new GxSimpleCollection();
         AV36OptionsDesc = new GxSimpleCollection();
         AV38OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV41Session = context.GetSession();
         AV43GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV44GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratante_RazaoSocial = "";
         AV11TFContratante_RazaoSocial_Sel = "";
         AV12TFContratante_NomeFantasia = "";
         AV13TFContratante_NomeFantasia_Sel = "";
         AV14TFContratante_CNPJ = "";
         AV15TFContratante_CNPJ_Sel = "";
         AV16TFContratante_Telefone = "";
         AV17TFContratante_Telefone_Sel = "";
         AV18TFContratante_Ramal = "";
         AV19TFContratante_Ramal_Sel = "";
         AV20TFMunicipio_Nome = "";
         AV21TFMunicipio_Nome_Sel = "";
         AV22TFEstado_UF = "";
         AV23TFEstado_UF_Sel = "";
         AV24TFContratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         AV25TFContratante_InicioDoExpediente_To = (DateTime)(DateTime.MinValue);
         AV26TFContratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         AV27TFContratante_FimDoExpediente_To = (DateTime)(DateTime.MinValue);
         AV61WWContratanteDS_2_Tfcontratante_razaosocial = "";
         AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel = "";
         AV63WWContratanteDS_4_Tfcontratante_nomefantasia = "";
         AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel = "";
         AV65WWContratanteDS_6_Tfcontratante_cnpj = "";
         AV66WWContratanteDS_7_Tfcontratante_cnpj_sel = "";
         AV67WWContratanteDS_8_Tfcontratante_telefone = "";
         AV68WWContratanteDS_9_Tfcontratante_telefone_sel = "";
         AV69WWContratanteDS_10_Tfcontratante_ramal = "";
         AV70WWContratanteDS_11_Tfcontratante_ramal_sel = "";
         AV71WWContratanteDS_12_Tfmunicipio_nome = "";
         AV72WWContratanteDS_13_Tfmunicipio_nome_sel = "";
         AV73WWContratanteDS_14_Tfestado_uf = "";
         AV74WWContratanteDS_15_Tfestado_uf_sel = "";
         AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente = (DateTime)(DateTime.MinValue);
         AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = (DateTime)(DateTime.MinValue);
         AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente = (DateTime)(DateTime.MinValue);
         AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV61WWContratanteDS_2_Tfcontratante_razaosocial = "";
         lV63WWContratanteDS_4_Tfcontratante_nomefantasia = "";
         lV65WWContratanteDS_6_Tfcontratante_cnpj = "";
         lV67WWContratanteDS_8_Tfcontratante_telefone = "";
         lV69WWContratanteDS_10_Tfcontratante_ramal = "";
         lV71WWContratanteDS_12_Tfmunicipio_nome = "";
         lV73WWContratanteDS_14_Tfestado_uf = "";
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A12Contratante_CNPJ = "";
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         A26Municipio_Nome = "";
         A23Estado_UF = "";
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         P00EB2_A29Contratante_Codigo = new int[1] ;
         P00EB2_n29Contratante_Codigo = new bool[] {false} ;
         P00EB2_A25Municipio_Codigo = new int[1] ;
         P00EB2_n25Municipio_Codigo = new bool[] {false} ;
         P00EB2_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB2_A335Contratante_PessoaCod = new int[1] ;
         P00EB2_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB2_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB2_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB2_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB2_A23Estado_UF = new String[] {""} ;
         P00EB2_A26Municipio_Nome = new String[] {""} ;
         P00EB2_A32Contratante_Ramal = new String[] {""} ;
         P00EB2_n32Contratante_Ramal = new bool[] {false} ;
         P00EB2_A31Contratante_Telefone = new String[] {""} ;
         P00EB2_A12Contratante_CNPJ = new String[] {""} ;
         P00EB2_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB2_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB2_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB2_n9Contratante_RazaoSocial = new bool[] {false} ;
         AV32Option = "";
         P00EB3_A29Contratante_Codigo = new int[1] ;
         P00EB3_n29Contratante_Codigo = new bool[] {false} ;
         P00EB3_A25Municipio_Codigo = new int[1] ;
         P00EB3_n25Municipio_Codigo = new bool[] {false} ;
         P00EB3_A335Contratante_PessoaCod = new int[1] ;
         P00EB3_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB3_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB3_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB3_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB3_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB3_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB3_A23Estado_UF = new String[] {""} ;
         P00EB3_A26Municipio_Nome = new String[] {""} ;
         P00EB3_A32Contratante_Ramal = new String[] {""} ;
         P00EB3_n32Contratante_Ramal = new bool[] {false} ;
         P00EB3_A31Contratante_Telefone = new String[] {""} ;
         P00EB3_A12Contratante_CNPJ = new String[] {""} ;
         P00EB3_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB3_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB3_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00EB4_A29Contratante_Codigo = new int[1] ;
         P00EB4_n29Contratante_Codigo = new bool[] {false} ;
         P00EB4_A25Municipio_Codigo = new int[1] ;
         P00EB4_n25Municipio_Codigo = new bool[] {false} ;
         P00EB4_A335Contratante_PessoaCod = new int[1] ;
         P00EB4_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB4_A12Contratante_CNPJ = new String[] {""} ;
         P00EB4_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB4_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB4_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB4_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB4_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB4_A23Estado_UF = new String[] {""} ;
         P00EB4_A26Municipio_Nome = new String[] {""} ;
         P00EB4_A32Contratante_Ramal = new String[] {""} ;
         P00EB4_n32Contratante_Ramal = new bool[] {false} ;
         P00EB4_A31Contratante_Telefone = new String[] {""} ;
         P00EB4_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB4_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB4_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00EB5_A29Contratante_Codigo = new int[1] ;
         P00EB5_n29Contratante_Codigo = new bool[] {false} ;
         P00EB5_A25Municipio_Codigo = new int[1] ;
         P00EB5_n25Municipio_Codigo = new bool[] {false} ;
         P00EB5_A335Contratante_PessoaCod = new int[1] ;
         P00EB5_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB5_A31Contratante_Telefone = new String[] {""} ;
         P00EB5_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB5_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB5_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB5_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB5_A23Estado_UF = new String[] {""} ;
         P00EB5_A26Municipio_Nome = new String[] {""} ;
         P00EB5_A32Contratante_Ramal = new String[] {""} ;
         P00EB5_n32Contratante_Ramal = new bool[] {false} ;
         P00EB5_A12Contratante_CNPJ = new String[] {""} ;
         P00EB5_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB5_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB5_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB5_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00EB6_A29Contratante_Codigo = new int[1] ;
         P00EB6_n29Contratante_Codigo = new bool[] {false} ;
         P00EB6_A25Municipio_Codigo = new int[1] ;
         P00EB6_n25Municipio_Codigo = new bool[] {false} ;
         P00EB6_A335Contratante_PessoaCod = new int[1] ;
         P00EB6_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB6_A32Contratante_Ramal = new String[] {""} ;
         P00EB6_n32Contratante_Ramal = new bool[] {false} ;
         P00EB6_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB6_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB6_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB6_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB6_A23Estado_UF = new String[] {""} ;
         P00EB6_A26Municipio_Nome = new String[] {""} ;
         P00EB6_A31Contratante_Telefone = new String[] {""} ;
         P00EB6_A12Contratante_CNPJ = new String[] {""} ;
         P00EB6_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB6_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB6_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB6_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00EB7_A29Contratante_Codigo = new int[1] ;
         P00EB7_n29Contratante_Codigo = new bool[] {false} ;
         P00EB7_A335Contratante_PessoaCod = new int[1] ;
         P00EB7_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB7_A25Municipio_Codigo = new int[1] ;
         P00EB7_n25Municipio_Codigo = new bool[] {false} ;
         P00EB7_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB7_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB7_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB7_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB7_A23Estado_UF = new String[] {""} ;
         P00EB7_A26Municipio_Nome = new String[] {""} ;
         P00EB7_A32Contratante_Ramal = new String[] {""} ;
         P00EB7_n32Contratante_Ramal = new bool[] {false} ;
         P00EB7_A31Contratante_Telefone = new String[] {""} ;
         P00EB7_A12Contratante_CNPJ = new String[] {""} ;
         P00EB7_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB7_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB7_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB7_n9Contratante_RazaoSocial = new bool[] {false} ;
         P00EB8_A29Contratante_Codigo = new int[1] ;
         P00EB8_n29Contratante_Codigo = new bool[] {false} ;
         P00EB8_A25Municipio_Codigo = new int[1] ;
         P00EB8_n25Municipio_Codigo = new bool[] {false} ;
         P00EB8_A335Contratante_PessoaCod = new int[1] ;
         P00EB8_A5AreaTrabalho_Codigo = new int[1] ;
         P00EB8_A23Estado_UF = new String[] {""} ;
         P00EB8_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB8_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         P00EB8_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00EB8_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00EB8_A26Municipio_Nome = new String[] {""} ;
         P00EB8_A32Contratante_Ramal = new String[] {""} ;
         P00EB8_n32Contratante_Ramal = new bool[] {false} ;
         P00EB8_A31Contratante_Telefone = new String[] {""} ;
         P00EB8_A12Contratante_CNPJ = new String[] {""} ;
         P00EB8_n12Contratante_CNPJ = new bool[] {false} ;
         P00EB8_A10Contratante_NomeFantasia = new String[] {""} ;
         P00EB8_A9Contratante_RazaoSocial = new String[] {""} ;
         P00EB8_n9Contratante_RazaoSocial = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratantefilterdata__default(),
            new Object[][] {
                new Object[] {
               P00EB2_A29Contratante_Codigo, P00EB2_n29Contratante_Codigo, P00EB2_A25Municipio_Codigo, P00EB2_n25Municipio_Codigo, P00EB2_A5AreaTrabalho_Codigo, P00EB2_A335Contratante_PessoaCod, P00EB2_A1192Contratante_FimDoExpediente, P00EB2_n1192Contratante_FimDoExpediente, P00EB2_A1448Contratante_InicioDoExpediente, P00EB2_n1448Contratante_InicioDoExpediente,
               P00EB2_A23Estado_UF, P00EB2_A26Municipio_Nome, P00EB2_A32Contratante_Ramal, P00EB2_n32Contratante_Ramal, P00EB2_A31Contratante_Telefone, P00EB2_A12Contratante_CNPJ, P00EB2_n12Contratante_CNPJ, P00EB2_A10Contratante_NomeFantasia, P00EB2_A9Contratante_RazaoSocial, P00EB2_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00EB3_A29Contratante_Codigo, P00EB3_n29Contratante_Codigo, P00EB3_A25Municipio_Codigo, P00EB3_n25Municipio_Codigo, P00EB3_A335Contratante_PessoaCod, P00EB3_A5AreaTrabalho_Codigo, P00EB3_A10Contratante_NomeFantasia, P00EB3_A1192Contratante_FimDoExpediente, P00EB3_n1192Contratante_FimDoExpediente, P00EB3_A1448Contratante_InicioDoExpediente,
               P00EB3_n1448Contratante_InicioDoExpediente, P00EB3_A23Estado_UF, P00EB3_A26Municipio_Nome, P00EB3_A32Contratante_Ramal, P00EB3_n32Contratante_Ramal, P00EB3_A31Contratante_Telefone, P00EB3_A12Contratante_CNPJ, P00EB3_n12Contratante_CNPJ, P00EB3_A9Contratante_RazaoSocial, P00EB3_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00EB4_A29Contratante_Codigo, P00EB4_n29Contratante_Codigo, P00EB4_A25Municipio_Codigo, P00EB4_n25Municipio_Codigo, P00EB4_A335Contratante_PessoaCod, P00EB4_A5AreaTrabalho_Codigo, P00EB4_A12Contratante_CNPJ, P00EB4_n12Contratante_CNPJ, P00EB4_A1192Contratante_FimDoExpediente, P00EB4_n1192Contratante_FimDoExpediente,
               P00EB4_A1448Contratante_InicioDoExpediente, P00EB4_n1448Contratante_InicioDoExpediente, P00EB4_A23Estado_UF, P00EB4_A26Municipio_Nome, P00EB4_A32Contratante_Ramal, P00EB4_n32Contratante_Ramal, P00EB4_A31Contratante_Telefone, P00EB4_A10Contratante_NomeFantasia, P00EB4_A9Contratante_RazaoSocial, P00EB4_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00EB5_A29Contratante_Codigo, P00EB5_n29Contratante_Codigo, P00EB5_A25Municipio_Codigo, P00EB5_n25Municipio_Codigo, P00EB5_A335Contratante_PessoaCod, P00EB5_A5AreaTrabalho_Codigo, P00EB5_A31Contratante_Telefone, P00EB5_A1192Contratante_FimDoExpediente, P00EB5_n1192Contratante_FimDoExpediente, P00EB5_A1448Contratante_InicioDoExpediente,
               P00EB5_n1448Contratante_InicioDoExpediente, P00EB5_A23Estado_UF, P00EB5_A26Municipio_Nome, P00EB5_A32Contratante_Ramal, P00EB5_n32Contratante_Ramal, P00EB5_A12Contratante_CNPJ, P00EB5_n12Contratante_CNPJ, P00EB5_A10Contratante_NomeFantasia, P00EB5_A9Contratante_RazaoSocial, P00EB5_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00EB6_A29Contratante_Codigo, P00EB6_n29Contratante_Codigo, P00EB6_A25Municipio_Codigo, P00EB6_n25Municipio_Codigo, P00EB6_A335Contratante_PessoaCod, P00EB6_A5AreaTrabalho_Codigo, P00EB6_A32Contratante_Ramal, P00EB6_n32Contratante_Ramal, P00EB6_A1192Contratante_FimDoExpediente, P00EB6_n1192Contratante_FimDoExpediente,
               P00EB6_A1448Contratante_InicioDoExpediente, P00EB6_n1448Contratante_InicioDoExpediente, P00EB6_A23Estado_UF, P00EB6_A26Municipio_Nome, P00EB6_A31Contratante_Telefone, P00EB6_A12Contratante_CNPJ, P00EB6_n12Contratante_CNPJ, P00EB6_A10Contratante_NomeFantasia, P00EB6_A9Contratante_RazaoSocial, P00EB6_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00EB7_A29Contratante_Codigo, P00EB7_n29Contratante_Codigo, P00EB7_A335Contratante_PessoaCod, P00EB7_A5AreaTrabalho_Codigo, P00EB7_A25Municipio_Codigo, P00EB7_n25Municipio_Codigo, P00EB7_A1192Contratante_FimDoExpediente, P00EB7_n1192Contratante_FimDoExpediente, P00EB7_A1448Contratante_InicioDoExpediente, P00EB7_n1448Contratante_InicioDoExpediente,
               P00EB7_A23Estado_UF, P00EB7_A26Municipio_Nome, P00EB7_A32Contratante_Ramal, P00EB7_n32Contratante_Ramal, P00EB7_A31Contratante_Telefone, P00EB7_A12Contratante_CNPJ, P00EB7_n12Contratante_CNPJ, P00EB7_A10Contratante_NomeFantasia, P00EB7_A9Contratante_RazaoSocial, P00EB7_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00EB8_A29Contratante_Codigo, P00EB8_n29Contratante_Codigo, P00EB8_A25Municipio_Codigo, P00EB8_n25Municipio_Codigo, P00EB8_A335Contratante_PessoaCod, P00EB8_A5AreaTrabalho_Codigo, P00EB8_A23Estado_UF, P00EB8_A1192Contratante_FimDoExpediente, P00EB8_n1192Contratante_FimDoExpediente, P00EB8_A1448Contratante_InicioDoExpediente,
               P00EB8_n1448Contratante_InicioDoExpediente, P00EB8_A26Municipio_Nome, P00EB8_A32Contratante_Ramal, P00EB8_n32Contratante_Ramal, P00EB8_A31Contratante_Telefone, P00EB8_A12Contratante_CNPJ, P00EB8_n12Contratante_CNPJ, P00EB8_A10Contratante_NomeFantasia, P00EB8_A9Contratante_RazaoSocial, P00EB8_n9Contratante_RazaoSocial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV58GXV1 ;
      private int AV46AreaTrabalho_Codigo ;
      private int AV60WWContratanteDS_1_Areatrabalho_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int AV31InsertIndex ;
      private long AV40count ;
      private String AV10TFContratante_RazaoSocial ;
      private String AV11TFContratante_RazaoSocial_Sel ;
      private String AV12TFContratante_NomeFantasia ;
      private String AV13TFContratante_NomeFantasia_Sel ;
      private String AV16TFContratante_Telefone ;
      private String AV17TFContratante_Telefone_Sel ;
      private String AV18TFContratante_Ramal ;
      private String AV19TFContratante_Ramal_Sel ;
      private String AV20TFMunicipio_Nome ;
      private String AV21TFMunicipio_Nome_Sel ;
      private String AV22TFEstado_UF ;
      private String AV23TFEstado_UF_Sel ;
      private String AV61WWContratanteDS_2_Tfcontratante_razaosocial ;
      private String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ;
      private String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ;
      private String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ;
      private String AV67WWContratanteDS_8_Tfcontratante_telefone ;
      private String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ;
      private String AV69WWContratanteDS_10_Tfcontratante_ramal ;
      private String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ;
      private String AV71WWContratanteDS_12_Tfmunicipio_nome ;
      private String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ;
      private String AV73WWContratanteDS_14_Tfestado_uf ;
      private String AV74WWContratanteDS_15_Tfestado_uf_sel ;
      private String scmdbuf ;
      private String lV61WWContratanteDS_2_Tfcontratante_razaosocial ;
      private String lV63WWContratanteDS_4_Tfcontratante_nomefantasia ;
      private String lV67WWContratanteDS_8_Tfcontratante_telefone ;
      private String lV69WWContratanteDS_10_Tfcontratante_ramal ;
      private String lV71WWContratanteDS_12_Tfmunicipio_nome ;
      private String lV73WWContratanteDS_14_Tfestado_uf ;
      private String A9Contratante_RazaoSocial ;
      private String A10Contratante_NomeFantasia ;
      private String A31Contratante_Telefone ;
      private String A32Contratante_Ramal ;
      private String A26Municipio_Nome ;
      private String A23Estado_UF ;
      private DateTime AV24TFContratante_InicioDoExpediente ;
      private DateTime AV25TFContratante_InicioDoExpediente_To ;
      private DateTime AV26TFContratante_FimDoExpediente ;
      private DateTime AV27TFContratante_FimDoExpediente_To ;
      private DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ;
      private DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ;
      private DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ;
      private DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private bool returnInSub ;
      private bool BRKEB2 ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n32Contratante_Ramal ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool BRKEB4 ;
      private bool BRKEB6 ;
      private bool BRKEB8 ;
      private bool BRKEB10 ;
      private bool BRKEB12 ;
      private bool BRKEB14 ;
      private String AV39OptionIndexesJson ;
      private String AV34OptionsJson ;
      private String AV37OptionsDescJson ;
      private String AV30DDOName ;
      private String AV28SearchTxt ;
      private String AV29SearchTxtTo ;
      private String AV14TFContratante_CNPJ ;
      private String AV15TFContratante_CNPJ_Sel ;
      private String AV65WWContratanteDS_6_Tfcontratante_cnpj ;
      private String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ;
      private String lV65WWContratanteDS_6_Tfcontratante_cnpj ;
      private String A12Contratante_CNPJ ;
      private String AV32Option ;
      private IGxSession AV41Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00EB2_A29Contratante_Codigo ;
      private bool[] P00EB2_n29Contratante_Codigo ;
      private int[] P00EB2_A25Municipio_Codigo ;
      private bool[] P00EB2_n25Municipio_Codigo ;
      private int[] P00EB2_A5AreaTrabalho_Codigo ;
      private int[] P00EB2_A335Contratante_PessoaCod ;
      private DateTime[] P00EB2_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB2_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB2_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB2_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB2_A23Estado_UF ;
      private String[] P00EB2_A26Municipio_Nome ;
      private String[] P00EB2_A32Contratante_Ramal ;
      private bool[] P00EB2_n32Contratante_Ramal ;
      private String[] P00EB2_A31Contratante_Telefone ;
      private String[] P00EB2_A12Contratante_CNPJ ;
      private bool[] P00EB2_n12Contratante_CNPJ ;
      private String[] P00EB2_A10Contratante_NomeFantasia ;
      private String[] P00EB2_A9Contratante_RazaoSocial ;
      private bool[] P00EB2_n9Contratante_RazaoSocial ;
      private int[] P00EB3_A29Contratante_Codigo ;
      private bool[] P00EB3_n29Contratante_Codigo ;
      private int[] P00EB3_A25Municipio_Codigo ;
      private bool[] P00EB3_n25Municipio_Codigo ;
      private int[] P00EB3_A335Contratante_PessoaCod ;
      private int[] P00EB3_A5AreaTrabalho_Codigo ;
      private String[] P00EB3_A10Contratante_NomeFantasia ;
      private DateTime[] P00EB3_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB3_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB3_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB3_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB3_A23Estado_UF ;
      private String[] P00EB3_A26Municipio_Nome ;
      private String[] P00EB3_A32Contratante_Ramal ;
      private bool[] P00EB3_n32Contratante_Ramal ;
      private String[] P00EB3_A31Contratante_Telefone ;
      private String[] P00EB3_A12Contratante_CNPJ ;
      private bool[] P00EB3_n12Contratante_CNPJ ;
      private String[] P00EB3_A9Contratante_RazaoSocial ;
      private bool[] P00EB3_n9Contratante_RazaoSocial ;
      private int[] P00EB4_A29Contratante_Codigo ;
      private bool[] P00EB4_n29Contratante_Codigo ;
      private int[] P00EB4_A25Municipio_Codigo ;
      private bool[] P00EB4_n25Municipio_Codigo ;
      private int[] P00EB4_A335Contratante_PessoaCod ;
      private int[] P00EB4_A5AreaTrabalho_Codigo ;
      private String[] P00EB4_A12Contratante_CNPJ ;
      private bool[] P00EB4_n12Contratante_CNPJ ;
      private DateTime[] P00EB4_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB4_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB4_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB4_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB4_A23Estado_UF ;
      private String[] P00EB4_A26Municipio_Nome ;
      private String[] P00EB4_A32Contratante_Ramal ;
      private bool[] P00EB4_n32Contratante_Ramal ;
      private String[] P00EB4_A31Contratante_Telefone ;
      private String[] P00EB4_A10Contratante_NomeFantasia ;
      private String[] P00EB4_A9Contratante_RazaoSocial ;
      private bool[] P00EB4_n9Contratante_RazaoSocial ;
      private int[] P00EB5_A29Contratante_Codigo ;
      private bool[] P00EB5_n29Contratante_Codigo ;
      private int[] P00EB5_A25Municipio_Codigo ;
      private bool[] P00EB5_n25Municipio_Codigo ;
      private int[] P00EB5_A335Contratante_PessoaCod ;
      private int[] P00EB5_A5AreaTrabalho_Codigo ;
      private String[] P00EB5_A31Contratante_Telefone ;
      private DateTime[] P00EB5_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB5_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB5_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB5_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB5_A23Estado_UF ;
      private String[] P00EB5_A26Municipio_Nome ;
      private String[] P00EB5_A32Contratante_Ramal ;
      private bool[] P00EB5_n32Contratante_Ramal ;
      private String[] P00EB5_A12Contratante_CNPJ ;
      private bool[] P00EB5_n12Contratante_CNPJ ;
      private String[] P00EB5_A10Contratante_NomeFantasia ;
      private String[] P00EB5_A9Contratante_RazaoSocial ;
      private bool[] P00EB5_n9Contratante_RazaoSocial ;
      private int[] P00EB6_A29Contratante_Codigo ;
      private bool[] P00EB6_n29Contratante_Codigo ;
      private int[] P00EB6_A25Municipio_Codigo ;
      private bool[] P00EB6_n25Municipio_Codigo ;
      private int[] P00EB6_A335Contratante_PessoaCod ;
      private int[] P00EB6_A5AreaTrabalho_Codigo ;
      private String[] P00EB6_A32Contratante_Ramal ;
      private bool[] P00EB6_n32Contratante_Ramal ;
      private DateTime[] P00EB6_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB6_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB6_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB6_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB6_A23Estado_UF ;
      private String[] P00EB6_A26Municipio_Nome ;
      private String[] P00EB6_A31Contratante_Telefone ;
      private String[] P00EB6_A12Contratante_CNPJ ;
      private bool[] P00EB6_n12Contratante_CNPJ ;
      private String[] P00EB6_A10Contratante_NomeFantasia ;
      private String[] P00EB6_A9Contratante_RazaoSocial ;
      private bool[] P00EB6_n9Contratante_RazaoSocial ;
      private int[] P00EB7_A29Contratante_Codigo ;
      private bool[] P00EB7_n29Contratante_Codigo ;
      private int[] P00EB7_A335Contratante_PessoaCod ;
      private int[] P00EB7_A5AreaTrabalho_Codigo ;
      private int[] P00EB7_A25Municipio_Codigo ;
      private bool[] P00EB7_n25Municipio_Codigo ;
      private DateTime[] P00EB7_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB7_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB7_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB7_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB7_A23Estado_UF ;
      private String[] P00EB7_A26Municipio_Nome ;
      private String[] P00EB7_A32Contratante_Ramal ;
      private bool[] P00EB7_n32Contratante_Ramal ;
      private String[] P00EB7_A31Contratante_Telefone ;
      private String[] P00EB7_A12Contratante_CNPJ ;
      private bool[] P00EB7_n12Contratante_CNPJ ;
      private String[] P00EB7_A10Contratante_NomeFantasia ;
      private String[] P00EB7_A9Contratante_RazaoSocial ;
      private bool[] P00EB7_n9Contratante_RazaoSocial ;
      private int[] P00EB8_A29Contratante_Codigo ;
      private bool[] P00EB8_n29Contratante_Codigo ;
      private int[] P00EB8_A25Municipio_Codigo ;
      private bool[] P00EB8_n25Municipio_Codigo ;
      private int[] P00EB8_A335Contratante_PessoaCod ;
      private int[] P00EB8_A5AreaTrabalho_Codigo ;
      private String[] P00EB8_A23Estado_UF ;
      private DateTime[] P00EB8_A1192Contratante_FimDoExpediente ;
      private bool[] P00EB8_n1192Contratante_FimDoExpediente ;
      private DateTime[] P00EB8_A1448Contratante_InicioDoExpediente ;
      private bool[] P00EB8_n1448Contratante_InicioDoExpediente ;
      private String[] P00EB8_A26Municipio_Nome ;
      private String[] P00EB8_A32Contratante_Ramal ;
      private bool[] P00EB8_n32Contratante_Ramal ;
      private String[] P00EB8_A31Contratante_Telefone ;
      private String[] P00EB8_A12Contratante_CNPJ ;
      private bool[] P00EB8_n12Contratante_CNPJ ;
      private String[] P00EB8_A10Contratante_NomeFantasia ;
      private String[] P00EB8_A9Contratante_RazaoSocial ;
      private bool[] P00EB8_n9Contratante_RazaoSocial ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV43GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV44GridStateFilterValue ;
   }

   public class getwwcontratantefilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00EB2( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [19] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratante_PessoaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00EB3( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [19] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T2.[Contratante_NomeFantasia], T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratante_NomeFantasia]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00EB4( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [19] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T4.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T2.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00EB5( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [19] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T2.[Contratante_Telefone], T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T2.[Contratante_Ramal], T4.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratante_Telefone]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00EB6( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [19] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T2.[Contratante_Ramal], T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int9[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratante_Ramal]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00EB7( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [19] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T2.[Municipio_Codigo], T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T4.[Estado_UF], T4.[Municipio_Nome], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T3.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T3.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = T2.[Municipio_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int11[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int11[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int11[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int11[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int11[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int11[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int11[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int11[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int11[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int11[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int11[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int11[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int11[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int11[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int11[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int11[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int11[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int11[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Municipio_Codigo]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      protected Object[] conditional_P00EB8( IGxContext context ,
                                             String AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV61WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV63WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV66WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV65WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV68WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV67WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV70WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV69WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV72WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV71WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV74WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV73WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int13 ;
         GXv_int13 = new short [19] ;
         Object[] GXv_Object14 ;
         GXv_Object14 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratante_Codigo], T2.[Municipio_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T3.[Estado_UF], T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Municipio_Nome], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV61WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int13[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int13[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV63WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int13[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int13[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV65WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int13[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV66WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int13[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV67WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int13[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV68WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int13[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV69WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int13[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV70WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int13[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV71WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int13[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV72WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int13[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV73WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int13[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV74WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int13[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int13[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int13[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int13[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int13[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Estado_UF]";
         GXv_Object14[0] = scmdbuf;
         GXv_Object14[1] = GXv_int13;
         return GXv_Object14 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00EB2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 1 :
                     return conditional_P00EB3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 2 :
                     return conditional_P00EB4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 3 :
                     return conditional_P00EB5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 4 :
                     return conditional_P00EB6(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 5 :
                     return conditional_P00EB7(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 6 :
                     return conditional_P00EB8(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00EB2 ;
          prmP00EB2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          Object[] prmP00EB3 ;
          prmP00EB3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          Object[] prmP00EB4 ;
          prmP00EB4 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          Object[] prmP00EB5 ;
          prmP00EB5 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          Object[] prmP00EB6 ;
          prmP00EB6 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          Object[] prmP00EB7 ;
          prmP00EB7 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          Object[] prmP00EB8 ;
          prmP00EB8 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV63WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV69WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV70WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV71WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV74WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV75WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV76WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV77WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV78WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00EB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB2,100,0,true,false )
             ,new CursorDef("P00EB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB3,100,0,true,false )
             ,new CursorDef("P00EB4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB4,100,0,true,false )
             ,new CursorDef("P00EB5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB5,100,0,true,false )
             ,new CursorDef("P00EB6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB6,100,0,true,false )
             ,new CursorDef("P00EB7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB7,100,0,true,false )
             ,new CursorDef("P00EB8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EB8,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 20) ;
                ((String[]) buf[16])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 20) ;
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 20) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratantefilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratantefilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratantefilterdata") )
          {
             return  ;
          }
          getwwcontratantefilterdata worker = new getwwcontratantefilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
