/*
               File: Menu_BC
        Description: Menu
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:22.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class menu_bc : GXHttpHandler, IGxSilentTrn
   {
      public menu_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public menu_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1B48( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1B48( ) ;
         standaloneModal( ) ;
         AddRow1B48( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E111B2 */
            E111B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z277Menu_Codigo = A277Menu_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1B0( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1B48( ) ;
            }
            else
            {
               CheckExtendedTable1B48( ) ;
               if ( AnyError == 0 )
               {
                  ZM1B48( 9) ;
               }
               CloseExtendedTableCursors1B48( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E121B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Menu_PaiCod") == 0 )
               {
                  AV11Insert_Menu_PaiCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
            }
         }
      }

      protected void E111B2( )
      {
         /* After Trn Routine */
      }

      protected void ZM1B48( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z278Menu_Nome = A278Menu_Nome;
            Z279Menu_Descricao = A279Menu_Descricao;
            Z280Menu_Tipo = A280Menu_Tipo;
            Z281Menu_Link = A281Menu_Link;
            Z283Menu_Ordem = A283Menu_Ordem;
            Z284Menu_Ativo = A284Menu_Ativo;
            Z285Menu_PaiCod = A285Menu_PaiCod;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z286Menu_PaiNom = A286Menu_PaiNom;
            Z287Menu_PaiTip = A287Menu_PaiTip;
            Z288Menu_PaiAti = A288Menu_PaiAti;
         }
         if ( GX_JID == -8 )
         {
            Z277Menu_Codigo = A277Menu_Codigo;
            Z278Menu_Nome = A278Menu_Nome;
            Z279Menu_Descricao = A279Menu_Descricao;
            Z280Menu_Tipo = A280Menu_Tipo;
            Z281Menu_Link = A281Menu_Link;
            Z282Menu_Imagem = A282Menu_Imagem;
            Z40000Menu_Imagem_GXI = A40000Menu_Imagem_GXI;
            Z283Menu_Ordem = A283Menu_Ordem;
            Z284Menu_Ativo = A284Menu_Ativo;
            Z285Menu_PaiCod = A285Menu_PaiCod;
            Z286Menu_PaiNom = A286Menu_PaiNom;
            Z287Menu_PaiTip = A287Menu_PaiTip;
            Z288Menu_PaiAti = A288Menu_PaiAti;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV13Pgmname = "Menu_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A284Menu_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A284Menu_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A283Menu_Ordem) && ( Gx_BScreen == 0 ) )
         {
            A283Menu_Ordem = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load1B48( )
      {
         /* Using cursor BC001B5 */
         pr_default.execute(3, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound48 = 1;
            A278Menu_Nome = BC001B5_A278Menu_Nome[0];
            A279Menu_Descricao = BC001B5_A279Menu_Descricao[0];
            n279Menu_Descricao = BC001B5_n279Menu_Descricao[0];
            A280Menu_Tipo = BC001B5_A280Menu_Tipo[0];
            A281Menu_Link = BC001B5_A281Menu_Link[0];
            n281Menu_Link = BC001B5_n281Menu_Link[0];
            A40000Menu_Imagem_GXI = BC001B5_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001B5_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = BC001B5_A283Menu_Ordem[0];
            A286Menu_PaiNom = BC001B5_A286Menu_PaiNom[0];
            n286Menu_PaiNom = BC001B5_n286Menu_PaiNom[0];
            A287Menu_PaiTip = BC001B5_A287Menu_PaiTip[0];
            n287Menu_PaiTip = BC001B5_n287Menu_PaiTip[0];
            A288Menu_PaiAti = BC001B5_A288Menu_PaiAti[0];
            n288Menu_PaiAti = BC001B5_n288Menu_PaiAti[0];
            A284Menu_Ativo = BC001B5_A284Menu_Ativo[0];
            A285Menu_PaiCod = BC001B5_A285Menu_PaiCod[0];
            n285Menu_PaiCod = BC001B5_n285Menu_PaiCod[0];
            A282Menu_Imagem = BC001B5_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001B5_n282Menu_Imagem[0];
            ZM1B48( -8) ;
         }
         pr_default.close(3);
         OnLoadActions1B48( ) ;
      }

      protected void OnLoadActions1B48( )
      {
      }

      protected void CheckExtendedTable1B48( )
      {
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A278Menu_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A280Menu_Tipo == 1 ) || ( A280Menu_Tipo == 2 ) ) )
         {
            GX_msglist.addItem("Campo Tipo(Superior ou Acesso R�pido) fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( (0==A283Menu_Ordem) )
         {
            GX_msglist.addItem("Ordem do Menu � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC001B4 */
         pr_default.execute(2, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A285Menu_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Menu_Pai'.", "ForeignKeyNotFound", 1, "MENU_PAICOD");
               AnyError = 1;
            }
         }
         A286Menu_PaiNom = BC001B4_A286Menu_PaiNom[0];
         n286Menu_PaiNom = BC001B4_n286Menu_PaiNom[0];
         A287Menu_PaiTip = BC001B4_A287Menu_PaiTip[0];
         n287Menu_PaiTip = BC001B4_n287Menu_PaiTip[0];
         A288Menu_PaiAti = BC001B4_A288Menu_PaiAti[0];
         n288Menu_PaiAti = BC001B4_n288Menu_PaiAti[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1B48( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1B48( )
      {
         /* Using cursor BC001B6 */
         pr_default.execute(4, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound48 = 1;
         }
         else
         {
            RcdFound48 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001B3 */
         pr_default.execute(1, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1B48( 8) ;
            RcdFound48 = 1;
            A277Menu_Codigo = BC001B3_A277Menu_Codigo[0];
            A278Menu_Nome = BC001B3_A278Menu_Nome[0];
            A279Menu_Descricao = BC001B3_A279Menu_Descricao[0];
            n279Menu_Descricao = BC001B3_n279Menu_Descricao[0];
            A280Menu_Tipo = BC001B3_A280Menu_Tipo[0];
            A281Menu_Link = BC001B3_A281Menu_Link[0];
            n281Menu_Link = BC001B3_n281Menu_Link[0];
            A40000Menu_Imagem_GXI = BC001B3_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001B3_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = BC001B3_A283Menu_Ordem[0];
            A284Menu_Ativo = BC001B3_A284Menu_Ativo[0];
            A285Menu_PaiCod = BC001B3_A285Menu_PaiCod[0];
            n285Menu_PaiCod = BC001B3_n285Menu_PaiCod[0];
            A282Menu_Imagem = BC001B3_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001B3_n282Menu_Imagem[0];
            Z277Menu_Codigo = A277Menu_Codigo;
            sMode48 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1B48( ) ;
            if ( AnyError == 1 )
            {
               RcdFound48 = 0;
               InitializeNonKey1B48( ) ;
            }
            Gx_mode = sMode48;
         }
         else
         {
            RcdFound48 = 0;
            InitializeNonKey1B48( ) ;
            sMode48 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode48;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1B48( ) ;
         if ( RcdFound48 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1B0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1B48( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001B2 */
            pr_default.execute(0, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Menu"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z278Menu_Nome, BC001B2_A278Menu_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z279Menu_Descricao, BC001B2_A279Menu_Descricao[0]) != 0 ) || ( Z280Menu_Tipo != BC001B2_A280Menu_Tipo[0] ) || ( StringUtil.StrCmp(Z281Menu_Link, BC001B2_A281Menu_Link[0]) != 0 ) || ( Z283Menu_Ordem != BC001B2_A283Menu_Ordem[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z284Menu_Ativo != BC001B2_A284Menu_Ativo[0] ) || ( Z285Menu_PaiCod != BC001B2_A285Menu_PaiCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Menu"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1B48( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1B48( 0) ;
            CheckOptimisticConcurrency1B48( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1B48( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1B48( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001B7 */
                     pr_default.execute(5, new Object[] {A278Menu_Nome, n279Menu_Descricao, A279Menu_Descricao, A280Menu_Tipo, n281Menu_Link, A281Menu_Link, n282Menu_Imagem, A282Menu_Imagem, n40000Menu_Imagem_GXI, A40000Menu_Imagem_GXI, A283Menu_Ordem, A284Menu_Ativo, n285Menu_PaiCod, A285Menu_PaiCod});
                     A277Menu_Codigo = BC001B7_A277Menu_Codigo[0];
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1B48( ) ;
            }
            EndLevel1B48( ) ;
         }
         CloseExtendedTableCursors1B48( ) ;
      }

      protected void Update1B48( )
      {
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1B48( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1B48( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1B48( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001B8 */
                     pr_default.execute(6, new Object[] {A278Menu_Nome, n279Menu_Descricao, A279Menu_Descricao, A280Menu_Tipo, n281Menu_Link, A281Menu_Link, A283Menu_Ordem, A284Menu_Ativo, n285Menu_PaiCod, A285Menu_PaiCod, A277Menu_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Menu"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1B48( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1B48( ) ;
         }
         CloseExtendedTableCursors1B48( ) ;
      }

      protected void DeferredUpdate1B48( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC001B9 */
            pr_default.execute(7, new Object[] {n282Menu_Imagem, A282Menu_Imagem, n40000Menu_Imagem_GXI, A40000Menu_Imagem_GXI, A277Menu_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1B48( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1B48( ) ;
            AfterConfirm1B48( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1B48( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001B10 */
                  pr_default.execute(8, new Object[] {A277Menu_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode48 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1B48( ) ;
         Gx_mode = sMode48;
      }

      protected void OnDeleteControls1B48( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001B11 */
            pr_default.execute(9, new Object[] {n285Menu_PaiCod, A285Menu_PaiCod});
            A286Menu_PaiNom = BC001B11_A286Menu_PaiNom[0];
            n286Menu_PaiNom = BC001B11_n286Menu_PaiNom[0];
            A287Menu_PaiTip = BC001B11_A287Menu_PaiTip[0];
            n287Menu_PaiTip = BC001B11_n287Menu_PaiTip[0];
            A288Menu_PaiAti = BC001B11_A288Menu_PaiAti[0];
            n288Menu_PaiAti = BC001B11_n288Menu_PaiAti[0];
            pr_default.close(9);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC001B12 */
            pr_default.execute(10, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Menu"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC001B13 */
            pr_default.execute(11, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Menu Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
         }
      }

      protected void EndLevel1B48( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1B48( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1B48( )
      {
         /* Scan By routine */
         /* Using cursor BC001B14 */
         pr_default.execute(12, new Object[] {A277Menu_Codigo});
         RcdFound48 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound48 = 1;
            A277Menu_Codigo = BC001B14_A277Menu_Codigo[0];
            A278Menu_Nome = BC001B14_A278Menu_Nome[0];
            A279Menu_Descricao = BC001B14_A279Menu_Descricao[0];
            n279Menu_Descricao = BC001B14_n279Menu_Descricao[0];
            A280Menu_Tipo = BC001B14_A280Menu_Tipo[0];
            A281Menu_Link = BC001B14_A281Menu_Link[0];
            n281Menu_Link = BC001B14_n281Menu_Link[0];
            A40000Menu_Imagem_GXI = BC001B14_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001B14_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = BC001B14_A283Menu_Ordem[0];
            A286Menu_PaiNom = BC001B14_A286Menu_PaiNom[0];
            n286Menu_PaiNom = BC001B14_n286Menu_PaiNom[0];
            A287Menu_PaiTip = BC001B14_A287Menu_PaiTip[0];
            n287Menu_PaiTip = BC001B14_n287Menu_PaiTip[0];
            A288Menu_PaiAti = BC001B14_A288Menu_PaiAti[0];
            n288Menu_PaiAti = BC001B14_n288Menu_PaiAti[0];
            A284Menu_Ativo = BC001B14_A284Menu_Ativo[0];
            A285Menu_PaiCod = BC001B14_A285Menu_PaiCod[0];
            n285Menu_PaiCod = BC001B14_n285Menu_PaiCod[0];
            A282Menu_Imagem = BC001B14_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001B14_n282Menu_Imagem[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1B48( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound48 = 0;
         ScanKeyLoad1B48( ) ;
      }

      protected void ScanKeyLoad1B48( )
      {
         sMode48 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound48 = 1;
            A277Menu_Codigo = BC001B14_A277Menu_Codigo[0];
            A278Menu_Nome = BC001B14_A278Menu_Nome[0];
            A279Menu_Descricao = BC001B14_A279Menu_Descricao[0];
            n279Menu_Descricao = BC001B14_n279Menu_Descricao[0];
            A280Menu_Tipo = BC001B14_A280Menu_Tipo[0];
            A281Menu_Link = BC001B14_A281Menu_Link[0];
            n281Menu_Link = BC001B14_n281Menu_Link[0];
            A40000Menu_Imagem_GXI = BC001B14_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001B14_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = BC001B14_A283Menu_Ordem[0];
            A286Menu_PaiNom = BC001B14_A286Menu_PaiNom[0];
            n286Menu_PaiNom = BC001B14_n286Menu_PaiNom[0];
            A287Menu_PaiTip = BC001B14_A287Menu_PaiTip[0];
            n287Menu_PaiTip = BC001B14_n287Menu_PaiTip[0];
            A288Menu_PaiAti = BC001B14_A288Menu_PaiAti[0];
            n288Menu_PaiAti = BC001B14_n288Menu_PaiAti[0];
            A284Menu_Ativo = BC001B14_A284Menu_Ativo[0];
            A285Menu_PaiCod = BC001B14_A285Menu_PaiCod[0];
            n285Menu_PaiCod = BC001B14_n285Menu_PaiCod[0];
            A282Menu_Imagem = BC001B14_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001B14_n282Menu_Imagem[0];
         }
         Gx_mode = sMode48;
      }

      protected void ScanKeyEnd1B48( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm1B48( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1B48( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1B48( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1B48( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1B48( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1B48( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1B48( )
      {
      }

      protected void AddRow1B48( )
      {
         VarsToRow48( bcMenu) ;
      }

      protected void ReadRow1B48( )
      {
         RowToVars48( bcMenu, 1) ;
      }

      protected void InitializeNonKey1B48( )
      {
         A278Menu_Nome = "";
         A279Menu_Descricao = "";
         n279Menu_Descricao = false;
         A280Menu_Tipo = 0;
         A281Menu_Link = "";
         n281Menu_Link = false;
         A282Menu_Imagem = "";
         n282Menu_Imagem = false;
         A40000Menu_Imagem_GXI = "";
         n40000Menu_Imagem_GXI = false;
         A285Menu_PaiCod = 0;
         n285Menu_PaiCod = false;
         A286Menu_PaiNom = "";
         n286Menu_PaiNom = false;
         A287Menu_PaiTip = 0;
         n287Menu_PaiTip = false;
         A288Menu_PaiAti = false;
         n288Menu_PaiAti = false;
         A283Menu_Ordem = 0;
         A284Menu_Ativo = true;
         Z278Menu_Nome = "";
         Z279Menu_Descricao = "";
         Z280Menu_Tipo = 0;
         Z281Menu_Link = "";
         Z283Menu_Ordem = 0;
         Z284Menu_Ativo = false;
         Z285Menu_PaiCod = 0;
      }

      protected void InitAll1B48( )
      {
         A277Menu_Codigo = 0;
         InitializeNonKey1B48( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A284Menu_Ativo = i284Menu_Ativo;
         A283Menu_Ordem = i283Menu_Ordem;
      }

      public void VarsToRow48( SdtMenu obj48 )
      {
         obj48.gxTpr_Mode = Gx_mode;
         obj48.gxTpr_Menu_nome = A278Menu_Nome;
         obj48.gxTpr_Menu_descricao = A279Menu_Descricao;
         obj48.gxTpr_Menu_tipo = A280Menu_Tipo;
         obj48.gxTpr_Menu_link = A281Menu_Link;
         obj48.gxTpr_Menu_imagem = A282Menu_Imagem;
         obj48.gxTpr_Menu_imagem_gxi = A40000Menu_Imagem_GXI;
         obj48.gxTpr_Menu_paicod = A285Menu_PaiCod;
         obj48.gxTpr_Menu_painom = A286Menu_PaiNom;
         obj48.gxTpr_Menu_paitip = A287Menu_PaiTip;
         obj48.gxTpr_Menu_paiati = A288Menu_PaiAti;
         obj48.gxTpr_Menu_ordem = A283Menu_Ordem;
         obj48.gxTpr_Menu_ativo = A284Menu_Ativo;
         obj48.gxTpr_Menu_codigo = A277Menu_Codigo;
         obj48.gxTpr_Menu_codigo_Z = Z277Menu_Codigo;
         obj48.gxTpr_Menu_nome_Z = Z278Menu_Nome;
         obj48.gxTpr_Menu_descricao_Z = Z279Menu_Descricao;
         obj48.gxTpr_Menu_tipo_Z = Z280Menu_Tipo;
         obj48.gxTpr_Menu_link_Z = Z281Menu_Link;
         obj48.gxTpr_Menu_ordem_Z = Z283Menu_Ordem;
         obj48.gxTpr_Menu_paicod_Z = Z285Menu_PaiCod;
         obj48.gxTpr_Menu_painom_Z = Z286Menu_PaiNom;
         obj48.gxTpr_Menu_paitip_Z = Z287Menu_PaiTip;
         obj48.gxTpr_Menu_paiati_Z = Z288Menu_PaiAti;
         obj48.gxTpr_Menu_ativo_Z = Z284Menu_Ativo;
         obj48.gxTpr_Menu_imagem_gxi_Z = Z40000Menu_Imagem_GXI;
         obj48.gxTpr_Menu_descricao_N = (short)(Convert.ToInt16(n279Menu_Descricao));
         obj48.gxTpr_Menu_link_N = (short)(Convert.ToInt16(n281Menu_Link));
         obj48.gxTpr_Menu_imagem_N = (short)(Convert.ToInt16(n282Menu_Imagem));
         obj48.gxTpr_Menu_paicod_N = (short)(Convert.ToInt16(n285Menu_PaiCod));
         obj48.gxTpr_Menu_painom_N = (short)(Convert.ToInt16(n286Menu_PaiNom));
         obj48.gxTpr_Menu_paitip_N = (short)(Convert.ToInt16(n287Menu_PaiTip));
         obj48.gxTpr_Menu_paiati_N = (short)(Convert.ToInt16(n288Menu_PaiAti));
         obj48.gxTpr_Menu_imagem_gxi_N = (short)(Convert.ToInt16(n40000Menu_Imagem_GXI));
         obj48.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow48( SdtMenu obj48 )
      {
         obj48.gxTpr_Menu_codigo = A277Menu_Codigo;
         return  ;
      }

      public void RowToVars48( SdtMenu obj48 ,
                               int forceLoad )
      {
         Gx_mode = obj48.gxTpr_Mode;
         A278Menu_Nome = obj48.gxTpr_Menu_nome;
         A279Menu_Descricao = obj48.gxTpr_Menu_descricao;
         n279Menu_Descricao = false;
         A280Menu_Tipo = obj48.gxTpr_Menu_tipo;
         A281Menu_Link = obj48.gxTpr_Menu_link;
         n281Menu_Link = false;
         A282Menu_Imagem = obj48.gxTpr_Menu_imagem;
         n282Menu_Imagem = false;
         A40000Menu_Imagem_GXI = obj48.gxTpr_Menu_imagem_gxi;
         n40000Menu_Imagem_GXI = false;
         A285Menu_PaiCod = obj48.gxTpr_Menu_paicod;
         n285Menu_PaiCod = false;
         A286Menu_PaiNom = obj48.gxTpr_Menu_painom;
         n286Menu_PaiNom = false;
         A287Menu_PaiTip = obj48.gxTpr_Menu_paitip;
         n287Menu_PaiTip = false;
         A288Menu_PaiAti = obj48.gxTpr_Menu_paiati;
         n288Menu_PaiAti = false;
         A283Menu_Ordem = obj48.gxTpr_Menu_ordem;
         A284Menu_Ativo = obj48.gxTpr_Menu_ativo;
         A277Menu_Codigo = obj48.gxTpr_Menu_codigo;
         Z277Menu_Codigo = obj48.gxTpr_Menu_codigo_Z;
         Z278Menu_Nome = obj48.gxTpr_Menu_nome_Z;
         Z279Menu_Descricao = obj48.gxTpr_Menu_descricao_Z;
         Z280Menu_Tipo = obj48.gxTpr_Menu_tipo_Z;
         Z281Menu_Link = obj48.gxTpr_Menu_link_Z;
         Z283Menu_Ordem = obj48.gxTpr_Menu_ordem_Z;
         Z285Menu_PaiCod = obj48.gxTpr_Menu_paicod_Z;
         Z286Menu_PaiNom = obj48.gxTpr_Menu_painom_Z;
         Z287Menu_PaiTip = obj48.gxTpr_Menu_paitip_Z;
         Z288Menu_PaiAti = obj48.gxTpr_Menu_paiati_Z;
         Z284Menu_Ativo = obj48.gxTpr_Menu_ativo_Z;
         Z40000Menu_Imagem_GXI = obj48.gxTpr_Menu_imagem_gxi_Z;
         n279Menu_Descricao = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_descricao_N));
         n281Menu_Link = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_link_N));
         n282Menu_Imagem = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_imagem_N));
         n285Menu_PaiCod = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_paicod_N));
         n286Menu_PaiNom = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_painom_N));
         n287Menu_PaiTip = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_paitip_N));
         n288Menu_PaiAti = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_paiati_N));
         n40000Menu_Imagem_GXI = (bool)(Convert.ToBoolean(obj48.gxTpr_Menu_imagem_gxi_N));
         Gx_mode = obj48.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A277Menu_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1B48( ) ;
         ScanKeyStart1B48( ) ;
         if ( RcdFound48 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z277Menu_Codigo = A277Menu_Codigo;
         }
         ZM1B48( -8) ;
         OnLoadActions1B48( ) ;
         AddRow1B48( ) ;
         ScanKeyEnd1B48( ) ;
         if ( RcdFound48 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars48( bcMenu, 0) ;
         ScanKeyStart1B48( ) ;
         if ( RcdFound48 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z277Menu_Codigo = A277Menu_Codigo;
         }
         ZM1B48( -8) ;
         OnLoadActions1B48( ) ;
         AddRow1B48( ) ;
         ScanKeyEnd1B48( ) ;
         if ( RcdFound48 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars48( bcMenu, 0) ;
         nKeyPressed = 1;
         GetKey1B48( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1B48( ) ;
         }
         else
         {
            if ( RcdFound48 == 1 )
            {
               if ( A277Menu_Codigo != Z277Menu_Codigo )
               {
                  A277Menu_Codigo = Z277Menu_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1B48( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A277Menu_Codigo != Z277Menu_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1B48( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1B48( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow48( bcMenu) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars48( bcMenu, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1B48( ) ;
         if ( RcdFound48 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A277Menu_Codigo != Z277Menu_Codigo )
            {
               A277Menu_Codigo = Z277Menu_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A277Menu_Codigo != Z277Menu_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "Menu_BC");
         VarsToRow48( bcMenu) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcMenu.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcMenu.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcMenu )
         {
            bcMenu = (SdtMenu)(sdt);
            if ( StringUtil.StrCmp(bcMenu.gxTpr_Mode, "") == 0 )
            {
               bcMenu.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow48( bcMenu) ;
            }
            else
            {
               RowToVars48( bcMenu, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcMenu.gxTpr_Mode, "") == 0 )
            {
               bcMenu.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars48( bcMenu, 1) ;
         return  ;
      }

      public SdtMenu Menu_BC
      {
         get {
            return bcMenu ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z278Menu_Nome = "";
         A278Menu_Nome = "";
         Z279Menu_Descricao = "";
         A279Menu_Descricao = "";
         Z281Menu_Link = "";
         A281Menu_Link = "";
         Z286Menu_PaiNom = "";
         A286Menu_PaiNom = "";
         Z282Menu_Imagem = "";
         A282Menu_Imagem = "";
         Z40000Menu_Imagem_GXI = "";
         A40000Menu_Imagem_GXI = "";
         BC001B5_A277Menu_Codigo = new int[1] ;
         BC001B5_A278Menu_Nome = new String[] {""} ;
         BC001B5_A279Menu_Descricao = new String[] {""} ;
         BC001B5_n279Menu_Descricao = new bool[] {false} ;
         BC001B5_A280Menu_Tipo = new short[1] ;
         BC001B5_A281Menu_Link = new String[] {""} ;
         BC001B5_n281Menu_Link = new bool[] {false} ;
         BC001B5_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001B5_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001B5_A283Menu_Ordem = new short[1] ;
         BC001B5_A286Menu_PaiNom = new String[] {""} ;
         BC001B5_n286Menu_PaiNom = new bool[] {false} ;
         BC001B5_A287Menu_PaiTip = new short[1] ;
         BC001B5_n287Menu_PaiTip = new bool[] {false} ;
         BC001B5_A288Menu_PaiAti = new bool[] {false} ;
         BC001B5_n288Menu_PaiAti = new bool[] {false} ;
         BC001B5_A284Menu_Ativo = new bool[] {false} ;
         BC001B5_A285Menu_PaiCod = new int[1] ;
         BC001B5_n285Menu_PaiCod = new bool[] {false} ;
         BC001B5_A282Menu_Imagem = new String[] {""} ;
         BC001B5_n282Menu_Imagem = new bool[] {false} ;
         BC001B4_A286Menu_PaiNom = new String[] {""} ;
         BC001B4_n286Menu_PaiNom = new bool[] {false} ;
         BC001B4_A287Menu_PaiTip = new short[1] ;
         BC001B4_n287Menu_PaiTip = new bool[] {false} ;
         BC001B4_A288Menu_PaiAti = new bool[] {false} ;
         BC001B4_n288Menu_PaiAti = new bool[] {false} ;
         BC001B6_A277Menu_Codigo = new int[1] ;
         BC001B3_A277Menu_Codigo = new int[1] ;
         BC001B3_A278Menu_Nome = new String[] {""} ;
         BC001B3_A279Menu_Descricao = new String[] {""} ;
         BC001B3_n279Menu_Descricao = new bool[] {false} ;
         BC001B3_A280Menu_Tipo = new short[1] ;
         BC001B3_A281Menu_Link = new String[] {""} ;
         BC001B3_n281Menu_Link = new bool[] {false} ;
         BC001B3_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001B3_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001B3_A283Menu_Ordem = new short[1] ;
         BC001B3_A284Menu_Ativo = new bool[] {false} ;
         BC001B3_A285Menu_PaiCod = new int[1] ;
         BC001B3_n285Menu_PaiCod = new bool[] {false} ;
         BC001B3_A282Menu_Imagem = new String[] {""} ;
         BC001B3_n282Menu_Imagem = new bool[] {false} ;
         sMode48 = "";
         BC001B2_A277Menu_Codigo = new int[1] ;
         BC001B2_A278Menu_Nome = new String[] {""} ;
         BC001B2_A279Menu_Descricao = new String[] {""} ;
         BC001B2_n279Menu_Descricao = new bool[] {false} ;
         BC001B2_A280Menu_Tipo = new short[1] ;
         BC001B2_A281Menu_Link = new String[] {""} ;
         BC001B2_n281Menu_Link = new bool[] {false} ;
         BC001B2_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001B2_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001B2_A283Menu_Ordem = new short[1] ;
         BC001B2_A284Menu_Ativo = new bool[] {false} ;
         BC001B2_A285Menu_PaiCod = new int[1] ;
         BC001B2_n285Menu_PaiCod = new bool[] {false} ;
         BC001B2_A282Menu_Imagem = new String[] {""} ;
         BC001B2_n282Menu_Imagem = new bool[] {false} ;
         BC001B7_A277Menu_Codigo = new int[1] ;
         BC001B11_A286Menu_PaiNom = new String[] {""} ;
         BC001B11_n286Menu_PaiNom = new bool[] {false} ;
         BC001B11_A287Menu_PaiTip = new short[1] ;
         BC001B11_n287Menu_PaiTip = new bool[] {false} ;
         BC001B11_A288Menu_PaiAti = new bool[] {false} ;
         BC001B11_n288Menu_PaiAti = new bool[] {false} ;
         BC001B12_A285Menu_PaiCod = new int[1] ;
         BC001B12_n285Menu_PaiCod = new bool[] {false} ;
         BC001B13_A277Menu_Codigo = new int[1] ;
         BC001B13_A3Perfil_Codigo = new int[1] ;
         BC001B14_A277Menu_Codigo = new int[1] ;
         BC001B14_A278Menu_Nome = new String[] {""} ;
         BC001B14_A279Menu_Descricao = new String[] {""} ;
         BC001B14_n279Menu_Descricao = new bool[] {false} ;
         BC001B14_A280Menu_Tipo = new short[1] ;
         BC001B14_A281Menu_Link = new String[] {""} ;
         BC001B14_n281Menu_Link = new bool[] {false} ;
         BC001B14_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001B14_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001B14_A283Menu_Ordem = new short[1] ;
         BC001B14_A286Menu_PaiNom = new String[] {""} ;
         BC001B14_n286Menu_PaiNom = new bool[] {false} ;
         BC001B14_A287Menu_PaiTip = new short[1] ;
         BC001B14_n287Menu_PaiTip = new bool[] {false} ;
         BC001B14_A288Menu_PaiAti = new bool[] {false} ;
         BC001B14_n288Menu_PaiAti = new bool[] {false} ;
         BC001B14_A284Menu_Ativo = new bool[] {false} ;
         BC001B14_A285Menu_PaiCod = new int[1] ;
         BC001B14_n285Menu_PaiCod = new bool[] {false} ;
         BC001B14_A282Menu_Imagem = new String[] {""} ;
         BC001B14_n282Menu_Imagem = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.menu_bc__default(),
            new Object[][] {
                new Object[] {
               BC001B2_A277Menu_Codigo, BC001B2_A278Menu_Nome, BC001B2_A279Menu_Descricao, BC001B2_n279Menu_Descricao, BC001B2_A280Menu_Tipo, BC001B2_A281Menu_Link, BC001B2_n281Menu_Link, BC001B2_A40000Menu_Imagem_GXI, BC001B2_n40000Menu_Imagem_GXI, BC001B2_A283Menu_Ordem,
               BC001B2_A284Menu_Ativo, BC001B2_A285Menu_PaiCod, BC001B2_n285Menu_PaiCod, BC001B2_A282Menu_Imagem, BC001B2_n282Menu_Imagem
               }
               , new Object[] {
               BC001B3_A277Menu_Codigo, BC001B3_A278Menu_Nome, BC001B3_A279Menu_Descricao, BC001B3_n279Menu_Descricao, BC001B3_A280Menu_Tipo, BC001B3_A281Menu_Link, BC001B3_n281Menu_Link, BC001B3_A40000Menu_Imagem_GXI, BC001B3_n40000Menu_Imagem_GXI, BC001B3_A283Menu_Ordem,
               BC001B3_A284Menu_Ativo, BC001B3_A285Menu_PaiCod, BC001B3_n285Menu_PaiCod, BC001B3_A282Menu_Imagem, BC001B3_n282Menu_Imagem
               }
               , new Object[] {
               BC001B4_A286Menu_PaiNom, BC001B4_n286Menu_PaiNom, BC001B4_A287Menu_PaiTip, BC001B4_n287Menu_PaiTip, BC001B4_A288Menu_PaiAti, BC001B4_n288Menu_PaiAti
               }
               , new Object[] {
               BC001B5_A277Menu_Codigo, BC001B5_A278Menu_Nome, BC001B5_A279Menu_Descricao, BC001B5_n279Menu_Descricao, BC001B5_A280Menu_Tipo, BC001B5_A281Menu_Link, BC001B5_n281Menu_Link, BC001B5_A40000Menu_Imagem_GXI, BC001B5_n40000Menu_Imagem_GXI, BC001B5_A283Menu_Ordem,
               BC001B5_A286Menu_PaiNom, BC001B5_n286Menu_PaiNom, BC001B5_A287Menu_PaiTip, BC001B5_n287Menu_PaiTip, BC001B5_A288Menu_PaiAti, BC001B5_n288Menu_PaiAti, BC001B5_A284Menu_Ativo, BC001B5_A285Menu_PaiCod, BC001B5_n285Menu_PaiCod, BC001B5_A282Menu_Imagem,
               BC001B5_n282Menu_Imagem
               }
               , new Object[] {
               BC001B6_A277Menu_Codigo
               }
               , new Object[] {
               BC001B7_A277Menu_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001B11_A286Menu_PaiNom, BC001B11_n286Menu_PaiNom, BC001B11_A287Menu_PaiTip, BC001B11_n287Menu_PaiTip, BC001B11_A288Menu_PaiAti, BC001B11_n288Menu_PaiAti
               }
               , new Object[] {
               BC001B12_A285Menu_PaiCod
               }
               , new Object[] {
               BC001B13_A277Menu_Codigo, BC001B13_A3Perfil_Codigo
               }
               , new Object[] {
               BC001B14_A277Menu_Codigo, BC001B14_A278Menu_Nome, BC001B14_A279Menu_Descricao, BC001B14_n279Menu_Descricao, BC001B14_A280Menu_Tipo, BC001B14_A281Menu_Link, BC001B14_n281Menu_Link, BC001B14_A40000Menu_Imagem_GXI, BC001B14_n40000Menu_Imagem_GXI, BC001B14_A283Menu_Ordem,
               BC001B14_A286Menu_PaiNom, BC001B14_n286Menu_PaiNom, BC001B14_A287Menu_PaiTip, BC001B14_n287Menu_PaiTip, BC001B14_A288Menu_PaiAti, BC001B14_n288Menu_PaiAti, BC001B14_A284Menu_Ativo, BC001B14_A285Menu_PaiCod, BC001B14_n285Menu_PaiCod, BC001B14_A282Menu_Imagem,
               BC001B14_n282Menu_Imagem
               }
            }
         );
         Z284Menu_Ativo = true;
         A284Menu_Ativo = true;
         i284Menu_Ativo = true;
         Z283Menu_Ordem = 0;
         A283Menu_Ordem = 0;
         i283Menu_Ordem = 0;
         AV13Pgmname = "Menu_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E121B2 */
         E121B2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z280Menu_Tipo ;
      private short A280Menu_Tipo ;
      private short Z283Menu_Ordem ;
      private short A283Menu_Ordem ;
      private short Z287Menu_PaiTip ;
      private short A287Menu_PaiTip ;
      private short Gx_BScreen ;
      private short RcdFound48 ;
      private short i283Menu_Ordem ;
      private int trnEnded ;
      private int Z277Menu_Codigo ;
      private int A277Menu_Codigo ;
      private int AV14GXV1 ;
      private int AV11Insert_Menu_PaiCod ;
      private int Z285Menu_PaiCod ;
      private int A285Menu_PaiCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV13Pgmname ;
      private String Z278Menu_Nome ;
      private String A278Menu_Nome ;
      private String Z286Menu_PaiNom ;
      private String A286Menu_PaiNom ;
      private String sMode48 ;
      private bool Z284Menu_Ativo ;
      private bool A284Menu_Ativo ;
      private bool Z288Menu_PaiAti ;
      private bool A288Menu_PaiAti ;
      private bool n279Menu_Descricao ;
      private bool n281Menu_Link ;
      private bool n40000Menu_Imagem_GXI ;
      private bool n286Menu_PaiNom ;
      private bool n287Menu_PaiTip ;
      private bool n288Menu_PaiAti ;
      private bool n285Menu_PaiCod ;
      private bool n282Menu_Imagem ;
      private bool Gx_longc ;
      private bool i284Menu_Ativo ;
      private String Z279Menu_Descricao ;
      private String A279Menu_Descricao ;
      private String Z281Menu_Link ;
      private String A281Menu_Link ;
      private String Z40000Menu_Imagem_GXI ;
      private String A40000Menu_Imagem_GXI ;
      private String Z282Menu_Imagem ;
      private String A282Menu_Imagem ;
      private IGxSession AV10WebSession ;
      private SdtMenu bcMenu ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC001B5_A277Menu_Codigo ;
      private String[] BC001B5_A278Menu_Nome ;
      private String[] BC001B5_A279Menu_Descricao ;
      private bool[] BC001B5_n279Menu_Descricao ;
      private short[] BC001B5_A280Menu_Tipo ;
      private String[] BC001B5_A281Menu_Link ;
      private bool[] BC001B5_n281Menu_Link ;
      private String[] BC001B5_A40000Menu_Imagem_GXI ;
      private bool[] BC001B5_n40000Menu_Imagem_GXI ;
      private short[] BC001B5_A283Menu_Ordem ;
      private String[] BC001B5_A286Menu_PaiNom ;
      private bool[] BC001B5_n286Menu_PaiNom ;
      private short[] BC001B5_A287Menu_PaiTip ;
      private bool[] BC001B5_n287Menu_PaiTip ;
      private bool[] BC001B5_A288Menu_PaiAti ;
      private bool[] BC001B5_n288Menu_PaiAti ;
      private bool[] BC001B5_A284Menu_Ativo ;
      private int[] BC001B5_A285Menu_PaiCod ;
      private bool[] BC001B5_n285Menu_PaiCod ;
      private String[] BC001B5_A282Menu_Imagem ;
      private bool[] BC001B5_n282Menu_Imagem ;
      private String[] BC001B4_A286Menu_PaiNom ;
      private bool[] BC001B4_n286Menu_PaiNom ;
      private short[] BC001B4_A287Menu_PaiTip ;
      private bool[] BC001B4_n287Menu_PaiTip ;
      private bool[] BC001B4_A288Menu_PaiAti ;
      private bool[] BC001B4_n288Menu_PaiAti ;
      private int[] BC001B6_A277Menu_Codigo ;
      private int[] BC001B3_A277Menu_Codigo ;
      private String[] BC001B3_A278Menu_Nome ;
      private String[] BC001B3_A279Menu_Descricao ;
      private bool[] BC001B3_n279Menu_Descricao ;
      private short[] BC001B3_A280Menu_Tipo ;
      private String[] BC001B3_A281Menu_Link ;
      private bool[] BC001B3_n281Menu_Link ;
      private String[] BC001B3_A40000Menu_Imagem_GXI ;
      private bool[] BC001B3_n40000Menu_Imagem_GXI ;
      private short[] BC001B3_A283Menu_Ordem ;
      private bool[] BC001B3_A284Menu_Ativo ;
      private int[] BC001B3_A285Menu_PaiCod ;
      private bool[] BC001B3_n285Menu_PaiCod ;
      private String[] BC001B3_A282Menu_Imagem ;
      private bool[] BC001B3_n282Menu_Imagem ;
      private int[] BC001B2_A277Menu_Codigo ;
      private String[] BC001B2_A278Menu_Nome ;
      private String[] BC001B2_A279Menu_Descricao ;
      private bool[] BC001B2_n279Menu_Descricao ;
      private short[] BC001B2_A280Menu_Tipo ;
      private String[] BC001B2_A281Menu_Link ;
      private bool[] BC001B2_n281Menu_Link ;
      private String[] BC001B2_A40000Menu_Imagem_GXI ;
      private bool[] BC001B2_n40000Menu_Imagem_GXI ;
      private short[] BC001B2_A283Menu_Ordem ;
      private bool[] BC001B2_A284Menu_Ativo ;
      private int[] BC001B2_A285Menu_PaiCod ;
      private bool[] BC001B2_n285Menu_PaiCod ;
      private String[] BC001B2_A282Menu_Imagem ;
      private bool[] BC001B2_n282Menu_Imagem ;
      private int[] BC001B7_A277Menu_Codigo ;
      private String[] BC001B11_A286Menu_PaiNom ;
      private bool[] BC001B11_n286Menu_PaiNom ;
      private short[] BC001B11_A287Menu_PaiTip ;
      private bool[] BC001B11_n287Menu_PaiTip ;
      private bool[] BC001B11_A288Menu_PaiAti ;
      private bool[] BC001B11_n288Menu_PaiAti ;
      private int[] BC001B12_A285Menu_PaiCod ;
      private bool[] BC001B12_n285Menu_PaiCod ;
      private int[] BC001B13_A277Menu_Codigo ;
      private int[] BC001B13_A3Perfil_Codigo ;
      private int[] BC001B14_A277Menu_Codigo ;
      private String[] BC001B14_A278Menu_Nome ;
      private String[] BC001B14_A279Menu_Descricao ;
      private bool[] BC001B14_n279Menu_Descricao ;
      private short[] BC001B14_A280Menu_Tipo ;
      private String[] BC001B14_A281Menu_Link ;
      private bool[] BC001B14_n281Menu_Link ;
      private String[] BC001B14_A40000Menu_Imagem_GXI ;
      private bool[] BC001B14_n40000Menu_Imagem_GXI ;
      private short[] BC001B14_A283Menu_Ordem ;
      private String[] BC001B14_A286Menu_PaiNom ;
      private bool[] BC001B14_n286Menu_PaiNom ;
      private short[] BC001B14_A287Menu_PaiTip ;
      private bool[] BC001B14_n287Menu_PaiTip ;
      private bool[] BC001B14_A288Menu_PaiAti ;
      private bool[] BC001B14_n288Menu_PaiAti ;
      private bool[] BC001B14_A284Menu_Ativo ;
      private int[] BC001B14_A285Menu_PaiCod ;
      private bool[] BC001B14_n285Menu_PaiCod ;
      private String[] BC001B14_A282Menu_Imagem ;
      private bool[] BC001B14_n282Menu_Imagem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class menu_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001B5 ;
          prmBC001B5 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B4 ;
          prmBC001B4 = new Object[] {
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B6 ;
          prmBC001B6 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B3 ;
          prmBC001B3 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B2 ;
          prmBC001B2 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B7 ;
          prmBC001B7 = new Object[] {
          new Object[] {"@Menu_Nome",SqlDbType.Char,30,0} ,
          new Object[] {"@Menu_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Menu_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Menu_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Menu_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Menu_Imagem_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Menu_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Menu_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B8 ;
          prmBC001B8 = new Object[] {
          new Object[] {"@Menu_Nome",SqlDbType.Char,30,0} ,
          new Object[] {"@Menu_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Menu_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Menu_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Menu_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Menu_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B9 ;
          prmBC001B9 = new Object[] {
          new Object[] {"@Menu_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Menu_Imagem_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B10 ;
          prmBC001B10 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B11 ;
          prmBC001B11 = new Object[] {
          new Object[] {"@Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B12 ;
          prmBC001B12 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B13 ;
          prmBC001B13 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001B14 ;
          prmBC001B14 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001B2", "SELECT [Menu_Codigo], [Menu_Nome], [Menu_Descricao], [Menu_Tipo], [Menu_Link], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Ativo], [Menu_PaiCod] AS Menu_PaiCod, [Menu_Imagem] FROM [Menu] WITH (UPDLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B2,1,0,true,false )
             ,new CursorDef("BC001B3", "SELECT [Menu_Codigo], [Menu_Nome], [Menu_Descricao], [Menu_Tipo], [Menu_Link], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Ativo], [Menu_PaiCod] AS Menu_PaiCod, [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B3,1,0,true,false )
             ,new CursorDef("BC001B4", "SELECT [Menu_Nome] AS Menu_PaiNom, [Menu_Tipo] AS Menu_PaiTip, [Menu_Ativo] AS Menu_PaiAti FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B4,1,0,true,false )
             ,new CursorDef("BC001B5", "SELECT TM1.[Menu_Codigo], TM1.[Menu_Nome], TM1.[Menu_Descricao], TM1.[Menu_Tipo], TM1.[Menu_Link], TM1.[Menu_Imagem_GXI], TM1.[Menu_Ordem], T2.[Menu_Nome] AS Menu_PaiNom, T2.[Menu_Tipo] AS Menu_PaiTip, T2.[Menu_Ativo] AS Menu_PaiAti, TM1.[Menu_Ativo], TM1.[Menu_PaiCod] AS Menu_PaiCod, TM1.[Menu_Imagem] FROM ([Menu] TM1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = TM1.[Menu_PaiCod]) WHERE TM1.[Menu_Codigo] = @Menu_Codigo ORDER BY TM1.[Menu_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B5,100,0,true,false )
             ,new CursorDef("BC001B6", "SELECT [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B6,1,0,true,false )
             ,new CursorDef("BC001B7", "INSERT INTO [Menu]([Menu_Nome], [Menu_Descricao], [Menu_Tipo], [Menu_Link], [Menu_Imagem], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Ativo], [Menu_PaiCod]) VALUES(@Menu_Nome, @Menu_Descricao, @Menu_Tipo, @Menu_Link, @Menu_Imagem, @Menu_Imagem_GXI, @Menu_Ordem, @Menu_Ativo, @Menu_PaiCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC001B7)
             ,new CursorDef("BC001B8", "UPDATE [Menu] SET [Menu_Nome]=@Menu_Nome, [Menu_Descricao]=@Menu_Descricao, [Menu_Tipo]=@Menu_Tipo, [Menu_Link]=@Menu_Link, [Menu_Ordem]=@Menu_Ordem, [Menu_Ativo]=@Menu_Ativo, [Menu_PaiCod]=@Menu_PaiCod  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK,prmBC001B8)
             ,new CursorDef("BC001B9", "UPDATE [Menu] SET [Menu_Imagem]=@Menu_Imagem, [Menu_Imagem_GXI]=@Menu_Imagem_GXI  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK,prmBC001B9)
             ,new CursorDef("BC001B10", "DELETE FROM [Menu]  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK,prmBC001B10)
             ,new CursorDef("BC001B11", "SELECT [Menu_Nome] AS Menu_PaiNom, [Menu_Tipo] AS Menu_PaiTip, [Menu_Ativo] AS Menu_PaiAti FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B11,1,0,true,false )
             ,new CursorDef("BC001B12", "SELECT TOP 1 [Menu_Codigo] AS Menu_PaiCod FROM [Menu] WITH (NOLOCK) WHERE [Menu_PaiCod] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B12,1,0,true,true )
             ,new CursorDef("BC001B13", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B13,1,0,true,true )
             ,new CursorDef("BC001B14", "SELECT TM1.[Menu_Codigo], TM1.[Menu_Nome], TM1.[Menu_Descricao], TM1.[Menu_Tipo], TM1.[Menu_Link], TM1.[Menu_Imagem_GXI], TM1.[Menu_Ordem], T2.[Menu_Nome] AS Menu_PaiNom, T2.[Menu_Tipo] AS Menu_PaiTip, T2.[Menu_Ativo] AS Menu_PaiAti, TM1.[Menu_Ativo], TM1.[Menu_PaiCod] AS Menu_PaiCod, TM1.[Menu_Imagem] FROM ([Menu] TM1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = TM1.[Menu_PaiCod]) WHERE TM1.[Menu_Codigo] = @Menu_Codigo ORDER BY TM1.[Menu_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001B14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(6)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(6)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 30) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getMultimediaFile(13, rslt.getVarchar(6)) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getMultimediaUri(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 30) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getMultimediaFile(13, rslt.getVarchar(6)) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(6, (String)parms[9], (String)parms[7]);
                }
                stmt.SetParameter(7, (short)parms[10]);
                stmt.SetParameter(8, (bool)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[13]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (short)parms[6]);
                stmt.SetParameter(6, (bool)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                stmt.SetParameter(8, (int)parms[10]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
