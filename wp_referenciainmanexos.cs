/*
               File: WP_ReferenciaINMAnexos
        Description: Anexar Arquivo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:5.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_referenciainmanexos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_referenciainmanexos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_referenciainmanexos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ReferenciaINM_Codigo )
      {
         this.AV19ReferenciaINM_Codigo = aP0_ReferenciaINM_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavTipodocumento_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODOCUMENTO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODOCUMENTO_CODIGOTB2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV19ReferenciaINM_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ReferenciaINM_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV19ReferenciaINM_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATB2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTB2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042904585");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_referenciainmanexos.aspx") + "?" + UrlEncode("" +AV19ReferenciaINM_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vREFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vREFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV19ReferenciaINM_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vREFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV19ReferenciaINM_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV9Blob);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETB2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTB2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_referenciainmanexos.aspx") + "?" + UrlEncode("" +AV19ReferenciaINM_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ReferenciaINMAnexos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexar Arquivo" ;
      }

      protected void WBTB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TB2( true) ;
         }
         else
         {
            wb_table1_2_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p></p>") ;
         }
         wbLoad = true;
      }

      protected void STARTTB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Anexar Arquivo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTB0( ) ;
      }

      protected void WSTB2( )
      {
         STARTTB2( ) ;
         EVTTB2( ) ;
      }

      protected void EVTTB2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TB2 */
                              E11TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12TB2 */
                              E12TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13TB2 */
                              E13TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12TB2 */
                                    E12TB2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavTipodocumento_codigo.Name = "vTIPODOCUMENTO_CODIGO";
            dynavTipodocumento_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavBlob_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGOTB2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODOCUMENTO_CODIGO_dataTB2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODOCUMENTO_CODIGO_htmlTB2( )
      {
         int gxdynajaxvalue ;
         GXDLVvTIPODOCUMENTO_CODIGO_dataTB2( ) ;
         gxdynajaxindex = 1;
         dynavTipodocumento_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTipodocumento_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV20TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGO_dataTB2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00TB2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB2_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB2_A646TipoDocumento_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV20TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFTB2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13TB2 */
            E13TB2 ();
            WBTB0( ) ;
         }
      }

      protected void STRUPTB0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvTIPODOCUMENTO_CODIGO_htmlTB2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11TB2 */
         E11TB2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV9Blob = cgiGet( edtavBlob_Internalname);
            AV17Link = cgiGet( edtavLink_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Link", AV17Link);
            dynavTipodocumento_codigo.CurrentValue = cgiGet( dynavTipodocumento_codigo_Internalname);
            AV20TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTipodocumento_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0)));
            AV13Descricao = cgiGet( edtavDescricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Descricao", AV13Descricao);
            AV16FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FileName", AV16FileName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV9Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvTIPODOCUMENTO_CODIGO_htmlTB2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11TB2 */
         E11TB2 ();
         if (returnInSub) return;
      }

      protected void E11TB2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV22WWPContext) ;
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
      }

      protected void E12TB2( )
      {
         /* 'Enter' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) && String.IsNullOrEmpty(StringUtil.RTrim( AV17Link)) )
         {
            GX_msglist.addItem("Selecione um Arquivo ou informe um Link que deseja anexar!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Link)) )
         {
            GX_msglist.addItem("Arquivo e Link n�o podem ser anexados na mesma a��o!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Link)) && (0==AV20TipoDocumento_Codigo) )
         {
            GX_msglist.addItem("Informe o Tipo de Documento associado ao Link a ser anexado!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) && (0==AV20TipoDocumento_Codigo) )
         {
            GX_msglist.addItem("Informe o Tipo de Documento associado ao arquivo a ser anexado!");
         }
         else
         {
            AV7BC_ReferenciaINM.Load(AV19ReferenciaINM_Codigo);
            if ( AV7BC_ReferenciaINM.Success() )
            {
               AV8BC_ReferenciaINMAnexos = new SdtReferenciaINM_Anexos(context);
               AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_codigo = (int)(AV7BC_ReferenciaINM.gxTpr_Anexos.Count+1);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) )
               {
                  AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_arquivonome = AV17Link;
               }
               else
               {
                  AV6Arquivo = AV9Blob;
                  AV16FileName = StringUtil.Substring( AV16FileName, StringUtil.StringSearchRev( AV16FileName, "\\", -1)+1, 255);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FileName", AV16FileName);
                  AV14Ext = StringUtil.Trim( StringUtil.Substring( AV16FileName, StringUtil.StringSearchRev( AV16FileName, ".", -1)+1, 4));
                  AV16FileName = StringUtil.Substring( AV16FileName, 1, StringUtil.StringSearchRev( AV16FileName, ".", -1)-1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FileName", AV16FileName);
                  AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_arquivo = AV6Arquivo;
                  AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_arquivonome = AV16FileName;
                  AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_arquivotipo = AV14Ext;
               }
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Link)) )
               {
                  AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_link = AV17Link;
               }
               AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_descricao = AV13Descricao;
               AV8BC_ReferenciaINMAnexos.gxTpr_Referenciainmanexos_data = DateTimeUtil.ServerNow( context, "DEFAULT");
               AV8BC_ReferenciaINMAnexos.gxTpr_Tipodocumento_codigo = AV20TipoDocumento_Codigo;
               AV7BC_ReferenciaINM.gxTpr_Anexos.Add(AV8BC_ReferenciaINMAnexos, 0);
               AV7BC_ReferenciaINM.Save();
               if ( AV7BC_ReferenciaINM.Success() )
               {
                  context.CommitDataStores( "WP_ReferenciaINMAnexos");
                  context.setWebReturnParms(new Object[] {});
                  context.wjLocDisableFrm = 1;
                  context.nUserReturn = 1;
                  returnInSub = true;
                  if (true) return;
               }
               else
               {
                  context.RollbackDataStores( "WP_ReferenciaINMAnexos");
                  GX_msglist.addItem("Falha ao salvar o anexo.");
               }
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E13TB2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "left", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCell'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;height:24px")+"\" class='DataContentCell'>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) )
            {
               gxblobfileaux.Source = AV9Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV9Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV9Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV9Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV9Blob), context.PathToRelativeUrl( AV9Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV9Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 0, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "", "", "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:36px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Link:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLink_Internalname, AV17Link, AV17Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLink_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Tipo Documento:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodocumento_codigo, dynavTipodocumento_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0)), 1, dynavTipodocumento_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_ReferenciaINMAnexos.htm");
            dynavTipodocumento_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodocumento_codigo_Internalname, "Values", (String)(dynavTipodocumento_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Descri��o:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDescricao_Internalname, AV13Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", 0, 1, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:6px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, AV16FileName, StringUtil.RTrim( context.localUtil.Format( AV16FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "", "", "", edtavFilename_Visible, 1, 0, "text", "", 0, "px", 1, "px", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:36px")+"\">") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirm_Internalname, "", "Confirmar", bttConfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ReferenciaINMAnexos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Fechar", bttCancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_ReferenciaINMAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TB2e( true) ;
         }
         else
         {
            wb_table1_2_TB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV19ReferenciaINM_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ReferenciaINM_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV19ReferenciaINM_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATB2( ) ;
         WSTB2( ) ;
         WETB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042904618");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_referenciainmanexos.js", "?202042904618");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         edtavBlob_Internalname = "vBLOB";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavLink_Internalname = "vLINK";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavTipodocumento_codigo_Internalname = "vTIPODOCUMENTO_CODIGO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDescricao_Internalname = "vDESCRICAO";
         edtavFilename_Internalname = "vFILENAME";
         bttConfirm_Internalname = "CONFIRM";
         bttCancel_Internalname = "CANCEL";
         lblTbjava_Internalname = "TBJAVA";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTbjava_Visible = 1;
         edtavFilename_Jsonclick = "";
         dynavTipodocumento_codigo_Jsonclick = "";
         edtavLink_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         edtavFilename_Visible = 1;
         lblTbjava_Caption = "Script";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Anexar Arquivo";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'ENTER'","{handler:'E12TB2',iparms:[{av:'AV9Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV17Link',fld:'vLINK',pic:'',nv:''},{av:'AV20TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV16FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV13Descricao',fld:'vDESCRICAO',pic:'',nv:''}],oparms:[{av:'AV16FileName',fld:'vFILENAME',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         AV9Blob = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00TB2_A645TipoDocumento_Codigo = new int[1] ;
         H00TB2_A646TipoDocumento_Nome = new String[] {""} ;
         H00TB2_A647TipoDocumento_Ativo = new bool[] {false} ;
         AV17Link = "";
         AV13Descricao = "";
         AV16FileName = "";
         AV22WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV7BC_ReferenciaINM = new SdtReferenciaINM(context);
         AV8BC_ReferenciaINMAnexos = new SdtReferenciaINM_Anexos(context);
         AV6Arquivo = "";
         AV14Ext = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         bttConfirm_Jsonclick = "";
         bttCancel_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_referenciainmanexos__default(),
            new Object[][] {
                new Object[] {
               H00TB2_A645TipoDocumento_Codigo, H00TB2_A646TipoDocumento_Nome, H00TB2_A647TipoDocumento_Ativo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV19ReferenciaINM_Codigo ;
      private int wcpOAV19ReferenciaINM_Codigo ;
      private int gxdynajaxindex ;
      private int AV20TipoDocumento_Codigo ;
      private int lblTbjava_Visible ;
      private int edtavFilename_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBlob_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavLink_Internalname ;
      private String dynavTipodocumento_codigo_Internalname ;
      private String edtavDescricao_Internalname ;
      private String edtavFilename_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String AV6Arquivo ;
      private String AV14Ext ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavLink_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavTipodocumento_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavFilename_Jsonclick ;
      private String bttConfirm_Internalname ;
      private String bttConfirm_Jsonclick ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV17Link ;
      private String AV13Descricao ;
      private String AV16FileName ;
      private String AV9Blob ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavTipodocumento_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00TB2_A645TipoDocumento_Codigo ;
      private String[] H00TB2_A646TipoDocumento_Nome ;
      private bool[] H00TB2_A647TipoDocumento_Ativo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtReferenciaINM AV7BC_ReferenciaINM ;
      private SdtReferenciaINM_Anexos AV8BC_ReferenciaINMAnexos ;
      private wwpbaseobjects.SdtWWPContext AV22WWPContext ;
   }

   public class wp_referenciainmanexos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TB2 ;
          prmH00TB2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TB2", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Ativo] = 1 ORDER BY [TipoDocumento_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB2,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
