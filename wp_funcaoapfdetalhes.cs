/*
               File: WP_FuncaoAPFDetalhes
        Description: Detalhes da Fun��o de Transa��o:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:18:1.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_funcaoapfdetalhes : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_funcaoapfdetalhes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_funcaoapfdetalhes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_FuncaoAPF_SistemaCod )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A360FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n360FuncaoAPF_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAA42( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTA42( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282318111");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_funcaoapfdetalhes.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( "", context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_DESCRICAO", GetSecureSignedToken( "", A167FuncaoAPF_Descricao));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Webcomp1 == null ) )
         {
            WebComp_Webcomp1.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEA42( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTA42( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_funcaoapfdetalhes.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod) ;
      }

      public override String GetPgmname( )
      {
         return "WP_FuncaoAPFDetalhes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Detalhes da Fun��o de Transa��o:" ;
      }

      protected void WBA40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_A42( true) ;
         }
         else
         {
            wb_table1_2_A42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_A42e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTA42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Detalhes da Fun��o de Transa��o:", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPA40( ) ;
      }

      protected void WSA42( )
      {
         STARTA42( ) ;
         EVTA42( ) ;
      }

      protected void EVTA42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11A42 */
                              E11A42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12A42 */
                              E12A42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 53 )
                        {
                           OldWebcomp1 = cgiGet( "W0053");
                           if ( ( StringUtil.Len( OldWebcomp1) == 0 ) || ( StringUtil.StrCmp(OldWebcomp1, WebComp_Webcomp1_Component) != 0 ) )
                           {
                              WebComp_Webcomp1 = getWebComponent(GetType(), "GeneXus.Programs", OldWebcomp1, new Object[] {context} );
                              WebComp_Webcomp1.ComponentInit();
                              WebComp_Webcomp1.Name = "OldWebcomp1";
                              WebComp_Webcomp1_Component = OldWebcomp1;
                           }
                           WebComp_Webcomp1.componentprocess("W0053", "", sEvt);
                           WebComp_Webcomp1_Component = OldWebcomp1;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEA42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAA42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbFuncaoAPF_Tipo.Name = "FUNCAOAPF_TIPO";
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
            }
            cmbFuncaoAPF_Complexidade.Name = "FUNCAOAPF_COMPLEXIDADE";
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
         }
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFA42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFA42( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Webcomp1_Component, "") == 0 )
            {
               WebComp_Webcomp1 = getWebComponent(GetType(), "GeneXus.Programs", "funcaoapfatributosconsultawc", new Object[] {context} );
               WebComp_Webcomp1.ComponentInit();
               WebComp_Webcomp1.Name = "FuncaoAPFAtributosConsultaWC";
               WebComp_Webcomp1_Component = "FuncaoAPFAtributosConsultaWC";
            }
            WebComp_Webcomp1.setjustcreated();
            WebComp_Webcomp1.componentprepare(new Object[] {(String)"W0053",(String)"",(int)A165FuncaoAPF_Codigo});
            WebComp_Webcomp1.componentbind(new Object[] {(String)""});
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0053"+"");
               WebComp_Webcomp1.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            if ( 1 != 0 )
            {
               WebComp_Webcomp1.componentstart();
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00A42 */
            pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo, n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A167FuncaoAPF_Descricao = H00A42_A167FuncaoAPF_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_DESCRICAO", GetSecureSignedToken( "", A167FuncaoAPF_Descricao));
               A184FuncaoAPF_Tipo = H00A42_A184FuncaoAPF_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
               A166FuncaoAPF_Nome = H00A42_A166FuncaoAPF_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
               /* Execute user event: E12A42 */
               E12A42 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBA40( ) ;
         }
      }

      protected void STRUPA40( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXt_char1 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         A185FuncaoAPF_Complexidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GXt_int2 = A388FuncaoAPF_TD;
         new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         A388FuncaoAPF_TD = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GXt_int2 = A387FuncaoAPF_AR;
         new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         A387FuncaoAPF_AR = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GXt_decimal3 = A386FuncaoAPF_PF;
         new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         A386FuncaoAPF_PF = GXt_decimal3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( "", context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11A42 */
         E11A42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
            cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
            A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
            cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
            A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            A388FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_TD_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
            A387FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_AR_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
            A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( "", context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
            A167FuncaoAPF_Descricao = cgiGet( edtFuncaoAPF_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_DESCRICAO", GetSecureSignedToken( "", A167FuncaoAPF_Descricao));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11A42 */
         E11A42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11A42( )
      {
         /* Start Routine */
      }

      protected void nextLoad( )
      {
      }

      protected void E12A42( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_A42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_8_A42( true) ;
         }
         else
         {
            wb_table2_8_A42( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_A42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table3_20_A42( true) ;
         }
         else
         {
            wb_table3_20_A42( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_A42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_A42e( true) ;
         }
         else
         {
            wb_table1_2_A42e( false) ;
         }
      }

      protected void wb_table3_20_A42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle")+"\" class='Table'>") ;
            wb_table4_23_A42( true) ;
         }
         else
         {
            wb_table4_23_A42( false) ;
         }
         return  ;
      }

      protected void wb_table4_23_A42e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table5_45_A42( true) ;
         }
         else
         {
            wb_table5_45_A42( false) ;
         }
         return  ;
      }

      protected void wb_table5_45_A42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0053"+"", StringUtil.RTrim( WebComp_Webcomp1_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0053"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWebcomp1), StringUtil.Lower( WebComp_Webcomp1_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0053"+"");
               }
               WebComp_Webcomp1.componentdraw();
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWebcomp1), StringUtil.Lower( WebComp_Webcomp1_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_A42e( true) ;
         }
         else
         {
            wb_table3_20_A42e( false) ;
         }
      }

      protected void wb_table5_45_A42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;vertical-align:top;width:50px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo6_Internalname, "Descri��o:", "", "", lblTextblockfuncaodados_tipo6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Descricao_Internalname, A167FuncaoAPF_Descricao, "", "", 0, 1, 0, 0, 110, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", 0, true, "DescricaoLonga", "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_45_A42e( true) ;
         }
         else
         {
            wb_table5_45_A42e( false) ;
         }
      }

      protected void wb_table4_23_A42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;width:50px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo_Internalname, "Tipo:", "", "", lblTextblockfuncaodados_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Tipo, cmbFuncaoAPF_Tipo_Internalname, StringUtil.RTrim( A184FuncaoAPF_Tipo), 1, cmbFuncaoAPF_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "BootstrapAttribute", "", "", "", true, "HLP_WP_FuncaoAPFDetalhes.htm");
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo2_Internalname, "Complexidade:", "", "", lblTextblockfuncaodados_tipo2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Complexidade, cmbFuncaoAPF_Complexidade_Internalname, StringUtil.RTrim( A185FuncaoAPF_Complexidade), 1, cmbFuncaoAPF_Complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "Attribute", "", "", "", true, "HLP_WP_FuncaoAPFDetalhes.htm");
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo3_Internalname, "DER:", "", "", lblTextblockfuncaodados_tipo3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_TD_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_TD_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo4_Internalname, "RLR:", "", "", lblTextblockfuncaodados_tipo4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_AR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_AR_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo5_Internalname, "Pontos de Fun��o:", "", "", lblTextblockfuncaodados_tipo5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_PF_Internalname, StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")), context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_PF_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_23_A42e( true) ;
         }
         else
         {
            wb_table4_23_A42e( false) ;
         }
      }

      protected void wb_table2_8_A42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            wb_table6_12_A42( true) ;
         }
         else
         {
            wb_table6_12_A42( false) ;
         }
         return  ;
      }

      protected void wb_table6_12_A42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_A42e( true) ;
         }
         else
         {
            wb_table2_8_A42e( false) ;
         }
      }

      protected void wb_table6_12_A42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle2_Internalname, tblTablemergedviewtitle2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Fun��o de Transa��o ::", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:bold; font-style:normal;", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Multiple line edit */
            ClassString = "";
            StyleString = "font-family:'Calibri'; font-size:16.0pt; font-weight:bold; font-style:normal;";
            ClassString = "";
            StyleString = "font-family:'Calibri'; font-size:16.0pt; font-weight:bold; font-style:normal;";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", 1, "", "", -1, true, "", "HLP_WP_FuncaoAPFDetalhes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_12_A42e( true) ;
         }
         else
         {
            wb_table6_12_A42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         A360FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,1));
         n360FuncaoAPF_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_FUNCAOAPF_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAA42( ) ;
         WSA42( ) ;
         WEA42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( StringUtil.StrCmp(WebComp_Webcomp1_Component, "") == 0 )
         {
            WebComp_Webcomp1 = getWebComponent(GetType(), "GeneXus.Programs", "funcaoapfatributosconsultawc", new Object[] {context} );
            WebComp_Webcomp1.ComponentInit();
            WebComp_Webcomp1.Name = "FuncaoAPFAtributosConsultaWC";
            WebComp_Webcomp1_Component = "FuncaoAPFAtributosConsultaWC";
         }
         if ( ! ( WebComp_Webcomp1 == null ) )
         {
            WebComp_Webcomp1.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282318138");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_funcaoapfdetalhes.js", "?20204282318138");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         tblTablemergedviewtitle2_Internalname = "TABLEMERGEDVIEWTITLE2";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblTextblockfuncaodados_tipo_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO";
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO";
         lblTextblockfuncaodados_tipo2_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO2";
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE";
         lblTextblockfuncaodados_tipo3_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO3";
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD";
         lblTextblockfuncaodados_tipo4_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO4";
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR";
         lblTextblockfuncaodados_tipo5_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO5";
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF";
         tblTable1_Internalname = "TABLE1";
         lblTextblockfuncaodados_tipo6_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO6";
         edtFuncaoAPF_Descricao_Internalname = "FUNCAOAPF_DESCRICAO";
         tblTable2_Internalname = "TABLE2";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFuncaoAPF_Nome_Backcolor = (int)(0x0);
         edtFuncaoAPF_PF_Jsonclick = "";
         edtFuncaoAPF_AR_Jsonclick = "";
         edtFuncaoAPF_TD_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Detalhes da Fun��o de Transa��o:";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         A167FuncaoAPF_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldWebcomp1 = "";
         WebComp_Webcomp1_Component = "";
         scmdbuf = "";
         H00A42_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00A42_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00A42_A167FuncaoAPF_Descricao = new String[] {""} ;
         H00A42_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00A42_A166FuncaoAPF_Nome = new String[] {""} ;
         H00A42_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblockfuncaodados_tipo6_Jsonclick = "";
         lblTextblockfuncaodados_tipo_Jsonclick = "";
         lblTextblockfuncaodados_tipo2_Jsonclick = "";
         lblTextblockfuncaodados_tipo3_Jsonclick = "";
         lblTextblockfuncaodados_tipo4_Jsonclick = "";
         lblTextblockfuncaodados_tipo5_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_funcaoapfdetalhes__default(),
            new Object[][] {
                new Object[] {
               H00A42_A360FuncaoAPF_SistemaCod, H00A42_n360FuncaoAPF_SistemaCod, H00A42_A167FuncaoAPF_Descricao, H00A42_A184FuncaoAPF_Tipo, H00A42_A166FuncaoAPF_Nome, H00A42_A165FuncaoAPF_Codigo
               }
            }
         );
         WebComp_Webcomp1 = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short GXt_int2 ;
      private short nGXWrapped ;
      private int A165FuncaoAPF_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int wcpOA360FuncaoAPF_SistemaCod ;
      private int idxLst ;
      private int edtFuncaoAPF_Nome_Backcolor ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A184FuncaoAPF_Tipo ;
      private String A185FuncaoAPF_Complexidade ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldWebcomp1 ;
      private String WebComp_Webcomp1_Component ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String edtFuncaoAPF_TD_Internalname ;
      private String edtFuncaoAPF_AR_Internalname ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String edtFuncaoAPF_Descricao_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableattributes_Internalname ;
      private String tblTable2_Internalname ;
      private String lblTextblockfuncaodados_tipo6_Internalname ;
      private String lblTextblockfuncaodados_tipo6_Jsonclick ;
      private String tblTable1_Internalname ;
      private String lblTextblockfuncaodados_tipo_Internalname ;
      private String lblTextblockfuncaodados_tipo_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String lblTextblockfuncaodados_tipo2_Internalname ;
      private String lblTextblockfuncaodados_tipo2_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String lblTextblockfuncaodados_tipo3_Internalname ;
      private String lblTextblockfuncaodados_tipo3_Jsonclick ;
      private String edtFuncaoAPF_TD_Jsonclick ;
      private String lblTextblockfuncaodados_tipo4_Internalname ;
      private String lblTextblockfuncaodados_tipo4_Jsonclick ;
      private String edtFuncaoAPF_AR_Jsonclick ;
      private String lblTextblockfuncaodados_tipo5_Internalname ;
      private String lblTextblockfuncaodados_tipo5_Jsonclick ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String tblTablemergedviewtitle2_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String A167FuncaoAPF_Descricao ;
      private String A166FuncaoAPF_Nome ;
      private GXWebComponent WebComp_Webcomp1 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private IDataStoreProvider pr_default ;
      private int[] H00A42_A360FuncaoAPF_SistemaCod ;
      private bool[] H00A42_n360FuncaoAPF_SistemaCod ;
      private String[] H00A42_A167FuncaoAPF_Descricao ;
      private String[] H00A42_A184FuncaoAPF_Tipo ;
      private String[] H00A42_A166FuncaoAPF_Nome ;
      private int[] H00A42_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class wp_funcaoapfdetalhes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00A42 ;
          prmH00A42 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00A42", "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Descricao], [FuncaoAPF_Tipo], [FuncaoAPF_Nome], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND ([FuncaoAPF_SistemaCod] = @FuncaoAPF_SistemaCod) ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A42,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
       }
    }

 }

}
