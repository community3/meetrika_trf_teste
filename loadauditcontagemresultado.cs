/*
               File: LoadAuditContagemResultado
        Description: Load Audit Contagem Resultado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 2:2:10.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontagemresultado : GXProcedure
   {
      public loadauditcontagemresultado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontagemresultado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_ContagemResultado_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16ContagemResultado_Codigo = aP2_ContagemResultado_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_ContagemResultado_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditcontagemresultado objloadauditcontagemresultado;
         objloadauditcontagemresultado = new loadauditcontagemresultado();
         objloadauditcontagemresultado.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontagemresultado.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontagemresultado.AV16ContagemResultado_Codigo = aP2_ContagemResultado_Codigo;
         objloadauditcontagemresultado.AV14ActualMode = aP3_ActualMode;
         objloadauditcontagemresultado.context.SetSubmitInitialConfig(context);
         objloadauditcontagemresultado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontagemresultado);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontagemresultado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A812 */
         pr_default.execute(0, new Object[] {n52Contratada_AreaTrabalhoCod, A52Contratada_AreaTrabalhoCod, AV16ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1600ContagemResultado_CntSrvSttPgmFnc = P00A812_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = P00A812_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A1601ContagemResultado_CntSrvIndDvr = P00A812_A1601ContagemResultado_CntSrvIndDvr[0];
            n1601ContagemResultado_CntSrvIndDvr = P00A812_n1601ContagemResultado_CntSrvIndDvr[0];
            A1613ContagemResultado_CntSrvQtdUntCns = P00A812_A1613ContagemResultado_CntSrvQtdUntCns[0];
            n1613ContagemResultado_CntSrvQtdUntCns = P00A812_n1613ContagemResultado_CntSrvQtdUntCns[0];
            A1620ContagemResultado_CntSrvUndCnt = P00A812_A1620ContagemResultado_CntSrvUndCnt[0];
            n1620ContagemResultado_CntSrvUndCnt = P00A812_n1620ContagemResultado_CntSrvUndCnt[0];
            A1621ContagemResultado_CntSrvUndCntSgl = P00A812_A1621ContagemResultado_CntSrvUndCntSgl[0];
            n1621ContagemResultado_CntSrvUndCntSgl = P00A812_n1621ContagemResultado_CntSrvUndCntSgl[0];
            A2209ContagemResultado_CntSrvUndCntNome = P00A812_A2209ContagemResultado_CntSrvUndCntNome[0];
            n2209ContagemResultado_CntSrvUndCntNome = P00A812_n2209ContagemResultado_CntSrvUndCntNome[0];
            A1623ContagemResultado_CntSrvMmn = P00A812_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P00A812_n1623ContagemResultado_CntSrvMmn[0];
            A1607ContagemResultado_PrzAnl = P00A812_A1607ContagemResultado_PrzAnl[0];
            n1607ContagemResultado_PrzAnl = P00A812_n1607ContagemResultado_PrzAnl[0];
            A1609ContagemResultado_PrzCrr = P00A812_A1609ContagemResultado_PrzCrr[0];
            n1609ContagemResultado_PrzCrr = P00A812_n1609ContagemResultado_PrzCrr[0];
            A1618ContagemResultado_PrzRsp = P00A812_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P00A812_n1618ContagemResultado_PrzRsp[0];
            A1619ContagemResultado_PrzGrt = P00A812_A1619ContagemResultado_PrzGrt[0];
            n1619ContagemResultado_PrzGrt = P00A812_n1619ContagemResultado_PrzGrt[0];
            A1611ContagemResultado_PrzTpDias = P00A812_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00A812_n1611ContagemResultado_PrzTpDias[0];
            A1603ContagemResultado_CntCod = P00A812_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00A812_n1603ContagemResultado_CntCod[0];
            A1612ContagemResultado_CntNum = P00A812_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00A812_n1612ContagemResultado_CntNum[0];
            A1604ContagemResultado_CntPrpCod = P00A812_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P00A812_n1604ContagemResultado_CntPrpCod[0];
            A1605ContagemResultado_CntPrpPesCod = P00A812_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P00A812_n1605ContagemResultado_CntPrpPesCod[0];
            A1606ContagemResultado_CntPrpPesNom = P00A812_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P00A812_n1606ContagemResultado_CntPrpPesNom[0];
            A1615ContagemResultado_CntIndDvr = P00A812_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = P00A812_n1615ContagemResultado_CntIndDvr[0];
            A1617ContagemResultado_CntClcDvr = P00A812_A1617ContagemResultado_CntClcDvr[0];
            n1617ContagemResultado_CntClcDvr = P00A812_n1617ContagemResultado_CntClcDvr[0];
            A1616ContagemResultado_CntVlrUndCnt = P00A812_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00A812_n1616ContagemResultado_CntVlrUndCnt[0];
            A1632ContagemResultado_CntPrdFtrCada = P00A812_A1632ContagemResultado_CntPrdFtrCada[0];
            n1632ContagemResultado_CntPrdFtrCada = P00A812_n1632ContagemResultado_CntPrdFtrCada[0];
            A1633ContagemResultado_CntPrdFtrIni = P00A812_A1633ContagemResultado_CntPrdFtrIni[0];
            n1633ContagemResultado_CntPrdFtrIni = P00A812_n1633ContagemResultado_CntPrdFtrIni[0];
            A1634ContagemResultado_CntPrdFtrFim = P00A812_A1634ContagemResultado_CntPrdFtrFim[0];
            n1634ContagemResultado_CntPrdFtrFim = P00A812_n1634ContagemResultado_CntPrdFtrFim[0];
            A2082ContagemResultado_CntLmtFtr = P00A812_A2082ContagemResultado_CntLmtFtr[0];
            n2082ContagemResultado_CntLmtFtr = P00A812_n2082ContagemResultado_CntLmtFtr[0];
            A1624ContagemResultado_DatVgnInc = P00A812_A1624ContagemResultado_DatVgnInc[0];
            n1624ContagemResultado_DatVgnInc = P00A812_n1624ContagemResultado_DatVgnInc[0];
            A1598ContagemResultado_ServicoNome = P00A812_A1598ContagemResultado_ServicoNome[0];
            n1598ContagemResultado_ServicoNome = P00A812_n1598ContagemResultado_ServicoNome[0];
            A1599ContagemResultado_ServicoTpHrrq = P00A812_A1599ContagemResultado_ServicoTpHrrq[0];
            n1599ContagemResultado_ServicoTpHrrq = P00A812_n1599ContagemResultado_ServicoTpHrrq[0];
            A1591ContagemResultado_CodSrvVnc = P00A812_A1591ContagemResultado_CodSrvVnc[0];
            n1591ContagemResultado_CodSrvVnc = P00A812_n1591ContagemResultado_CodSrvVnc[0];
            A1590ContagemResultado_SiglaSrvVnc = P00A812_A1590ContagemResultado_SiglaSrvVnc[0];
            n1590ContagemResultado_SiglaSrvVnc = P00A812_n1590ContagemResultado_SiglaSrvVnc[0];
            A1765ContagemResultado_CodSrvSSVnc = P00A812_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00A812_n1765ContagemResultado_CodSrvSSVnc[0];
            A1627ContagemResultado_CntSrvVncCod = P00A812_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = P00A812_n1627ContagemResultado_CntSrvVncCod[0];
            A1622ContagemResultado_CntVncCod = P00A812_A1622ContagemResultado_CntVncCod[0];
            n1622ContagemResultado_CntVncCod = P00A812_n1622ContagemResultado_CntVncCod[0];
            A2136ContagemResultado_CntadaOsVinc = P00A812_A2136ContagemResultado_CntadaOsVinc[0];
            n2136ContagemResultado_CntadaOsVinc = P00A812_n2136ContagemResultado_CntadaOsVinc[0];
            A2137ContagemResultado_SerGrupoVinc = P00A812_A2137ContagemResultado_SerGrupoVinc[0];
            n2137ContagemResultado_SerGrupoVinc = P00A812_n2137ContagemResultado_SerGrupoVinc[0];
            A1583ContagemResultado_TipoRegistro = P00A812_A1583ContagemResultado_TipoRegistro[0];
            A1584ContagemResultado_UOOwner = P00A812_A1584ContagemResultado_UOOwner[0];
            n1584ContagemResultado_UOOwner = P00A812_n1584ContagemResultado_UOOwner[0];
            A1585ContagemResultado_Referencia = P00A812_A1585ContagemResultado_Referencia[0];
            n1585ContagemResultado_Referencia = P00A812_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = P00A812_A1586ContagemResultado_Restricoes[0];
            n1586ContagemResultado_Restricoes = P00A812_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = P00A812_A1587ContagemResultado_PrioridadePrevista[0];
            n1587ContagemResultado_PrioridadePrevista = P00A812_n1587ContagemResultado_PrioridadePrevista[0];
            A1650ContagemResultado_PrzInc = P00A812_A1650ContagemResultado_PrzInc[0];
            n1650ContagemResultado_PrzInc = P00A812_n1650ContagemResultado_PrzInc[0];
            A1714ContagemResultado_Combinada = P00A812_A1714ContagemResultado_Combinada[0];
            n1714ContagemResultado_Combinada = P00A812_n1714ContagemResultado_Combinada[0];
            A1762ContagemResultado_Entrega = P00A812_A1762ContagemResultado_Entrega[0];
            n1762ContagemResultado_Entrega = P00A812_n1762ContagemResultado_Entrega[0];
            A1791ContagemResultado_SemCusto = P00A812_A1791ContagemResultado_SemCusto[0];
            n1791ContagemResultado_SemCusto = P00A812_n1791ContagemResultado_SemCusto[0];
            A1854ContagemResultado_VlrCnc = P00A812_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00A812_n1854ContagemResultado_VlrCnc[0];
            A1855ContagemResultado_PFCnc = P00A812_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P00A812_n1855ContagemResultado_PFCnc[0];
            A1903ContagemResultado_DataPrvPgm = P00A812_A1903ContagemResultado_DataPrvPgm[0];
            n1903ContagemResultado_DataPrvPgm = P00A812_n1903ContagemResultado_DataPrvPgm[0];
            A2133ContagemResultado_QuantidadeSolicitada = P00A812_A2133ContagemResultado_QuantidadeSolicitada[0];
            n2133ContagemResultado_QuantidadeSolicitada = P00A812_n2133ContagemResultado_QuantidadeSolicitada[0];
            A531ContagemResultado_StatusUltCnt = P00A812_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P00A812_A566ContagemResultado_DataUltCnt[0];
            A825ContagemResultado_HoraUltCnt = P00A812_A825ContagemResultado_HoraUltCnt[0];
            A578ContagemResultado_CntComNaoCnf = P00A812_A578ContagemResultado_CntComNaoCnf[0];
            n578ContagemResultado_CntComNaoCnf = P00A812_n578ContagemResultado_CntComNaoCnf[0];
            A584ContagemResultado_ContadorFM = P00A812_A584ContagemResultado_ContadorFM[0];
            A682ContagemResultado_PFBFMUltima = P00A812_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = P00A812_A683ContagemResultado_PFLFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00A812_A684ContagemResultado_PFBFSUltima[0];
            A1480ContagemResultado_CstUntUltima = P00A812_A1480ContagemResultado_CstUntUltima[0];
            A685ContagemResultado_PFLFSUltima = P00A812_A685ContagemResultado_PFLFSUltima[0];
            A802ContagemResultado_DeflatorCnt = P00A812_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00A812_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00A812_n510ContagemResultado_EsforcoSoma[0];
            A2118ContagemResultado_Owner_Identificao = P00A812_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00A812_n2118ContagemResultado_Owner_Identificao[0];
            A1353ContagemResultado_ContratadaDoOwner = P00A812_A1353ContagemResultado_ContratadaDoOwner[0];
            n1353ContagemResultado_ContratadaDoOwner = P00A812_n1353ContagemResultado_ContratadaDoOwner[0];
            A1608ContagemResultado_PrzExc = P00A812_A1608ContagemResultado_PrzExc[0];
            n1608ContagemResultado_PrzExc = P00A812_n1608ContagemResultado_PrzExc[0];
            A1610ContagemResultado_PrzTp = P00A812_A1610ContagemResultado_PrzTp[0];
            n1610ContagemResultado_PrzTp = P00A812_n1610ContagemResultado_PrzTp[0];
            A1625ContagemResultado_DatIncTA = P00A812_A1625ContagemResultado_DatIncTA[0];
            n1625ContagemResultado_DatIncTA = P00A812_n1625ContagemResultado_DatIncTA[0];
            A1602ContagemResultado_GlsIndValor = P00A812_A1602ContagemResultado_GlsIndValor[0];
            n1602ContagemResultado_GlsIndValor = P00A812_n1602ContagemResultado_GlsIndValor[0];
            A1701ContagemResultado_TemProposta = P00A812_A1701ContagemResultado_TemProposta[0];
            n1701ContagemResultado_TemProposta = P00A812_n1701ContagemResultado_TemProposta[0];
            A1443ContagemResultado_CntSrvPrrCod = P00A812_A1443ContagemResultado_CntSrvPrrCod[0];
            n1443ContagemResultado_CntSrvPrrCod = P00A812_n1443ContagemResultado_CntSrvPrrCod[0];
            A1043ContagemResultado_LiqLogCod = P00A812_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = P00A812_n1043ContagemResultado_LiqLogCod[0];
            A512ContagemResultado_ValorPF = P00A812_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00A812_n512ContagemResultado_ValorPF[0];
            A52Contratada_AreaTrabalhoCod = P00A812_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00A812_n52Contratada_AreaTrabalhoCod[0];
            A890ContagemResultado_Responsavel = P00A812_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00A812_n890ContagemResultado_Responsavel[0];
            A508ContagemResultado_Owner = P00A812_A508ContagemResultado_Owner[0];
            A456ContagemResultado_Codigo = P00A812_A456ContagemResultado_Codigo[0];
            A457ContagemResultado_Demanda = P00A812_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00A812_n457ContagemResultado_Demanda[0];
            A803ContagemResultado_ContratadaSigla = P00A812_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00A812_n803ContagemResultado_ContratadaSigla[0];
            A801ContagemResultado_ServicoSigla = P00A812_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00A812_n801ContagemResultado_ServicoSigla[0];
            A493ContagemResultado_DemandaFM = P00A812_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00A812_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = P00A812_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00A812_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P00A812_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00A812_n1553ContagemResultado_CntSrvCod[0];
            A471ContagemResultado_DataDmn = P00A812_A471ContagemResultado_DataDmn[0];
            A1790ContagemResultado_DataInicio = P00A812_A1790ContagemResultado_DataInicio[0];
            n1790ContagemResultado_DataInicio = P00A812_n1790ContagemResultado_DataInicio[0];
            A472ContagemResultado_DataEntrega = P00A812_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00A812_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00A812_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00A812_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = P00A812_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00A812_n1351ContagemResultado_DataPrevista[0];
            A1349ContagemResultado_DataExecucao = P00A812_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00A812_n1349ContagemResultado_DataExecucao[0];
            A2017ContagemResultado_DataEntregaReal = P00A812_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00A812_n2017ContagemResultado_DataEntregaReal[0];
            A1348ContagemResultado_DataHomologacao = P00A812_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P00A812_n1348ContagemResultado_DataHomologacao[0];
            A1227ContagemResultado_PrazoInicialDias = P00A812_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P00A812_n1227ContagemResultado_PrazoInicialDias[0];
            A1237ContagemResultado_PrazoMaisDias = P00A812_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P00A812_n1237ContagemResultado_PrazoMaisDias[0];
            A499ContagemResultado_ContratadaPessoaCod = P00A812_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00A812_n499ContagemResultado_ContratadaPessoaCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00A812_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00A812_n500ContagemResultado_ContratadaPessoaNom[0];
            A1326ContagemResultado_ContratadaTipoFab = P00A812_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00A812_n1326ContagemResultado_ContratadaTipoFab[0];
            A1817ContagemResultado_ContratadaCNPJ = P00A812_A1817ContagemResultado_ContratadaCNPJ[0];
            n1817ContagemResultado_ContratadaCNPJ = P00A812_n1817ContagemResultado_ContratadaCNPJ[0];
            A805ContagemResultado_ContratadaOrigemCod = P00A812_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P00A812_n805ContagemResultado_ContratadaOrigemCod[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00A812_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00A812_n866ContagemResultado_ContratadaOrigemSigla[0];
            A807ContagemResultado_ContratadaOrigemPesCod = P00A812_A807ContagemResultado_ContratadaOrigemPesCod[0];
            n807ContagemResultado_ContratadaOrigemPesCod = P00A812_n807ContagemResultado_ContratadaOrigemPesCod[0];
            A808ContagemResultado_ContratadaOrigemPesNom = P00A812_A808ContagemResultado_ContratadaOrigemPesNom[0];
            n808ContagemResultado_ContratadaOrigemPesNom = P00A812_n808ContagemResultado_ContratadaOrigemPesNom[0];
            A867ContagemResultado_ContratadaOrigemAreaCod = P00A812_A867ContagemResultado_ContratadaOrigemAreaCod[0];
            n867ContagemResultado_ContratadaOrigemAreaCod = P00A812_n867ContagemResultado_ContratadaOrigemAreaCod[0];
            A2093ContagemResultado_ContratadaOrigemUsaSistema = P00A812_A2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            n2093ContagemResultado_ContratadaOrigemUsaSistema = P00A812_n2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            A454ContagemResultado_ContadorFSCod = P00A812_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = P00A812_n454ContagemResultado_ContadorFSCod[0];
            A480ContagemResultado_CrFSPessoaCod = P00A812_A480ContagemResultado_CrFSPessoaCod[0];
            n480ContagemResultado_CrFSPessoaCod = P00A812_n480ContagemResultado_CrFSPessoaCod[0];
            A455ContagemResultado_ContadorFSNom = P00A812_A455ContagemResultado_ContadorFSNom[0];
            n455ContagemResultado_ContadorFSNom = P00A812_n455ContagemResultado_ContadorFSNom[0];
            A1452ContagemResultado_SS = P00A812_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P00A812_n1452ContagemResultado_SS[0];
            A1173ContagemResultado_OSManual = P00A812_A1173ContagemResultado_OSManual[0];
            n1173ContagemResultado_OSManual = P00A812_n1173ContagemResultado_OSManual[0];
            A494ContagemResultado_Descricao = P00A812_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00A812_n494ContagemResultado_Descricao[0];
            A465ContagemResultado_Link = P00A812_A465ContagemResultado_Link[0];
            n465ContagemResultado_Link = P00A812_n465ContagemResultado_Link[0];
            A489ContagemResultado_SistemaCod = P00A812_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00A812_n489ContagemResultado_SistemaCod[0];
            A495ContagemResultado_SistemaNom = P00A812_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P00A812_n495ContagemResultado_SistemaNom[0];
            A496Contagemresultado_SistemaAreaCod = P00A812_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P00A812_n496Contagemresultado_SistemaAreaCod[0];
            A509ContagemrResultado_SistemaSigla = P00A812_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00A812_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P00A812_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00A812_n515ContagemResultado_SistemaCoord[0];
            A804ContagemResultado_SistemaAtivo = P00A812_A804ContagemResultado_SistemaAtivo[0];
            n804ContagemResultado_SistemaAtivo = P00A812_n804ContagemResultado_SistemaAtivo[0];
            A146Modulo_Codigo = P00A812_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00A812_n146Modulo_Codigo[0];
            A143Modulo_Nome = P00A812_A143Modulo_Nome[0];
            A145Modulo_Sigla = P00A812_A145Modulo_Sigla[0];
            A468ContagemResultado_NaoCnfDmnCod = P00A812_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00A812_n468ContagemResultado_NaoCnfDmnCod[0];
            A477ContagemResultado_NaoCnfDmnNom = P00A812_A477ContagemResultado_NaoCnfDmnNom[0];
            n477ContagemResultado_NaoCnfDmnNom = P00A812_n477ContagemResultado_NaoCnfDmnNom[0];
            A2030ContagemResultado_NaoCnfDmnGls = P00A812_A2030ContagemResultado_NaoCnfDmnGls[0];
            n2030ContagemResultado_NaoCnfDmnGls = P00A812_n2030ContagemResultado_NaoCnfDmnGls[0];
            A484ContagemResultado_StatusDmn = P00A812_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00A812_n484ContagemResultado_StatusDmn[0];
            A771ContagemResultado_StatusDmnVnc = P00A812_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00A812_n771ContagemResultado_StatusDmnVnc[0];
            A601ContagemResultado_Servico = P00A812_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00A812_n601ContagemResultado_Servico[0];
            A2008ContagemResultado_CntSrvAls = P00A812_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00A812_n2008ContagemResultado_CntSrvAls[0];
            A764ContagemResultado_ServicoGrupo = P00A812_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00A812_n764ContagemResultado_ServicoGrupo[0];
            A1062ContagemResultado_ServicoTela = P00A812_A1062ContagemResultado_ServicoTela[0];
            n1062ContagemResultado_ServicoTela = P00A812_n1062ContagemResultado_ServicoTela[0];
            A897ContagemResultado_ServicoAtivo = P00A812_A897ContagemResultado_ServicoAtivo[0];
            n897ContagemResultado_ServicoAtivo = P00A812_n897ContagemResultado_ServicoAtivo[0];
            A1398ContagemResultado_ServicoNaoRqrAtr = P00A812_A1398ContagemResultado_ServicoNaoRqrAtr[0];
            n1398ContagemResultado_ServicoNaoRqrAtr = P00A812_n1398ContagemResultado_ServicoNaoRqrAtr[0];
            A1636ContagemResultado_ServicoSS = P00A812_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = P00A812_n1636ContagemResultado_ServicoSS[0];
            A1637ContagemResultado_ServicoSSSgl = P00A812_A1637ContagemResultado_ServicoSSSgl[0];
            n1637ContagemResultado_ServicoSSSgl = P00A812_n1637ContagemResultado_ServicoSSSgl[0];
            A2050ContagemResultado_SrvLnhNegCod = P00A812_A2050ContagemResultado_SrvLnhNegCod[0];
            n2050ContagemResultado_SrvLnhNegCod = P00A812_n2050ContagemResultado_SrvLnhNegCod[0];
            A485ContagemResultado_EhValidacao = P00A812_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = P00A812_n485ContagemResultado_EhValidacao[0];
            A514ContagemResultado_Observacao = P00A812_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = P00A812_n514ContagemResultado_Observacao[0];
            A592ContagemResultado_Evidencia = P00A812_A592ContagemResultado_Evidencia[0];
            n592ContagemResultado_Evidencia = P00A812_n592ContagemResultado_Evidencia[0];
            A1350ContagemResultado_DataCadastro = P00A812_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = P00A812_n1350ContagemResultado_DataCadastro[0];
            A1312ContagemResultado_ResponsavelPessCod = P00A812_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = P00A812_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = P00A812_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = P00A812_n1313ContagemResultado_ResponsavelPessNome[0];
            A1180ContagemResultado_Custo = P00A812_A1180ContagemResultado_Custo[0];
            n1180ContagemResultado_Custo = P00A812_n1180ContagemResultado_Custo[0];
            A597ContagemResultado_LoteAceiteCod = P00A812_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00A812_n597ContagemResultado_LoteAceiteCod[0];
            A528ContagemResultado_LoteAceite = P00A812_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00A812_n528ContagemResultado_LoteAceite[0];
            A529ContagemResultado_DataAceite = P00A812_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00A812_n529ContagemResultado_DataAceite[0];
            A525ContagemResultado_AceiteUserCod = P00A812_A525ContagemResultado_AceiteUserCod[0];
            n525ContagemResultado_AceiteUserCod = P00A812_n525ContagemResultado_AceiteUserCod[0];
            A526ContagemResultado_AceitePessoaCod = P00A812_A526ContagemResultado_AceitePessoaCod[0];
            n526ContagemResultado_AceitePessoaCod = P00A812_n526ContagemResultado_AceitePessoaCod[0];
            A527ContagemResultado_AceiteUserNom = P00A812_A527ContagemResultado_AceiteUserNom[0];
            n527ContagemResultado_AceiteUserNom = P00A812_n527ContagemResultado_AceiteUserNom[0];
            A1547ContagemResultado_LoteNFe = P00A812_A1547ContagemResultado_LoteNFe[0];
            n1547ContagemResultado_LoteNFe = P00A812_n1547ContagemResultado_LoteNFe[0];
            A1559ContagemResultado_VlrAceite = P00A812_A1559ContagemResultado_VlrAceite[0];
            n1559ContagemResultado_VlrAceite = P00A812_n1559ContagemResultado_VlrAceite[0];
            A598ContagemResultado_Baseline = P00A812_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00A812_n598ContagemResultado_Baseline[0];
            A602ContagemResultado_OSVinculada = P00A812_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00A812_n602ContagemResultado_OSVinculada[0];
            A603ContagemResultado_DmnVinculada = P00A812_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00A812_n603ContagemResultado_DmnVinculada[0];
            A798ContagemResultado_PFBFSImp = P00A812_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = P00A812_n798ContagemResultado_PFBFSImp[0];
            A799ContagemResultado_PFLFSImp = P00A812_A799ContagemResultado_PFLFSImp[0];
            n799ContagemResultado_PFLFSImp = P00A812_n799ContagemResultado_PFLFSImp[0];
            A1178ContagemResultado_PBFinal = P00A812_A1178ContagemResultado_PBFinal[0];
            n1178ContagemResultado_PBFinal = P00A812_n1178ContagemResultado_PBFinal[0];
            A1179ContagemResultado_PLFinal = P00A812_A1179ContagemResultado_PLFinal[0];
            n1179ContagemResultado_PLFinal = P00A812_n1179ContagemResultado_PLFinal[0];
            A1034ContagemResultadoLiqLog_Data = P00A812_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = P00A812_n1034ContagemResultadoLiqLog_Data[0];
            A1044ContagemResultado_FncUsrCod = P00A812_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = P00A812_n1044ContagemResultado_FncUsrCod[0];
            A1046ContagemResultado_Agrupador = P00A812_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00A812_n1046ContagemResultado_Agrupador[0];
            A1049ContagemResultado_GlsData = P00A812_A1049ContagemResultado_GlsData[0];
            n1049ContagemResultado_GlsData = P00A812_n1049ContagemResultado_GlsData[0];
            A1050ContagemResultado_GlsDescricao = P00A812_A1050ContagemResultado_GlsDescricao[0];
            n1050ContagemResultado_GlsDescricao = P00A812_n1050ContagemResultado_GlsDescricao[0];
            A1051ContagemResultado_GlsValor = P00A812_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P00A812_n1051ContagemResultado_GlsValor[0];
            A1052ContagemResultado_GlsUser = P00A812_A1052ContagemResultado_GlsUser[0];
            n1052ContagemResultado_GlsUser = P00A812_n1052ContagemResultado_GlsUser[0];
            A1389ContagemResultado_RdmnIssueId = P00A812_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = P00A812_n1389ContagemResultado_RdmnIssueId[0];
            A1390ContagemResultado_RdmnProjectId = P00A812_A1390ContagemResultado_RdmnProjectId[0];
            n1390ContagemResultado_RdmnProjectId = P00A812_n1390ContagemResultado_RdmnProjectId[0];
            A1392ContagemResultado_RdmnUpdated = P00A812_A1392ContagemResultado_RdmnUpdated[0];
            n1392ContagemResultado_RdmnUpdated = P00A812_n1392ContagemResultado_RdmnUpdated[0];
            A1444ContagemResultado_CntSrvPrrPrz = P00A812_A1444ContagemResultado_CntSrvPrrPrz[0];
            n1444ContagemResultado_CntSrvPrrPrz = P00A812_n1444ContagemResultado_CntSrvPrrPrz[0];
            A1445ContagemResultado_CntSrvPrrCst = P00A812_A1445ContagemResultado_CntSrvPrrCst[0];
            n1445ContagemResultado_CntSrvPrrCst = P00A812_n1445ContagemResultado_CntSrvPrrCst[0];
            A1457ContagemResultado_TemDpnHmlg = P00A812_A1457ContagemResultado_TemDpnHmlg[0];
            n1457ContagemResultado_TemDpnHmlg = P00A812_n1457ContagemResultado_TemDpnHmlg[0];
            A1519ContagemResultado_TmpEstAnl = P00A812_A1519ContagemResultado_TmpEstAnl[0];
            n1519ContagemResultado_TmpEstAnl = P00A812_n1519ContagemResultado_TmpEstAnl[0];
            A1505ContagemResultado_TmpEstExc = P00A812_A1505ContagemResultado_TmpEstExc[0];
            n1505ContagemResultado_TmpEstExc = P00A812_n1505ContagemResultado_TmpEstExc[0];
            A1506ContagemResultado_TmpEstCrr = P00A812_A1506ContagemResultado_TmpEstCrr[0];
            n1506ContagemResultado_TmpEstCrr = P00A812_n1506ContagemResultado_TmpEstCrr[0];
            A1520ContagemResultado_InicioAnl = P00A812_A1520ContagemResultado_InicioAnl[0];
            n1520ContagemResultado_InicioAnl = P00A812_n1520ContagemResultado_InicioAnl[0];
            A1521ContagemResultado_FimAnl = P00A812_A1521ContagemResultado_FimAnl[0];
            n1521ContagemResultado_FimAnl = P00A812_n1521ContagemResultado_FimAnl[0];
            A1509ContagemResultado_InicioExc = P00A812_A1509ContagemResultado_InicioExc[0];
            n1509ContagemResultado_InicioExc = P00A812_n1509ContagemResultado_InicioExc[0];
            A1510ContagemResultado_FimExc = P00A812_A1510ContagemResultado_FimExc[0];
            n1510ContagemResultado_FimExc = P00A812_n1510ContagemResultado_FimExc[0];
            A1511ContagemResultado_InicioCrr = P00A812_A1511ContagemResultado_InicioCrr[0];
            n1511ContagemResultado_InicioCrr = P00A812_n1511ContagemResultado_InicioCrr[0];
            A1512ContagemResultado_FimCrr = P00A812_A1512ContagemResultado_FimCrr[0];
            n1512ContagemResultado_FimCrr = P00A812_n1512ContagemResultado_FimCrr[0];
            A1515ContagemResultado_Evento = P00A812_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = P00A812_n1515ContagemResultado_Evento[0];
            A1544ContagemResultado_ProjetoCod = P00A812_A1544ContagemResultado_ProjetoCod[0];
            n1544ContagemResultado_ProjetoCod = P00A812_n1544ContagemResultado_ProjetoCod[0];
            A1593ContagemResultado_CntSrvTpVnc = P00A812_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00A812_n1593ContagemResultado_CntSrvTpVnc[0];
            A1594ContagemResultado_CntSrvFtrm = P00A812_A1594ContagemResultado_CntSrvFtrm[0];
            n1594ContagemResultado_CntSrvFtrm = P00A812_n1594ContagemResultado_CntSrvFtrm[0];
            A1596ContagemResultado_CntSrvPrc = P00A812_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00A812_n1596ContagemResultado_CntSrvPrc[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00A812_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00A812_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00A812_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00A812_n866ContagemResultado_ContratadaOrigemSigla[0];
            A807ContagemResultado_ContratadaOrigemPesCod = P00A812_A807ContagemResultado_ContratadaOrigemPesCod[0];
            n807ContagemResultado_ContratadaOrigemPesCod = P00A812_n807ContagemResultado_ContratadaOrigemPesCod[0];
            A867ContagemResultado_ContratadaOrigemAreaCod = P00A812_A867ContagemResultado_ContratadaOrigemAreaCod[0];
            n867ContagemResultado_ContratadaOrigemAreaCod = P00A812_n867ContagemResultado_ContratadaOrigemAreaCod[0];
            A2093ContagemResultado_ContratadaOrigemUsaSistema = P00A812_A2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            n2093ContagemResultado_ContratadaOrigemUsaSistema = P00A812_n2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            A808ContagemResultado_ContratadaOrigemPesNom = P00A812_A808ContagemResultado_ContratadaOrigemPesNom[0];
            n808ContagemResultado_ContratadaOrigemPesNom = P00A812_n808ContagemResultado_ContratadaOrigemPesNom[0];
            A480ContagemResultado_CrFSPessoaCod = P00A812_A480ContagemResultado_CrFSPessoaCod[0];
            n480ContagemResultado_CrFSPessoaCod = P00A812_n480ContagemResultado_CrFSPessoaCod[0];
            A455ContagemResultado_ContadorFSNom = P00A812_A455ContagemResultado_ContadorFSNom[0];
            n455ContagemResultado_ContadorFSNom = P00A812_n455ContagemResultado_ContadorFSNom[0];
            A495ContagemResultado_SistemaNom = P00A812_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P00A812_n495ContagemResultado_SistemaNom[0];
            A496Contagemresultado_SistemaAreaCod = P00A812_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P00A812_n496Contagemresultado_SistemaAreaCod[0];
            A509ContagemrResultado_SistemaSigla = P00A812_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00A812_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P00A812_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00A812_n515ContagemResultado_SistemaCoord[0];
            A804ContagemResultado_SistemaAtivo = P00A812_A804ContagemResultado_SistemaAtivo[0];
            n804ContagemResultado_SistemaAtivo = P00A812_n804ContagemResultado_SistemaAtivo[0];
            A143Modulo_Nome = P00A812_A143Modulo_Nome[0];
            A145Modulo_Sigla = P00A812_A145Modulo_Sigla[0];
            A477ContagemResultado_NaoCnfDmnNom = P00A812_A477ContagemResultado_NaoCnfDmnNom[0];
            n477ContagemResultado_NaoCnfDmnNom = P00A812_n477ContagemResultado_NaoCnfDmnNom[0];
            A2030ContagemResultado_NaoCnfDmnGls = P00A812_A2030ContagemResultado_NaoCnfDmnGls[0];
            n2030ContagemResultado_NaoCnfDmnGls = P00A812_n2030ContagemResultado_NaoCnfDmnGls[0];
            A1637ContagemResultado_ServicoSSSgl = P00A812_A1637ContagemResultado_ServicoSSSgl[0];
            n1637ContagemResultado_ServicoSSSgl = P00A812_n1637ContagemResultado_ServicoSSSgl[0];
            A528ContagemResultado_LoteAceite = P00A812_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00A812_n528ContagemResultado_LoteAceite[0];
            A529ContagemResultado_DataAceite = P00A812_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00A812_n529ContagemResultado_DataAceite[0];
            A525ContagemResultado_AceiteUserCod = P00A812_A525ContagemResultado_AceiteUserCod[0];
            n525ContagemResultado_AceiteUserCod = P00A812_n525ContagemResultado_AceiteUserCod[0];
            A1547ContagemResultado_LoteNFe = P00A812_A1547ContagemResultado_LoteNFe[0];
            n1547ContagemResultado_LoteNFe = P00A812_n1547ContagemResultado_LoteNFe[0];
            A526ContagemResultado_AceitePessoaCod = P00A812_A526ContagemResultado_AceitePessoaCod[0];
            n526ContagemResultado_AceitePessoaCod = P00A812_n526ContagemResultado_AceitePessoaCod[0];
            A527ContagemResultado_AceiteUserNom = P00A812_A527ContagemResultado_AceiteUserNom[0];
            n527ContagemResultado_AceiteUserNom = P00A812_n527ContagemResultado_AceiteUserNom[0];
            A1765ContagemResultado_CodSrvSSVnc = P00A812_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00A812_n1765ContagemResultado_CodSrvSSVnc[0];
            A1627ContagemResultado_CntSrvVncCod = P00A812_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = P00A812_n1627ContagemResultado_CntSrvVncCod[0];
            A2136ContagemResultado_CntadaOsVinc = P00A812_A2136ContagemResultado_CntadaOsVinc[0];
            n2136ContagemResultado_CntadaOsVinc = P00A812_n2136ContagemResultado_CntadaOsVinc[0];
            A771ContagemResultado_StatusDmnVnc = P00A812_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00A812_n771ContagemResultado_StatusDmnVnc[0];
            A603ContagemResultado_DmnVinculada = P00A812_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00A812_n603ContagemResultado_DmnVinculada[0];
            A1591ContagemResultado_CodSrvVnc = P00A812_A1591ContagemResultado_CodSrvVnc[0];
            n1591ContagemResultado_CodSrvVnc = P00A812_n1591ContagemResultado_CodSrvVnc[0];
            A1622ContagemResultado_CntVncCod = P00A812_A1622ContagemResultado_CntVncCod[0];
            n1622ContagemResultado_CntVncCod = P00A812_n1622ContagemResultado_CntVncCod[0];
            A1590ContagemResultado_SiglaSrvVnc = P00A812_A1590ContagemResultado_SiglaSrvVnc[0];
            n1590ContagemResultado_SiglaSrvVnc = P00A812_n1590ContagemResultado_SiglaSrvVnc[0];
            A2137ContagemResultado_SerGrupoVinc = P00A812_A2137ContagemResultado_SerGrupoVinc[0];
            n2137ContagemResultado_SerGrupoVinc = P00A812_n2137ContagemResultado_SerGrupoVinc[0];
            A1034ContagemResultadoLiqLog_Data = P00A812_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = P00A812_n1034ContagemResultadoLiqLog_Data[0];
            A1312ContagemResultado_ResponsavelPessCod = P00A812_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = P00A812_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = P00A812_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = P00A812_n1313ContagemResultado_ResponsavelPessNome[0];
            A2118ContagemResultado_Owner_Identificao = P00A812_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00A812_n2118ContagemResultado_Owner_Identificao[0];
            A531ContagemResultado_StatusUltCnt = P00A812_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P00A812_A566ContagemResultado_DataUltCnt[0];
            A825ContagemResultado_HoraUltCnt = P00A812_A825ContagemResultado_HoraUltCnt[0];
            A584ContagemResultado_ContadorFM = P00A812_A584ContagemResultado_ContadorFM[0];
            A682ContagemResultado_PFBFMUltima = P00A812_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = P00A812_A683ContagemResultado_PFLFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00A812_A684ContagemResultado_PFBFSUltima[0];
            A1480ContagemResultado_CstUntUltima = P00A812_A1480ContagemResultado_CstUntUltima[0];
            A685ContagemResultado_PFLFSUltima = P00A812_A685ContagemResultado_PFLFSUltima[0];
            A802ContagemResultado_DeflatorCnt = P00A812_A802ContagemResultado_DeflatorCnt[0];
            A578ContagemResultado_CntComNaoCnf = P00A812_A578ContagemResultado_CntComNaoCnf[0];
            n578ContagemResultado_CntComNaoCnf = P00A812_n578ContagemResultado_CntComNaoCnf[0];
            A510ContagemResultado_EsforcoSoma = P00A812_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00A812_n510ContagemResultado_EsforcoSoma[0];
            A1353ContagemResultado_ContratadaDoOwner = P00A812_A1353ContagemResultado_ContratadaDoOwner[0];
            n1353ContagemResultado_ContratadaDoOwner = P00A812_n1353ContagemResultado_ContratadaDoOwner[0];
            A1602ContagemResultado_GlsIndValor = P00A812_A1602ContagemResultado_GlsIndValor[0];
            n1602ContagemResultado_GlsIndValor = P00A812_n1602ContagemResultado_GlsIndValor[0];
            A1701ContagemResultado_TemProposta = P00A812_A1701ContagemResultado_TemProposta[0];
            n1701ContagemResultado_TemProposta = P00A812_n1701ContagemResultado_TemProposta[0];
            A52Contratada_AreaTrabalhoCod = P00A812_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00A812_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00A812_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00A812_n803ContagemResultado_ContratadaSigla[0];
            A499ContagemResultado_ContratadaPessoaCod = P00A812_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00A812_n499ContagemResultado_ContratadaPessoaCod[0];
            A1326ContagemResultado_ContratadaTipoFab = P00A812_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00A812_n1326ContagemResultado_ContratadaTipoFab[0];
            A500ContagemResultado_ContratadaPessoaNom = P00A812_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00A812_n500ContagemResultado_ContratadaPessoaNom[0];
            A1817ContagemResultado_ContratadaCNPJ = P00A812_A1817ContagemResultado_ContratadaCNPJ[0];
            n1817ContagemResultado_ContratadaCNPJ = P00A812_n1817ContagemResultado_ContratadaCNPJ[0];
            A1600ContagemResultado_CntSrvSttPgmFnc = P00A812_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = P00A812_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A1601ContagemResultado_CntSrvIndDvr = P00A812_A1601ContagemResultado_CntSrvIndDvr[0];
            n1601ContagemResultado_CntSrvIndDvr = P00A812_n1601ContagemResultado_CntSrvIndDvr[0];
            A1613ContagemResultado_CntSrvQtdUntCns = P00A812_A1613ContagemResultado_CntSrvQtdUntCns[0];
            n1613ContagemResultado_CntSrvQtdUntCns = P00A812_n1613ContagemResultado_CntSrvQtdUntCns[0];
            A1620ContagemResultado_CntSrvUndCnt = P00A812_A1620ContagemResultado_CntSrvUndCnt[0];
            n1620ContagemResultado_CntSrvUndCnt = P00A812_n1620ContagemResultado_CntSrvUndCnt[0];
            A1623ContagemResultado_CntSrvMmn = P00A812_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P00A812_n1623ContagemResultado_CntSrvMmn[0];
            A1607ContagemResultado_PrzAnl = P00A812_A1607ContagemResultado_PrzAnl[0];
            n1607ContagemResultado_PrzAnl = P00A812_n1607ContagemResultado_PrzAnl[0];
            A1609ContagemResultado_PrzCrr = P00A812_A1609ContagemResultado_PrzCrr[0];
            n1609ContagemResultado_PrzCrr = P00A812_n1609ContagemResultado_PrzCrr[0];
            A1618ContagemResultado_PrzRsp = P00A812_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P00A812_n1618ContagemResultado_PrzRsp[0];
            A1619ContagemResultado_PrzGrt = P00A812_A1619ContagemResultado_PrzGrt[0];
            n1619ContagemResultado_PrzGrt = P00A812_n1619ContagemResultado_PrzGrt[0];
            A1611ContagemResultado_PrzTpDias = P00A812_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00A812_n1611ContagemResultado_PrzTpDias[0];
            A1603ContagemResultado_CntCod = P00A812_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00A812_n1603ContagemResultado_CntCod[0];
            A1650ContagemResultado_PrzInc = P00A812_A1650ContagemResultado_PrzInc[0];
            n1650ContagemResultado_PrzInc = P00A812_n1650ContagemResultado_PrzInc[0];
            A601ContagemResultado_Servico = P00A812_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00A812_n601ContagemResultado_Servico[0];
            A2008ContagemResultado_CntSrvAls = P00A812_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00A812_n2008ContagemResultado_CntSrvAls[0];
            A1398ContagemResultado_ServicoNaoRqrAtr = P00A812_A1398ContagemResultado_ServicoNaoRqrAtr[0];
            n1398ContagemResultado_ServicoNaoRqrAtr = P00A812_n1398ContagemResultado_ServicoNaoRqrAtr[0];
            A1593ContagemResultado_CntSrvTpVnc = P00A812_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00A812_n1593ContagemResultado_CntSrvTpVnc[0];
            A1594ContagemResultado_CntSrvFtrm = P00A812_A1594ContagemResultado_CntSrvFtrm[0];
            n1594ContagemResultado_CntSrvFtrm = P00A812_n1594ContagemResultado_CntSrvFtrm[0];
            A1596ContagemResultado_CntSrvPrc = P00A812_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00A812_n1596ContagemResultado_CntSrvPrc[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00A812_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00A812_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1598ContagemResultado_ServicoNome = P00A812_A1598ContagemResultado_ServicoNome[0];
            n1598ContagemResultado_ServicoNome = P00A812_n1598ContagemResultado_ServicoNome[0];
            A1599ContagemResultado_ServicoTpHrrq = P00A812_A1599ContagemResultado_ServicoTpHrrq[0];
            n1599ContagemResultado_ServicoTpHrrq = P00A812_n1599ContagemResultado_ServicoTpHrrq[0];
            A801ContagemResultado_ServicoSigla = P00A812_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00A812_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P00A812_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00A812_n764ContagemResultado_ServicoGrupo[0];
            A1062ContagemResultado_ServicoTela = P00A812_A1062ContagemResultado_ServicoTela[0];
            n1062ContagemResultado_ServicoTela = P00A812_n1062ContagemResultado_ServicoTela[0];
            A897ContagemResultado_ServicoAtivo = P00A812_A897ContagemResultado_ServicoAtivo[0];
            n897ContagemResultado_ServicoAtivo = P00A812_n897ContagemResultado_ServicoAtivo[0];
            A2050ContagemResultado_SrvLnhNegCod = P00A812_A2050ContagemResultado_SrvLnhNegCod[0];
            n2050ContagemResultado_SrvLnhNegCod = P00A812_n2050ContagemResultado_SrvLnhNegCod[0];
            A1621ContagemResultado_CntSrvUndCntSgl = P00A812_A1621ContagemResultado_CntSrvUndCntSgl[0];
            n1621ContagemResultado_CntSrvUndCntSgl = P00A812_n1621ContagemResultado_CntSrvUndCntSgl[0];
            A2209ContagemResultado_CntSrvUndCntNome = P00A812_A2209ContagemResultado_CntSrvUndCntNome[0];
            n2209ContagemResultado_CntSrvUndCntNome = P00A812_n2209ContagemResultado_CntSrvUndCntNome[0];
            A1612ContagemResultado_CntNum = P00A812_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00A812_n1612ContagemResultado_CntNum[0];
            A1604ContagemResultado_CntPrpCod = P00A812_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P00A812_n1604ContagemResultado_CntPrpCod[0];
            A1615ContagemResultado_CntIndDvr = P00A812_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = P00A812_n1615ContagemResultado_CntIndDvr[0];
            A1617ContagemResultado_CntClcDvr = P00A812_A1617ContagemResultado_CntClcDvr[0];
            n1617ContagemResultado_CntClcDvr = P00A812_n1617ContagemResultado_CntClcDvr[0];
            A1616ContagemResultado_CntVlrUndCnt = P00A812_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00A812_n1616ContagemResultado_CntVlrUndCnt[0];
            A1632ContagemResultado_CntPrdFtrCada = P00A812_A1632ContagemResultado_CntPrdFtrCada[0];
            n1632ContagemResultado_CntPrdFtrCada = P00A812_n1632ContagemResultado_CntPrdFtrCada[0];
            A1633ContagemResultado_CntPrdFtrIni = P00A812_A1633ContagemResultado_CntPrdFtrIni[0];
            n1633ContagemResultado_CntPrdFtrIni = P00A812_n1633ContagemResultado_CntPrdFtrIni[0];
            A1634ContagemResultado_CntPrdFtrFim = P00A812_A1634ContagemResultado_CntPrdFtrFim[0];
            n1634ContagemResultado_CntPrdFtrFim = P00A812_n1634ContagemResultado_CntPrdFtrFim[0];
            A2082ContagemResultado_CntLmtFtr = P00A812_A2082ContagemResultado_CntLmtFtr[0];
            n2082ContagemResultado_CntLmtFtr = P00A812_n2082ContagemResultado_CntLmtFtr[0];
            A1624ContagemResultado_DatVgnInc = P00A812_A1624ContagemResultado_DatVgnInc[0];
            n1624ContagemResultado_DatVgnInc = P00A812_n1624ContagemResultado_DatVgnInc[0];
            A1605ContagemResultado_CntPrpPesCod = P00A812_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P00A812_n1605ContagemResultado_CntPrpPesCod[0];
            A1606ContagemResultado_CntPrpPesNom = P00A812_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P00A812_n1606ContagemResultado_CntPrpPesNom[0];
            A1608ContagemResultado_PrzExc = P00A812_A1608ContagemResultado_PrzExc[0];
            n1608ContagemResultado_PrzExc = P00A812_n1608ContagemResultado_PrzExc[0];
            A1610ContagemResultado_PrzTp = P00A812_A1610ContagemResultado_PrzTp[0];
            n1610ContagemResultado_PrzTp = P00A812_n1610ContagemResultado_PrzTp[0];
            A1625ContagemResultado_DatIncTA = P00A812_A1625ContagemResultado_DatIncTA[0];
            n1625ContagemResultado_DatIncTA = P00A812_n1625ContagemResultado_DatIncTA[0];
            GXt_decimal1 = A497IndiceDivergencia;
            new prc_indicedivergencia(context ).execute( ref  A1553ContagemResultado_CntSrvCod, out  GXt_decimal1) ;
            A497IndiceDivergencia = GXt_decimal1;
            GXt_char2 = A498CalculoDivergencia;
            new prc_calculodivergencia(context ).execute(  A490ContagemResultado_ContratadaCod, out  GXt_char2) ;
            A498CalculoDivergencia = GXt_char2;
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            A553ContagemResultado_DemandaFM_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A493ContagemResultado_DemandaFM, 1, 18), "."));
            A1815ContagemResultado_DmnSrvPrst = StringUtil.Trim( A493ContagemResultado_DemandaFM) + " " + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " (" + A803ContagemResultado_ContratadaSigla + ")";
            A599ContagemResultado_Demanda_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A457ContagemResultado_Demanda, 1, 18), "."));
            GXt_boolean3 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean3) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean3;
            GXt_int4 = A1236ContagemResultado_ContratanteDoResponsavel;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int4) ;
            A1236ContagemResultado_ContratanteDoResponsavel = GXt_int4;
            GXt_int4 = A1352ContagemResultado_ContratanteDoOwner;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A508ContagemResultado_Owner, out  GXt_int4) ;
            A1352ContagemResultado_ContratanteDoOwner = GXt_int4;
            GXt_char2 = A486ContagemResultado_EsforcoTotal;
            new prc_contageresultado_esforcototal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_char2) ;
            A486ContagemResultado_EsforcoTotal = GXt_char2;
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            GXt_int5 = A585ContagemResultado_Erros;
            new prc_pendenciasdaos(context ).execute(  A456ContagemResultado_Codigo, out  GXt_int5) ;
            A585ContagemResultado_Erros = GXt_int5;
            GXt_int4 = A1229ContagemResultado_ContratadaDoResponsavel;
            new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int4) ;
            A1229ContagemResultado_ContratadaDoResponsavel = GXt_int4;
            if ( ! P00A812_n1043ContagemResultado_LiqLogCod[0] )
            {
               A1028ContagemResultado_Liquidada = true;
            }
            else
            {
               if ( P00A812_n1043ContagemResultado_LiqLogCod[0] )
               {
                  A1028ContagemResultado_Liquidada = false;
               }
               else
               {
                  A1028ContagemResultado_Liquidada = false;
               }
            }
            GXt_char2 = A2091ContagemResultado_CntSrvPrrNome;
            new prc_buscaprioridadeos(context ).execute(  A1443ContagemResultado_CntSrvPrrCod, out  GXt_char2) ;
            A2091ContagemResultado_CntSrvPrrNome = GXt_char2;
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContagemResultado";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Resultado_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataDmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataInicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Inicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1790ContagemResultado_DataInicio, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataEntrega";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de Entrega";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A472ContagemResultado_DataEntrega, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_HoraEntrega";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Hora de Entrega";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataPrevista";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data Prevista";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataExecucao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1349ContagemResultado_DataExecucao, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataEntregaReal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Entrega Real";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataHomologacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Homologa��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1348ContagemResultado_DataHomologacao, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrazoInicialDias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo inicial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrazoMaisDias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo extendido";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1237ContagemResultado_PrazoMaisDias), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prestadora";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaPessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaPessoaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A500ContagemResultado_ContratadaPessoaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prestadora";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A803ContagemResultado_ContratadaSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaTipoFab";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de F�brica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1326ContagemResultado_ContratadaTipoFab;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaCNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1817ContagemResultado_ContratadaCNPJ;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "IndiceDivergencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice Diverg�ncia:";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A497IndiceDivergencia, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "CalculoDivergencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usando:";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A498CalculoDivergencia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Origem";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Origem";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A866ContagemResultado_ContratadaOrigemSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemPesCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Contratada Origem Pes Cod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A807ContagemResultado_ContratadaOrigemPesCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemPesNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada Origem";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A808ContagemResultado_ContratadaOrigemPesNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemAreaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A867ContagemResultado_ContratadaOrigemAreaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemUsaSistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada Usa o Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2093ContagemResultado_ContratadaOrigemUsaSistema);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContadorFSCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CrFSPessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A480ContagemResultado_CrFSPessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContadorFSNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A455ContagemResultado_ContadorFSNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Demanda";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� Refer�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A457ContagemResultado_Demanda;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DmnSrvPrst";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Demanda Servi�o Prestadora";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1815ContagemResultado_DmnSrvPrst;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Demanda_ORDER";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Demanda_ORDER";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A599ContagemResultado_Demanda_ORDER), 18, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Erros";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Erros";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A585ContagemResultado_Erros), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DemandaFM";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A493ContagemResultado_DemandaFM;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DemandaFM_ORDER";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Demanda FM_ORDER";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A553ContagemResultado_DemandaFM_ORDER), 18, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� SS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_OsFsOsFm";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS Ref|OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A501ContagemResultado_OsFsOsFm;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_OSManual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Solicita��o manual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1173ContagemResultado_OSManual);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Titulo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A494ContagemResultado_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Link";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Link";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A465ContagemResultado_Link;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A495ContagemResultado_SistemaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contagemresultado_SistemaAreaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A496Contagemresultado_SistemaAreaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemrResultado_SistemaSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A509ContagemrResultado_SistemaSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaCoord";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Coordena��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A515ContagemResultado_SistemaCoord;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaAtivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A804ContagemResultado_SistemaAtivo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Modulo_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "M�dulo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Modulo_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "M�dulo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A143Modulo_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Modulo_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A145Modulo_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_NaoCnfDmnCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pend�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_NaoCnfDmnNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�o Conformidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A477ContagemResultado_NaoCnfDmnNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_NaoCnfDmnGls";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Glos�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2030ContagemResultado_NaoCnfDmnGls);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_StatusDmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A484ContagemResultado_StatusDmn;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_StatusUltCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status Cnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_StatusDmnVnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status Dmn Vinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A771ContagemResultado_StatusDmnVnc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataUltCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data da Cnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_HoraUltCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Hora Ult Cnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A825ContagemResultado_HoraUltCnt;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntComNaoCnf";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Com Nao Cnf";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A578ContagemResultado_CntComNaoCnf), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContadorFM";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contador FM";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A584ContagemResultado_ContadorFM), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFBFMUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PFBFM Ultima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFLFMUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFLFMUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A683ContagemResultado_PFLFMUltima, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFBFSUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFBFSUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CstUntUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cst Unt Ultima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1480ContagemResultado_CstUntUltima, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFLFSUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFLFSUltima";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A685ContagemResultado_PFLFSUltima, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DeflatorCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Deflator";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A802ContagemResultado_DeflatorCnt, 6, 3);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Servico";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo no Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvAls";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Srv Als";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2008ContagemResultado_CntSrvAls;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoGrupo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo de Servi�os";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A764ContagemResultado_ServicoGrupo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A801ContagemResultado_ServicoSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoTela";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tela";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1062ContagemResultado_ServicoTela;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoAtivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A897ContagemResultado_ServicoAtivo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoNaoRqrAtr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Servico Nao Rqr Atr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1398ContagemResultado_ServicoNaoRqrAtr);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoSS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o solicitado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoSSSgl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1637ContagemResultado_ServicoSSSgl;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SrvLnhNegCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Linha de Neg�cio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2050ContagemResultado_SrvLnhNegCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_EhValidacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A485ContagemResultado_EhValidacao);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_EsforcoTotal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Esfor�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A486ContagemResultado_EsforcoTotal;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_EsforcoSoma";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Esforco Soma";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A510ContagemResultado_EsforcoSoma), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Observacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Observa��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A514ContagemResultado_Observacao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Evidencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A592ContagemResultado_Evidencia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataCadastro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Cadastro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Owner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Criador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Owner_Identificao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Criador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2118ContagemResultado_Owner_Identificao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratanteDoOwner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratante do Owner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1352ContagemResultado_ContratanteDoOwner), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaDoOwner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Contratada Do Owner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1353ContagemResultado_ContratadaDoOwner), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Responsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ResponsavelPessCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1312ContagemResultado_ResponsavelPessCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ResponsavelPessNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1313ContagemResultado_ResponsavelPessNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaDoResponsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada do Responsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratanteDoResponsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratante do Responsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ValorPF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da Unidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A512ContagemResultado_ValorPF, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Custo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Custo da Unidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1180ContagemResultado_Custo, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ValorFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor Final";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LoteAceiteCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Lote";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LoteAceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Lote";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A528ContagemResultado_LoteAceite;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataAceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data do Aceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A529ContagemResultado_DataAceite, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_AceiteUserCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel do Aceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A525ContagemResultado_AceiteUserCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_AceitePessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel do Aceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A526ContagemResultado_AceitePessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_AceiteUserNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel do Aceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A527ContagemResultado_AceiteUserNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LoteNFe";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "NFe do Lote de Aceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1547ContagemResultado_LoteNFe), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_VlrAceite";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor faturado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1559ContagemResultado_VlrAceite, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Baseline";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Baseline";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A598ContagemResultado_Baseline);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_OSVinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Os vinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DmnVinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A603ContagemResultado_DmnVinculada;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFBFSImp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PF FS Importados";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFLFSImp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "FS Importado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A799ContagemResultado_PFLFSImp, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PBFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Bruto final";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1178ContagemResultado_PBFinal, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PLFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Liquido final";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1179ContagemResultado_PLFinal, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LiqLogCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Liq Log Cod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Liquidada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Liquidada ao Prestador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1028ContagemResultado_Liquidada);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultadoLiqLog_Data";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FncUsrCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fun��o de Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Agrupador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Agrupador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1046ContagemResultado_Agrupador;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsData";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data da glosa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1049ContagemResultado_GlsData, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsDescricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1050ContagemResultado_GlsDescricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsValor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1051ContagemResultado_GlsValor, 12, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsUser";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1052ContagemResultado_GlsUser), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_RdmnIssueId";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Issue Id";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_RdmnProjectId";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Project Id";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1390ContagemResultado_RdmnProjectId), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_RdmnUpdated";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ultima atualiza��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1392ContagemResultado_RdmnUpdated, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "da prioridade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o da Prioridade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2091ContagemResultado_CntSrvPrrNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrPrz";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo da prioridade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1444ContagemResultado_CntSrvPrrPrz, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrCst";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Custo da prioridade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1445ContagemResultado_CntSrvPrrCst, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TemDpnHmlg";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tem depend�ncia para homologar?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1457ContagemResultado_TemDpnHmlg);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TemPndHmlg";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tem pend�ncia para homologar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1802ContagemResultado_TemPndHmlg);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TmpEstAnl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tempo estimado de an�lise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1519ContagemResultado_TmpEstAnl), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TmpEstExc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tempo estimado de execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1505ContagemResultado_TmpEstExc), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TmpEstCrr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tempo estimado de corre��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1506ContagemResultado_TmpEstCrr), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_InicioAnl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio do an�lise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1520ContagemResultado_InicioAnl, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FimAnl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim do an�lise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1521ContagemResultado_FimAnl, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_InicioExc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio da execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1509ContagemResultado_InicioExc, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FimExc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim da execu��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1510ContagemResultado_FimExc, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_InicioCrr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio da corre��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1511ContagemResultado_InicioCrr, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FimCrr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim da corre��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A1512ContagemResultado_FimCrr, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Evento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Evento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ProjetoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Projeto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1544ContagemResultado_ProjetoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvTpVnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de vinculo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1593ContagemResultado_CntSrvTpVnc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvFtrm";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faturamento sob";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1594ContagemResultado_CntSrvFtrm;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Percentual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1596ContagemResultado_CntSrvPrc, 7, 3);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvVlrUndCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor unidade contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1597ContagemResultado_CntSrvVlrUndCnt, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvSttPgmFnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status pagamento de funcion�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1600ContagemResultado_CntSrvSttPgmFnc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvIndDvr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice aceitavel de diverg�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1601ContagemResultado_CntSrvIndDvr, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvQtdUntCns";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quantidade unit�ria de consumo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvUndCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade de medi��o contratda";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1620ContagemResultado_CntSrvUndCnt), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvUndCntSgl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla da unidade contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1621ContagemResultado_CntSrvUndCntSgl;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvUndCntNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome da Unidade Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2209ContagemResultado_CntSrvUndCntNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvMmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Momento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1623ContagemResultado_CntSrvMmn;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzAnl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de an�lise";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1607ContagemResultado_PrzAnl), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzExc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prz Exc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1608ContagemResultado_PrzExc), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzCrr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prz Crr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1609ContagemResultado_PrzCrr), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzRsp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de resposta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1618ContagemResultado_PrzRsp), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzGrt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de garantia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1619ContagemResultado_PrzGrt), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzTp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prz Tp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1610ContagemResultado_PrzTp;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzTpDias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias Corridos / Uteis";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1611ContagemResultado_PrzTpDias;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1603ContagemResultado_CntCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntNum";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Numero do Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1612ContagemResultado_CntNum;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrpCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1604ContagemResultado_CntPrpCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrpPesCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prp Pes Cod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1605ContagemResultado_CntPrpPesCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrpPesNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prp Pes Nom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1606ContagemResultado_CntPrpPesNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntIndDvr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de aceita��o de diverg�ncia do contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1615ContagemResultado_CntIndDvr, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntClcDvr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo da diverg�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1617ContagemResultado_CntClcDvr;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntVlrUndCnt";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da unidade contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1616ContagemResultado_CntVlrUndCnt, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrdFtrCada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prd Ftr Cada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1632ContagemResultado_CntPrdFtrCada;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrdFtrIni";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prd Ftr Ini";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1633ContagemResultado_CntPrdFtrIni), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrdFtrFim";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prd Ftr Fim";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1634ContagemResultado_CntPrdFtrFim), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntLmtFtr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Limite de Faturamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2082ContagemResultado_CntLmtFtr, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DatVgnInc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data vigencia inicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1624ContagemResultado_DatVgnInc, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DatIncTA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data TA inicio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1625ContagemResultado_DatIncTA, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Servico";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1598ContagemResultado_ServicoNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoTpHrrq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de hierarquia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1599ContagemResultado_ServicoTpHrrq), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CodSrvVnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cod Srv Vnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1591ContagemResultado_CodSrvVnc), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SiglaSrvVnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla servi�o vinculado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1590ContagemResultado_SiglaSrvVnc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CodSrvSSVnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o SS vinculado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1765ContagemResultado_CodSrvSSVnc), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvVncCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato servico da OS vinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1627ContagemResultado_CntSrvVncCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntVncCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato da OS Vinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1622ContagemResultado_CntVncCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntadaOsVinc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada da Os Vinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2136ContagemResultado_CntadaOsVinc), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SerGrupoVinc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo de Servi�os do Servi�o da OS Vinculada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2137ContagemResultado_SerGrupoVinc), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsIndValor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor das glosas dos indicadores";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1602ContagemResultado_GlsIndValor, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TipoRegistro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Tipo Registro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_UOOwner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_UOOwner";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Referencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Referencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1585ContagemResultado_Referencia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Restricoes";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Restricoes";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1586ContagemResultado_Restricoes;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrioridadePrevista";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prioridade Prevista";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1587ContagemResultado_PrioridadePrevista;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzInc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio dos prazos";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1650ContagemResultado_PrzInc), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TemProposta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tem propostas?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1701ContagemResultado_TemProposta);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Combinada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Combinada?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1714ContagemResultado_Combinada);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Entrega";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Entrega";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1762ContagemResultado_Entrega), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SemCusto";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sem custo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1791ContagemResultado_SemCusto);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_VlrCnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor do cancelamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1854ContagemResultado_VlrCnc, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFCnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidades remuneradas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1855ContagemResultado_PFCnc, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataPrvPgm";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data prevista de pagamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1903ContagemResultado_DataPrvPgm, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_QuantidadeSolicitada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quantidade Solicitada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2133ContagemResultado_QuantidadeSolicitada, 9, 4);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A823 */
         pr_default.execute(1, new Object[] {n52Contratada_AreaTrabalhoCod, A52Contratada_AreaTrabalhoCod, AV16ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1600ContagemResultado_CntSrvSttPgmFnc = P00A823_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = P00A823_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A1601ContagemResultado_CntSrvIndDvr = P00A823_A1601ContagemResultado_CntSrvIndDvr[0];
            n1601ContagemResultado_CntSrvIndDvr = P00A823_n1601ContagemResultado_CntSrvIndDvr[0];
            A1613ContagemResultado_CntSrvQtdUntCns = P00A823_A1613ContagemResultado_CntSrvQtdUntCns[0];
            n1613ContagemResultado_CntSrvQtdUntCns = P00A823_n1613ContagemResultado_CntSrvQtdUntCns[0];
            A1620ContagemResultado_CntSrvUndCnt = P00A823_A1620ContagemResultado_CntSrvUndCnt[0];
            n1620ContagemResultado_CntSrvUndCnt = P00A823_n1620ContagemResultado_CntSrvUndCnt[0];
            A1621ContagemResultado_CntSrvUndCntSgl = P00A823_A1621ContagemResultado_CntSrvUndCntSgl[0];
            n1621ContagemResultado_CntSrvUndCntSgl = P00A823_n1621ContagemResultado_CntSrvUndCntSgl[0];
            A2209ContagemResultado_CntSrvUndCntNome = P00A823_A2209ContagemResultado_CntSrvUndCntNome[0];
            n2209ContagemResultado_CntSrvUndCntNome = P00A823_n2209ContagemResultado_CntSrvUndCntNome[0];
            A1623ContagemResultado_CntSrvMmn = P00A823_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P00A823_n1623ContagemResultado_CntSrvMmn[0];
            A1607ContagemResultado_PrzAnl = P00A823_A1607ContagemResultado_PrzAnl[0];
            n1607ContagemResultado_PrzAnl = P00A823_n1607ContagemResultado_PrzAnl[0];
            A1609ContagemResultado_PrzCrr = P00A823_A1609ContagemResultado_PrzCrr[0];
            n1609ContagemResultado_PrzCrr = P00A823_n1609ContagemResultado_PrzCrr[0];
            A1618ContagemResultado_PrzRsp = P00A823_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P00A823_n1618ContagemResultado_PrzRsp[0];
            A1619ContagemResultado_PrzGrt = P00A823_A1619ContagemResultado_PrzGrt[0];
            n1619ContagemResultado_PrzGrt = P00A823_n1619ContagemResultado_PrzGrt[0];
            A1611ContagemResultado_PrzTpDias = P00A823_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00A823_n1611ContagemResultado_PrzTpDias[0];
            A1603ContagemResultado_CntCod = P00A823_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00A823_n1603ContagemResultado_CntCod[0];
            A1612ContagemResultado_CntNum = P00A823_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00A823_n1612ContagemResultado_CntNum[0];
            A1604ContagemResultado_CntPrpCod = P00A823_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P00A823_n1604ContagemResultado_CntPrpCod[0];
            A1605ContagemResultado_CntPrpPesCod = P00A823_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P00A823_n1605ContagemResultado_CntPrpPesCod[0];
            A1606ContagemResultado_CntPrpPesNom = P00A823_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P00A823_n1606ContagemResultado_CntPrpPesNom[0];
            A1615ContagemResultado_CntIndDvr = P00A823_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = P00A823_n1615ContagemResultado_CntIndDvr[0];
            A1617ContagemResultado_CntClcDvr = P00A823_A1617ContagemResultado_CntClcDvr[0];
            n1617ContagemResultado_CntClcDvr = P00A823_n1617ContagemResultado_CntClcDvr[0];
            A1616ContagemResultado_CntVlrUndCnt = P00A823_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00A823_n1616ContagemResultado_CntVlrUndCnt[0];
            A1632ContagemResultado_CntPrdFtrCada = P00A823_A1632ContagemResultado_CntPrdFtrCada[0];
            n1632ContagemResultado_CntPrdFtrCada = P00A823_n1632ContagemResultado_CntPrdFtrCada[0];
            A1633ContagemResultado_CntPrdFtrIni = P00A823_A1633ContagemResultado_CntPrdFtrIni[0];
            n1633ContagemResultado_CntPrdFtrIni = P00A823_n1633ContagemResultado_CntPrdFtrIni[0];
            A1634ContagemResultado_CntPrdFtrFim = P00A823_A1634ContagemResultado_CntPrdFtrFim[0];
            n1634ContagemResultado_CntPrdFtrFim = P00A823_n1634ContagemResultado_CntPrdFtrFim[0];
            A2082ContagemResultado_CntLmtFtr = P00A823_A2082ContagemResultado_CntLmtFtr[0];
            n2082ContagemResultado_CntLmtFtr = P00A823_n2082ContagemResultado_CntLmtFtr[0];
            A1624ContagemResultado_DatVgnInc = P00A823_A1624ContagemResultado_DatVgnInc[0];
            n1624ContagemResultado_DatVgnInc = P00A823_n1624ContagemResultado_DatVgnInc[0];
            A1598ContagemResultado_ServicoNome = P00A823_A1598ContagemResultado_ServicoNome[0];
            n1598ContagemResultado_ServicoNome = P00A823_n1598ContagemResultado_ServicoNome[0];
            A1599ContagemResultado_ServicoTpHrrq = P00A823_A1599ContagemResultado_ServicoTpHrrq[0];
            n1599ContagemResultado_ServicoTpHrrq = P00A823_n1599ContagemResultado_ServicoTpHrrq[0];
            A1591ContagemResultado_CodSrvVnc = P00A823_A1591ContagemResultado_CodSrvVnc[0];
            n1591ContagemResultado_CodSrvVnc = P00A823_n1591ContagemResultado_CodSrvVnc[0];
            A1590ContagemResultado_SiglaSrvVnc = P00A823_A1590ContagemResultado_SiglaSrvVnc[0];
            n1590ContagemResultado_SiglaSrvVnc = P00A823_n1590ContagemResultado_SiglaSrvVnc[0];
            A1765ContagemResultado_CodSrvSSVnc = P00A823_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00A823_n1765ContagemResultado_CodSrvSSVnc[0];
            A1627ContagemResultado_CntSrvVncCod = P00A823_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = P00A823_n1627ContagemResultado_CntSrvVncCod[0];
            A1622ContagemResultado_CntVncCod = P00A823_A1622ContagemResultado_CntVncCod[0];
            n1622ContagemResultado_CntVncCod = P00A823_n1622ContagemResultado_CntVncCod[0];
            A2136ContagemResultado_CntadaOsVinc = P00A823_A2136ContagemResultado_CntadaOsVinc[0];
            n2136ContagemResultado_CntadaOsVinc = P00A823_n2136ContagemResultado_CntadaOsVinc[0];
            A2137ContagemResultado_SerGrupoVinc = P00A823_A2137ContagemResultado_SerGrupoVinc[0];
            n2137ContagemResultado_SerGrupoVinc = P00A823_n2137ContagemResultado_SerGrupoVinc[0];
            A1583ContagemResultado_TipoRegistro = P00A823_A1583ContagemResultado_TipoRegistro[0];
            A1584ContagemResultado_UOOwner = P00A823_A1584ContagemResultado_UOOwner[0];
            n1584ContagemResultado_UOOwner = P00A823_n1584ContagemResultado_UOOwner[0];
            A1585ContagemResultado_Referencia = P00A823_A1585ContagemResultado_Referencia[0];
            n1585ContagemResultado_Referencia = P00A823_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = P00A823_A1586ContagemResultado_Restricoes[0];
            n1586ContagemResultado_Restricoes = P00A823_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = P00A823_A1587ContagemResultado_PrioridadePrevista[0];
            n1587ContagemResultado_PrioridadePrevista = P00A823_n1587ContagemResultado_PrioridadePrevista[0];
            A1650ContagemResultado_PrzInc = P00A823_A1650ContagemResultado_PrzInc[0];
            n1650ContagemResultado_PrzInc = P00A823_n1650ContagemResultado_PrzInc[0];
            A1714ContagemResultado_Combinada = P00A823_A1714ContagemResultado_Combinada[0];
            n1714ContagemResultado_Combinada = P00A823_n1714ContagemResultado_Combinada[0];
            A1762ContagemResultado_Entrega = P00A823_A1762ContagemResultado_Entrega[0];
            n1762ContagemResultado_Entrega = P00A823_n1762ContagemResultado_Entrega[0];
            A1791ContagemResultado_SemCusto = P00A823_A1791ContagemResultado_SemCusto[0];
            n1791ContagemResultado_SemCusto = P00A823_n1791ContagemResultado_SemCusto[0];
            A1854ContagemResultado_VlrCnc = P00A823_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00A823_n1854ContagemResultado_VlrCnc[0];
            A1855ContagemResultado_PFCnc = P00A823_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P00A823_n1855ContagemResultado_PFCnc[0];
            A1903ContagemResultado_DataPrvPgm = P00A823_A1903ContagemResultado_DataPrvPgm[0];
            n1903ContagemResultado_DataPrvPgm = P00A823_n1903ContagemResultado_DataPrvPgm[0];
            A2133ContagemResultado_QuantidadeSolicitada = P00A823_A2133ContagemResultado_QuantidadeSolicitada[0];
            n2133ContagemResultado_QuantidadeSolicitada = P00A823_n2133ContagemResultado_QuantidadeSolicitada[0];
            A531ContagemResultado_StatusUltCnt = P00A823_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P00A823_A566ContagemResultado_DataUltCnt[0];
            A825ContagemResultado_HoraUltCnt = P00A823_A825ContagemResultado_HoraUltCnt[0];
            A578ContagemResultado_CntComNaoCnf = P00A823_A578ContagemResultado_CntComNaoCnf[0];
            n578ContagemResultado_CntComNaoCnf = P00A823_n578ContagemResultado_CntComNaoCnf[0];
            A584ContagemResultado_ContadorFM = P00A823_A584ContagemResultado_ContadorFM[0];
            A682ContagemResultado_PFBFMUltima = P00A823_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = P00A823_A683ContagemResultado_PFLFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00A823_A684ContagemResultado_PFBFSUltima[0];
            A1480ContagemResultado_CstUntUltima = P00A823_A1480ContagemResultado_CstUntUltima[0];
            A685ContagemResultado_PFLFSUltima = P00A823_A685ContagemResultado_PFLFSUltima[0];
            A802ContagemResultado_DeflatorCnt = P00A823_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00A823_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00A823_n510ContagemResultado_EsforcoSoma[0];
            A2118ContagemResultado_Owner_Identificao = P00A823_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00A823_n2118ContagemResultado_Owner_Identificao[0];
            A1353ContagemResultado_ContratadaDoOwner = P00A823_A1353ContagemResultado_ContratadaDoOwner[0];
            n1353ContagemResultado_ContratadaDoOwner = P00A823_n1353ContagemResultado_ContratadaDoOwner[0];
            A1608ContagemResultado_PrzExc = P00A823_A1608ContagemResultado_PrzExc[0];
            n1608ContagemResultado_PrzExc = P00A823_n1608ContagemResultado_PrzExc[0];
            A1610ContagemResultado_PrzTp = P00A823_A1610ContagemResultado_PrzTp[0];
            n1610ContagemResultado_PrzTp = P00A823_n1610ContagemResultado_PrzTp[0];
            A1625ContagemResultado_DatIncTA = P00A823_A1625ContagemResultado_DatIncTA[0];
            n1625ContagemResultado_DatIncTA = P00A823_n1625ContagemResultado_DatIncTA[0];
            A1602ContagemResultado_GlsIndValor = P00A823_A1602ContagemResultado_GlsIndValor[0];
            n1602ContagemResultado_GlsIndValor = P00A823_n1602ContagemResultado_GlsIndValor[0];
            A1701ContagemResultado_TemProposta = P00A823_A1701ContagemResultado_TemProposta[0];
            n1701ContagemResultado_TemProposta = P00A823_n1701ContagemResultado_TemProposta[0];
            A1443ContagemResultado_CntSrvPrrCod = P00A823_A1443ContagemResultado_CntSrvPrrCod[0];
            n1443ContagemResultado_CntSrvPrrCod = P00A823_n1443ContagemResultado_CntSrvPrrCod[0];
            A1043ContagemResultado_LiqLogCod = P00A823_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = P00A823_n1043ContagemResultado_LiqLogCod[0];
            A512ContagemResultado_ValorPF = P00A823_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00A823_n512ContagemResultado_ValorPF[0];
            A52Contratada_AreaTrabalhoCod = P00A823_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00A823_n52Contratada_AreaTrabalhoCod[0];
            A890ContagemResultado_Responsavel = P00A823_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00A823_n890ContagemResultado_Responsavel[0];
            A508ContagemResultado_Owner = P00A823_A508ContagemResultado_Owner[0];
            A456ContagemResultado_Codigo = P00A823_A456ContagemResultado_Codigo[0];
            A457ContagemResultado_Demanda = P00A823_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00A823_n457ContagemResultado_Demanda[0];
            A803ContagemResultado_ContratadaSigla = P00A823_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00A823_n803ContagemResultado_ContratadaSigla[0];
            A801ContagemResultado_ServicoSigla = P00A823_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00A823_n801ContagemResultado_ServicoSigla[0];
            A493ContagemResultado_DemandaFM = P00A823_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00A823_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = P00A823_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00A823_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P00A823_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00A823_n1553ContagemResultado_CntSrvCod[0];
            A471ContagemResultado_DataDmn = P00A823_A471ContagemResultado_DataDmn[0];
            A1790ContagemResultado_DataInicio = P00A823_A1790ContagemResultado_DataInicio[0];
            n1790ContagemResultado_DataInicio = P00A823_n1790ContagemResultado_DataInicio[0];
            A472ContagemResultado_DataEntrega = P00A823_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00A823_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00A823_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00A823_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = P00A823_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00A823_n1351ContagemResultado_DataPrevista[0];
            A1349ContagemResultado_DataExecucao = P00A823_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00A823_n1349ContagemResultado_DataExecucao[0];
            A2017ContagemResultado_DataEntregaReal = P00A823_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00A823_n2017ContagemResultado_DataEntregaReal[0];
            A1348ContagemResultado_DataHomologacao = P00A823_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P00A823_n1348ContagemResultado_DataHomologacao[0];
            A1227ContagemResultado_PrazoInicialDias = P00A823_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P00A823_n1227ContagemResultado_PrazoInicialDias[0];
            A1237ContagemResultado_PrazoMaisDias = P00A823_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P00A823_n1237ContagemResultado_PrazoMaisDias[0];
            A499ContagemResultado_ContratadaPessoaCod = P00A823_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00A823_n499ContagemResultado_ContratadaPessoaCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00A823_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00A823_n500ContagemResultado_ContratadaPessoaNom[0];
            A1326ContagemResultado_ContratadaTipoFab = P00A823_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00A823_n1326ContagemResultado_ContratadaTipoFab[0];
            A1817ContagemResultado_ContratadaCNPJ = P00A823_A1817ContagemResultado_ContratadaCNPJ[0];
            n1817ContagemResultado_ContratadaCNPJ = P00A823_n1817ContagemResultado_ContratadaCNPJ[0];
            A805ContagemResultado_ContratadaOrigemCod = P00A823_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P00A823_n805ContagemResultado_ContratadaOrigemCod[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00A823_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00A823_n866ContagemResultado_ContratadaOrigemSigla[0];
            A807ContagemResultado_ContratadaOrigemPesCod = P00A823_A807ContagemResultado_ContratadaOrigemPesCod[0];
            n807ContagemResultado_ContratadaOrigemPesCod = P00A823_n807ContagemResultado_ContratadaOrigemPesCod[0];
            A808ContagemResultado_ContratadaOrigemPesNom = P00A823_A808ContagemResultado_ContratadaOrigemPesNom[0];
            n808ContagemResultado_ContratadaOrigemPesNom = P00A823_n808ContagemResultado_ContratadaOrigemPesNom[0];
            A867ContagemResultado_ContratadaOrigemAreaCod = P00A823_A867ContagemResultado_ContratadaOrigemAreaCod[0];
            n867ContagemResultado_ContratadaOrigemAreaCod = P00A823_n867ContagemResultado_ContratadaOrigemAreaCod[0];
            A2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            n2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            A454ContagemResultado_ContadorFSCod = P00A823_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = P00A823_n454ContagemResultado_ContadorFSCod[0];
            A480ContagemResultado_CrFSPessoaCod = P00A823_A480ContagemResultado_CrFSPessoaCod[0];
            n480ContagemResultado_CrFSPessoaCod = P00A823_n480ContagemResultado_CrFSPessoaCod[0];
            A455ContagemResultado_ContadorFSNom = P00A823_A455ContagemResultado_ContadorFSNom[0];
            n455ContagemResultado_ContadorFSNom = P00A823_n455ContagemResultado_ContadorFSNom[0];
            A1452ContagemResultado_SS = P00A823_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P00A823_n1452ContagemResultado_SS[0];
            A1173ContagemResultado_OSManual = P00A823_A1173ContagemResultado_OSManual[0];
            n1173ContagemResultado_OSManual = P00A823_n1173ContagemResultado_OSManual[0];
            A494ContagemResultado_Descricao = P00A823_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00A823_n494ContagemResultado_Descricao[0];
            A465ContagemResultado_Link = P00A823_A465ContagemResultado_Link[0];
            n465ContagemResultado_Link = P00A823_n465ContagemResultado_Link[0];
            A489ContagemResultado_SistemaCod = P00A823_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00A823_n489ContagemResultado_SistemaCod[0];
            A495ContagemResultado_SistemaNom = P00A823_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P00A823_n495ContagemResultado_SistemaNom[0];
            A496Contagemresultado_SistemaAreaCod = P00A823_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P00A823_n496Contagemresultado_SistemaAreaCod[0];
            A509ContagemrResultado_SistemaSigla = P00A823_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00A823_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P00A823_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00A823_n515ContagemResultado_SistemaCoord[0];
            A804ContagemResultado_SistemaAtivo = P00A823_A804ContagemResultado_SistemaAtivo[0];
            n804ContagemResultado_SistemaAtivo = P00A823_n804ContagemResultado_SistemaAtivo[0];
            A146Modulo_Codigo = P00A823_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00A823_n146Modulo_Codigo[0];
            A143Modulo_Nome = P00A823_A143Modulo_Nome[0];
            A145Modulo_Sigla = P00A823_A145Modulo_Sigla[0];
            A468ContagemResultado_NaoCnfDmnCod = P00A823_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00A823_n468ContagemResultado_NaoCnfDmnCod[0];
            A477ContagemResultado_NaoCnfDmnNom = P00A823_A477ContagemResultado_NaoCnfDmnNom[0];
            n477ContagemResultado_NaoCnfDmnNom = P00A823_n477ContagemResultado_NaoCnfDmnNom[0];
            A2030ContagemResultado_NaoCnfDmnGls = P00A823_A2030ContagemResultado_NaoCnfDmnGls[0];
            n2030ContagemResultado_NaoCnfDmnGls = P00A823_n2030ContagemResultado_NaoCnfDmnGls[0];
            A484ContagemResultado_StatusDmn = P00A823_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00A823_n484ContagemResultado_StatusDmn[0];
            A771ContagemResultado_StatusDmnVnc = P00A823_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00A823_n771ContagemResultado_StatusDmnVnc[0];
            A601ContagemResultado_Servico = P00A823_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00A823_n601ContagemResultado_Servico[0];
            A2008ContagemResultado_CntSrvAls = P00A823_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00A823_n2008ContagemResultado_CntSrvAls[0];
            A764ContagemResultado_ServicoGrupo = P00A823_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00A823_n764ContagemResultado_ServicoGrupo[0];
            A1062ContagemResultado_ServicoTela = P00A823_A1062ContagemResultado_ServicoTela[0];
            n1062ContagemResultado_ServicoTela = P00A823_n1062ContagemResultado_ServicoTela[0];
            A897ContagemResultado_ServicoAtivo = P00A823_A897ContagemResultado_ServicoAtivo[0];
            n897ContagemResultado_ServicoAtivo = P00A823_n897ContagemResultado_ServicoAtivo[0];
            A1398ContagemResultado_ServicoNaoRqrAtr = P00A823_A1398ContagemResultado_ServicoNaoRqrAtr[0];
            n1398ContagemResultado_ServicoNaoRqrAtr = P00A823_n1398ContagemResultado_ServicoNaoRqrAtr[0];
            A1636ContagemResultado_ServicoSS = P00A823_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = P00A823_n1636ContagemResultado_ServicoSS[0];
            A1637ContagemResultado_ServicoSSSgl = P00A823_A1637ContagemResultado_ServicoSSSgl[0];
            n1637ContagemResultado_ServicoSSSgl = P00A823_n1637ContagemResultado_ServicoSSSgl[0];
            A2050ContagemResultado_SrvLnhNegCod = P00A823_A2050ContagemResultado_SrvLnhNegCod[0];
            n2050ContagemResultado_SrvLnhNegCod = P00A823_n2050ContagemResultado_SrvLnhNegCod[0];
            A485ContagemResultado_EhValidacao = P00A823_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = P00A823_n485ContagemResultado_EhValidacao[0];
            A514ContagemResultado_Observacao = P00A823_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = P00A823_n514ContagemResultado_Observacao[0];
            A592ContagemResultado_Evidencia = P00A823_A592ContagemResultado_Evidencia[0];
            n592ContagemResultado_Evidencia = P00A823_n592ContagemResultado_Evidencia[0];
            A1350ContagemResultado_DataCadastro = P00A823_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = P00A823_n1350ContagemResultado_DataCadastro[0];
            A1312ContagemResultado_ResponsavelPessCod = P00A823_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = P00A823_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = P00A823_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = P00A823_n1313ContagemResultado_ResponsavelPessNome[0];
            A1180ContagemResultado_Custo = P00A823_A1180ContagemResultado_Custo[0];
            n1180ContagemResultado_Custo = P00A823_n1180ContagemResultado_Custo[0];
            A597ContagemResultado_LoteAceiteCod = P00A823_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00A823_n597ContagemResultado_LoteAceiteCod[0];
            A528ContagemResultado_LoteAceite = P00A823_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00A823_n528ContagemResultado_LoteAceite[0];
            A529ContagemResultado_DataAceite = P00A823_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00A823_n529ContagemResultado_DataAceite[0];
            A525ContagemResultado_AceiteUserCod = P00A823_A525ContagemResultado_AceiteUserCod[0];
            n525ContagemResultado_AceiteUserCod = P00A823_n525ContagemResultado_AceiteUserCod[0];
            A526ContagemResultado_AceitePessoaCod = P00A823_A526ContagemResultado_AceitePessoaCod[0];
            n526ContagemResultado_AceitePessoaCod = P00A823_n526ContagemResultado_AceitePessoaCod[0];
            A527ContagemResultado_AceiteUserNom = P00A823_A527ContagemResultado_AceiteUserNom[0];
            n527ContagemResultado_AceiteUserNom = P00A823_n527ContagemResultado_AceiteUserNom[0];
            A1547ContagemResultado_LoteNFe = P00A823_A1547ContagemResultado_LoteNFe[0];
            n1547ContagemResultado_LoteNFe = P00A823_n1547ContagemResultado_LoteNFe[0];
            A1559ContagemResultado_VlrAceite = P00A823_A1559ContagemResultado_VlrAceite[0];
            n1559ContagemResultado_VlrAceite = P00A823_n1559ContagemResultado_VlrAceite[0];
            A598ContagemResultado_Baseline = P00A823_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00A823_n598ContagemResultado_Baseline[0];
            A602ContagemResultado_OSVinculada = P00A823_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00A823_n602ContagemResultado_OSVinculada[0];
            A603ContagemResultado_DmnVinculada = P00A823_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00A823_n603ContagemResultado_DmnVinculada[0];
            A798ContagemResultado_PFBFSImp = P00A823_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = P00A823_n798ContagemResultado_PFBFSImp[0];
            A799ContagemResultado_PFLFSImp = P00A823_A799ContagemResultado_PFLFSImp[0];
            n799ContagemResultado_PFLFSImp = P00A823_n799ContagemResultado_PFLFSImp[0];
            A1178ContagemResultado_PBFinal = P00A823_A1178ContagemResultado_PBFinal[0];
            n1178ContagemResultado_PBFinal = P00A823_n1178ContagemResultado_PBFinal[0];
            A1179ContagemResultado_PLFinal = P00A823_A1179ContagemResultado_PLFinal[0];
            n1179ContagemResultado_PLFinal = P00A823_n1179ContagemResultado_PLFinal[0];
            A1034ContagemResultadoLiqLog_Data = P00A823_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = P00A823_n1034ContagemResultadoLiqLog_Data[0];
            A1044ContagemResultado_FncUsrCod = P00A823_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = P00A823_n1044ContagemResultado_FncUsrCod[0];
            A1046ContagemResultado_Agrupador = P00A823_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00A823_n1046ContagemResultado_Agrupador[0];
            A1049ContagemResultado_GlsData = P00A823_A1049ContagemResultado_GlsData[0];
            n1049ContagemResultado_GlsData = P00A823_n1049ContagemResultado_GlsData[0];
            A1050ContagemResultado_GlsDescricao = P00A823_A1050ContagemResultado_GlsDescricao[0];
            n1050ContagemResultado_GlsDescricao = P00A823_n1050ContagemResultado_GlsDescricao[0];
            A1051ContagemResultado_GlsValor = P00A823_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P00A823_n1051ContagemResultado_GlsValor[0];
            A1052ContagemResultado_GlsUser = P00A823_A1052ContagemResultado_GlsUser[0];
            n1052ContagemResultado_GlsUser = P00A823_n1052ContagemResultado_GlsUser[0];
            A1389ContagemResultado_RdmnIssueId = P00A823_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = P00A823_n1389ContagemResultado_RdmnIssueId[0];
            A1390ContagemResultado_RdmnProjectId = P00A823_A1390ContagemResultado_RdmnProjectId[0];
            n1390ContagemResultado_RdmnProjectId = P00A823_n1390ContagemResultado_RdmnProjectId[0];
            A1392ContagemResultado_RdmnUpdated = P00A823_A1392ContagemResultado_RdmnUpdated[0];
            n1392ContagemResultado_RdmnUpdated = P00A823_n1392ContagemResultado_RdmnUpdated[0];
            A1444ContagemResultado_CntSrvPrrPrz = P00A823_A1444ContagemResultado_CntSrvPrrPrz[0];
            n1444ContagemResultado_CntSrvPrrPrz = P00A823_n1444ContagemResultado_CntSrvPrrPrz[0];
            A1445ContagemResultado_CntSrvPrrCst = P00A823_A1445ContagemResultado_CntSrvPrrCst[0];
            n1445ContagemResultado_CntSrvPrrCst = P00A823_n1445ContagemResultado_CntSrvPrrCst[0];
            A1457ContagemResultado_TemDpnHmlg = P00A823_A1457ContagemResultado_TemDpnHmlg[0];
            n1457ContagemResultado_TemDpnHmlg = P00A823_n1457ContagemResultado_TemDpnHmlg[0];
            A1519ContagemResultado_TmpEstAnl = P00A823_A1519ContagemResultado_TmpEstAnl[0];
            n1519ContagemResultado_TmpEstAnl = P00A823_n1519ContagemResultado_TmpEstAnl[0];
            A1505ContagemResultado_TmpEstExc = P00A823_A1505ContagemResultado_TmpEstExc[0];
            n1505ContagemResultado_TmpEstExc = P00A823_n1505ContagemResultado_TmpEstExc[0];
            A1506ContagemResultado_TmpEstCrr = P00A823_A1506ContagemResultado_TmpEstCrr[0];
            n1506ContagemResultado_TmpEstCrr = P00A823_n1506ContagemResultado_TmpEstCrr[0];
            A1520ContagemResultado_InicioAnl = P00A823_A1520ContagemResultado_InicioAnl[0];
            n1520ContagemResultado_InicioAnl = P00A823_n1520ContagemResultado_InicioAnl[0];
            A1521ContagemResultado_FimAnl = P00A823_A1521ContagemResultado_FimAnl[0];
            n1521ContagemResultado_FimAnl = P00A823_n1521ContagemResultado_FimAnl[0];
            A1509ContagemResultado_InicioExc = P00A823_A1509ContagemResultado_InicioExc[0];
            n1509ContagemResultado_InicioExc = P00A823_n1509ContagemResultado_InicioExc[0];
            A1510ContagemResultado_FimExc = P00A823_A1510ContagemResultado_FimExc[0];
            n1510ContagemResultado_FimExc = P00A823_n1510ContagemResultado_FimExc[0];
            A1511ContagemResultado_InicioCrr = P00A823_A1511ContagemResultado_InicioCrr[0];
            n1511ContagemResultado_InicioCrr = P00A823_n1511ContagemResultado_InicioCrr[0];
            A1512ContagemResultado_FimCrr = P00A823_A1512ContagemResultado_FimCrr[0];
            n1512ContagemResultado_FimCrr = P00A823_n1512ContagemResultado_FimCrr[0];
            A1515ContagemResultado_Evento = P00A823_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = P00A823_n1515ContagemResultado_Evento[0];
            A1544ContagemResultado_ProjetoCod = P00A823_A1544ContagemResultado_ProjetoCod[0];
            n1544ContagemResultado_ProjetoCod = P00A823_n1544ContagemResultado_ProjetoCod[0];
            A1593ContagemResultado_CntSrvTpVnc = P00A823_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00A823_n1593ContagemResultado_CntSrvTpVnc[0];
            A1594ContagemResultado_CntSrvFtrm = P00A823_A1594ContagemResultado_CntSrvFtrm[0];
            n1594ContagemResultado_CntSrvFtrm = P00A823_n1594ContagemResultado_CntSrvFtrm[0];
            A1596ContagemResultado_CntSrvPrc = P00A823_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00A823_n1596ContagemResultado_CntSrvPrc[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00A823_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00A823_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00A823_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00A823_n866ContagemResultado_ContratadaOrigemSigla[0];
            A807ContagemResultado_ContratadaOrigemPesCod = P00A823_A807ContagemResultado_ContratadaOrigemPesCod[0];
            n807ContagemResultado_ContratadaOrigemPesCod = P00A823_n807ContagemResultado_ContratadaOrigemPesCod[0];
            A867ContagemResultado_ContratadaOrigemAreaCod = P00A823_A867ContagemResultado_ContratadaOrigemAreaCod[0];
            n867ContagemResultado_ContratadaOrigemAreaCod = P00A823_n867ContagemResultado_ContratadaOrigemAreaCod[0];
            A2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            n2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema[0];
            A808ContagemResultado_ContratadaOrigemPesNom = P00A823_A808ContagemResultado_ContratadaOrigemPesNom[0];
            n808ContagemResultado_ContratadaOrigemPesNom = P00A823_n808ContagemResultado_ContratadaOrigemPesNom[0];
            A480ContagemResultado_CrFSPessoaCod = P00A823_A480ContagemResultado_CrFSPessoaCod[0];
            n480ContagemResultado_CrFSPessoaCod = P00A823_n480ContagemResultado_CrFSPessoaCod[0];
            A455ContagemResultado_ContadorFSNom = P00A823_A455ContagemResultado_ContadorFSNom[0];
            n455ContagemResultado_ContadorFSNom = P00A823_n455ContagemResultado_ContadorFSNom[0];
            A495ContagemResultado_SistemaNom = P00A823_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = P00A823_n495ContagemResultado_SistemaNom[0];
            A496Contagemresultado_SistemaAreaCod = P00A823_A496Contagemresultado_SistemaAreaCod[0];
            n496Contagemresultado_SistemaAreaCod = P00A823_n496Contagemresultado_SistemaAreaCod[0];
            A509ContagemrResultado_SistemaSigla = P00A823_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00A823_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P00A823_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P00A823_n515ContagemResultado_SistemaCoord[0];
            A804ContagemResultado_SistemaAtivo = P00A823_A804ContagemResultado_SistemaAtivo[0];
            n804ContagemResultado_SistemaAtivo = P00A823_n804ContagemResultado_SistemaAtivo[0];
            A143Modulo_Nome = P00A823_A143Modulo_Nome[0];
            A145Modulo_Sigla = P00A823_A145Modulo_Sigla[0];
            A477ContagemResultado_NaoCnfDmnNom = P00A823_A477ContagemResultado_NaoCnfDmnNom[0];
            n477ContagemResultado_NaoCnfDmnNom = P00A823_n477ContagemResultado_NaoCnfDmnNom[0];
            A2030ContagemResultado_NaoCnfDmnGls = P00A823_A2030ContagemResultado_NaoCnfDmnGls[0];
            n2030ContagemResultado_NaoCnfDmnGls = P00A823_n2030ContagemResultado_NaoCnfDmnGls[0];
            A1637ContagemResultado_ServicoSSSgl = P00A823_A1637ContagemResultado_ServicoSSSgl[0];
            n1637ContagemResultado_ServicoSSSgl = P00A823_n1637ContagemResultado_ServicoSSSgl[0];
            A528ContagemResultado_LoteAceite = P00A823_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00A823_n528ContagemResultado_LoteAceite[0];
            A529ContagemResultado_DataAceite = P00A823_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00A823_n529ContagemResultado_DataAceite[0];
            A525ContagemResultado_AceiteUserCod = P00A823_A525ContagemResultado_AceiteUserCod[0];
            n525ContagemResultado_AceiteUserCod = P00A823_n525ContagemResultado_AceiteUserCod[0];
            A1547ContagemResultado_LoteNFe = P00A823_A1547ContagemResultado_LoteNFe[0];
            n1547ContagemResultado_LoteNFe = P00A823_n1547ContagemResultado_LoteNFe[0];
            A526ContagemResultado_AceitePessoaCod = P00A823_A526ContagemResultado_AceitePessoaCod[0];
            n526ContagemResultado_AceitePessoaCod = P00A823_n526ContagemResultado_AceitePessoaCod[0];
            A527ContagemResultado_AceiteUserNom = P00A823_A527ContagemResultado_AceiteUserNom[0];
            n527ContagemResultado_AceiteUserNom = P00A823_n527ContagemResultado_AceiteUserNom[0];
            A1765ContagemResultado_CodSrvSSVnc = P00A823_A1765ContagemResultado_CodSrvSSVnc[0];
            n1765ContagemResultado_CodSrvSSVnc = P00A823_n1765ContagemResultado_CodSrvSSVnc[0];
            A1627ContagemResultado_CntSrvVncCod = P00A823_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = P00A823_n1627ContagemResultado_CntSrvVncCod[0];
            A2136ContagemResultado_CntadaOsVinc = P00A823_A2136ContagemResultado_CntadaOsVinc[0];
            n2136ContagemResultado_CntadaOsVinc = P00A823_n2136ContagemResultado_CntadaOsVinc[0];
            A771ContagemResultado_StatusDmnVnc = P00A823_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00A823_n771ContagemResultado_StatusDmnVnc[0];
            A603ContagemResultado_DmnVinculada = P00A823_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00A823_n603ContagemResultado_DmnVinculada[0];
            A1591ContagemResultado_CodSrvVnc = P00A823_A1591ContagemResultado_CodSrvVnc[0];
            n1591ContagemResultado_CodSrvVnc = P00A823_n1591ContagemResultado_CodSrvVnc[0];
            A1622ContagemResultado_CntVncCod = P00A823_A1622ContagemResultado_CntVncCod[0];
            n1622ContagemResultado_CntVncCod = P00A823_n1622ContagemResultado_CntVncCod[0];
            A1590ContagemResultado_SiglaSrvVnc = P00A823_A1590ContagemResultado_SiglaSrvVnc[0];
            n1590ContagemResultado_SiglaSrvVnc = P00A823_n1590ContagemResultado_SiglaSrvVnc[0];
            A2137ContagemResultado_SerGrupoVinc = P00A823_A2137ContagemResultado_SerGrupoVinc[0];
            n2137ContagemResultado_SerGrupoVinc = P00A823_n2137ContagemResultado_SerGrupoVinc[0];
            A1034ContagemResultadoLiqLog_Data = P00A823_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = P00A823_n1034ContagemResultadoLiqLog_Data[0];
            A1312ContagemResultado_ResponsavelPessCod = P00A823_A1312ContagemResultado_ResponsavelPessCod[0];
            n1312ContagemResultado_ResponsavelPessCod = P00A823_n1312ContagemResultado_ResponsavelPessCod[0];
            A1313ContagemResultado_ResponsavelPessNome = P00A823_A1313ContagemResultado_ResponsavelPessNome[0];
            n1313ContagemResultado_ResponsavelPessNome = P00A823_n1313ContagemResultado_ResponsavelPessNome[0];
            A2118ContagemResultado_Owner_Identificao = P00A823_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00A823_n2118ContagemResultado_Owner_Identificao[0];
            A531ContagemResultado_StatusUltCnt = P00A823_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P00A823_A566ContagemResultado_DataUltCnt[0];
            A825ContagemResultado_HoraUltCnt = P00A823_A825ContagemResultado_HoraUltCnt[0];
            A584ContagemResultado_ContadorFM = P00A823_A584ContagemResultado_ContadorFM[0];
            A682ContagemResultado_PFBFMUltima = P00A823_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = P00A823_A683ContagemResultado_PFLFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00A823_A684ContagemResultado_PFBFSUltima[0];
            A1480ContagemResultado_CstUntUltima = P00A823_A1480ContagemResultado_CstUntUltima[0];
            A685ContagemResultado_PFLFSUltima = P00A823_A685ContagemResultado_PFLFSUltima[0];
            A802ContagemResultado_DeflatorCnt = P00A823_A802ContagemResultado_DeflatorCnt[0];
            A578ContagemResultado_CntComNaoCnf = P00A823_A578ContagemResultado_CntComNaoCnf[0];
            n578ContagemResultado_CntComNaoCnf = P00A823_n578ContagemResultado_CntComNaoCnf[0];
            A510ContagemResultado_EsforcoSoma = P00A823_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00A823_n510ContagemResultado_EsforcoSoma[0];
            A1353ContagemResultado_ContratadaDoOwner = P00A823_A1353ContagemResultado_ContratadaDoOwner[0];
            n1353ContagemResultado_ContratadaDoOwner = P00A823_n1353ContagemResultado_ContratadaDoOwner[0];
            A1602ContagemResultado_GlsIndValor = P00A823_A1602ContagemResultado_GlsIndValor[0];
            n1602ContagemResultado_GlsIndValor = P00A823_n1602ContagemResultado_GlsIndValor[0];
            A1701ContagemResultado_TemProposta = P00A823_A1701ContagemResultado_TemProposta[0];
            n1701ContagemResultado_TemProposta = P00A823_n1701ContagemResultado_TemProposta[0];
            A52Contratada_AreaTrabalhoCod = P00A823_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00A823_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00A823_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00A823_n803ContagemResultado_ContratadaSigla[0];
            A499ContagemResultado_ContratadaPessoaCod = P00A823_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00A823_n499ContagemResultado_ContratadaPessoaCod[0];
            A1326ContagemResultado_ContratadaTipoFab = P00A823_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00A823_n1326ContagemResultado_ContratadaTipoFab[0];
            A500ContagemResultado_ContratadaPessoaNom = P00A823_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00A823_n500ContagemResultado_ContratadaPessoaNom[0];
            A1817ContagemResultado_ContratadaCNPJ = P00A823_A1817ContagemResultado_ContratadaCNPJ[0];
            n1817ContagemResultado_ContratadaCNPJ = P00A823_n1817ContagemResultado_ContratadaCNPJ[0];
            A1600ContagemResultado_CntSrvSttPgmFnc = P00A823_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = P00A823_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A1601ContagemResultado_CntSrvIndDvr = P00A823_A1601ContagemResultado_CntSrvIndDvr[0];
            n1601ContagemResultado_CntSrvIndDvr = P00A823_n1601ContagemResultado_CntSrvIndDvr[0];
            A1613ContagemResultado_CntSrvQtdUntCns = P00A823_A1613ContagemResultado_CntSrvQtdUntCns[0];
            n1613ContagemResultado_CntSrvQtdUntCns = P00A823_n1613ContagemResultado_CntSrvQtdUntCns[0];
            A1620ContagemResultado_CntSrvUndCnt = P00A823_A1620ContagemResultado_CntSrvUndCnt[0];
            n1620ContagemResultado_CntSrvUndCnt = P00A823_n1620ContagemResultado_CntSrvUndCnt[0];
            A1623ContagemResultado_CntSrvMmn = P00A823_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P00A823_n1623ContagemResultado_CntSrvMmn[0];
            A1607ContagemResultado_PrzAnl = P00A823_A1607ContagemResultado_PrzAnl[0];
            n1607ContagemResultado_PrzAnl = P00A823_n1607ContagemResultado_PrzAnl[0];
            A1609ContagemResultado_PrzCrr = P00A823_A1609ContagemResultado_PrzCrr[0];
            n1609ContagemResultado_PrzCrr = P00A823_n1609ContagemResultado_PrzCrr[0];
            A1618ContagemResultado_PrzRsp = P00A823_A1618ContagemResultado_PrzRsp[0];
            n1618ContagemResultado_PrzRsp = P00A823_n1618ContagemResultado_PrzRsp[0];
            A1619ContagemResultado_PrzGrt = P00A823_A1619ContagemResultado_PrzGrt[0];
            n1619ContagemResultado_PrzGrt = P00A823_n1619ContagemResultado_PrzGrt[0];
            A1611ContagemResultado_PrzTpDias = P00A823_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00A823_n1611ContagemResultado_PrzTpDias[0];
            A1603ContagemResultado_CntCod = P00A823_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00A823_n1603ContagemResultado_CntCod[0];
            A1650ContagemResultado_PrzInc = P00A823_A1650ContagemResultado_PrzInc[0];
            n1650ContagemResultado_PrzInc = P00A823_n1650ContagemResultado_PrzInc[0];
            A601ContagemResultado_Servico = P00A823_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00A823_n601ContagemResultado_Servico[0];
            A2008ContagemResultado_CntSrvAls = P00A823_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00A823_n2008ContagemResultado_CntSrvAls[0];
            A1398ContagemResultado_ServicoNaoRqrAtr = P00A823_A1398ContagemResultado_ServicoNaoRqrAtr[0];
            n1398ContagemResultado_ServicoNaoRqrAtr = P00A823_n1398ContagemResultado_ServicoNaoRqrAtr[0];
            A1593ContagemResultado_CntSrvTpVnc = P00A823_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00A823_n1593ContagemResultado_CntSrvTpVnc[0];
            A1594ContagemResultado_CntSrvFtrm = P00A823_A1594ContagemResultado_CntSrvFtrm[0];
            n1594ContagemResultado_CntSrvFtrm = P00A823_n1594ContagemResultado_CntSrvFtrm[0];
            A1596ContagemResultado_CntSrvPrc = P00A823_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00A823_n1596ContagemResultado_CntSrvPrc[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00A823_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00A823_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1598ContagemResultado_ServicoNome = P00A823_A1598ContagemResultado_ServicoNome[0];
            n1598ContagemResultado_ServicoNome = P00A823_n1598ContagemResultado_ServicoNome[0];
            A1599ContagemResultado_ServicoTpHrrq = P00A823_A1599ContagemResultado_ServicoTpHrrq[0];
            n1599ContagemResultado_ServicoTpHrrq = P00A823_n1599ContagemResultado_ServicoTpHrrq[0];
            A801ContagemResultado_ServicoSigla = P00A823_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00A823_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P00A823_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00A823_n764ContagemResultado_ServicoGrupo[0];
            A1062ContagemResultado_ServicoTela = P00A823_A1062ContagemResultado_ServicoTela[0];
            n1062ContagemResultado_ServicoTela = P00A823_n1062ContagemResultado_ServicoTela[0];
            A897ContagemResultado_ServicoAtivo = P00A823_A897ContagemResultado_ServicoAtivo[0];
            n897ContagemResultado_ServicoAtivo = P00A823_n897ContagemResultado_ServicoAtivo[0];
            A2050ContagemResultado_SrvLnhNegCod = P00A823_A2050ContagemResultado_SrvLnhNegCod[0];
            n2050ContagemResultado_SrvLnhNegCod = P00A823_n2050ContagemResultado_SrvLnhNegCod[0];
            A1621ContagemResultado_CntSrvUndCntSgl = P00A823_A1621ContagemResultado_CntSrvUndCntSgl[0];
            n1621ContagemResultado_CntSrvUndCntSgl = P00A823_n1621ContagemResultado_CntSrvUndCntSgl[0];
            A2209ContagemResultado_CntSrvUndCntNome = P00A823_A2209ContagemResultado_CntSrvUndCntNome[0];
            n2209ContagemResultado_CntSrvUndCntNome = P00A823_n2209ContagemResultado_CntSrvUndCntNome[0];
            A1612ContagemResultado_CntNum = P00A823_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00A823_n1612ContagemResultado_CntNum[0];
            A1604ContagemResultado_CntPrpCod = P00A823_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P00A823_n1604ContagemResultado_CntPrpCod[0];
            A1615ContagemResultado_CntIndDvr = P00A823_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = P00A823_n1615ContagemResultado_CntIndDvr[0];
            A1617ContagemResultado_CntClcDvr = P00A823_A1617ContagemResultado_CntClcDvr[0];
            n1617ContagemResultado_CntClcDvr = P00A823_n1617ContagemResultado_CntClcDvr[0];
            A1616ContagemResultado_CntVlrUndCnt = P00A823_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00A823_n1616ContagemResultado_CntVlrUndCnt[0];
            A1632ContagemResultado_CntPrdFtrCada = P00A823_A1632ContagemResultado_CntPrdFtrCada[0];
            n1632ContagemResultado_CntPrdFtrCada = P00A823_n1632ContagemResultado_CntPrdFtrCada[0];
            A1633ContagemResultado_CntPrdFtrIni = P00A823_A1633ContagemResultado_CntPrdFtrIni[0];
            n1633ContagemResultado_CntPrdFtrIni = P00A823_n1633ContagemResultado_CntPrdFtrIni[0];
            A1634ContagemResultado_CntPrdFtrFim = P00A823_A1634ContagemResultado_CntPrdFtrFim[0];
            n1634ContagemResultado_CntPrdFtrFim = P00A823_n1634ContagemResultado_CntPrdFtrFim[0];
            A2082ContagemResultado_CntLmtFtr = P00A823_A2082ContagemResultado_CntLmtFtr[0];
            n2082ContagemResultado_CntLmtFtr = P00A823_n2082ContagemResultado_CntLmtFtr[0];
            A1624ContagemResultado_DatVgnInc = P00A823_A1624ContagemResultado_DatVgnInc[0];
            n1624ContagemResultado_DatVgnInc = P00A823_n1624ContagemResultado_DatVgnInc[0];
            A1605ContagemResultado_CntPrpPesCod = P00A823_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P00A823_n1605ContagemResultado_CntPrpPesCod[0];
            A1606ContagemResultado_CntPrpPesNom = P00A823_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P00A823_n1606ContagemResultado_CntPrpPesNom[0];
            A1608ContagemResultado_PrzExc = P00A823_A1608ContagemResultado_PrzExc[0];
            n1608ContagemResultado_PrzExc = P00A823_n1608ContagemResultado_PrzExc[0];
            A1610ContagemResultado_PrzTp = P00A823_A1610ContagemResultado_PrzTp[0];
            n1610ContagemResultado_PrzTp = P00A823_n1610ContagemResultado_PrzTp[0];
            A1625ContagemResultado_DatIncTA = P00A823_A1625ContagemResultado_DatIncTA[0];
            n1625ContagemResultado_DatIncTA = P00A823_n1625ContagemResultado_DatIncTA[0];
            GXt_decimal1 = A497IndiceDivergencia;
            new prc_indicedivergencia(context ).execute( ref  A1553ContagemResultado_CntSrvCod, out  GXt_decimal1) ;
            A497IndiceDivergencia = GXt_decimal1;
            GXt_char2 = A498CalculoDivergencia;
            new prc_calculodivergencia(context ).execute(  A490ContagemResultado_ContratadaCod, out  GXt_char2) ;
            A498CalculoDivergencia = GXt_char2;
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            A553ContagemResultado_DemandaFM_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A493ContagemResultado_DemandaFM, 1, 18), "."));
            A1815ContagemResultado_DmnSrvPrst = StringUtil.Trim( A493ContagemResultado_DemandaFM) + " " + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " (" + A803ContagemResultado_ContratadaSigla + ")";
            A599ContagemResultado_Demanda_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A457ContagemResultado_Demanda, 1, 18), "."));
            GXt_boolean3 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean3) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean3;
            GXt_int4 = A1236ContagemResultado_ContratanteDoResponsavel;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int4) ;
            A1236ContagemResultado_ContratanteDoResponsavel = GXt_int4;
            GXt_int4 = A1352ContagemResultado_ContratanteDoOwner;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A508ContagemResultado_Owner, out  GXt_int4) ;
            A1352ContagemResultado_ContratanteDoOwner = GXt_int4;
            GXt_char2 = A486ContagemResultado_EsforcoTotal;
            new prc_contageresultado_esforcototal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_char2) ;
            A486ContagemResultado_EsforcoTotal = GXt_char2;
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            GXt_int5 = A585ContagemResultado_Erros;
            new prc_pendenciasdaos(context ).execute(  A456ContagemResultado_Codigo, out  GXt_int5) ;
            A585ContagemResultado_Erros = GXt_int5;
            GXt_int4 = A1229ContagemResultado_ContratadaDoResponsavel;
            new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int4) ;
            A1229ContagemResultado_ContratadaDoResponsavel = GXt_int4;
            if ( ! P00A823_n1043ContagemResultado_LiqLogCod[0] )
            {
               A1028ContagemResultado_Liquidada = true;
            }
            else
            {
               if ( P00A823_n1043ContagemResultado_LiqLogCod[0] )
               {
                  A1028ContagemResultado_Liquidada = false;
               }
               else
               {
                  A1028ContagemResultado_Liquidada = false;
               }
            }
            GXt_char2 = A2091ContagemResultado_CntSrvPrrNome;
            new prc_buscaprioridadeos(context ).execute(  A1443ContagemResultado_CntSrvPrrCod, out  GXt_char2) ;
            A2091ContagemResultado_CntSrvPrrNome = GXt_char2;
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContagemResultado";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Resultado_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataDmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataInicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1790ContagemResultado_DataInicio, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataEntrega";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de Entrega";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A472ContagemResultado_DataEntrega, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_HoraEntrega";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Hora de Entrega";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataPrevista";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data Prevista";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataExecucao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1349ContagemResultado_DataExecucao, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataEntregaReal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Entrega Real";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataHomologacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Homologa��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1348ContagemResultado_DataHomologacao, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrazoInicialDias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo inicial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrazoMaisDias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo extendido";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1237ContagemResultado_PrazoMaisDias), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prestadora";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaPessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaPessoaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A500ContagemResultado_ContratadaPessoaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prestadora";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A803ContagemResultado_ContratadaSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaTipoFab";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de F�brica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1326ContagemResultado_ContratadaTipoFab;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaCNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1817ContagemResultado_ContratadaCNPJ;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "IndiceDivergencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice Diverg�ncia:";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A497IndiceDivergencia, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "CalculoDivergencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usando:";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A498CalculoDivergencia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Origem";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Origem";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A866ContagemResultado_ContratadaOrigemSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemPesCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Contratada Origem Pes Cod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A807ContagemResultado_ContratadaOrigemPesCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemPesNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada Origem";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A808ContagemResultado_ContratadaOrigemPesNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemAreaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A867ContagemResultado_ContratadaOrigemAreaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaOrigemUsaSistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada Usa o Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2093ContagemResultado_ContratadaOrigemUsaSistema);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContadorFSCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CrFSPessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A480ContagemResultado_CrFSPessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContadorFSNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A455ContagemResultado_ContadorFSNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Demanda";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� Refer�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A457ContagemResultado_Demanda;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DmnSrvPrst";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Demanda Servi�o Prestadora";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1815ContagemResultado_DmnSrvPrst;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Demanda_ORDER";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Demanda_ORDER";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A599ContagemResultado_Demanda_ORDER), 18, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Erros";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Erros";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A585ContagemResultado_Erros), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DemandaFM";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A493ContagemResultado_DemandaFM;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DemandaFM_ORDER";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Demanda FM_ORDER";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A553ContagemResultado_DemandaFM_ORDER), 18, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N� SS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_OsFsOsFm";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS Ref|OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A501ContagemResultado_OsFsOsFm;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_OSManual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Solicita��o manual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1173ContagemResultado_OSManual);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Titulo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A494ContagemResultado_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Link";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Link";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A465ContagemResultado_Link;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A495ContagemResultado_SistemaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contagemresultado_SistemaAreaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A496Contagemresultado_SistemaAreaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemrResultado_SistemaSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A509ContagemrResultado_SistemaSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaCoord";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Coordena��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A515ContagemResultado_SistemaCoord;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SistemaAtivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A804ContagemResultado_SistemaAtivo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Modulo_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "M�dulo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Modulo_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "M�dulo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A143Modulo_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Modulo_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A145Modulo_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_NaoCnfDmnCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pend�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_NaoCnfDmnNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�o Conformidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A477ContagemResultado_NaoCnfDmnNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_NaoCnfDmnGls";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Glos�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2030ContagemResultado_NaoCnfDmnGls);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_StatusDmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A484ContagemResultado_StatusDmn;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_StatusUltCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status Cnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_StatusDmnVnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status Dmn Vinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A771ContagemResultado_StatusDmnVnc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataUltCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data da Cnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_HoraUltCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Hora Ult Cnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A825ContagemResultado_HoraUltCnt;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntComNaoCnf";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Com Nao Cnf";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A578ContagemResultado_CntComNaoCnf), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContadorFM";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contador FM";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A584ContagemResultado_ContadorFM), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFBFMUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PFBFM Ultima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFLFMUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFLFMUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A683ContagemResultado_PFLFMUltima, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFBFSUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFBFSUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CstUntUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cst Unt Ultima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1480ContagemResultado_CstUntUltima, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFLFSUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_PFLFSUltima";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A685ContagemResultado_PFLFSUltima, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DeflatorCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Deflator";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A802ContagemResultado_DeflatorCnt, 6, 3);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Servico";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo no Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvAls";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Srv Als";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2008ContagemResultado_CntSrvAls;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoGrupo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo de Servi�os";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A764ContagemResultado_ServicoGrupo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A801ContagemResultado_ServicoSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoTela";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tela";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1062ContagemResultado_ServicoTela;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoAtivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A897ContagemResultado_ServicoAtivo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoNaoRqrAtr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Servico Nao Rqr Atr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1398ContagemResultado_ServicoNaoRqrAtr);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoSS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o solicitado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoSSSgl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1637ContagemResultado_ServicoSSSgl;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SrvLnhNegCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Linha de Neg�cio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2050ContagemResultado_SrvLnhNegCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_EhValidacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A485ContagemResultado_EhValidacao);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_EsforcoTotal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Esfor�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A486ContagemResultado_EsforcoTotal;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_EsforcoSoma";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Esforco Soma";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A510ContagemResultado_EsforcoSoma), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Observacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Observa��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A514ContagemResultado_Observacao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Evidencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A592ContagemResultado_Evidencia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataCadastro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data de Cadastro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Owner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Criador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Owner_Identificao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Criador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2118ContagemResultado_Owner_Identificao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratanteDoOwner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratante do Owner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1352ContagemResultado_ContratanteDoOwner), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaDoOwner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Contratada Do Owner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1353ContagemResultado_ContratadaDoOwner), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Responsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ResponsavelPessCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1312ContagemResultado_ResponsavelPessCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ResponsavelPessNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1313ContagemResultado_ResponsavelPessNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratadaDoResponsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada do Responsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ContratanteDoResponsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratante do Responsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ValorPF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da Unidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A512ContagemResultado_ValorPF, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Custo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Custo da Unidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1180ContagemResultado_Custo, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ValorFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor Final";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LoteAceiteCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Lote";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LoteAceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Lote";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A528ContagemResultado_LoteAceite;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataAceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data do Aceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A529ContagemResultado_DataAceite, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_AceiteUserCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel do Aceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A525ContagemResultado_AceiteUserCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_AceitePessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel do Aceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A526ContagemResultado_AceitePessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_AceiteUserNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel do Aceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A527ContagemResultado_AceiteUserNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LoteNFe";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "NFe do Lote de Aceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1547ContagemResultado_LoteNFe), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_VlrAceite";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor faturado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1559ContagemResultado_VlrAceite, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Baseline";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Baseline";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A598ContagemResultado_Baseline);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_OSVinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Os vinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DmnVinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A603ContagemResultado_DmnVinculada;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFBFSImp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "PF FS Importados";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFLFSImp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "FS Importado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A799ContagemResultado_PFLFSImp, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PBFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Bruto final";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1178ContagemResultado_PBFinal, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PLFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Liquido final";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1179ContagemResultado_PLFinal, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_LiqLogCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Liq Log Cod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Liquidada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Liquidada ao Prestador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1028ContagemResultado_Liquidada);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultadoLiqLog_Data";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FncUsrCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fun��o de Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Agrupador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Agrupador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1046ContagemResultado_Agrupador;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsData";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data da glosa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1049ContagemResultado_GlsData, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsDescricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1050ContagemResultado_GlsDescricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsValor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1051ContagemResultado_GlsValor, 12, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsUser";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1052ContagemResultado_GlsUser), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_RdmnIssueId";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Issue Id";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_RdmnProjectId";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Project Id";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1390ContagemResultado_RdmnProjectId), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_RdmnUpdated";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ultima atualiza��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1392ContagemResultado_RdmnUpdated, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "da prioridade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o da Prioridade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2091ContagemResultado_CntSrvPrrNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrPrz";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo da prioridade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1444ContagemResultado_CntSrvPrrPrz, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrrCst";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Custo da prioridade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1445ContagemResultado_CntSrvPrrCst, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TemDpnHmlg";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tem depend�ncia para homologar?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1457ContagemResultado_TemDpnHmlg);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TemPndHmlg";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tem pend�ncia para homologar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1802ContagemResultado_TemPndHmlg);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TmpEstAnl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tempo estimado de an�lise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1519ContagemResultado_TmpEstAnl), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TmpEstExc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tempo estimado de execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1505ContagemResultado_TmpEstExc), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TmpEstCrr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tempo estimado de corre��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1506ContagemResultado_TmpEstCrr), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_InicioAnl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio do an�lise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1520ContagemResultado_InicioAnl, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FimAnl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim do an�lise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1521ContagemResultado_FimAnl, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_InicioExc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio da execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1509ContagemResultado_InicioExc, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FimExc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim da execu��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1510ContagemResultado_FimExc, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_InicioCrr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio da corre��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1511ContagemResultado_InicioCrr, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_FimCrr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim da corre��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1512ContagemResultado_FimCrr, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Evento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Evento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ProjetoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Projeto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1544ContagemResultado_ProjetoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvTpVnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de vinculo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1593ContagemResultado_CntSrvTpVnc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvFtrm";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faturamento sob";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1594ContagemResultado_CntSrvFtrm;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvPrc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Percentual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1596ContagemResultado_CntSrvPrc, 7, 3);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvVlrUndCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor unidade contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1597ContagemResultado_CntSrvVlrUndCnt, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvSttPgmFnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status pagamento de funcion�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1600ContagemResultado_CntSrvSttPgmFnc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvIndDvr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice aceitavel de diverg�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1601ContagemResultado_CntSrvIndDvr, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvQtdUntCns";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quantidade unit�ria de consumo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvUndCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade de medi��o contratda";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1620ContagemResultado_CntSrvUndCnt), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvUndCntSgl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla da unidade contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1621ContagemResultado_CntSrvUndCntSgl;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvUndCntNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome da Unidade Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2209ContagemResultado_CntSrvUndCntNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvMmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Momento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1623ContagemResultado_CntSrvMmn;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzAnl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de an�lise";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1607ContagemResultado_PrzAnl), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzExc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prz Exc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1608ContagemResultado_PrzExc), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzCrr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prz Crr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1609ContagemResultado_PrzCrr), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzRsp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de resposta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1618ContagemResultado_PrzRsp), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzGrt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prazo de garantia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1619ContagemResultado_PrzGrt), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzTp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prz Tp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1610ContagemResultado_PrzTp;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzTpDias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias Corridos / Uteis";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1611ContagemResultado_PrzTpDias;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1603ContagemResultado_CntCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntNum";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Numero do Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1612ContagemResultado_CntNum;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrpCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preposto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1604ContagemResultado_CntPrpCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrpPesCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prp Pes Cod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1605ContagemResultado_CntPrpPesCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrpPesNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prp Pes Nom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1606ContagemResultado_CntPrpPesNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntIndDvr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indice de aceita��o de diverg�ncia do contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1615ContagemResultado_CntIndDvr, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntClcDvr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo da diverg�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1617ContagemResultado_CntClcDvr;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntVlrUndCnt";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor da unidade contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1616ContagemResultado_CntVlrUndCnt, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrdFtrCada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prd Ftr Cada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1632ContagemResultado_CntPrdFtrCada;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrdFtrIni";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prd Ftr Ini";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1633ContagemResultado_CntPrdFtrIni), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntPrdFtrFim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cnt Prd Ftr Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1634ContagemResultado_CntPrdFtrFim), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntLmtFtr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Limite de Faturamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2082ContagemResultado_CntLmtFtr, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DatVgnInc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data vigencia inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1624ContagemResultado_DatVgnInc, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DatIncTA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data TA inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1625ContagemResultado_DatIncTA, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome do Servico";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1598ContagemResultado_ServicoNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_ServicoTpHrrq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de hierarquia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1599ContagemResultado_ServicoTpHrrq), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CodSrvVnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Cod Srv Vnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1591ContagemResultado_CodSrvVnc), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SiglaSrvVnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla servi�o vinculado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1590ContagemResultado_SiglaSrvVnc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CodSrvSSVnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o SS vinculado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1765ContagemResultado_CodSrvSSVnc), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntSrvVncCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato servico da OS vinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1627ContagemResultado_CntSrvVncCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntVncCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato da OS Vinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1622ContagemResultado_CntVncCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_CntadaOsVinc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada da Os Vinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2136ContagemResultado_CntadaOsVinc), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SerGrupoVinc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo de Servi�os do Servi�o da OS Vinculada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2137ContagemResultado_SerGrupoVinc), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_GlsIndValor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor das glosas dos indicadores";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1602ContagemResultado_GlsIndValor, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TipoRegistro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Tipo Registro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_UOOwner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_UOOwner";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Referencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Referencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1585ContagemResultado_Referencia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Restricoes";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Restricoes";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1586ContagemResultado_Restricoes;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrioridadePrevista";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contagem Resultado_Prioridade Prevista";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1587ContagemResultado_PrioridadePrevista;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PrzInc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio dos prazos";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1650ContagemResultado_PrzInc), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_TemProposta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tem propostas?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1701ContagemResultado_TemProposta);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Combinada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Combinada?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1714ContagemResultado_Combinada);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_Entrega";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Entrega";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1762ContagemResultado_Entrega), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_SemCusto";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sem custo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1791ContagemResultado_SemCusto);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_VlrCnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor do cancelamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1854ContagemResultado_VlrCnc, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_PFCnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidades remuneradas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1855ContagemResultado_PFCnc, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_DataPrvPgm";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data prevista de pagamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1903ContagemResultado_DataPrvPgm, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContagemResultado_QuantidadeSolicitada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Quantidade Solicitada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2133ContagemResultado_QuantidadeSolicitada, 9, 4);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00A823_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) )
                  {
                     A1600ContagemResultado_CntSrvSttPgmFnc = P00A823_A1600ContagemResultado_CntSrvSttPgmFnc[0];
                     n1600ContagemResultado_CntSrvSttPgmFnc = P00A823_n1600ContagemResultado_CntSrvSttPgmFnc[0];
                     A1601ContagemResultado_CntSrvIndDvr = P00A823_A1601ContagemResultado_CntSrvIndDvr[0];
                     n1601ContagemResultado_CntSrvIndDvr = P00A823_n1601ContagemResultado_CntSrvIndDvr[0];
                     A1613ContagemResultado_CntSrvQtdUntCns = P00A823_A1613ContagemResultado_CntSrvQtdUntCns[0];
                     n1613ContagemResultado_CntSrvQtdUntCns = P00A823_n1613ContagemResultado_CntSrvQtdUntCns[0];
                     A1620ContagemResultado_CntSrvUndCnt = P00A823_A1620ContagemResultado_CntSrvUndCnt[0];
                     n1620ContagemResultado_CntSrvUndCnt = P00A823_n1620ContagemResultado_CntSrvUndCnt[0];
                     A1621ContagemResultado_CntSrvUndCntSgl = P00A823_A1621ContagemResultado_CntSrvUndCntSgl[0];
                     n1621ContagemResultado_CntSrvUndCntSgl = P00A823_n1621ContagemResultado_CntSrvUndCntSgl[0];
                     A2209ContagemResultado_CntSrvUndCntNome = P00A823_A2209ContagemResultado_CntSrvUndCntNome[0];
                     n2209ContagemResultado_CntSrvUndCntNome = P00A823_n2209ContagemResultado_CntSrvUndCntNome[0];
                     A1623ContagemResultado_CntSrvMmn = P00A823_A1623ContagemResultado_CntSrvMmn[0];
                     n1623ContagemResultado_CntSrvMmn = P00A823_n1623ContagemResultado_CntSrvMmn[0];
                     A1607ContagemResultado_PrzAnl = P00A823_A1607ContagemResultado_PrzAnl[0];
                     n1607ContagemResultado_PrzAnl = P00A823_n1607ContagemResultado_PrzAnl[0];
                     A1609ContagemResultado_PrzCrr = P00A823_A1609ContagemResultado_PrzCrr[0];
                     n1609ContagemResultado_PrzCrr = P00A823_n1609ContagemResultado_PrzCrr[0];
                     A1618ContagemResultado_PrzRsp = P00A823_A1618ContagemResultado_PrzRsp[0];
                     n1618ContagemResultado_PrzRsp = P00A823_n1618ContagemResultado_PrzRsp[0];
                     A1619ContagemResultado_PrzGrt = P00A823_A1619ContagemResultado_PrzGrt[0];
                     n1619ContagemResultado_PrzGrt = P00A823_n1619ContagemResultado_PrzGrt[0];
                     A1611ContagemResultado_PrzTpDias = P00A823_A1611ContagemResultado_PrzTpDias[0];
                     n1611ContagemResultado_PrzTpDias = P00A823_n1611ContagemResultado_PrzTpDias[0];
                     A1603ContagemResultado_CntCod = P00A823_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = P00A823_n1603ContagemResultado_CntCod[0];
                     A1612ContagemResultado_CntNum = P00A823_A1612ContagemResultado_CntNum[0];
                     n1612ContagemResultado_CntNum = P00A823_n1612ContagemResultado_CntNum[0];
                     A1604ContagemResultado_CntPrpCod = P00A823_A1604ContagemResultado_CntPrpCod[0];
                     n1604ContagemResultado_CntPrpCod = P00A823_n1604ContagemResultado_CntPrpCod[0];
                     A1605ContagemResultado_CntPrpPesCod = P00A823_A1605ContagemResultado_CntPrpPesCod[0];
                     n1605ContagemResultado_CntPrpPesCod = P00A823_n1605ContagemResultado_CntPrpPesCod[0];
                     A1606ContagemResultado_CntPrpPesNom = P00A823_A1606ContagemResultado_CntPrpPesNom[0];
                     n1606ContagemResultado_CntPrpPesNom = P00A823_n1606ContagemResultado_CntPrpPesNom[0];
                     A1615ContagemResultado_CntIndDvr = P00A823_A1615ContagemResultado_CntIndDvr[0];
                     n1615ContagemResultado_CntIndDvr = P00A823_n1615ContagemResultado_CntIndDvr[0];
                     A1617ContagemResultado_CntClcDvr = P00A823_A1617ContagemResultado_CntClcDvr[0];
                     n1617ContagemResultado_CntClcDvr = P00A823_n1617ContagemResultado_CntClcDvr[0];
                     A1616ContagemResultado_CntVlrUndCnt = P00A823_A1616ContagemResultado_CntVlrUndCnt[0];
                     n1616ContagemResultado_CntVlrUndCnt = P00A823_n1616ContagemResultado_CntVlrUndCnt[0];
                     A1632ContagemResultado_CntPrdFtrCada = P00A823_A1632ContagemResultado_CntPrdFtrCada[0];
                     n1632ContagemResultado_CntPrdFtrCada = P00A823_n1632ContagemResultado_CntPrdFtrCada[0];
                     A1633ContagemResultado_CntPrdFtrIni = P00A823_A1633ContagemResultado_CntPrdFtrIni[0];
                     n1633ContagemResultado_CntPrdFtrIni = P00A823_n1633ContagemResultado_CntPrdFtrIni[0];
                     A1634ContagemResultado_CntPrdFtrFim = P00A823_A1634ContagemResultado_CntPrdFtrFim[0];
                     n1634ContagemResultado_CntPrdFtrFim = P00A823_n1634ContagemResultado_CntPrdFtrFim[0];
                     A2082ContagemResultado_CntLmtFtr = P00A823_A2082ContagemResultado_CntLmtFtr[0];
                     n2082ContagemResultado_CntLmtFtr = P00A823_n2082ContagemResultado_CntLmtFtr[0];
                     A1624ContagemResultado_DatVgnInc = P00A823_A1624ContagemResultado_DatVgnInc[0];
                     n1624ContagemResultado_DatVgnInc = P00A823_n1624ContagemResultado_DatVgnInc[0];
                     A1598ContagemResultado_ServicoNome = P00A823_A1598ContagemResultado_ServicoNome[0];
                     n1598ContagemResultado_ServicoNome = P00A823_n1598ContagemResultado_ServicoNome[0];
                     A1599ContagemResultado_ServicoTpHrrq = P00A823_A1599ContagemResultado_ServicoTpHrrq[0];
                     n1599ContagemResultado_ServicoTpHrrq = P00A823_n1599ContagemResultado_ServicoTpHrrq[0];
                     A1591ContagemResultado_CodSrvVnc = P00A823_A1591ContagemResultado_CodSrvVnc[0];
                     n1591ContagemResultado_CodSrvVnc = P00A823_n1591ContagemResultado_CodSrvVnc[0];
                     A1590ContagemResultado_SiglaSrvVnc = P00A823_A1590ContagemResultado_SiglaSrvVnc[0];
                     n1590ContagemResultado_SiglaSrvVnc = P00A823_n1590ContagemResultado_SiglaSrvVnc[0];
                     A1765ContagemResultado_CodSrvSSVnc = P00A823_A1765ContagemResultado_CodSrvSSVnc[0];
                     n1765ContagemResultado_CodSrvSSVnc = P00A823_n1765ContagemResultado_CodSrvSSVnc[0];
                     A1627ContagemResultado_CntSrvVncCod = P00A823_A1627ContagemResultado_CntSrvVncCod[0];
                     n1627ContagemResultado_CntSrvVncCod = P00A823_n1627ContagemResultado_CntSrvVncCod[0];
                     A1622ContagemResultado_CntVncCod = P00A823_A1622ContagemResultado_CntVncCod[0];
                     n1622ContagemResultado_CntVncCod = P00A823_n1622ContagemResultado_CntVncCod[0];
                     A2136ContagemResultado_CntadaOsVinc = P00A823_A2136ContagemResultado_CntadaOsVinc[0];
                     n2136ContagemResultado_CntadaOsVinc = P00A823_n2136ContagemResultado_CntadaOsVinc[0];
                     A2137ContagemResultado_SerGrupoVinc = P00A823_A2137ContagemResultado_SerGrupoVinc[0];
                     n2137ContagemResultado_SerGrupoVinc = P00A823_n2137ContagemResultado_SerGrupoVinc[0];
                     A1583ContagemResultado_TipoRegistro = P00A823_A1583ContagemResultado_TipoRegistro[0];
                     A1584ContagemResultado_UOOwner = P00A823_A1584ContagemResultado_UOOwner[0];
                     n1584ContagemResultado_UOOwner = P00A823_n1584ContagemResultado_UOOwner[0];
                     A1585ContagemResultado_Referencia = P00A823_A1585ContagemResultado_Referencia[0];
                     n1585ContagemResultado_Referencia = P00A823_n1585ContagemResultado_Referencia[0];
                     A1586ContagemResultado_Restricoes = P00A823_A1586ContagemResultado_Restricoes[0];
                     n1586ContagemResultado_Restricoes = P00A823_n1586ContagemResultado_Restricoes[0];
                     A1587ContagemResultado_PrioridadePrevista = P00A823_A1587ContagemResultado_PrioridadePrevista[0];
                     n1587ContagemResultado_PrioridadePrevista = P00A823_n1587ContagemResultado_PrioridadePrevista[0];
                     A1650ContagemResultado_PrzInc = P00A823_A1650ContagemResultado_PrzInc[0];
                     n1650ContagemResultado_PrzInc = P00A823_n1650ContagemResultado_PrzInc[0];
                     A1714ContagemResultado_Combinada = P00A823_A1714ContagemResultado_Combinada[0];
                     n1714ContagemResultado_Combinada = P00A823_n1714ContagemResultado_Combinada[0];
                     A1762ContagemResultado_Entrega = P00A823_A1762ContagemResultado_Entrega[0];
                     n1762ContagemResultado_Entrega = P00A823_n1762ContagemResultado_Entrega[0];
                     A1791ContagemResultado_SemCusto = P00A823_A1791ContagemResultado_SemCusto[0];
                     n1791ContagemResultado_SemCusto = P00A823_n1791ContagemResultado_SemCusto[0];
                     A1854ContagemResultado_VlrCnc = P00A823_A1854ContagemResultado_VlrCnc[0];
                     n1854ContagemResultado_VlrCnc = P00A823_n1854ContagemResultado_VlrCnc[0];
                     A1855ContagemResultado_PFCnc = P00A823_A1855ContagemResultado_PFCnc[0];
                     n1855ContagemResultado_PFCnc = P00A823_n1855ContagemResultado_PFCnc[0];
                     A1903ContagemResultado_DataPrvPgm = P00A823_A1903ContagemResultado_DataPrvPgm[0];
                     n1903ContagemResultado_DataPrvPgm = P00A823_n1903ContagemResultado_DataPrvPgm[0];
                     A2133ContagemResultado_QuantidadeSolicitada = P00A823_A2133ContagemResultado_QuantidadeSolicitada[0];
                     n2133ContagemResultado_QuantidadeSolicitada = P00A823_n2133ContagemResultado_QuantidadeSolicitada[0];
                     A578ContagemResultado_CntComNaoCnf = P00A823_A578ContagemResultado_CntComNaoCnf[0];
                     n578ContagemResultado_CntComNaoCnf = P00A823_n578ContagemResultado_CntComNaoCnf[0];
                     A510ContagemResultado_EsforcoSoma = P00A823_A510ContagemResultado_EsforcoSoma[0];
                     n510ContagemResultado_EsforcoSoma = P00A823_n510ContagemResultado_EsforcoSoma[0];
                     A2118ContagemResultado_Owner_Identificao = P00A823_A2118ContagemResultado_Owner_Identificao[0];
                     n2118ContagemResultado_Owner_Identificao = P00A823_n2118ContagemResultado_Owner_Identificao[0];
                     A1353ContagemResultado_ContratadaDoOwner = P00A823_A1353ContagemResultado_ContratadaDoOwner[0];
                     n1353ContagemResultado_ContratadaDoOwner = P00A823_n1353ContagemResultado_ContratadaDoOwner[0];
                     A1608ContagemResultado_PrzExc = P00A823_A1608ContagemResultado_PrzExc[0];
                     n1608ContagemResultado_PrzExc = P00A823_n1608ContagemResultado_PrzExc[0];
                     A1610ContagemResultado_PrzTp = P00A823_A1610ContagemResultado_PrzTp[0];
                     n1610ContagemResultado_PrzTp = P00A823_n1610ContagemResultado_PrzTp[0];
                     A1625ContagemResultado_DatIncTA = P00A823_A1625ContagemResultado_DatIncTA[0];
                     n1625ContagemResultado_DatIncTA = P00A823_n1625ContagemResultado_DatIncTA[0];
                     A1602ContagemResultado_GlsIndValor = P00A823_A1602ContagemResultado_GlsIndValor[0];
                     n1602ContagemResultado_GlsIndValor = P00A823_n1602ContagemResultado_GlsIndValor[0];
                     A1701ContagemResultado_TemProposta = P00A823_A1701ContagemResultado_TemProposta[0];
                     n1701ContagemResultado_TemProposta = P00A823_n1701ContagemResultado_TemProposta[0];
                     A1443ContagemResultado_CntSrvPrrCod = P00A823_A1443ContagemResultado_CntSrvPrrCod[0];
                     n1443ContagemResultado_CntSrvPrrCod = P00A823_n1443ContagemResultado_CntSrvPrrCod[0];
                     A1043ContagemResultado_LiqLogCod = P00A823_A1043ContagemResultado_LiqLogCod[0];
                     n1043ContagemResultado_LiqLogCod = P00A823_n1043ContagemResultado_LiqLogCod[0];
                     A512ContagemResultado_ValorPF = P00A823_A512ContagemResultado_ValorPF[0];
                     n512ContagemResultado_ValorPF = P00A823_n512ContagemResultado_ValorPF[0];
                     A52Contratada_AreaTrabalhoCod = P00A823_A52Contratada_AreaTrabalhoCod[0];
                     n52Contratada_AreaTrabalhoCod = P00A823_n52Contratada_AreaTrabalhoCod[0];
                     A890ContagemResultado_Responsavel = P00A823_A890ContagemResultado_Responsavel[0];
                     n890ContagemResultado_Responsavel = P00A823_n890ContagemResultado_Responsavel[0];
                     A508ContagemResultado_Owner = P00A823_A508ContagemResultado_Owner[0];
                     A457ContagemResultado_Demanda = P00A823_A457ContagemResultado_Demanda[0];
                     n457ContagemResultado_Demanda = P00A823_n457ContagemResultado_Demanda[0];
                     A803ContagemResultado_ContratadaSigla = P00A823_A803ContagemResultado_ContratadaSigla[0];
                     n803ContagemResultado_ContratadaSigla = P00A823_n803ContagemResultado_ContratadaSigla[0];
                     A801ContagemResultado_ServicoSigla = P00A823_A801ContagemResultado_ServicoSigla[0];
                     n801ContagemResultado_ServicoSigla = P00A823_n801ContagemResultado_ServicoSigla[0];
                     A493ContagemResultado_DemandaFM = P00A823_A493ContagemResultado_DemandaFM[0];
                     n493ContagemResultado_DemandaFM = P00A823_n493ContagemResultado_DemandaFM[0];
                     A490ContagemResultado_ContratadaCod = P00A823_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00A823_n490ContagemResultado_ContratadaCod[0];
                     A1553ContagemResultado_CntSrvCod = P00A823_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00A823_n1553ContagemResultado_CntSrvCod[0];
                     A471ContagemResultado_DataDmn = P00A823_A471ContagemResultado_DataDmn[0];
                     A1790ContagemResultado_DataInicio = P00A823_A1790ContagemResultado_DataInicio[0];
                     n1790ContagemResultado_DataInicio = P00A823_n1790ContagemResultado_DataInicio[0];
                     A472ContagemResultado_DataEntrega = P00A823_A472ContagemResultado_DataEntrega[0];
                     n472ContagemResultado_DataEntrega = P00A823_n472ContagemResultado_DataEntrega[0];
                     A912ContagemResultado_HoraEntrega = P00A823_A912ContagemResultado_HoraEntrega[0];
                     n912ContagemResultado_HoraEntrega = P00A823_n912ContagemResultado_HoraEntrega[0];
                     A1351ContagemResultado_DataPrevista = P00A823_A1351ContagemResultado_DataPrevista[0];
                     n1351ContagemResultado_DataPrevista = P00A823_n1351ContagemResultado_DataPrevista[0];
                     A1349ContagemResultado_DataExecucao = P00A823_A1349ContagemResultado_DataExecucao[0];
                     n1349ContagemResultado_DataExecucao = P00A823_n1349ContagemResultado_DataExecucao[0];
                     A2017ContagemResultado_DataEntregaReal = P00A823_A2017ContagemResultado_DataEntregaReal[0];
                     n2017ContagemResultado_DataEntregaReal = P00A823_n2017ContagemResultado_DataEntregaReal[0];
                     A1348ContagemResultado_DataHomologacao = P00A823_A1348ContagemResultado_DataHomologacao[0];
                     n1348ContagemResultado_DataHomologacao = P00A823_n1348ContagemResultado_DataHomologacao[0];
                     A1227ContagemResultado_PrazoInicialDias = P00A823_A1227ContagemResultado_PrazoInicialDias[0];
                     n1227ContagemResultado_PrazoInicialDias = P00A823_n1227ContagemResultado_PrazoInicialDias[0];
                     A1237ContagemResultado_PrazoMaisDias = P00A823_A1237ContagemResultado_PrazoMaisDias[0];
                     n1237ContagemResultado_PrazoMaisDias = P00A823_n1237ContagemResultado_PrazoMaisDias[0];
                     A499ContagemResultado_ContratadaPessoaCod = P00A823_A499ContagemResultado_ContratadaPessoaCod[0];
                     n499ContagemResultado_ContratadaPessoaCod = P00A823_n499ContagemResultado_ContratadaPessoaCod[0];
                     A500ContagemResultado_ContratadaPessoaNom = P00A823_A500ContagemResultado_ContratadaPessoaNom[0];
                     n500ContagemResultado_ContratadaPessoaNom = P00A823_n500ContagemResultado_ContratadaPessoaNom[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00A823_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00A823_n1326ContagemResultado_ContratadaTipoFab[0];
                     A1817ContagemResultado_ContratadaCNPJ = P00A823_A1817ContagemResultado_ContratadaCNPJ[0];
                     n1817ContagemResultado_ContratadaCNPJ = P00A823_n1817ContagemResultado_ContratadaCNPJ[0];
                     A805ContagemResultado_ContratadaOrigemCod = P00A823_A805ContagemResultado_ContratadaOrigemCod[0];
                     n805ContagemResultado_ContratadaOrigemCod = P00A823_n805ContagemResultado_ContratadaOrigemCod[0];
                     A866ContagemResultado_ContratadaOrigemSigla = P00A823_A866ContagemResultado_ContratadaOrigemSigla[0];
                     n866ContagemResultado_ContratadaOrigemSigla = P00A823_n866ContagemResultado_ContratadaOrigemSigla[0];
                     A807ContagemResultado_ContratadaOrigemPesCod = P00A823_A807ContagemResultado_ContratadaOrigemPesCod[0];
                     n807ContagemResultado_ContratadaOrigemPesCod = P00A823_n807ContagemResultado_ContratadaOrigemPesCod[0];
                     A808ContagemResultado_ContratadaOrigemPesNom = P00A823_A808ContagemResultado_ContratadaOrigemPesNom[0];
                     n808ContagemResultado_ContratadaOrigemPesNom = P00A823_n808ContagemResultado_ContratadaOrigemPesNom[0];
                     A867ContagemResultado_ContratadaOrigemAreaCod = P00A823_A867ContagemResultado_ContratadaOrigemAreaCod[0];
                     n867ContagemResultado_ContratadaOrigemAreaCod = P00A823_n867ContagemResultado_ContratadaOrigemAreaCod[0];
                     A2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema[0];
                     n2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema[0];
                     A454ContagemResultado_ContadorFSCod = P00A823_A454ContagemResultado_ContadorFSCod[0];
                     n454ContagemResultado_ContadorFSCod = P00A823_n454ContagemResultado_ContadorFSCod[0];
                     A480ContagemResultado_CrFSPessoaCod = P00A823_A480ContagemResultado_CrFSPessoaCod[0];
                     n480ContagemResultado_CrFSPessoaCod = P00A823_n480ContagemResultado_CrFSPessoaCod[0];
                     A455ContagemResultado_ContadorFSNom = P00A823_A455ContagemResultado_ContadorFSNom[0];
                     n455ContagemResultado_ContadorFSNom = P00A823_n455ContagemResultado_ContadorFSNom[0];
                     A1452ContagemResultado_SS = P00A823_A1452ContagemResultado_SS[0];
                     n1452ContagemResultado_SS = P00A823_n1452ContagemResultado_SS[0];
                     A1173ContagemResultado_OSManual = P00A823_A1173ContagemResultado_OSManual[0];
                     n1173ContagemResultado_OSManual = P00A823_n1173ContagemResultado_OSManual[0];
                     A494ContagemResultado_Descricao = P00A823_A494ContagemResultado_Descricao[0];
                     n494ContagemResultado_Descricao = P00A823_n494ContagemResultado_Descricao[0];
                     A465ContagemResultado_Link = P00A823_A465ContagemResultado_Link[0];
                     n465ContagemResultado_Link = P00A823_n465ContagemResultado_Link[0];
                     A489ContagemResultado_SistemaCod = P00A823_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P00A823_n489ContagemResultado_SistemaCod[0];
                     A495ContagemResultado_SistemaNom = P00A823_A495ContagemResultado_SistemaNom[0];
                     n495ContagemResultado_SistemaNom = P00A823_n495ContagemResultado_SistemaNom[0];
                     A496Contagemresultado_SistemaAreaCod = P00A823_A496Contagemresultado_SistemaAreaCod[0];
                     n496Contagemresultado_SistemaAreaCod = P00A823_n496Contagemresultado_SistemaAreaCod[0];
                     A509ContagemrResultado_SistemaSigla = P00A823_A509ContagemrResultado_SistemaSigla[0];
                     n509ContagemrResultado_SistemaSigla = P00A823_n509ContagemrResultado_SistemaSigla[0];
                     A515ContagemResultado_SistemaCoord = P00A823_A515ContagemResultado_SistemaCoord[0];
                     n515ContagemResultado_SistemaCoord = P00A823_n515ContagemResultado_SistemaCoord[0];
                     A804ContagemResultado_SistemaAtivo = P00A823_A804ContagemResultado_SistemaAtivo[0];
                     n804ContagemResultado_SistemaAtivo = P00A823_n804ContagemResultado_SistemaAtivo[0];
                     A146Modulo_Codigo = P00A823_A146Modulo_Codigo[0];
                     n146Modulo_Codigo = P00A823_n146Modulo_Codigo[0];
                     A143Modulo_Nome = P00A823_A143Modulo_Nome[0];
                     A145Modulo_Sigla = P00A823_A145Modulo_Sigla[0];
                     A468ContagemResultado_NaoCnfDmnCod = P00A823_A468ContagemResultado_NaoCnfDmnCod[0];
                     n468ContagemResultado_NaoCnfDmnCod = P00A823_n468ContagemResultado_NaoCnfDmnCod[0];
                     A477ContagemResultado_NaoCnfDmnNom = P00A823_A477ContagemResultado_NaoCnfDmnNom[0];
                     n477ContagemResultado_NaoCnfDmnNom = P00A823_n477ContagemResultado_NaoCnfDmnNom[0];
                     A2030ContagemResultado_NaoCnfDmnGls = P00A823_A2030ContagemResultado_NaoCnfDmnGls[0];
                     n2030ContagemResultado_NaoCnfDmnGls = P00A823_n2030ContagemResultado_NaoCnfDmnGls[0];
                     A484ContagemResultado_StatusDmn = P00A823_A484ContagemResultado_StatusDmn[0];
                     n484ContagemResultado_StatusDmn = P00A823_n484ContagemResultado_StatusDmn[0];
                     A771ContagemResultado_StatusDmnVnc = P00A823_A771ContagemResultado_StatusDmnVnc[0];
                     n771ContagemResultado_StatusDmnVnc = P00A823_n771ContagemResultado_StatusDmnVnc[0];
                     A601ContagemResultado_Servico = P00A823_A601ContagemResultado_Servico[0];
                     n601ContagemResultado_Servico = P00A823_n601ContagemResultado_Servico[0];
                     A2008ContagemResultado_CntSrvAls = P00A823_A2008ContagemResultado_CntSrvAls[0];
                     n2008ContagemResultado_CntSrvAls = P00A823_n2008ContagemResultado_CntSrvAls[0];
                     A764ContagemResultado_ServicoGrupo = P00A823_A764ContagemResultado_ServicoGrupo[0];
                     n764ContagemResultado_ServicoGrupo = P00A823_n764ContagemResultado_ServicoGrupo[0];
                     A1062ContagemResultado_ServicoTela = P00A823_A1062ContagemResultado_ServicoTela[0];
                     n1062ContagemResultado_ServicoTela = P00A823_n1062ContagemResultado_ServicoTela[0];
                     A897ContagemResultado_ServicoAtivo = P00A823_A897ContagemResultado_ServicoAtivo[0];
                     n897ContagemResultado_ServicoAtivo = P00A823_n897ContagemResultado_ServicoAtivo[0];
                     A1398ContagemResultado_ServicoNaoRqrAtr = P00A823_A1398ContagemResultado_ServicoNaoRqrAtr[0];
                     n1398ContagemResultado_ServicoNaoRqrAtr = P00A823_n1398ContagemResultado_ServicoNaoRqrAtr[0];
                     A1636ContagemResultado_ServicoSS = P00A823_A1636ContagemResultado_ServicoSS[0];
                     n1636ContagemResultado_ServicoSS = P00A823_n1636ContagemResultado_ServicoSS[0];
                     A1637ContagemResultado_ServicoSSSgl = P00A823_A1637ContagemResultado_ServicoSSSgl[0];
                     n1637ContagemResultado_ServicoSSSgl = P00A823_n1637ContagemResultado_ServicoSSSgl[0];
                     A2050ContagemResultado_SrvLnhNegCod = P00A823_A2050ContagemResultado_SrvLnhNegCod[0];
                     n2050ContagemResultado_SrvLnhNegCod = P00A823_n2050ContagemResultado_SrvLnhNegCod[0];
                     A485ContagemResultado_EhValidacao = P00A823_A485ContagemResultado_EhValidacao[0];
                     n485ContagemResultado_EhValidacao = P00A823_n485ContagemResultado_EhValidacao[0];
                     A514ContagemResultado_Observacao = P00A823_A514ContagemResultado_Observacao[0];
                     n514ContagemResultado_Observacao = P00A823_n514ContagemResultado_Observacao[0];
                     A592ContagemResultado_Evidencia = P00A823_A592ContagemResultado_Evidencia[0];
                     n592ContagemResultado_Evidencia = P00A823_n592ContagemResultado_Evidencia[0];
                     A1350ContagemResultado_DataCadastro = P00A823_A1350ContagemResultado_DataCadastro[0];
                     n1350ContagemResultado_DataCadastro = P00A823_n1350ContagemResultado_DataCadastro[0];
                     A1312ContagemResultado_ResponsavelPessCod = P00A823_A1312ContagemResultado_ResponsavelPessCod[0];
                     n1312ContagemResultado_ResponsavelPessCod = P00A823_n1312ContagemResultado_ResponsavelPessCod[0];
                     A1313ContagemResultado_ResponsavelPessNome = P00A823_A1313ContagemResultado_ResponsavelPessNome[0];
                     n1313ContagemResultado_ResponsavelPessNome = P00A823_n1313ContagemResultado_ResponsavelPessNome[0];
                     A1180ContagemResultado_Custo = P00A823_A1180ContagemResultado_Custo[0];
                     n1180ContagemResultado_Custo = P00A823_n1180ContagemResultado_Custo[0];
                     A597ContagemResultado_LoteAceiteCod = P00A823_A597ContagemResultado_LoteAceiteCod[0];
                     n597ContagemResultado_LoteAceiteCod = P00A823_n597ContagemResultado_LoteAceiteCod[0];
                     A528ContagemResultado_LoteAceite = P00A823_A528ContagemResultado_LoteAceite[0];
                     n528ContagemResultado_LoteAceite = P00A823_n528ContagemResultado_LoteAceite[0];
                     A529ContagemResultado_DataAceite = P00A823_A529ContagemResultado_DataAceite[0];
                     n529ContagemResultado_DataAceite = P00A823_n529ContagemResultado_DataAceite[0];
                     A525ContagemResultado_AceiteUserCod = P00A823_A525ContagemResultado_AceiteUserCod[0];
                     n525ContagemResultado_AceiteUserCod = P00A823_n525ContagemResultado_AceiteUserCod[0];
                     A526ContagemResultado_AceitePessoaCod = P00A823_A526ContagemResultado_AceitePessoaCod[0];
                     n526ContagemResultado_AceitePessoaCod = P00A823_n526ContagemResultado_AceitePessoaCod[0];
                     A527ContagemResultado_AceiteUserNom = P00A823_A527ContagemResultado_AceiteUserNom[0];
                     n527ContagemResultado_AceiteUserNom = P00A823_n527ContagemResultado_AceiteUserNom[0];
                     A1547ContagemResultado_LoteNFe = P00A823_A1547ContagemResultado_LoteNFe[0];
                     n1547ContagemResultado_LoteNFe = P00A823_n1547ContagemResultado_LoteNFe[0];
                     A1559ContagemResultado_VlrAceite = P00A823_A1559ContagemResultado_VlrAceite[0];
                     n1559ContagemResultado_VlrAceite = P00A823_n1559ContagemResultado_VlrAceite[0];
                     A598ContagemResultado_Baseline = P00A823_A598ContagemResultado_Baseline[0];
                     n598ContagemResultado_Baseline = P00A823_n598ContagemResultado_Baseline[0];
                     A602ContagemResultado_OSVinculada = P00A823_A602ContagemResultado_OSVinculada[0];
                     n602ContagemResultado_OSVinculada = P00A823_n602ContagemResultado_OSVinculada[0];
                     A603ContagemResultado_DmnVinculada = P00A823_A603ContagemResultado_DmnVinculada[0];
                     n603ContagemResultado_DmnVinculada = P00A823_n603ContagemResultado_DmnVinculada[0];
                     A798ContagemResultado_PFBFSImp = P00A823_A798ContagemResultado_PFBFSImp[0];
                     n798ContagemResultado_PFBFSImp = P00A823_n798ContagemResultado_PFBFSImp[0];
                     A799ContagemResultado_PFLFSImp = P00A823_A799ContagemResultado_PFLFSImp[0];
                     n799ContagemResultado_PFLFSImp = P00A823_n799ContagemResultado_PFLFSImp[0];
                     A1178ContagemResultado_PBFinal = P00A823_A1178ContagemResultado_PBFinal[0];
                     n1178ContagemResultado_PBFinal = P00A823_n1178ContagemResultado_PBFinal[0];
                     A1179ContagemResultado_PLFinal = P00A823_A1179ContagemResultado_PLFinal[0];
                     n1179ContagemResultado_PLFinal = P00A823_n1179ContagemResultado_PLFinal[0];
                     A1034ContagemResultadoLiqLog_Data = P00A823_A1034ContagemResultadoLiqLog_Data[0];
                     n1034ContagemResultadoLiqLog_Data = P00A823_n1034ContagemResultadoLiqLog_Data[0];
                     A1044ContagemResultado_FncUsrCod = P00A823_A1044ContagemResultado_FncUsrCod[0];
                     n1044ContagemResultado_FncUsrCod = P00A823_n1044ContagemResultado_FncUsrCod[0];
                     A1046ContagemResultado_Agrupador = P00A823_A1046ContagemResultado_Agrupador[0];
                     n1046ContagemResultado_Agrupador = P00A823_n1046ContagemResultado_Agrupador[0];
                     A1049ContagemResultado_GlsData = P00A823_A1049ContagemResultado_GlsData[0];
                     n1049ContagemResultado_GlsData = P00A823_n1049ContagemResultado_GlsData[0];
                     A1050ContagemResultado_GlsDescricao = P00A823_A1050ContagemResultado_GlsDescricao[0];
                     n1050ContagemResultado_GlsDescricao = P00A823_n1050ContagemResultado_GlsDescricao[0];
                     A1051ContagemResultado_GlsValor = P00A823_A1051ContagemResultado_GlsValor[0];
                     n1051ContagemResultado_GlsValor = P00A823_n1051ContagemResultado_GlsValor[0];
                     A1052ContagemResultado_GlsUser = P00A823_A1052ContagemResultado_GlsUser[0];
                     n1052ContagemResultado_GlsUser = P00A823_n1052ContagemResultado_GlsUser[0];
                     A1389ContagemResultado_RdmnIssueId = P00A823_A1389ContagemResultado_RdmnIssueId[0];
                     n1389ContagemResultado_RdmnIssueId = P00A823_n1389ContagemResultado_RdmnIssueId[0];
                     A1390ContagemResultado_RdmnProjectId = P00A823_A1390ContagemResultado_RdmnProjectId[0];
                     n1390ContagemResultado_RdmnProjectId = P00A823_n1390ContagemResultado_RdmnProjectId[0];
                     A1392ContagemResultado_RdmnUpdated = P00A823_A1392ContagemResultado_RdmnUpdated[0];
                     n1392ContagemResultado_RdmnUpdated = P00A823_n1392ContagemResultado_RdmnUpdated[0];
                     A1444ContagemResultado_CntSrvPrrPrz = P00A823_A1444ContagemResultado_CntSrvPrrPrz[0];
                     n1444ContagemResultado_CntSrvPrrPrz = P00A823_n1444ContagemResultado_CntSrvPrrPrz[0];
                     A1445ContagemResultado_CntSrvPrrCst = P00A823_A1445ContagemResultado_CntSrvPrrCst[0];
                     n1445ContagemResultado_CntSrvPrrCst = P00A823_n1445ContagemResultado_CntSrvPrrCst[0];
                     A1457ContagemResultado_TemDpnHmlg = P00A823_A1457ContagemResultado_TemDpnHmlg[0];
                     n1457ContagemResultado_TemDpnHmlg = P00A823_n1457ContagemResultado_TemDpnHmlg[0];
                     A1519ContagemResultado_TmpEstAnl = P00A823_A1519ContagemResultado_TmpEstAnl[0];
                     n1519ContagemResultado_TmpEstAnl = P00A823_n1519ContagemResultado_TmpEstAnl[0];
                     A1505ContagemResultado_TmpEstExc = P00A823_A1505ContagemResultado_TmpEstExc[0];
                     n1505ContagemResultado_TmpEstExc = P00A823_n1505ContagemResultado_TmpEstExc[0];
                     A1506ContagemResultado_TmpEstCrr = P00A823_A1506ContagemResultado_TmpEstCrr[0];
                     n1506ContagemResultado_TmpEstCrr = P00A823_n1506ContagemResultado_TmpEstCrr[0];
                     A1520ContagemResultado_InicioAnl = P00A823_A1520ContagemResultado_InicioAnl[0];
                     n1520ContagemResultado_InicioAnl = P00A823_n1520ContagemResultado_InicioAnl[0];
                     A1521ContagemResultado_FimAnl = P00A823_A1521ContagemResultado_FimAnl[0];
                     n1521ContagemResultado_FimAnl = P00A823_n1521ContagemResultado_FimAnl[0];
                     A1509ContagemResultado_InicioExc = P00A823_A1509ContagemResultado_InicioExc[0];
                     n1509ContagemResultado_InicioExc = P00A823_n1509ContagemResultado_InicioExc[0];
                     A1510ContagemResultado_FimExc = P00A823_A1510ContagemResultado_FimExc[0];
                     n1510ContagemResultado_FimExc = P00A823_n1510ContagemResultado_FimExc[0];
                     A1511ContagemResultado_InicioCrr = P00A823_A1511ContagemResultado_InicioCrr[0];
                     n1511ContagemResultado_InicioCrr = P00A823_n1511ContagemResultado_InicioCrr[0];
                     A1512ContagemResultado_FimCrr = P00A823_A1512ContagemResultado_FimCrr[0];
                     n1512ContagemResultado_FimCrr = P00A823_n1512ContagemResultado_FimCrr[0];
                     A1515ContagemResultado_Evento = P00A823_A1515ContagemResultado_Evento[0];
                     n1515ContagemResultado_Evento = P00A823_n1515ContagemResultado_Evento[0];
                     A1544ContagemResultado_ProjetoCod = P00A823_A1544ContagemResultado_ProjetoCod[0];
                     n1544ContagemResultado_ProjetoCod = P00A823_n1544ContagemResultado_ProjetoCod[0];
                     A1593ContagemResultado_CntSrvTpVnc = P00A823_A1593ContagemResultado_CntSrvTpVnc[0];
                     n1593ContagemResultado_CntSrvTpVnc = P00A823_n1593ContagemResultado_CntSrvTpVnc[0];
                     A1594ContagemResultado_CntSrvFtrm = P00A823_A1594ContagemResultado_CntSrvFtrm[0];
                     n1594ContagemResultado_CntSrvFtrm = P00A823_n1594ContagemResultado_CntSrvFtrm[0];
                     A1596ContagemResultado_CntSrvPrc = P00A823_A1596ContagemResultado_CntSrvPrc[0];
                     n1596ContagemResultado_CntSrvPrc = P00A823_n1596ContagemResultado_CntSrvPrc[0];
                     A1597ContagemResultado_CntSrvVlrUndCnt = P00A823_A1597ContagemResultado_CntSrvVlrUndCnt[0];
                     n1597ContagemResultado_CntSrvVlrUndCnt = P00A823_n1597ContagemResultado_CntSrvVlrUndCnt[0];
                     A866ContagemResultado_ContratadaOrigemSigla = P00A823_A866ContagemResultado_ContratadaOrigemSigla[0];
                     n866ContagemResultado_ContratadaOrigemSigla = P00A823_n866ContagemResultado_ContratadaOrigemSigla[0];
                     A807ContagemResultado_ContratadaOrigemPesCod = P00A823_A807ContagemResultado_ContratadaOrigemPesCod[0];
                     n807ContagemResultado_ContratadaOrigemPesCod = P00A823_n807ContagemResultado_ContratadaOrigemPesCod[0];
                     A867ContagemResultado_ContratadaOrigemAreaCod = P00A823_A867ContagemResultado_ContratadaOrigemAreaCod[0];
                     n867ContagemResultado_ContratadaOrigemAreaCod = P00A823_n867ContagemResultado_ContratadaOrigemAreaCod[0];
                     A2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema[0];
                     n2093ContagemResultado_ContratadaOrigemUsaSistema = P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema[0];
                     A808ContagemResultado_ContratadaOrigemPesNom = P00A823_A808ContagemResultado_ContratadaOrigemPesNom[0];
                     n808ContagemResultado_ContratadaOrigemPesNom = P00A823_n808ContagemResultado_ContratadaOrigemPesNom[0];
                     A480ContagemResultado_CrFSPessoaCod = P00A823_A480ContagemResultado_CrFSPessoaCod[0];
                     n480ContagemResultado_CrFSPessoaCod = P00A823_n480ContagemResultado_CrFSPessoaCod[0];
                     A455ContagemResultado_ContadorFSNom = P00A823_A455ContagemResultado_ContadorFSNom[0];
                     n455ContagemResultado_ContadorFSNom = P00A823_n455ContagemResultado_ContadorFSNom[0];
                     A495ContagemResultado_SistemaNom = P00A823_A495ContagemResultado_SistemaNom[0];
                     n495ContagemResultado_SistemaNom = P00A823_n495ContagemResultado_SistemaNom[0];
                     A496Contagemresultado_SistemaAreaCod = P00A823_A496Contagemresultado_SistemaAreaCod[0];
                     n496Contagemresultado_SistemaAreaCod = P00A823_n496Contagemresultado_SistemaAreaCod[0];
                     A509ContagemrResultado_SistemaSigla = P00A823_A509ContagemrResultado_SistemaSigla[0];
                     n509ContagemrResultado_SistemaSigla = P00A823_n509ContagemrResultado_SistemaSigla[0];
                     A515ContagemResultado_SistemaCoord = P00A823_A515ContagemResultado_SistemaCoord[0];
                     n515ContagemResultado_SistemaCoord = P00A823_n515ContagemResultado_SistemaCoord[0];
                     A804ContagemResultado_SistemaAtivo = P00A823_A804ContagemResultado_SistemaAtivo[0];
                     n804ContagemResultado_SistemaAtivo = P00A823_n804ContagemResultado_SistemaAtivo[0];
                     A143Modulo_Nome = P00A823_A143Modulo_Nome[0];
                     A145Modulo_Sigla = P00A823_A145Modulo_Sigla[0];
                     A477ContagemResultado_NaoCnfDmnNom = P00A823_A477ContagemResultado_NaoCnfDmnNom[0];
                     n477ContagemResultado_NaoCnfDmnNom = P00A823_n477ContagemResultado_NaoCnfDmnNom[0];
                     A2030ContagemResultado_NaoCnfDmnGls = P00A823_A2030ContagemResultado_NaoCnfDmnGls[0];
                     n2030ContagemResultado_NaoCnfDmnGls = P00A823_n2030ContagemResultado_NaoCnfDmnGls[0];
                     A1637ContagemResultado_ServicoSSSgl = P00A823_A1637ContagemResultado_ServicoSSSgl[0];
                     n1637ContagemResultado_ServicoSSSgl = P00A823_n1637ContagemResultado_ServicoSSSgl[0];
                     A528ContagemResultado_LoteAceite = P00A823_A528ContagemResultado_LoteAceite[0];
                     n528ContagemResultado_LoteAceite = P00A823_n528ContagemResultado_LoteAceite[0];
                     A529ContagemResultado_DataAceite = P00A823_A529ContagemResultado_DataAceite[0];
                     n529ContagemResultado_DataAceite = P00A823_n529ContagemResultado_DataAceite[0];
                     A525ContagemResultado_AceiteUserCod = P00A823_A525ContagemResultado_AceiteUserCod[0];
                     n525ContagemResultado_AceiteUserCod = P00A823_n525ContagemResultado_AceiteUserCod[0];
                     A1547ContagemResultado_LoteNFe = P00A823_A1547ContagemResultado_LoteNFe[0];
                     n1547ContagemResultado_LoteNFe = P00A823_n1547ContagemResultado_LoteNFe[0];
                     A526ContagemResultado_AceitePessoaCod = P00A823_A526ContagemResultado_AceitePessoaCod[0];
                     n526ContagemResultado_AceitePessoaCod = P00A823_n526ContagemResultado_AceitePessoaCod[0];
                     A527ContagemResultado_AceiteUserNom = P00A823_A527ContagemResultado_AceiteUserNom[0];
                     n527ContagemResultado_AceiteUserNom = P00A823_n527ContagemResultado_AceiteUserNom[0];
                     A1765ContagemResultado_CodSrvSSVnc = P00A823_A1765ContagemResultado_CodSrvSSVnc[0];
                     n1765ContagemResultado_CodSrvSSVnc = P00A823_n1765ContagemResultado_CodSrvSSVnc[0];
                     A1627ContagemResultado_CntSrvVncCod = P00A823_A1627ContagemResultado_CntSrvVncCod[0];
                     n1627ContagemResultado_CntSrvVncCod = P00A823_n1627ContagemResultado_CntSrvVncCod[0];
                     A2136ContagemResultado_CntadaOsVinc = P00A823_A2136ContagemResultado_CntadaOsVinc[0];
                     n2136ContagemResultado_CntadaOsVinc = P00A823_n2136ContagemResultado_CntadaOsVinc[0];
                     A771ContagemResultado_StatusDmnVnc = P00A823_A771ContagemResultado_StatusDmnVnc[0];
                     n771ContagemResultado_StatusDmnVnc = P00A823_n771ContagemResultado_StatusDmnVnc[0];
                     A603ContagemResultado_DmnVinculada = P00A823_A603ContagemResultado_DmnVinculada[0];
                     n603ContagemResultado_DmnVinculada = P00A823_n603ContagemResultado_DmnVinculada[0];
                     A1591ContagemResultado_CodSrvVnc = P00A823_A1591ContagemResultado_CodSrvVnc[0];
                     n1591ContagemResultado_CodSrvVnc = P00A823_n1591ContagemResultado_CodSrvVnc[0];
                     A1622ContagemResultado_CntVncCod = P00A823_A1622ContagemResultado_CntVncCod[0];
                     n1622ContagemResultado_CntVncCod = P00A823_n1622ContagemResultado_CntVncCod[0];
                     A1590ContagemResultado_SiglaSrvVnc = P00A823_A1590ContagemResultado_SiglaSrvVnc[0];
                     n1590ContagemResultado_SiglaSrvVnc = P00A823_n1590ContagemResultado_SiglaSrvVnc[0];
                     A2137ContagemResultado_SerGrupoVinc = P00A823_A2137ContagemResultado_SerGrupoVinc[0];
                     n2137ContagemResultado_SerGrupoVinc = P00A823_n2137ContagemResultado_SerGrupoVinc[0];
                     A1034ContagemResultadoLiqLog_Data = P00A823_A1034ContagemResultadoLiqLog_Data[0];
                     n1034ContagemResultadoLiqLog_Data = P00A823_n1034ContagemResultadoLiqLog_Data[0];
                     A1312ContagemResultado_ResponsavelPessCod = P00A823_A1312ContagemResultado_ResponsavelPessCod[0];
                     n1312ContagemResultado_ResponsavelPessCod = P00A823_n1312ContagemResultado_ResponsavelPessCod[0];
                     A1313ContagemResultado_ResponsavelPessNome = P00A823_A1313ContagemResultado_ResponsavelPessNome[0];
                     n1313ContagemResultado_ResponsavelPessNome = P00A823_n1313ContagemResultado_ResponsavelPessNome[0];
                     A2118ContagemResultado_Owner_Identificao = P00A823_A2118ContagemResultado_Owner_Identificao[0];
                     n2118ContagemResultado_Owner_Identificao = P00A823_n2118ContagemResultado_Owner_Identificao[0];
                     A578ContagemResultado_CntComNaoCnf = P00A823_A578ContagemResultado_CntComNaoCnf[0];
                     n578ContagemResultado_CntComNaoCnf = P00A823_n578ContagemResultado_CntComNaoCnf[0];
                     A510ContagemResultado_EsforcoSoma = P00A823_A510ContagemResultado_EsforcoSoma[0];
                     n510ContagemResultado_EsforcoSoma = P00A823_n510ContagemResultado_EsforcoSoma[0];
                     A1353ContagemResultado_ContratadaDoOwner = P00A823_A1353ContagemResultado_ContratadaDoOwner[0];
                     n1353ContagemResultado_ContratadaDoOwner = P00A823_n1353ContagemResultado_ContratadaDoOwner[0];
                     A1602ContagemResultado_GlsIndValor = P00A823_A1602ContagemResultado_GlsIndValor[0];
                     n1602ContagemResultado_GlsIndValor = P00A823_n1602ContagemResultado_GlsIndValor[0];
                     A1701ContagemResultado_TemProposta = P00A823_A1701ContagemResultado_TemProposta[0];
                     n1701ContagemResultado_TemProposta = P00A823_n1701ContagemResultado_TemProposta[0];
                     A52Contratada_AreaTrabalhoCod = P00A823_A52Contratada_AreaTrabalhoCod[0];
                     n52Contratada_AreaTrabalhoCod = P00A823_n52Contratada_AreaTrabalhoCod[0];
                     A803ContagemResultado_ContratadaSigla = P00A823_A803ContagemResultado_ContratadaSigla[0];
                     n803ContagemResultado_ContratadaSigla = P00A823_n803ContagemResultado_ContratadaSigla[0];
                     A499ContagemResultado_ContratadaPessoaCod = P00A823_A499ContagemResultado_ContratadaPessoaCod[0];
                     n499ContagemResultado_ContratadaPessoaCod = P00A823_n499ContagemResultado_ContratadaPessoaCod[0];
                     A1326ContagemResultado_ContratadaTipoFab = P00A823_A1326ContagemResultado_ContratadaTipoFab[0];
                     n1326ContagemResultado_ContratadaTipoFab = P00A823_n1326ContagemResultado_ContratadaTipoFab[0];
                     A500ContagemResultado_ContratadaPessoaNom = P00A823_A500ContagemResultado_ContratadaPessoaNom[0];
                     n500ContagemResultado_ContratadaPessoaNom = P00A823_n500ContagemResultado_ContratadaPessoaNom[0];
                     A1817ContagemResultado_ContratadaCNPJ = P00A823_A1817ContagemResultado_ContratadaCNPJ[0];
                     n1817ContagemResultado_ContratadaCNPJ = P00A823_n1817ContagemResultado_ContratadaCNPJ[0];
                     A1600ContagemResultado_CntSrvSttPgmFnc = P00A823_A1600ContagemResultado_CntSrvSttPgmFnc[0];
                     n1600ContagemResultado_CntSrvSttPgmFnc = P00A823_n1600ContagemResultado_CntSrvSttPgmFnc[0];
                     A1601ContagemResultado_CntSrvIndDvr = P00A823_A1601ContagemResultado_CntSrvIndDvr[0];
                     n1601ContagemResultado_CntSrvIndDvr = P00A823_n1601ContagemResultado_CntSrvIndDvr[0];
                     A1613ContagemResultado_CntSrvQtdUntCns = P00A823_A1613ContagemResultado_CntSrvQtdUntCns[0];
                     n1613ContagemResultado_CntSrvQtdUntCns = P00A823_n1613ContagemResultado_CntSrvQtdUntCns[0];
                     A1620ContagemResultado_CntSrvUndCnt = P00A823_A1620ContagemResultado_CntSrvUndCnt[0];
                     n1620ContagemResultado_CntSrvUndCnt = P00A823_n1620ContagemResultado_CntSrvUndCnt[0];
                     A1623ContagemResultado_CntSrvMmn = P00A823_A1623ContagemResultado_CntSrvMmn[0];
                     n1623ContagemResultado_CntSrvMmn = P00A823_n1623ContagemResultado_CntSrvMmn[0];
                     A1607ContagemResultado_PrzAnl = P00A823_A1607ContagemResultado_PrzAnl[0];
                     n1607ContagemResultado_PrzAnl = P00A823_n1607ContagemResultado_PrzAnl[0];
                     A1609ContagemResultado_PrzCrr = P00A823_A1609ContagemResultado_PrzCrr[0];
                     n1609ContagemResultado_PrzCrr = P00A823_n1609ContagemResultado_PrzCrr[0];
                     A1618ContagemResultado_PrzRsp = P00A823_A1618ContagemResultado_PrzRsp[0];
                     n1618ContagemResultado_PrzRsp = P00A823_n1618ContagemResultado_PrzRsp[0];
                     A1619ContagemResultado_PrzGrt = P00A823_A1619ContagemResultado_PrzGrt[0];
                     n1619ContagemResultado_PrzGrt = P00A823_n1619ContagemResultado_PrzGrt[0];
                     A1611ContagemResultado_PrzTpDias = P00A823_A1611ContagemResultado_PrzTpDias[0];
                     n1611ContagemResultado_PrzTpDias = P00A823_n1611ContagemResultado_PrzTpDias[0];
                     A1603ContagemResultado_CntCod = P00A823_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = P00A823_n1603ContagemResultado_CntCod[0];
                     A1650ContagemResultado_PrzInc = P00A823_A1650ContagemResultado_PrzInc[0];
                     n1650ContagemResultado_PrzInc = P00A823_n1650ContagemResultado_PrzInc[0];
                     A601ContagemResultado_Servico = P00A823_A601ContagemResultado_Servico[0];
                     n601ContagemResultado_Servico = P00A823_n601ContagemResultado_Servico[0];
                     A2008ContagemResultado_CntSrvAls = P00A823_A2008ContagemResultado_CntSrvAls[0];
                     n2008ContagemResultado_CntSrvAls = P00A823_n2008ContagemResultado_CntSrvAls[0];
                     A1398ContagemResultado_ServicoNaoRqrAtr = P00A823_A1398ContagemResultado_ServicoNaoRqrAtr[0];
                     n1398ContagemResultado_ServicoNaoRqrAtr = P00A823_n1398ContagemResultado_ServicoNaoRqrAtr[0];
                     A1593ContagemResultado_CntSrvTpVnc = P00A823_A1593ContagemResultado_CntSrvTpVnc[0];
                     n1593ContagemResultado_CntSrvTpVnc = P00A823_n1593ContagemResultado_CntSrvTpVnc[0];
                     A1594ContagemResultado_CntSrvFtrm = P00A823_A1594ContagemResultado_CntSrvFtrm[0];
                     n1594ContagemResultado_CntSrvFtrm = P00A823_n1594ContagemResultado_CntSrvFtrm[0];
                     A1596ContagemResultado_CntSrvPrc = P00A823_A1596ContagemResultado_CntSrvPrc[0];
                     n1596ContagemResultado_CntSrvPrc = P00A823_n1596ContagemResultado_CntSrvPrc[0];
                     A1597ContagemResultado_CntSrvVlrUndCnt = P00A823_A1597ContagemResultado_CntSrvVlrUndCnt[0];
                     n1597ContagemResultado_CntSrvVlrUndCnt = P00A823_n1597ContagemResultado_CntSrvVlrUndCnt[0];
                     A1598ContagemResultado_ServicoNome = P00A823_A1598ContagemResultado_ServicoNome[0];
                     n1598ContagemResultado_ServicoNome = P00A823_n1598ContagemResultado_ServicoNome[0];
                     A1599ContagemResultado_ServicoTpHrrq = P00A823_A1599ContagemResultado_ServicoTpHrrq[0];
                     n1599ContagemResultado_ServicoTpHrrq = P00A823_n1599ContagemResultado_ServicoTpHrrq[0];
                     A801ContagemResultado_ServicoSigla = P00A823_A801ContagemResultado_ServicoSigla[0];
                     n801ContagemResultado_ServicoSigla = P00A823_n801ContagemResultado_ServicoSigla[0];
                     A764ContagemResultado_ServicoGrupo = P00A823_A764ContagemResultado_ServicoGrupo[0];
                     n764ContagemResultado_ServicoGrupo = P00A823_n764ContagemResultado_ServicoGrupo[0];
                     A1062ContagemResultado_ServicoTela = P00A823_A1062ContagemResultado_ServicoTela[0];
                     n1062ContagemResultado_ServicoTela = P00A823_n1062ContagemResultado_ServicoTela[0];
                     A897ContagemResultado_ServicoAtivo = P00A823_A897ContagemResultado_ServicoAtivo[0];
                     n897ContagemResultado_ServicoAtivo = P00A823_n897ContagemResultado_ServicoAtivo[0];
                     A2050ContagemResultado_SrvLnhNegCod = P00A823_A2050ContagemResultado_SrvLnhNegCod[0];
                     n2050ContagemResultado_SrvLnhNegCod = P00A823_n2050ContagemResultado_SrvLnhNegCod[0];
                     A1621ContagemResultado_CntSrvUndCntSgl = P00A823_A1621ContagemResultado_CntSrvUndCntSgl[0];
                     n1621ContagemResultado_CntSrvUndCntSgl = P00A823_n1621ContagemResultado_CntSrvUndCntSgl[0];
                     A2209ContagemResultado_CntSrvUndCntNome = P00A823_A2209ContagemResultado_CntSrvUndCntNome[0];
                     n2209ContagemResultado_CntSrvUndCntNome = P00A823_n2209ContagemResultado_CntSrvUndCntNome[0];
                     A1612ContagemResultado_CntNum = P00A823_A1612ContagemResultado_CntNum[0];
                     n1612ContagemResultado_CntNum = P00A823_n1612ContagemResultado_CntNum[0];
                     A1604ContagemResultado_CntPrpCod = P00A823_A1604ContagemResultado_CntPrpCod[0];
                     n1604ContagemResultado_CntPrpCod = P00A823_n1604ContagemResultado_CntPrpCod[0];
                     A1615ContagemResultado_CntIndDvr = P00A823_A1615ContagemResultado_CntIndDvr[0];
                     n1615ContagemResultado_CntIndDvr = P00A823_n1615ContagemResultado_CntIndDvr[0];
                     A1617ContagemResultado_CntClcDvr = P00A823_A1617ContagemResultado_CntClcDvr[0];
                     n1617ContagemResultado_CntClcDvr = P00A823_n1617ContagemResultado_CntClcDvr[0];
                     A1616ContagemResultado_CntVlrUndCnt = P00A823_A1616ContagemResultado_CntVlrUndCnt[0];
                     n1616ContagemResultado_CntVlrUndCnt = P00A823_n1616ContagemResultado_CntVlrUndCnt[0];
                     A1632ContagemResultado_CntPrdFtrCada = P00A823_A1632ContagemResultado_CntPrdFtrCada[0];
                     n1632ContagemResultado_CntPrdFtrCada = P00A823_n1632ContagemResultado_CntPrdFtrCada[0];
                     A1633ContagemResultado_CntPrdFtrIni = P00A823_A1633ContagemResultado_CntPrdFtrIni[0];
                     n1633ContagemResultado_CntPrdFtrIni = P00A823_n1633ContagemResultado_CntPrdFtrIni[0];
                     A1634ContagemResultado_CntPrdFtrFim = P00A823_A1634ContagemResultado_CntPrdFtrFim[0];
                     n1634ContagemResultado_CntPrdFtrFim = P00A823_n1634ContagemResultado_CntPrdFtrFim[0];
                     A2082ContagemResultado_CntLmtFtr = P00A823_A2082ContagemResultado_CntLmtFtr[0];
                     n2082ContagemResultado_CntLmtFtr = P00A823_n2082ContagemResultado_CntLmtFtr[0];
                     A1624ContagemResultado_DatVgnInc = P00A823_A1624ContagemResultado_DatVgnInc[0];
                     n1624ContagemResultado_DatVgnInc = P00A823_n1624ContagemResultado_DatVgnInc[0];
                     A1605ContagemResultado_CntPrpPesCod = P00A823_A1605ContagemResultado_CntPrpPesCod[0];
                     n1605ContagemResultado_CntPrpPesCod = P00A823_n1605ContagemResultado_CntPrpPesCod[0];
                     A1606ContagemResultado_CntPrpPesNom = P00A823_A1606ContagemResultado_CntPrpPesNom[0];
                     n1606ContagemResultado_CntPrpPesNom = P00A823_n1606ContagemResultado_CntPrpPesNom[0];
                     A1608ContagemResultado_PrzExc = P00A823_A1608ContagemResultado_PrzExc[0];
                     n1608ContagemResultado_PrzExc = P00A823_n1608ContagemResultado_PrzExc[0];
                     A1610ContagemResultado_PrzTp = P00A823_A1610ContagemResultado_PrzTp[0];
                     n1610ContagemResultado_PrzTp = P00A823_n1610ContagemResultado_PrzTp[0];
                     A1625ContagemResultado_DatIncTA = P00A823_A1625ContagemResultado_DatIncTA[0];
                     n1625ContagemResultado_DatIncTA = P00A823_n1625ContagemResultado_DatIncTA[0];
                     /* Using cursor P00A825 */
                     pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
                     if ( (pr_default.getStatus(2) != 101) )
                     {
                        A531ContagemResultado_StatusUltCnt = P00A825_A531ContagemResultado_StatusUltCnt[0];
                        A566ContagemResultado_DataUltCnt = P00A825_A566ContagemResultado_DataUltCnt[0];
                        A825ContagemResultado_HoraUltCnt = P00A825_A825ContagemResultado_HoraUltCnt[0];
                        A584ContagemResultado_ContadorFM = P00A825_A584ContagemResultado_ContadorFM[0];
                        A682ContagemResultado_PFBFMUltima = P00A825_A682ContagemResultado_PFBFMUltima[0];
                        A683ContagemResultado_PFLFMUltima = P00A825_A683ContagemResultado_PFLFMUltima[0];
                        A684ContagemResultado_PFBFSUltima = P00A825_A684ContagemResultado_PFBFSUltima[0];
                        A1480ContagemResultado_CstUntUltima = P00A825_A1480ContagemResultado_CstUntUltima[0];
                        A685ContagemResultado_PFLFSUltima = P00A825_A685ContagemResultado_PFLFSUltima[0];
                        A802ContagemResultado_DeflatorCnt = P00A825_A802ContagemResultado_DeflatorCnt[0];
                     }
                     else
                     {
                        A531ContagemResultado_StatusUltCnt = 0;
                        A566ContagemResultado_DataUltCnt = DateTime.MinValue;
                        A825ContagemResultado_HoraUltCnt = "";
                        A584ContagemResultado_ContadorFM = 0;
                        A682ContagemResultado_PFBFMUltima = 0;
                        A683ContagemResultado_PFLFMUltima = 0;
                        A684ContagemResultado_PFBFSUltima = 0;
                        A1480ContagemResultado_CstUntUltima = 0;
                        A685ContagemResultado_PFLFSUltima = 0;
                        A802ContagemResultado_DeflatorCnt = 0;
                     }
                     pr_default.close(2);
                     GXt_boolean3 = A1802ContagemResultado_TemPndHmlg;
                     new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean3) ;
                     A1802ContagemResultado_TemPndHmlg = GXt_boolean3;
                     GXt_char2 = A486ContagemResultado_EsforcoTotal;
                     new prc_contageresultado_esforcototal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_char2) ;
                     A486ContagemResultado_EsforcoTotal = GXt_char2;
                     GXt_decimal1 = A574ContagemResultado_PFFinal;
                     new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                     A574ContagemResultado_PFFinal = GXt_decimal1;
                     GXt_int5 = A585ContagemResultado_Erros;
                     new prc_pendenciasdaos(context ).execute(  A456ContagemResultado_Codigo, out  GXt_int5) ;
                     A585ContagemResultado_Erros = GXt_int5;
                     GXt_decimal1 = A497IndiceDivergencia;
                     new prc_indicedivergencia(context ).execute( ref  A1553ContagemResultado_CntSrvCod, out  GXt_decimal1) ;
                     A497IndiceDivergencia = GXt_decimal1;
                     GXt_char2 = A498CalculoDivergencia;
                     new prc_calculodivergencia(context ).execute(  A490ContagemResultado_ContratadaCod, out  GXt_char2) ;
                     A498CalculoDivergencia = GXt_char2;
                     A553ContagemResultado_DemandaFM_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A493ContagemResultado_DemandaFM, 1, 18), "."));
                     A1815ContagemResultado_DmnSrvPrst = StringUtil.Trim( A493ContagemResultado_DemandaFM) + " " + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " (" + A803ContagemResultado_ContratadaSigla + ")";
                     A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                     A599ContagemResultado_Demanda_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A457ContagemResultado_Demanda, 1, 18), "."));
                     GXt_int4 = A1352ContagemResultado_ContratanteDoOwner;
                     new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A508ContagemResultado_Owner, out  GXt_int4) ;
                     A1352ContagemResultado_ContratanteDoOwner = GXt_int4;
                     GXt_int4 = A1236ContagemResultado_ContratanteDoResponsavel;
                     new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int4) ;
                     A1236ContagemResultado_ContratanteDoResponsavel = GXt_int4;
                     GXt_int4 = A1229ContagemResultado_ContratadaDoResponsavel;
                     new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int4) ;
                     A1229ContagemResultado_ContratadaDoResponsavel = GXt_int4;
                     A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
                     if ( ! P00A823_n1043ContagemResultado_LiqLogCod[0] )
                     {
                        A1028ContagemResultado_Liquidada = true;
                     }
                     else
                     {
                        if ( P00A823_n1043ContagemResultado_LiqLogCod[0] )
                        {
                           A1028ContagemResultado_Liquidada = false;
                        }
                        else
                        {
                           A1028ContagemResultado_Liquidada = false;
                        }
                     }
                     GXt_char2 = A2091ContagemResultado_CntSrvPrrNome;
                     new prc_buscaprioridadeos(context ).execute(  A1443ContagemResultado_CntSrvPrrCod, out  GXt_char2) ;
                     A2091ContagemResultado_CntSrvPrrNome = GXt_char2;
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataDmn") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A471ContagemResultado_DataDmn, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataInicio") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1790ContagemResultado_DataInicio, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataEntrega") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A472ContagemResultado_DataEntrega, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_HoraEntrega") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataPrevista") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataExecucao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1349ContagemResultado_DataExecucao, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataEntregaReal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataHomologacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1348ContagemResultado_DataHomologacao, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrazoInicialDias") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrazoMaisDias") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1237ContagemResultado_PrazoMaisDias), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaPessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A499ContagemResultado_ContratadaPessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaPessoaNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A500ContagemResultado_ContratadaPessoaNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaSigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A803ContagemResultado_ContratadaSigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaTipoFab") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1326ContagemResultado_ContratadaTipoFab;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaCNPJ") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1817ContagemResultado_ContratadaCNPJ;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AreaTrabalhoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "IndiceDivergencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A497IndiceDivergencia, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "CalculoDivergencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A498CalculoDivergencia;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaOrigemCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaOrigemSigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A866ContagemResultado_ContratadaOrigemSigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaOrigemPesCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A807ContagemResultado_ContratadaOrigemPesCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaOrigemPesNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A808ContagemResultado_ContratadaOrigemPesNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaOrigemAreaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A867ContagemResultado_ContratadaOrigemAreaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaOrigemUsaSistema") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2093ContagemResultado_ContratadaOrigemUsaSistema);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContadorFSCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CrFSPessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A480ContagemResultado_CrFSPessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContadorFSNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A455ContagemResultado_ContadorFSNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Demanda") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A457ContagemResultado_Demanda;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DmnSrvPrst") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1815ContagemResultado_DmnSrvPrst;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Demanda_ORDER") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A599ContagemResultado_Demanda_ORDER), 18, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Erros") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A585ContagemResultado_Erros), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DemandaFM") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A493ContagemResultado_DemandaFM;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DemandaFM_ORDER") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A553ContagemResultado_DemandaFM_ORDER), 18, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_OsFsOsFm") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A501ContagemResultado_OsFsOsFm;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_OSManual") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1173ContagemResultado_OSManual);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Descricao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A494ContagemResultado_Descricao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Link") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A465ContagemResultado_Link;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SistemaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SistemaNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A495ContagemResultado_SistemaNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contagemresultado_SistemaAreaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A496Contagemresultado_SistemaAreaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemrResultado_SistemaSigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A509ContagemrResultado_SistemaSigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SistemaCoord") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A515ContagemResultado_SistemaCoord;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SistemaAtivo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A804ContagemResultado_SistemaAtivo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Modulo_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Modulo_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A143Modulo_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Modulo_Sigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A145Modulo_Sigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_NaoCnfDmnCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_NaoCnfDmnNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A477ContagemResultado_NaoCnfDmnNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_NaoCnfDmnGls") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2030ContagemResultado_NaoCnfDmnGls);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_StatusDmn") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A484ContagemResultado_StatusDmn;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_StatusUltCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_StatusDmnVnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A771ContagemResultado_StatusDmnVnc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataUltCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_HoraUltCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A825ContagemResultado_HoraUltCnt;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntComNaoCnf") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A578ContagemResultado_CntComNaoCnf), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFFinal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContadorFM") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A584ContagemResultado_ContadorFM), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFBFMUltima") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFLFMUltima") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A683ContagemResultado_PFLFMUltima, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFBFSUltima") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CstUntUltima") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1480ContagemResultado_CstUntUltima, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFLFSUltima") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A685ContagemResultado_PFLFSUltima, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DeflatorCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A802ContagemResultado_DeflatorCnt, 6, 3);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Servico") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvAls") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2008ContagemResultado_CntSrvAls;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoGrupo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A764ContagemResultado_ServicoGrupo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoSigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A801ContagemResultado_ServicoSigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoTela") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1062ContagemResultado_ServicoTela;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoAtivo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A897ContagemResultado_ServicoAtivo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoNaoRqrAtr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1398ContagemResultado_ServicoNaoRqrAtr);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoSS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoSSSgl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1637ContagemResultado_ServicoSSSgl;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SrvLnhNegCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2050ContagemResultado_SrvLnhNegCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_EhValidacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A485ContagemResultado_EhValidacao);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_EsforcoTotal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A486ContagemResultado_EsforcoTotal;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_EsforcoSoma") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A510ContagemResultado_EsforcoSoma), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Observacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A514ContagemResultado_Observacao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Evidencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A592ContagemResultado_Evidencia;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataCadastro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Owner") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Owner_Identificao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2118ContagemResultado_Owner_Identificao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratanteDoOwner") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1352ContagemResultado_ContratanteDoOwner), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaDoOwner") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1353ContagemResultado_ContratadaDoOwner), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Responsavel") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ResponsavelPessCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1312ContagemResultado_ResponsavelPessCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ResponsavelPessNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1313ContagemResultado_ResponsavelPessNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratadaDoResponsavel") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ContratanteDoResponsavel") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ValorPF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A512ContagemResultado_ValorPF, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Custo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1180ContagemResultado_Custo, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ValorFinal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_LoteAceiteCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_LoteAceite") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A528ContagemResultado_LoteAceite;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataAceite") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A529ContagemResultado_DataAceite, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_AceiteUserCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A525ContagemResultado_AceiteUserCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_AceitePessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A526ContagemResultado_AceitePessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_AceiteUserNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A527ContagemResultado_AceiteUserNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_LoteNFe") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1547ContagemResultado_LoteNFe), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_VlrAceite") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1559ContagemResultado_VlrAceite, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Baseline") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A598ContagemResultado_Baseline);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_OSVinculada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DmnVinculada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A603ContagemResultado_DmnVinculada;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFBFSImp") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFLFSImp") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A799ContagemResultado_PFLFSImp, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PBFinal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1178ContagemResultado_PBFinal, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PLFinal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1179ContagemResultado_PLFinal, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_LiqLogCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Liquidada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1028ContagemResultado_Liquidada);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultadoLiqLog_Data") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_FncUsrCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Agrupador") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1046ContagemResultado_Agrupador;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_GlsData") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1049ContagemResultado_GlsData, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_GlsDescricao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1050ContagemResultado_GlsDescricao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_GlsValor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1051ContagemResultado_GlsValor, 12, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_GlsUser") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1052ContagemResultado_GlsUser), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_RdmnIssueId") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_RdmnProjectId") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1390ContagemResultado_RdmnProjectId), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_RdmnUpdated") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1392ContagemResultado_RdmnUpdated, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvPrrCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvPrrNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2091ContagemResultado_CntSrvPrrNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvPrrPrz") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1444ContagemResultado_CntSrvPrrPrz, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvPrrCst") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1445ContagemResultado_CntSrvPrrCst, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TemDpnHmlg") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1457ContagemResultado_TemDpnHmlg);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TemPndHmlg") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1802ContagemResultado_TemPndHmlg);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TmpEstAnl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1519ContagemResultado_TmpEstAnl), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TmpEstExc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1505ContagemResultado_TmpEstExc), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TmpEstCrr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1506ContagemResultado_TmpEstCrr), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_InicioAnl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1520ContagemResultado_InicioAnl, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_FimAnl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1521ContagemResultado_FimAnl, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_InicioExc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1509ContagemResultado_InicioExc, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_FimExc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1510ContagemResultado_FimExc, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_InicioCrr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1511ContagemResultado_InicioCrr, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_FimCrr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A1512ContagemResultado_FimCrr, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Evento") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ProjetoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1544ContagemResultado_ProjetoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvTpVnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1593ContagemResultado_CntSrvTpVnc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvFtrm") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1594ContagemResultado_CntSrvFtrm;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvPrc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1596ContagemResultado_CntSrvPrc, 7, 3);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvVlrUndCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1597ContagemResultado_CntSrvVlrUndCnt, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvSttPgmFnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1600ContagemResultado_CntSrvSttPgmFnc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvIndDvr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1601ContagemResultado_CntSrvIndDvr, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvQtdUntCns") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvUndCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1620ContagemResultado_CntSrvUndCnt), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvUndCntSgl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1621ContagemResultado_CntSrvUndCntSgl;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvUndCntNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2209ContagemResultado_CntSrvUndCntNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvMmn") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1623ContagemResultado_CntSrvMmn;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzAnl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1607ContagemResultado_PrzAnl), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzExc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1608ContagemResultado_PrzExc), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzCrr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1609ContagemResultado_PrzCrr), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzRsp") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1618ContagemResultado_PrzRsp), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzGrt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1619ContagemResultado_PrzGrt), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzTp") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1610ContagemResultado_PrzTp;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzTpDias") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1611ContagemResultado_PrzTpDias;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1603ContagemResultado_CntCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntNum") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1612ContagemResultado_CntNum;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntPrpCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1604ContagemResultado_CntPrpCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntPrpPesCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1605ContagemResultado_CntPrpPesCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntPrpPesNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1606ContagemResultado_CntPrpPesNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntIndDvr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1615ContagemResultado_CntIndDvr, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntClcDvr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1617ContagemResultado_CntClcDvr;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntVlrUndCnt") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1616ContagemResultado_CntVlrUndCnt, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntPrdFtrCada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1632ContagemResultado_CntPrdFtrCada;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntPrdFtrIni") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1633ContagemResultado_CntPrdFtrIni), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntPrdFtrFim") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1634ContagemResultado_CntPrdFtrFim), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntLmtFtr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2082ContagemResultado_CntLmtFtr, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DatVgnInc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1624ContagemResultado_DatVgnInc, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DatIncTA") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1625ContagemResultado_DatIncTA, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1598ContagemResultado_ServicoNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_ServicoTpHrrq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1599ContagemResultado_ServicoTpHrrq), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CodSrvVnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1591ContagemResultado_CodSrvVnc), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SiglaSrvVnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1590ContagemResultado_SiglaSrvVnc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CodSrvSSVnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1765ContagemResultado_CodSrvSSVnc), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntSrvVncCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1627ContagemResultado_CntSrvVncCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntVncCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1622ContagemResultado_CntVncCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_CntadaOsVinc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2136ContagemResultado_CntadaOsVinc), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SerGrupoVinc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2137ContagemResultado_SerGrupoVinc), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_GlsIndValor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1602ContagemResultado_GlsIndValor, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TipoRegistro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_UOOwner") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Referencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1585ContagemResultado_Referencia;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Restricoes") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1586ContagemResultado_Restricoes;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrioridadePrevista") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1587ContagemResultado_PrioridadePrevista;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PrzInc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1650ContagemResultado_PrzInc), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_TemProposta") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1701ContagemResultado_TemProposta);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Combinada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1714ContagemResultado_Combinada);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_Entrega") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1762ContagemResultado_Entrega), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_SemCusto") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1791ContagemResultado_SemCusto);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_VlrCnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1854ContagemResultado_VlrCnc, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_PFCnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1855ContagemResultado_PFCnc, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_DataPrvPgm") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1903ContagemResultado_DataPrvPgm, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContagemResultado_QuantidadeSolicitada") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2133ContagemResultado_QuantidadeSolicitada, 9, 4);
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A812_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00A812_A1600ContagemResultado_CntSrvSttPgmFnc = new String[] {""} ;
         P00A812_n1600ContagemResultado_CntSrvSttPgmFnc = new bool[] {false} ;
         P00A812_A1601ContagemResultado_CntSrvIndDvr = new decimal[1] ;
         P00A812_n1601ContagemResultado_CntSrvIndDvr = new bool[] {false} ;
         P00A812_A1613ContagemResultado_CntSrvQtdUntCns = new decimal[1] ;
         P00A812_n1613ContagemResultado_CntSrvQtdUntCns = new bool[] {false} ;
         P00A812_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         P00A812_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         P00A812_A1621ContagemResultado_CntSrvUndCntSgl = new String[] {""} ;
         P00A812_n1621ContagemResultado_CntSrvUndCntSgl = new bool[] {false} ;
         P00A812_A2209ContagemResultado_CntSrvUndCntNome = new String[] {""} ;
         P00A812_n2209ContagemResultado_CntSrvUndCntNome = new bool[] {false} ;
         P00A812_A1623ContagemResultado_CntSrvMmn = new String[] {""} ;
         P00A812_n1623ContagemResultado_CntSrvMmn = new bool[] {false} ;
         P00A812_A1607ContagemResultado_PrzAnl = new short[1] ;
         P00A812_n1607ContagemResultado_PrzAnl = new bool[] {false} ;
         P00A812_A1609ContagemResultado_PrzCrr = new short[1] ;
         P00A812_n1609ContagemResultado_PrzCrr = new bool[] {false} ;
         P00A812_A1618ContagemResultado_PrzRsp = new short[1] ;
         P00A812_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         P00A812_A1619ContagemResultado_PrzGrt = new short[1] ;
         P00A812_n1619ContagemResultado_PrzGrt = new bool[] {false} ;
         P00A812_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00A812_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00A812_A1603ContagemResultado_CntCod = new int[1] ;
         P00A812_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00A812_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00A812_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00A812_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P00A812_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P00A812_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P00A812_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P00A812_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P00A812_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P00A812_A1615ContagemResultado_CntIndDvr = new decimal[1] ;
         P00A812_n1615ContagemResultado_CntIndDvr = new bool[] {false} ;
         P00A812_A1617ContagemResultado_CntClcDvr = new String[] {""} ;
         P00A812_n1617ContagemResultado_CntClcDvr = new bool[] {false} ;
         P00A812_A1616ContagemResultado_CntVlrUndCnt = new decimal[1] ;
         P00A812_n1616ContagemResultado_CntVlrUndCnt = new bool[] {false} ;
         P00A812_A1632ContagemResultado_CntPrdFtrCada = new String[] {""} ;
         P00A812_n1632ContagemResultado_CntPrdFtrCada = new bool[] {false} ;
         P00A812_A1633ContagemResultado_CntPrdFtrIni = new short[1] ;
         P00A812_n1633ContagemResultado_CntPrdFtrIni = new bool[] {false} ;
         P00A812_A1634ContagemResultado_CntPrdFtrFim = new short[1] ;
         P00A812_n1634ContagemResultado_CntPrdFtrFim = new bool[] {false} ;
         P00A812_A2082ContagemResultado_CntLmtFtr = new decimal[1] ;
         P00A812_n2082ContagemResultado_CntLmtFtr = new bool[] {false} ;
         P00A812_A1624ContagemResultado_DatVgnInc = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1624ContagemResultado_DatVgnInc = new bool[] {false} ;
         P00A812_A1598ContagemResultado_ServicoNome = new String[] {""} ;
         P00A812_n1598ContagemResultado_ServicoNome = new bool[] {false} ;
         P00A812_A1599ContagemResultado_ServicoTpHrrq = new short[1] ;
         P00A812_n1599ContagemResultado_ServicoTpHrrq = new bool[] {false} ;
         P00A812_A1591ContagemResultado_CodSrvVnc = new int[1] ;
         P00A812_n1591ContagemResultado_CodSrvVnc = new bool[] {false} ;
         P00A812_A1590ContagemResultado_SiglaSrvVnc = new String[] {""} ;
         P00A812_n1590ContagemResultado_SiglaSrvVnc = new bool[] {false} ;
         P00A812_A1765ContagemResultado_CodSrvSSVnc = new int[1] ;
         P00A812_n1765ContagemResultado_CodSrvSSVnc = new bool[] {false} ;
         P00A812_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         P00A812_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         P00A812_A1622ContagemResultado_CntVncCod = new int[1] ;
         P00A812_n1622ContagemResultado_CntVncCod = new bool[] {false} ;
         P00A812_A2136ContagemResultado_CntadaOsVinc = new int[1] ;
         P00A812_n2136ContagemResultado_CntadaOsVinc = new bool[] {false} ;
         P00A812_A2137ContagemResultado_SerGrupoVinc = new int[1] ;
         P00A812_n2137ContagemResultado_SerGrupoVinc = new bool[] {false} ;
         P00A812_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00A812_A1584ContagemResultado_UOOwner = new int[1] ;
         P00A812_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         P00A812_A1585ContagemResultado_Referencia = new String[] {""} ;
         P00A812_n1585ContagemResultado_Referencia = new bool[] {false} ;
         P00A812_A1586ContagemResultado_Restricoes = new String[] {""} ;
         P00A812_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         P00A812_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         P00A812_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         P00A812_A1650ContagemResultado_PrzInc = new short[1] ;
         P00A812_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         P00A812_A1714ContagemResultado_Combinada = new bool[] {false} ;
         P00A812_n1714ContagemResultado_Combinada = new bool[] {false} ;
         P00A812_A1762ContagemResultado_Entrega = new short[1] ;
         P00A812_n1762ContagemResultado_Entrega = new bool[] {false} ;
         P00A812_A1791ContagemResultado_SemCusto = new bool[] {false} ;
         P00A812_n1791ContagemResultado_SemCusto = new bool[] {false} ;
         P00A812_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00A812_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00A812_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P00A812_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P00A812_A1903ContagemResultado_DataPrvPgm = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1903ContagemResultado_DataPrvPgm = new bool[] {false} ;
         P00A812_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         P00A812_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         P00A812_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00A812_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00A812_A825ContagemResultado_HoraUltCnt = new String[] {""} ;
         P00A812_A578ContagemResultado_CntComNaoCnf = new int[1] ;
         P00A812_n578ContagemResultado_CntComNaoCnf = new bool[] {false} ;
         P00A812_A584ContagemResultado_ContadorFM = new int[1] ;
         P00A812_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00A812_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00A812_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00A812_A1480ContagemResultado_CstUntUltima = new decimal[1] ;
         P00A812_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00A812_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00A812_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00A812_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00A812_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00A812_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00A812_A1353ContagemResultado_ContratadaDoOwner = new int[1] ;
         P00A812_n1353ContagemResultado_ContratadaDoOwner = new bool[] {false} ;
         P00A812_A1608ContagemResultado_PrzExc = new short[1] ;
         P00A812_n1608ContagemResultado_PrzExc = new bool[] {false} ;
         P00A812_A1610ContagemResultado_PrzTp = new String[] {""} ;
         P00A812_n1610ContagemResultado_PrzTp = new bool[] {false} ;
         P00A812_A1625ContagemResultado_DatIncTA = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1625ContagemResultado_DatIncTA = new bool[] {false} ;
         P00A812_A1602ContagemResultado_GlsIndValor = new decimal[1] ;
         P00A812_n1602ContagemResultado_GlsIndValor = new bool[] {false} ;
         P00A812_A1701ContagemResultado_TemProposta = new bool[] {false} ;
         P00A812_n1701ContagemResultado_TemProposta = new bool[] {false} ;
         P00A812_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         P00A812_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         P00A812_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P00A812_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P00A812_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00A812_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00A812_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00A812_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00A812_A890ContagemResultado_Responsavel = new int[1] ;
         P00A812_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00A812_A508ContagemResultado_Owner = new int[1] ;
         P00A812_A456ContagemResultado_Codigo = new int[1] ;
         P00A812_A457ContagemResultado_Demanda = new String[] {""} ;
         P00A812_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00A812_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00A812_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00A812_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00A812_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00A812_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00A812_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00A812_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00A812_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00A812_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00A812_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00A812_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00A812_A1790ContagemResultado_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1790ContagemResultado_DataInicio = new bool[] {false} ;
         P00A812_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00A812_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00A812_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00A812_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00A812_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00A812_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00A812_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00A812_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00A812_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00A812_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00A812_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00A812_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00A812_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00A812_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00A812_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00A812_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00A812_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00A812_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00A812_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00A812_A1817ContagemResultado_ContratadaCNPJ = new String[] {""} ;
         P00A812_n1817ContagemResultado_ContratadaCNPJ = new bool[] {false} ;
         P00A812_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00A812_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00A812_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         P00A812_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         P00A812_A807ContagemResultado_ContratadaOrigemPesCod = new int[1] ;
         P00A812_n807ContagemResultado_ContratadaOrigemPesCod = new bool[] {false} ;
         P00A812_A808ContagemResultado_ContratadaOrigemPesNom = new String[] {""} ;
         P00A812_n808ContagemResultado_ContratadaOrigemPesNom = new bool[] {false} ;
         P00A812_A867ContagemResultado_ContratadaOrigemAreaCod = new int[1] ;
         P00A812_n867ContagemResultado_ContratadaOrigemAreaCod = new bool[] {false} ;
         P00A812_A2093ContagemResultado_ContratadaOrigemUsaSistema = new bool[] {false} ;
         P00A812_n2093ContagemResultado_ContratadaOrigemUsaSistema = new bool[] {false} ;
         P00A812_A454ContagemResultado_ContadorFSCod = new int[1] ;
         P00A812_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         P00A812_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         P00A812_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         P00A812_A455ContagemResultado_ContadorFSNom = new String[] {""} ;
         P00A812_n455ContagemResultado_ContadorFSNom = new bool[] {false} ;
         P00A812_A1452ContagemResultado_SS = new int[1] ;
         P00A812_n1452ContagemResultado_SS = new bool[] {false} ;
         P00A812_A1173ContagemResultado_OSManual = new bool[] {false} ;
         P00A812_n1173ContagemResultado_OSManual = new bool[] {false} ;
         P00A812_A494ContagemResultado_Descricao = new String[] {""} ;
         P00A812_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00A812_A465ContagemResultado_Link = new String[] {""} ;
         P00A812_n465ContagemResultado_Link = new bool[] {false} ;
         P00A812_A489ContagemResultado_SistemaCod = new int[1] ;
         P00A812_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00A812_A495ContagemResultado_SistemaNom = new String[] {""} ;
         P00A812_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         P00A812_A496Contagemresultado_SistemaAreaCod = new int[1] ;
         P00A812_n496Contagemresultado_SistemaAreaCod = new bool[] {false} ;
         P00A812_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00A812_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00A812_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00A812_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00A812_A804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         P00A812_n804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         P00A812_A146Modulo_Codigo = new int[1] ;
         P00A812_n146Modulo_Codigo = new bool[] {false} ;
         P00A812_A143Modulo_Nome = new String[] {""} ;
         P00A812_A145Modulo_Sigla = new String[] {""} ;
         P00A812_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00A812_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00A812_A477ContagemResultado_NaoCnfDmnNom = new String[] {""} ;
         P00A812_n477ContagemResultado_NaoCnfDmnNom = new bool[] {false} ;
         P00A812_A2030ContagemResultado_NaoCnfDmnGls = new bool[] {false} ;
         P00A812_n2030ContagemResultado_NaoCnfDmnGls = new bool[] {false} ;
         P00A812_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00A812_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00A812_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00A812_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00A812_A601ContagemResultado_Servico = new int[1] ;
         P00A812_n601ContagemResultado_Servico = new bool[] {false} ;
         P00A812_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P00A812_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P00A812_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00A812_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00A812_A1062ContagemResultado_ServicoTela = new String[] {""} ;
         P00A812_n1062ContagemResultado_ServicoTela = new bool[] {false} ;
         P00A812_A897ContagemResultado_ServicoAtivo = new bool[] {false} ;
         P00A812_n897ContagemResultado_ServicoAtivo = new bool[] {false} ;
         P00A812_A1398ContagemResultado_ServicoNaoRqrAtr = new bool[] {false} ;
         P00A812_n1398ContagemResultado_ServicoNaoRqrAtr = new bool[] {false} ;
         P00A812_A1636ContagemResultado_ServicoSS = new int[1] ;
         P00A812_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P00A812_A1637ContagemResultado_ServicoSSSgl = new String[] {""} ;
         P00A812_n1637ContagemResultado_ServicoSSSgl = new bool[] {false} ;
         P00A812_A2050ContagemResultado_SrvLnhNegCod = new int[1] ;
         P00A812_n2050ContagemResultado_SrvLnhNegCod = new bool[] {false} ;
         P00A812_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00A812_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00A812_A514ContagemResultado_Observacao = new String[] {""} ;
         P00A812_n514ContagemResultado_Observacao = new bool[] {false} ;
         P00A812_A592ContagemResultado_Evidencia = new String[] {""} ;
         P00A812_n592ContagemResultado_Evidencia = new bool[] {false} ;
         P00A812_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         P00A812_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         P00A812_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         P00A812_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         P00A812_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         P00A812_A1180ContagemResultado_Custo = new decimal[1] ;
         P00A812_n1180ContagemResultado_Custo = new bool[] {false} ;
         P00A812_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00A812_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00A812_A528ContagemResultado_LoteAceite = new String[] {""} ;
         P00A812_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         P00A812_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         P00A812_n529ContagemResultado_DataAceite = new bool[] {false} ;
         P00A812_A525ContagemResultado_AceiteUserCod = new int[1] ;
         P00A812_n525ContagemResultado_AceiteUserCod = new bool[] {false} ;
         P00A812_A526ContagemResultado_AceitePessoaCod = new int[1] ;
         P00A812_n526ContagemResultado_AceitePessoaCod = new bool[] {false} ;
         P00A812_A527ContagemResultado_AceiteUserNom = new String[] {""} ;
         P00A812_n527ContagemResultado_AceiteUserNom = new bool[] {false} ;
         P00A812_A1547ContagemResultado_LoteNFe = new int[1] ;
         P00A812_n1547ContagemResultado_LoteNFe = new bool[] {false} ;
         P00A812_A1559ContagemResultado_VlrAceite = new decimal[1] ;
         P00A812_n1559ContagemResultado_VlrAceite = new bool[] {false} ;
         P00A812_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00A812_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00A812_A602ContagemResultado_OSVinculada = new int[1] ;
         P00A812_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00A812_A603ContagemResultado_DmnVinculada = new String[] {""} ;
         P00A812_n603ContagemResultado_DmnVinculada = new bool[] {false} ;
         P00A812_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P00A812_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P00A812_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P00A812_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P00A812_A1178ContagemResultado_PBFinal = new decimal[1] ;
         P00A812_n1178ContagemResultado_PBFinal = new bool[] {false} ;
         P00A812_A1179ContagemResultado_PLFinal = new decimal[1] ;
         P00A812_n1179ContagemResultado_PLFinal = new bool[] {false} ;
         P00A812_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         P00A812_A1044ContagemResultado_FncUsrCod = new int[1] ;
         P00A812_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         P00A812_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00A812_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00A812_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P00A812_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P00A812_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P00A812_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00A812_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00A812_A1052ContagemResultado_GlsUser = new int[1] ;
         P00A812_n1052ContagemResultado_GlsUser = new bool[] {false} ;
         P00A812_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         P00A812_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         P00A812_A1390ContagemResultado_RdmnProjectId = new int[1] ;
         P00A812_n1390ContagemResultado_RdmnProjectId = new bool[] {false} ;
         P00A812_A1392ContagemResultado_RdmnUpdated = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1392ContagemResultado_RdmnUpdated = new bool[] {false} ;
         P00A812_A1444ContagemResultado_CntSrvPrrPrz = new decimal[1] ;
         P00A812_n1444ContagemResultado_CntSrvPrrPrz = new bool[] {false} ;
         P00A812_A1445ContagemResultado_CntSrvPrrCst = new decimal[1] ;
         P00A812_n1445ContagemResultado_CntSrvPrrCst = new bool[] {false} ;
         P00A812_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00A812_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00A812_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00A812_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00A812_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00A812_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00A812_A1506ContagemResultado_TmpEstCrr = new int[1] ;
         P00A812_n1506ContagemResultado_TmpEstCrr = new bool[] {false} ;
         P00A812_A1520ContagemResultado_InicioAnl = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1520ContagemResultado_InicioAnl = new bool[] {false} ;
         P00A812_A1521ContagemResultado_FimAnl = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1521ContagemResultado_FimAnl = new bool[] {false} ;
         P00A812_A1509ContagemResultado_InicioExc = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1509ContagemResultado_InicioExc = new bool[] {false} ;
         P00A812_A1510ContagemResultado_FimExc = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1510ContagemResultado_FimExc = new bool[] {false} ;
         P00A812_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P00A812_A1512ContagemResultado_FimCrr = new DateTime[] {DateTime.MinValue} ;
         P00A812_n1512ContagemResultado_FimCrr = new bool[] {false} ;
         P00A812_A1515ContagemResultado_Evento = new short[1] ;
         P00A812_n1515ContagemResultado_Evento = new bool[] {false} ;
         P00A812_A1544ContagemResultado_ProjetoCod = new int[1] ;
         P00A812_n1544ContagemResultado_ProjetoCod = new bool[] {false} ;
         P00A812_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P00A812_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P00A812_A1594ContagemResultado_CntSrvFtrm = new String[] {""} ;
         P00A812_n1594ContagemResultado_CntSrvFtrm = new bool[] {false} ;
         P00A812_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P00A812_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P00A812_A1597ContagemResultado_CntSrvVlrUndCnt = new decimal[1] ;
         P00A812_n1597ContagemResultado_CntSrvVlrUndCnt = new bool[] {false} ;
         A1600ContagemResultado_CntSrvSttPgmFnc = "";
         A1621ContagemResultado_CntSrvUndCntSgl = "";
         A2209ContagemResultado_CntSrvUndCntNome = "";
         A1623ContagemResultado_CntSrvMmn = "";
         A1611ContagemResultado_PrzTpDias = "";
         A1612ContagemResultado_CntNum = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         A1617ContagemResultado_CntClcDvr = "";
         A1632ContagemResultado_CntPrdFtrCada = "";
         A1624ContagemResultado_DatVgnInc = DateTime.MinValue;
         A1598ContagemResultado_ServicoNome = "";
         A1590ContagemResultado_SiglaSrvVnc = "";
         A1585ContagemResultado_Referencia = "";
         A1586ContagemResultado_Restricoes = "";
         A1587ContagemResultado_PrioridadePrevista = "";
         A1903ContagemResultado_DataPrvPgm = DateTime.MinValue;
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A825ContagemResultado_HoraUltCnt = "";
         A2118ContagemResultado_Owner_Identificao = "";
         A1610ContagemResultado_PrzTp = "";
         A1625ContagemResultado_DatIncTA = DateTime.MinValue;
         A457ContagemResultado_Demanda = "";
         A803ContagemResultado_ContratadaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1790ContagemResultado_DataInicio = DateTime.MinValue;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         A500ContagemResultado_ContratadaPessoaNom = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A1817ContagemResultado_ContratadaCNPJ = "";
         A866ContagemResultado_ContratadaOrigemSigla = "";
         A808ContagemResultado_ContratadaOrigemPesNom = "";
         A455ContagemResultado_ContadorFSNom = "";
         A494ContagemResultado_Descricao = "";
         A465ContagemResultado_Link = "";
         A495ContagemResultado_SistemaNom = "";
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         A477ContagemResultado_NaoCnfDmnNom = "";
         A484ContagemResultado_StatusDmn = "";
         A771ContagemResultado_StatusDmnVnc = "";
         A2008ContagemResultado_CntSrvAls = "";
         A1062ContagemResultado_ServicoTela = "";
         A1637ContagemResultado_ServicoSSSgl = "";
         A514ContagemResultado_Observacao = "";
         A592ContagemResultado_Evidencia = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1313ContagemResultado_ResponsavelPessNome = "";
         A528ContagemResultado_LoteAceite = "";
         A529ContagemResultado_DataAceite = (DateTime)(DateTime.MinValue);
         A527ContagemResultado_AceiteUserNom = "";
         A603ContagemResultado_DmnVinculada = "";
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A1049ContagemResultado_GlsData = DateTime.MinValue;
         A1050ContagemResultado_GlsDescricao = "";
         A1392ContagemResultado_RdmnUpdated = (DateTime)(DateTime.MinValue);
         A1520ContagemResultado_InicioAnl = (DateTime)(DateTime.MinValue);
         A1521ContagemResultado_FimAnl = (DateTime)(DateTime.MinValue);
         A1509ContagemResultado_InicioExc = (DateTime)(DateTime.MinValue);
         A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
         A1511ContagemResultado_InicioCrr = (DateTime)(DateTime.MinValue);
         A1512ContagemResultado_FimCrr = (DateTime)(DateTime.MinValue);
         A1593ContagemResultado_CntSrvTpVnc = "";
         A1594ContagemResultado_CntSrvFtrm = "";
         A498CalculoDivergencia = "";
         A501ContagemResultado_OsFsOsFm = "";
         A1815ContagemResultado_DmnSrvPrst = "";
         A486ContagemResultado_EsforcoTotal = "";
         A2091ContagemResultado_CntSrvPrrNome = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A823_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00A823_A1600ContagemResultado_CntSrvSttPgmFnc = new String[] {""} ;
         P00A823_n1600ContagemResultado_CntSrvSttPgmFnc = new bool[] {false} ;
         P00A823_A1601ContagemResultado_CntSrvIndDvr = new decimal[1] ;
         P00A823_n1601ContagemResultado_CntSrvIndDvr = new bool[] {false} ;
         P00A823_A1613ContagemResultado_CntSrvQtdUntCns = new decimal[1] ;
         P00A823_n1613ContagemResultado_CntSrvQtdUntCns = new bool[] {false} ;
         P00A823_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         P00A823_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         P00A823_A1621ContagemResultado_CntSrvUndCntSgl = new String[] {""} ;
         P00A823_n1621ContagemResultado_CntSrvUndCntSgl = new bool[] {false} ;
         P00A823_A2209ContagemResultado_CntSrvUndCntNome = new String[] {""} ;
         P00A823_n2209ContagemResultado_CntSrvUndCntNome = new bool[] {false} ;
         P00A823_A1623ContagemResultado_CntSrvMmn = new String[] {""} ;
         P00A823_n1623ContagemResultado_CntSrvMmn = new bool[] {false} ;
         P00A823_A1607ContagemResultado_PrzAnl = new short[1] ;
         P00A823_n1607ContagemResultado_PrzAnl = new bool[] {false} ;
         P00A823_A1609ContagemResultado_PrzCrr = new short[1] ;
         P00A823_n1609ContagemResultado_PrzCrr = new bool[] {false} ;
         P00A823_A1618ContagemResultado_PrzRsp = new short[1] ;
         P00A823_n1618ContagemResultado_PrzRsp = new bool[] {false} ;
         P00A823_A1619ContagemResultado_PrzGrt = new short[1] ;
         P00A823_n1619ContagemResultado_PrzGrt = new bool[] {false} ;
         P00A823_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00A823_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00A823_A1603ContagemResultado_CntCod = new int[1] ;
         P00A823_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00A823_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00A823_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00A823_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P00A823_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P00A823_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P00A823_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P00A823_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P00A823_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P00A823_A1615ContagemResultado_CntIndDvr = new decimal[1] ;
         P00A823_n1615ContagemResultado_CntIndDvr = new bool[] {false} ;
         P00A823_A1617ContagemResultado_CntClcDvr = new String[] {""} ;
         P00A823_n1617ContagemResultado_CntClcDvr = new bool[] {false} ;
         P00A823_A1616ContagemResultado_CntVlrUndCnt = new decimal[1] ;
         P00A823_n1616ContagemResultado_CntVlrUndCnt = new bool[] {false} ;
         P00A823_A1632ContagemResultado_CntPrdFtrCada = new String[] {""} ;
         P00A823_n1632ContagemResultado_CntPrdFtrCada = new bool[] {false} ;
         P00A823_A1633ContagemResultado_CntPrdFtrIni = new short[1] ;
         P00A823_n1633ContagemResultado_CntPrdFtrIni = new bool[] {false} ;
         P00A823_A1634ContagemResultado_CntPrdFtrFim = new short[1] ;
         P00A823_n1634ContagemResultado_CntPrdFtrFim = new bool[] {false} ;
         P00A823_A2082ContagemResultado_CntLmtFtr = new decimal[1] ;
         P00A823_n2082ContagemResultado_CntLmtFtr = new bool[] {false} ;
         P00A823_A1624ContagemResultado_DatVgnInc = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1624ContagemResultado_DatVgnInc = new bool[] {false} ;
         P00A823_A1598ContagemResultado_ServicoNome = new String[] {""} ;
         P00A823_n1598ContagemResultado_ServicoNome = new bool[] {false} ;
         P00A823_A1599ContagemResultado_ServicoTpHrrq = new short[1] ;
         P00A823_n1599ContagemResultado_ServicoTpHrrq = new bool[] {false} ;
         P00A823_A1591ContagemResultado_CodSrvVnc = new int[1] ;
         P00A823_n1591ContagemResultado_CodSrvVnc = new bool[] {false} ;
         P00A823_A1590ContagemResultado_SiglaSrvVnc = new String[] {""} ;
         P00A823_n1590ContagemResultado_SiglaSrvVnc = new bool[] {false} ;
         P00A823_A1765ContagemResultado_CodSrvSSVnc = new int[1] ;
         P00A823_n1765ContagemResultado_CodSrvSSVnc = new bool[] {false} ;
         P00A823_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         P00A823_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         P00A823_A1622ContagemResultado_CntVncCod = new int[1] ;
         P00A823_n1622ContagemResultado_CntVncCod = new bool[] {false} ;
         P00A823_A2136ContagemResultado_CntadaOsVinc = new int[1] ;
         P00A823_n2136ContagemResultado_CntadaOsVinc = new bool[] {false} ;
         P00A823_A2137ContagemResultado_SerGrupoVinc = new int[1] ;
         P00A823_n2137ContagemResultado_SerGrupoVinc = new bool[] {false} ;
         P00A823_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00A823_A1584ContagemResultado_UOOwner = new int[1] ;
         P00A823_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         P00A823_A1585ContagemResultado_Referencia = new String[] {""} ;
         P00A823_n1585ContagemResultado_Referencia = new bool[] {false} ;
         P00A823_A1586ContagemResultado_Restricoes = new String[] {""} ;
         P00A823_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         P00A823_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         P00A823_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         P00A823_A1650ContagemResultado_PrzInc = new short[1] ;
         P00A823_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         P00A823_A1714ContagemResultado_Combinada = new bool[] {false} ;
         P00A823_n1714ContagemResultado_Combinada = new bool[] {false} ;
         P00A823_A1762ContagemResultado_Entrega = new short[1] ;
         P00A823_n1762ContagemResultado_Entrega = new bool[] {false} ;
         P00A823_A1791ContagemResultado_SemCusto = new bool[] {false} ;
         P00A823_n1791ContagemResultado_SemCusto = new bool[] {false} ;
         P00A823_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00A823_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00A823_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P00A823_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P00A823_A1903ContagemResultado_DataPrvPgm = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1903ContagemResultado_DataPrvPgm = new bool[] {false} ;
         P00A823_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         P00A823_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         P00A823_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00A823_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00A823_A825ContagemResultado_HoraUltCnt = new String[] {""} ;
         P00A823_A578ContagemResultado_CntComNaoCnf = new int[1] ;
         P00A823_n578ContagemResultado_CntComNaoCnf = new bool[] {false} ;
         P00A823_A584ContagemResultado_ContadorFM = new int[1] ;
         P00A823_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00A823_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00A823_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00A823_A1480ContagemResultado_CstUntUltima = new decimal[1] ;
         P00A823_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00A823_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00A823_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00A823_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00A823_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00A823_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00A823_A1353ContagemResultado_ContratadaDoOwner = new int[1] ;
         P00A823_n1353ContagemResultado_ContratadaDoOwner = new bool[] {false} ;
         P00A823_A1608ContagemResultado_PrzExc = new short[1] ;
         P00A823_n1608ContagemResultado_PrzExc = new bool[] {false} ;
         P00A823_A1610ContagemResultado_PrzTp = new String[] {""} ;
         P00A823_n1610ContagemResultado_PrzTp = new bool[] {false} ;
         P00A823_A1625ContagemResultado_DatIncTA = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1625ContagemResultado_DatIncTA = new bool[] {false} ;
         P00A823_A1602ContagemResultado_GlsIndValor = new decimal[1] ;
         P00A823_n1602ContagemResultado_GlsIndValor = new bool[] {false} ;
         P00A823_A1701ContagemResultado_TemProposta = new bool[] {false} ;
         P00A823_n1701ContagemResultado_TemProposta = new bool[] {false} ;
         P00A823_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         P00A823_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         P00A823_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P00A823_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P00A823_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00A823_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00A823_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00A823_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00A823_A890ContagemResultado_Responsavel = new int[1] ;
         P00A823_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00A823_A508ContagemResultado_Owner = new int[1] ;
         P00A823_A456ContagemResultado_Codigo = new int[1] ;
         P00A823_A457ContagemResultado_Demanda = new String[] {""} ;
         P00A823_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00A823_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00A823_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00A823_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00A823_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00A823_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00A823_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00A823_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00A823_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00A823_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00A823_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00A823_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00A823_A1790ContagemResultado_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1790ContagemResultado_DataInicio = new bool[] {false} ;
         P00A823_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00A823_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00A823_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00A823_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00A823_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00A823_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00A823_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00A823_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00A823_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00A823_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00A823_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00A823_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00A823_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00A823_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00A823_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00A823_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00A823_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00A823_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00A823_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00A823_A1817ContagemResultado_ContratadaCNPJ = new String[] {""} ;
         P00A823_n1817ContagemResultado_ContratadaCNPJ = new bool[] {false} ;
         P00A823_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00A823_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00A823_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         P00A823_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         P00A823_A807ContagemResultado_ContratadaOrigemPesCod = new int[1] ;
         P00A823_n807ContagemResultado_ContratadaOrigemPesCod = new bool[] {false} ;
         P00A823_A808ContagemResultado_ContratadaOrigemPesNom = new String[] {""} ;
         P00A823_n808ContagemResultado_ContratadaOrigemPesNom = new bool[] {false} ;
         P00A823_A867ContagemResultado_ContratadaOrigemAreaCod = new int[1] ;
         P00A823_n867ContagemResultado_ContratadaOrigemAreaCod = new bool[] {false} ;
         P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema = new bool[] {false} ;
         P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema = new bool[] {false} ;
         P00A823_A454ContagemResultado_ContadorFSCod = new int[1] ;
         P00A823_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         P00A823_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         P00A823_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         P00A823_A455ContagemResultado_ContadorFSNom = new String[] {""} ;
         P00A823_n455ContagemResultado_ContadorFSNom = new bool[] {false} ;
         P00A823_A1452ContagemResultado_SS = new int[1] ;
         P00A823_n1452ContagemResultado_SS = new bool[] {false} ;
         P00A823_A1173ContagemResultado_OSManual = new bool[] {false} ;
         P00A823_n1173ContagemResultado_OSManual = new bool[] {false} ;
         P00A823_A494ContagemResultado_Descricao = new String[] {""} ;
         P00A823_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00A823_A465ContagemResultado_Link = new String[] {""} ;
         P00A823_n465ContagemResultado_Link = new bool[] {false} ;
         P00A823_A489ContagemResultado_SistemaCod = new int[1] ;
         P00A823_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00A823_A495ContagemResultado_SistemaNom = new String[] {""} ;
         P00A823_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         P00A823_A496Contagemresultado_SistemaAreaCod = new int[1] ;
         P00A823_n496Contagemresultado_SistemaAreaCod = new bool[] {false} ;
         P00A823_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00A823_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00A823_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00A823_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00A823_A804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         P00A823_n804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         P00A823_A146Modulo_Codigo = new int[1] ;
         P00A823_n146Modulo_Codigo = new bool[] {false} ;
         P00A823_A143Modulo_Nome = new String[] {""} ;
         P00A823_A145Modulo_Sigla = new String[] {""} ;
         P00A823_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00A823_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00A823_A477ContagemResultado_NaoCnfDmnNom = new String[] {""} ;
         P00A823_n477ContagemResultado_NaoCnfDmnNom = new bool[] {false} ;
         P00A823_A2030ContagemResultado_NaoCnfDmnGls = new bool[] {false} ;
         P00A823_n2030ContagemResultado_NaoCnfDmnGls = new bool[] {false} ;
         P00A823_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00A823_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00A823_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00A823_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00A823_A601ContagemResultado_Servico = new int[1] ;
         P00A823_n601ContagemResultado_Servico = new bool[] {false} ;
         P00A823_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P00A823_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P00A823_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00A823_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00A823_A1062ContagemResultado_ServicoTela = new String[] {""} ;
         P00A823_n1062ContagemResultado_ServicoTela = new bool[] {false} ;
         P00A823_A897ContagemResultado_ServicoAtivo = new bool[] {false} ;
         P00A823_n897ContagemResultado_ServicoAtivo = new bool[] {false} ;
         P00A823_A1398ContagemResultado_ServicoNaoRqrAtr = new bool[] {false} ;
         P00A823_n1398ContagemResultado_ServicoNaoRqrAtr = new bool[] {false} ;
         P00A823_A1636ContagemResultado_ServicoSS = new int[1] ;
         P00A823_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P00A823_A1637ContagemResultado_ServicoSSSgl = new String[] {""} ;
         P00A823_n1637ContagemResultado_ServicoSSSgl = new bool[] {false} ;
         P00A823_A2050ContagemResultado_SrvLnhNegCod = new int[1] ;
         P00A823_n2050ContagemResultado_SrvLnhNegCod = new bool[] {false} ;
         P00A823_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00A823_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         P00A823_A514ContagemResultado_Observacao = new String[] {""} ;
         P00A823_n514ContagemResultado_Observacao = new bool[] {false} ;
         P00A823_A592ContagemResultado_Evidencia = new String[] {""} ;
         P00A823_n592ContagemResultado_Evidencia = new bool[] {false} ;
         P00A823_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         P00A823_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         P00A823_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         P00A823_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         P00A823_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         P00A823_A1180ContagemResultado_Custo = new decimal[1] ;
         P00A823_n1180ContagemResultado_Custo = new bool[] {false} ;
         P00A823_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00A823_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00A823_A528ContagemResultado_LoteAceite = new String[] {""} ;
         P00A823_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         P00A823_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         P00A823_n529ContagemResultado_DataAceite = new bool[] {false} ;
         P00A823_A525ContagemResultado_AceiteUserCod = new int[1] ;
         P00A823_n525ContagemResultado_AceiteUserCod = new bool[] {false} ;
         P00A823_A526ContagemResultado_AceitePessoaCod = new int[1] ;
         P00A823_n526ContagemResultado_AceitePessoaCod = new bool[] {false} ;
         P00A823_A527ContagemResultado_AceiteUserNom = new String[] {""} ;
         P00A823_n527ContagemResultado_AceiteUserNom = new bool[] {false} ;
         P00A823_A1547ContagemResultado_LoteNFe = new int[1] ;
         P00A823_n1547ContagemResultado_LoteNFe = new bool[] {false} ;
         P00A823_A1559ContagemResultado_VlrAceite = new decimal[1] ;
         P00A823_n1559ContagemResultado_VlrAceite = new bool[] {false} ;
         P00A823_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00A823_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00A823_A602ContagemResultado_OSVinculada = new int[1] ;
         P00A823_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00A823_A603ContagemResultado_DmnVinculada = new String[] {""} ;
         P00A823_n603ContagemResultado_DmnVinculada = new bool[] {false} ;
         P00A823_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P00A823_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P00A823_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         P00A823_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         P00A823_A1178ContagemResultado_PBFinal = new decimal[1] ;
         P00A823_n1178ContagemResultado_PBFinal = new bool[] {false} ;
         P00A823_A1179ContagemResultado_PLFinal = new decimal[1] ;
         P00A823_n1179ContagemResultado_PLFinal = new bool[] {false} ;
         P00A823_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         P00A823_A1044ContagemResultado_FncUsrCod = new int[1] ;
         P00A823_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         P00A823_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00A823_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00A823_A1049ContagemResultado_GlsData = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1049ContagemResultado_GlsData = new bool[] {false} ;
         P00A823_A1050ContagemResultado_GlsDescricao = new String[] {""} ;
         P00A823_n1050ContagemResultado_GlsDescricao = new bool[] {false} ;
         P00A823_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00A823_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00A823_A1052ContagemResultado_GlsUser = new int[1] ;
         P00A823_n1052ContagemResultado_GlsUser = new bool[] {false} ;
         P00A823_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         P00A823_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         P00A823_A1390ContagemResultado_RdmnProjectId = new int[1] ;
         P00A823_n1390ContagemResultado_RdmnProjectId = new bool[] {false} ;
         P00A823_A1392ContagemResultado_RdmnUpdated = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1392ContagemResultado_RdmnUpdated = new bool[] {false} ;
         P00A823_A1444ContagemResultado_CntSrvPrrPrz = new decimal[1] ;
         P00A823_n1444ContagemResultado_CntSrvPrrPrz = new bool[] {false} ;
         P00A823_A1445ContagemResultado_CntSrvPrrCst = new decimal[1] ;
         P00A823_n1445ContagemResultado_CntSrvPrrCst = new bool[] {false} ;
         P00A823_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00A823_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00A823_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00A823_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00A823_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00A823_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00A823_A1506ContagemResultado_TmpEstCrr = new int[1] ;
         P00A823_n1506ContagemResultado_TmpEstCrr = new bool[] {false} ;
         P00A823_A1520ContagemResultado_InicioAnl = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1520ContagemResultado_InicioAnl = new bool[] {false} ;
         P00A823_A1521ContagemResultado_FimAnl = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1521ContagemResultado_FimAnl = new bool[] {false} ;
         P00A823_A1509ContagemResultado_InicioExc = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1509ContagemResultado_InicioExc = new bool[] {false} ;
         P00A823_A1510ContagemResultado_FimExc = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1510ContagemResultado_FimExc = new bool[] {false} ;
         P00A823_A1511ContagemResultado_InicioCrr = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1511ContagemResultado_InicioCrr = new bool[] {false} ;
         P00A823_A1512ContagemResultado_FimCrr = new DateTime[] {DateTime.MinValue} ;
         P00A823_n1512ContagemResultado_FimCrr = new bool[] {false} ;
         P00A823_A1515ContagemResultado_Evento = new short[1] ;
         P00A823_n1515ContagemResultado_Evento = new bool[] {false} ;
         P00A823_A1544ContagemResultado_ProjetoCod = new int[1] ;
         P00A823_n1544ContagemResultado_ProjetoCod = new bool[] {false} ;
         P00A823_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P00A823_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P00A823_A1594ContagemResultado_CntSrvFtrm = new String[] {""} ;
         P00A823_n1594ContagemResultado_CntSrvFtrm = new bool[] {false} ;
         P00A823_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P00A823_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P00A823_A1597ContagemResultado_CntSrvVlrUndCnt = new decimal[1] ;
         P00A823_n1597ContagemResultado_CntSrvVlrUndCnt = new bool[] {false} ;
         P00A825_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00A825_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00A825_A825ContagemResultado_HoraUltCnt = new String[] {""} ;
         P00A825_A584ContagemResultado_ContadorFM = new int[1] ;
         P00A825_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00A825_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00A825_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00A825_A1480ContagemResultado_CstUntUltima = new decimal[1] ;
         P00A825_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00A825_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         GXt_char2 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontagemresultado__default(),
            new Object[][] {
                new Object[] {
               P00A812_A903ContratoServicosPrazo_CntSrvCod, P00A812_A1600ContagemResultado_CntSrvSttPgmFnc, P00A812_n1600ContagemResultado_CntSrvSttPgmFnc, P00A812_A1601ContagemResultado_CntSrvIndDvr, P00A812_n1601ContagemResultado_CntSrvIndDvr, P00A812_A1613ContagemResultado_CntSrvQtdUntCns, P00A812_n1613ContagemResultado_CntSrvQtdUntCns, P00A812_A1620ContagemResultado_CntSrvUndCnt, P00A812_n1620ContagemResultado_CntSrvUndCnt, P00A812_A1621ContagemResultado_CntSrvUndCntSgl,
               P00A812_n1621ContagemResultado_CntSrvUndCntSgl, P00A812_A2209ContagemResultado_CntSrvUndCntNome, P00A812_n2209ContagemResultado_CntSrvUndCntNome, P00A812_A1623ContagemResultado_CntSrvMmn, P00A812_n1623ContagemResultado_CntSrvMmn, P00A812_A1607ContagemResultado_PrzAnl, P00A812_n1607ContagemResultado_PrzAnl, P00A812_A1609ContagemResultado_PrzCrr, P00A812_n1609ContagemResultado_PrzCrr, P00A812_A1618ContagemResultado_PrzRsp,
               P00A812_n1618ContagemResultado_PrzRsp, P00A812_A1619ContagemResultado_PrzGrt, P00A812_n1619ContagemResultado_PrzGrt, P00A812_A1611ContagemResultado_PrzTpDias, P00A812_n1611ContagemResultado_PrzTpDias, P00A812_A1603ContagemResultado_CntCod, P00A812_n1603ContagemResultado_CntCod, P00A812_A1612ContagemResultado_CntNum, P00A812_n1612ContagemResultado_CntNum, P00A812_A1604ContagemResultado_CntPrpCod,
               P00A812_n1604ContagemResultado_CntPrpCod, P00A812_A1605ContagemResultado_CntPrpPesCod, P00A812_n1605ContagemResultado_CntPrpPesCod, P00A812_A1606ContagemResultado_CntPrpPesNom, P00A812_n1606ContagemResultado_CntPrpPesNom, P00A812_A1615ContagemResultado_CntIndDvr, P00A812_n1615ContagemResultado_CntIndDvr, P00A812_A1617ContagemResultado_CntClcDvr, P00A812_n1617ContagemResultado_CntClcDvr, P00A812_A1616ContagemResultado_CntVlrUndCnt,
               P00A812_n1616ContagemResultado_CntVlrUndCnt, P00A812_A1632ContagemResultado_CntPrdFtrCada, P00A812_n1632ContagemResultado_CntPrdFtrCada, P00A812_A1633ContagemResultado_CntPrdFtrIni, P00A812_n1633ContagemResultado_CntPrdFtrIni, P00A812_A1634ContagemResultado_CntPrdFtrFim, P00A812_n1634ContagemResultado_CntPrdFtrFim, P00A812_A2082ContagemResultado_CntLmtFtr, P00A812_n2082ContagemResultado_CntLmtFtr, P00A812_A1624ContagemResultado_DatVgnInc,
               P00A812_n1624ContagemResultado_DatVgnInc, P00A812_A1598ContagemResultado_ServicoNome, P00A812_n1598ContagemResultado_ServicoNome, P00A812_A1599ContagemResultado_ServicoTpHrrq, P00A812_n1599ContagemResultado_ServicoTpHrrq, P00A812_A1591ContagemResultado_CodSrvVnc, P00A812_n1591ContagemResultado_CodSrvVnc, P00A812_A1590ContagemResultado_SiglaSrvVnc, P00A812_n1590ContagemResultado_SiglaSrvVnc, P00A812_A1765ContagemResultado_CodSrvSSVnc,
               P00A812_n1765ContagemResultado_CodSrvSSVnc, P00A812_A1627ContagemResultado_CntSrvVncCod, P00A812_n1627ContagemResultado_CntSrvVncCod, P00A812_A1622ContagemResultado_CntVncCod, P00A812_n1622ContagemResultado_CntVncCod, P00A812_A2136ContagemResultado_CntadaOsVinc, P00A812_n2136ContagemResultado_CntadaOsVinc, P00A812_A2137ContagemResultado_SerGrupoVinc, P00A812_n2137ContagemResultado_SerGrupoVinc, P00A812_A1583ContagemResultado_TipoRegistro,
               P00A812_A1584ContagemResultado_UOOwner, P00A812_n1584ContagemResultado_UOOwner, P00A812_A1585ContagemResultado_Referencia, P00A812_n1585ContagemResultado_Referencia, P00A812_A1586ContagemResultado_Restricoes, P00A812_n1586ContagemResultado_Restricoes, P00A812_A1587ContagemResultado_PrioridadePrevista, P00A812_n1587ContagemResultado_PrioridadePrevista, P00A812_A1650ContagemResultado_PrzInc, P00A812_n1650ContagemResultado_PrzInc,
               P00A812_A1714ContagemResultado_Combinada, P00A812_n1714ContagemResultado_Combinada, P00A812_A1762ContagemResultado_Entrega, P00A812_n1762ContagemResultado_Entrega, P00A812_A1791ContagemResultado_SemCusto, P00A812_n1791ContagemResultado_SemCusto, P00A812_A1854ContagemResultado_VlrCnc, P00A812_n1854ContagemResultado_VlrCnc, P00A812_A1855ContagemResultado_PFCnc, P00A812_n1855ContagemResultado_PFCnc,
               P00A812_A1903ContagemResultado_DataPrvPgm, P00A812_n1903ContagemResultado_DataPrvPgm, P00A812_A2133ContagemResultado_QuantidadeSolicitada, P00A812_n2133ContagemResultado_QuantidadeSolicitada, P00A812_A531ContagemResultado_StatusUltCnt, P00A812_A566ContagemResultado_DataUltCnt, P00A812_A825ContagemResultado_HoraUltCnt, P00A812_A578ContagemResultado_CntComNaoCnf, P00A812_n578ContagemResultado_CntComNaoCnf, P00A812_A584ContagemResultado_ContadorFM,
               P00A812_A682ContagemResultado_PFBFMUltima, P00A812_A683ContagemResultado_PFLFMUltima, P00A812_A684ContagemResultado_PFBFSUltima, P00A812_A1480ContagemResultado_CstUntUltima, P00A812_A685ContagemResultado_PFLFSUltima, P00A812_A802ContagemResultado_DeflatorCnt, P00A812_A510ContagemResultado_EsforcoSoma, P00A812_n510ContagemResultado_EsforcoSoma, P00A812_A2118ContagemResultado_Owner_Identificao, P00A812_n2118ContagemResultado_Owner_Identificao,
               P00A812_A1353ContagemResultado_ContratadaDoOwner, P00A812_n1353ContagemResultado_ContratadaDoOwner, P00A812_A1608ContagemResultado_PrzExc, P00A812_n1608ContagemResultado_PrzExc, P00A812_A1610ContagemResultado_PrzTp, P00A812_n1610ContagemResultado_PrzTp, P00A812_A1625ContagemResultado_DatIncTA, P00A812_n1625ContagemResultado_DatIncTA, P00A812_A1602ContagemResultado_GlsIndValor, P00A812_n1602ContagemResultado_GlsIndValor,
               P00A812_A1701ContagemResultado_TemProposta, P00A812_n1701ContagemResultado_TemProposta, P00A812_A1443ContagemResultado_CntSrvPrrCod, P00A812_n1443ContagemResultado_CntSrvPrrCod, P00A812_A1043ContagemResultado_LiqLogCod, P00A812_n1043ContagemResultado_LiqLogCod, P00A812_A512ContagemResultado_ValorPF, P00A812_n512ContagemResultado_ValorPF, P00A812_A52Contratada_AreaTrabalhoCod, P00A812_n52Contratada_AreaTrabalhoCod,
               P00A812_A890ContagemResultado_Responsavel, P00A812_n890ContagemResultado_Responsavel, P00A812_A508ContagemResultado_Owner, P00A812_A456ContagemResultado_Codigo, P00A812_A457ContagemResultado_Demanda, P00A812_n457ContagemResultado_Demanda, P00A812_A803ContagemResultado_ContratadaSigla, P00A812_n803ContagemResultado_ContratadaSigla, P00A812_A801ContagemResultado_ServicoSigla, P00A812_n801ContagemResultado_ServicoSigla,
               P00A812_A493ContagemResultado_DemandaFM, P00A812_n493ContagemResultado_DemandaFM, P00A812_A490ContagemResultado_ContratadaCod, P00A812_n490ContagemResultado_ContratadaCod, P00A812_A1553ContagemResultado_CntSrvCod, P00A812_n1553ContagemResultado_CntSrvCod, P00A812_A471ContagemResultado_DataDmn, P00A812_A1790ContagemResultado_DataInicio, P00A812_n1790ContagemResultado_DataInicio, P00A812_A472ContagemResultado_DataEntrega,
               P00A812_n472ContagemResultado_DataEntrega, P00A812_A912ContagemResultado_HoraEntrega, P00A812_n912ContagemResultado_HoraEntrega, P00A812_A1351ContagemResultado_DataPrevista, P00A812_n1351ContagemResultado_DataPrevista, P00A812_A1349ContagemResultado_DataExecucao, P00A812_n1349ContagemResultado_DataExecucao, P00A812_A2017ContagemResultado_DataEntregaReal, P00A812_n2017ContagemResultado_DataEntregaReal, P00A812_A1348ContagemResultado_DataHomologacao,
               P00A812_n1348ContagemResultado_DataHomologacao, P00A812_A1227ContagemResultado_PrazoInicialDias, P00A812_n1227ContagemResultado_PrazoInicialDias, P00A812_A1237ContagemResultado_PrazoMaisDias, P00A812_n1237ContagemResultado_PrazoMaisDias, P00A812_A499ContagemResultado_ContratadaPessoaCod, P00A812_n499ContagemResultado_ContratadaPessoaCod, P00A812_A500ContagemResultado_ContratadaPessoaNom, P00A812_n500ContagemResultado_ContratadaPessoaNom, P00A812_A1326ContagemResultado_ContratadaTipoFab,
               P00A812_n1326ContagemResultado_ContratadaTipoFab, P00A812_A1817ContagemResultado_ContratadaCNPJ, P00A812_n1817ContagemResultado_ContratadaCNPJ, P00A812_A805ContagemResultado_ContratadaOrigemCod, P00A812_n805ContagemResultado_ContratadaOrigemCod, P00A812_A866ContagemResultado_ContratadaOrigemSigla, P00A812_n866ContagemResultado_ContratadaOrigemSigla, P00A812_A807ContagemResultado_ContratadaOrigemPesCod, P00A812_n807ContagemResultado_ContratadaOrigemPesCod, P00A812_A808ContagemResultado_ContratadaOrigemPesNom,
               P00A812_n808ContagemResultado_ContratadaOrigemPesNom, P00A812_A867ContagemResultado_ContratadaOrigemAreaCod, P00A812_n867ContagemResultado_ContratadaOrigemAreaCod, P00A812_A2093ContagemResultado_ContratadaOrigemUsaSistema, P00A812_n2093ContagemResultado_ContratadaOrigemUsaSistema, P00A812_A454ContagemResultado_ContadorFSCod, P00A812_n454ContagemResultado_ContadorFSCod, P00A812_A480ContagemResultado_CrFSPessoaCod, P00A812_n480ContagemResultado_CrFSPessoaCod, P00A812_A455ContagemResultado_ContadorFSNom,
               P00A812_n455ContagemResultado_ContadorFSNom, P00A812_A1452ContagemResultado_SS, P00A812_n1452ContagemResultado_SS, P00A812_A1173ContagemResultado_OSManual, P00A812_n1173ContagemResultado_OSManual, P00A812_A494ContagemResultado_Descricao, P00A812_n494ContagemResultado_Descricao, P00A812_A465ContagemResultado_Link, P00A812_n465ContagemResultado_Link, P00A812_A489ContagemResultado_SistemaCod,
               P00A812_n489ContagemResultado_SistemaCod, P00A812_A495ContagemResultado_SistemaNom, P00A812_n495ContagemResultado_SistemaNom, P00A812_A496Contagemresultado_SistemaAreaCod, P00A812_n496Contagemresultado_SistemaAreaCod, P00A812_A509ContagemrResultado_SistemaSigla, P00A812_n509ContagemrResultado_SistemaSigla, P00A812_A515ContagemResultado_SistemaCoord, P00A812_n515ContagemResultado_SistemaCoord, P00A812_A804ContagemResultado_SistemaAtivo,
               P00A812_n804ContagemResultado_SistemaAtivo, P00A812_A146Modulo_Codigo, P00A812_n146Modulo_Codigo, P00A812_A143Modulo_Nome, P00A812_A145Modulo_Sigla, P00A812_A468ContagemResultado_NaoCnfDmnCod, P00A812_n468ContagemResultado_NaoCnfDmnCod, P00A812_A477ContagemResultado_NaoCnfDmnNom, P00A812_n477ContagemResultado_NaoCnfDmnNom, P00A812_A2030ContagemResultado_NaoCnfDmnGls,
               P00A812_n2030ContagemResultado_NaoCnfDmnGls, P00A812_A484ContagemResultado_StatusDmn, P00A812_n484ContagemResultado_StatusDmn, P00A812_A771ContagemResultado_StatusDmnVnc, P00A812_n771ContagemResultado_StatusDmnVnc, P00A812_A601ContagemResultado_Servico, P00A812_n601ContagemResultado_Servico, P00A812_A2008ContagemResultado_CntSrvAls, P00A812_n2008ContagemResultado_CntSrvAls, P00A812_A764ContagemResultado_ServicoGrupo,
               P00A812_n764ContagemResultado_ServicoGrupo, P00A812_A1062ContagemResultado_ServicoTela, P00A812_n1062ContagemResultado_ServicoTela, P00A812_A897ContagemResultado_ServicoAtivo, P00A812_n897ContagemResultado_ServicoAtivo, P00A812_A1398ContagemResultado_ServicoNaoRqrAtr, P00A812_n1398ContagemResultado_ServicoNaoRqrAtr, P00A812_A1636ContagemResultado_ServicoSS, P00A812_n1636ContagemResultado_ServicoSS, P00A812_A1637ContagemResultado_ServicoSSSgl,
               P00A812_n1637ContagemResultado_ServicoSSSgl, P00A812_A2050ContagemResultado_SrvLnhNegCod, P00A812_n2050ContagemResultado_SrvLnhNegCod, P00A812_A485ContagemResultado_EhValidacao, P00A812_n485ContagemResultado_EhValidacao, P00A812_A514ContagemResultado_Observacao, P00A812_n514ContagemResultado_Observacao, P00A812_A592ContagemResultado_Evidencia, P00A812_n592ContagemResultado_Evidencia, P00A812_A1350ContagemResultado_DataCadastro,
               P00A812_n1350ContagemResultado_DataCadastro, P00A812_A1312ContagemResultado_ResponsavelPessCod, P00A812_n1312ContagemResultado_ResponsavelPessCod, P00A812_A1313ContagemResultado_ResponsavelPessNome, P00A812_n1313ContagemResultado_ResponsavelPessNome, P00A812_A1180ContagemResultado_Custo, P00A812_n1180ContagemResultado_Custo, P00A812_A597ContagemResultado_LoteAceiteCod, P00A812_n597ContagemResultado_LoteAceiteCod, P00A812_A528ContagemResultado_LoteAceite,
               P00A812_n528ContagemResultado_LoteAceite, P00A812_A529ContagemResultado_DataAceite, P00A812_n529ContagemResultado_DataAceite, P00A812_A525ContagemResultado_AceiteUserCod, P00A812_n525ContagemResultado_AceiteUserCod, P00A812_A526ContagemResultado_AceitePessoaCod, P00A812_n526ContagemResultado_AceitePessoaCod, P00A812_A527ContagemResultado_AceiteUserNom, P00A812_n527ContagemResultado_AceiteUserNom, P00A812_A1547ContagemResultado_LoteNFe,
               P00A812_n1547ContagemResultado_LoteNFe, P00A812_A1559ContagemResultado_VlrAceite, P00A812_n1559ContagemResultado_VlrAceite, P00A812_A598ContagemResultado_Baseline, P00A812_n598ContagemResultado_Baseline, P00A812_A602ContagemResultado_OSVinculada, P00A812_n602ContagemResultado_OSVinculada, P00A812_A603ContagemResultado_DmnVinculada, P00A812_n603ContagemResultado_DmnVinculada, P00A812_A798ContagemResultado_PFBFSImp,
               P00A812_n798ContagemResultado_PFBFSImp, P00A812_A799ContagemResultado_PFLFSImp, P00A812_n799ContagemResultado_PFLFSImp, P00A812_A1178ContagemResultado_PBFinal, P00A812_n1178ContagemResultado_PBFinal, P00A812_A1179ContagemResultado_PLFinal, P00A812_n1179ContagemResultado_PLFinal, P00A812_A1034ContagemResultadoLiqLog_Data, P00A812_n1034ContagemResultadoLiqLog_Data, P00A812_A1044ContagemResultado_FncUsrCod,
               P00A812_n1044ContagemResultado_FncUsrCod, P00A812_A1046ContagemResultado_Agrupador, P00A812_n1046ContagemResultado_Agrupador, P00A812_A1049ContagemResultado_GlsData, P00A812_n1049ContagemResultado_GlsData, P00A812_A1050ContagemResultado_GlsDescricao, P00A812_n1050ContagemResultado_GlsDescricao, P00A812_A1051ContagemResultado_GlsValor, P00A812_n1051ContagemResultado_GlsValor, P00A812_A1052ContagemResultado_GlsUser,
               P00A812_n1052ContagemResultado_GlsUser, P00A812_A1389ContagemResultado_RdmnIssueId, P00A812_n1389ContagemResultado_RdmnIssueId, P00A812_A1390ContagemResultado_RdmnProjectId, P00A812_n1390ContagemResultado_RdmnProjectId, P00A812_A1392ContagemResultado_RdmnUpdated, P00A812_n1392ContagemResultado_RdmnUpdated, P00A812_A1444ContagemResultado_CntSrvPrrPrz, P00A812_n1444ContagemResultado_CntSrvPrrPrz, P00A812_A1445ContagemResultado_CntSrvPrrCst,
               P00A812_n1445ContagemResultado_CntSrvPrrCst, P00A812_A1457ContagemResultado_TemDpnHmlg, P00A812_n1457ContagemResultado_TemDpnHmlg, P00A812_A1519ContagemResultado_TmpEstAnl, P00A812_n1519ContagemResultado_TmpEstAnl, P00A812_A1505ContagemResultado_TmpEstExc, P00A812_n1505ContagemResultado_TmpEstExc, P00A812_A1506ContagemResultado_TmpEstCrr, P00A812_n1506ContagemResultado_TmpEstCrr, P00A812_A1520ContagemResultado_InicioAnl,
               P00A812_n1520ContagemResultado_InicioAnl, P00A812_A1521ContagemResultado_FimAnl, P00A812_n1521ContagemResultado_FimAnl, P00A812_A1509ContagemResultado_InicioExc, P00A812_n1509ContagemResultado_InicioExc, P00A812_A1510ContagemResultado_FimExc, P00A812_n1510ContagemResultado_FimExc, P00A812_A1511ContagemResultado_InicioCrr, P00A812_n1511ContagemResultado_InicioCrr, P00A812_A1512ContagemResultado_FimCrr,
               P00A812_n1512ContagemResultado_FimCrr, P00A812_A1515ContagemResultado_Evento, P00A812_n1515ContagemResultado_Evento, P00A812_A1544ContagemResultado_ProjetoCod, P00A812_n1544ContagemResultado_ProjetoCod, P00A812_A1593ContagemResultado_CntSrvTpVnc, P00A812_n1593ContagemResultado_CntSrvTpVnc, P00A812_A1594ContagemResultado_CntSrvFtrm, P00A812_n1594ContagemResultado_CntSrvFtrm, P00A812_A1596ContagemResultado_CntSrvPrc,
               P00A812_n1596ContagemResultado_CntSrvPrc, P00A812_A1597ContagemResultado_CntSrvVlrUndCnt, P00A812_n1597ContagemResultado_CntSrvVlrUndCnt
               }
               , new Object[] {
               P00A823_A903ContratoServicosPrazo_CntSrvCod, P00A823_A1600ContagemResultado_CntSrvSttPgmFnc, P00A823_n1600ContagemResultado_CntSrvSttPgmFnc, P00A823_A1601ContagemResultado_CntSrvIndDvr, P00A823_n1601ContagemResultado_CntSrvIndDvr, P00A823_A1613ContagemResultado_CntSrvQtdUntCns, P00A823_n1613ContagemResultado_CntSrvQtdUntCns, P00A823_A1620ContagemResultado_CntSrvUndCnt, P00A823_n1620ContagemResultado_CntSrvUndCnt, P00A823_A1621ContagemResultado_CntSrvUndCntSgl,
               P00A823_n1621ContagemResultado_CntSrvUndCntSgl, P00A823_A2209ContagemResultado_CntSrvUndCntNome, P00A823_n2209ContagemResultado_CntSrvUndCntNome, P00A823_A1623ContagemResultado_CntSrvMmn, P00A823_n1623ContagemResultado_CntSrvMmn, P00A823_A1607ContagemResultado_PrzAnl, P00A823_n1607ContagemResultado_PrzAnl, P00A823_A1609ContagemResultado_PrzCrr, P00A823_n1609ContagemResultado_PrzCrr, P00A823_A1618ContagemResultado_PrzRsp,
               P00A823_n1618ContagemResultado_PrzRsp, P00A823_A1619ContagemResultado_PrzGrt, P00A823_n1619ContagemResultado_PrzGrt, P00A823_A1611ContagemResultado_PrzTpDias, P00A823_n1611ContagemResultado_PrzTpDias, P00A823_A1603ContagemResultado_CntCod, P00A823_n1603ContagemResultado_CntCod, P00A823_A1612ContagemResultado_CntNum, P00A823_n1612ContagemResultado_CntNum, P00A823_A1604ContagemResultado_CntPrpCod,
               P00A823_n1604ContagemResultado_CntPrpCod, P00A823_A1605ContagemResultado_CntPrpPesCod, P00A823_n1605ContagemResultado_CntPrpPesCod, P00A823_A1606ContagemResultado_CntPrpPesNom, P00A823_n1606ContagemResultado_CntPrpPesNom, P00A823_A1615ContagemResultado_CntIndDvr, P00A823_n1615ContagemResultado_CntIndDvr, P00A823_A1617ContagemResultado_CntClcDvr, P00A823_n1617ContagemResultado_CntClcDvr, P00A823_A1616ContagemResultado_CntVlrUndCnt,
               P00A823_n1616ContagemResultado_CntVlrUndCnt, P00A823_A1632ContagemResultado_CntPrdFtrCada, P00A823_n1632ContagemResultado_CntPrdFtrCada, P00A823_A1633ContagemResultado_CntPrdFtrIni, P00A823_n1633ContagemResultado_CntPrdFtrIni, P00A823_A1634ContagemResultado_CntPrdFtrFim, P00A823_n1634ContagemResultado_CntPrdFtrFim, P00A823_A2082ContagemResultado_CntLmtFtr, P00A823_n2082ContagemResultado_CntLmtFtr, P00A823_A1624ContagemResultado_DatVgnInc,
               P00A823_n1624ContagemResultado_DatVgnInc, P00A823_A1598ContagemResultado_ServicoNome, P00A823_n1598ContagemResultado_ServicoNome, P00A823_A1599ContagemResultado_ServicoTpHrrq, P00A823_n1599ContagemResultado_ServicoTpHrrq, P00A823_A1591ContagemResultado_CodSrvVnc, P00A823_n1591ContagemResultado_CodSrvVnc, P00A823_A1590ContagemResultado_SiglaSrvVnc, P00A823_n1590ContagemResultado_SiglaSrvVnc, P00A823_A1765ContagemResultado_CodSrvSSVnc,
               P00A823_n1765ContagemResultado_CodSrvSSVnc, P00A823_A1627ContagemResultado_CntSrvVncCod, P00A823_n1627ContagemResultado_CntSrvVncCod, P00A823_A1622ContagemResultado_CntVncCod, P00A823_n1622ContagemResultado_CntVncCod, P00A823_A2136ContagemResultado_CntadaOsVinc, P00A823_n2136ContagemResultado_CntadaOsVinc, P00A823_A2137ContagemResultado_SerGrupoVinc, P00A823_n2137ContagemResultado_SerGrupoVinc, P00A823_A1583ContagemResultado_TipoRegistro,
               P00A823_A1584ContagemResultado_UOOwner, P00A823_n1584ContagemResultado_UOOwner, P00A823_A1585ContagemResultado_Referencia, P00A823_n1585ContagemResultado_Referencia, P00A823_A1586ContagemResultado_Restricoes, P00A823_n1586ContagemResultado_Restricoes, P00A823_A1587ContagemResultado_PrioridadePrevista, P00A823_n1587ContagemResultado_PrioridadePrevista, P00A823_A1650ContagemResultado_PrzInc, P00A823_n1650ContagemResultado_PrzInc,
               P00A823_A1714ContagemResultado_Combinada, P00A823_n1714ContagemResultado_Combinada, P00A823_A1762ContagemResultado_Entrega, P00A823_n1762ContagemResultado_Entrega, P00A823_A1791ContagemResultado_SemCusto, P00A823_n1791ContagemResultado_SemCusto, P00A823_A1854ContagemResultado_VlrCnc, P00A823_n1854ContagemResultado_VlrCnc, P00A823_A1855ContagemResultado_PFCnc, P00A823_n1855ContagemResultado_PFCnc,
               P00A823_A1903ContagemResultado_DataPrvPgm, P00A823_n1903ContagemResultado_DataPrvPgm, P00A823_A2133ContagemResultado_QuantidadeSolicitada, P00A823_n2133ContagemResultado_QuantidadeSolicitada, P00A823_A531ContagemResultado_StatusUltCnt, P00A823_A566ContagemResultado_DataUltCnt, P00A823_A825ContagemResultado_HoraUltCnt, P00A823_A578ContagemResultado_CntComNaoCnf, P00A823_n578ContagemResultado_CntComNaoCnf, P00A823_A584ContagemResultado_ContadorFM,
               P00A823_A682ContagemResultado_PFBFMUltima, P00A823_A683ContagemResultado_PFLFMUltima, P00A823_A684ContagemResultado_PFBFSUltima, P00A823_A1480ContagemResultado_CstUntUltima, P00A823_A685ContagemResultado_PFLFSUltima, P00A823_A802ContagemResultado_DeflatorCnt, P00A823_A510ContagemResultado_EsforcoSoma, P00A823_n510ContagemResultado_EsforcoSoma, P00A823_A2118ContagemResultado_Owner_Identificao, P00A823_n2118ContagemResultado_Owner_Identificao,
               P00A823_A1353ContagemResultado_ContratadaDoOwner, P00A823_n1353ContagemResultado_ContratadaDoOwner, P00A823_A1608ContagemResultado_PrzExc, P00A823_n1608ContagemResultado_PrzExc, P00A823_A1610ContagemResultado_PrzTp, P00A823_n1610ContagemResultado_PrzTp, P00A823_A1625ContagemResultado_DatIncTA, P00A823_n1625ContagemResultado_DatIncTA, P00A823_A1602ContagemResultado_GlsIndValor, P00A823_n1602ContagemResultado_GlsIndValor,
               P00A823_A1701ContagemResultado_TemProposta, P00A823_n1701ContagemResultado_TemProposta, P00A823_A1443ContagemResultado_CntSrvPrrCod, P00A823_n1443ContagemResultado_CntSrvPrrCod, P00A823_A1043ContagemResultado_LiqLogCod, P00A823_n1043ContagemResultado_LiqLogCod, P00A823_A512ContagemResultado_ValorPF, P00A823_n512ContagemResultado_ValorPF, P00A823_A52Contratada_AreaTrabalhoCod, P00A823_n52Contratada_AreaTrabalhoCod,
               P00A823_A890ContagemResultado_Responsavel, P00A823_n890ContagemResultado_Responsavel, P00A823_A508ContagemResultado_Owner, P00A823_A456ContagemResultado_Codigo, P00A823_A457ContagemResultado_Demanda, P00A823_n457ContagemResultado_Demanda, P00A823_A803ContagemResultado_ContratadaSigla, P00A823_n803ContagemResultado_ContratadaSigla, P00A823_A801ContagemResultado_ServicoSigla, P00A823_n801ContagemResultado_ServicoSigla,
               P00A823_A493ContagemResultado_DemandaFM, P00A823_n493ContagemResultado_DemandaFM, P00A823_A490ContagemResultado_ContratadaCod, P00A823_n490ContagemResultado_ContratadaCod, P00A823_A1553ContagemResultado_CntSrvCod, P00A823_n1553ContagemResultado_CntSrvCod, P00A823_A471ContagemResultado_DataDmn, P00A823_A1790ContagemResultado_DataInicio, P00A823_n1790ContagemResultado_DataInicio, P00A823_A472ContagemResultado_DataEntrega,
               P00A823_n472ContagemResultado_DataEntrega, P00A823_A912ContagemResultado_HoraEntrega, P00A823_n912ContagemResultado_HoraEntrega, P00A823_A1351ContagemResultado_DataPrevista, P00A823_n1351ContagemResultado_DataPrevista, P00A823_A1349ContagemResultado_DataExecucao, P00A823_n1349ContagemResultado_DataExecucao, P00A823_A2017ContagemResultado_DataEntregaReal, P00A823_n2017ContagemResultado_DataEntregaReal, P00A823_A1348ContagemResultado_DataHomologacao,
               P00A823_n1348ContagemResultado_DataHomologacao, P00A823_A1227ContagemResultado_PrazoInicialDias, P00A823_n1227ContagemResultado_PrazoInicialDias, P00A823_A1237ContagemResultado_PrazoMaisDias, P00A823_n1237ContagemResultado_PrazoMaisDias, P00A823_A499ContagemResultado_ContratadaPessoaCod, P00A823_n499ContagemResultado_ContratadaPessoaCod, P00A823_A500ContagemResultado_ContratadaPessoaNom, P00A823_n500ContagemResultado_ContratadaPessoaNom, P00A823_A1326ContagemResultado_ContratadaTipoFab,
               P00A823_n1326ContagemResultado_ContratadaTipoFab, P00A823_A1817ContagemResultado_ContratadaCNPJ, P00A823_n1817ContagemResultado_ContratadaCNPJ, P00A823_A805ContagemResultado_ContratadaOrigemCod, P00A823_n805ContagemResultado_ContratadaOrigemCod, P00A823_A866ContagemResultado_ContratadaOrigemSigla, P00A823_n866ContagemResultado_ContratadaOrigemSigla, P00A823_A807ContagemResultado_ContratadaOrigemPesCod, P00A823_n807ContagemResultado_ContratadaOrigemPesCod, P00A823_A808ContagemResultado_ContratadaOrigemPesNom,
               P00A823_n808ContagemResultado_ContratadaOrigemPesNom, P00A823_A867ContagemResultado_ContratadaOrigemAreaCod, P00A823_n867ContagemResultado_ContratadaOrigemAreaCod, P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema, P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema, P00A823_A454ContagemResultado_ContadorFSCod, P00A823_n454ContagemResultado_ContadorFSCod, P00A823_A480ContagemResultado_CrFSPessoaCod, P00A823_n480ContagemResultado_CrFSPessoaCod, P00A823_A455ContagemResultado_ContadorFSNom,
               P00A823_n455ContagemResultado_ContadorFSNom, P00A823_A1452ContagemResultado_SS, P00A823_n1452ContagemResultado_SS, P00A823_A1173ContagemResultado_OSManual, P00A823_n1173ContagemResultado_OSManual, P00A823_A494ContagemResultado_Descricao, P00A823_n494ContagemResultado_Descricao, P00A823_A465ContagemResultado_Link, P00A823_n465ContagemResultado_Link, P00A823_A489ContagemResultado_SistemaCod,
               P00A823_n489ContagemResultado_SistemaCod, P00A823_A495ContagemResultado_SistemaNom, P00A823_n495ContagemResultado_SistemaNom, P00A823_A496Contagemresultado_SistemaAreaCod, P00A823_n496Contagemresultado_SistemaAreaCod, P00A823_A509ContagemrResultado_SistemaSigla, P00A823_n509ContagemrResultado_SistemaSigla, P00A823_A515ContagemResultado_SistemaCoord, P00A823_n515ContagemResultado_SistemaCoord, P00A823_A804ContagemResultado_SistemaAtivo,
               P00A823_n804ContagemResultado_SistemaAtivo, P00A823_A146Modulo_Codigo, P00A823_n146Modulo_Codigo, P00A823_A143Modulo_Nome, P00A823_A145Modulo_Sigla, P00A823_A468ContagemResultado_NaoCnfDmnCod, P00A823_n468ContagemResultado_NaoCnfDmnCod, P00A823_A477ContagemResultado_NaoCnfDmnNom, P00A823_n477ContagemResultado_NaoCnfDmnNom, P00A823_A2030ContagemResultado_NaoCnfDmnGls,
               P00A823_n2030ContagemResultado_NaoCnfDmnGls, P00A823_A484ContagemResultado_StatusDmn, P00A823_n484ContagemResultado_StatusDmn, P00A823_A771ContagemResultado_StatusDmnVnc, P00A823_n771ContagemResultado_StatusDmnVnc, P00A823_A601ContagemResultado_Servico, P00A823_n601ContagemResultado_Servico, P00A823_A2008ContagemResultado_CntSrvAls, P00A823_n2008ContagemResultado_CntSrvAls, P00A823_A764ContagemResultado_ServicoGrupo,
               P00A823_n764ContagemResultado_ServicoGrupo, P00A823_A1062ContagemResultado_ServicoTela, P00A823_n1062ContagemResultado_ServicoTela, P00A823_A897ContagemResultado_ServicoAtivo, P00A823_n897ContagemResultado_ServicoAtivo, P00A823_A1398ContagemResultado_ServicoNaoRqrAtr, P00A823_n1398ContagemResultado_ServicoNaoRqrAtr, P00A823_A1636ContagemResultado_ServicoSS, P00A823_n1636ContagemResultado_ServicoSS, P00A823_A1637ContagemResultado_ServicoSSSgl,
               P00A823_n1637ContagemResultado_ServicoSSSgl, P00A823_A2050ContagemResultado_SrvLnhNegCod, P00A823_n2050ContagemResultado_SrvLnhNegCod, P00A823_A485ContagemResultado_EhValidacao, P00A823_n485ContagemResultado_EhValidacao, P00A823_A514ContagemResultado_Observacao, P00A823_n514ContagemResultado_Observacao, P00A823_A592ContagemResultado_Evidencia, P00A823_n592ContagemResultado_Evidencia, P00A823_A1350ContagemResultado_DataCadastro,
               P00A823_n1350ContagemResultado_DataCadastro, P00A823_A1312ContagemResultado_ResponsavelPessCod, P00A823_n1312ContagemResultado_ResponsavelPessCod, P00A823_A1313ContagemResultado_ResponsavelPessNome, P00A823_n1313ContagemResultado_ResponsavelPessNome, P00A823_A1180ContagemResultado_Custo, P00A823_n1180ContagemResultado_Custo, P00A823_A597ContagemResultado_LoteAceiteCod, P00A823_n597ContagemResultado_LoteAceiteCod, P00A823_A528ContagemResultado_LoteAceite,
               P00A823_n528ContagemResultado_LoteAceite, P00A823_A529ContagemResultado_DataAceite, P00A823_n529ContagemResultado_DataAceite, P00A823_A525ContagemResultado_AceiteUserCod, P00A823_n525ContagemResultado_AceiteUserCod, P00A823_A526ContagemResultado_AceitePessoaCod, P00A823_n526ContagemResultado_AceitePessoaCod, P00A823_A527ContagemResultado_AceiteUserNom, P00A823_n527ContagemResultado_AceiteUserNom, P00A823_A1547ContagemResultado_LoteNFe,
               P00A823_n1547ContagemResultado_LoteNFe, P00A823_A1559ContagemResultado_VlrAceite, P00A823_n1559ContagemResultado_VlrAceite, P00A823_A598ContagemResultado_Baseline, P00A823_n598ContagemResultado_Baseline, P00A823_A602ContagemResultado_OSVinculada, P00A823_n602ContagemResultado_OSVinculada, P00A823_A603ContagemResultado_DmnVinculada, P00A823_n603ContagemResultado_DmnVinculada, P00A823_A798ContagemResultado_PFBFSImp,
               P00A823_n798ContagemResultado_PFBFSImp, P00A823_A799ContagemResultado_PFLFSImp, P00A823_n799ContagemResultado_PFLFSImp, P00A823_A1178ContagemResultado_PBFinal, P00A823_n1178ContagemResultado_PBFinal, P00A823_A1179ContagemResultado_PLFinal, P00A823_n1179ContagemResultado_PLFinal, P00A823_A1034ContagemResultadoLiqLog_Data, P00A823_n1034ContagemResultadoLiqLog_Data, P00A823_A1044ContagemResultado_FncUsrCod,
               P00A823_n1044ContagemResultado_FncUsrCod, P00A823_A1046ContagemResultado_Agrupador, P00A823_n1046ContagemResultado_Agrupador, P00A823_A1049ContagemResultado_GlsData, P00A823_n1049ContagemResultado_GlsData, P00A823_A1050ContagemResultado_GlsDescricao, P00A823_n1050ContagemResultado_GlsDescricao, P00A823_A1051ContagemResultado_GlsValor, P00A823_n1051ContagemResultado_GlsValor, P00A823_A1052ContagemResultado_GlsUser,
               P00A823_n1052ContagemResultado_GlsUser, P00A823_A1389ContagemResultado_RdmnIssueId, P00A823_n1389ContagemResultado_RdmnIssueId, P00A823_A1390ContagemResultado_RdmnProjectId, P00A823_n1390ContagemResultado_RdmnProjectId, P00A823_A1392ContagemResultado_RdmnUpdated, P00A823_n1392ContagemResultado_RdmnUpdated, P00A823_A1444ContagemResultado_CntSrvPrrPrz, P00A823_n1444ContagemResultado_CntSrvPrrPrz, P00A823_A1445ContagemResultado_CntSrvPrrCst,
               P00A823_n1445ContagemResultado_CntSrvPrrCst, P00A823_A1457ContagemResultado_TemDpnHmlg, P00A823_n1457ContagemResultado_TemDpnHmlg, P00A823_A1519ContagemResultado_TmpEstAnl, P00A823_n1519ContagemResultado_TmpEstAnl, P00A823_A1505ContagemResultado_TmpEstExc, P00A823_n1505ContagemResultado_TmpEstExc, P00A823_A1506ContagemResultado_TmpEstCrr, P00A823_n1506ContagemResultado_TmpEstCrr, P00A823_A1520ContagemResultado_InicioAnl,
               P00A823_n1520ContagemResultado_InicioAnl, P00A823_A1521ContagemResultado_FimAnl, P00A823_n1521ContagemResultado_FimAnl, P00A823_A1509ContagemResultado_InicioExc, P00A823_n1509ContagemResultado_InicioExc, P00A823_A1510ContagemResultado_FimExc, P00A823_n1510ContagemResultado_FimExc, P00A823_A1511ContagemResultado_InicioCrr, P00A823_n1511ContagemResultado_InicioCrr, P00A823_A1512ContagemResultado_FimCrr,
               P00A823_n1512ContagemResultado_FimCrr, P00A823_A1515ContagemResultado_Evento, P00A823_n1515ContagemResultado_Evento, P00A823_A1544ContagemResultado_ProjetoCod, P00A823_n1544ContagemResultado_ProjetoCod, P00A823_A1593ContagemResultado_CntSrvTpVnc, P00A823_n1593ContagemResultado_CntSrvTpVnc, P00A823_A1594ContagemResultado_CntSrvFtrm, P00A823_n1594ContagemResultado_CntSrvFtrm, P00A823_A1596ContagemResultado_CntSrvPrc,
               P00A823_n1596ContagemResultado_CntSrvPrc, P00A823_A1597ContagemResultado_CntSrvVlrUndCnt, P00A823_n1597ContagemResultado_CntSrvVlrUndCnt
               }
               , new Object[] {
               P00A825_A531ContagemResultado_StatusUltCnt, P00A825_A566ContagemResultado_DataUltCnt, P00A825_A825ContagemResultado_HoraUltCnt, P00A825_A584ContagemResultado_ContadorFM, P00A825_A682ContagemResultado_PFBFMUltima, P00A825_A683ContagemResultado_PFLFMUltima, P00A825_A684ContagemResultado_PFBFSUltima, P00A825_A1480ContagemResultado_CstUntUltima, P00A825_A685ContagemResultado_PFLFSUltima, P00A825_A802ContagemResultado_DeflatorCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1607ContagemResultado_PrzAnl ;
      private short A1609ContagemResultado_PrzCrr ;
      private short A1618ContagemResultado_PrzRsp ;
      private short A1619ContagemResultado_PrzGrt ;
      private short A1633ContagemResultado_CntPrdFtrIni ;
      private short A1634ContagemResultado_CntPrdFtrFim ;
      private short A1599ContagemResultado_ServicoTpHrrq ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short A1650ContagemResultado_PrzInc ;
      private short A1762ContagemResultado_Entrega ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1608ContagemResultado_PrzExc ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1515ContagemResultado_Evento ;
      private short A585ContagemResultado_Erros ;
      private short GXt_int5 ;
      private int AV16ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1620ContagemResultado_CntSrvUndCnt ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A1591ContagemResultado_CodSrvVnc ;
      private int A1765ContagemResultado_CodSrvSSVnc ;
      private int A1627ContagemResultado_CntSrvVncCod ;
      private int A1622ContagemResultado_CntVncCod ;
      private int A2136ContagemResultado_CntadaOsVinc ;
      private int A2137ContagemResultado_SerGrupoVinc ;
      private int A1584ContagemResultado_UOOwner ;
      private int A578ContagemResultado_CntComNaoCnf ;
      private int A584ContagemResultado_ContadorFM ;
      private int A510ContagemResultado_EsforcoSoma ;
      private int A1353ContagemResultado_ContratadaDoOwner ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A508ContagemResultado_Owner ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A807ContagemResultado_ContratadaOrigemPesCod ;
      private int A867ContagemResultado_ContratadaOrigemAreaCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A480ContagemResultado_CrFSPessoaCod ;
      private int A1452ContagemResultado_SS ;
      private int A489ContagemResultado_SistemaCod ;
      private int A496Contagemresultado_SistemaAreaCod ;
      private int A146Modulo_Codigo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A601ContagemResultado_Servico ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A2050ContagemResultado_SrvLnhNegCod ;
      private int A1312ContagemResultado_ResponsavelPessCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A525ContagemResultado_AceiteUserCod ;
      private int A526ContagemResultado_AceitePessoaCod ;
      private int A1547ContagemResultado_LoteNFe ;
      private int A602ContagemResultado_OSVinculada ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A1052ContagemResultado_GlsUser ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int A1390ContagemResultado_RdmnProjectId ;
      private int A1519ContagemResultado_TmpEstAnl ;
      private int A1505ContagemResultado_TmpEstExc ;
      private int A1506ContagemResultado_TmpEstCrr ;
      private int A1544ContagemResultado_ProjetoCod ;
      private int A1236ContagemResultado_ContratanteDoResponsavel ;
      private int A1352ContagemResultado_ContratanteDoOwner ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int AV21GXV1 ;
      private int GXt_int4 ;
      private int AV23GXV2 ;
      private long A553ContagemResultado_DemandaFM_ORDER ;
      private long A599ContagemResultado_Demanda_ORDER ;
      private decimal A1601ContagemResultado_CntSrvIndDvr ;
      private decimal A1613ContagemResultado_CntSrvQtdUntCns ;
      private decimal A1615ContagemResultado_CntIndDvr ;
      private decimal A1616ContagemResultado_CntVlrUndCnt ;
      private decimal A2082ContagemResultado_CntLmtFtr ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A1855ContagemResultado_PFCnc ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A1480ContagemResultado_CstUntUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A802ContagemResultado_DeflatorCnt ;
      private decimal A1602ContagemResultado_GlsIndValor ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1180ContagemResultado_Custo ;
      private decimal A1559ContagemResultado_VlrAceite ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A1178ContagemResultado_PBFinal ;
      private decimal A1179ContagemResultado_PLFinal ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A1444ContagemResultado_CntSrvPrrPrz ;
      private decimal A1445ContagemResultado_CntSrvPrrCst ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal A1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal A497IndiceDivergencia ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal GXt_decimal1 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A1600ContagemResultado_CntSrvSttPgmFnc ;
      private String A1621ContagemResultado_CntSrvUndCntSgl ;
      private String A2209ContagemResultado_CntSrvUndCntNome ;
      private String A1623ContagemResultado_CntSrvMmn ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A1612ContagemResultado_CntNum ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String A1617ContagemResultado_CntClcDvr ;
      private String A1632ContagemResultado_CntPrdFtrCada ;
      private String A1598ContagemResultado_ServicoNome ;
      private String A1590ContagemResultado_SiglaSrvVnc ;
      private String A825ContagemResultado_HoraUltCnt ;
      private String A1610ContagemResultado_PrzTp ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A866ContagemResultado_ContratadaOrigemSigla ;
      private String A808ContagemResultado_ContratadaOrigemPesNom ;
      private String A455ContagemResultado_ContadorFSNom ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A143Modulo_Nome ;
      private String A145Modulo_Sigla ;
      private String A477ContagemResultado_NaoCnfDmnNom ;
      private String A484ContagemResultado_StatusDmn ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String A2008ContagemResultado_CntSrvAls ;
      private String A1062ContagemResultado_ServicoTela ;
      private String A1637ContagemResultado_ServicoSSSgl ;
      private String A1313ContagemResultado_ResponsavelPessNome ;
      private String A528ContagemResultado_LoteAceite ;
      private String A527ContagemResultado_AceiteUserNom ;
      private String A1046ContagemResultado_Agrupador ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A498CalculoDivergencia ;
      private String A486ContagemResultado_EsforcoTotal ;
      private String A2091ContagemResultado_CntSrvPrrNome ;
      private String GXt_char2 ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A529ContagemResultado_DataAceite ;
      private DateTime A1034ContagemResultadoLiqLog_Data ;
      private DateTime A1392ContagemResultado_RdmnUpdated ;
      private DateTime A1520ContagemResultado_InicioAnl ;
      private DateTime A1521ContagemResultado_FimAnl ;
      private DateTime A1509ContagemResultado_InicioExc ;
      private DateTime A1510ContagemResultado_FimExc ;
      private DateTime A1511ContagemResultado_InicioCrr ;
      private DateTime A1512ContagemResultado_FimCrr ;
      private DateTime A1624ContagemResultado_DatVgnInc ;
      private DateTime A1903ContagemResultado_DataPrvPgm ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A1625ContagemResultado_DatIncTA ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A1790ContagemResultado_DataInicio ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A1049ContagemResultado_GlsData ;
      private bool returnInSub ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool n1601ContagemResultado_CntSrvIndDvr ;
      private bool n1613ContagemResultado_CntSrvQtdUntCns ;
      private bool n1620ContagemResultado_CntSrvUndCnt ;
      private bool n1621ContagemResultado_CntSrvUndCntSgl ;
      private bool n2209ContagemResultado_CntSrvUndCntNome ;
      private bool n1623ContagemResultado_CntSrvMmn ;
      private bool n1607ContagemResultado_PrzAnl ;
      private bool n1609ContagemResultado_PrzCrr ;
      private bool n1618ContagemResultado_PrzRsp ;
      private bool n1619ContagemResultado_PrzGrt ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n1615ContagemResultado_CntIndDvr ;
      private bool n1617ContagemResultado_CntClcDvr ;
      private bool n1616ContagemResultado_CntVlrUndCnt ;
      private bool n1632ContagemResultado_CntPrdFtrCada ;
      private bool n1633ContagemResultado_CntPrdFtrIni ;
      private bool n1634ContagemResultado_CntPrdFtrFim ;
      private bool n2082ContagemResultado_CntLmtFtr ;
      private bool n1624ContagemResultado_DatVgnInc ;
      private bool n1598ContagemResultado_ServicoNome ;
      private bool n1599ContagemResultado_ServicoTpHrrq ;
      private bool n1591ContagemResultado_CodSrvVnc ;
      private bool n1590ContagemResultado_SiglaSrvVnc ;
      private bool n1765ContagemResultado_CodSrvSSVnc ;
      private bool n1627ContagemResultado_CntSrvVncCod ;
      private bool n1622ContagemResultado_CntVncCod ;
      private bool n2136ContagemResultado_CntadaOsVinc ;
      private bool n2137ContagemResultado_SerGrupoVinc ;
      private bool n1584ContagemResultado_UOOwner ;
      private bool n1585ContagemResultado_Referencia ;
      private bool n1586ContagemResultado_Restricoes ;
      private bool n1587ContagemResultado_PrioridadePrevista ;
      private bool n1650ContagemResultado_PrzInc ;
      private bool A1714ContagemResultado_Combinada ;
      private bool n1714ContagemResultado_Combinada ;
      private bool n1762ContagemResultado_Entrega ;
      private bool A1791ContagemResultado_SemCusto ;
      private bool n1791ContagemResultado_SemCusto ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool n1903ContagemResultado_DataPrvPgm ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private bool n578ContagemResultado_CntComNaoCnf ;
      private bool n510ContagemResultado_EsforcoSoma ;
      private bool n2118ContagemResultado_Owner_Identificao ;
      private bool n1353ContagemResultado_ContratadaDoOwner ;
      private bool n1608ContagemResultado_PrzExc ;
      private bool n1610ContagemResultado_PrzTp ;
      private bool n1625ContagemResultado_DatIncTA ;
      private bool n1602ContagemResultado_GlsIndValor ;
      private bool A1701ContagemResultado_TemProposta ;
      private bool n1701ContagemResultado_TemProposta ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n457ContagemResultado_Demanda ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1790ContagemResultado_DataInicio ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n1817ContagemResultado_ContratadaCNPJ ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n866ContagemResultado_ContratadaOrigemSigla ;
      private bool n807ContagemResultado_ContratadaOrigemPesCod ;
      private bool n808ContagemResultado_ContratadaOrigemPesNom ;
      private bool n867ContagemResultado_ContratadaOrigemAreaCod ;
      private bool A2093ContagemResultado_ContratadaOrigemUsaSistema ;
      private bool n2093ContagemResultado_ContratadaOrigemUsaSistema ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n480ContagemResultado_CrFSPessoaCod ;
      private bool n455ContagemResultado_ContadorFSNom ;
      private bool n1452ContagemResultado_SS ;
      private bool A1173ContagemResultado_OSManual ;
      private bool n1173ContagemResultado_OSManual ;
      private bool n494ContagemResultado_Descricao ;
      private bool n465ContagemResultado_Link ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n495ContagemResultado_SistemaNom ;
      private bool n496Contagemresultado_SistemaAreaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool A804ContagemResultado_SistemaAtivo ;
      private bool n804ContagemResultado_SistemaAtivo ;
      private bool n146Modulo_Codigo ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n477ContagemResultado_NaoCnfDmnNom ;
      private bool A2030ContagemResultado_NaoCnfDmnGls ;
      private bool n2030ContagemResultado_NaoCnfDmnGls ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n601ContagemResultado_Servico ;
      private bool n2008ContagemResultado_CntSrvAls ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n1062ContagemResultado_ServicoTela ;
      private bool A897ContagemResultado_ServicoAtivo ;
      private bool n897ContagemResultado_ServicoAtivo ;
      private bool A1398ContagemResultado_ServicoNaoRqrAtr ;
      private bool n1398ContagemResultado_ServicoNaoRqrAtr ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1637ContagemResultado_ServicoSSSgl ;
      private bool n2050ContagemResultado_SrvLnhNegCod ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n592ContagemResultado_Evidencia ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1312ContagemResultado_ResponsavelPessCod ;
      private bool n1313ContagemResultado_ResponsavelPessNome ;
      private bool n1180ContagemResultado_Custo ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n528ContagemResultado_LoteAceite ;
      private bool n529ContagemResultado_DataAceite ;
      private bool n525ContagemResultado_AceiteUserCod ;
      private bool n526ContagemResultado_AceitePessoaCod ;
      private bool n527ContagemResultado_AceiteUserNom ;
      private bool n1547ContagemResultado_LoteNFe ;
      private bool n1559ContagemResultado_VlrAceite ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n603ContagemResultado_DmnVinculada ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n1178ContagemResultado_PBFinal ;
      private bool n1179ContagemResultado_PLFinal ;
      private bool n1034ContagemResultadoLiqLog_Data ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n1049ContagemResultado_GlsData ;
      private bool n1050ContagemResultado_GlsDescricao ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1052ContagemResultado_GlsUser ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1390ContagemResultado_RdmnProjectId ;
      private bool n1392ContagemResultado_RdmnUpdated ;
      private bool n1444ContagemResultado_CntSrvPrrPrz ;
      private bool n1445ContagemResultado_CntSrvPrrCst ;
      private bool A1457ContagemResultado_TemDpnHmlg ;
      private bool n1457ContagemResultado_TemDpnHmlg ;
      private bool n1519ContagemResultado_TmpEstAnl ;
      private bool n1505ContagemResultado_TmpEstExc ;
      private bool n1506ContagemResultado_TmpEstCrr ;
      private bool n1520ContagemResultado_InicioAnl ;
      private bool n1521ContagemResultado_FimAnl ;
      private bool n1509ContagemResultado_InicioExc ;
      private bool n1510ContagemResultado_FimExc ;
      private bool n1511ContagemResultado_InicioCrr ;
      private bool n1512ContagemResultado_FimCrr ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1544ContagemResultado_ProjetoCod ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n1594ContagemResultado_CntSrvFtrm ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool n1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool A1802ContagemResultado_TemPndHmlg ;
      private bool A1028ContagemResultado_Liquidada ;
      private bool GXt_boolean3 ;
      private String A465ContagemResultado_Link ;
      private String A514ContagemResultado_Observacao ;
      private String A592ContagemResultado_Evidencia ;
      private String A1050ContagemResultado_GlsDescricao ;
      private String A1585ContagemResultado_Referencia ;
      private String A1586ContagemResultado_Restricoes ;
      private String A1587ContagemResultado_PrioridadePrevista ;
      private String A2118ContagemResultado_Owner_Identificao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A1817ContagemResultado_ContratadaCNPJ ;
      private String A494ContagemResultado_Descricao ;
      private String A495ContagemResultado_SistemaNom ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A603ContagemResultado_DmnVinculada ;
      private String A1594ContagemResultado_CntSrvFtrm ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String A1815ContagemResultado_DmnSrvPrst ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00A812_A903ContratoServicosPrazo_CntSrvCod ;
      private String[] P00A812_A1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool[] P00A812_n1600ContagemResultado_CntSrvSttPgmFnc ;
      private decimal[] P00A812_A1601ContagemResultado_CntSrvIndDvr ;
      private bool[] P00A812_n1601ContagemResultado_CntSrvIndDvr ;
      private decimal[] P00A812_A1613ContagemResultado_CntSrvQtdUntCns ;
      private bool[] P00A812_n1613ContagemResultado_CntSrvQtdUntCns ;
      private int[] P00A812_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] P00A812_n1620ContagemResultado_CntSrvUndCnt ;
      private String[] P00A812_A1621ContagemResultado_CntSrvUndCntSgl ;
      private bool[] P00A812_n1621ContagemResultado_CntSrvUndCntSgl ;
      private String[] P00A812_A2209ContagemResultado_CntSrvUndCntNome ;
      private bool[] P00A812_n2209ContagemResultado_CntSrvUndCntNome ;
      private String[] P00A812_A1623ContagemResultado_CntSrvMmn ;
      private bool[] P00A812_n1623ContagemResultado_CntSrvMmn ;
      private short[] P00A812_A1607ContagemResultado_PrzAnl ;
      private bool[] P00A812_n1607ContagemResultado_PrzAnl ;
      private short[] P00A812_A1609ContagemResultado_PrzCrr ;
      private bool[] P00A812_n1609ContagemResultado_PrzCrr ;
      private short[] P00A812_A1618ContagemResultado_PrzRsp ;
      private bool[] P00A812_n1618ContagemResultado_PrzRsp ;
      private short[] P00A812_A1619ContagemResultado_PrzGrt ;
      private bool[] P00A812_n1619ContagemResultado_PrzGrt ;
      private String[] P00A812_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00A812_n1611ContagemResultado_PrzTpDias ;
      private int[] P00A812_A1603ContagemResultado_CntCod ;
      private bool[] P00A812_n1603ContagemResultado_CntCod ;
      private String[] P00A812_A1612ContagemResultado_CntNum ;
      private bool[] P00A812_n1612ContagemResultado_CntNum ;
      private int[] P00A812_A1604ContagemResultado_CntPrpCod ;
      private bool[] P00A812_n1604ContagemResultado_CntPrpCod ;
      private int[] P00A812_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P00A812_n1605ContagemResultado_CntPrpPesCod ;
      private String[] P00A812_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P00A812_n1606ContagemResultado_CntPrpPesNom ;
      private decimal[] P00A812_A1615ContagemResultado_CntIndDvr ;
      private bool[] P00A812_n1615ContagemResultado_CntIndDvr ;
      private String[] P00A812_A1617ContagemResultado_CntClcDvr ;
      private bool[] P00A812_n1617ContagemResultado_CntClcDvr ;
      private decimal[] P00A812_A1616ContagemResultado_CntVlrUndCnt ;
      private bool[] P00A812_n1616ContagemResultado_CntVlrUndCnt ;
      private String[] P00A812_A1632ContagemResultado_CntPrdFtrCada ;
      private bool[] P00A812_n1632ContagemResultado_CntPrdFtrCada ;
      private short[] P00A812_A1633ContagemResultado_CntPrdFtrIni ;
      private bool[] P00A812_n1633ContagemResultado_CntPrdFtrIni ;
      private short[] P00A812_A1634ContagemResultado_CntPrdFtrFim ;
      private bool[] P00A812_n1634ContagemResultado_CntPrdFtrFim ;
      private decimal[] P00A812_A2082ContagemResultado_CntLmtFtr ;
      private bool[] P00A812_n2082ContagemResultado_CntLmtFtr ;
      private DateTime[] P00A812_A1624ContagemResultado_DatVgnInc ;
      private bool[] P00A812_n1624ContagemResultado_DatVgnInc ;
      private String[] P00A812_A1598ContagemResultado_ServicoNome ;
      private bool[] P00A812_n1598ContagemResultado_ServicoNome ;
      private short[] P00A812_A1599ContagemResultado_ServicoTpHrrq ;
      private bool[] P00A812_n1599ContagemResultado_ServicoTpHrrq ;
      private int[] P00A812_A1591ContagemResultado_CodSrvVnc ;
      private bool[] P00A812_n1591ContagemResultado_CodSrvVnc ;
      private String[] P00A812_A1590ContagemResultado_SiglaSrvVnc ;
      private bool[] P00A812_n1590ContagemResultado_SiglaSrvVnc ;
      private int[] P00A812_A1765ContagemResultado_CodSrvSSVnc ;
      private bool[] P00A812_n1765ContagemResultado_CodSrvSSVnc ;
      private int[] P00A812_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] P00A812_n1627ContagemResultado_CntSrvVncCod ;
      private int[] P00A812_A1622ContagemResultado_CntVncCod ;
      private bool[] P00A812_n1622ContagemResultado_CntVncCod ;
      private int[] P00A812_A2136ContagemResultado_CntadaOsVinc ;
      private bool[] P00A812_n2136ContagemResultado_CntadaOsVinc ;
      private int[] P00A812_A2137ContagemResultado_SerGrupoVinc ;
      private bool[] P00A812_n2137ContagemResultado_SerGrupoVinc ;
      private short[] P00A812_A1583ContagemResultado_TipoRegistro ;
      private int[] P00A812_A1584ContagemResultado_UOOwner ;
      private bool[] P00A812_n1584ContagemResultado_UOOwner ;
      private String[] P00A812_A1585ContagemResultado_Referencia ;
      private bool[] P00A812_n1585ContagemResultado_Referencia ;
      private String[] P00A812_A1586ContagemResultado_Restricoes ;
      private bool[] P00A812_n1586ContagemResultado_Restricoes ;
      private String[] P00A812_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] P00A812_n1587ContagemResultado_PrioridadePrevista ;
      private short[] P00A812_A1650ContagemResultado_PrzInc ;
      private bool[] P00A812_n1650ContagemResultado_PrzInc ;
      private bool[] P00A812_A1714ContagemResultado_Combinada ;
      private bool[] P00A812_n1714ContagemResultado_Combinada ;
      private short[] P00A812_A1762ContagemResultado_Entrega ;
      private bool[] P00A812_n1762ContagemResultado_Entrega ;
      private bool[] P00A812_A1791ContagemResultado_SemCusto ;
      private bool[] P00A812_n1791ContagemResultado_SemCusto ;
      private decimal[] P00A812_A1854ContagemResultado_VlrCnc ;
      private bool[] P00A812_n1854ContagemResultado_VlrCnc ;
      private decimal[] P00A812_A1855ContagemResultado_PFCnc ;
      private bool[] P00A812_n1855ContagemResultado_PFCnc ;
      private DateTime[] P00A812_A1903ContagemResultado_DataPrvPgm ;
      private bool[] P00A812_n1903ContagemResultado_DataPrvPgm ;
      private decimal[] P00A812_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] P00A812_n2133ContagemResultado_QuantidadeSolicitada ;
      private short[] P00A812_A531ContagemResultado_StatusUltCnt ;
      private DateTime[] P00A812_A566ContagemResultado_DataUltCnt ;
      private String[] P00A812_A825ContagemResultado_HoraUltCnt ;
      private int[] P00A812_A578ContagemResultado_CntComNaoCnf ;
      private bool[] P00A812_n578ContagemResultado_CntComNaoCnf ;
      private int[] P00A812_A584ContagemResultado_ContadorFM ;
      private decimal[] P00A812_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00A812_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00A812_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00A812_A1480ContagemResultado_CstUntUltima ;
      private decimal[] P00A812_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00A812_A802ContagemResultado_DeflatorCnt ;
      private int[] P00A812_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00A812_n510ContagemResultado_EsforcoSoma ;
      private String[] P00A812_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00A812_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00A812_A1353ContagemResultado_ContratadaDoOwner ;
      private bool[] P00A812_n1353ContagemResultado_ContratadaDoOwner ;
      private short[] P00A812_A1608ContagemResultado_PrzExc ;
      private bool[] P00A812_n1608ContagemResultado_PrzExc ;
      private String[] P00A812_A1610ContagemResultado_PrzTp ;
      private bool[] P00A812_n1610ContagemResultado_PrzTp ;
      private DateTime[] P00A812_A1625ContagemResultado_DatIncTA ;
      private bool[] P00A812_n1625ContagemResultado_DatIncTA ;
      private decimal[] P00A812_A1602ContagemResultado_GlsIndValor ;
      private bool[] P00A812_n1602ContagemResultado_GlsIndValor ;
      private bool[] P00A812_A1701ContagemResultado_TemProposta ;
      private bool[] P00A812_n1701ContagemResultado_TemProposta ;
      private int[] P00A812_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] P00A812_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] P00A812_A1043ContagemResultado_LiqLogCod ;
      private bool[] P00A812_n1043ContagemResultado_LiqLogCod ;
      private decimal[] P00A812_A512ContagemResultado_ValorPF ;
      private bool[] P00A812_n512ContagemResultado_ValorPF ;
      private int[] P00A812_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00A812_n52Contratada_AreaTrabalhoCod ;
      private int[] P00A812_A890ContagemResultado_Responsavel ;
      private bool[] P00A812_n890ContagemResultado_Responsavel ;
      private int[] P00A812_A508ContagemResultado_Owner ;
      private int[] P00A812_A456ContagemResultado_Codigo ;
      private String[] P00A812_A457ContagemResultado_Demanda ;
      private bool[] P00A812_n457ContagemResultado_Demanda ;
      private String[] P00A812_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00A812_n803ContagemResultado_ContratadaSigla ;
      private String[] P00A812_A801ContagemResultado_ServicoSigla ;
      private bool[] P00A812_n801ContagemResultado_ServicoSigla ;
      private String[] P00A812_A493ContagemResultado_DemandaFM ;
      private bool[] P00A812_n493ContagemResultado_DemandaFM ;
      private int[] P00A812_A490ContagemResultado_ContratadaCod ;
      private bool[] P00A812_n490ContagemResultado_ContratadaCod ;
      private int[] P00A812_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00A812_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P00A812_A471ContagemResultado_DataDmn ;
      private DateTime[] P00A812_A1790ContagemResultado_DataInicio ;
      private bool[] P00A812_n1790ContagemResultado_DataInicio ;
      private DateTime[] P00A812_A472ContagemResultado_DataEntrega ;
      private bool[] P00A812_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00A812_A912ContagemResultado_HoraEntrega ;
      private bool[] P00A812_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00A812_A1351ContagemResultado_DataPrevista ;
      private bool[] P00A812_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00A812_A1349ContagemResultado_DataExecucao ;
      private bool[] P00A812_n1349ContagemResultado_DataExecucao ;
      private DateTime[] P00A812_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00A812_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00A812_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00A812_n1348ContagemResultado_DataHomologacao ;
      private short[] P00A812_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00A812_n1227ContagemResultado_PrazoInicialDias ;
      private short[] P00A812_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00A812_n1237ContagemResultado_PrazoMaisDias ;
      private int[] P00A812_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00A812_n499ContagemResultado_ContratadaPessoaCod ;
      private String[] P00A812_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00A812_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00A812_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00A812_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] P00A812_A1817ContagemResultado_ContratadaCNPJ ;
      private bool[] P00A812_n1817ContagemResultado_ContratadaCNPJ ;
      private int[] P00A812_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00A812_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] P00A812_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] P00A812_n866ContagemResultado_ContratadaOrigemSigla ;
      private int[] P00A812_A807ContagemResultado_ContratadaOrigemPesCod ;
      private bool[] P00A812_n807ContagemResultado_ContratadaOrigemPesCod ;
      private String[] P00A812_A808ContagemResultado_ContratadaOrigemPesNom ;
      private bool[] P00A812_n808ContagemResultado_ContratadaOrigemPesNom ;
      private int[] P00A812_A867ContagemResultado_ContratadaOrigemAreaCod ;
      private bool[] P00A812_n867ContagemResultado_ContratadaOrigemAreaCod ;
      private bool[] P00A812_A2093ContagemResultado_ContratadaOrigemUsaSistema ;
      private bool[] P00A812_n2093ContagemResultado_ContratadaOrigemUsaSistema ;
      private int[] P00A812_A454ContagemResultado_ContadorFSCod ;
      private bool[] P00A812_n454ContagemResultado_ContadorFSCod ;
      private int[] P00A812_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] P00A812_n480ContagemResultado_CrFSPessoaCod ;
      private String[] P00A812_A455ContagemResultado_ContadorFSNom ;
      private bool[] P00A812_n455ContagemResultado_ContadorFSNom ;
      private int[] P00A812_A1452ContagemResultado_SS ;
      private bool[] P00A812_n1452ContagemResultado_SS ;
      private bool[] P00A812_A1173ContagemResultado_OSManual ;
      private bool[] P00A812_n1173ContagemResultado_OSManual ;
      private String[] P00A812_A494ContagemResultado_Descricao ;
      private bool[] P00A812_n494ContagemResultado_Descricao ;
      private String[] P00A812_A465ContagemResultado_Link ;
      private bool[] P00A812_n465ContagemResultado_Link ;
      private int[] P00A812_A489ContagemResultado_SistemaCod ;
      private bool[] P00A812_n489ContagemResultado_SistemaCod ;
      private String[] P00A812_A495ContagemResultado_SistemaNom ;
      private bool[] P00A812_n495ContagemResultado_SistemaNom ;
      private int[] P00A812_A496Contagemresultado_SistemaAreaCod ;
      private bool[] P00A812_n496Contagemresultado_SistemaAreaCod ;
      private String[] P00A812_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00A812_n509ContagemrResultado_SistemaSigla ;
      private String[] P00A812_A515ContagemResultado_SistemaCoord ;
      private bool[] P00A812_n515ContagemResultado_SistemaCoord ;
      private bool[] P00A812_A804ContagemResultado_SistemaAtivo ;
      private bool[] P00A812_n804ContagemResultado_SistemaAtivo ;
      private int[] P00A812_A146Modulo_Codigo ;
      private bool[] P00A812_n146Modulo_Codigo ;
      private String[] P00A812_A143Modulo_Nome ;
      private String[] P00A812_A145Modulo_Sigla ;
      private int[] P00A812_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00A812_n468ContagemResultado_NaoCnfDmnCod ;
      private String[] P00A812_A477ContagemResultado_NaoCnfDmnNom ;
      private bool[] P00A812_n477ContagemResultado_NaoCnfDmnNom ;
      private bool[] P00A812_A2030ContagemResultado_NaoCnfDmnGls ;
      private bool[] P00A812_n2030ContagemResultado_NaoCnfDmnGls ;
      private String[] P00A812_A484ContagemResultado_StatusDmn ;
      private bool[] P00A812_n484ContagemResultado_StatusDmn ;
      private String[] P00A812_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00A812_n771ContagemResultado_StatusDmnVnc ;
      private int[] P00A812_A601ContagemResultado_Servico ;
      private bool[] P00A812_n601ContagemResultado_Servico ;
      private String[] P00A812_A2008ContagemResultado_CntSrvAls ;
      private bool[] P00A812_n2008ContagemResultado_CntSrvAls ;
      private int[] P00A812_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00A812_n764ContagemResultado_ServicoGrupo ;
      private String[] P00A812_A1062ContagemResultado_ServicoTela ;
      private bool[] P00A812_n1062ContagemResultado_ServicoTela ;
      private bool[] P00A812_A897ContagemResultado_ServicoAtivo ;
      private bool[] P00A812_n897ContagemResultado_ServicoAtivo ;
      private bool[] P00A812_A1398ContagemResultado_ServicoNaoRqrAtr ;
      private bool[] P00A812_n1398ContagemResultado_ServicoNaoRqrAtr ;
      private int[] P00A812_A1636ContagemResultado_ServicoSS ;
      private bool[] P00A812_n1636ContagemResultado_ServicoSS ;
      private String[] P00A812_A1637ContagemResultado_ServicoSSSgl ;
      private bool[] P00A812_n1637ContagemResultado_ServicoSSSgl ;
      private int[] P00A812_A2050ContagemResultado_SrvLnhNegCod ;
      private bool[] P00A812_n2050ContagemResultado_SrvLnhNegCod ;
      private bool[] P00A812_A485ContagemResultado_EhValidacao ;
      private bool[] P00A812_n485ContagemResultado_EhValidacao ;
      private String[] P00A812_A514ContagemResultado_Observacao ;
      private bool[] P00A812_n514ContagemResultado_Observacao ;
      private String[] P00A812_A592ContagemResultado_Evidencia ;
      private bool[] P00A812_n592ContagemResultado_Evidencia ;
      private DateTime[] P00A812_A1350ContagemResultado_DataCadastro ;
      private bool[] P00A812_n1350ContagemResultado_DataCadastro ;
      private int[] P00A812_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] P00A812_n1312ContagemResultado_ResponsavelPessCod ;
      private String[] P00A812_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] P00A812_n1313ContagemResultado_ResponsavelPessNome ;
      private decimal[] P00A812_A1180ContagemResultado_Custo ;
      private bool[] P00A812_n1180ContagemResultado_Custo ;
      private int[] P00A812_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00A812_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00A812_A528ContagemResultado_LoteAceite ;
      private bool[] P00A812_n528ContagemResultado_LoteAceite ;
      private DateTime[] P00A812_A529ContagemResultado_DataAceite ;
      private bool[] P00A812_n529ContagemResultado_DataAceite ;
      private int[] P00A812_A525ContagemResultado_AceiteUserCod ;
      private bool[] P00A812_n525ContagemResultado_AceiteUserCod ;
      private int[] P00A812_A526ContagemResultado_AceitePessoaCod ;
      private bool[] P00A812_n526ContagemResultado_AceitePessoaCod ;
      private String[] P00A812_A527ContagemResultado_AceiteUserNom ;
      private bool[] P00A812_n527ContagemResultado_AceiteUserNom ;
      private int[] P00A812_A1547ContagemResultado_LoteNFe ;
      private bool[] P00A812_n1547ContagemResultado_LoteNFe ;
      private decimal[] P00A812_A1559ContagemResultado_VlrAceite ;
      private bool[] P00A812_n1559ContagemResultado_VlrAceite ;
      private bool[] P00A812_A598ContagemResultado_Baseline ;
      private bool[] P00A812_n598ContagemResultado_Baseline ;
      private int[] P00A812_A602ContagemResultado_OSVinculada ;
      private bool[] P00A812_n602ContagemResultado_OSVinculada ;
      private String[] P00A812_A603ContagemResultado_DmnVinculada ;
      private bool[] P00A812_n603ContagemResultado_DmnVinculada ;
      private decimal[] P00A812_A798ContagemResultado_PFBFSImp ;
      private bool[] P00A812_n798ContagemResultado_PFBFSImp ;
      private decimal[] P00A812_A799ContagemResultado_PFLFSImp ;
      private bool[] P00A812_n799ContagemResultado_PFLFSImp ;
      private decimal[] P00A812_A1178ContagemResultado_PBFinal ;
      private bool[] P00A812_n1178ContagemResultado_PBFinal ;
      private decimal[] P00A812_A1179ContagemResultado_PLFinal ;
      private bool[] P00A812_n1179ContagemResultado_PLFinal ;
      private DateTime[] P00A812_A1034ContagemResultadoLiqLog_Data ;
      private bool[] P00A812_n1034ContagemResultadoLiqLog_Data ;
      private int[] P00A812_A1044ContagemResultado_FncUsrCod ;
      private bool[] P00A812_n1044ContagemResultado_FncUsrCod ;
      private String[] P00A812_A1046ContagemResultado_Agrupador ;
      private bool[] P00A812_n1046ContagemResultado_Agrupador ;
      private DateTime[] P00A812_A1049ContagemResultado_GlsData ;
      private bool[] P00A812_n1049ContagemResultado_GlsData ;
      private String[] P00A812_A1050ContagemResultado_GlsDescricao ;
      private bool[] P00A812_n1050ContagemResultado_GlsDescricao ;
      private decimal[] P00A812_A1051ContagemResultado_GlsValor ;
      private bool[] P00A812_n1051ContagemResultado_GlsValor ;
      private int[] P00A812_A1052ContagemResultado_GlsUser ;
      private bool[] P00A812_n1052ContagemResultado_GlsUser ;
      private int[] P00A812_A1389ContagemResultado_RdmnIssueId ;
      private bool[] P00A812_n1389ContagemResultado_RdmnIssueId ;
      private int[] P00A812_A1390ContagemResultado_RdmnProjectId ;
      private bool[] P00A812_n1390ContagemResultado_RdmnProjectId ;
      private DateTime[] P00A812_A1392ContagemResultado_RdmnUpdated ;
      private bool[] P00A812_n1392ContagemResultado_RdmnUpdated ;
      private decimal[] P00A812_A1444ContagemResultado_CntSrvPrrPrz ;
      private bool[] P00A812_n1444ContagemResultado_CntSrvPrrPrz ;
      private decimal[] P00A812_A1445ContagemResultado_CntSrvPrrCst ;
      private bool[] P00A812_n1445ContagemResultado_CntSrvPrrCst ;
      private bool[] P00A812_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P00A812_n1457ContagemResultado_TemDpnHmlg ;
      private int[] P00A812_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00A812_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00A812_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00A812_n1505ContagemResultado_TmpEstExc ;
      private int[] P00A812_A1506ContagemResultado_TmpEstCrr ;
      private bool[] P00A812_n1506ContagemResultado_TmpEstCrr ;
      private DateTime[] P00A812_A1520ContagemResultado_InicioAnl ;
      private bool[] P00A812_n1520ContagemResultado_InicioAnl ;
      private DateTime[] P00A812_A1521ContagemResultado_FimAnl ;
      private bool[] P00A812_n1521ContagemResultado_FimAnl ;
      private DateTime[] P00A812_A1509ContagemResultado_InicioExc ;
      private bool[] P00A812_n1509ContagemResultado_InicioExc ;
      private DateTime[] P00A812_A1510ContagemResultado_FimExc ;
      private bool[] P00A812_n1510ContagemResultado_FimExc ;
      private DateTime[] P00A812_A1511ContagemResultado_InicioCrr ;
      private bool[] P00A812_n1511ContagemResultado_InicioCrr ;
      private DateTime[] P00A812_A1512ContagemResultado_FimCrr ;
      private bool[] P00A812_n1512ContagemResultado_FimCrr ;
      private short[] P00A812_A1515ContagemResultado_Evento ;
      private bool[] P00A812_n1515ContagemResultado_Evento ;
      private int[] P00A812_A1544ContagemResultado_ProjetoCod ;
      private bool[] P00A812_n1544ContagemResultado_ProjetoCod ;
      private String[] P00A812_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P00A812_n1593ContagemResultado_CntSrvTpVnc ;
      private String[] P00A812_A1594ContagemResultado_CntSrvFtrm ;
      private bool[] P00A812_n1594ContagemResultado_CntSrvFtrm ;
      private decimal[] P00A812_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P00A812_n1596ContagemResultado_CntSrvPrc ;
      private decimal[] P00A812_A1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool[] P00A812_n1597ContagemResultado_CntSrvVlrUndCnt ;
      private int[] P00A823_A903ContratoServicosPrazo_CntSrvCod ;
      private String[] P00A823_A1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool[] P00A823_n1600ContagemResultado_CntSrvSttPgmFnc ;
      private decimal[] P00A823_A1601ContagemResultado_CntSrvIndDvr ;
      private bool[] P00A823_n1601ContagemResultado_CntSrvIndDvr ;
      private decimal[] P00A823_A1613ContagemResultado_CntSrvQtdUntCns ;
      private bool[] P00A823_n1613ContagemResultado_CntSrvQtdUntCns ;
      private int[] P00A823_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] P00A823_n1620ContagemResultado_CntSrvUndCnt ;
      private String[] P00A823_A1621ContagemResultado_CntSrvUndCntSgl ;
      private bool[] P00A823_n1621ContagemResultado_CntSrvUndCntSgl ;
      private String[] P00A823_A2209ContagemResultado_CntSrvUndCntNome ;
      private bool[] P00A823_n2209ContagemResultado_CntSrvUndCntNome ;
      private String[] P00A823_A1623ContagemResultado_CntSrvMmn ;
      private bool[] P00A823_n1623ContagemResultado_CntSrvMmn ;
      private short[] P00A823_A1607ContagemResultado_PrzAnl ;
      private bool[] P00A823_n1607ContagemResultado_PrzAnl ;
      private short[] P00A823_A1609ContagemResultado_PrzCrr ;
      private bool[] P00A823_n1609ContagemResultado_PrzCrr ;
      private short[] P00A823_A1618ContagemResultado_PrzRsp ;
      private bool[] P00A823_n1618ContagemResultado_PrzRsp ;
      private short[] P00A823_A1619ContagemResultado_PrzGrt ;
      private bool[] P00A823_n1619ContagemResultado_PrzGrt ;
      private String[] P00A823_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00A823_n1611ContagemResultado_PrzTpDias ;
      private int[] P00A823_A1603ContagemResultado_CntCod ;
      private bool[] P00A823_n1603ContagemResultado_CntCod ;
      private String[] P00A823_A1612ContagemResultado_CntNum ;
      private bool[] P00A823_n1612ContagemResultado_CntNum ;
      private int[] P00A823_A1604ContagemResultado_CntPrpCod ;
      private bool[] P00A823_n1604ContagemResultado_CntPrpCod ;
      private int[] P00A823_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P00A823_n1605ContagemResultado_CntPrpPesCod ;
      private String[] P00A823_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P00A823_n1606ContagemResultado_CntPrpPesNom ;
      private decimal[] P00A823_A1615ContagemResultado_CntIndDvr ;
      private bool[] P00A823_n1615ContagemResultado_CntIndDvr ;
      private String[] P00A823_A1617ContagemResultado_CntClcDvr ;
      private bool[] P00A823_n1617ContagemResultado_CntClcDvr ;
      private decimal[] P00A823_A1616ContagemResultado_CntVlrUndCnt ;
      private bool[] P00A823_n1616ContagemResultado_CntVlrUndCnt ;
      private String[] P00A823_A1632ContagemResultado_CntPrdFtrCada ;
      private bool[] P00A823_n1632ContagemResultado_CntPrdFtrCada ;
      private short[] P00A823_A1633ContagemResultado_CntPrdFtrIni ;
      private bool[] P00A823_n1633ContagemResultado_CntPrdFtrIni ;
      private short[] P00A823_A1634ContagemResultado_CntPrdFtrFim ;
      private bool[] P00A823_n1634ContagemResultado_CntPrdFtrFim ;
      private decimal[] P00A823_A2082ContagemResultado_CntLmtFtr ;
      private bool[] P00A823_n2082ContagemResultado_CntLmtFtr ;
      private DateTime[] P00A823_A1624ContagemResultado_DatVgnInc ;
      private bool[] P00A823_n1624ContagemResultado_DatVgnInc ;
      private String[] P00A823_A1598ContagemResultado_ServicoNome ;
      private bool[] P00A823_n1598ContagemResultado_ServicoNome ;
      private short[] P00A823_A1599ContagemResultado_ServicoTpHrrq ;
      private bool[] P00A823_n1599ContagemResultado_ServicoTpHrrq ;
      private int[] P00A823_A1591ContagemResultado_CodSrvVnc ;
      private bool[] P00A823_n1591ContagemResultado_CodSrvVnc ;
      private String[] P00A823_A1590ContagemResultado_SiglaSrvVnc ;
      private bool[] P00A823_n1590ContagemResultado_SiglaSrvVnc ;
      private int[] P00A823_A1765ContagemResultado_CodSrvSSVnc ;
      private bool[] P00A823_n1765ContagemResultado_CodSrvSSVnc ;
      private int[] P00A823_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] P00A823_n1627ContagemResultado_CntSrvVncCod ;
      private int[] P00A823_A1622ContagemResultado_CntVncCod ;
      private bool[] P00A823_n1622ContagemResultado_CntVncCod ;
      private int[] P00A823_A2136ContagemResultado_CntadaOsVinc ;
      private bool[] P00A823_n2136ContagemResultado_CntadaOsVinc ;
      private int[] P00A823_A2137ContagemResultado_SerGrupoVinc ;
      private bool[] P00A823_n2137ContagemResultado_SerGrupoVinc ;
      private short[] P00A823_A1583ContagemResultado_TipoRegistro ;
      private int[] P00A823_A1584ContagemResultado_UOOwner ;
      private bool[] P00A823_n1584ContagemResultado_UOOwner ;
      private String[] P00A823_A1585ContagemResultado_Referencia ;
      private bool[] P00A823_n1585ContagemResultado_Referencia ;
      private String[] P00A823_A1586ContagemResultado_Restricoes ;
      private bool[] P00A823_n1586ContagemResultado_Restricoes ;
      private String[] P00A823_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] P00A823_n1587ContagemResultado_PrioridadePrevista ;
      private short[] P00A823_A1650ContagemResultado_PrzInc ;
      private bool[] P00A823_n1650ContagemResultado_PrzInc ;
      private bool[] P00A823_A1714ContagemResultado_Combinada ;
      private bool[] P00A823_n1714ContagemResultado_Combinada ;
      private short[] P00A823_A1762ContagemResultado_Entrega ;
      private bool[] P00A823_n1762ContagemResultado_Entrega ;
      private bool[] P00A823_A1791ContagemResultado_SemCusto ;
      private bool[] P00A823_n1791ContagemResultado_SemCusto ;
      private decimal[] P00A823_A1854ContagemResultado_VlrCnc ;
      private bool[] P00A823_n1854ContagemResultado_VlrCnc ;
      private decimal[] P00A823_A1855ContagemResultado_PFCnc ;
      private bool[] P00A823_n1855ContagemResultado_PFCnc ;
      private DateTime[] P00A823_A1903ContagemResultado_DataPrvPgm ;
      private bool[] P00A823_n1903ContagemResultado_DataPrvPgm ;
      private decimal[] P00A823_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] P00A823_n2133ContagemResultado_QuantidadeSolicitada ;
      private short[] P00A823_A531ContagemResultado_StatusUltCnt ;
      private DateTime[] P00A823_A566ContagemResultado_DataUltCnt ;
      private String[] P00A823_A825ContagemResultado_HoraUltCnt ;
      private int[] P00A823_A578ContagemResultado_CntComNaoCnf ;
      private bool[] P00A823_n578ContagemResultado_CntComNaoCnf ;
      private int[] P00A823_A584ContagemResultado_ContadorFM ;
      private decimal[] P00A823_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00A823_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00A823_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00A823_A1480ContagemResultado_CstUntUltima ;
      private decimal[] P00A823_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00A823_A802ContagemResultado_DeflatorCnt ;
      private int[] P00A823_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00A823_n510ContagemResultado_EsforcoSoma ;
      private String[] P00A823_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00A823_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00A823_A1353ContagemResultado_ContratadaDoOwner ;
      private bool[] P00A823_n1353ContagemResultado_ContratadaDoOwner ;
      private short[] P00A823_A1608ContagemResultado_PrzExc ;
      private bool[] P00A823_n1608ContagemResultado_PrzExc ;
      private String[] P00A823_A1610ContagemResultado_PrzTp ;
      private bool[] P00A823_n1610ContagemResultado_PrzTp ;
      private DateTime[] P00A823_A1625ContagemResultado_DatIncTA ;
      private bool[] P00A823_n1625ContagemResultado_DatIncTA ;
      private decimal[] P00A823_A1602ContagemResultado_GlsIndValor ;
      private bool[] P00A823_n1602ContagemResultado_GlsIndValor ;
      private bool[] P00A823_A1701ContagemResultado_TemProposta ;
      private bool[] P00A823_n1701ContagemResultado_TemProposta ;
      private int[] P00A823_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] P00A823_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] P00A823_A1043ContagemResultado_LiqLogCod ;
      private bool[] P00A823_n1043ContagemResultado_LiqLogCod ;
      private decimal[] P00A823_A512ContagemResultado_ValorPF ;
      private bool[] P00A823_n512ContagemResultado_ValorPF ;
      private int[] P00A823_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00A823_n52Contratada_AreaTrabalhoCod ;
      private int[] P00A823_A890ContagemResultado_Responsavel ;
      private bool[] P00A823_n890ContagemResultado_Responsavel ;
      private int[] P00A823_A508ContagemResultado_Owner ;
      private int[] P00A823_A456ContagemResultado_Codigo ;
      private String[] P00A823_A457ContagemResultado_Demanda ;
      private bool[] P00A823_n457ContagemResultado_Demanda ;
      private String[] P00A823_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00A823_n803ContagemResultado_ContratadaSigla ;
      private String[] P00A823_A801ContagemResultado_ServicoSigla ;
      private bool[] P00A823_n801ContagemResultado_ServicoSigla ;
      private String[] P00A823_A493ContagemResultado_DemandaFM ;
      private bool[] P00A823_n493ContagemResultado_DemandaFM ;
      private int[] P00A823_A490ContagemResultado_ContratadaCod ;
      private bool[] P00A823_n490ContagemResultado_ContratadaCod ;
      private int[] P00A823_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00A823_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P00A823_A471ContagemResultado_DataDmn ;
      private DateTime[] P00A823_A1790ContagemResultado_DataInicio ;
      private bool[] P00A823_n1790ContagemResultado_DataInicio ;
      private DateTime[] P00A823_A472ContagemResultado_DataEntrega ;
      private bool[] P00A823_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00A823_A912ContagemResultado_HoraEntrega ;
      private bool[] P00A823_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00A823_A1351ContagemResultado_DataPrevista ;
      private bool[] P00A823_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00A823_A1349ContagemResultado_DataExecucao ;
      private bool[] P00A823_n1349ContagemResultado_DataExecucao ;
      private DateTime[] P00A823_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00A823_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00A823_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00A823_n1348ContagemResultado_DataHomologacao ;
      private short[] P00A823_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00A823_n1227ContagemResultado_PrazoInicialDias ;
      private short[] P00A823_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00A823_n1237ContagemResultado_PrazoMaisDias ;
      private int[] P00A823_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00A823_n499ContagemResultado_ContratadaPessoaCod ;
      private String[] P00A823_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00A823_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00A823_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00A823_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] P00A823_A1817ContagemResultado_ContratadaCNPJ ;
      private bool[] P00A823_n1817ContagemResultado_ContratadaCNPJ ;
      private int[] P00A823_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00A823_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] P00A823_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] P00A823_n866ContagemResultado_ContratadaOrigemSigla ;
      private int[] P00A823_A807ContagemResultado_ContratadaOrigemPesCod ;
      private bool[] P00A823_n807ContagemResultado_ContratadaOrigemPesCod ;
      private String[] P00A823_A808ContagemResultado_ContratadaOrigemPesNom ;
      private bool[] P00A823_n808ContagemResultado_ContratadaOrigemPesNom ;
      private int[] P00A823_A867ContagemResultado_ContratadaOrigemAreaCod ;
      private bool[] P00A823_n867ContagemResultado_ContratadaOrigemAreaCod ;
      private bool[] P00A823_A2093ContagemResultado_ContratadaOrigemUsaSistema ;
      private bool[] P00A823_n2093ContagemResultado_ContratadaOrigemUsaSistema ;
      private int[] P00A823_A454ContagemResultado_ContadorFSCod ;
      private bool[] P00A823_n454ContagemResultado_ContadorFSCod ;
      private int[] P00A823_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] P00A823_n480ContagemResultado_CrFSPessoaCod ;
      private String[] P00A823_A455ContagemResultado_ContadorFSNom ;
      private bool[] P00A823_n455ContagemResultado_ContadorFSNom ;
      private int[] P00A823_A1452ContagemResultado_SS ;
      private bool[] P00A823_n1452ContagemResultado_SS ;
      private bool[] P00A823_A1173ContagemResultado_OSManual ;
      private bool[] P00A823_n1173ContagemResultado_OSManual ;
      private String[] P00A823_A494ContagemResultado_Descricao ;
      private bool[] P00A823_n494ContagemResultado_Descricao ;
      private String[] P00A823_A465ContagemResultado_Link ;
      private bool[] P00A823_n465ContagemResultado_Link ;
      private int[] P00A823_A489ContagemResultado_SistemaCod ;
      private bool[] P00A823_n489ContagemResultado_SistemaCod ;
      private String[] P00A823_A495ContagemResultado_SistemaNom ;
      private bool[] P00A823_n495ContagemResultado_SistemaNom ;
      private int[] P00A823_A496Contagemresultado_SistemaAreaCod ;
      private bool[] P00A823_n496Contagemresultado_SistemaAreaCod ;
      private String[] P00A823_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00A823_n509ContagemrResultado_SistemaSigla ;
      private String[] P00A823_A515ContagemResultado_SistemaCoord ;
      private bool[] P00A823_n515ContagemResultado_SistemaCoord ;
      private bool[] P00A823_A804ContagemResultado_SistemaAtivo ;
      private bool[] P00A823_n804ContagemResultado_SistemaAtivo ;
      private int[] P00A823_A146Modulo_Codigo ;
      private bool[] P00A823_n146Modulo_Codigo ;
      private String[] P00A823_A143Modulo_Nome ;
      private String[] P00A823_A145Modulo_Sigla ;
      private int[] P00A823_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00A823_n468ContagemResultado_NaoCnfDmnCod ;
      private String[] P00A823_A477ContagemResultado_NaoCnfDmnNom ;
      private bool[] P00A823_n477ContagemResultado_NaoCnfDmnNom ;
      private bool[] P00A823_A2030ContagemResultado_NaoCnfDmnGls ;
      private bool[] P00A823_n2030ContagemResultado_NaoCnfDmnGls ;
      private String[] P00A823_A484ContagemResultado_StatusDmn ;
      private bool[] P00A823_n484ContagemResultado_StatusDmn ;
      private String[] P00A823_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00A823_n771ContagemResultado_StatusDmnVnc ;
      private int[] P00A823_A601ContagemResultado_Servico ;
      private bool[] P00A823_n601ContagemResultado_Servico ;
      private String[] P00A823_A2008ContagemResultado_CntSrvAls ;
      private bool[] P00A823_n2008ContagemResultado_CntSrvAls ;
      private int[] P00A823_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00A823_n764ContagemResultado_ServicoGrupo ;
      private String[] P00A823_A1062ContagemResultado_ServicoTela ;
      private bool[] P00A823_n1062ContagemResultado_ServicoTela ;
      private bool[] P00A823_A897ContagemResultado_ServicoAtivo ;
      private bool[] P00A823_n897ContagemResultado_ServicoAtivo ;
      private bool[] P00A823_A1398ContagemResultado_ServicoNaoRqrAtr ;
      private bool[] P00A823_n1398ContagemResultado_ServicoNaoRqrAtr ;
      private int[] P00A823_A1636ContagemResultado_ServicoSS ;
      private bool[] P00A823_n1636ContagemResultado_ServicoSS ;
      private String[] P00A823_A1637ContagemResultado_ServicoSSSgl ;
      private bool[] P00A823_n1637ContagemResultado_ServicoSSSgl ;
      private int[] P00A823_A2050ContagemResultado_SrvLnhNegCod ;
      private bool[] P00A823_n2050ContagemResultado_SrvLnhNegCod ;
      private bool[] P00A823_A485ContagemResultado_EhValidacao ;
      private bool[] P00A823_n485ContagemResultado_EhValidacao ;
      private String[] P00A823_A514ContagemResultado_Observacao ;
      private bool[] P00A823_n514ContagemResultado_Observacao ;
      private String[] P00A823_A592ContagemResultado_Evidencia ;
      private bool[] P00A823_n592ContagemResultado_Evidencia ;
      private DateTime[] P00A823_A1350ContagemResultado_DataCadastro ;
      private bool[] P00A823_n1350ContagemResultado_DataCadastro ;
      private int[] P00A823_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] P00A823_n1312ContagemResultado_ResponsavelPessCod ;
      private String[] P00A823_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] P00A823_n1313ContagemResultado_ResponsavelPessNome ;
      private decimal[] P00A823_A1180ContagemResultado_Custo ;
      private bool[] P00A823_n1180ContagemResultado_Custo ;
      private int[] P00A823_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00A823_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00A823_A528ContagemResultado_LoteAceite ;
      private bool[] P00A823_n528ContagemResultado_LoteAceite ;
      private DateTime[] P00A823_A529ContagemResultado_DataAceite ;
      private bool[] P00A823_n529ContagemResultado_DataAceite ;
      private int[] P00A823_A525ContagemResultado_AceiteUserCod ;
      private bool[] P00A823_n525ContagemResultado_AceiteUserCod ;
      private int[] P00A823_A526ContagemResultado_AceitePessoaCod ;
      private bool[] P00A823_n526ContagemResultado_AceitePessoaCod ;
      private String[] P00A823_A527ContagemResultado_AceiteUserNom ;
      private bool[] P00A823_n527ContagemResultado_AceiteUserNom ;
      private int[] P00A823_A1547ContagemResultado_LoteNFe ;
      private bool[] P00A823_n1547ContagemResultado_LoteNFe ;
      private decimal[] P00A823_A1559ContagemResultado_VlrAceite ;
      private bool[] P00A823_n1559ContagemResultado_VlrAceite ;
      private bool[] P00A823_A598ContagemResultado_Baseline ;
      private bool[] P00A823_n598ContagemResultado_Baseline ;
      private int[] P00A823_A602ContagemResultado_OSVinculada ;
      private bool[] P00A823_n602ContagemResultado_OSVinculada ;
      private String[] P00A823_A603ContagemResultado_DmnVinculada ;
      private bool[] P00A823_n603ContagemResultado_DmnVinculada ;
      private decimal[] P00A823_A798ContagemResultado_PFBFSImp ;
      private bool[] P00A823_n798ContagemResultado_PFBFSImp ;
      private decimal[] P00A823_A799ContagemResultado_PFLFSImp ;
      private bool[] P00A823_n799ContagemResultado_PFLFSImp ;
      private decimal[] P00A823_A1178ContagemResultado_PBFinal ;
      private bool[] P00A823_n1178ContagemResultado_PBFinal ;
      private decimal[] P00A823_A1179ContagemResultado_PLFinal ;
      private bool[] P00A823_n1179ContagemResultado_PLFinal ;
      private DateTime[] P00A823_A1034ContagemResultadoLiqLog_Data ;
      private bool[] P00A823_n1034ContagemResultadoLiqLog_Data ;
      private int[] P00A823_A1044ContagemResultado_FncUsrCod ;
      private bool[] P00A823_n1044ContagemResultado_FncUsrCod ;
      private String[] P00A823_A1046ContagemResultado_Agrupador ;
      private bool[] P00A823_n1046ContagemResultado_Agrupador ;
      private DateTime[] P00A823_A1049ContagemResultado_GlsData ;
      private bool[] P00A823_n1049ContagemResultado_GlsData ;
      private String[] P00A823_A1050ContagemResultado_GlsDescricao ;
      private bool[] P00A823_n1050ContagemResultado_GlsDescricao ;
      private decimal[] P00A823_A1051ContagemResultado_GlsValor ;
      private bool[] P00A823_n1051ContagemResultado_GlsValor ;
      private int[] P00A823_A1052ContagemResultado_GlsUser ;
      private bool[] P00A823_n1052ContagemResultado_GlsUser ;
      private int[] P00A823_A1389ContagemResultado_RdmnIssueId ;
      private bool[] P00A823_n1389ContagemResultado_RdmnIssueId ;
      private int[] P00A823_A1390ContagemResultado_RdmnProjectId ;
      private bool[] P00A823_n1390ContagemResultado_RdmnProjectId ;
      private DateTime[] P00A823_A1392ContagemResultado_RdmnUpdated ;
      private bool[] P00A823_n1392ContagemResultado_RdmnUpdated ;
      private decimal[] P00A823_A1444ContagemResultado_CntSrvPrrPrz ;
      private bool[] P00A823_n1444ContagemResultado_CntSrvPrrPrz ;
      private decimal[] P00A823_A1445ContagemResultado_CntSrvPrrCst ;
      private bool[] P00A823_n1445ContagemResultado_CntSrvPrrCst ;
      private bool[] P00A823_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P00A823_n1457ContagemResultado_TemDpnHmlg ;
      private int[] P00A823_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00A823_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00A823_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00A823_n1505ContagemResultado_TmpEstExc ;
      private int[] P00A823_A1506ContagemResultado_TmpEstCrr ;
      private bool[] P00A823_n1506ContagemResultado_TmpEstCrr ;
      private DateTime[] P00A823_A1520ContagemResultado_InicioAnl ;
      private bool[] P00A823_n1520ContagemResultado_InicioAnl ;
      private DateTime[] P00A823_A1521ContagemResultado_FimAnl ;
      private bool[] P00A823_n1521ContagemResultado_FimAnl ;
      private DateTime[] P00A823_A1509ContagemResultado_InicioExc ;
      private bool[] P00A823_n1509ContagemResultado_InicioExc ;
      private DateTime[] P00A823_A1510ContagemResultado_FimExc ;
      private bool[] P00A823_n1510ContagemResultado_FimExc ;
      private DateTime[] P00A823_A1511ContagemResultado_InicioCrr ;
      private bool[] P00A823_n1511ContagemResultado_InicioCrr ;
      private DateTime[] P00A823_A1512ContagemResultado_FimCrr ;
      private bool[] P00A823_n1512ContagemResultado_FimCrr ;
      private short[] P00A823_A1515ContagemResultado_Evento ;
      private bool[] P00A823_n1515ContagemResultado_Evento ;
      private int[] P00A823_A1544ContagemResultado_ProjetoCod ;
      private bool[] P00A823_n1544ContagemResultado_ProjetoCod ;
      private String[] P00A823_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P00A823_n1593ContagemResultado_CntSrvTpVnc ;
      private String[] P00A823_A1594ContagemResultado_CntSrvFtrm ;
      private bool[] P00A823_n1594ContagemResultado_CntSrvFtrm ;
      private decimal[] P00A823_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P00A823_n1596ContagemResultado_CntSrvPrc ;
      private decimal[] P00A823_A1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool[] P00A823_n1597ContagemResultado_CntSrvVlrUndCnt ;
      private short[] P00A825_A531ContagemResultado_StatusUltCnt ;
      private DateTime[] P00A825_A566ContagemResultado_DataUltCnt ;
      private String[] P00A825_A825ContagemResultado_HoraUltCnt ;
      private int[] P00A825_A584ContagemResultado_ContadorFM ;
      private decimal[] P00A825_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00A825_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00A825_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00A825_A1480ContagemResultado_CstUntUltima ;
      private decimal[] P00A825_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00A825_A802ContagemResultado_DeflatorCnt ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditcontagemresultado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A812 ;
          prmP00A812 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A812 ;
          cmdBufferP00A812=" SELECT T34.[ContratoServicosPrazo_CntSrvCod], T28.[ContratoServicos_StatusPagFnc] AS ContagemResultado_CntSrvSttPgmFnc, T28.[ContratoServicos_IndiceDivergencia] AS ContagemResultado_CntSrvIndDvr, T28.[ContratoServicos_QntUntCns] AS ContagemResultado_CntSrvQtdUntCns, T28.[ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, T30.[UnidadeMedicao_Sigla] AS ContagemResultado_CntSrvUndCntSgl, T30.[UnidadeMedicao_Nome] AS ContagemResultado_CntSrvUndCntNome, T28.[ContratoServicos_Momento] AS ContagemResultado_CntSrvMmn, T28.[ContratoServicos_PrazoAnalise] AS ContagemResultado_PrzAnl, T28.[ContratoServicos_PrazoCorrecao] AS ContagemResultado_PrzCrr, T28.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T28.[ContratoServicos_PrazoGarantia] AS ContagemResultado_PrzGrt, T28.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T28.[Contrato_Codigo] AS ContagemResultado_CntCod, T31.[Contrato_Numero] AS ContagemResultado_CntNum, T31.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T32.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T33.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T31.[Contrato_IndiceDivergencia] AS ContagemResultado_CntIndDvr, T31.[Contrato_CalculoDivergencia] AS ContagemResultado_CntClcDvr, T31.[Contrato_ValorUnidadeContratacao] AS ContagemResultado_CntVlrUndCnt, T31.[Contrato_PrdFtrCada] AS ContagemResultado_CntPrdFtrCada, T31.[Contrato_PrdFtrIni] AS ContagemResultado_CntPrdFtrIni, T31.[Contrato_PrdFtrFim] AS ContagemResultado_CntPrdFtrFim, T31.[Contrato_LmtFtr] AS ContagemResultado_CntLmtFtr, T31.[Contrato_DataVigenciaInicio] AS ContagemResultado_DatVgnInc, T29.[Servico_Nome] AS ContagemResultado_ServicoNome, T29.[Servico_TipoHierarquia] AS ContagemResultado_ServicoTpHrrq, T14.[Servico_Codigo] AS ContagemResultado_CodSrvVnc, "
          + " T15.[Servico_Sigla] AS ContagemResultado_SiglaSrvVnc, T13.[ContagemResultado_ServicoSS] AS ContagemResultado_CodSrvSSVnc, T13.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T14.[Contrato_Codigo] AS ContagemResultado_CntVncCod, T13.[ContagemResultado_ContratadaCod] AS ContagemResultado_CntadaOsVinc, T15.[ServicoGrupo_Codigo] AS ContagemResultado_SerGrupoVinc, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_UOOwner], T1.[ContagemResultado_Referencia], T1.[ContagemResultado_Restricoes], T1.[ContagemResultado_PrioridadePrevista], T28.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc, T1.[ContagemResultado_Combinada], T1.[ContagemResultado_Entrega], T1.[ContagemResultado_SemCusto], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_PFCnc], T1.[ContagemResultado_DataPrvPgm], T1.[ContagemResultado_QuantidadeSolicitada], COALESCE( T20.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T20.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T20.[ContagemResultado_HoraUltCnt], '') AS ContagemResultado_HoraUltCnt, COALESCE( T21.[ContagemResultado_CntComNaoCnf], 0) AS ContagemResultado_CntComNaoCnf, COALESCE( T20.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T20.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T20.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T20.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T20.[ContagemResultado_CstUntUltima], 0) AS ContagemResultado_CstUntUltima, COALESCE( T20.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T20.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt,"
          + " COALESCE( T22.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T19.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, COALESCE( T23.[ContagemResultado_ContratadaDoOwner], 0) AS ContagemResultado_ContratadaDoOwner, COALESCE( T34.[ContratoServicosPrazo_Dias], 0) AS ContagemResultado_PrzExc, COALESCE( T34.[ContratoServicosPrazo_Tipo], '') AS ContagemResultado_PrzTp, COALESCE( T35.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DatIncTA, COALESCE( T24.[ContagemResultado_GlsIndValor], 0) AS ContagemResultado_GlsIndValor, COALESCE( T25.[ContagemResultado_TemProposta], CONVERT(BIT, 0)) AS ContagemResultado_TemProposta, T1.[ContagemResultado_CntSrvPrrCod], T1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, T1.[ContagemResultado_ValorPF], T26.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Demanda], T26.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T29.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataInicio], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_PrazoMaisDias], T26.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPe,"
          + " T27.[Pessoa_Nome] AS ContagemResultado_ContratadaPe, T26.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T27.[Pessoa_Docto] AS ContagemResultado_ContratadaCN, T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T2.[Contratada_Sigla] AS ContagemResultado_ContratadaOr, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaOr, T3.[Pessoa_Nome] AS ContagemResultado_ContratadaOr, T2.[Contratada_AreaTrabalhoCod] AS ContagemResultado_ContratadaOr, T2.[Contratada_UsaOSistema] AS ContagemResultado_ContratadaOrigemUsaSistema, T1.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CrFSPessoaCod, T5.[Pessoa_Nome] AS ContagemResultado_ContadorFSNo, T1.[ContagemResultado_SS], T1.[ContagemResultado_OSManual], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Link], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T6.[Sistema_Nome] AS ContagemResultado_SistemaNom, T6.[Sistema_AreaTrabalhoCod] AS Contagemresultado_SistemaAreaCod, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T6.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T6.[Sistema_Ativo] AS ContagemResultado_SistemaAtivo, T1.[Modulo_Codigo], T7.[Modulo_Nome], T7.[Modulo_Sigla], T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T8.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfDmnNom, T8.[NaoConformidade_Glosavel] AS ContagemResultado_NaoCnfDmnGls, T1.[ContagemResultado_StatusDmn], T13.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T28.[Servico_Codigo] AS ContagemResultado_Servico, T28.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T29.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T29.[Servico_Tela] AS ContagemResultado_ServicoTela,"
          + " T29.[Servico_Ativo] AS ContagemResultado_ServicoAtivo, T28.[ContratoServicos_NaoRequerAtr] AS ContagemResultado_ServicoNaoRqrAtr, T1.[ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, T9.[Servico_Sigla] AS ContagemResultado_ServicoSSSgl, T29.[Servico_LinNegCod] AS ContagemResultado_SrvLnhNegCod, T1.[ContagemResultado_EhValidacao], T1.[ContagemResultado_Observacao], T1.[ContagemResultado_Evidencia], T1.[ContagemResultado_DataCadastro], T17.[Usuario_PessoaCod] AS ContagemResultado_ResponsavelP, T18.[Pessoa_Nome] AS ContagemResultado_ResponsavelP, T1.[ContagemResultado_Custo], T1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T10.[Lote_Numero] AS ContagemResultado_LoteAceite, T10.[Lote_Data] AS ContagemResultado_DataAceite, T10.[Lote_UserCod] AS ContagemResultado_AceiteUserCod, T11.[Usuario_PessoaCod] AS ContagemResultado_AceitePessoa, T12.[Pessoa_Nome] AS ContagemResultado_AceiteUserNo, T10.[Lote_NFe] AS ContagemResultado_LoteNFe, T1.[ContagemResultado_VlrAceite], T1.[ContagemResultado_Baseline], T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T13.[ContagemResultado_Demanda] AS ContagemResultado_DmnVinculada, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T1.[ContagemResultado_PBFinal], T1.[ContagemResultado_PLFinal], T16.[ContagemResultadoLiqLog_Data], T1.[ContagemResultado_FncUsrCod], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_GlsData], T1.[ContagemResultado_GlsDescricao], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsUser], T1.[ContagemResultado_RdmnIssueId], T1.[ContagemResultado_RdmnProjectId], T1.[ContagemResultado_RdmnUpdated], T1.[ContagemResultado_CntSrvPrrPrz], T1.[ContagemResultado_CntSrvPrrCst], T1.[ContagemResultado_TemDpnHmlg], T1.[ContagemResultado_TmpEstAnl],"
          + " T1.[ContagemResultado_TmpEstExc], T1.[ContagemResultado_TmpEstCrr], T1.[ContagemResultado_InicioAnl], T1.[ContagemResultado_FimAnl], T1.[ContagemResultado_InicioExc], T1.[ContagemResultado_FimExc], T1.[ContagemResultado_InicioCrr], T1.[ContagemResultado_FimCrr], T1.[ContagemResultado_Evento], T1.[ContagemResultado_ProjetoCod], T28.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T28.[ServicoContrato_Faturamento] AS ContagemResultado_CntSrvFtrm, T28.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T28.[Servico_VlrUnidadeContratada] AS ContagemResultado_CntSrvVlrUndCnt FROM ((((((((((((((((((((((((((((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFSCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Modulo] T7 WITH (NOLOCK) ON T7.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [NaoConformidade] T8 WITH (NOLOCK) ON T8.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [Servico] T9 WITH (NOLOCK) ON T9.[Servico_Codigo] = T1.[ContagemResultado_ServicoSS]) LEFT JOIN [Lote] T10 WITH (NOLOCK) ON T10.[Lote_Codigo] = T1.[ContagemResultado_LoteAceiteCod]) LEFT JOIN [Usuario] T11 WITH (NOLOCK) ON T11.[Usuario_Codigo] = T10.[Lote_UserCod]) LEFT JOIN [Pessoa] T12 WITH (NOLOCK) ON T12.[Pessoa_Codigo] = T11.[Usuario_PessoaCod]) LEFT JOIN [ContagemResultado] T13 WITH (NOLOCK) ON T13.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada])"
          + " LEFT JOIN [ContratoServicos] T14 WITH (NOLOCK) ON T14.[ContratoServicos_Codigo] = T13.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T15 WITH (NOLOCK) ON T15.[Servico_Codigo] = T14.[Servico_Codigo]) LEFT JOIN [ContagemResultadoLiqLog] T16 WITH (NOLOCK) ON T16.[ContagemResultadoLiqLog_Codigo] = T1.[ContagemResultado_LiqLogCod]) LEFT JOIN [Usuario] T17 WITH (NOLOCK) ON T17.[Usuario_Codigo] = T1.[ContagemResultado_Responsavel]) LEFT JOIN [Pessoa] T18 WITH (NOLOCK) ON T18.[Pessoa_Codigo] = T17.[Usuario_PessoaCod]) LEFT JOIN (SELECT T37.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T36.[Usuario_Codigo] FROM ([Usuario] T36 WITH (NOLOCK) INNER JOIN [Pessoa] T37 WITH (NOLOCK) ON T37.[Pessoa_Codigo] = T36.[Usuario_PessoaCod]) ) T19 ON T19.[Usuario_Codigo] = T1.[ContagemResultado_Owner]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_HoraCnt]) AS ContagemResultado_HoraUltCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_CstUntPrd]) AS ContagemResultado_CstUntUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T20 ON T20.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultado_CntComNaoCnf, [ContagemResultado_Codigo]"
          + " FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 and Not ([ContagemResultado_NaoCnfCntCod] = convert(int, 0)) GROUP BY [ContagemResultado_Codigo] ) T21 ON T21.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T22 ON T22.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN(T36.[ContratadaUsuario_ContratadaCod]) AS ContagemResultado_ContratadaDoOwner, T38.[ContagemResultado_Codigo] FROM ([ContratadaUsuario] T36 WITH (NOLOCK) INNER JOIN [Contratada] T37 WITH (NOLOCK) ON T37.[Contratada_Codigo] = T36.[ContratadaUsuario_ContratadaCod]),  [ContagemResultado] T38 WITH (NOLOCK), [AreaTrabalho] T39 WITH (NOLOCK) WHERE (T39.[AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod) AND (T37.[Contratada_AreaTrabalhoCod] = T39.[AreaTrabalho_Codigo] and T36.[ContratadaUsuario_UsuarioCod] = T38.[ContagemResultado_Owner]) GROUP BY T38.[ContagemResultado_Codigo] ) T23 ON T23.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM(T36.[ContagemResultadoIndicadores_Valor]) AS ContagemResultado_GlsIndValor, T37.[ContagemResultado_Codigo] FROM [ContagemResultadoIndicadores] T36 WITH (NOLOCK),  [ContagemResultado] T37 WITH (NOLOCK) WHERE T36.[ContagemResultadoIndicadores_DemandaCod] = T37.[ContagemResultado_Codigo] GROUP BY T37.[ContagemResultado_Codigo] ) T24 ON T24.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T37.[GXC5], 0) > 0 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS ContagemResultado_TemProposta, T36.[ContagemResultado_Codigo] FROM"
          + " [ContagemResultado] T36 WITH (NOLOCK),  (SELECT COUNT(*) AS GXC5 FROM dbo.[Proposta] WITH (NOLOCK) ) T37 ) T25 ON T25.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T26 WITH (NOLOCK) ON T26.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T27 WITH (NOLOCK) ON T27.[Pessoa_Codigo] = T26.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T28 WITH (NOLOCK) ON T28.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T29 WITH (NOLOCK) ON T29.[Servico_Codigo] = T28.[Servico_Codigo]) LEFT JOIN [UnidadeMedicao] T30 WITH (NOLOCK) ON T30.[UnidadeMedicao_Codigo] = T28.[ContratoServicos_UnidadeContratada]) LEFT JOIN [Contrato] T31 WITH (NOLOCK) ON T31.[Contrato_Codigo] = T28.[Contrato_Codigo]) LEFT JOIN [Usuario] T32 WITH (NOLOCK) ON T32.[Usuario_Codigo] = T31.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T33 WITH (NOLOCK) ON T33.[Pessoa_Codigo] = T32.[Usuario_PessoaCod]) LEFT JOIN [ContratoServicosPrazo] T34 WITH (NOLOCK) ON T34.[ContratoServicosPrazo_CntSrvCod] = T1.[ContagemResultado_CntSrvCod]),  (SELECT T36.[ContratoTermoAditivo_DataInicio], T36.[ContratoTermoAditivo_Codigo], T37.[GXC4] AS GXC4 FROM [ContratoTermoAditivo] T36 WITH (NOLOCK),  (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4 FROM [ContratoTermoAditivo] WITH (NOLOCK) ) T37 WHERE T36.[ContratoTermoAditivo_Codigo] = T37.[GXC4] ) T35 WHERE T1.[ContagemResultado_Codigo] = @AV16ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmP00A823 ;
          prmP00A823 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A823 ;
          cmdBufferP00A823=" SELECT T34.[ContratoServicosPrazo_CntSrvCod], T28.[ContratoServicos_StatusPagFnc] AS ContagemResultado_CntSrvSttPgmFnc, T28.[ContratoServicos_IndiceDivergencia] AS ContagemResultado_CntSrvIndDvr, T28.[ContratoServicos_QntUntCns] AS ContagemResultado_CntSrvQtdUntCns, T28.[ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, T30.[UnidadeMedicao_Sigla] AS ContagemResultado_CntSrvUndCntSgl, T30.[UnidadeMedicao_Nome] AS ContagemResultado_CntSrvUndCntNome, T28.[ContratoServicos_Momento] AS ContagemResultado_CntSrvMmn, T28.[ContratoServicos_PrazoAnalise] AS ContagemResultado_PrzAnl, T28.[ContratoServicos_PrazoCorrecao] AS ContagemResultado_PrzCrr, T28.[ContratoServicos_PrazoResposta] AS ContagemResultado_PrzRsp, T28.[ContratoServicos_PrazoGarantia] AS ContagemResultado_PrzGrt, T28.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T28.[Contrato_Codigo] AS ContagemResultado_CntCod, T31.[Contrato_Numero] AS ContagemResultado_CntNum, T31.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T32.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T33.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T31.[Contrato_IndiceDivergencia] AS ContagemResultado_CntIndDvr, T31.[Contrato_CalculoDivergencia] AS ContagemResultado_CntClcDvr, T31.[Contrato_ValorUnidadeContratacao] AS ContagemResultado_CntVlrUndCnt, T31.[Contrato_PrdFtrCada] AS ContagemResultado_CntPrdFtrCada, T31.[Contrato_PrdFtrIni] AS ContagemResultado_CntPrdFtrIni, T31.[Contrato_PrdFtrFim] AS ContagemResultado_CntPrdFtrFim, T31.[Contrato_LmtFtr] AS ContagemResultado_CntLmtFtr, T31.[Contrato_DataVigenciaInicio] AS ContagemResultado_DatVgnInc, T29.[Servico_Nome] AS ContagemResultado_ServicoNome, T29.[Servico_TipoHierarquia] AS ContagemResultado_ServicoTpHrrq, T14.[Servico_Codigo] AS ContagemResultado_CodSrvVnc, "
          + " T15.[Servico_Sigla] AS ContagemResultado_SiglaSrvVnc, T13.[ContagemResultado_ServicoSS] AS ContagemResultado_CodSrvSSVnc, T13.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T14.[Contrato_Codigo] AS ContagemResultado_CntVncCod, T13.[ContagemResultado_ContratadaCod] AS ContagemResultado_CntadaOsVinc, T15.[ServicoGrupo_Codigo] AS ContagemResultado_SerGrupoVinc, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_UOOwner], T1.[ContagemResultado_Referencia], T1.[ContagemResultado_Restricoes], T1.[ContagemResultado_PrioridadePrevista], T28.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc, T1.[ContagemResultado_Combinada], T1.[ContagemResultado_Entrega], T1.[ContagemResultado_SemCusto], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_PFCnc], T1.[ContagemResultado_DataPrvPgm], T1.[ContagemResultado_QuantidadeSolicitada], COALESCE( T20.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T20.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T20.[ContagemResultado_HoraUltCnt], '') AS ContagemResultado_HoraUltCnt, COALESCE( T21.[ContagemResultado_CntComNaoCnf], 0) AS ContagemResultado_CntComNaoCnf, COALESCE( T20.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T20.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T20.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T20.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T20.[ContagemResultado_CstUntUltima], 0) AS ContagemResultado_CstUntUltima, COALESCE( T20.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T20.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt,"
          + " COALESCE( T22.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T19.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, COALESCE( T23.[ContagemResultado_ContratadaDoOwner], 0) AS ContagemResultado_ContratadaDoOwner, COALESCE( T34.[ContratoServicosPrazo_Dias], 0) AS ContagemResultado_PrzExc, COALESCE( T34.[ContratoServicosPrazo_Tipo], '') AS ContagemResultado_PrzTp, COALESCE( T35.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DatIncTA, COALESCE( T24.[ContagemResultado_GlsIndValor], 0) AS ContagemResultado_GlsIndValor, COALESCE( T25.[ContagemResultado_TemProposta], CONVERT(BIT, 0)) AS ContagemResultado_TemProposta, T1.[ContagemResultado_CntSrvPrrCod], T1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, T1.[ContagemResultado_ValorPF], T26.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Demanda], T26.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T29.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataInicio], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_PrazoInicialDias], T1.[ContagemResultado_PrazoMaisDias], T26.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPe,"
          + " T27.[Pessoa_Nome] AS ContagemResultado_ContratadaPe, T26.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T27.[Pessoa_Docto] AS ContagemResultado_ContratadaCN, T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T2.[Contratada_Sigla] AS ContagemResultado_ContratadaOr, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaOr, T3.[Pessoa_Nome] AS ContagemResultado_ContratadaOr, T2.[Contratada_AreaTrabalhoCod] AS ContagemResultado_ContratadaOr, T2.[Contratada_UsaOSistema] AS ContagemResultado_ContratadaOrigemUsaSistema, T1.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CrFSPessoaCod, T5.[Pessoa_Nome] AS ContagemResultado_ContadorFSNo, T1.[ContagemResultado_SS], T1.[ContagemResultado_OSManual], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Link], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T6.[Sistema_Nome] AS ContagemResultado_SistemaNom, T6.[Sistema_AreaTrabalhoCod] AS Contagemresultado_SistemaAreaCod, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T6.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T6.[Sistema_Ativo] AS ContagemResultado_SistemaAtivo, T1.[Modulo_Codigo], T7.[Modulo_Nome], T7.[Modulo_Sigla], T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T8.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfDmnNom, T8.[NaoConformidade_Glosavel] AS ContagemResultado_NaoCnfDmnGls, T1.[ContagemResultado_StatusDmn], T13.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T28.[Servico_Codigo] AS ContagemResultado_Servico, T28.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T29.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T29.[Servico_Tela] AS ContagemResultado_ServicoTela,"
          + " T29.[Servico_Ativo] AS ContagemResultado_ServicoAtivo, T28.[ContratoServicos_NaoRequerAtr] AS ContagemResultado_ServicoNaoRqrAtr, T1.[ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, T9.[Servico_Sigla] AS ContagemResultado_ServicoSSSgl, T29.[Servico_LinNegCod] AS ContagemResultado_SrvLnhNegCod, T1.[ContagemResultado_EhValidacao], T1.[ContagemResultado_Observacao], T1.[ContagemResultado_Evidencia], T1.[ContagemResultado_DataCadastro], T17.[Usuario_PessoaCod] AS ContagemResultado_ResponsavelP, T18.[Pessoa_Nome] AS ContagemResultado_ResponsavelP, T1.[ContagemResultado_Custo], T1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T10.[Lote_Numero] AS ContagemResultado_LoteAceite, T10.[Lote_Data] AS ContagemResultado_DataAceite, T10.[Lote_UserCod] AS ContagemResultado_AceiteUserCod, T11.[Usuario_PessoaCod] AS ContagemResultado_AceitePessoa, T12.[Pessoa_Nome] AS ContagemResultado_AceiteUserNo, T10.[Lote_NFe] AS ContagemResultado_LoteNFe, T1.[ContagemResultado_VlrAceite], T1.[ContagemResultado_Baseline], T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T13.[ContagemResultado_Demanda] AS ContagemResultado_DmnVinculada, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T1.[ContagemResultado_PBFinal], T1.[ContagemResultado_PLFinal], T16.[ContagemResultadoLiqLog_Data], T1.[ContagemResultado_FncUsrCod], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_GlsData], T1.[ContagemResultado_GlsDescricao], T1.[ContagemResultado_GlsValor], T1.[ContagemResultado_GlsUser], T1.[ContagemResultado_RdmnIssueId], T1.[ContagemResultado_RdmnProjectId], T1.[ContagemResultado_RdmnUpdated], T1.[ContagemResultado_CntSrvPrrPrz], T1.[ContagemResultado_CntSrvPrrCst], T1.[ContagemResultado_TemDpnHmlg], T1.[ContagemResultado_TmpEstAnl],"
          + " T1.[ContagemResultado_TmpEstExc], T1.[ContagemResultado_TmpEstCrr], T1.[ContagemResultado_InicioAnl], T1.[ContagemResultado_FimAnl], T1.[ContagemResultado_InicioExc], T1.[ContagemResultado_FimExc], T1.[ContagemResultado_InicioCrr], T1.[ContagemResultado_FimCrr], T1.[ContagemResultado_Evento], T1.[ContagemResultado_ProjetoCod], T28.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T28.[ServicoContrato_Faturamento] AS ContagemResultado_CntSrvFtrm, T28.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T28.[Servico_VlrUnidadeContratada] AS ContagemResultado_CntSrvVlrUndCnt FROM ((((((((((((((((((((((((((((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFSCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Modulo] T7 WITH (NOLOCK) ON T7.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [NaoConformidade] T8 WITH (NOLOCK) ON T8.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [Servico] T9 WITH (NOLOCK) ON T9.[Servico_Codigo] = T1.[ContagemResultado_ServicoSS]) LEFT JOIN [Lote] T10 WITH (NOLOCK) ON T10.[Lote_Codigo] = T1.[ContagemResultado_LoteAceiteCod]) LEFT JOIN [Usuario] T11 WITH (NOLOCK) ON T11.[Usuario_Codigo] = T10.[Lote_UserCod]) LEFT JOIN [Pessoa] T12 WITH (NOLOCK) ON T12.[Pessoa_Codigo] = T11.[Usuario_PessoaCod]) LEFT JOIN [ContagemResultado] T13 WITH (NOLOCK) ON T13.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada])"
          + " LEFT JOIN [ContratoServicos] T14 WITH (NOLOCK) ON T14.[ContratoServicos_Codigo] = T13.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T15 WITH (NOLOCK) ON T15.[Servico_Codigo] = T14.[Servico_Codigo]) LEFT JOIN [ContagemResultadoLiqLog] T16 WITH (NOLOCK) ON T16.[ContagemResultadoLiqLog_Codigo] = T1.[ContagemResultado_LiqLogCod]) LEFT JOIN [Usuario] T17 WITH (NOLOCK) ON T17.[Usuario_Codigo] = T1.[ContagemResultado_Responsavel]) LEFT JOIN [Pessoa] T18 WITH (NOLOCK) ON T18.[Pessoa_Codigo] = T17.[Usuario_PessoaCod]) LEFT JOIN (SELECT T37.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T36.[Usuario_Codigo] FROM ([Usuario] T36 WITH (NOLOCK) INNER JOIN [Pessoa] T37 WITH (NOLOCK) ON T37.[Pessoa_Codigo] = T36.[Usuario_PessoaCod]) ) T19 ON T19.[Usuario_Codigo] = T1.[ContagemResultado_Owner]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_HoraCnt]) AS ContagemResultado_HoraUltCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_CstUntPrd]) AS ContagemResultado_CstUntUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T20 ON T20.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultado_CntComNaoCnf, [ContagemResultado_Codigo]"
          + " FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 and Not ([ContagemResultado_NaoCnfCntCod] = convert(int, 0)) GROUP BY [ContagemResultado_Codigo] ) T21 ON T21.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T22 ON T22.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN(T36.[ContratadaUsuario_ContratadaCod]) AS ContagemResultado_ContratadaDoOwner, T38.[ContagemResultado_Codigo] FROM ([ContratadaUsuario] T36 WITH (NOLOCK) INNER JOIN [Contratada] T37 WITH (NOLOCK) ON T37.[Contratada_Codigo] = T36.[ContratadaUsuario_ContratadaCod]),  [ContagemResultado] T38 WITH (NOLOCK), [AreaTrabalho] T39 WITH (NOLOCK) WHERE (T39.[AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod) AND (T37.[Contratada_AreaTrabalhoCod] = T39.[AreaTrabalho_Codigo] and T36.[ContratadaUsuario_UsuarioCod] = T38.[ContagemResultado_Owner]) GROUP BY T38.[ContagemResultado_Codigo] ) T23 ON T23.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM(T36.[ContagemResultadoIndicadores_Valor]) AS ContagemResultado_GlsIndValor, T37.[ContagemResultado_Codigo] FROM [ContagemResultadoIndicadores] T36 WITH (NOLOCK),  [ContagemResultado] T37 WITH (NOLOCK) WHERE T36.[ContagemResultadoIndicadores_DemandaCod] = T37.[ContagemResultado_Codigo] GROUP BY T37.[ContagemResultado_Codigo] ) T24 ON T24.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T37.[GXC5], 0) > 0 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS ContagemResultado_TemProposta, T36.[ContagemResultado_Codigo] FROM"
          + " [ContagemResultado] T36 WITH (NOLOCK),  (SELECT COUNT(*) AS GXC5 FROM dbo.[Proposta] WITH (NOLOCK) ) T37 ) T25 ON T25.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T26 WITH (NOLOCK) ON T26.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T27 WITH (NOLOCK) ON T27.[Pessoa_Codigo] = T26.[Contratada_PessoaCod]) LEFT JOIN [ContratoServicos] T28 WITH (NOLOCK) ON T28.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T29 WITH (NOLOCK) ON T29.[Servico_Codigo] = T28.[Servico_Codigo]) LEFT JOIN [UnidadeMedicao] T30 WITH (NOLOCK) ON T30.[UnidadeMedicao_Codigo] = T28.[ContratoServicos_UnidadeContratada]) LEFT JOIN [Contrato] T31 WITH (NOLOCK) ON T31.[Contrato_Codigo] = T28.[Contrato_Codigo]) LEFT JOIN [Usuario] T32 WITH (NOLOCK) ON T32.[Usuario_Codigo] = T31.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T33 WITH (NOLOCK) ON T33.[Pessoa_Codigo] = T32.[Usuario_PessoaCod]) LEFT JOIN [ContratoServicosPrazo] T34 WITH (NOLOCK) ON T34.[ContratoServicosPrazo_CntSrvCod] = T1.[ContagemResultado_CntSrvCod]),  (SELECT T36.[ContratoTermoAditivo_DataInicio], T36.[ContratoTermoAditivo_Codigo], T37.[GXC4] AS GXC4 FROM [ContratoTermoAditivo] T36 WITH (NOLOCK),  (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4 FROM [ContratoTermoAditivo] WITH (NOLOCK) ) T37 WHERE T36.[ContratoTermoAditivo_Codigo] = T37.[GXC4] ) T35 WHERE T1.[ContagemResultado_Codigo] = @AV16ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmP00A825 ;
          prmP00A825 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00A812", cmdBufferP00A812,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A812,1,0,true,true )
             ,new CursorDef("P00A823", cmdBufferP00A823,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A823,1,0,true,true )
             ,new CursorDef("P00A825", "SELECT COALESCE( T1.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T1.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T1.[ContagemResultado_HoraUltCnt], '') AS ContagemResultado_HoraUltCnt, COALESCE( T1.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T1.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T1.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T1.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T1.[ContagemResultado_CstUntUltima], 0) AS ContagemResultado_CstUntUltima, COALESCE( T1.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T1.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt FROM (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_HoraCnt]) AS ContagemResultado_HoraUltCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_CstUntPrd]) AS ContagemResultado_CstUntUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A825,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 20) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getString(18, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((String[]) buf[37])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(20);
                ((decimal[]) buf[39])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(21);
                ((String[]) buf[41])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(22);
                ((short[]) buf[43])[0] = rslt.getShort(23) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(23);
                ((short[]) buf[45])[0] = rslt.getShort(24) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(24);
                ((decimal[]) buf[47])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[49])[0] = rslt.getGXDate(26) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(26);
                ((String[]) buf[51])[0] = rslt.getString(27, 50) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(27);
                ((short[]) buf[53])[0] = rslt.getShort(28) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(28);
                ((int[]) buf[55])[0] = rslt.getInt(29) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(29);
                ((String[]) buf[57])[0] = rslt.getString(30, 15) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(30);
                ((int[]) buf[59])[0] = rslt.getInt(31) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(31);
                ((int[]) buf[61])[0] = rslt.getInt(32) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(32);
                ((int[]) buf[63])[0] = rslt.getInt(33) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(33);
                ((int[]) buf[65])[0] = rslt.getInt(34) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(34);
                ((int[]) buf[67])[0] = rslt.getInt(35) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(35);
                ((short[]) buf[69])[0] = rslt.getShort(36) ;
                ((int[]) buf[70])[0] = rslt.getInt(37) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(37);
                ((String[]) buf[72])[0] = rslt.getVarchar(38) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(38);
                ((String[]) buf[74])[0] = rslt.getVarchar(39) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(39);
                ((String[]) buf[76])[0] = rslt.getVarchar(40) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(40);
                ((short[]) buf[78])[0] = rslt.getShort(41) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(41);
                ((bool[]) buf[80])[0] = rslt.getBool(42) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(42);
                ((short[]) buf[82])[0] = rslt.getShort(43) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(43);
                ((bool[]) buf[84])[0] = rslt.getBool(44) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(44);
                ((decimal[]) buf[86])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(45);
                ((decimal[]) buf[88])[0] = rslt.getDecimal(46) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(46);
                ((DateTime[]) buf[90])[0] = rslt.getGXDate(47) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(47);
                ((decimal[]) buf[92])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(48);
                ((short[]) buf[94])[0] = rslt.getShort(49) ;
                ((DateTime[]) buf[95])[0] = rslt.getGXDate(50) ;
                ((String[]) buf[96])[0] = rslt.getString(51, 5) ;
                ((int[]) buf[97])[0] = rslt.getInt(52) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(52);
                ((int[]) buf[99])[0] = rslt.getInt(53) ;
                ((decimal[]) buf[100])[0] = rslt.getDecimal(54) ;
                ((decimal[]) buf[101])[0] = rslt.getDecimal(55) ;
                ((decimal[]) buf[102])[0] = rslt.getDecimal(56) ;
                ((decimal[]) buf[103])[0] = rslt.getDecimal(57) ;
                ((decimal[]) buf[104])[0] = rslt.getDecimal(58) ;
                ((decimal[]) buf[105])[0] = rslt.getDecimal(59) ;
                ((int[]) buf[106])[0] = rslt.getInt(60) ;
                ((bool[]) buf[107])[0] = rslt.wasNull(60);
                ((String[]) buf[108])[0] = rslt.getVarchar(61) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                ((int[]) buf[110])[0] = rslt.getInt(62) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(62);
                ((short[]) buf[112])[0] = rslt.getShort(63) ;
                ((bool[]) buf[113])[0] = rslt.wasNull(63);
                ((String[]) buf[114])[0] = rslt.getString(64, 20) ;
                ((bool[]) buf[115])[0] = rslt.wasNull(64);
                ((DateTime[]) buf[116])[0] = rslt.getGXDate(65) ;
                ((bool[]) buf[117])[0] = rslt.wasNull(65);
                ((decimal[]) buf[118])[0] = rslt.getDecimal(66) ;
                ((bool[]) buf[119])[0] = rslt.wasNull(66);
                ((bool[]) buf[120])[0] = rslt.getBool(67) ;
                ((bool[]) buf[121])[0] = rslt.wasNull(67);
                ((int[]) buf[122])[0] = rslt.getInt(68) ;
                ((bool[]) buf[123])[0] = rslt.wasNull(68);
                ((int[]) buf[124])[0] = rslt.getInt(69) ;
                ((bool[]) buf[125])[0] = rslt.wasNull(69);
                ((decimal[]) buf[126])[0] = rslt.getDecimal(70) ;
                ((bool[]) buf[127])[0] = rslt.wasNull(70);
                ((int[]) buf[128])[0] = rslt.getInt(71) ;
                ((bool[]) buf[129])[0] = rslt.wasNull(71);
                ((int[]) buf[130])[0] = rslt.getInt(72) ;
                ((bool[]) buf[131])[0] = rslt.wasNull(72);
                ((int[]) buf[132])[0] = rslt.getInt(73) ;
                ((int[]) buf[133])[0] = rslt.getInt(74) ;
                ((String[]) buf[134])[0] = rslt.getVarchar(75) ;
                ((bool[]) buf[135])[0] = rslt.wasNull(75);
                ((String[]) buf[136])[0] = rslt.getString(76, 15) ;
                ((bool[]) buf[137])[0] = rslt.wasNull(76);
                ((String[]) buf[138])[0] = rslt.getString(77, 15) ;
                ((bool[]) buf[139])[0] = rslt.wasNull(77);
                ((String[]) buf[140])[0] = rslt.getVarchar(78) ;
                ((bool[]) buf[141])[0] = rslt.wasNull(78);
                ((int[]) buf[142])[0] = rslt.getInt(79) ;
                ((bool[]) buf[143])[0] = rslt.wasNull(79);
                ((int[]) buf[144])[0] = rslt.getInt(80) ;
                ((bool[]) buf[145])[0] = rslt.wasNull(80);
                ((DateTime[]) buf[146])[0] = rslt.getGXDate(81) ;
                ((DateTime[]) buf[147])[0] = rslt.getGXDate(82) ;
                ((bool[]) buf[148])[0] = rslt.wasNull(82);
                ((DateTime[]) buf[149])[0] = rslt.getGXDate(83) ;
                ((bool[]) buf[150])[0] = rslt.wasNull(83);
                ((DateTime[]) buf[151])[0] = rslt.getGXDateTime(84) ;
                ((bool[]) buf[152])[0] = rslt.wasNull(84);
                ((DateTime[]) buf[153])[0] = rslt.getGXDateTime(85) ;
                ((bool[]) buf[154])[0] = rslt.wasNull(85);
                ((DateTime[]) buf[155])[0] = rslt.getGXDateTime(86) ;
                ((bool[]) buf[156])[0] = rslt.wasNull(86);
                ((DateTime[]) buf[157])[0] = rslt.getGXDateTime(87) ;
                ((bool[]) buf[158])[0] = rslt.wasNull(87);
                ((DateTime[]) buf[159])[0] = rslt.getGXDateTime(88) ;
                ((bool[]) buf[160])[0] = rslt.wasNull(88);
                ((short[]) buf[161])[0] = rslt.getShort(89) ;
                ((bool[]) buf[162])[0] = rslt.wasNull(89);
                ((short[]) buf[163])[0] = rslt.getShort(90) ;
                ((bool[]) buf[164])[0] = rslt.wasNull(90);
                ((int[]) buf[165])[0] = rslt.getInt(91) ;
                ((bool[]) buf[166])[0] = rslt.wasNull(91);
                ((String[]) buf[167])[0] = rslt.getString(92, 100) ;
                ((bool[]) buf[168])[0] = rslt.wasNull(92);
                ((String[]) buf[169])[0] = rslt.getString(93, 1) ;
                ((bool[]) buf[170])[0] = rslt.wasNull(93);
                ((String[]) buf[171])[0] = rslt.getVarchar(94) ;
                ((bool[]) buf[172])[0] = rslt.wasNull(94);
                ((int[]) buf[173])[0] = rslt.getInt(95) ;
                ((bool[]) buf[174])[0] = rslt.wasNull(95);
                ((String[]) buf[175])[0] = rslt.getString(96, 15) ;
                ((bool[]) buf[176])[0] = rslt.wasNull(96);
                ((int[]) buf[177])[0] = rslt.getInt(97) ;
                ((bool[]) buf[178])[0] = rslt.wasNull(97);
                ((String[]) buf[179])[0] = rslt.getString(98, 100) ;
                ((bool[]) buf[180])[0] = rslt.wasNull(98);
                ((int[]) buf[181])[0] = rslt.getInt(99) ;
                ((bool[]) buf[182])[0] = rslt.wasNull(99);
                ((bool[]) buf[183])[0] = rslt.getBool(100) ;
                ((bool[]) buf[184])[0] = rslt.wasNull(100);
                ((int[]) buf[185])[0] = rslt.getInt(101) ;
                ((bool[]) buf[186])[0] = rslt.wasNull(101);
                ((int[]) buf[187])[0] = rslt.getInt(102) ;
                ((bool[]) buf[188])[0] = rslt.wasNull(102);
                ((String[]) buf[189])[0] = rslt.getString(103, 100) ;
                ((bool[]) buf[190])[0] = rslt.wasNull(103);
                ((int[]) buf[191])[0] = rslt.getInt(104) ;
                ((bool[]) buf[192])[0] = rslt.wasNull(104);
                ((bool[]) buf[193])[0] = rslt.getBool(105) ;
                ((bool[]) buf[194])[0] = rslt.wasNull(105);
                ((String[]) buf[195])[0] = rslt.getVarchar(106) ;
                ((bool[]) buf[196])[0] = rslt.wasNull(106);
                ((String[]) buf[197])[0] = rslt.getLongVarchar(107) ;
                ((bool[]) buf[198])[0] = rslt.wasNull(107);
                ((int[]) buf[199])[0] = rslt.getInt(108) ;
                ((bool[]) buf[200])[0] = rslt.wasNull(108);
                ((String[]) buf[201])[0] = rslt.getVarchar(109) ;
                ((bool[]) buf[202])[0] = rslt.wasNull(109);
                ((int[]) buf[203])[0] = rslt.getInt(110) ;
                ((bool[]) buf[204])[0] = rslt.wasNull(110);
                ((String[]) buf[205])[0] = rslt.getString(111, 25) ;
                ((bool[]) buf[206])[0] = rslt.wasNull(111);
                ((String[]) buf[207])[0] = rslt.getVarchar(112) ;
                ((bool[]) buf[208])[0] = rslt.wasNull(112);
                ((bool[]) buf[209])[0] = rslt.getBool(113) ;
                ((bool[]) buf[210])[0] = rslt.wasNull(113);
                ((int[]) buf[211])[0] = rslt.getInt(114) ;
                ((bool[]) buf[212])[0] = rslt.wasNull(114);
                ((String[]) buf[213])[0] = rslt.getString(115, 50) ;
                ((String[]) buf[214])[0] = rslt.getString(116, 15) ;
                ((int[]) buf[215])[0] = rslt.getInt(117) ;
                ((bool[]) buf[216])[0] = rslt.wasNull(117);
                ((String[]) buf[217])[0] = rslt.getString(118, 50) ;
                ((bool[]) buf[218])[0] = rslt.wasNull(118);
                ((bool[]) buf[219])[0] = rslt.getBool(119) ;
                ((bool[]) buf[220])[0] = rslt.wasNull(119);
                ((String[]) buf[221])[0] = rslt.getString(120, 1) ;
                ((bool[]) buf[222])[0] = rslt.wasNull(120);
                ((String[]) buf[223])[0] = rslt.getString(121, 1) ;
                ((bool[]) buf[224])[0] = rslt.wasNull(121);
                ((int[]) buf[225])[0] = rslt.getInt(122) ;
                ((bool[]) buf[226])[0] = rslt.wasNull(122);
                ((String[]) buf[227])[0] = rslt.getString(123, 15) ;
                ((bool[]) buf[228])[0] = rslt.wasNull(123);
                ((int[]) buf[229])[0] = rslt.getInt(124) ;
                ((bool[]) buf[230])[0] = rslt.wasNull(124);
                ((String[]) buf[231])[0] = rslt.getString(125, 3) ;
                ((bool[]) buf[232])[0] = rslt.wasNull(125);
                ((bool[]) buf[233])[0] = rslt.getBool(126) ;
                ((bool[]) buf[234])[0] = rslt.wasNull(126);
                ((bool[]) buf[235])[0] = rslt.getBool(127) ;
                ((bool[]) buf[236])[0] = rslt.wasNull(127);
                ((int[]) buf[237])[0] = rslt.getInt(128) ;
                ((bool[]) buf[238])[0] = rslt.wasNull(128);
                ((String[]) buf[239])[0] = rslt.getString(129, 15) ;
                ((bool[]) buf[240])[0] = rslt.wasNull(129);
                ((int[]) buf[241])[0] = rslt.getInt(130) ;
                ((bool[]) buf[242])[0] = rslt.wasNull(130);
                ((bool[]) buf[243])[0] = rslt.getBool(131) ;
                ((bool[]) buf[244])[0] = rslt.wasNull(131);
                ((String[]) buf[245])[0] = rslt.getLongVarchar(132) ;
                ((bool[]) buf[246])[0] = rslt.wasNull(132);
                ((String[]) buf[247])[0] = rslt.getLongVarchar(133) ;
                ((bool[]) buf[248])[0] = rslt.wasNull(133);
                ((DateTime[]) buf[249])[0] = rslt.getGXDateTime(134) ;
                ((bool[]) buf[250])[0] = rslt.wasNull(134);
                ((int[]) buf[251])[0] = rslt.getInt(135) ;
                ((bool[]) buf[252])[0] = rslt.wasNull(135);
                ((String[]) buf[253])[0] = rslt.getString(136, 100) ;
                ((bool[]) buf[254])[0] = rslt.wasNull(136);
                ((decimal[]) buf[255])[0] = rslt.getDecimal(137) ;
                ((bool[]) buf[256])[0] = rslt.wasNull(137);
                ((int[]) buf[257])[0] = rslt.getInt(138) ;
                ((bool[]) buf[258])[0] = rslt.wasNull(138);
                ((String[]) buf[259])[0] = rslt.getString(139, 10) ;
                ((bool[]) buf[260])[0] = rslt.wasNull(139);
                ((DateTime[]) buf[261])[0] = rslt.getGXDateTime(140) ;
                ((bool[]) buf[262])[0] = rslt.wasNull(140);
                ((int[]) buf[263])[0] = rslt.getInt(141) ;
                ((bool[]) buf[264])[0] = rslt.wasNull(141);
                ((int[]) buf[265])[0] = rslt.getInt(142) ;
                ((bool[]) buf[266])[0] = rslt.wasNull(142);
                ((String[]) buf[267])[0] = rslt.getString(143, 100) ;
                ((bool[]) buf[268])[0] = rslt.wasNull(143);
                ((int[]) buf[269])[0] = rslt.getInt(144) ;
                ((bool[]) buf[270])[0] = rslt.wasNull(144);
                ((decimal[]) buf[271])[0] = rslt.getDecimal(145) ;
                ((bool[]) buf[272])[0] = rslt.wasNull(145);
                ((bool[]) buf[273])[0] = rslt.getBool(146) ;
                ((bool[]) buf[274])[0] = rslt.wasNull(146);
                ((int[]) buf[275])[0] = rslt.getInt(147) ;
                ((bool[]) buf[276])[0] = rslt.wasNull(147);
                ((String[]) buf[277])[0] = rslt.getVarchar(148) ;
                ((bool[]) buf[278])[0] = rslt.wasNull(148);
                ((decimal[]) buf[279])[0] = rslt.getDecimal(149) ;
                ((bool[]) buf[280])[0] = rslt.wasNull(149);
                ((decimal[]) buf[281])[0] = rslt.getDecimal(150) ;
                ((bool[]) buf[282])[0] = rslt.wasNull(150);
                ((decimal[]) buf[283])[0] = rslt.getDecimal(151) ;
                ((bool[]) buf[284])[0] = rslt.wasNull(151);
                ((decimal[]) buf[285])[0] = rslt.getDecimal(152) ;
                ((bool[]) buf[286])[0] = rslt.wasNull(152);
                ((DateTime[]) buf[287])[0] = rslt.getGXDateTime(153) ;
                ((bool[]) buf[288])[0] = rslt.wasNull(153);
                ((int[]) buf[289])[0] = rslt.getInt(154) ;
                ((bool[]) buf[290])[0] = rslt.wasNull(154);
                ((String[]) buf[291])[0] = rslt.getString(155, 15) ;
                ((bool[]) buf[292])[0] = rslt.wasNull(155);
                ((DateTime[]) buf[293])[0] = rslt.getGXDate(156) ;
                ((bool[]) buf[294])[0] = rslt.wasNull(156);
                ((String[]) buf[295])[0] = rslt.getLongVarchar(157) ;
                ((bool[]) buf[296])[0] = rslt.wasNull(157);
                ((decimal[]) buf[297])[0] = rslt.getDecimal(158) ;
                ((bool[]) buf[298])[0] = rslt.wasNull(158);
                ((int[]) buf[299])[0] = rslt.getInt(159) ;
                ((bool[]) buf[300])[0] = rslt.wasNull(159);
                ((int[]) buf[301])[0] = rslt.getInt(160) ;
                ((bool[]) buf[302])[0] = rslt.wasNull(160);
                ((int[]) buf[303])[0] = rslt.getInt(161) ;
                ((bool[]) buf[304])[0] = rslt.wasNull(161);
                ((DateTime[]) buf[305])[0] = rslt.getGXDateTime(162) ;
                ((bool[]) buf[306])[0] = rslt.wasNull(162);
                ((decimal[]) buf[307])[0] = rslt.getDecimal(163) ;
                ((bool[]) buf[308])[0] = rslt.wasNull(163);
                ((decimal[]) buf[309])[0] = rslt.getDecimal(164) ;
                ((bool[]) buf[310])[0] = rslt.wasNull(164);
                ((bool[]) buf[311])[0] = rslt.getBool(165) ;
                ((bool[]) buf[312])[0] = rslt.wasNull(165);
                ((int[]) buf[313])[0] = rslt.getInt(166) ;
                ((bool[]) buf[314])[0] = rslt.wasNull(166);
                ((int[]) buf[315])[0] = rslt.getInt(167) ;
                ((bool[]) buf[316])[0] = rslt.wasNull(167);
                ((int[]) buf[317])[0] = rslt.getInt(168) ;
                ((bool[]) buf[318])[0] = rslt.wasNull(168);
                ((DateTime[]) buf[319])[0] = rslt.getGXDateTime(169) ;
                ((bool[]) buf[320])[0] = rslt.wasNull(169);
                ((DateTime[]) buf[321])[0] = rslt.getGXDateTime(170) ;
                ((bool[]) buf[322])[0] = rslt.wasNull(170);
                ((DateTime[]) buf[323])[0] = rslt.getGXDateTime(171) ;
                ((bool[]) buf[324])[0] = rslt.wasNull(171);
                ((DateTime[]) buf[325])[0] = rslt.getGXDateTime(172) ;
                ((bool[]) buf[326])[0] = rslt.wasNull(172);
                ((DateTime[]) buf[327])[0] = rslt.getGXDateTime(173) ;
                ((bool[]) buf[328])[0] = rslt.wasNull(173);
                ((DateTime[]) buf[329])[0] = rslt.getGXDateTime(174) ;
                ((bool[]) buf[330])[0] = rslt.wasNull(174);
                ((short[]) buf[331])[0] = rslt.getShort(175) ;
                ((bool[]) buf[332])[0] = rslt.wasNull(175);
                ((int[]) buf[333])[0] = rslt.getInt(176) ;
                ((bool[]) buf[334])[0] = rslt.wasNull(176);
                ((String[]) buf[335])[0] = rslt.getString(177, 1) ;
                ((bool[]) buf[336])[0] = rslt.wasNull(177);
                ((String[]) buf[337])[0] = rslt.getVarchar(178) ;
                ((bool[]) buf[338])[0] = rslt.wasNull(178);
                ((decimal[]) buf[339])[0] = rslt.getDecimal(179) ;
                ((bool[]) buf[340])[0] = rslt.wasNull(179);
                ((decimal[]) buf[341])[0] = rslt.getDecimal(180) ;
                ((bool[]) buf[342])[0] = rslt.wasNull(180);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 20) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getString(18, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((String[]) buf[37])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(20);
                ((decimal[]) buf[39])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(21);
                ((String[]) buf[41])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(22);
                ((short[]) buf[43])[0] = rslt.getShort(23) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(23);
                ((short[]) buf[45])[0] = rslt.getShort(24) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(24);
                ((decimal[]) buf[47])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[49])[0] = rslt.getGXDate(26) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(26);
                ((String[]) buf[51])[0] = rslt.getString(27, 50) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(27);
                ((short[]) buf[53])[0] = rslt.getShort(28) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(28);
                ((int[]) buf[55])[0] = rslt.getInt(29) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(29);
                ((String[]) buf[57])[0] = rslt.getString(30, 15) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(30);
                ((int[]) buf[59])[0] = rslt.getInt(31) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(31);
                ((int[]) buf[61])[0] = rslt.getInt(32) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(32);
                ((int[]) buf[63])[0] = rslt.getInt(33) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(33);
                ((int[]) buf[65])[0] = rslt.getInt(34) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(34);
                ((int[]) buf[67])[0] = rslt.getInt(35) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(35);
                ((short[]) buf[69])[0] = rslt.getShort(36) ;
                ((int[]) buf[70])[0] = rslt.getInt(37) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(37);
                ((String[]) buf[72])[0] = rslt.getVarchar(38) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(38);
                ((String[]) buf[74])[0] = rslt.getVarchar(39) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(39);
                ((String[]) buf[76])[0] = rslt.getVarchar(40) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(40);
                ((short[]) buf[78])[0] = rslt.getShort(41) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(41);
                ((bool[]) buf[80])[0] = rslt.getBool(42) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(42);
                ((short[]) buf[82])[0] = rslt.getShort(43) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(43);
                ((bool[]) buf[84])[0] = rslt.getBool(44) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(44);
                ((decimal[]) buf[86])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(45);
                ((decimal[]) buf[88])[0] = rslt.getDecimal(46) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(46);
                ((DateTime[]) buf[90])[0] = rslt.getGXDate(47) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(47);
                ((decimal[]) buf[92])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(48);
                ((short[]) buf[94])[0] = rslt.getShort(49) ;
                ((DateTime[]) buf[95])[0] = rslt.getGXDate(50) ;
                ((String[]) buf[96])[0] = rslt.getString(51, 5) ;
                ((int[]) buf[97])[0] = rslt.getInt(52) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(52);
                ((int[]) buf[99])[0] = rslt.getInt(53) ;
                ((decimal[]) buf[100])[0] = rslt.getDecimal(54) ;
                ((decimal[]) buf[101])[0] = rslt.getDecimal(55) ;
                ((decimal[]) buf[102])[0] = rslt.getDecimal(56) ;
                ((decimal[]) buf[103])[0] = rslt.getDecimal(57) ;
                ((decimal[]) buf[104])[0] = rslt.getDecimal(58) ;
                ((decimal[]) buf[105])[0] = rslt.getDecimal(59) ;
                ((int[]) buf[106])[0] = rslt.getInt(60) ;
                ((bool[]) buf[107])[0] = rslt.wasNull(60);
                ((String[]) buf[108])[0] = rslt.getVarchar(61) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                ((int[]) buf[110])[0] = rslt.getInt(62) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(62);
                ((short[]) buf[112])[0] = rslt.getShort(63) ;
                ((bool[]) buf[113])[0] = rslt.wasNull(63);
                ((String[]) buf[114])[0] = rslt.getString(64, 20) ;
                ((bool[]) buf[115])[0] = rslt.wasNull(64);
                ((DateTime[]) buf[116])[0] = rslt.getGXDate(65) ;
                ((bool[]) buf[117])[0] = rslt.wasNull(65);
                ((decimal[]) buf[118])[0] = rslt.getDecimal(66) ;
                ((bool[]) buf[119])[0] = rslt.wasNull(66);
                ((bool[]) buf[120])[0] = rslt.getBool(67) ;
                ((bool[]) buf[121])[0] = rslt.wasNull(67);
                ((int[]) buf[122])[0] = rslt.getInt(68) ;
                ((bool[]) buf[123])[0] = rslt.wasNull(68);
                ((int[]) buf[124])[0] = rslt.getInt(69) ;
                ((bool[]) buf[125])[0] = rslt.wasNull(69);
                ((decimal[]) buf[126])[0] = rslt.getDecimal(70) ;
                ((bool[]) buf[127])[0] = rslt.wasNull(70);
                ((int[]) buf[128])[0] = rslt.getInt(71) ;
                ((bool[]) buf[129])[0] = rslt.wasNull(71);
                ((int[]) buf[130])[0] = rslt.getInt(72) ;
                ((bool[]) buf[131])[0] = rslt.wasNull(72);
                ((int[]) buf[132])[0] = rslt.getInt(73) ;
                ((int[]) buf[133])[0] = rslt.getInt(74) ;
                ((String[]) buf[134])[0] = rslt.getVarchar(75) ;
                ((bool[]) buf[135])[0] = rslt.wasNull(75);
                ((String[]) buf[136])[0] = rslt.getString(76, 15) ;
                ((bool[]) buf[137])[0] = rslt.wasNull(76);
                ((String[]) buf[138])[0] = rslt.getString(77, 15) ;
                ((bool[]) buf[139])[0] = rslt.wasNull(77);
                ((String[]) buf[140])[0] = rslt.getVarchar(78) ;
                ((bool[]) buf[141])[0] = rslt.wasNull(78);
                ((int[]) buf[142])[0] = rslt.getInt(79) ;
                ((bool[]) buf[143])[0] = rslt.wasNull(79);
                ((int[]) buf[144])[0] = rslt.getInt(80) ;
                ((bool[]) buf[145])[0] = rslt.wasNull(80);
                ((DateTime[]) buf[146])[0] = rslt.getGXDate(81) ;
                ((DateTime[]) buf[147])[0] = rslt.getGXDate(82) ;
                ((bool[]) buf[148])[0] = rslt.wasNull(82);
                ((DateTime[]) buf[149])[0] = rslt.getGXDate(83) ;
                ((bool[]) buf[150])[0] = rslt.wasNull(83);
                ((DateTime[]) buf[151])[0] = rslt.getGXDateTime(84) ;
                ((bool[]) buf[152])[0] = rslt.wasNull(84);
                ((DateTime[]) buf[153])[0] = rslt.getGXDateTime(85) ;
                ((bool[]) buf[154])[0] = rslt.wasNull(85);
                ((DateTime[]) buf[155])[0] = rslt.getGXDateTime(86) ;
                ((bool[]) buf[156])[0] = rslt.wasNull(86);
                ((DateTime[]) buf[157])[0] = rslt.getGXDateTime(87) ;
                ((bool[]) buf[158])[0] = rslt.wasNull(87);
                ((DateTime[]) buf[159])[0] = rslt.getGXDateTime(88) ;
                ((bool[]) buf[160])[0] = rslt.wasNull(88);
                ((short[]) buf[161])[0] = rslt.getShort(89) ;
                ((bool[]) buf[162])[0] = rslt.wasNull(89);
                ((short[]) buf[163])[0] = rslt.getShort(90) ;
                ((bool[]) buf[164])[0] = rslt.wasNull(90);
                ((int[]) buf[165])[0] = rslt.getInt(91) ;
                ((bool[]) buf[166])[0] = rslt.wasNull(91);
                ((String[]) buf[167])[0] = rslt.getString(92, 100) ;
                ((bool[]) buf[168])[0] = rslt.wasNull(92);
                ((String[]) buf[169])[0] = rslt.getString(93, 1) ;
                ((bool[]) buf[170])[0] = rslt.wasNull(93);
                ((String[]) buf[171])[0] = rslt.getVarchar(94) ;
                ((bool[]) buf[172])[0] = rslt.wasNull(94);
                ((int[]) buf[173])[0] = rslt.getInt(95) ;
                ((bool[]) buf[174])[0] = rslt.wasNull(95);
                ((String[]) buf[175])[0] = rslt.getString(96, 15) ;
                ((bool[]) buf[176])[0] = rslt.wasNull(96);
                ((int[]) buf[177])[0] = rslt.getInt(97) ;
                ((bool[]) buf[178])[0] = rslt.wasNull(97);
                ((String[]) buf[179])[0] = rslt.getString(98, 100) ;
                ((bool[]) buf[180])[0] = rslt.wasNull(98);
                ((int[]) buf[181])[0] = rslt.getInt(99) ;
                ((bool[]) buf[182])[0] = rslt.wasNull(99);
                ((bool[]) buf[183])[0] = rslt.getBool(100) ;
                ((bool[]) buf[184])[0] = rslt.wasNull(100);
                ((int[]) buf[185])[0] = rslt.getInt(101) ;
                ((bool[]) buf[186])[0] = rslt.wasNull(101);
                ((int[]) buf[187])[0] = rslt.getInt(102) ;
                ((bool[]) buf[188])[0] = rslt.wasNull(102);
                ((String[]) buf[189])[0] = rslt.getString(103, 100) ;
                ((bool[]) buf[190])[0] = rslt.wasNull(103);
                ((int[]) buf[191])[0] = rslt.getInt(104) ;
                ((bool[]) buf[192])[0] = rslt.wasNull(104);
                ((bool[]) buf[193])[0] = rslt.getBool(105) ;
                ((bool[]) buf[194])[0] = rslt.wasNull(105);
                ((String[]) buf[195])[0] = rslt.getVarchar(106) ;
                ((bool[]) buf[196])[0] = rslt.wasNull(106);
                ((String[]) buf[197])[0] = rslt.getLongVarchar(107) ;
                ((bool[]) buf[198])[0] = rslt.wasNull(107);
                ((int[]) buf[199])[0] = rslt.getInt(108) ;
                ((bool[]) buf[200])[0] = rslt.wasNull(108);
                ((String[]) buf[201])[0] = rslt.getVarchar(109) ;
                ((bool[]) buf[202])[0] = rslt.wasNull(109);
                ((int[]) buf[203])[0] = rslt.getInt(110) ;
                ((bool[]) buf[204])[0] = rslt.wasNull(110);
                ((String[]) buf[205])[0] = rslt.getString(111, 25) ;
                ((bool[]) buf[206])[0] = rslt.wasNull(111);
                ((String[]) buf[207])[0] = rslt.getVarchar(112) ;
                ((bool[]) buf[208])[0] = rslt.wasNull(112);
                ((bool[]) buf[209])[0] = rslt.getBool(113) ;
                ((bool[]) buf[210])[0] = rslt.wasNull(113);
                ((int[]) buf[211])[0] = rslt.getInt(114) ;
                ((bool[]) buf[212])[0] = rslt.wasNull(114);
                ((String[]) buf[213])[0] = rslt.getString(115, 50) ;
                ((String[]) buf[214])[0] = rslt.getString(116, 15) ;
                ((int[]) buf[215])[0] = rslt.getInt(117) ;
                ((bool[]) buf[216])[0] = rslt.wasNull(117);
                ((String[]) buf[217])[0] = rslt.getString(118, 50) ;
                ((bool[]) buf[218])[0] = rslt.wasNull(118);
                ((bool[]) buf[219])[0] = rslt.getBool(119) ;
                ((bool[]) buf[220])[0] = rslt.wasNull(119);
                ((String[]) buf[221])[0] = rslt.getString(120, 1) ;
                ((bool[]) buf[222])[0] = rslt.wasNull(120);
                ((String[]) buf[223])[0] = rslt.getString(121, 1) ;
                ((bool[]) buf[224])[0] = rslt.wasNull(121);
                ((int[]) buf[225])[0] = rslt.getInt(122) ;
                ((bool[]) buf[226])[0] = rslt.wasNull(122);
                ((String[]) buf[227])[0] = rslt.getString(123, 15) ;
                ((bool[]) buf[228])[0] = rslt.wasNull(123);
                ((int[]) buf[229])[0] = rslt.getInt(124) ;
                ((bool[]) buf[230])[0] = rslt.wasNull(124);
                ((String[]) buf[231])[0] = rslt.getString(125, 3) ;
                ((bool[]) buf[232])[0] = rslt.wasNull(125);
                ((bool[]) buf[233])[0] = rslt.getBool(126) ;
                ((bool[]) buf[234])[0] = rslt.wasNull(126);
                ((bool[]) buf[235])[0] = rslt.getBool(127) ;
                ((bool[]) buf[236])[0] = rslt.wasNull(127);
                ((int[]) buf[237])[0] = rslt.getInt(128) ;
                ((bool[]) buf[238])[0] = rslt.wasNull(128);
                ((String[]) buf[239])[0] = rslt.getString(129, 15) ;
                ((bool[]) buf[240])[0] = rslt.wasNull(129);
                ((int[]) buf[241])[0] = rslt.getInt(130) ;
                ((bool[]) buf[242])[0] = rslt.wasNull(130);
                ((bool[]) buf[243])[0] = rslt.getBool(131) ;
                ((bool[]) buf[244])[0] = rslt.wasNull(131);
                ((String[]) buf[245])[0] = rslt.getLongVarchar(132) ;
                ((bool[]) buf[246])[0] = rslt.wasNull(132);
                ((String[]) buf[247])[0] = rslt.getLongVarchar(133) ;
                ((bool[]) buf[248])[0] = rslt.wasNull(133);
                ((DateTime[]) buf[249])[0] = rslt.getGXDateTime(134) ;
                ((bool[]) buf[250])[0] = rslt.wasNull(134);
                ((int[]) buf[251])[0] = rslt.getInt(135) ;
                ((bool[]) buf[252])[0] = rslt.wasNull(135);
                ((String[]) buf[253])[0] = rslt.getString(136, 100) ;
                ((bool[]) buf[254])[0] = rslt.wasNull(136);
                ((decimal[]) buf[255])[0] = rslt.getDecimal(137) ;
                ((bool[]) buf[256])[0] = rslt.wasNull(137);
                ((int[]) buf[257])[0] = rslt.getInt(138) ;
                ((bool[]) buf[258])[0] = rslt.wasNull(138);
                ((String[]) buf[259])[0] = rslt.getString(139, 10) ;
                ((bool[]) buf[260])[0] = rslt.wasNull(139);
                ((DateTime[]) buf[261])[0] = rslt.getGXDateTime(140) ;
                ((bool[]) buf[262])[0] = rslt.wasNull(140);
                ((int[]) buf[263])[0] = rslt.getInt(141) ;
                ((bool[]) buf[264])[0] = rslt.wasNull(141);
                ((int[]) buf[265])[0] = rslt.getInt(142) ;
                ((bool[]) buf[266])[0] = rslt.wasNull(142);
                ((String[]) buf[267])[0] = rslt.getString(143, 100) ;
                ((bool[]) buf[268])[0] = rslt.wasNull(143);
                ((int[]) buf[269])[0] = rslt.getInt(144) ;
                ((bool[]) buf[270])[0] = rslt.wasNull(144);
                ((decimal[]) buf[271])[0] = rslt.getDecimal(145) ;
                ((bool[]) buf[272])[0] = rslt.wasNull(145);
                ((bool[]) buf[273])[0] = rslt.getBool(146) ;
                ((bool[]) buf[274])[0] = rslt.wasNull(146);
                ((int[]) buf[275])[0] = rslt.getInt(147) ;
                ((bool[]) buf[276])[0] = rslt.wasNull(147);
                ((String[]) buf[277])[0] = rslt.getVarchar(148) ;
                ((bool[]) buf[278])[0] = rslt.wasNull(148);
                ((decimal[]) buf[279])[0] = rslt.getDecimal(149) ;
                ((bool[]) buf[280])[0] = rslt.wasNull(149);
                ((decimal[]) buf[281])[0] = rslt.getDecimal(150) ;
                ((bool[]) buf[282])[0] = rslt.wasNull(150);
                ((decimal[]) buf[283])[0] = rslt.getDecimal(151) ;
                ((bool[]) buf[284])[0] = rslt.wasNull(151);
                ((decimal[]) buf[285])[0] = rslt.getDecimal(152) ;
                ((bool[]) buf[286])[0] = rslt.wasNull(152);
                ((DateTime[]) buf[287])[0] = rslt.getGXDateTime(153) ;
                ((bool[]) buf[288])[0] = rslt.wasNull(153);
                ((int[]) buf[289])[0] = rslt.getInt(154) ;
                ((bool[]) buf[290])[0] = rslt.wasNull(154);
                ((String[]) buf[291])[0] = rslt.getString(155, 15) ;
                ((bool[]) buf[292])[0] = rslt.wasNull(155);
                ((DateTime[]) buf[293])[0] = rslt.getGXDate(156) ;
                ((bool[]) buf[294])[0] = rslt.wasNull(156);
                ((String[]) buf[295])[0] = rslt.getLongVarchar(157) ;
                ((bool[]) buf[296])[0] = rslt.wasNull(157);
                ((decimal[]) buf[297])[0] = rslt.getDecimal(158) ;
                ((bool[]) buf[298])[0] = rslt.wasNull(158);
                ((int[]) buf[299])[0] = rslt.getInt(159) ;
                ((bool[]) buf[300])[0] = rslt.wasNull(159);
                ((int[]) buf[301])[0] = rslt.getInt(160) ;
                ((bool[]) buf[302])[0] = rslt.wasNull(160);
                ((int[]) buf[303])[0] = rslt.getInt(161) ;
                ((bool[]) buf[304])[0] = rslt.wasNull(161);
                ((DateTime[]) buf[305])[0] = rslt.getGXDateTime(162) ;
                ((bool[]) buf[306])[0] = rslt.wasNull(162);
                ((decimal[]) buf[307])[0] = rslt.getDecimal(163) ;
                ((bool[]) buf[308])[0] = rslt.wasNull(163);
                ((decimal[]) buf[309])[0] = rslt.getDecimal(164) ;
                ((bool[]) buf[310])[0] = rslt.wasNull(164);
                ((bool[]) buf[311])[0] = rslt.getBool(165) ;
                ((bool[]) buf[312])[0] = rslt.wasNull(165);
                ((int[]) buf[313])[0] = rslt.getInt(166) ;
                ((bool[]) buf[314])[0] = rslt.wasNull(166);
                ((int[]) buf[315])[0] = rslt.getInt(167) ;
                ((bool[]) buf[316])[0] = rslt.wasNull(167);
                ((int[]) buf[317])[0] = rslt.getInt(168) ;
                ((bool[]) buf[318])[0] = rslt.wasNull(168);
                ((DateTime[]) buf[319])[0] = rslt.getGXDateTime(169) ;
                ((bool[]) buf[320])[0] = rslt.wasNull(169);
                ((DateTime[]) buf[321])[0] = rslt.getGXDateTime(170) ;
                ((bool[]) buf[322])[0] = rslt.wasNull(170);
                ((DateTime[]) buf[323])[0] = rslt.getGXDateTime(171) ;
                ((bool[]) buf[324])[0] = rslt.wasNull(171);
                ((DateTime[]) buf[325])[0] = rslt.getGXDateTime(172) ;
                ((bool[]) buf[326])[0] = rslt.wasNull(172);
                ((DateTime[]) buf[327])[0] = rslt.getGXDateTime(173) ;
                ((bool[]) buf[328])[0] = rslt.wasNull(173);
                ((DateTime[]) buf[329])[0] = rslt.getGXDateTime(174) ;
                ((bool[]) buf[330])[0] = rslt.wasNull(174);
                ((short[]) buf[331])[0] = rslt.getShort(175) ;
                ((bool[]) buf[332])[0] = rslt.wasNull(175);
                ((int[]) buf[333])[0] = rslt.getInt(176) ;
                ((bool[]) buf[334])[0] = rslt.wasNull(176);
                ((String[]) buf[335])[0] = rslt.getString(177, 1) ;
                ((bool[]) buf[336])[0] = rslt.wasNull(177);
                ((String[]) buf[337])[0] = rslt.getVarchar(178) ;
                ((bool[]) buf[338])[0] = rslt.wasNull(178);
                ((decimal[]) buf[339])[0] = rslt.getDecimal(179) ;
                ((bool[]) buf[340])[0] = rslt.wasNull(179);
                ((decimal[]) buf[341])[0] = rslt.getDecimal(180) ;
                ((bool[]) buf[342])[0] = rslt.wasNull(180);
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
