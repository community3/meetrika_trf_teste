/*
               File: WWServico
        Description:  Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:34:47.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwservico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavServicogrupo_codigo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavServicogrupo_codigo2 = new GXCombobox();
         cmbServico_ObjetoControle = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_65 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_65_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_65_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV62Servico_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Servico_Sigla1", AV62Servico_Sigla1);
               AV63Servico_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Servico_Nome1", AV63Servico_Nome1);
               AV64ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV65Servico_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Servico_Sigla2", AV65Servico_Sigla2);
               AV66Servico_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Servico_Nome2", AV66Servico_Nome2);
               AV67ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV73TFServico_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFServico_Nome", AV73TFServico_Nome);
               AV74TFServico_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFServico_Nome_Sel", AV74TFServico_Nome_Sel);
               AV77TFServico_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFServico_Sigla", AV77TFServico_Sigla);
               AV78TFServico_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFServico_Sigla_Sel", AV78TFServico_Sigla_Sel);
               AV81TFServicoGrupo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFServicoGrupo_Descricao", AV81TFServicoGrupo_Descricao);
               AV82TFServicoGrupo_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFServicoGrupo_Descricao_Sel", AV82TFServicoGrupo_Descricao_Sel);
               AV75ddo_Servico_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Servico_NomeTitleControlIdToReplace", AV75ddo_Servico_NomeTitleControlIdToReplace);
               AV79ddo_Servico_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_Servico_SiglaTitleControlIdToReplace", AV79ddo_Servico_SiglaTitleControlIdToReplace);
               AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
               AV87ddo_Servico_ObjetoControleTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_Servico_ObjetoControleTitleControlIdToReplace", AV87ddo_Servico_ObjetoControleTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV86TFServico_ObjetoControle_Sels);
               AV115Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV68ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ServicoGrupo_Codigo), 6, 0)));
               A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A1551Servico_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA6Y2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START6Y2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299344757");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwservico.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_SIGLA1", StringUtil.RTrim( AV62Servico_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME1", StringUtil.RTrim( AV63Servico_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_SIGLA2", StringUtil.RTrim( AV65Servico_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME2", StringUtil.RTrim( AV66Servico_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOGRUPO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME", StringUtil.RTrim( AV73TFServico_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME_SEL", StringUtil.RTrim( AV74TFServico_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_SIGLA", StringUtil.RTrim( AV77TFServico_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_SIGLA_SEL", StringUtil.RTrim( AV78TFServico_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_DESCRICAO", AV81TFServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL", AV82TFServicoGrupo_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_65", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_65), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV88DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV88DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_NOMETITLEFILTERDATA", AV72Servico_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_NOMETITLEFILTERDATA", AV72Servico_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_SIGLATITLEFILTERDATA", AV76Servico_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_SIGLATITLEFILTERDATA", AV76Servico_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA", AV80ServicoGrupo_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA", AV80ServicoGrupo_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_OBJETOCONTROLETITLEFILTERDATA", AV84Servico_ObjetoControleTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_OBJETOCONTROLETITLEFILTERDATA", AV84Servico_ObjetoControleTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFSERVICO_OBJETOCONTROLE_SELS", AV86TFServico_ObjetoControle_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFSERVICO_OBJETOCONTROLE_SELS", AV86TFServico_ObjetoControle_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV115Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Caption", StringUtil.RTrim( Ddo_servico_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Tooltip", StringUtil.RTrim( Ddo_servico_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cls", StringUtil.RTrim( Ddo_servico_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servico_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servico_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filtertype", StringUtil.RTrim( Ddo_servico_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servico_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servico_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalisttype", StringUtil.RTrim( Ddo_servico_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistproc", StringUtil.RTrim( Ddo_servico_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortasc", StringUtil.RTrim( Ddo_servico_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortdsc", StringUtil.RTrim( Ddo_servico_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Loadingdata", StringUtil.RTrim( Ddo_servico_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servico_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servico_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servico_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Caption", StringUtil.RTrim( Ddo_servico_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_servico_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Cls", StringUtil.RTrim( Ddo_servico_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_servico_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_servico_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_servico_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_servico_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_servico_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_servico_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_servico_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_servico_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_servico_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_servico_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_servico_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_servico_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_servico_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_servico_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_servico_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_servicogrupo_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_servicogrupo_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_servicogrupo_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_servicogrupo_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicogrupo_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_servicogrupo_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_servicogrupo_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_servicogrupo_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_servicogrupo_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_servicogrupo_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_servicogrupo_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Caption", StringUtil.RTrim( Ddo_servico_objetocontrole_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Tooltip", StringUtil.RTrim( Ddo_servico_objetocontrole_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Cls", StringUtil.RTrim( Ddo_servico_objetocontrole_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_objetocontrole_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_objetocontrole_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_objetocontrole_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Includesortasc", StringUtil.BoolToStr( Ddo_servico_objetocontrole_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_objetocontrole_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Sortedstatus", StringUtil.RTrim( Ddo_servico_objetocontrole_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Includefilter", StringUtil.BoolToStr( Ddo_servico_objetocontrole_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Includedatalist", StringUtil.BoolToStr( Ddo_servico_objetocontrole_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Datalisttype", StringUtil.RTrim( Ddo_servico_objetocontrole_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_servico_objetocontrole_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Datalistfixedvalues", StringUtil.RTrim( Ddo_servico_objetocontrole_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Sortasc", StringUtil.RTrim( Ddo_servico_objetocontrole_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Sortdsc", StringUtil.RTrim( Ddo_servico_objetocontrole_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Cleanfilter", StringUtil.RTrim( Ddo_servico_objetocontrole_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Searchbuttontext", StringUtil.RTrim( Ddo_servico_objetocontrole_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servico_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_servico_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_servico_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_servicogrupo_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_servicogrupo_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_servicogrupo_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Activeeventkey", StringUtil.RTrim( Ddo_servico_objetocontrole_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_OBJETOCONTROLE_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_objetocontrole_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE6Y2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT6Y2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwservico.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWServico" ;
      }

      public override String GetPgmdesc( )
      {
         return " Servico" ;
      }

      protected void WB6Y0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_6Y2( true) ;
         }
         else
         {
            wb_table1_2_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_Internalname, StringUtil.RTrim( AV73TFServico_Nome), StringUtil.RTrim( context.localUtil.Format( AV73TFServico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_sel_Internalname, StringUtil.RTrim( AV74TFServico_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV74TFServico_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_sigla_Internalname, StringUtil.RTrim( AV77TFServico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV77TFServico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_sigla_sel_Internalname, StringUtil.RTrim( AV78TFServico_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV78TFServico_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_descricao_Internalname, AV81TFServicoGrupo_Descricao, StringUtil.RTrim( context.localUtil.Format( AV81TFServicoGrupo_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_descricao_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicogrupo_descricao_sel_Internalname, AV82TFServicoGrupo_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV82TFServicoGrupo_Descricao_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicogrupo_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicogrupo_descricao_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, AV75ddo_Servico_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", 0, edtavDdo_servico_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname, AV79ddo_Servico_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_servico_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOGRUPO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_OBJETOCONTROLEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Internalname, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServico.htm");
         }
         wbLoad = true;
      }

      protected void START6Y2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Servico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6Y0( ) ;
      }

      protected void WS6Y2( )
      {
         START6Y2( ) ;
         EVT6Y2( ) ;
      }

      protected void EVT6Y2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E116Y2 */
                              E116Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E126Y2 */
                              E126Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E136Y2 */
                              E136Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOGRUPO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E146Y2 */
                              E146Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_OBJETOCONTROLE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E156Y2 */
                              E156Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E166Y2 */
                              E166Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E176Y2 */
                              E176Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E186Y2 */
                              E186Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E196Y2 */
                              E196Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E206Y2 */
                              E206Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E216Y2 */
                              E216Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E226Y2 */
                              E226Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E236Y2 */
                              E236Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_65_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
                              SubsflControlProps_652( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV111Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV112Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV40Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV40Display)) ? AV113Display_GXI : context.convertURL( context.PathToRelativeUrl( AV40Display))));
                              A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                              A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
                              A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                              A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
                              A158ServicoGrupo_Descricao = cgiGet( edtServicoGrupo_Descricao_Internalname);
                              AV92Nome = StringUtil.Upper( cgiGet( edtavNome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV92Nome);
                              cmbServico_ObjetoControle.Name = cmbServico_ObjetoControle_Internalname;
                              cmbServico_ObjetoControle.CurrentValue = cgiGet( cmbServico_ObjetoControle_Internalname);
                              A1436Servico_ObjetoControle = cgiGet( cmbServico_ObjetoControle_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E246Y2 */
                                    E246Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E256Y2 */
                                    E256Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E266Y2 */
                                    E266Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_sigla1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA1"), AV62Servico_Sigla1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV63Servico_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicogrupo_codigo1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV64ServicoGrupo_Codigo1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_sigla2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA2"), AV65Servico_Sigla2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV66Servico_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicogrupo_codigo2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV67ServicoGrupo_Codigo2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservico_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV73TFServico_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservico_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV74TFServico_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservico_sigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA"), AV77TFServico_Sigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservico_sigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA_SEL"), AV78TFServico_Sigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicogrupo_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO"), AV81TFServicoGrupo_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicogrupo_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL"), AV82TFServicoGrupo_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6Y2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA6Y2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICOGRUPO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavServicogrupo_codigo1.Name = "vSERVICOGRUPO_CODIGO1";
            dynavServicogrupo_codigo1.WebTags = "";
            dynavServicogrupo_codigo1.removeAllItems();
            /* Using cursor H006Y2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavServicogrupo_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H006Y2_A157ServicoGrupo_Codigo[0]), 6, 0)), H006Y2_A158ServicoGrupo_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavServicogrupo_codigo1.ItemCount > 0 )
            {
               AV64ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( dynavServicogrupo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("SERVICO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("SERVICOGRUPO_CODIGO", "Grupo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            dynavServicogrupo_codigo2.Name = "vSERVICOGRUPO_CODIGO2";
            dynavServicogrupo_codigo2.WebTags = "";
            dynavServicogrupo_codigo2.removeAllItems();
            /* Using cursor H006Y3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               dynavServicogrupo_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H006Y3_A157ServicoGrupo_Codigo[0]), 6, 0)), H006Y3_A158ServicoGrupo_Descricao[0], 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( dynavServicogrupo_codigo2.ItemCount > 0 )
            {
               AV67ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( dynavServicogrupo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)));
            }
            GXCCtl = "SERVICO_OBJETOCONTROLE_" + sGXsfl_65_idx;
            cmbServico_ObjetoControle.Name = GXCCtl;
            cmbServico_ObjetoControle.WebTags = "";
            cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
            cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
            cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
            if ( cmbServico_ObjetoControle.ItemCount > 0 )
            {
               A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO26Y1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOGRUPO_CODIGO2_data6Y1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOGRUPO_CODIGO2_html6Y1( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOGRUPO_CODIGO2_data6Y1( ) ;
         gxdynajaxindex = 1;
         dynavServicogrupo_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicogrupo_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServicogrupo_codigo2.ItemCount > 0 )
         {
            AV67ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( dynavServicogrupo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)));
         }
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO2_data6Y1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H006Y4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H006Y4_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H006Y4_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO16Y1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOGRUPO_CODIGO1_data6Y1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOGRUPO_CODIGO1_html6Y1( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOGRUPO_CODIGO1_data6Y1( ) ;
         gxdynajaxindex = 1;
         dynavServicogrupo_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicogrupo_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServicogrupo_codigo1.ItemCount > 0 )
         {
            AV64ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( dynavServicogrupo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)));
         }
      }

      protected void GXDLVvSERVICOGRUPO_CODIGO1_data6Y1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H006Y5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H006Y5_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H006Y5_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_652( ) ;
         while ( nGXsfl_65_idx <= nRC_GXsfl_65 )
         {
            sendrow_652( ) ;
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV62Servico_Sigla1 ,
                                       String AV63Servico_Nome1 ,
                                       int AV64ServicoGrupo_Codigo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV65Servico_Sigla2 ,
                                       String AV66Servico_Nome2 ,
                                       int AV67ServicoGrupo_Codigo2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV73TFServico_Nome ,
                                       String AV74TFServico_Nome_Sel ,
                                       String AV77TFServico_Sigla ,
                                       String AV78TFServico_Sigla_Sel ,
                                       String AV81TFServicoGrupo_Descricao ,
                                       String AV82TFServicoGrupo_Descricao_Sel ,
                                       String AV75ddo_Servico_NomeTitleControlIdToReplace ,
                                       String AV79ddo_Servico_SiglaTitleControlIdToReplace ,
                                       String AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace ,
                                       String AV87ddo_Servico_ObjetoControleTitleControlIdToReplace ,
                                       IGxCollection AV86TFServico_ObjetoControle_Sels ,
                                       String AV115Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A155Servico_Codigo ,
                                       int AV68ServicoGrupo_Codigo ,
                                       int A157ServicoGrupo_Codigo ,
                                       int A1Usuario_Codigo ,
                                       int A1551Servico_Responsavel ,
                                       String A58Usuario_PessoaNom )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6Y2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
         GxWebStd.gx_hidden_field( context, "SERVICO_OBJETOCONTROLE", StringUtil.RTrim( A1436Servico_ObjetoControle));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavServicogrupo_codigo1.ItemCount > 0 )
         {
            AV64ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( dynavServicogrupo_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( dynavServicogrupo_codigo2.ItemCount > 0 )
         {
            AV67ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( dynavServicogrupo_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6Y2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV115Pgmname = "WWServico";
         context.Gx_err = 0;
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
      }

      protected void RF6Y2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 65;
         /* Execute user event: E256Y2 */
         E256Y2 ();
         nGXsfl_65_idx = 1;
         sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
         SubsflControlProps_652( ) ;
         nGXsfl_65_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_652( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(4, new Object[]{ new Object[]{
                                                 A1436Servico_ObjetoControle ,
                                                 AV110WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                                 AV95WWServicoDS_1_Dynamicfiltersselector1 ,
                                                 AV96WWServicoDS_2_Servico_sigla1 ,
                                                 AV97WWServicoDS_3_Servico_nome1 ,
                                                 AV98WWServicoDS_4_Servicogrupo_codigo1 ,
                                                 AV99WWServicoDS_5_Dynamicfiltersenabled2 ,
                                                 AV100WWServicoDS_6_Dynamicfiltersselector2 ,
                                                 AV101WWServicoDS_7_Servico_sigla2 ,
                                                 AV102WWServicoDS_8_Servico_nome2 ,
                                                 AV103WWServicoDS_9_Servicogrupo_codigo2 ,
                                                 AV105WWServicoDS_11_Tfservico_nome_sel ,
                                                 AV104WWServicoDS_10_Tfservico_nome ,
                                                 AV107WWServicoDS_13_Tfservico_sigla_sel ,
                                                 AV106WWServicoDS_12_Tfservico_sigla ,
                                                 AV109WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                                 AV108WWServicoDS_14_Tfservicogrupo_descricao ,
                                                 AV110WWServicoDS_16_Tfservico_objetocontrole_sels.Count ,
                                                 A605Servico_Sigla ,
                                                 A608Servico_Nome ,
                                                 A157ServicoGrupo_Codigo ,
                                                 A159ServicoGrupo_Ativo ,
                                                 A158ServicoGrupo_Descricao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV97WWServicoDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV97WWServicoDS_3_Servico_nome1), 50, "%");
            lV102WWServicoDS_8_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV102WWServicoDS_8_Servico_nome2), 50, "%");
            lV104WWServicoDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV104WWServicoDS_10_Tfservico_nome), 50, "%");
            lV106WWServicoDS_12_Tfservico_sigla = StringUtil.PadR( StringUtil.RTrim( AV106WWServicoDS_12_Tfservico_sigla), 15, "%");
            lV108WWServicoDS_14_Tfservicogrupo_descricao = StringUtil.Concat( StringUtil.RTrim( AV108WWServicoDS_14_Tfservicogrupo_descricao), "%", "");
            /* Using cursor H006Y6 */
            pr_default.execute(4, new Object[] {AV96WWServicoDS_2_Servico_sigla1, lV97WWServicoDS_3_Servico_nome1, AV98WWServicoDS_4_Servicogrupo_codigo1, AV101WWServicoDS_7_Servico_sigla2, lV102WWServicoDS_8_Servico_nome2, AV103WWServicoDS_9_Servicogrupo_codigo2, lV104WWServicoDS_10_Tfservico_nome, AV105WWServicoDS_11_Tfservico_nome_sel, lV106WWServicoDS_12_Tfservico_sigla, AV107WWServicoDS_13_Tfservico_sigla_sel, lV108WWServicoDS_14_Tfservicogrupo_descricao, AV109WWServicoDS_15_Tfservicogrupo_descricao_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_65_idx = 1;
            while ( ( (pr_default.getStatus(4) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A159ServicoGrupo_Ativo = H006Y6_A159ServicoGrupo_Ativo[0];
               A1436Servico_ObjetoControle = H006Y6_A1436Servico_ObjetoControle[0];
               A158ServicoGrupo_Descricao = H006Y6_A158ServicoGrupo_Descricao[0];
               A605Servico_Sigla = H006Y6_A605Servico_Sigla[0];
               A608Servico_Nome = H006Y6_A608Servico_Nome[0];
               A157ServicoGrupo_Codigo = H006Y6_A157ServicoGrupo_Codigo[0];
               A155Servico_Codigo = H006Y6_A155Servico_Codigo[0];
               A159ServicoGrupo_Ativo = H006Y6_A159ServicoGrupo_Ativo[0];
               A158ServicoGrupo_Descricao = H006Y6_A158ServicoGrupo_Descricao[0];
               GXt_int1 = A1551Servico_Responsavel;
               new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
               A1551Servico_Responsavel = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
               /* Execute user event: E266Y2 */
               E266Y2 ();
               pr_default.readNext(4);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(4) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(4);
            wbEnd = 65;
            WB6Y0( ) ;
         }
         nGXsfl_65_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A1436Servico_ObjetoControle ,
                                              AV110WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                              AV95WWServicoDS_1_Dynamicfiltersselector1 ,
                                              AV96WWServicoDS_2_Servico_sigla1 ,
                                              AV97WWServicoDS_3_Servico_nome1 ,
                                              AV98WWServicoDS_4_Servicogrupo_codigo1 ,
                                              AV99WWServicoDS_5_Dynamicfiltersenabled2 ,
                                              AV100WWServicoDS_6_Dynamicfiltersselector2 ,
                                              AV101WWServicoDS_7_Servico_sigla2 ,
                                              AV102WWServicoDS_8_Servico_nome2 ,
                                              AV103WWServicoDS_9_Servicogrupo_codigo2 ,
                                              AV105WWServicoDS_11_Tfservico_nome_sel ,
                                              AV104WWServicoDS_10_Tfservico_nome ,
                                              AV107WWServicoDS_13_Tfservico_sigla_sel ,
                                              AV106WWServicoDS_12_Tfservico_sigla ,
                                              AV109WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                              AV108WWServicoDS_14_Tfservicogrupo_descricao ,
                                              AV110WWServicoDS_16_Tfservico_objetocontrole_sels.Count ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV97WWServicoDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV97WWServicoDS_3_Servico_nome1), 50, "%");
         lV102WWServicoDS_8_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV102WWServicoDS_8_Servico_nome2), 50, "%");
         lV104WWServicoDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV104WWServicoDS_10_Tfservico_nome), 50, "%");
         lV106WWServicoDS_12_Tfservico_sigla = StringUtil.PadR( StringUtil.RTrim( AV106WWServicoDS_12_Tfservico_sigla), 15, "%");
         lV108WWServicoDS_14_Tfservicogrupo_descricao = StringUtil.Concat( StringUtil.RTrim( AV108WWServicoDS_14_Tfservicogrupo_descricao), "%", "");
         /* Using cursor H006Y7 */
         pr_default.execute(5, new Object[] {AV96WWServicoDS_2_Servico_sigla1, lV97WWServicoDS_3_Servico_nome1, AV98WWServicoDS_4_Servicogrupo_codigo1, AV101WWServicoDS_7_Servico_sigla2, lV102WWServicoDS_8_Servico_nome2, AV103WWServicoDS_9_Servicogrupo_codigo2, lV104WWServicoDS_10_Tfservico_nome, AV105WWServicoDS_11_Tfservico_nome_sel, lV106WWServicoDS_12_Tfservico_sigla, AV107WWServicoDS_13_Tfservico_sigla_sel, lV108WWServicoDS_14_Tfservicogrupo_descricao, AV109WWServicoDS_15_Tfservicogrupo_descricao_sel});
         GRID_nRecordCount = H006Y7_AGRID_nRecordCount[0];
         pr_default.close(5);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6Y0( )
      {
         /* Before Start, stand alone formulas. */
         AV115Pgmname = "WWServico";
         context.Gx_err = 0;
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E246Y2 */
         E246Y2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV88DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_NOMETITLEFILTERDATA"), AV72Servico_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_SIGLATITLEFILTERDATA"), AV76Servico_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA"), AV80ServicoGrupo_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_OBJETOCONTROLETITLEFILTERDATA"), AV84Servico_ObjetoControleTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV62Servico_Sigla1 = StringUtil.Upper( cgiGet( edtavServico_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Servico_Sigla1", AV62Servico_Sigla1);
            AV63Servico_Nome1 = StringUtil.Upper( cgiGet( edtavServico_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Servico_Nome1", AV63Servico_Nome1);
            dynavServicogrupo_codigo1.Name = dynavServicogrupo_codigo1_Internalname;
            dynavServicogrupo_codigo1.CurrentValue = cgiGet( dynavServicogrupo_codigo1_Internalname);
            AV64ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( cgiGet( dynavServicogrupo_codigo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV65Servico_Sigla2 = StringUtil.Upper( cgiGet( edtavServico_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Servico_Sigla2", AV65Servico_Sigla2);
            AV66Servico_Nome2 = StringUtil.Upper( cgiGet( edtavServico_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Servico_Nome2", AV66Servico_Nome2);
            dynavServicogrupo_codigo2.Name = dynavServicogrupo_codigo2_Internalname;
            dynavServicogrupo_codigo2.CurrentValue = cgiGet( dynavServicogrupo_codigo2_Internalname);
            AV67ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( cgiGet( dynavServicogrupo_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV73TFServico_Nome = StringUtil.Upper( cgiGet( edtavTfservico_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFServico_Nome", AV73TFServico_Nome);
            AV74TFServico_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservico_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFServico_Nome_Sel", AV74TFServico_Nome_Sel);
            AV77TFServico_Sigla = StringUtil.Upper( cgiGet( edtavTfservico_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFServico_Sigla", AV77TFServico_Sigla);
            AV78TFServico_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfservico_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFServico_Sigla_Sel", AV78TFServico_Sigla_Sel);
            AV81TFServicoGrupo_Descricao = cgiGet( edtavTfservicogrupo_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFServicoGrupo_Descricao", AV81TFServicoGrupo_Descricao);
            AV82TFServicoGrupo_Descricao_Sel = cgiGet( edtavTfservicogrupo_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFServicoGrupo_Descricao_Sel", AV82TFServicoGrupo_Descricao_Sel);
            AV75ddo_Servico_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servico_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Servico_NomeTitleControlIdToReplace", AV75ddo_Servico_NomeTitleControlIdToReplace);
            AV79ddo_Servico_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_Servico_SiglaTitleControlIdToReplace", AV79ddo_Servico_SiglaTitleControlIdToReplace);
            AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
            AV87ddo_Servico_ObjetoControleTitleControlIdToReplace = cgiGet( edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_Servico_ObjetoControleTitleControlIdToReplace", AV87ddo_Servico_ObjetoControleTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_65 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_65"), ",", "."));
            AV90GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV91GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servico_nome_Caption = cgiGet( "DDO_SERVICO_NOME_Caption");
            Ddo_servico_nome_Tooltip = cgiGet( "DDO_SERVICO_NOME_Tooltip");
            Ddo_servico_nome_Cls = cgiGet( "DDO_SERVICO_NOME_Cls");
            Ddo_servico_nome_Filteredtext_set = cgiGet( "DDO_SERVICO_NOME_Filteredtext_set");
            Ddo_servico_nome_Selectedvalue_set = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_set");
            Ddo_servico_nome_Dropdownoptionstype = cgiGet( "DDO_SERVICO_NOME_Dropdownoptionstype");
            Ddo_servico_nome_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_NOME_Titlecontrolidtoreplace");
            Ddo_servico_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortasc"));
            Ddo_servico_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortdsc"));
            Ddo_servico_nome_Sortedstatus = cgiGet( "DDO_SERVICO_NOME_Sortedstatus");
            Ddo_servico_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includefilter"));
            Ddo_servico_nome_Filtertype = cgiGet( "DDO_SERVICO_NOME_Filtertype");
            Ddo_servico_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Filterisrange"));
            Ddo_servico_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includedatalist"));
            Ddo_servico_nome_Datalisttype = cgiGet( "DDO_SERVICO_NOME_Datalisttype");
            Ddo_servico_nome_Datalistproc = cgiGet( "DDO_SERVICO_NOME_Datalistproc");
            Ddo_servico_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_nome_Sortasc = cgiGet( "DDO_SERVICO_NOME_Sortasc");
            Ddo_servico_nome_Sortdsc = cgiGet( "DDO_SERVICO_NOME_Sortdsc");
            Ddo_servico_nome_Loadingdata = cgiGet( "DDO_SERVICO_NOME_Loadingdata");
            Ddo_servico_nome_Cleanfilter = cgiGet( "DDO_SERVICO_NOME_Cleanfilter");
            Ddo_servico_nome_Noresultsfound = cgiGet( "DDO_SERVICO_NOME_Noresultsfound");
            Ddo_servico_nome_Searchbuttontext = cgiGet( "DDO_SERVICO_NOME_Searchbuttontext");
            Ddo_servico_sigla_Caption = cgiGet( "DDO_SERVICO_SIGLA_Caption");
            Ddo_servico_sigla_Tooltip = cgiGet( "DDO_SERVICO_SIGLA_Tooltip");
            Ddo_servico_sigla_Cls = cgiGet( "DDO_SERVICO_SIGLA_Cls");
            Ddo_servico_sigla_Filteredtext_set = cgiGet( "DDO_SERVICO_SIGLA_Filteredtext_set");
            Ddo_servico_sigla_Selectedvalue_set = cgiGet( "DDO_SERVICO_SIGLA_Selectedvalue_set");
            Ddo_servico_sigla_Dropdownoptionstype = cgiGet( "DDO_SERVICO_SIGLA_Dropdownoptionstype");
            Ddo_servico_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_SIGLA_Titlecontrolidtoreplace");
            Ddo_servico_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includesortasc"));
            Ddo_servico_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includesortdsc"));
            Ddo_servico_sigla_Sortedstatus = cgiGet( "DDO_SERVICO_SIGLA_Sortedstatus");
            Ddo_servico_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includefilter"));
            Ddo_servico_sigla_Filtertype = cgiGet( "DDO_SERVICO_SIGLA_Filtertype");
            Ddo_servico_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Filterisrange"));
            Ddo_servico_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_SIGLA_Includedatalist"));
            Ddo_servico_sigla_Datalisttype = cgiGet( "DDO_SERVICO_SIGLA_Datalisttype");
            Ddo_servico_sigla_Datalistproc = cgiGet( "DDO_SERVICO_SIGLA_Datalistproc");
            Ddo_servico_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_sigla_Sortasc = cgiGet( "DDO_SERVICO_SIGLA_Sortasc");
            Ddo_servico_sigla_Sortdsc = cgiGet( "DDO_SERVICO_SIGLA_Sortdsc");
            Ddo_servico_sigla_Loadingdata = cgiGet( "DDO_SERVICO_SIGLA_Loadingdata");
            Ddo_servico_sigla_Cleanfilter = cgiGet( "DDO_SERVICO_SIGLA_Cleanfilter");
            Ddo_servico_sigla_Noresultsfound = cgiGet( "DDO_SERVICO_SIGLA_Noresultsfound");
            Ddo_servico_sigla_Searchbuttontext = cgiGet( "DDO_SERVICO_SIGLA_Searchbuttontext");
            Ddo_servicogrupo_descricao_Caption = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Caption");
            Ddo_servicogrupo_descricao_Tooltip = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Tooltip");
            Ddo_servicogrupo_descricao_Cls = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Cls");
            Ddo_servicogrupo_descricao_Filteredtext_set = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_set");
            Ddo_servicogrupo_descricao_Selectedvalue_set = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_set");
            Ddo_servicogrupo_descricao_Dropdownoptionstype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Dropdownoptionstype");
            Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_servicogrupo_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includesortasc"));
            Ddo_servicogrupo_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includesortdsc"));
            Ddo_servicogrupo_descricao_Sortedstatus = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortedstatus");
            Ddo_servicogrupo_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includefilter"));
            Ddo_servicogrupo_descricao_Filtertype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filtertype");
            Ddo_servicogrupo_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filterisrange"));
            Ddo_servicogrupo_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Includedatalist"));
            Ddo_servicogrupo_descricao_Datalisttype = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalisttype");
            Ddo_servicogrupo_descricao_Datalistproc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistproc");
            Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicogrupo_descricao_Sortasc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortasc");
            Ddo_servicogrupo_descricao_Sortdsc = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Sortdsc");
            Ddo_servicogrupo_descricao_Loadingdata = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Loadingdata");
            Ddo_servicogrupo_descricao_Cleanfilter = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Cleanfilter");
            Ddo_servicogrupo_descricao_Noresultsfound = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Noresultsfound");
            Ddo_servicogrupo_descricao_Searchbuttontext = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Searchbuttontext");
            Ddo_servico_objetocontrole_Caption = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Caption");
            Ddo_servico_objetocontrole_Tooltip = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Tooltip");
            Ddo_servico_objetocontrole_Cls = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Cls");
            Ddo_servico_objetocontrole_Selectedvalue_set = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Selectedvalue_set");
            Ddo_servico_objetocontrole_Dropdownoptionstype = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Dropdownoptionstype");
            Ddo_servico_objetocontrole_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Titlecontrolidtoreplace");
            Ddo_servico_objetocontrole_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Includesortasc"));
            Ddo_servico_objetocontrole_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Includesortdsc"));
            Ddo_servico_objetocontrole_Sortedstatus = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Sortedstatus");
            Ddo_servico_objetocontrole_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Includefilter"));
            Ddo_servico_objetocontrole_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Includedatalist"));
            Ddo_servico_objetocontrole_Datalisttype = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Datalisttype");
            Ddo_servico_objetocontrole_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Allowmultipleselection"));
            Ddo_servico_objetocontrole_Datalistfixedvalues = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Datalistfixedvalues");
            Ddo_servico_objetocontrole_Sortasc = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Sortasc");
            Ddo_servico_objetocontrole_Sortdsc = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Sortdsc");
            Ddo_servico_objetocontrole_Cleanfilter = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Cleanfilter");
            Ddo_servico_objetocontrole_Searchbuttontext = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servico_nome_Activeeventkey = cgiGet( "DDO_SERVICO_NOME_Activeeventkey");
            Ddo_servico_nome_Filteredtext_get = cgiGet( "DDO_SERVICO_NOME_Filteredtext_get");
            Ddo_servico_nome_Selectedvalue_get = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_get");
            Ddo_servico_sigla_Activeeventkey = cgiGet( "DDO_SERVICO_SIGLA_Activeeventkey");
            Ddo_servico_sigla_Filteredtext_get = cgiGet( "DDO_SERVICO_SIGLA_Filteredtext_get");
            Ddo_servico_sigla_Selectedvalue_get = cgiGet( "DDO_SERVICO_SIGLA_Selectedvalue_get");
            Ddo_servicogrupo_descricao_Activeeventkey = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Activeeventkey");
            Ddo_servicogrupo_descricao_Filteredtext_get = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Filteredtext_get");
            Ddo_servicogrupo_descricao_Selectedvalue_get = cgiGet( "DDO_SERVICOGRUPO_DESCRICAO_Selectedvalue_get");
            Ddo_servico_objetocontrole_Activeeventkey = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Activeeventkey");
            Ddo_servico_objetocontrole_Selectedvalue_get = cgiGet( "DDO_SERVICO_OBJETOCONTROLE_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA1"), AV62Servico_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV63Servico_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV64ServicoGrupo_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_SIGLA2"), AV65Servico_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV66Servico_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSERVICOGRUPO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV67ServicoGrupo_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV73TFServico_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV74TFServico_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA"), AV77TFServico_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_SIGLA_SEL"), AV78TFServico_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO"), AV81TFServicoGrupo_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOGRUPO_DESCRICAO_SEL"), AV82TFServicoGrupo_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E246Y2 */
         E246Y2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E246Y2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfservico_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_Visible), 5, 0)));
         edtavTfservico_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_sel_Visible), 5, 0)));
         edtavTfservico_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_sigla_Visible), 5, 0)));
         edtavTfservico_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_sigla_sel_Visible), 5, 0)));
         edtavTfservicogrupo_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_descricao_Visible), 5, 0)));
         edtavTfservicogrupo_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicogrupo_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicogrupo_descricao_sel_Visible), 5, 0)));
         Ddo_servico_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "TitleControlIdToReplace", Ddo_servico_nome_Titlecontrolidtoreplace);
         AV75ddo_Servico_NomeTitleControlIdToReplace = Ddo_servico_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Servico_NomeTitleControlIdToReplace", AV75ddo_Servico_NomeTitleControlIdToReplace);
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "TitleControlIdToReplace", Ddo_servico_sigla_Titlecontrolidtoreplace);
         AV79ddo_Servico_SiglaTitleControlIdToReplace = Ddo_servico_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_Servico_SiglaTitleControlIdToReplace", AV79ddo_Servico_SiglaTitleControlIdToReplace);
         edtavDdo_servico_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoGrupo_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "TitleControlIdToReplace", Ddo_servicogrupo_descricao_Titlecontrolidtoreplace);
         AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = Ddo_servicogrupo_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace", AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace);
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_objetocontrole_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_ObjetoControle";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "TitleControlIdToReplace", Ddo_servico_objetocontrole_Titlecontrolidtoreplace);
         AV87ddo_Servico_ObjetoControleTitleControlIdToReplace = Ddo_servico_objetocontrole_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_Servico_ObjetoControleTitleControlIdToReplace", AV87ddo_Servico_ObjetoControleTitleControlIdToReplace);
         edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Servico";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Sigla", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Grupo", 0);
         cmbavOrderedby.addItem("4", "Obj. de Ctrl.", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV88DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV88DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
      }

      protected void E256Y2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV72Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76Servico_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ServicoGrupo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84Servico_ObjetoControleTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServico_Nome_Titleformat = 2;
         edtServico_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV75ddo_Servico_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Nome_Internalname, "Title", edtServico_Nome_Title);
         edtServico_Sigla_Titleformat = 2;
         edtServico_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV79ddo_Servico_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Sigla_Internalname, "Title", edtServico_Sigla_Title);
         edtServicoGrupo_Descricao_Titleformat = 2;
         edtServicoGrupo_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Grupo", AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoGrupo_Descricao_Internalname, "Title", edtServicoGrupo_Descricao_Title);
         cmbServico_ObjetoControle_Titleformat = 2;
         cmbServico_ObjetoControle.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Obj. de Ctrl.", AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_ObjetoControle_Internalname, "Title", cmbServico_ObjetoControle.Title.Text);
         AV90GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90GridCurrentPage), 10, 0)));
         AV91GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV95WWServicoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV96WWServicoDS_2_Servico_sigla1 = AV62Servico_Sigla1;
         AV97WWServicoDS_3_Servico_nome1 = AV63Servico_Nome1;
         AV98WWServicoDS_4_Servicogrupo_codigo1 = AV64ServicoGrupo_Codigo1;
         AV99WWServicoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV100WWServicoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV101WWServicoDS_7_Servico_sigla2 = AV65Servico_Sigla2;
         AV102WWServicoDS_8_Servico_nome2 = AV66Servico_Nome2;
         AV103WWServicoDS_9_Servicogrupo_codigo2 = AV67ServicoGrupo_Codigo2;
         AV104WWServicoDS_10_Tfservico_nome = AV73TFServico_Nome;
         AV105WWServicoDS_11_Tfservico_nome_sel = AV74TFServico_Nome_Sel;
         AV106WWServicoDS_12_Tfservico_sigla = AV77TFServico_Sigla;
         AV107WWServicoDS_13_Tfservico_sigla_sel = AV78TFServico_Sigla_Sel;
         AV108WWServicoDS_14_Tfservicogrupo_descricao = AV81TFServicoGrupo_Descricao;
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = AV82TFServicoGrupo_Descricao_Sel;
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = AV86TFServico_ObjetoControle_Sels;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72Servico_NomeTitleFilterData", AV72Servico_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76Servico_SiglaTitleFilterData", AV76Servico_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80ServicoGrupo_DescricaoTitleFilterData", AV80ServicoGrupo_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV84Servico_ObjetoControleTitleFilterData", AV84Servico_ObjetoControleTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116Y2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV89PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV89PageToGo) ;
         }
      }

      protected void E126Y2( )
      {
         /* Ddo_servico_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFServico_Nome = Ddo_servico_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFServico_Nome", AV73TFServico_Nome);
            AV74TFServico_Nome_Sel = Ddo_servico_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFServico_Nome_Sel", AV74TFServico_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136Y2( )
      {
         /* Ddo_servico_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFServico_Sigla = Ddo_servico_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFServico_Sigla", AV77TFServico_Sigla);
            AV78TFServico_Sigla_Sel = Ddo_servico_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFServico_Sigla_Sel", AV78TFServico_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146Y2( )
      {
         /* Ddo_servicogrupo_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicogrupo_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicogrupo_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV81TFServicoGrupo_Descricao = Ddo_servicogrupo_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFServicoGrupo_Descricao", AV81TFServicoGrupo_Descricao);
            AV82TFServicoGrupo_Descricao_Sel = Ddo_servicogrupo_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFServicoGrupo_Descricao_Sel", AV82TFServicoGrupo_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E156Y2( )
      {
         /* Ddo_servico_objetocontrole_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_objetocontrole_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_objetocontrole_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "SortedStatus", Ddo_servico_objetocontrole_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_objetocontrole_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_objetocontrole_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "SortedStatus", Ddo_servico_objetocontrole_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_objetocontrole_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV85TFServico_ObjetoControle_SelsJson = Ddo_servico_objetocontrole_Selectedvalue_get;
            AV86TFServico_ObjetoControle_Sels.FromJSonString(AV85TFServico_ObjetoControle_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV86TFServico_ObjetoControle_Sels", AV86TFServico_ObjetoControle_Sels);
      }

      private void E266Y2( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV111Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV68ServicoGrupo_Codigo);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV112Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV68ServicoGrupo_Codigo);
         AV40Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV40Display);
         AV113Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "ANS";
         edtavDisplay_Link = formatLink("viewservico.aspx") + "?" + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtServicoGrupo_Descricao_Link = formatLink("viewservicogrupo.aspx") + "?" + UrlEncode("" +A157ServicoGrupo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Using cursor H006Y8 */
         pr_default.execute(6, new Object[] {A1551Servico_Responsavel});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A57Usuario_PessoaCod = H006Y8_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H006Y8_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H006Y8_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H006Y8_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H006Y8_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H006Y8_n58Usuario_PessoaNom[0];
            AV92Nome = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV92Nome);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 65;
         }
         sendrow_652( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_65_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(65, GridRow);
         }
      }

      protected void E166Y2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E216Y2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E176Y2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
      }

      protected void E226Y2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E186Y2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV62Servico_Sigla1, AV63Servico_Nome1, AV64ServicoGrupo_Codigo1, AV20DynamicFiltersSelector2, AV65Servico_Sigla2, AV66Servico_Nome2, AV67ServicoGrupo_Codigo2, AV19DynamicFiltersEnabled2, AV73TFServico_Nome, AV74TFServico_Nome_Sel, AV77TFServico_Sigla, AV78TFServico_Sigla_Sel, AV81TFServicoGrupo_Descricao, AV82TFServicoGrupo_Descricao_Sel, AV75ddo_Servico_NomeTitleControlIdToReplace, AV79ddo_Servico_SiglaTitleControlIdToReplace, AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace, AV87ddo_Servico_ObjetoControleTitleControlIdToReplace, AV86TFServico_ObjetoControle_Sels, AV115Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A155Servico_Codigo, AV68ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, A1Usuario_Codigo, A1551Servico_Responsavel, A58Usuario_PessoaNom) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
      }

      protected void E236Y2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E196Y2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV86TFServico_ObjetoControle_Sels", AV86TFServico_ObjetoControle_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", dynavServicogrupo_codigo1.ToJavascriptSource());
         dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", dynavServicogrupo_codigo2.ToJavascriptSource());
      }

      protected void E206Y2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV68ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servico_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         Ddo_servico_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
         Ddo_servicogrupo_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
         Ddo_servico_objetocontrole_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "SortedStatus", Ddo_servico_objetocontrole_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_servico_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_servico_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SortedStatus", Ddo_servico_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_servicogrupo_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SortedStatus", Ddo_servicogrupo_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_servico_objetocontrole_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "SortedStatus", Ddo_servico_objetocontrole_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServico_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla1_Visible), 5, 0)));
         edtavServico_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         dynavServicogrupo_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 )
         {
            edtavServico_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            dynavServicogrupo_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServico_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla2_Visible), 5, 0)));
         edtavServico_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         dynavServicogrupo_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 )
         {
            edtavServico_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 )
         {
            dynavServicogrupo_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServicogrupo_codigo2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV65Servico_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Servico_Sigla2", AV65Servico_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV73TFServico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFServico_Nome", AV73TFServico_Nome);
         Ddo_servico_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
         AV74TFServico_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFServico_Nome_Sel", AV74TFServico_Nome_Sel);
         Ddo_servico_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
         AV77TFServico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFServico_Sigla", AV77TFServico_Sigla);
         Ddo_servico_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "FilteredText_set", Ddo_servico_sigla_Filteredtext_set);
         AV78TFServico_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFServico_Sigla_Sel", AV78TFServico_Sigla_Sel);
         Ddo_servico_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SelectedValue_set", Ddo_servico_sigla_Selectedvalue_set);
         AV81TFServicoGrupo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFServicoGrupo_Descricao", AV81TFServicoGrupo_Descricao);
         Ddo_servicogrupo_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "FilteredText_set", Ddo_servicogrupo_descricao_Filteredtext_set);
         AV82TFServicoGrupo_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFServicoGrupo_Descricao_Sel", AV82TFServicoGrupo_Descricao_Sel);
         Ddo_servicogrupo_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SelectedValue_set", Ddo_servicogrupo_descricao_Selectedvalue_set);
         AV86TFServico_ObjetoControle_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_servico_objetocontrole_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "SelectedValue_set", Ddo_servico_objetocontrole_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SERVICO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV62Servico_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Servico_Sigla1", AV62Servico_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV115Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV115Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV115Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV116GXV1 = 1;
         while ( AV116GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV116GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV73TFServico_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFServico_Nome", AV73TFServico_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFServico_Nome)) )
               {
                  Ddo_servico_nome_Filteredtext_set = AV73TFServico_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV74TFServico_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFServico_Nome_Sel", AV74TFServico_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFServico_Nome_Sel)) )
               {
                  Ddo_servico_nome_Selectedvalue_set = AV74TFServico_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA") == 0 )
            {
               AV77TFServico_Sigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFServico_Sigla", AV77TFServico_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFServico_Sigla)) )
               {
                  Ddo_servico_sigla_Filteredtext_set = AV77TFServico_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "FilteredText_set", Ddo_servico_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA_SEL") == 0 )
            {
               AV78TFServico_Sigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFServico_Sigla_Sel", AV78TFServico_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFServico_Sigla_Sel)) )
               {
                  Ddo_servico_sigla_Selectedvalue_set = AV78TFServico_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_sigla_Internalname, "SelectedValue_set", Ddo_servico_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV81TFServicoGrupo_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFServicoGrupo_Descricao", AV81TFServicoGrupo_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFServicoGrupo_Descricao)) )
               {
                  Ddo_servicogrupo_descricao_Filteredtext_set = AV81TFServicoGrupo_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "FilteredText_set", Ddo_servicogrupo_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO_SEL") == 0 )
            {
               AV82TFServicoGrupo_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFServicoGrupo_Descricao_Sel", AV82TFServicoGrupo_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFServicoGrupo_Descricao_Sel)) )
               {
                  Ddo_servicogrupo_descricao_Selectedvalue_set = AV82TFServicoGrupo_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicogrupo_descricao_Internalname, "SelectedValue_set", Ddo_servicogrupo_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_OBJETOCONTROLE_SEL") == 0 )
            {
               AV85TFServico_ObjetoControle_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV86TFServico_ObjetoControle_Sels.FromJSonString(AV85TFServico_ObjetoControle_SelsJson);
               if ( ! ( AV86TFServico_ObjetoControle_Sels.Count == 0 ) )
               {
                  Ddo_servico_objetocontrole_Selectedvalue_set = AV85TFServico_ObjetoControle_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_objetocontrole_Internalname, "SelectedValue_set", Ddo_servico_objetocontrole_Selectedvalue_set);
               }
            }
            AV116GXV1 = (int)(AV116GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 )
            {
               AV62Servico_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Servico_Sigla1", AV62Servico_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV63Servico_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Servico_Nome1", AV63Servico_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
            {
               AV64ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ServicoGrupo_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 )
               {
                  AV65Servico_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Servico_Sigla2", AV65Servico_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV66Servico_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Servico_Nome2", AV66Servico_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 )
               {
                  AV67ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoGrupo_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV115Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFServico_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFServico_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFServico_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFServico_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFServico_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV77TFServico_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFServico_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV78TFServico_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFServicoGrupo_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV81TFServicoGrupo_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFServicoGrupo_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOGRUPO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV82TFServicoGrupo_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV86TFServico_ObjetoControle_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_OBJETOCONTROLE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV86TFServico_ObjetoControle_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV115Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV62Servico_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV62Servico_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV63Servico_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV63Servico_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ! (0==AV64ServicoGrupo_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV65Servico_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV65Servico_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV66Servico_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV66Servico_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ! (0==AV67ServicoGrupo_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV115Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Servico";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_6Y2( true) ;
         }
         else
         {
            wb_table2_8_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_6Y2( true) ;
         }
         else
         {
            wb_table3_59_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table3_59_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6Y2e( true) ;
         }
         else
         {
            wb_table1_2_6Y2e( false) ;
         }
      }

      protected void wb_table3_59_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_62_6Y2( true) ;
         }
         else
         {
            wb_table4_62_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table4_62_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_6Y2e( true) ;
         }
         else
         {
            wb_table3_59_6Y2e( false) ;
         }
      }

      protected void wb_table4_62_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"65\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo de Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Grupo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoGrupo_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoGrupo_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoGrupo_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_ObjetoControle_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_ObjetoControle.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_ObjetoControle.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV40Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A605Servico_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A158ServicoGrupo_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoGrupo_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoGrupo_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServicoGrupo_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV92Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1436Servico_ObjetoControle));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_ObjetoControle.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_ObjetoControle_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 65 )
         {
            wbEnd = 0;
            nRC_GXsfl_65 = (short)(nGXsfl_65_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_62_6Y2e( true) ;
         }
         else
         {
            wb_table4_62_6Y2e( false) ;
         }
      }

      protected void wb_table2_8_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicotitle_Internalname, "Servi�os", "", "", lblServicotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_6Y2( true) ;
         }
         else
         {
            wb_table5_13_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWServico.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_6Y2( true) ;
         }
         else
         {
            wb_table6_23_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_6Y2e( true) ;
         }
         else
         {
            wb_table2_8_6Y2e( false) ;
         }
      }

      protected void wb_table6_23_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_6Y2( true) ;
         }
         else
         {
            wb_table7_28_6Y2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_6Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_6Y2e( true) ;
         }
         else
         {
            wb_table6_23_6Y2e( false) ;
         }
      }

      protected void wb_table7_28_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWServico.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla1_Internalname, StringUtil.RTrim( AV62Servico_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV62Servico_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_sigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome1_Internalname, StringUtil.RTrim( AV63Servico_Nome1), StringUtil.RTrim( context.localUtil.Format( AV63Servico_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicogrupo_codigo1, dynavServicogrupo_codigo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0)), 1, dynavServicogrupo_codigo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavServicogrupo_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWServico.htm");
            dynavServicogrupo_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV64ServicoGrupo_Codigo1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo1_Internalname, "Values", (String)(dynavServicogrupo_codigo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WWServico.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla2_Internalname, StringUtil.RTrim( AV65Servico_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV65Servico_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_sigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome2_Internalname, StringUtil.RTrim( AV66Servico_Nome2), StringUtil.RTrim( context.localUtil.Format( AV66Servico_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicogrupo_codigo2, dynavServicogrupo_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0)), 1, dynavServicogrupo_codigo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavServicogrupo_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWServico.htm");
            dynavServicogrupo_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoGrupo_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicogrupo_codigo2_Internalname, "Values", (String)(dynavServicogrupo_codigo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_6Y2e( true) ;
         }
         else
         {
            wb_table7_28_6Y2e( false) ;
         }
      }

      protected void wb_table5_13_6Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_6Y2e( true) ;
         }
         else
         {
            wb_table5_13_6Y2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6Y2( ) ;
         WS6Y2( ) ;
         WE6Y2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299345336");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwservico.js", "?20205299345337");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_652( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_65_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_65_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_65_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_65_idx;
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO_"+sGXsfl_65_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_65_idx;
         edtServico_Sigla_Internalname = "SERVICO_SIGLA_"+sGXsfl_65_idx;
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO_"+sGXsfl_65_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_65_idx;
         cmbServico_ObjetoControle_Internalname = "SERVICO_OBJETOCONTROLE_"+sGXsfl_65_idx;
      }

      protected void SubsflControlProps_fel_652( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_65_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_65_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_65_fel_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_65_fel_idx;
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO_"+sGXsfl_65_fel_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_65_fel_idx;
         edtServico_Sigla_Internalname = "SERVICO_SIGLA_"+sGXsfl_65_fel_idx;
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO_"+sGXsfl_65_fel_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_65_fel_idx;
         cmbServico_ObjetoControle_Internalname = "SERVICO_OBJETOCONTROLE_"+sGXsfl_65_fel_idx;
      }

      protected void sendrow_652( )
      {
         SubsflControlProps_652( ) ;
         WB6Y0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_65_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_65_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_65_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV111Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV112Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV40Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV40Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV113Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV40Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV40Display)) ? AV113Display_GXI : context.PathToRelativeUrl( AV40Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV40Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoGrupo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Sigla_Internalname,StringUtil.RTrim( A605Servico_Sigla),StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Descricao_Internalname,(String)A158ServicoGrupo_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtServicoGrupo_Descricao_Link,(String)"",(String)"",(String)"",(String)edtServicoGrupo_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNome_Internalname,StringUtil.RTrim( AV92Nome),StringUtil.RTrim( context.localUtil.Format( AV92Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_65_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_OBJETOCONTROLE_" + sGXsfl_65_idx;
               cmbServico_ObjetoControle.Name = GXCCtl;
               cmbServico_ObjetoControle.WebTags = "";
               cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
               cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
               cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
               if ( cmbServico_ObjetoControle.ItemCount > 0 )
               {
                  A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_ObjetoControle,(String)cmbServico_ObjetoControle_Internalname,StringUtil.RTrim( A1436Servico_ObjetoControle),(short)1,(String)cmbServico_ObjetoControle_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_ObjetoControle.CurrentValue = StringUtil.RTrim( A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_ObjetoControle_Internalname, "Values", (String)(cmbServico_ObjetoControle.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOGRUPO_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_NOME"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_SIGLA"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_OBJETOCONTROLE"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         /* End function sendrow_652 */
      }

      protected void init_default_properties( )
      {
         lblServicotitle_Internalname = "SERVICOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavServico_sigla1_Internalname = "vSERVICO_SIGLA1";
         edtavServico_nome1_Internalname = "vSERVICO_NOME1";
         dynavServicogrupo_codigo1_Internalname = "vSERVICOGRUPO_CODIGO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavServico_sigla2_Internalname = "vSERVICO_SIGLA2";
         edtavServico_nome2_Internalname = "vSERVICO_NOME2";
         dynavServicogrupo_codigo2_Internalname = "vSERVICOGRUPO_CODIGO2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         edtServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO";
         edtServico_Nome_Internalname = "SERVICO_NOME";
         edtServico_Sigla_Internalname = "SERVICO_SIGLA";
         edtServicoGrupo_Descricao_Internalname = "SERVICOGRUPO_DESCRICAO";
         edtavNome_Internalname = "vNOME";
         cmbServico_ObjetoControle_Internalname = "SERVICO_OBJETOCONTROLE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfservico_nome_Internalname = "vTFSERVICO_NOME";
         edtavTfservico_nome_sel_Internalname = "vTFSERVICO_NOME_SEL";
         edtavTfservico_sigla_Internalname = "vTFSERVICO_SIGLA";
         edtavTfservico_sigla_sel_Internalname = "vTFSERVICO_SIGLA_SEL";
         edtavTfservicogrupo_descricao_Internalname = "vTFSERVICOGRUPO_DESCRICAO";
         edtavTfservicogrupo_descricao_sel_Internalname = "vTFSERVICOGRUPO_DESCRICAO_SEL";
         Ddo_servico_nome_Internalname = "DDO_SERVICO_NOME";
         edtavDdo_servico_nometitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_servico_sigla_Internalname = "DDO_SERVICO_SIGLA";
         edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_servicogrupo_descricao_Internalname = "DDO_SERVICOGRUPO_DESCRICAO";
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname = "vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_servico_objetocontrole_Internalname = "DDO_SERVICO_OBJETOCONTROLE";
         edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbServico_ObjetoControle_Jsonclick = "";
         edtavNome_Jsonclick = "";
         edtServicoGrupo_Descricao_Jsonclick = "";
         edtServico_Sigla_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         dynavServicogrupo_codigo2_Jsonclick = "";
         edtavServico_nome2_Jsonclick = "";
         edtavServico_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavServicogrupo_codigo1_Jsonclick = "";
         edtavServico_nome1_Jsonclick = "";
         edtavServico_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavNome_Enabled = 0;
         edtServicoGrupo_Descricao_Link = "";
         edtavDisplay_Tooltiptext = "ANS";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbServico_ObjetoControle_Titleformat = 0;
         edtServicoGrupo_Descricao_Titleformat = 0;
         edtServico_Sigla_Titleformat = 0;
         edtServico_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavServicogrupo_codigo2.Visible = 1;
         edtavServico_nome2_Visible = 1;
         edtavServico_sigla2_Visible = 1;
         dynavServicogrupo_codigo1.Visible = 1;
         edtavServico_nome1_Visible = 1;
         edtavServico_sigla1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         cmbServico_ObjetoControle.Title.Text = "Obj. de Ctrl.";
         edtServicoGrupo_Descricao_Title = "Grupo";
         edtServico_Sigla_Title = "Sigla";
         edtServico_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfservicogrupo_descricao_sel_Jsonclick = "";
         edtavTfservicogrupo_descricao_sel_Visible = 1;
         edtavTfservicogrupo_descricao_Jsonclick = "";
         edtavTfservicogrupo_descricao_Visible = 1;
         edtavTfservico_sigla_sel_Jsonclick = "";
         edtavTfservico_sigla_sel_Visible = 1;
         edtavTfservico_sigla_Jsonclick = "";
         edtavTfservico_sigla_Visible = 1;
         edtavTfservico_nome_sel_Jsonclick = "";
         edtavTfservico_nome_sel_Visible = 1;
         edtavTfservico_nome_Jsonclick = "";
         edtavTfservico_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_servico_objetocontrole_Searchbuttontext = "Filtrar Selecionados";
         Ddo_servico_objetocontrole_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_objetocontrole_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_objetocontrole_Sortasc = "Ordenar de A � Z";
         Ddo_servico_objetocontrole_Datalistfixedvalues = "SIS:Sistema,PRC:Processo,PRD:Produto";
         Ddo_servico_objetocontrole_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_servico_objetocontrole_Datalisttype = "FixedValues";
         Ddo_servico_objetocontrole_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_objetocontrole_Includefilter = Convert.ToBoolean( 0);
         Ddo_servico_objetocontrole_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_objetocontrole_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_objetocontrole_Titlecontrolidtoreplace = "";
         Ddo_servico_objetocontrole_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_objetocontrole_Cls = "ColumnSettings";
         Ddo_servico_objetocontrole_Tooltip = "Op��es";
         Ddo_servico_objetocontrole_Caption = "";
         Ddo_servicogrupo_descricao_Searchbuttontext = "Pesquisar";
         Ddo_servicogrupo_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicogrupo_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_servicogrupo_descricao_Loadingdata = "Carregando dados...";
         Ddo_servicogrupo_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_servicogrupo_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_servicogrupo_descricao_Datalistproc = "GetWWServicoFilterData";
         Ddo_servicogrupo_descricao_Datalisttype = "Dynamic";
         Ddo_servicogrupo_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicogrupo_descricao_Filtertype = "Character";
         Ddo_servicogrupo_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicogrupo_descricao_Titlecontrolidtoreplace = "";
         Ddo_servicogrupo_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicogrupo_descricao_Cls = "ColumnSettings";
         Ddo_servicogrupo_descricao_Tooltip = "Op��es";
         Ddo_servicogrupo_descricao_Caption = "";
         Ddo_servico_sigla_Searchbuttontext = "Pesquisar";
         Ddo_servico_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_sigla_Loadingdata = "Carregando dados...";
         Ddo_servico_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_servico_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_servico_sigla_Datalistproc = "GetWWServicoFilterData";
         Ddo_servico_sigla_Datalisttype = "Dynamic";
         Ddo_servico_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_sigla_Filtertype = "Character";
         Ddo_servico_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_sigla_Titlecontrolidtoreplace = "";
         Ddo_servico_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_sigla_Cls = "ColumnSettings";
         Ddo_servico_sigla_Tooltip = "Op��es";
         Ddo_servico_sigla_Caption = "";
         Ddo_servico_nome_Searchbuttontext = "Pesquisar";
         Ddo_servico_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_nome_Loadingdata = "Carregando dados...";
         Ddo_servico_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servico_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servico_nome_Datalistproc = "GetWWServicoFilterData";
         Ddo_servico_nome_Datalisttype = "Dynamic";
         Ddo_servico_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_nome_Filtertype = "Character";
         Ddo_servico_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Titlecontrolidtoreplace = "";
         Ddo_servico_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_nome_Cls = "ColumnSettings";
         Ddo_servico_nome_Tooltip = "Op��es";
         Ddo_servico_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Servico";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV72Servico_NomeTitleFilterData',fld:'vSERVICO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV76Servico_SiglaTitleFilterData',fld:'vSERVICO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV80ServicoGrupo_DescricaoTitleFilterData',fld:'vSERVICOGRUPO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV84Servico_ObjetoControleTitleFilterData',fld:'vSERVICO_OBJETOCONTROLETITLEFILTERDATA',pic:'',nv:null},{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServicoGrupo_Descricao_Titleformat',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Titleformat'},{av:'edtServicoGrupo_Descricao_Title',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Title'},{av:'cmbServico_ObjetoControle'},{av:'AV90GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV91GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICO_NOME.ONOPTIONCLICKED","{handler:'E126Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_servico_nome_Activeeventkey',ctrl:'DDO_SERVICO_NOME',prop:'ActiveEventKey'},{av:'Ddo_servico_nome_Filteredtext_get',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_get'},{av:'Ddo_servico_nome_Selectedvalue_get',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_servico_objetocontrole_Sortedstatus',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_SIGLA.ONOPTIONCLICKED","{handler:'E136Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Activeeventkey',ctrl:'DDO_SERVICO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_servico_sigla_Filteredtext_get',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_servico_sigla_Selectedvalue_get',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_servico_objetocontrole_Sortedstatus',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOGRUPO_DESCRICAO.ONOPTIONCLICKED","{handler:'E146Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_servicogrupo_descricao_Activeeventkey',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_servicogrupo_descricao_Filteredtext_get',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_servicogrupo_descricao_Selectedvalue_get',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'Ddo_servico_objetocontrole_Sortedstatus',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_OBJETOCONTROLE.ONOPTIONCLICKED","{handler:'E156Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_servico_objetocontrole_Activeeventkey',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'ActiveEventKey'},{av:'Ddo_servico_objetocontrole_Selectedvalue_get',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_objetocontrole_Sortedstatus',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'SortedStatus'},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servico_sigla_Sortedstatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'Ddo_servicogrupo_descricao_Sortedstatus',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E266Y2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV40Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtServicoGrupo_Descricao_Link',ctrl:'SERVICOGRUPO_DESCRICAO',prop:'Link'},{av:'AV92Nome',fld:'vNOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E166Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E216Y2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E176Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E226Y2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E186Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E236Y2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E196Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV75ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace',fld:'vDDO_SERVICOGRUPO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_Servico_ObjetoControleTitleControlIdToReplace',fld:'vDDO_SERVICO_OBJETOCONTROLETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV73TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'Ddo_servico_nome_Filteredtext_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV74TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Selectedvalue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV77TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Filteredtext_set',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_set'},{av:'AV78TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_servico_sigla_Selectedvalue_set',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_set'},{av:'AV81TFServicoGrupo_Descricao',fld:'vTFSERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'Ddo_servicogrupo_descricao_Filteredtext_set',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'FilteredText_set'},{av:'AV82TFServicoGrupo_Descricao_Sel',fld:'vTFSERVICOGRUPO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_servicogrupo_descricao_Selectedvalue_set',ctrl:'DDO_SERVICOGRUPO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV86TFServico_ObjetoControle_Sels',fld:'vTFSERVICO_OBJETOCONTROLE_SELS',pic:'',nv:null},{av:'Ddo_servico_objetocontrole_Selectedvalue_set',ctrl:'DDO_SERVICO_OBJETOCONTROLE',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV62Servico_Sigla1',fld:'vSERVICO_SIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServico_sigla1_Visible',ctrl:'vSERVICO_SIGLA1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'dynavServicogrupo_codigo1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV65Servico_Sigla2',fld:'vSERVICO_SIGLA2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV63Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV64ServicoGrupo_Codigo1',fld:'vSERVICOGRUPO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV66Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV67ServicoGrupo_Codigo2',fld:'vSERVICOGRUPO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'edtavServico_sigla2_Visible',ctrl:'vSERVICO_SIGLA2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'dynavServicogrupo_codigo2'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E206Y2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV68ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_servico_nome_Activeeventkey = "";
         Ddo_servico_nome_Filteredtext_get = "";
         Ddo_servico_nome_Selectedvalue_get = "";
         Ddo_servico_sigla_Activeeventkey = "";
         Ddo_servico_sigla_Filteredtext_get = "";
         Ddo_servico_sigla_Selectedvalue_get = "";
         Ddo_servicogrupo_descricao_Activeeventkey = "";
         Ddo_servicogrupo_descricao_Filteredtext_get = "";
         Ddo_servicogrupo_descricao_Selectedvalue_get = "";
         Ddo_servico_objetocontrole_Activeeventkey = "";
         Ddo_servico_objetocontrole_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV62Servico_Sigla1 = "";
         AV63Servico_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV65Servico_Sigla2 = "";
         AV66Servico_Nome2 = "";
         AV73TFServico_Nome = "";
         AV74TFServico_Nome_Sel = "";
         AV77TFServico_Sigla = "";
         AV78TFServico_Sigla_Sel = "";
         AV81TFServicoGrupo_Descricao = "";
         AV82TFServicoGrupo_Descricao_Sel = "";
         AV75ddo_Servico_NomeTitleControlIdToReplace = "";
         AV79ddo_Servico_SiglaTitleControlIdToReplace = "";
         AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace = "";
         AV87ddo_Servico_ObjetoControleTitleControlIdToReplace = "";
         AV86TFServico_ObjetoControle_Sels = new GxSimpleCollection();
         AV115Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A58Usuario_PessoaNom = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV88DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV72Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76Servico_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ServicoGrupo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84Servico_ObjetoControleTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servico_nome_Filteredtext_set = "";
         Ddo_servico_nome_Selectedvalue_set = "";
         Ddo_servico_nome_Sortedstatus = "";
         Ddo_servico_sigla_Filteredtext_set = "";
         Ddo_servico_sigla_Selectedvalue_set = "";
         Ddo_servico_sigla_Sortedstatus = "";
         Ddo_servicogrupo_descricao_Filteredtext_set = "";
         Ddo_servicogrupo_descricao_Selectedvalue_set = "";
         Ddo_servicogrupo_descricao_Sortedstatus = "";
         Ddo_servico_objetocontrole_Selectedvalue_set = "";
         Ddo_servico_objetocontrole_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV111Update_GXI = "";
         AV32Delete = "";
         AV112Delete_GXI = "";
         AV40Display = "";
         AV113Display_GXI = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         A158ServicoGrupo_Descricao = "";
         AV92Nome = "";
         A1436Servico_ObjetoControle = "";
         scmdbuf = "";
         H006Y2_A157ServicoGrupo_Codigo = new int[1] ;
         H006Y2_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Y3_A157ServicoGrupo_Codigo = new int[1] ;
         H006Y3_A158ServicoGrupo_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H006Y4_A157ServicoGrupo_Codigo = new int[1] ;
         H006Y4_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Y5_A157ServicoGrupo_Codigo = new int[1] ;
         H006Y5_A158ServicoGrupo_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV110WWServicoDS_16_Tfservico_objetocontrole_sels = new GxSimpleCollection();
         lV97WWServicoDS_3_Servico_nome1 = "";
         lV102WWServicoDS_8_Servico_nome2 = "";
         lV104WWServicoDS_10_Tfservico_nome = "";
         lV106WWServicoDS_12_Tfservico_sigla = "";
         lV108WWServicoDS_14_Tfservicogrupo_descricao = "";
         AV95WWServicoDS_1_Dynamicfiltersselector1 = "";
         AV96WWServicoDS_2_Servico_sigla1 = "";
         AV97WWServicoDS_3_Servico_nome1 = "";
         AV100WWServicoDS_6_Dynamicfiltersselector2 = "";
         AV101WWServicoDS_7_Servico_sigla2 = "";
         AV102WWServicoDS_8_Servico_nome2 = "";
         AV105WWServicoDS_11_Tfservico_nome_sel = "";
         AV104WWServicoDS_10_Tfservico_nome = "";
         AV107WWServicoDS_13_Tfservico_sigla_sel = "";
         AV106WWServicoDS_12_Tfservico_sigla = "";
         AV109WWServicoDS_15_Tfservicogrupo_descricao_sel = "";
         AV108WWServicoDS_14_Tfservicogrupo_descricao = "";
         H006Y6_A159ServicoGrupo_Ativo = new bool[] {false} ;
         H006Y6_A1436Servico_ObjetoControle = new String[] {""} ;
         H006Y6_A158ServicoGrupo_Descricao = new String[] {""} ;
         H006Y6_A605Servico_Sigla = new String[] {""} ;
         H006Y6_A608Servico_Nome = new String[] {""} ;
         H006Y6_A157ServicoGrupo_Codigo = new int[1] ;
         H006Y6_A155Servico_Codigo = new int[1] ;
         H006Y7_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV85TFServico_ObjetoControle_SelsJson = "";
         H006Y8_A57Usuario_PessoaCod = new int[1] ;
         H006Y8_A1Usuario_Codigo = new int[1] ;
         H006Y8_A58Usuario_PessoaNom = new String[] {""} ;
         H006Y8_n58Usuario_PessoaNom = new bool[] {false} ;
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblServicotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwservico__default(),
            new Object[][] {
                new Object[] {
               H006Y2_A157ServicoGrupo_Codigo, H006Y2_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Y3_A157ServicoGrupo_Codigo, H006Y3_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Y4_A157ServicoGrupo_Codigo, H006Y4_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Y5_A157ServicoGrupo_Codigo, H006Y5_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H006Y6_A159ServicoGrupo_Ativo, H006Y6_A1436Servico_ObjetoControle, H006Y6_A158ServicoGrupo_Descricao, H006Y6_A605Servico_Sigla, H006Y6_A608Servico_Nome, H006Y6_A157ServicoGrupo_Codigo, H006Y6_A155Servico_Codigo
               }
               , new Object[] {
               H006Y7_AGRID_nRecordCount
               }
               , new Object[] {
               H006Y8_A57Usuario_PessoaCod, H006Y8_A1Usuario_Codigo, H006Y8_A58Usuario_PessoaNom, H006Y8_n58Usuario_PessoaNom
               }
            }
         );
         AV115Pgmname = "WWServico";
         /* GeneXus formulas. */
         AV115Pgmname = "WWServico";
         context.Gx_err = 0;
         edtavNome_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_65 ;
      private short nGXsfl_65_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_65_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServico_Nome_Titleformat ;
      private short edtServico_Sigla_Titleformat ;
      private short edtServicoGrupo_Descricao_Titleformat ;
      private short cmbServico_ObjetoControle_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV64ServicoGrupo_Codigo1 ;
      private int AV67ServicoGrupo_Codigo2 ;
      private int A155Servico_Codigo ;
      private int AV68ServicoGrupo_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A1Usuario_Codigo ;
      private int A1551Servico_Responsavel ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servico_nome_Datalistupdateminimumcharacters ;
      private int Ddo_servico_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_servicogrupo_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfservico_nome_Visible ;
      private int edtavTfservico_nome_sel_Visible ;
      private int edtavTfservico_sigla_Visible ;
      private int edtavTfservico_sigla_sel_Visible ;
      private int edtavTfservicogrupo_descricao_Visible ;
      private int edtavTfservicogrupo_descricao_sel_Visible ;
      private int edtavDdo_servico_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavNome_Enabled ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV110WWServicoDS_16_Tfservico_objetocontrole_sels_Count ;
      private int AV98WWServicoDS_4_Servicogrupo_codigo1 ;
      private int AV103WWServicoDS_9_Servicogrupo_codigo2 ;
      private int GXt_int1 ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV89PageToGo ;
      private int A57Usuario_PessoaCod ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavServico_sigla1_Visible ;
      private int edtavServico_nome1_Visible ;
      private int edtavServico_sigla2_Visible ;
      private int edtavServico_nome2_Visible ;
      private int AV116GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV90GridCurrentPage ;
      private long AV91GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servico_nome_Activeeventkey ;
      private String Ddo_servico_nome_Filteredtext_get ;
      private String Ddo_servico_nome_Selectedvalue_get ;
      private String Ddo_servico_sigla_Activeeventkey ;
      private String Ddo_servico_sigla_Filteredtext_get ;
      private String Ddo_servico_sigla_Selectedvalue_get ;
      private String Ddo_servicogrupo_descricao_Activeeventkey ;
      private String Ddo_servicogrupo_descricao_Filteredtext_get ;
      private String Ddo_servicogrupo_descricao_Selectedvalue_get ;
      private String Ddo_servico_objetocontrole_Activeeventkey ;
      private String Ddo_servico_objetocontrole_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_65_idx="0001" ;
      private String AV62Servico_Sigla1 ;
      private String AV63Servico_Nome1 ;
      private String AV65Servico_Sigla2 ;
      private String AV66Servico_Nome2 ;
      private String AV73TFServico_Nome ;
      private String AV74TFServico_Nome_Sel ;
      private String AV77TFServico_Sigla ;
      private String AV78TFServico_Sigla_Sel ;
      private String AV115Pgmname ;
      private String A58Usuario_PessoaNom ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servico_nome_Caption ;
      private String Ddo_servico_nome_Tooltip ;
      private String Ddo_servico_nome_Cls ;
      private String Ddo_servico_nome_Filteredtext_set ;
      private String Ddo_servico_nome_Selectedvalue_set ;
      private String Ddo_servico_nome_Dropdownoptionstype ;
      private String Ddo_servico_nome_Titlecontrolidtoreplace ;
      private String Ddo_servico_nome_Sortedstatus ;
      private String Ddo_servico_nome_Filtertype ;
      private String Ddo_servico_nome_Datalisttype ;
      private String Ddo_servico_nome_Datalistproc ;
      private String Ddo_servico_nome_Sortasc ;
      private String Ddo_servico_nome_Sortdsc ;
      private String Ddo_servico_nome_Loadingdata ;
      private String Ddo_servico_nome_Cleanfilter ;
      private String Ddo_servico_nome_Noresultsfound ;
      private String Ddo_servico_nome_Searchbuttontext ;
      private String Ddo_servico_sigla_Caption ;
      private String Ddo_servico_sigla_Tooltip ;
      private String Ddo_servico_sigla_Cls ;
      private String Ddo_servico_sigla_Filteredtext_set ;
      private String Ddo_servico_sigla_Selectedvalue_set ;
      private String Ddo_servico_sigla_Dropdownoptionstype ;
      private String Ddo_servico_sigla_Titlecontrolidtoreplace ;
      private String Ddo_servico_sigla_Sortedstatus ;
      private String Ddo_servico_sigla_Filtertype ;
      private String Ddo_servico_sigla_Datalisttype ;
      private String Ddo_servico_sigla_Datalistproc ;
      private String Ddo_servico_sigla_Sortasc ;
      private String Ddo_servico_sigla_Sortdsc ;
      private String Ddo_servico_sigla_Loadingdata ;
      private String Ddo_servico_sigla_Cleanfilter ;
      private String Ddo_servico_sigla_Noresultsfound ;
      private String Ddo_servico_sigla_Searchbuttontext ;
      private String Ddo_servicogrupo_descricao_Caption ;
      private String Ddo_servicogrupo_descricao_Tooltip ;
      private String Ddo_servicogrupo_descricao_Cls ;
      private String Ddo_servicogrupo_descricao_Filteredtext_set ;
      private String Ddo_servicogrupo_descricao_Selectedvalue_set ;
      private String Ddo_servicogrupo_descricao_Dropdownoptionstype ;
      private String Ddo_servicogrupo_descricao_Titlecontrolidtoreplace ;
      private String Ddo_servicogrupo_descricao_Sortedstatus ;
      private String Ddo_servicogrupo_descricao_Filtertype ;
      private String Ddo_servicogrupo_descricao_Datalisttype ;
      private String Ddo_servicogrupo_descricao_Datalistproc ;
      private String Ddo_servicogrupo_descricao_Sortasc ;
      private String Ddo_servicogrupo_descricao_Sortdsc ;
      private String Ddo_servicogrupo_descricao_Loadingdata ;
      private String Ddo_servicogrupo_descricao_Cleanfilter ;
      private String Ddo_servicogrupo_descricao_Noresultsfound ;
      private String Ddo_servicogrupo_descricao_Searchbuttontext ;
      private String Ddo_servico_objetocontrole_Caption ;
      private String Ddo_servico_objetocontrole_Tooltip ;
      private String Ddo_servico_objetocontrole_Cls ;
      private String Ddo_servico_objetocontrole_Selectedvalue_set ;
      private String Ddo_servico_objetocontrole_Dropdownoptionstype ;
      private String Ddo_servico_objetocontrole_Titlecontrolidtoreplace ;
      private String Ddo_servico_objetocontrole_Sortedstatus ;
      private String Ddo_servico_objetocontrole_Datalisttype ;
      private String Ddo_servico_objetocontrole_Datalistfixedvalues ;
      private String Ddo_servico_objetocontrole_Sortasc ;
      private String Ddo_servico_objetocontrole_Sortdsc ;
      private String Ddo_servico_objetocontrole_Cleanfilter ;
      private String Ddo_servico_objetocontrole_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfservico_nome_Internalname ;
      private String edtavTfservico_nome_Jsonclick ;
      private String edtavTfservico_nome_sel_Internalname ;
      private String edtavTfservico_nome_sel_Jsonclick ;
      private String edtavTfservico_sigla_Internalname ;
      private String edtavTfservico_sigla_Jsonclick ;
      private String edtavTfservico_sigla_sel_Internalname ;
      private String edtavTfservico_sigla_sel_Jsonclick ;
      private String edtavTfservicogrupo_descricao_Internalname ;
      private String edtavTfservicogrupo_descricao_Jsonclick ;
      private String edtavTfservicogrupo_descricao_sel_Internalname ;
      private String edtavTfservicogrupo_descricao_sel_Jsonclick ;
      private String edtavDdo_servico_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicogrupo_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_objetocontroletitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Internalname ;
      private String edtServicoGrupo_Descricao_Internalname ;
      private String AV92Nome ;
      private String edtavNome_Internalname ;
      private String cmbServico_ObjetoControle_Internalname ;
      private String A1436Servico_ObjetoControle ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String lV97WWServicoDS_3_Servico_nome1 ;
      private String lV102WWServicoDS_8_Servico_nome2 ;
      private String lV104WWServicoDS_10_Tfservico_nome ;
      private String lV106WWServicoDS_12_Tfservico_sigla ;
      private String AV96WWServicoDS_2_Servico_sigla1 ;
      private String AV97WWServicoDS_3_Servico_nome1 ;
      private String AV101WWServicoDS_7_Servico_sigla2 ;
      private String AV102WWServicoDS_8_Servico_nome2 ;
      private String AV105WWServicoDS_11_Tfservico_nome_sel ;
      private String AV104WWServicoDS_10_Tfservico_nome ;
      private String AV107WWServicoDS_13_Tfservico_sigla_sel ;
      private String AV106WWServicoDS_12_Tfservico_sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavServico_sigla1_Internalname ;
      private String edtavServico_nome1_Internalname ;
      private String dynavServicogrupo_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavServico_sigla2_Internalname ;
      private String edtavServico_nome2_Internalname ;
      private String dynavServicogrupo_codigo2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servico_nome_Internalname ;
      private String Ddo_servico_sigla_Internalname ;
      private String Ddo_servicogrupo_descricao_Internalname ;
      private String Ddo_servico_objetocontrole_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtServico_Nome_Title ;
      private String edtServico_Sigla_Title ;
      private String edtServicoGrupo_Descricao_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtServicoGrupo_Descricao_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblServicotitle_Internalname ;
      private String lblServicotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavServico_sigla1_Jsonclick ;
      private String edtavServico_nome1_Jsonclick ;
      private String dynavServicogrupo_codigo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavServico_sigla2_Jsonclick ;
      private String edtavServico_nome2_Jsonclick ;
      private String dynavServicogrupo_codigo2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_65_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String edtServico_Sigla_Jsonclick ;
      private String edtServicoGrupo_Descricao_Jsonclick ;
      private String edtavNome_Jsonclick ;
      private String cmbServico_ObjetoControle_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool n58Usuario_PessoaNom ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servico_nome_Includesortasc ;
      private bool Ddo_servico_nome_Includesortdsc ;
      private bool Ddo_servico_nome_Includefilter ;
      private bool Ddo_servico_nome_Filterisrange ;
      private bool Ddo_servico_nome_Includedatalist ;
      private bool Ddo_servico_sigla_Includesortasc ;
      private bool Ddo_servico_sigla_Includesortdsc ;
      private bool Ddo_servico_sigla_Includefilter ;
      private bool Ddo_servico_sigla_Filterisrange ;
      private bool Ddo_servico_sigla_Includedatalist ;
      private bool Ddo_servicogrupo_descricao_Includesortasc ;
      private bool Ddo_servicogrupo_descricao_Includesortdsc ;
      private bool Ddo_servicogrupo_descricao_Includefilter ;
      private bool Ddo_servicogrupo_descricao_Filterisrange ;
      private bool Ddo_servicogrupo_descricao_Includedatalist ;
      private bool Ddo_servico_objetocontrole_Includesortasc ;
      private bool Ddo_servico_objetocontrole_Includesortdsc ;
      private bool Ddo_servico_objetocontrole_Includefilter ;
      private bool Ddo_servico_objetocontrole_Includedatalist ;
      private bool Ddo_servico_objetocontrole_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV99WWServicoDS_5_Dynamicfiltersenabled2 ;
      private bool A159ServicoGrupo_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV40Display_IsBlob ;
      private String AV85TFServico_ObjetoControle_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV81TFServicoGrupo_Descricao ;
      private String AV82TFServicoGrupo_Descricao_Sel ;
      private String AV75ddo_Servico_NomeTitleControlIdToReplace ;
      private String AV79ddo_Servico_SiglaTitleControlIdToReplace ;
      private String AV83ddo_ServicoGrupo_DescricaoTitleControlIdToReplace ;
      private String AV87ddo_Servico_ObjetoControleTitleControlIdToReplace ;
      private String AV111Update_GXI ;
      private String AV112Delete_GXI ;
      private String AV113Display_GXI ;
      private String A158ServicoGrupo_Descricao ;
      private String lV108WWServicoDS_14_Tfservicogrupo_descricao ;
      private String AV95WWServicoDS_1_Dynamicfiltersselector1 ;
      private String AV100WWServicoDS_6_Dynamicfiltersselector2 ;
      private String AV109WWServicoDS_15_Tfservicogrupo_descricao_sel ;
      private String AV108WWServicoDS_14_Tfservicogrupo_descricao ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV40Display ;
      private IGxSession AV33Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavServicogrupo_codigo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavServicogrupo_codigo2 ;
      private GXCombobox cmbServico_ObjetoControle ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H006Y2_A157ServicoGrupo_Codigo ;
      private String[] H006Y2_A158ServicoGrupo_Descricao ;
      private int[] H006Y3_A157ServicoGrupo_Codigo ;
      private String[] H006Y3_A158ServicoGrupo_Descricao ;
      private int[] H006Y4_A157ServicoGrupo_Codigo ;
      private String[] H006Y4_A158ServicoGrupo_Descricao ;
      private int[] H006Y5_A157ServicoGrupo_Codigo ;
      private String[] H006Y5_A158ServicoGrupo_Descricao ;
      private bool[] H006Y6_A159ServicoGrupo_Ativo ;
      private String[] H006Y6_A1436Servico_ObjetoControle ;
      private String[] H006Y6_A158ServicoGrupo_Descricao ;
      private String[] H006Y6_A605Servico_Sigla ;
      private String[] H006Y6_A608Servico_Nome ;
      private int[] H006Y6_A157ServicoGrupo_Codigo ;
      private int[] H006Y6_A155Servico_Codigo ;
      private long[] H006Y7_AGRID_nRecordCount ;
      private int[] H006Y8_A57Usuario_PessoaCod ;
      private int[] H006Y8_A1Usuario_Codigo ;
      private String[] H006Y8_A58Usuario_PessoaNom ;
      private bool[] H006Y8_n58Usuario_PessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV86TFServico_ObjetoControle_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV110WWServicoDS_16_Tfservico_objetocontrole_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72Servico_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76Servico_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV80ServicoGrupo_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV84Servico_ObjetoControleTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV88DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
   }

   public class wwservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006Y6( IGxContext context ,
                                             String A1436Servico_ObjetoControle ,
                                             IGxCollection AV110WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                             String AV95WWServicoDS_1_Dynamicfiltersselector1 ,
                                             String AV96WWServicoDS_2_Servico_sigla1 ,
                                             String AV97WWServicoDS_3_Servico_nome1 ,
                                             int AV98WWServicoDS_4_Servicogrupo_codigo1 ,
                                             bool AV99WWServicoDS_5_Dynamicfiltersenabled2 ,
                                             String AV100WWServicoDS_6_Dynamicfiltersselector2 ,
                                             String AV101WWServicoDS_7_Servico_sigla2 ,
                                             String AV102WWServicoDS_8_Servico_nome2 ,
                                             int AV103WWServicoDS_9_Servicogrupo_codigo2 ,
                                             String AV105WWServicoDS_11_Tfservico_nome_sel ,
                                             String AV104WWServicoDS_10_Tfservico_nome ,
                                             String AV107WWServicoDS_13_Tfservico_sigla_sel ,
                                             String AV106WWServicoDS_12_Tfservico_sigla ,
                                             String AV109WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                             String AV108WWServicoDS_14_Tfservicogrupo_descricao ,
                                             int AV110WWServicoDS_16_Tfservico_objetocontrole_sels_Count ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [17] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ServicoGrupo_Ativo], T1.[Servico_ObjetoControle], T2.[ServicoGrupo_Descricao], T1.[Servico_Sigla], T1.[Servico_Nome], T1.[ServicoGrupo_Codigo], T1.[Servico_Codigo]";
         sFromString = " FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWServicoDS_2_Servico_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV96WWServicoDS_2_Servico_sigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV96WWServicoDS_2_Servico_sigla1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWServicoDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV97WWServicoDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV97WWServicoDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV98WWServicoDS_4_Servicogrupo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV98WWServicoDS_4_Servicogrupo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV98WWServicoDS_4_Servicogrupo_codigo1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWServicoDS_7_Servico_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV101WWServicoDS_7_Servico_sigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV101WWServicoDS_7_Servico_sigla2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWServicoDS_8_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV102WWServicoDS_8_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV102WWServicoDS_8_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV103WWServicoDS_9_Servicogrupo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV103WWServicoDS_9_Servicogrupo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV103WWServicoDS_9_Servicogrupo_codigo2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWServicoDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWServicoDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV104WWServicoDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like @lV104WWServicoDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWServicoDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV105WWServicoDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] = @AV105WWServicoDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWServicoDS_13_Tfservico_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWServicoDS_12_Tfservico_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV106WWServicoDS_12_Tfservico_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] like @lV106WWServicoDS_12_Tfservico_sigla)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWServicoDS_13_Tfservico_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV107WWServicoDS_13_Tfservico_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV107WWServicoDS_13_Tfservico_sigla_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWServicoDS_14_Tfservicogrupo_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV108WWServicoDS_14_Tfservicogrupo_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] like @lV108WWServicoDS_14_Tfservicogrupo_descricao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] = @AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV110WWServicoDS_16_Tfservico_objetocontrole_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ServicoGrupo_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ServicoGrupo_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_ObjetoControle]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_ObjetoControle] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H006Y7( IGxContext context ,
                                             String A1436Servico_ObjetoControle ,
                                             IGxCollection AV110WWServicoDS_16_Tfservico_objetocontrole_sels ,
                                             String AV95WWServicoDS_1_Dynamicfiltersselector1 ,
                                             String AV96WWServicoDS_2_Servico_sigla1 ,
                                             String AV97WWServicoDS_3_Servico_nome1 ,
                                             int AV98WWServicoDS_4_Servicogrupo_codigo1 ,
                                             bool AV99WWServicoDS_5_Dynamicfiltersenabled2 ,
                                             String AV100WWServicoDS_6_Dynamicfiltersselector2 ,
                                             String AV101WWServicoDS_7_Servico_sigla2 ,
                                             String AV102WWServicoDS_8_Servico_nome2 ,
                                             int AV103WWServicoDS_9_Servicogrupo_codigo2 ,
                                             String AV105WWServicoDS_11_Tfservico_nome_sel ,
                                             String AV104WWServicoDS_10_Tfservico_nome ,
                                             String AV107WWServicoDS_13_Tfservico_sigla_sel ,
                                             String AV106WWServicoDS_12_Tfservico_sigla ,
                                             String AV109WWServicoDS_15_Tfservicogrupo_descricao_sel ,
                                             String AV108WWServicoDS_14_Tfservicogrupo_descricao ,
                                             int AV110WWServicoDS_16_Tfservico_objetocontrole_sels_Count ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         if ( ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWServicoDS_2_Servico_sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV96WWServicoDS_2_Servico_sigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV96WWServicoDS_2_Servico_sigla1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWServicoDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV97WWServicoDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV97WWServicoDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV98WWServicoDS_4_Servicogrupo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV98WWServicoDS_4_Servicogrupo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV98WWServicoDS_4_Servicogrupo_codigo1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( StringUtil.StrCmp(AV95WWServicoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWServicoDS_7_Servico_sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV101WWServicoDS_7_Servico_sigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV101WWServicoDS_7_Servico_sigla2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWServicoDS_8_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV102WWServicoDS_8_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like '%' + @lV102WWServicoDS_8_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV103WWServicoDS_9_Servicogrupo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV103WWServicoDS_9_Servicogrupo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoGrupo_Codigo] = @AV103WWServicoDS_9_Servicogrupo_codigo2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV99WWServicoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV100WWServicoDS_6_Dynamicfiltersselector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWServicoDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWServicoDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV104WWServicoDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] like @lV104WWServicoDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWServicoDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV105WWServicoDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Nome] = @AV105WWServicoDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWServicoDS_13_Tfservico_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWServicoDS_12_Tfservico_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV106WWServicoDS_12_Tfservico_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] like @lV106WWServicoDS_12_Tfservico_sigla)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWServicoDS_13_Tfservico_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV107WWServicoDS_13_Tfservico_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Sigla] = @AV107WWServicoDS_13_Tfservico_sigla_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWServicoDS_14_Tfservicogrupo_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV108WWServicoDS_14_Tfservicogrupo_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] like @lV108WWServicoDS_14_Tfservicogrupo_descricao)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ServicoGrupo_Descricao] = @AV109WWServicoDS_15_Tfservicogrupo_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV110WWServicoDS_16_Tfservico_objetocontrole_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV110WWServicoDS_16_Tfservico_objetocontrole_sels, "T1.[Servico_ObjetoControle] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 4 :
                     return conditional_H006Y6(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 5 :
                     return conditional_H006Y7(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006Y2 ;
          prmH006Y2 = new Object[] {
          } ;
          Object[] prmH006Y3 ;
          prmH006Y3 = new Object[] {
          } ;
          Object[] prmH006Y4 ;
          prmH006Y4 = new Object[] {
          } ;
          Object[] prmH006Y5 ;
          prmH006Y5 = new Object[] {
          } ;
          Object[] prmH006Y8 ;
          prmH006Y8 = new Object[] {
          new Object[] {"@Servico_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006Y6 ;
          prmH006Y6 = new Object[] {
          new Object[] {"@AV96WWServicoDS_2_Servico_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV97WWServicoDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV98WWServicoDS_4_Servicogrupo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWServicoDS_7_Servico_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV102WWServicoDS_8_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWServicoDS_9_Servicogrupo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV104WWServicoDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV105WWServicoDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWServicoDS_12_Tfservico_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107WWServicoDS_13_Tfservico_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV108WWServicoDS_14_Tfservicogrupo_descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV109WWServicoDS_15_Tfservicogrupo_descricao_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006Y7 ;
          prmH006Y7 = new Object[] {
          new Object[] {"@AV96WWServicoDS_2_Servico_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV97WWServicoDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV98WWServicoDS_4_Servicogrupo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV101WWServicoDS_7_Servico_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV102WWServicoDS_8_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWServicoDS_9_Servicogrupo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV104WWServicoDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV105WWServicoDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWServicoDS_12_Tfservico_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107WWServicoDS_13_Tfservico_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV108WWServicoDS_14_Tfservicogrupo_descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV109WWServicoDS_15_Tfservicogrupo_descricao_sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006Y2", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y2,0,0,true,false )
             ,new CursorDef("H006Y3", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y3,0,0,true,false )
             ,new CursorDef("H006Y4", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y4,0,0,true,false )
             ,new CursorDef("H006Y5", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y5,0,0,true,false )
             ,new CursorDef("H006Y6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y6,11,0,true,false )
             ,new CursorDef("H006Y7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y7,1,0,true,false )
             ,new CursorDef("H006Y8", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @Servico_Responsavel ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006Y8,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 4 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
