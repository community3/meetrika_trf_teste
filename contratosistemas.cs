/*
               File: ContratoSistemas
        Description: Sistemas atendidos pelo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:40:52.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratosistemas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1725ContratoSistemas_CntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1725ContratoSistemas_CntCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1726ContratoSistemas_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1726ContratoSistemas_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Sistemas atendidos pelo Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratosistemas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratosistemas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_49189( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_49189e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_49189( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_49189( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_49189e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_49189( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_49189e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_49189e( true) ;
         }
         else
         {
            wb_table1_2_49189e( false) ;
         }
      }

      protected void wb_table3_26_49189( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_49189e( true) ;
         }
         else
         {
            wb_table3_26_49189e( false) ;
         }
      }

      protected void wb_table2_5_49189( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_49189( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_49189e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_49189e( true) ;
         }
         else
         {
            wb_table2_5_49189e( false) ;
         }
      }

      protected void wb_table4_13_49189( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosistemas_cntcod_Internalname, "Contrato", "", "", lblTextblockcontratosistemas_cntcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoSistemas_CntCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1725ContratoSistemas_CntCod), 6, 0, ",", "")), ((edtContratoSistemas_CntCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1725ContratoSistemas_CntCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1725ContratoSistemas_CntCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoSistemas_CntCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoSistemas_CntCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosistemas_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontratosistemas_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoSistemas_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0, ",", "")), ((edtContratoSistemas_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1726ContratoSistemas_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1726ContratoSistemas_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoSistemas_SistemaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoSistemas_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_49189e( true) ;
         }
         else
         {
            wb_table4_13_49189e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11492 */
         E11492 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoSistemas_CntCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoSistemas_CntCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSISTEMAS_CNTCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1725ContratoSistemas_CntCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
               }
               else
               {
                  A1725ContratoSistemas_CntCod = (int)(context.localUtil.CToN( cgiGet( edtContratoSistemas_CntCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoSistemas_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoSistemas_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSISTEMAS_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoSistemas_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1726ContratoSistemas_SistemaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
               }
               else
               {
                  A1726ContratoSistemas_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContratoSistemas_SistemaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
               }
               /* Read saved values. */
               Z1725ContratoSistemas_CntCod = (int)(context.localUtil.CToN( cgiGet( "Z1725ContratoSistemas_CntCod"), ",", "."));
               Z1726ContratoSistemas_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z1726ContratoSistemas_SistemaCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1725ContratoSistemas_CntCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
                  A1726ContratoSistemas_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11492 */
                           E11492 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12492 */
                           E12492 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12492 */
            E12492 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll49189( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         }
         DisableAttributes49189( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption490( )
      {
      }

      protected void E11492( )
      {
         /* Start Routine */
      }

      protected void E12492( )
      {
         /* After Trn Routine */
      }

      protected void ZM49189( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
            Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load49189( )
      {
         /* Using cursor T00496 */
         pr_default.execute(4, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound189 = 1;
            ZM49189( -1) ;
         }
         pr_default.close(4);
         OnLoadActions49189( ) ;
      }

      protected void OnLoadActions49189( )
      {
      }

      protected void CheckExtendedTable49189( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00494 */
         pr_default.execute(2, new Object[] {A1725ContratoSistemas_CntCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00495 */
         pr_default.execute(3, new Object[] {A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors49189( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A1725ContratoSistemas_CntCod )
      {
         /* Using cursor T00497 */
         pr_default.execute(5, new Object[] {A1725ContratoSistemas_CntCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A1726ContratoSistemas_SistemaCod )
      {
         /* Using cursor T00498 */
         pr_default.execute(6, new Object[] {A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey49189( )
      {
         /* Using cursor T00499 */
         pr_default.execute(7, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound189 = 1;
         }
         else
         {
            RcdFound189 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00493 */
         pr_default.execute(1, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM49189( 1) ;
            RcdFound189 = 1;
            A1725ContratoSistemas_CntCod = T00493_A1725ContratoSistemas_CntCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
            A1726ContratoSistemas_SistemaCod = T00493_A1726ContratoSistemas_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
            Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
            Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
            sMode189 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load49189( ) ;
            if ( AnyError == 1 )
            {
               RcdFound189 = 0;
               InitializeNonKey49189( ) ;
            }
            Gx_mode = sMode189;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound189 = 0;
            InitializeNonKey49189( ) ;
            sMode189 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode189;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey49189( ) ;
         if ( RcdFound189 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound189 = 0;
         /* Using cursor T004910 */
         pr_default.execute(8, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004910_A1725ContratoSistemas_CntCod[0] < A1725ContratoSistemas_CntCod ) || ( T004910_A1725ContratoSistemas_CntCod[0] == A1725ContratoSistemas_CntCod ) && ( T004910_A1726ContratoSistemas_SistemaCod[0] < A1726ContratoSistemas_SistemaCod ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004910_A1725ContratoSistemas_CntCod[0] > A1725ContratoSistemas_CntCod ) || ( T004910_A1725ContratoSistemas_CntCod[0] == A1725ContratoSistemas_CntCod ) && ( T004910_A1726ContratoSistemas_SistemaCod[0] > A1726ContratoSistemas_SistemaCod ) ) )
            {
               A1725ContratoSistemas_CntCod = T004910_A1725ContratoSistemas_CntCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
               A1726ContratoSistemas_SistemaCod = T004910_A1726ContratoSistemas_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
               RcdFound189 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound189 = 0;
         /* Using cursor T004911 */
         pr_default.execute(9, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004911_A1725ContratoSistemas_CntCod[0] > A1725ContratoSistemas_CntCod ) || ( T004911_A1725ContratoSistemas_CntCod[0] == A1725ContratoSistemas_CntCod ) && ( T004911_A1726ContratoSistemas_SistemaCod[0] > A1726ContratoSistemas_SistemaCod ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004911_A1725ContratoSistemas_CntCod[0] < A1725ContratoSistemas_CntCod ) || ( T004911_A1725ContratoSistemas_CntCod[0] == A1725ContratoSistemas_CntCod ) && ( T004911_A1726ContratoSistemas_SistemaCod[0] < A1726ContratoSistemas_SistemaCod ) ) )
            {
               A1725ContratoSistemas_CntCod = T004911_A1725ContratoSistemas_CntCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
               A1726ContratoSistemas_SistemaCod = T004911_A1726ContratoSistemas_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
               RcdFound189 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey49189( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert49189( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound189 == 1 )
            {
               if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
               {
                  A1725ContratoSistemas_CntCod = Z1725ContratoSistemas_CntCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
                  A1726ContratoSistemas_SistemaCod = Z1726ContratoSistemas_SistemaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update49189( ) ;
                  GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert49189( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSISTEMAS_CNTCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert49189( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
         {
            A1725ContratoSistemas_CntCod = Z1725ContratoSistemas_CntCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
            A1726ContratoSistemas_SistemaCod = Z1726ContratoSistemas_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart49189( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd49189( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart49189( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound189 != 0 )
            {
               ScanNext49189( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd49189( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency49189( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00492 */
            pr_default.execute(0, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoSistemas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoSistemas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert49189( )
      {
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable49189( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM49189( 0) ;
            CheckOptimisticConcurrency49189( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm49189( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert49189( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004912 */
                     pr_default.execute(10, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoSistemas") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption490( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load49189( ) ;
            }
            EndLevel49189( ) ;
         }
         CloseExtendedTableCursors49189( ) ;
      }

      protected void Update49189( )
      {
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable49189( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency49189( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm49189( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate49189( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContratoSistemas] */
                     DeferredUpdate49189( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption490( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel49189( ) ;
         }
         CloseExtendedTableCursors49189( ) ;
      }

      protected void DeferredUpdate49189( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency49189( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls49189( ) ;
            AfterConfirm49189( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete49189( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004913 */
                  pr_default.execute(11, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoSistemas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound189 == 0 )
                        {
                           InitAll49189( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption490( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode189 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel49189( ) ;
         Gx_mode = sMode189;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls49189( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel49189( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete49189( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoSistemas");
            if ( AnyError == 0 )
            {
               ConfirmValues490( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoSistemas");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart49189( )
      {
         /* Scan By routine */
         /* Using cursor T004914 */
         pr_default.execute(12);
         RcdFound189 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound189 = 1;
            A1725ContratoSistemas_CntCod = T004914_A1725ContratoSistemas_CntCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
            A1726ContratoSistemas_SistemaCod = T004914_A1726ContratoSistemas_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext49189( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound189 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound189 = 1;
            A1725ContratoSistemas_CntCod = T004914_A1725ContratoSistemas_CntCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
            A1726ContratoSistemas_SistemaCod = T004914_A1726ContratoSistemas_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
         }
      }

      protected void ScanEnd49189( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm49189( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert49189( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate49189( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete49189( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete49189( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate49189( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes49189( )
      {
         edtContratoSistemas_CntCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSistemas_CntCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSistemas_CntCod_Enabled), 5, 0)));
         edtContratoSistemas_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSistemas_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSistemas_SistemaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues490( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020548405352");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratosistemas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1725ContratoSistemas_CntCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1726ContratoSistemas_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratosistemas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContratoSistemas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistemas atendidos pelo Contrato" ;
      }

      protected void InitializeNonKey49189( )
      {
      }

      protected void InitAll49189( )
      {
         A1725ContratoSistemas_CntCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1725ContratoSistemas_CntCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1725ContratoSistemas_CntCod), 6, 0)));
         A1726ContratoSistemas_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1726ContratoSistemas_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1726ContratoSistemas_SistemaCod), 6, 0)));
         InitializeNonKey49189( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020548405363");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratosistemas.js", "?2020548405363");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratosistemas_cntcod_Internalname = "TEXTBLOCKCONTRATOSISTEMAS_CNTCOD";
         edtContratoSistemas_CntCod_Internalname = "CONTRATOSISTEMAS_CNTCOD";
         lblTextblockcontratosistemas_sistemacod_Internalname = "TEXTBLOCKCONTRATOSISTEMAS_SISTEMACOD";
         edtContratoSistemas_SistemaCod_Internalname = "CONTRATOSISTEMAS_SISTEMACOD";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Sistemas atendidos pelo Contrato";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Sistemas atendidos pelo Contrato";
         edtContratoSistemas_SistemaCod_Jsonclick = "";
         edtContratoSistemas_SistemaCod_Enabled = 1;
         edtContratoSistemas_CntCod_Jsonclick = "";
         edtContratoSistemas_CntCod_Enabled = 1;
         bttBtn_trn_delete_Enabled = 1;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T004915 */
         pr_default.execute(13, new Object[] {A1725ContratoSistemas_CntCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(13);
         /* Using cursor T004916 */
         pr_default.execute(14, new Object[] {A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(14);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contratosistemas_cntcod( int GX_Parm1 )
      {
         A1725ContratoSistemas_CntCod = GX_Parm1;
         /* Using cursor T004915 */
         pr_default.execute(13, new Object[] {A1725ContratoSistemas_CntCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_CntCod_Internalname;
         }
         pr_default.close(13);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratosistemas_sistemacod( int GX_Parm1 ,
                                                     int GX_Parm2 )
      {
         A1725ContratoSistemas_CntCod = GX_Parm1;
         A1726ContratoSistemas_SistemaCod = GX_Parm2;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T004916 */
         pr_default.execute(14, new Object[] {A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtContratoSistemas_SistemaCod_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1725ContratoSistemas_CntCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1726ContratoSistemas_SistemaCod), 6, 0, ",", "")));
         isValidOutput.Add(bttBtn_trn_delete_Enabled);
         isValidOutput.Add(bttBtn_trn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12492',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratosistemas_cntcod_Jsonclick = "";
         lblTextblockcontratosistemas_sistemacod_Jsonclick = "";
         Gx_mode = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T00496_A1725ContratoSistemas_CntCod = new int[1] ;
         T00496_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T00494_A1725ContratoSistemas_CntCod = new int[1] ;
         T00495_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T00497_A1725ContratoSistemas_CntCod = new int[1] ;
         T00498_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T00499_A1725ContratoSistemas_CntCod = new int[1] ;
         T00499_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T00493_A1725ContratoSistemas_CntCod = new int[1] ;
         T00493_A1726ContratoSistemas_SistemaCod = new int[1] ;
         sMode189 = "";
         T004910_A1725ContratoSistemas_CntCod = new int[1] ;
         T004910_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T004911_A1725ContratoSistemas_CntCod = new int[1] ;
         T004911_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T00492_A1725ContratoSistemas_CntCod = new int[1] ;
         T00492_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T004914_A1725ContratoSistemas_CntCod = new int[1] ;
         T004914_A1726ContratoSistemas_SistemaCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T004915_A1725ContratoSistemas_CntCod = new int[1] ;
         T004916_A1726ContratoSistemas_SistemaCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratosistemas__default(),
            new Object[][] {
                new Object[] {
               T00492_A1725ContratoSistemas_CntCod, T00492_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T00493_A1725ContratoSistemas_CntCod, T00493_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T00494_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               T00495_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T00496_A1725ContratoSistemas_CntCod, T00496_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T00497_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               T00498_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T00499_A1725ContratoSistemas_CntCod, T00499_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T004910_A1725ContratoSistemas_CntCod, T004910_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T004911_A1725ContratoSistemas_CntCod, T004911_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004914_A1725ContratoSistemas_CntCod, T004914_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T004915_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               T004916_A1726ContratoSistemas_SistemaCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound189 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1725ContratoSistemas_CntCod ;
      private int Z1726ContratoSistemas_SistemaCod ;
      private int A1725ContratoSistemas_CntCod ;
      private int A1726ContratoSistemas_SistemaCod ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoSistemas_CntCod_Enabled ;
      private int edtContratoSistemas_SistemaCod_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoSistemas_CntCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratosistemas_cntcod_Internalname ;
      private String lblTextblockcontratosistemas_cntcod_Jsonclick ;
      private String edtContratoSistemas_CntCod_Jsonclick ;
      private String lblTextblockcontratosistemas_sistemacod_Internalname ;
      private String lblTextblockcontratosistemas_sistemacod_Jsonclick ;
      private String edtContratoSistemas_SistemaCod_Internalname ;
      private String edtContratoSistemas_SistemaCod_Jsonclick ;
      private String Gx_mode ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode189 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00496_A1725ContratoSistemas_CntCod ;
      private int[] T00496_A1726ContratoSistemas_SistemaCod ;
      private int[] T00494_A1725ContratoSistemas_CntCod ;
      private int[] T00495_A1726ContratoSistemas_SistemaCod ;
      private int[] T00497_A1725ContratoSistemas_CntCod ;
      private int[] T00498_A1726ContratoSistemas_SistemaCod ;
      private int[] T00499_A1725ContratoSistemas_CntCod ;
      private int[] T00499_A1726ContratoSistemas_SistemaCod ;
      private int[] T00493_A1725ContratoSistemas_CntCod ;
      private int[] T00493_A1726ContratoSistemas_SistemaCod ;
      private int[] T004910_A1725ContratoSistemas_CntCod ;
      private int[] T004910_A1726ContratoSistemas_SistemaCod ;
      private int[] T004911_A1725ContratoSistemas_CntCod ;
      private int[] T004911_A1726ContratoSistemas_SistemaCod ;
      private int[] T00492_A1725ContratoSistemas_CntCod ;
      private int[] T00492_A1726ContratoSistemas_SistemaCod ;
      private int[] T004914_A1725ContratoSistemas_CntCod ;
      private int[] T004914_A1726ContratoSistemas_SistemaCod ;
      private int[] T004915_A1725ContratoSistemas_CntCod ;
      private int[] T004916_A1726ContratoSistemas_SistemaCod ;
      private GXWebForm Form ;
   }

   public class contratosistemas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00496 ;
          prmT00496 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00494 ;
          prmT00494 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00495 ;
          prmT00495 = new Object[] {
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00497 ;
          prmT00497 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00498 ;
          prmT00498 = new Object[] {
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00499 ;
          prmT00499 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00493 ;
          prmT00493 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004910 ;
          prmT004910 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004911 ;
          prmT004911 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00492 ;
          prmT00492 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004912 ;
          prmT004912 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004913 ;
          prmT004913 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004914 ;
          prmT004914 = new Object[] {
          } ;
          Object[] prmT004915 ;
          prmT004915 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004916 ;
          prmT004916 = new Object[] {
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00492", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (UPDLOCK) WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00492,1,0,true,false )
             ,new CursorDef("T00493", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00493,1,0,true,false )
             ,new CursorDef("T00494", "SELECT [Contrato_Codigo] AS ContratoSistemas_CntCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSistemas_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00494,1,0,true,false )
             ,new CursorDef("T00495", "SELECT [Sistema_Codigo] AS ContratoSistemas_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00495,1,0,true,false )
             ,new CursorDef("T00496", "SELECT TM1.[ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, TM1.[ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] TM1 WITH (NOLOCK) WHERE TM1.[ContratoSistemas_CntCod] = @ContratoSistemas_CntCod and TM1.[ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ORDER BY TM1.[ContratoSistemas_CntCod], TM1.[ContratoSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00496,100,0,true,false )
             ,new CursorDef("T00497", "SELECT [Contrato_Codigo] AS ContratoSistemas_CntCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSistemas_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00497,1,0,true,false )
             ,new CursorDef("T00498", "SELECT [Sistema_Codigo] AS ContratoSistemas_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00498,1,0,true,false )
             ,new CursorDef("T00499", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00499,1,0,true,false )
             ,new CursorDef("T004910", "SELECT TOP 1 [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) WHERE ( [ContratoSistemas_CntCod] > @ContratoSistemas_CntCod or [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod and [ContratoSistemas_SistemaCod] > @ContratoSistemas_SistemaCod) ORDER BY [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004910,1,0,true,true )
             ,new CursorDef("T004911", "SELECT TOP 1 [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) WHERE ( [ContratoSistemas_CntCod] < @ContratoSistemas_CntCod or [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod and [ContratoSistemas_SistemaCod] < @ContratoSistemas_SistemaCod) ORDER BY [ContratoSistemas_CntCod] DESC, [ContratoSistemas_SistemaCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004911,1,0,true,true )
             ,new CursorDef("T004912", "INSERT INTO [ContratoSistemas]([ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod]) VALUES(@ContratoSistemas_CntCod, @ContratoSistemas_SistemaCod)", GxErrorMask.GX_NOMASK,prmT004912)
             ,new CursorDef("T004913", "DELETE FROM [ContratoSistemas]  WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod", GxErrorMask.GX_NOMASK,prmT004913)
             ,new CursorDef("T004914", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) ORDER BY [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004914,100,0,true,false )
             ,new CursorDef("T004915", "SELECT [Contrato_Codigo] AS ContratoSistemas_CntCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSistemas_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004915,1,0,true,false )
             ,new CursorDef("T004916", "SELECT [Sistema_Codigo] AS ContratoSistemas_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004916,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
