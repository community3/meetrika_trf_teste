/*
               File: ContagemHistorico
        Description: Contagem Historico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:6.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemhistorico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1156ContagemHistorico_Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1156ContagemHistorico_Contagem_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Historico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemhistorico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemhistorico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_38136( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_38136e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_38136( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_38136( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_38136e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Historico", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemHistorico.htm");
            wb_table3_28_38136( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_38136e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_38136e( true) ;
         }
         else
         {
            wb_table1_2_38136e( false) ;
         }
      }

      protected void wb_table3_28_38136( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_38136( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_38136e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemHistorico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemHistorico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_38136e( true) ;
         }
         else
         {
            wb_table3_28_38136e( false) ;
         }
      }

      protected void wb_table4_34_38136( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemhistorico_codigo_Internalname, "Codigo", "", "", lblTextblockcontagemhistorico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemHistorico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1157ContagemHistorico_Codigo), 6, 0, ",", "")), ((edtContagemHistorico_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1157ContagemHistorico_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1157ContagemHistorico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemHistorico_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemHistorico_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemhistorico_usuariocod_Internalname, "Usuario", "", "", lblTextblockcontagemhistorico_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemHistorico_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0, ",", "")), ((edtContagemHistorico_UsuarioCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1155ContagemHistorico_UsuarioCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1155ContagemHistorico_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemHistorico_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemHistorico_UsuarioCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemhistorico_data_Internalname, "Data", "", "", lblTextblockcontagemhistorico_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemHistorico_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemHistorico_Data_Internalname, context.localUtil.TToC( A1158ContagemHistorico_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1158ContagemHistorico_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemHistorico_Data_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemHistorico_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemHistorico.htm");
            GxWebStd.gx_bitmap( context, edtContagemHistorico_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemHistorico_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemhistorico_antes_Internalname, "Antes", "", "", lblTextblockcontagemhistorico_antes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemHistorico_Antes_Internalname, A1159ContagemHistorico_Antes, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, 1, edtContagemHistorico_Antes_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemhistorico_depois_Internalname, "Depois", "", "", lblTextblockcontagemhistorico_depois_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemHistorico_Depois_Internalname, A1160ContagemHistorico_Depois, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtContagemHistorico_Depois_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemhistorico_contagem_codigo_Internalname, "Contagem", "", "", lblTextblockcontagemhistorico_contagem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemHistorico_Contagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0, ",", "")), ((edtContagemHistorico_Contagem_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1156ContagemHistorico_Contagem_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1156ContagemHistorico_Contagem_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemHistorico_Contagem_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemHistorico_Contagem_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_contratadacod_Internalname, "Contratada", "", "", lblTextblockcontagem_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ",", "")), ((edtContagem_ContratadaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ContratadaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagem_ContratadaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemHistorico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_38136e( true) ;
         }
         else
         {
            wb_table4_34_38136e( false) ;
         }
      }

      protected void wb_table2_5_38136( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemHistorico.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_38136e( true) ;
         }
         else
         {
            wb_table2_5_38136e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemHistorico_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemHistorico_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMHISTORICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1157ContagemHistorico_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
               }
               else
               {
                  A1157ContagemHistorico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemHistorico_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemHistorico_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemHistorico_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMHISTORICO_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1155ContagemHistorico_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1155ContagemHistorico_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0)));
               }
               else
               {
                  A1155ContagemHistorico_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemHistorico_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1155ContagemHistorico_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemHistorico_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "CONTAGEMHISTORICO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemHistorico_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1158ContagemHistorico_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1158ContagemHistorico_Data", context.localUtil.TToC( A1158ContagemHistorico_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1158ContagemHistorico_Data = context.localUtil.CToT( cgiGet( edtContagemHistorico_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1158ContagemHistorico_Data", context.localUtil.TToC( A1158ContagemHistorico_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               A1159ContagemHistorico_Antes = cgiGet( edtContagemHistorico_Antes_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1159ContagemHistorico_Antes", A1159ContagemHistorico_Antes);
               A1160ContagemHistorico_Depois = cgiGet( edtContagemHistorico_Depois_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1160ContagemHistorico_Depois", A1160ContagemHistorico_Depois);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemHistorico_Contagem_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemHistorico_Contagem_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMHISTORICO_CONTAGEM_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemHistorico_Contagem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1156ContagemHistorico_Contagem_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0)));
               }
               else
               {
                  A1156ContagemHistorico_Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemHistorico_Contagem_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0)));
               }
               A1118Contagem_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ContratadaCod_Internalname), ",", "."));
               n1118Contagem_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
               /* Read saved values. */
               Z1157ContagemHistorico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1157ContagemHistorico_Codigo"), ",", "."));
               Z1155ContagemHistorico_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z1155ContagemHistorico_UsuarioCod"), ",", "."));
               Z1158ContagemHistorico_Data = context.localUtil.CToT( cgiGet( "Z1158ContagemHistorico_Data"), 0);
               Z1156ContagemHistorico_Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1156ContagemHistorico_Contagem_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1157ContagemHistorico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll38136( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes38136( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption380( )
      {
      }

      protected void ZM38136( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1155ContagemHistorico_UsuarioCod = T00383_A1155ContagemHistorico_UsuarioCod[0];
               Z1158ContagemHistorico_Data = T00383_A1158ContagemHistorico_Data[0];
               Z1156ContagemHistorico_Contagem_Codigo = T00383_A1156ContagemHistorico_Contagem_Codigo[0];
            }
            else
            {
               Z1155ContagemHistorico_UsuarioCod = A1155ContagemHistorico_UsuarioCod;
               Z1158ContagemHistorico_Data = A1158ContagemHistorico_Data;
               Z1156ContagemHistorico_Contagem_Codigo = A1156ContagemHistorico_Contagem_Codigo;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1157ContagemHistorico_Codigo = A1157ContagemHistorico_Codigo;
            Z1155ContagemHistorico_UsuarioCod = A1155ContagemHistorico_UsuarioCod;
            Z1158ContagemHistorico_Data = A1158ContagemHistorico_Data;
            Z1159ContagemHistorico_Antes = A1159ContagemHistorico_Antes;
            Z1160ContagemHistorico_Depois = A1160ContagemHistorico_Depois;
            Z1156ContagemHistorico_Contagem_Codigo = A1156ContagemHistorico_Contagem_Codigo;
            Z1118Contagem_ContratadaCod = A1118Contagem_ContratadaCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load38136( )
      {
         /* Using cursor T00385 */
         pr_default.execute(3, new Object[] {A1157ContagemHistorico_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound136 = 1;
            A1155ContagemHistorico_UsuarioCod = T00385_A1155ContagemHistorico_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1155ContagemHistorico_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0)));
            A1158ContagemHistorico_Data = T00385_A1158ContagemHistorico_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1158ContagemHistorico_Data", context.localUtil.TToC( A1158ContagemHistorico_Data, 8, 5, 0, 3, "/", ":", " "));
            A1159ContagemHistorico_Antes = T00385_A1159ContagemHistorico_Antes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1159ContagemHistorico_Antes", A1159ContagemHistorico_Antes);
            A1160ContagemHistorico_Depois = T00385_A1160ContagemHistorico_Depois[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1160ContagemHistorico_Depois", A1160ContagemHistorico_Depois);
            A1118Contagem_ContratadaCod = T00385_A1118Contagem_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
            n1118Contagem_ContratadaCod = T00385_n1118Contagem_ContratadaCod[0];
            A1156ContagemHistorico_Contagem_Codigo = T00385_A1156ContagemHistorico_Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0)));
            ZM38136( -2) ;
         }
         pr_default.close(3);
         OnLoadActions38136( ) ;
      }

      protected void OnLoadActions38136( )
      {
      }

      protected void CheckExtendedTable38136( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1158ContagemHistorico_Data) || ( A1158ContagemHistorico_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMHISTORICO_DATA");
            AnyError = 1;
            GX_FocusControl = edtContagemHistorico_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00384 */
         pr_default.execute(2, new Object[] {A1156ContagemHistorico_Contagem_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Historico_Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMHISTORICO_CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemHistorico_Contagem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1118Contagem_ContratadaCod = T00384_A1118Contagem_ContratadaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
         n1118Contagem_ContratadaCod = T00384_n1118Contagem_ContratadaCod[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors38136( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A1156ContagemHistorico_Contagem_Codigo )
      {
         /* Using cursor T00386 */
         pr_default.execute(4, new Object[] {A1156ContagemHistorico_Contagem_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Historico_Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMHISTORICO_CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemHistorico_Contagem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1118Contagem_ContratadaCod = T00386_A1118Contagem_ContratadaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
         n1118Contagem_ContratadaCod = T00386_n1118Contagem_ContratadaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey38136( )
      {
         /* Using cursor T00387 */
         pr_default.execute(5, new Object[] {A1157ContagemHistorico_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound136 = 1;
         }
         else
         {
            RcdFound136 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00383 */
         pr_default.execute(1, new Object[] {A1157ContagemHistorico_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM38136( 2) ;
            RcdFound136 = 1;
            A1157ContagemHistorico_Codigo = T00383_A1157ContagemHistorico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
            A1155ContagemHistorico_UsuarioCod = T00383_A1155ContagemHistorico_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1155ContagemHistorico_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0)));
            A1158ContagemHistorico_Data = T00383_A1158ContagemHistorico_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1158ContagemHistorico_Data", context.localUtil.TToC( A1158ContagemHistorico_Data, 8, 5, 0, 3, "/", ":", " "));
            A1159ContagemHistorico_Antes = T00383_A1159ContagemHistorico_Antes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1159ContagemHistorico_Antes", A1159ContagemHistorico_Antes);
            A1160ContagemHistorico_Depois = T00383_A1160ContagemHistorico_Depois[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1160ContagemHistorico_Depois", A1160ContagemHistorico_Depois);
            A1156ContagemHistorico_Contagem_Codigo = T00383_A1156ContagemHistorico_Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0)));
            Z1157ContagemHistorico_Codigo = A1157ContagemHistorico_Codigo;
            sMode136 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load38136( ) ;
            if ( AnyError == 1 )
            {
               RcdFound136 = 0;
               InitializeNonKey38136( ) ;
            }
            Gx_mode = sMode136;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound136 = 0;
            InitializeNonKey38136( ) ;
            sMode136 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode136;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey38136( ) ;
         if ( RcdFound136 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound136 = 0;
         /* Using cursor T00388 */
         pr_default.execute(6, new Object[] {A1157ContagemHistorico_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00388_A1157ContagemHistorico_Codigo[0] < A1157ContagemHistorico_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00388_A1157ContagemHistorico_Codigo[0] > A1157ContagemHistorico_Codigo ) ) )
            {
               A1157ContagemHistorico_Codigo = T00388_A1157ContagemHistorico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
               RcdFound136 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound136 = 0;
         /* Using cursor T00389 */
         pr_default.execute(7, new Object[] {A1157ContagemHistorico_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00389_A1157ContagemHistorico_Codigo[0] > A1157ContagemHistorico_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00389_A1157ContagemHistorico_Codigo[0] < A1157ContagemHistorico_Codigo ) ) )
            {
               A1157ContagemHistorico_Codigo = T00389_A1157ContagemHistorico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
               RcdFound136 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey38136( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert38136( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound136 == 1 )
            {
               if ( A1157ContagemHistorico_Codigo != Z1157ContagemHistorico_Codigo )
               {
                  A1157ContagemHistorico_Codigo = Z1157ContagemHistorico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMHISTORICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update38136( ) ;
                  GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1157ContagemHistorico_Codigo != Z1157ContagemHistorico_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert38136( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMHISTORICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert38136( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1157ContagemHistorico_Codigo != Z1157ContagemHistorico_Codigo )
         {
            A1157ContagemHistorico_Codigo = Z1157ContagemHistorico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMHISTORICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound136 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMHISTORICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemHistorico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart38136( ) ;
         if ( RcdFound136 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd38136( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound136 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound136 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart38136( ) ;
         if ( RcdFound136 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound136 != 0 )
            {
               ScanNext38136( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd38136( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency38136( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00382 */
            pr_default.execute(0, new Object[] {A1157ContagemHistorico_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemHistorico"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1155ContagemHistorico_UsuarioCod != T00382_A1155ContagemHistorico_UsuarioCod[0] ) || ( Z1158ContagemHistorico_Data != T00382_A1158ContagemHistorico_Data[0] ) || ( Z1156ContagemHistorico_Contagem_Codigo != T00382_A1156ContagemHistorico_Contagem_Codigo[0] ) )
            {
               if ( Z1155ContagemHistorico_UsuarioCod != T00382_A1155ContagemHistorico_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("contagemhistorico:[seudo value changed for attri]"+"ContagemHistorico_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z1155ContagemHistorico_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T00382_A1155ContagemHistorico_UsuarioCod[0]);
               }
               if ( Z1158ContagemHistorico_Data != T00382_A1158ContagemHistorico_Data[0] )
               {
                  GXUtil.WriteLog("contagemhistorico:[seudo value changed for attri]"+"ContagemHistorico_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1158ContagemHistorico_Data);
                  GXUtil.WriteLogRaw("Current: ",T00382_A1158ContagemHistorico_Data[0]);
               }
               if ( Z1156ContagemHistorico_Contagem_Codigo != T00382_A1156ContagemHistorico_Contagem_Codigo[0] )
               {
                  GXUtil.WriteLog("contagemhistorico:[seudo value changed for attri]"+"ContagemHistorico_Contagem_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1156ContagemHistorico_Contagem_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00382_A1156ContagemHistorico_Contagem_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemHistorico"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert38136( )
      {
         BeforeValidate38136( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable38136( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM38136( 0) ;
            CheckOptimisticConcurrency38136( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm38136( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert38136( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003810 */
                     pr_default.execute(8, new Object[] {A1157ContagemHistorico_Codigo, A1155ContagemHistorico_UsuarioCod, A1158ContagemHistorico_Data, A1159ContagemHistorico_Antes, A1160ContagemHistorico_Depois, A1156ContagemHistorico_Contagem_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemHistorico") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption380( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load38136( ) ;
            }
            EndLevel38136( ) ;
         }
         CloseExtendedTableCursors38136( ) ;
      }

      protected void Update38136( )
      {
         BeforeValidate38136( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable38136( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency38136( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm38136( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate38136( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003811 */
                     pr_default.execute(9, new Object[] {A1155ContagemHistorico_UsuarioCod, A1158ContagemHistorico_Data, A1159ContagemHistorico_Antes, A1160ContagemHistorico_Depois, A1156ContagemHistorico_Contagem_Codigo, A1157ContagemHistorico_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemHistorico") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemHistorico"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate38136( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption380( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel38136( ) ;
         }
         CloseExtendedTableCursors38136( ) ;
      }

      protected void DeferredUpdate38136( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate38136( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency38136( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls38136( ) ;
            AfterConfirm38136( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete38136( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003812 */
                  pr_default.execute(10, new Object[] {A1157ContagemHistorico_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemHistorico") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound136 == 0 )
                        {
                           InitAll38136( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption380( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode136 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel38136( ) ;
         Gx_mode = sMode136;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls38136( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003813 */
            pr_default.execute(11, new Object[] {A1156ContagemHistorico_Contagem_Codigo});
            A1118Contagem_ContratadaCod = T003813_A1118Contagem_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
            n1118Contagem_ContratadaCod = T003813_n1118Contagem_ContratadaCod[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel38136( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete38136( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "ContagemHistorico");
            if ( AnyError == 0 )
            {
               ConfirmValues380( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "ContagemHistorico");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart38136( )
      {
         /* Using cursor T003814 */
         pr_default.execute(12);
         RcdFound136 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound136 = 1;
            A1157ContagemHistorico_Codigo = T003814_A1157ContagemHistorico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext38136( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound136 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound136 = 1;
            A1157ContagemHistorico_Codigo = T003814_A1157ContagemHistorico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd38136( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm38136( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert38136( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate38136( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete38136( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete38136( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate38136( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes38136( )
      {
         edtContagemHistorico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemHistorico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemHistorico_Codigo_Enabled), 5, 0)));
         edtContagemHistorico_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemHistorico_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemHistorico_UsuarioCod_Enabled), 5, 0)));
         edtContagemHistorico_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemHistorico_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemHistorico_Data_Enabled), 5, 0)));
         edtContagemHistorico_Antes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemHistorico_Antes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemHistorico_Antes_Enabled), 5, 0)));
         edtContagemHistorico_Depois_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemHistorico_Depois_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemHistorico_Depois_Enabled), 5, 0)));
         edtContagemHistorico_Contagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemHistorico_Contagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemHistorico_Contagem_Codigo_Enabled), 5, 0)));
         edtContagem_ContratadaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ContratadaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues380( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529931727");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemhistorico.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1157ContagemHistorico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1155ContagemHistorico_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1155ContagemHistorico_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1158ContagemHistorico_Data", context.localUtil.TToC( Z1158ContagemHistorico_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1156ContagemHistorico_Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemhistorico.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemHistorico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Historico" ;
      }

      protected void InitializeNonKey38136( )
      {
         A1155ContagemHistorico_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1155ContagemHistorico_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0)));
         A1158ContagemHistorico_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1158ContagemHistorico_Data", context.localUtil.TToC( A1158ContagemHistorico_Data, 8, 5, 0, 3, "/", ":", " "));
         A1159ContagemHistorico_Antes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1159ContagemHistorico_Antes", A1159ContagemHistorico_Antes);
         A1160ContagemHistorico_Depois = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1160ContagemHistorico_Depois", A1160ContagemHistorico_Depois);
         A1156ContagemHistorico_Contagem_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1156ContagemHistorico_Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0)));
         A1118Contagem_ContratadaCod = 0;
         n1118Contagem_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
         Z1155ContagemHistorico_UsuarioCod = 0;
         Z1158ContagemHistorico_Data = (DateTime)(DateTime.MinValue);
         Z1156ContagemHistorico_Contagem_Codigo = 0;
      }

      protected void InitAll38136( )
      {
         A1157ContagemHistorico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1157ContagemHistorico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1157ContagemHistorico_Codigo), 6, 0)));
         InitializeNonKey38136( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529931733");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemhistorico.js", "?2020529931733");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemhistorico_codigo_Internalname = "TEXTBLOCKCONTAGEMHISTORICO_CODIGO";
         edtContagemHistorico_Codigo_Internalname = "CONTAGEMHISTORICO_CODIGO";
         lblTextblockcontagemhistorico_usuariocod_Internalname = "TEXTBLOCKCONTAGEMHISTORICO_USUARIOCOD";
         edtContagemHistorico_UsuarioCod_Internalname = "CONTAGEMHISTORICO_USUARIOCOD";
         lblTextblockcontagemhistorico_data_Internalname = "TEXTBLOCKCONTAGEMHISTORICO_DATA";
         edtContagemHistorico_Data_Internalname = "CONTAGEMHISTORICO_DATA";
         lblTextblockcontagemhistorico_antes_Internalname = "TEXTBLOCKCONTAGEMHISTORICO_ANTES";
         edtContagemHistorico_Antes_Internalname = "CONTAGEMHISTORICO_ANTES";
         lblTextblockcontagemhistorico_depois_Internalname = "TEXTBLOCKCONTAGEMHISTORICO_DEPOIS";
         edtContagemHistorico_Depois_Internalname = "CONTAGEMHISTORICO_DEPOIS";
         lblTextblockcontagemhistorico_contagem_codigo_Internalname = "TEXTBLOCKCONTAGEMHISTORICO_CONTAGEM_CODIGO";
         edtContagemHistorico_Contagem_Codigo_Internalname = "CONTAGEMHISTORICO_CONTAGEM_CODIGO";
         lblTextblockcontagem_contratadacod_Internalname = "TEXTBLOCKCONTAGEM_CONTRATADACOD";
         edtContagem_ContratadaCod_Internalname = "CONTAGEM_CONTRATADACOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Historico";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagem_ContratadaCod_Jsonclick = "";
         edtContagem_ContratadaCod_Enabled = 0;
         edtContagemHistorico_Contagem_Codigo_Jsonclick = "";
         edtContagemHistorico_Contagem_Codigo_Enabled = 1;
         edtContagemHistorico_Depois_Enabled = 1;
         edtContagemHistorico_Antes_Enabled = 1;
         edtContagemHistorico_Data_Jsonclick = "";
         edtContagemHistorico_Data_Enabled = 1;
         edtContagemHistorico_UsuarioCod_Jsonclick = "";
         edtContagemHistorico_UsuarioCod_Enabled = 1;
         edtContagemHistorico_Codigo_Jsonclick = "";
         edtContagemHistorico_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemHistorico_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemhistorico_codigo( int GX_Parm1 ,
                                                  int GX_Parm2 ,
                                                  DateTime GX_Parm3 ,
                                                  String GX_Parm4 ,
                                                  String GX_Parm5 ,
                                                  int GX_Parm6 )
      {
         A1157ContagemHistorico_Codigo = GX_Parm1;
         A1155ContagemHistorico_UsuarioCod = GX_Parm2;
         A1158ContagemHistorico_Data = GX_Parm3;
         A1159ContagemHistorico_Antes = GX_Parm4;
         A1160ContagemHistorico_Depois = GX_Parm5;
         A1156ContagemHistorico_Contagem_Codigo = GX_Parm6;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1118Contagem_ContratadaCod = 0;
            n1118Contagem_ContratadaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1155ContagemHistorico_UsuarioCod), 6, 0, ".", "")));
         isValidOutput.Add(context.localUtil.TToC( A1158ContagemHistorico_Data, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(A1159ContagemHistorico_Antes);
         isValidOutput.Add(A1160ContagemHistorico_Depois);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1156ContagemHistorico_Contagem_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1157ContagemHistorico_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1155ContagemHistorico_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1158ContagemHistorico_Data, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(Z1159ContagemHistorico_Antes);
         isValidOutput.Add(Z1160ContagemHistorico_Depois);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1156ContagemHistorico_Contagem_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1118Contagem_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemhistorico_contagem_codigo( int GX_Parm1 ,
                                                           int GX_Parm2 )
      {
         A1156ContagemHistorico_Contagem_Codigo = GX_Parm1;
         A1118Contagem_ContratadaCod = GX_Parm2;
         n1118Contagem_ContratadaCod = false;
         /* Using cursor T003813 */
         pr_default.execute(11, new Object[] {A1156ContagemHistorico_Contagem_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Historico_Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMHISTORICO_CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemHistorico_Contagem_Codigo_Internalname;
         }
         A1118Contagem_ContratadaCod = T003813_A1118Contagem_ContratadaCod[0];
         n1118Contagem_ContratadaCod = T003813_n1118Contagem_ContratadaCod[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1118Contagem_ContratadaCod = 0;
            n1118Contagem_ContratadaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1158ContagemHistorico_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemhistorico_codigo_Jsonclick = "";
         lblTextblockcontagemhistorico_usuariocod_Jsonclick = "";
         lblTextblockcontagemhistorico_data_Jsonclick = "";
         A1158ContagemHistorico_Data = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemhistorico_antes_Jsonclick = "";
         A1159ContagemHistorico_Antes = "";
         lblTextblockcontagemhistorico_depois_Jsonclick = "";
         A1160ContagemHistorico_Depois = "";
         lblTextblockcontagemhistorico_contagem_codigo_Jsonclick = "";
         lblTextblockcontagem_contratadacod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1159ContagemHistorico_Antes = "";
         Z1160ContagemHistorico_Depois = "";
         T00385_A1157ContagemHistorico_Codigo = new int[1] ;
         T00385_A1155ContagemHistorico_UsuarioCod = new int[1] ;
         T00385_A1158ContagemHistorico_Data = new DateTime[] {DateTime.MinValue} ;
         T00385_A1159ContagemHistorico_Antes = new String[] {""} ;
         T00385_A1160ContagemHistorico_Depois = new String[] {""} ;
         T00385_A1118Contagem_ContratadaCod = new int[1] ;
         T00385_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T00385_A1156ContagemHistorico_Contagem_Codigo = new int[1] ;
         T00384_A1118Contagem_ContratadaCod = new int[1] ;
         T00384_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T00386_A1118Contagem_ContratadaCod = new int[1] ;
         T00386_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T00387_A1157ContagemHistorico_Codigo = new int[1] ;
         T00383_A1157ContagemHistorico_Codigo = new int[1] ;
         T00383_A1155ContagemHistorico_UsuarioCod = new int[1] ;
         T00383_A1158ContagemHistorico_Data = new DateTime[] {DateTime.MinValue} ;
         T00383_A1159ContagemHistorico_Antes = new String[] {""} ;
         T00383_A1160ContagemHistorico_Depois = new String[] {""} ;
         T00383_A1156ContagemHistorico_Contagem_Codigo = new int[1] ;
         sMode136 = "";
         T00388_A1157ContagemHistorico_Codigo = new int[1] ;
         T00389_A1157ContagemHistorico_Codigo = new int[1] ;
         T00382_A1157ContagemHistorico_Codigo = new int[1] ;
         T00382_A1155ContagemHistorico_UsuarioCod = new int[1] ;
         T00382_A1158ContagemHistorico_Data = new DateTime[] {DateTime.MinValue} ;
         T00382_A1159ContagemHistorico_Antes = new String[] {""} ;
         T00382_A1160ContagemHistorico_Depois = new String[] {""} ;
         T00382_A1156ContagemHistorico_Contagem_Codigo = new int[1] ;
         T003813_A1118Contagem_ContratadaCod = new int[1] ;
         T003813_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T003814_A1157ContagemHistorico_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemhistorico__default(),
            new Object[][] {
                new Object[] {
               T00382_A1157ContagemHistorico_Codigo, T00382_A1155ContagemHistorico_UsuarioCod, T00382_A1158ContagemHistorico_Data, T00382_A1159ContagemHistorico_Antes, T00382_A1160ContagemHistorico_Depois, T00382_A1156ContagemHistorico_Contagem_Codigo
               }
               , new Object[] {
               T00383_A1157ContagemHistorico_Codigo, T00383_A1155ContagemHistorico_UsuarioCod, T00383_A1158ContagemHistorico_Data, T00383_A1159ContagemHistorico_Antes, T00383_A1160ContagemHistorico_Depois, T00383_A1156ContagemHistorico_Contagem_Codigo
               }
               , new Object[] {
               T00384_A1118Contagem_ContratadaCod, T00384_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               T00385_A1157ContagemHistorico_Codigo, T00385_A1155ContagemHistorico_UsuarioCod, T00385_A1158ContagemHistorico_Data, T00385_A1159ContagemHistorico_Antes, T00385_A1160ContagemHistorico_Depois, T00385_A1118Contagem_ContratadaCod, T00385_n1118Contagem_ContratadaCod, T00385_A1156ContagemHistorico_Contagem_Codigo
               }
               , new Object[] {
               T00386_A1118Contagem_ContratadaCod, T00386_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               T00387_A1157ContagemHistorico_Codigo
               }
               , new Object[] {
               T00388_A1157ContagemHistorico_Codigo
               }
               , new Object[] {
               T00389_A1157ContagemHistorico_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003813_A1118Contagem_ContratadaCod, T003813_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               T003814_A1157ContagemHistorico_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound136 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1157ContagemHistorico_Codigo ;
      private int Z1155ContagemHistorico_UsuarioCod ;
      private int Z1156ContagemHistorico_Contagem_Codigo ;
      private int A1156ContagemHistorico_Contagem_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1157ContagemHistorico_Codigo ;
      private int edtContagemHistorico_Codigo_Enabled ;
      private int A1155ContagemHistorico_UsuarioCod ;
      private int edtContagemHistorico_UsuarioCod_Enabled ;
      private int edtContagemHistorico_Data_Enabled ;
      private int edtContagemHistorico_Antes_Enabled ;
      private int edtContagemHistorico_Depois_Enabled ;
      private int edtContagemHistorico_Contagem_Codigo_Enabled ;
      private int A1118Contagem_ContratadaCod ;
      private int edtContagem_ContratadaCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z1118Contagem_ContratadaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemHistorico_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemhistorico_codigo_Internalname ;
      private String lblTextblockcontagemhistorico_codigo_Jsonclick ;
      private String edtContagemHistorico_Codigo_Jsonclick ;
      private String lblTextblockcontagemhistorico_usuariocod_Internalname ;
      private String lblTextblockcontagemhistorico_usuariocod_Jsonclick ;
      private String edtContagemHistorico_UsuarioCod_Internalname ;
      private String edtContagemHistorico_UsuarioCod_Jsonclick ;
      private String lblTextblockcontagemhistorico_data_Internalname ;
      private String lblTextblockcontagemhistorico_data_Jsonclick ;
      private String edtContagemHistorico_Data_Internalname ;
      private String edtContagemHistorico_Data_Jsonclick ;
      private String lblTextblockcontagemhistorico_antes_Internalname ;
      private String lblTextblockcontagemhistorico_antes_Jsonclick ;
      private String edtContagemHistorico_Antes_Internalname ;
      private String lblTextblockcontagemhistorico_depois_Internalname ;
      private String lblTextblockcontagemhistorico_depois_Jsonclick ;
      private String edtContagemHistorico_Depois_Internalname ;
      private String lblTextblockcontagemhistorico_contagem_codigo_Internalname ;
      private String lblTextblockcontagemhistorico_contagem_codigo_Jsonclick ;
      private String edtContagemHistorico_Contagem_Codigo_Internalname ;
      private String edtContagemHistorico_Contagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_contratadacod_Internalname ;
      private String lblTextblockcontagem_contratadacod_Jsonclick ;
      private String edtContagem_ContratadaCod_Internalname ;
      private String edtContagem_ContratadaCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode136 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1158ContagemHistorico_Data ;
      private DateTime A1158ContagemHistorico_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1118Contagem_ContratadaCod ;
      private String A1159ContagemHistorico_Antes ;
      private String A1160ContagemHistorico_Depois ;
      private String Z1159ContagemHistorico_Antes ;
      private String Z1160ContagemHistorico_Depois ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00385_A1157ContagemHistorico_Codigo ;
      private int[] T00385_A1155ContagemHistorico_UsuarioCod ;
      private DateTime[] T00385_A1158ContagemHistorico_Data ;
      private String[] T00385_A1159ContagemHistorico_Antes ;
      private String[] T00385_A1160ContagemHistorico_Depois ;
      private int[] T00385_A1118Contagem_ContratadaCod ;
      private bool[] T00385_n1118Contagem_ContratadaCod ;
      private int[] T00385_A1156ContagemHistorico_Contagem_Codigo ;
      private int[] T00384_A1118Contagem_ContratadaCod ;
      private bool[] T00384_n1118Contagem_ContratadaCod ;
      private int[] T00386_A1118Contagem_ContratadaCod ;
      private bool[] T00386_n1118Contagem_ContratadaCod ;
      private int[] T00387_A1157ContagemHistorico_Codigo ;
      private int[] T00383_A1157ContagemHistorico_Codigo ;
      private int[] T00383_A1155ContagemHistorico_UsuarioCod ;
      private DateTime[] T00383_A1158ContagemHistorico_Data ;
      private String[] T00383_A1159ContagemHistorico_Antes ;
      private String[] T00383_A1160ContagemHistorico_Depois ;
      private int[] T00383_A1156ContagemHistorico_Contagem_Codigo ;
      private int[] T00388_A1157ContagemHistorico_Codigo ;
      private int[] T00389_A1157ContagemHistorico_Codigo ;
      private int[] T00382_A1157ContagemHistorico_Codigo ;
      private int[] T00382_A1155ContagemHistorico_UsuarioCod ;
      private DateTime[] T00382_A1158ContagemHistorico_Data ;
      private String[] T00382_A1159ContagemHistorico_Antes ;
      private String[] T00382_A1160ContagemHistorico_Depois ;
      private int[] T00382_A1156ContagemHistorico_Contagem_Codigo ;
      private int[] T003813_A1118Contagem_ContratadaCod ;
      private bool[] T003813_n1118Contagem_ContratadaCod ;
      private int[] T003814_A1157ContagemHistorico_Codigo ;
      private GXWebForm Form ;
   }

   public class contagemhistorico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00385 ;
          prmT00385 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00384 ;
          prmT00384 = new Object[] {
          new Object[] {"@ContagemHistorico_Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00386 ;
          prmT00386 = new Object[] {
          new Object[] {"@ContagemHistorico_Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00387 ;
          prmT00387 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00383 ;
          prmT00383 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00388 ;
          prmT00388 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00389 ;
          prmT00389 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00382 ;
          prmT00382 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003810 ;
          prmT003810 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemHistorico_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemHistorico_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemHistorico_Antes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemHistorico_Depois",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemHistorico_Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003811 ;
          prmT003811 = new Object[] {
          new Object[] {"@ContagemHistorico_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemHistorico_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemHistorico_Antes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemHistorico_Depois",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemHistorico_Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003812 ;
          prmT003812 = new Object[] {
          new Object[] {"@ContagemHistorico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003814 ;
          prmT003814 = new Object[] {
          } ;
          Object[] prmT003813 ;
          prmT003813 = new Object[] {
          new Object[] {"@ContagemHistorico_Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00382", "SELECT [ContagemHistorico_Codigo], [ContagemHistorico_UsuarioCod], [ContagemHistorico_Data], [ContagemHistorico_Antes], [ContagemHistorico_Depois], [ContagemHistorico_Contagem_Codigo] AS ContagemHistorico_Contagem_Codigo FROM [ContagemHistorico] WITH (UPDLOCK) WHERE [ContagemHistorico_Codigo] = @ContagemHistorico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00382,1,0,true,false )
             ,new CursorDef("T00383", "SELECT [ContagemHistorico_Codigo], [ContagemHistorico_UsuarioCod], [ContagemHistorico_Data], [ContagemHistorico_Antes], [ContagemHistorico_Depois], [ContagemHistorico_Contagem_Codigo] AS ContagemHistorico_Contagem_Codigo FROM [ContagemHistorico] WITH (NOLOCK) WHERE [ContagemHistorico_Codigo] = @ContagemHistorico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00383,1,0,true,false )
             ,new CursorDef("T00384", "SELECT [Contagem_ContratadaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @ContagemHistorico_Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00384,1,0,true,false )
             ,new CursorDef("T00385", "SELECT TM1.[ContagemHistorico_Codigo], TM1.[ContagemHistorico_UsuarioCod], TM1.[ContagemHistorico_Data], TM1.[ContagemHistorico_Antes], TM1.[ContagemHistorico_Depois], T2.[Contagem_ContratadaCod], TM1.[ContagemHistorico_Contagem_Codigo] AS ContagemHistorico_Contagem_Codigo FROM ([ContagemHistorico] TM1 WITH (NOLOCK) INNER JOIN [Contagem] T2 WITH (NOLOCK) ON T2.[Contagem_Codigo] = TM1.[ContagemHistorico_Contagem_Codigo]) WHERE TM1.[ContagemHistorico_Codigo] = @ContagemHistorico_Codigo ORDER BY TM1.[ContagemHistorico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00385,100,0,true,false )
             ,new CursorDef("T00386", "SELECT [Contagem_ContratadaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @ContagemHistorico_Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00386,1,0,true,false )
             ,new CursorDef("T00387", "SELECT [ContagemHistorico_Codigo] FROM [ContagemHistorico] WITH (NOLOCK) WHERE [ContagemHistorico_Codigo] = @ContagemHistorico_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00387,1,0,true,false )
             ,new CursorDef("T00388", "SELECT TOP 1 [ContagemHistorico_Codigo] FROM [ContagemHistorico] WITH (NOLOCK) WHERE ( [ContagemHistorico_Codigo] > @ContagemHistorico_Codigo) ORDER BY [ContagemHistorico_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00388,1,0,true,true )
             ,new CursorDef("T00389", "SELECT TOP 1 [ContagemHistorico_Codigo] FROM [ContagemHistorico] WITH (NOLOCK) WHERE ( [ContagemHistorico_Codigo] < @ContagemHistorico_Codigo) ORDER BY [ContagemHistorico_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00389,1,0,true,true )
             ,new CursorDef("T003810", "INSERT INTO [ContagemHistorico]([ContagemHistorico_Codigo], [ContagemHistorico_UsuarioCod], [ContagemHistorico_Data], [ContagemHistorico_Antes], [ContagemHistorico_Depois], [ContagemHistorico_Contagem_Codigo]) VALUES(@ContagemHistorico_Codigo, @ContagemHistorico_UsuarioCod, @ContagemHistorico_Data, @ContagemHistorico_Antes, @ContagemHistorico_Depois, @ContagemHistorico_Contagem_Codigo)", GxErrorMask.GX_NOMASK,prmT003810)
             ,new CursorDef("T003811", "UPDATE [ContagemHistorico] SET [ContagemHistorico_UsuarioCod]=@ContagemHistorico_UsuarioCod, [ContagemHistorico_Data]=@ContagemHistorico_Data, [ContagemHistorico_Antes]=@ContagemHistorico_Antes, [ContagemHistorico_Depois]=@ContagemHistorico_Depois, [ContagemHistorico_Contagem_Codigo]=@ContagemHistorico_Contagem_Codigo  WHERE [ContagemHistorico_Codigo] = @ContagemHistorico_Codigo", GxErrorMask.GX_NOMASK,prmT003811)
             ,new CursorDef("T003812", "DELETE FROM [ContagemHistorico]  WHERE [ContagemHistorico_Codigo] = @ContagemHistorico_Codigo", GxErrorMask.GX_NOMASK,prmT003812)
             ,new CursorDef("T003813", "SELECT [Contagem_ContratadaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @ContagemHistorico_Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003813,1,0,true,false )
             ,new CursorDef("T003814", "SELECT [ContagemHistorico_Codigo] FROM [ContagemHistorico] WITH (NOLOCK) ORDER BY [ContagemHistorico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003814,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
