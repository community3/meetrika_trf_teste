/*
               File: Rel_ContagemGerencial
        Description: Stub for Rel_ContagemGerencial
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:9.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemgerencial : GXProcedure
   {
      public rel_contagemgerencial( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_contagemgerencial( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_Data ,
                           int aP1_ContagemResultado_LoteAceite ,
                           String aP2_Filtro ,
                           DateTime aP3_Inicio ,
                           DateTime aP4_Fim )
      {
         this.AV2Data = aP0_Data;
         this.AV3ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         this.AV4Filtro = aP2_Filtro;
         this.AV5Inicio = aP3_Inicio;
         this.AV6Fim = aP4_Fim;
         initialize();
         executePrivate();
      }

      public void executeSubmit( DateTime aP0_Data ,
                                 int aP1_ContagemResultado_LoteAceite ,
                                 String aP2_Filtro ,
                                 DateTime aP3_Inicio ,
                                 DateTime aP4_Fim )
      {
         rel_contagemgerencial objrel_contagemgerencial;
         objrel_contagemgerencial = new rel_contagemgerencial();
         objrel_contagemgerencial.AV2Data = aP0_Data;
         objrel_contagemgerencial.AV3ContagemResultado_LoteAceite = aP1_ContagemResultado_LoteAceite;
         objrel_contagemgerencial.AV4Filtro = aP2_Filtro;
         objrel_contagemgerencial.AV5Inicio = aP3_Inicio;
         objrel_contagemgerencial.AV6Fim = aP4_Fim;
         objrel_contagemgerencial.context.SetSubmitInitialConfig(context);
         objrel_contagemgerencial.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemgerencial);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemgerencial)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(DateTime)AV2Data,(int)AV3ContagemResultado_LoteAceite,(String)AV4Filtro,(DateTime)AV5Inicio,(DateTime)AV6Fim} ;
         ClassLoader.Execute("arel_contagemgerencial","GeneXus.Programs.arel_contagemgerencial", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 5 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV3ContagemResultado_LoteAceite ;
      private String AV4Filtro ;
      private DateTime AV2Data ;
      private DateTime AV5Inicio ;
      private DateTime AV6Fim ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
