/*
               File: PRC_EncerrarDivergencia
        Description: Realiza o encerramento da Divergência
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:34.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_encerrardivergencia : GXProcedure
   {
      public prc_encerrardivergencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_encerrardivergencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out String aP1_Retorno )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV15Retorno = "" ;
         initialize();
         executePrivate();
         aP1_Retorno=this.AV15Retorno;
      }

      public String executeUdp( int aP0_ContagemResultado_Codigo )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV15Retorno = "" ;
         initialize();
         executePrivate();
         aP1_Retorno=this.AV15Retorno;
         return AV15Retorno ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out String aP1_Retorno )
      {
         prc_encerrardivergencia objprc_encerrardivergencia;
         objprc_encerrardivergencia = new prc_encerrardivergencia();
         objprc_encerrardivergencia.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_encerrardivergencia.AV15Retorno = "" ;
         objprc_encerrardivergencia.context.SetSubmitInitialConfig(context);
         objprc_encerrardivergencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_encerrardivergencia);
         aP1_Retorno=this.AV15Retorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_encerrardivergencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Using cursor P00Y32 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A483ContagemResultado_StatusCnt = P00Y32_A483ContagemResultado_StatusCnt[0];
            A456ContagemResultado_Codigo = P00Y32_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00Y32_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00Y32_n484ContagemResultado_StatusDmn[0];
            A890ContagemResultado_Responsavel = P00Y32_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00Y32_n890ContagemResultado_Responsavel[0];
            A602ContagemResultado_OSVinculada = P00Y32_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00Y32_n602ContagemResultado_OSVinculada[0];
            A511ContagemResultado_HoraCnt = P00Y32_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P00Y32_A473ContagemResultado_DataCnt[0];
            A484ContagemResultado_StatusDmn = P00Y32_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00Y32_n484ContagemResultado_StatusDmn[0];
            A890ContagemResultado_Responsavel = P00Y32_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00Y32_n890ContagemResultado_Responsavel[0];
            A602ContagemResultado_OSVinculada = P00Y32_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00Y32_n602ContagemResultado_OSVinculada[0];
            /* Execute user subroutine: 'REGISTRA.ESTORNO.HISTORICO' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            A483ContagemResultado_StatusCnt = 5;
            A484ContagemResultado_StatusDmn = "R";
            n484ContagemResultado_StatusDmn = false;
            A890ContagemResultado_Responsavel = AV9WWPContext.gxTpr_Userid;
            n890ContagemResultado_Responsavel = false;
            AV14ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            /* Execute user subroutine: 'BUSCA.DEMANDA.VINCULADA' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00Y33 */
            pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P00Y34 */
            pr_default.execute(2, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P00Y35 */
            pr_default.execute(3, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P00Y36 */
            pr_default.execute(4, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         context.CommitDataStores( "PRC_EncerrarDivergencia");
         AV15Retorno = StringUtil.Format( "A Divergência da Demanda foi encerrada.", "", "", "", "", "", "", "", "", "");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'BUSCA.DEMANDA.VINCULADA' Routine */
         /* Using cursor P00Y37 */
         pr_default.execute(5, new Object[] {AV14ContagemResultado_OSVinculada});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A483ContagemResultado_StatusCnt = P00Y37_A483ContagemResultado_StatusCnt[0];
            A456ContagemResultado_Codigo = P00Y37_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00Y37_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00Y37_n484ContagemResultado_StatusDmn[0];
            A890ContagemResultado_Responsavel = P00Y37_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00Y37_n890ContagemResultado_Responsavel[0];
            A511ContagemResultado_HoraCnt = P00Y37_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P00Y37_A473ContagemResultado_DataCnt[0];
            A484ContagemResultado_StatusDmn = P00Y37_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00Y37_n484ContagemResultado_StatusDmn[0];
            A890ContagemResultado_Responsavel = P00Y37_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00Y37_n890ContagemResultado_Responsavel[0];
            /* Execute user subroutine: 'REGISTRA.ESTORNO.HISTORICO' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(5);
               returnInSub = true;
               if (true) return;
            }
            A483ContagemResultado_StatusCnt = 5;
            A484ContagemResultado_StatusDmn = "R";
            n484ContagemResultado_StatusDmn = false;
            A890ContagemResultado_Responsavel = AV9WWPContext.gxTpr_Userid;
            n890ContagemResultado_Responsavel = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00Y38 */
            pr_default.execute(6, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(6);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P00Y39 */
            pr_default.execute(7, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P00Y310 */
            pr_default.execute(8, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P00Y311 */
            pr_default.execute(9, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void S123( )
      {
         /* 'REGISTRA.ESTORNO.HISTORICO' Routine */
         AV11ContagemResultado_Observacao = "Encerrada a divergência pelo Contratante";
         AV10Acao = "D";
         AV13PrazoEntrega = DateTimeUtil.ServerNow( context, "DEFAULT");
         new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV9WWPContext.gxTpr_Userid,  AV10Acao,  "D",  AV9WWPContext.gxTpr_Userid,  0,  A484ContagemResultado_StatusDmn,  "R",  AV11ContagemResultado_Observacao,  AV13PrazoEntrega,  false) ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00Y32_A483ContagemResultado_StatusCnt = new short[1] ;
         P00Y32_A456ContagemResultado_Codigo = new int[1] ;
         P00Y32_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00Y32_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00Y32_A890ContagemResultado_Responsavel = new int[1] ;
         P00Y32_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00Y32_A602ContagemResultado_OSVinculada = new int[1] ;
         P00Y32_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00Y32_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00Y32_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A484ContagemResultado_StatusDmn = "";
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         P00Y37_A483ContagemResultado_StatusCnt = new short[1] ;
         P00Y37_A456ContagemResultado_Codigo = new int[1] ;
         P00Y37_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00Y37_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00Y37_A890ContagemResultado_Responsavel = new int[1] ;
         P00Y37_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00Y37_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00Y37_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         AV11ContagemResultado_Observacao = "";
         AV10Acao = "";
         AV13PrazoEntrega = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_encerrardivergencia__default(),
            new Object[][] {
                new Object[] {
               P00Y32_A483ContagemResultado_StatusCnt, P00Y32_A456ContagemResultado_Codigo, P00Y32_A484ContagemResultado_StatusDmn, P00Y32_n484ContagemResultado_StatusDmn, P00Y32_A890ContagemResultado_Responsavel, P00Y32_n890ContagemResultado_Responsavel, P00Y32_A602ContagemResultado_OSVinculada, P00Y32_n602ContagemResultado_OSVinculada, P00Y32_A511ContagemResultado_HoraCnt, P00Y32_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00Y37_A483ContagemResultado_StatusCnt, P00Y37_A456ContagemResultado_Codigo, P00Y37_A484ContagemResultado_StatusDmn, P00Y37_n484ContagemResultado_StatusDmn, P00Y37_A890ContagemResultado_Responsavel, P00Y37_n890ContagemResultado_Responsavel, P00Y37_A511ContagemResultado_HoraCnt, P00Y37_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A483ContagemResultado_StatusCnt ;
      private int AV8ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A890ContagemResultado_Responsavel ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV14ContagemResultado_OSVinculada ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV10Acao ;
      private DateTime AV13PrazoEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool returnInSub ;
      private String AV11ContagemResultado_Observacao ;
      private String AV15Retorno ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P00Y32_A483ContagemResultado_StatusCnt ;
      private int[] P00Y32_A456ContagemResultado_Codigo ;
      private String[] P00Y32_A484ContagemResultado_StatusDmn ;
      private bool[] P00Y32_n484ContagemResultado_StatusDmn ;
      private int[] P00Y32_A890ContagemResultado_Responsavel ;
      private bool[] P00Y32_n890ContagemResultado_Responsavel ;
      private int[] P00Y32_A602ContagemResultado_OSVinculada ;
      private bool[] P00Y32_n602ContagemResultado_OSVinculada ;
      private String[] P00Y32_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00Y32_A473ContagemResultado_DataCnt ;
      private short[] P00Y37_A483ContagemResultado_StatusCnt ;
      private int[] P00Y37_A456ContagemResultado_Codigo ;
      private String[] P00Y37_A484ContagemResultado_StatusDmn ;
      private bool[] P00Y37_n484ContagemResultado_StatusDmn ;
      private int[] P00Y37_A890ContagemResultado_Responsavel ;
      private bool[] P00Y37_n890ContagemResultado_Responsavel ;
      private String[] P00Y37_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00Y37_A473ContagemResultado_DataCnt ;
      private String aP1_Retorno ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class prc_encerrardivergencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Y32 ;
          prmP00Y32 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y33 ;
          prmP00Y33 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y34 ;
          prmP00Y34 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00Y35 ;
          prmP00Y35 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y36 ;
          prmP00Y36 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00Y37 ;
          prmP00Y37 = new Object[] {
          new Object[] {"@AV14ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y38 ;
          prmP00Y38 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y39 ;
          prmP00Y39 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00Y310 ;
          prmP00Y310 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y311 ;
          prmP00Y311 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Y32", "SELECT TOP 1 T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_Responsavel], T2.[ContagemResultado_OSVinculada], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt] FROM ([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo) AND (T1.[ContagemResultado_StatusCnt] = 7) ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y32,1,0,true,true )
             ,new CursorDef("P00Y33", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y33)
             ,new CursorDef("P00Y34", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y34)
             ,new CursorDef("P00Y35", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y35)
             ,new CursorDef("P00Y36", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y36)
             ,new CursorDef("P00Y37", "SELECT TOP 1 T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_Responsavel], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt] FROM ([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV14ContagemResultado_OSVinculada) AND (T1.[ContagemResultado_StatusCnt] = 7) ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y37,1,0,true,true )
             ,new CursorDef("P00Y38", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y38)
             ,new CursorDef("P00Y39", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y39)
             ,new CursorDef("P00Y310", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y310)
             ,new CursorDef("P00Y311", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00Y311)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 5) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
       }
    }

 }

}
