/*
               File: GetWWCatalogoSutentacaoFilterData
        Description: Get WWCatalogo Sutentacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:47.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcatalogosutentacaofilterdata : GXProcedure
   {
      public getwwcatalogosutentacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcatalogosutentacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcatalogosutentacaofilterdata objgetwwcatalogosutentacaofilterdata;
         objgetwwcatalogosutentacaofilterdata = new getwwcatalogosutentacaofilterdata();
         objgetwwcatalogosutentacaofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcatalogosutentacaofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcatalogosutentacaofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcatalogosutentacaofilterdata.AV22OptionsJson = "" ;
         objgetwwcatalogosutentacaofilterdata.AV25OptionsDescJson = "" ;
         objgetwwcatalogosutentacaofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcatalogosutentacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcatalogosutentacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcatalogosutentacaofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcatalogosutentacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CATALOGOSUTENTACAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCATALOGOSUTENTACAO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CATALOGOSUTENTACAO_ATIVIDADE") == 0 )
         {
            /* Execute user subroutine: 'LOADCATALOGOSUTENTACAO_ATIVIDADEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWCatalogoSutentacaoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWCatalogoSutentacaoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWCatalogoSutentacaoGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_CODIGO") == 0 )
            {
               AV10TFCatalogoSutentacao_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV11TFCatalogoSutentacao_Codigo_To = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_DESCRICAO") == 0 )
            {
               AV12TFCatalogoSutentacao_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_DESCRICAO_SEL") == 0 )
            {
               AV13TFCatalogoSutentacao_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_ATIVIDADE") == 0 )
            {
               AV14TFCatalogoSutentacao_Atividade = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_ATIVIDADE_SEL") == 0 )
            {
               AV15TFCatalogoSutentacao_Atividade_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36CatalogoSutentacao_Descricao1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
               {
                  AV39DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV40CatalogoSutentacao_Descricao2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV44CatalogoSutentacao_Descricao3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCATALOGOSUTENTACAO_DESCRICAOOPTIONS' Routine */
         AV12TFCatalogoSutentacao_Descricao = AV16SearchTxt;
         AV13TFCatalogoSutentacao_Descricao_Sel = "";
         AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV36CatalogoSutentacao_Descricao1;
         AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV39DynamicFiltersOperator2;
         AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV40CatalogoSutentacao_Descricao2;
         AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV43DynamicFiltersOperator3;
         AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV44CatalogoSutentacao_Descricao3;
         AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV10TFCatalogoSutentacao_Codigo;
         AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV11TFCatalogoSutentacao_Codigo_To;
         AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV12TFCatalogoSutentacao_Descricao;
         AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV13TFCatalogoSutentacao_Descricao_Sel;
         AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV14TFCatalogoSutentacao_Atividade;
         AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV15TFCatalogoSutentacao_Atividade_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                              AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                              AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                              AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                              AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                              AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                              AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                              AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                              AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                              AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                              AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                              AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                              AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                              AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                              AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                              AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                              A1752CatalogoSutentacao_Descricao ,
                                              A1750CatalogoSutentacao_Codigo ,
                                              A1753CatalogoSutentacao_Atividade },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
         lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
         lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
         lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
         lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
         lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
         lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao), "%", "");
         lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = StringUtil.Concat( StringUtil.RTrim( AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade), "%", "");
         /* Using cursor P00SI2 */
         pr_default.execute(0, new Object[] {lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo, AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to, lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao, AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel, lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade, AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSI2 = false;
            A1752CatalogoSutentacao_Descricao = P00SI2_A1752CatalogoSutentacao_Descricao[0];
            A1753CatalogoSutentacao_Atividade = P00SI2_A1753CatalogoSutentacao_Atividade[0];
            A1750CatalogoSutentacao_Codigo = P00SI2_A1750CatalogoSutentacao_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SI2_A1752CatalogoSutentacao_Descricao[0], A1752CatalogoSutentacao_Descricao) == 0 ) )
            {
               BRKSI2 = false;
               A1750CatalogoSutentacao_Codigo = P00SI2_A1750CatalogoSutentacao_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKSI2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1752CatalogoSutentacao_Descricao)) )
            {
               AV20Option = A1752CatalogoSutentacao_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSI2 )
            {
               BRKSI2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCATALOGOSUTENTACAO_ATIVIDADEOPTIONS' Routine */
         AV14TFCatalogoSutentacao_Atividade = AV16SearchTxt;
         AV15TFCatalogoSutentacao_Atividade_Sel = "";
         AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV36CatalogoSutentacao_Descricao1;
         AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV39DynamicFiltersOperator2;
         AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV40CatalogoSutentacao_Descricao2;
         AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV43DynamicFiltersOperator3;
         AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV44CatalogoSutentacao_Descricao3;
         AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV10TFCatalogoSutentacao_Codigo;
         AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV11TFCatalogoSutentacao_Codigo_To;
         AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV12TFCatalogoSutentacao_Descricao;
         AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV13TFCatalogoSutentacao_Descricao_Sel;
         AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV14TFCatalogoSutentacao_Atividade;
         AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV15TFCatalogoSutentacao_Atividade_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                              AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                              AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                              AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                              AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                              AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                              AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                              AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                              AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                              AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                              AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                              AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                              AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                              AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                              AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                              AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                              A1752CatalogoSutentacao_Descricao ,
                                              A1750CatalogoSutentacao_Codigo ,
                                              A1753CatalogoSutentacao_Atividade },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
         lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
         lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
         lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
         lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
         lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
         lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao), "%", "");
         lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = StringUtil.Concat( StringUtil.RTrim( AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade), "%", "");
         /* Using cursor P00SI3 */
         pr_default.execute(1, new Object[] {lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo, AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to, lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao, AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel, lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade, AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSI4 = false;
            A1753CatalogoSutentacao_Atividade = P00SI3_A1753CatalogoSutentacao_Atividade[0];
            A1750CatalogoSutentacao_Codigo = P00SI3_A1750CatalogoSutentacao_Codigo[0];
            A1752CatalogoSutentacao_Descricao = P00SI3_A1752CatalogoSutentacao_Descricao[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00SI3_A1753CatalogoSutentacao_Atividade[0], A1753CatalogoSutentacao_Atividade) == 0 ) )
            {
               BRKSI4 = false;
               A1750CatalogoSutentacao_Codigo = P00SI3_A1750CatalogoSutentacao_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKSI4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1753CatalogoSutentacao_Atividade)) )
            {
               AV20Option = A1753CatalogoSutentacao_Atividade;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSI4 )
            {
               BRKSI4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFCatalogoSutentacao_Descricao = "";
         AV13TFCatalogoSutentacao_Descricao_Sel = "";
         AV14TFCatalogoSutentacao_Atividade = "";
         AV15TFCatalogoSutentacao_Atividade_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36CatalogoSutentacao_Descricao1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV40CatalogoSutentacao_Descricao2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV44CatalogoSutentacao_Descricao3 = "";
         AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = "";
         AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = "";
         AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = "";
         AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = "";
         AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = "";
         AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = "";
         AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = "";
         AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = "";
         AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = "";
         AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = "";
         scmdbuf = "";
         lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = "";
         lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = "";
         lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = "";
         lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = "";
         lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = "";
         A1752CatalogoSutentacao_Descricao = "";
         A1753CatalogoSutentacao_Atividade = "";
         P00SI2_A1752CatalogoSutentacao_Descricao = new String[] {""} ;
         P00SI2_A1753CatalogoSutentacao_Atividade = new String[] {""} ;
         P00SI2_A1750CatalogoSutentacao_Codigo = new int[1] ;
         AV20Option = "";
         P00SI3_A1753CatalogoSutentacao_Atividade = new String[] {""} ;
         P00SI3_A1750CatalogoSutentacao_Codigo = new int[1] ;
         P00SI3_A1752CatalogoSutentacao_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcatalogosutentacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SI2_A1752CatalogoSutentacao_Descricao, P00SI2_A1753CatalogoSutentacao_Atividade, P00SI2_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               P00SI3_A1753CatalogoSutentacao_Atividade, P00SI3_A1750CatalogoSutentacao_Codigo, P00SI3_A1752CatalogoSutentacao_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV39DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private short AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ;
      private short AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ;
      private short AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ;
      private int AV47GXV1 ;
      private int AV10TFCatalogoSutentacao_Codigo ;
      private int AV11TFCatalogoSutentacao_Codigo_To ;
      private int AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ;
      private int AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ;
      private int A1750CatalogoSutentacao_Codigo ;
      private long AV28count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ;
      private bool BRKSI2 ;
      private bool BRKSI4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV12TFCatalogoSutentacao_Descricao ;
      private String AV13TFCatalogoSutentacao_Descricao_Sel ;
      private String AV14TFCatalogoSutentacao_Atividade ;
      private String AV15TFCatalogoSutentacao_Atividade_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV36CatalogoSutentacao_Descricao1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV40CatalogoSutentacao_Descricao2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV44CatalogoSutentacao_Descricao3 ;
      private String AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ;
      private String AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ;
      private String AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ;
      private String AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ;
      private String AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ;
      private String AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ;
      private String AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ;
      private String AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ;
      private String AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ;
      private String AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ;
      private String lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ;
      private String lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ;
      private String lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ;
      private String lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ;
      private String lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ;
      private String A1752CatalogoSutentacao_Descricao ;
      private String A1753CatalogoSutentacao_Atividade ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00SI2_A1752CatalogoSutentacao_Descricao ;
      private String[] P00SI2_A1753CatalogoSutentacao_Atividade ;
      private int[] P00SI2_A1750CatalogoSutentacao_Codigo ;
      private String[] P00SI3_A1753CatalogoSutentacao_Atividade ;
      private int[] P00SI3_A1750CatalogoSutentacao_Codigo ;
      private String[] P00SI3_A1752CatalogoSutentacao_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwcatalogosutentacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SI2( IGxContext context ,
                                             String AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                             short AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                             bool AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                             short AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                             bool AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                             short AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                             int AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                             int AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                             String AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                             String AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                             String AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                             String AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                             String A1752CatalogoSutentacao_Descricao ,
                                             int A1750CatalogoSutentacao_Codigo ,
                                             String A1753CatalogoSutentacao_Atividade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [CatalogoSutentacao_Descricao], [CatalogoSutentacao_Atividade], [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] >= @AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] >= @AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] <= @AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] <= @AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] = @AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] = @AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] like @lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] like @lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] = @AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] = @AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [CatalogoSutentacao_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SI3( IGxContext context ,
                                             String AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                             short AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                             bool AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                             short AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                             bool AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                             short AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                             int AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                             int AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                             String AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                             String AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                             String AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                             String AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                             String A1752CatalogoSutentacao_Descricao ,
                                             int A1750CatalogoSutentacao_Codigo ,
                                             String A1753CatalogoSutentacao_Atividade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [CatalogoSutentacao_Atividade], [CatalogoSutentacao_Codigo], [CatalogoSutentacao_Descricao] FROM [CatalogoSutentacao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV50WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV52WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV53WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV54WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV56WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV58WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] >= @AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] >= @AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] <= @AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] <= @AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] = @AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] = @AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] like @lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] like @lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] = @AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] = @AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [CatalogoSutentacao_Atividade]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SI2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] );
               case 1 :
                     return conditional_P00SI3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SI2 ;
          prmP00SI2 = new Object[] {
          new Object[] {"@lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00SI3 ;
          prmP00SI3 = new Object[] {
          new Object[] {"@lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV51WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV62WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV63WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV65WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SI2,100,0,true,false )
             ,new CursorDef("P00SI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SI3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcatalogosutentacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcatalogosutentacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcatalogosutentacaofilterdata") )
          {
             return  ;
          }
          getwwcatalogosutentacaofilterdata worker = new getwwcatalogosutentacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
