/*
               File: GAMExampleEntrySecurityPolicy
        Description: Security Policy
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:11:46.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleentrysecuritypolicy : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleentrysecuritypolicy( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleentrysecuritypolicy( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref long aP1_Id )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV15Id = aP1_Id;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Id=this.AV15Id;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavAllowmultipleconcurrentwebsessions = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV15Id = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Id), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA272( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START272( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282311467");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleentrysecuritypolicy.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV15Id)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE272( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT272( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleentrysecuritypolicy.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV15Id) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleEntrySecurityPolicy" ;
      }

      public override String GetPgmdesc( )
      {
         return "Security Policy " ;
      }

      protected void WB270( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_272( true) ;
         }
         else
         {
            wb_table1_3_272( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_272e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START272( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Security Policy ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP270( ) ;
      }

      protected void WS272( )
      {
         START272( ) ;
         EVT272( ) ;
      }

      protected void EVT272( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11272 */
                              E11272 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12272 */
                                    E12272 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13272 */
                              E13272 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE272( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA272( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavAllowmultipleconcurrentwebsessions.Name = "vALLOWMULTIPLECONCURRENTWEBSESSIONS";
            cmbavAllowmultipleconcurrentwebsessions.WebTags = "";
            cmbavAllowmultipleconcurrentwebsessions.addItem("1", "Yes, from different IP address", 0);
            cmbavAllowmultipleconcurrentwebsessions.addItem("2", "Yes, from same IP address", 0);
            cmbavAllowmultipleconcurrentwebsessions.addItem("3", "No", 0);
            if ( cmbavAllowmultipleconcurrentwebsessions.ItemCount > 0 )
            {
               AV5AllowMultipleConcurrentWebSessions = (short)(NumberUtil.Val( cmbavAllowmultipleconcurrentwebsessions.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowMultipleConcurrentWebSessions", StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0));
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGuid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAllowmultipleconcurrentwebsessions.ItemCount > 0 )
         {
            AV5AllowMultipleConcurrentWebSessions = (short)(NumberUtil.Val( cmbavAllowmultipleconcurrentwebsessions.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowMultipleConcurrentWebSessions", StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF272( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
      }

      protected void RF272( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13272 */
            E13272 ();
            WB270( ) ;
         }
      }

      protected void STRUP270( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11272 */
         E11272 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV15Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")));
            AV14GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GUID", AV14GUID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14GUID, ""))));
            AV20Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Name", AV20Name);
            cmbavAllowmultipleconcurrentwebsessions.CurrentValue = cgiGet( cmbavAllowmultipleconcurrentwebsessions_Internalname);
            AV5AllowMultipleConcurrentWebSessions = (short)(NumberUtil.Val( cgiGet( cmbavAllowmultipleconcurrentwebsessions_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowMultipleConcurrentWebSessions", StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavWebsessiontimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavWebsessiontimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vWEBSESSIONTIMEOUT");
               GX_FocusControl = edtavWebsessiontimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26WebSessionTimeOut = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26WebSessionTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26WebSessionTimeOut), 4, 0)));
            }
            else
            {
               AV26WebSessionTimeOut = (short)(context.localUtil.CToN( cgiGet( edtavWebsessiontimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26WebSessionTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26WebSessionTimeOut), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOauthtokenexpire_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOauthtokenexpire_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOAUTHTOKENEXPIRE");
               GX_FocusControl = edtavOauthtokenexpire_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22OauthTokenExpire = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22OauthTokenExpire", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22OauthTokenExpire), 6, 0)));
            }
            else
            {
               AV22OauthTokenExpire = (int)(context.localUtil.CToN( cgiGet( edtavOauthtokenexpire_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22OauthTokenExpire", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22OauthTokenExpire), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOauthtokenmaximumrenovations_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOauthtokenmaximumrenovations_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOAUTHTOKENMAXIMUMRENOVATIONS");
               GX_FocusControl = edtavOauthtokenmaximumrenovations_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23OauthTokenMaximumRenovations = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23OauthTokenMaximumRenovations", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23OauthTokenMaximumRenovations), 3, 0)));
            }
            else
            {
               AV23OauthTokenMaximumRenovations = (short)(context.localUtil.CToN( cgiGet( edtavOauthtokenmaximumrenovations_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23OauthTokenMaximumRenovations", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23OauthTokenMaximumRenovations), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPeriodchangepassword_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPeriodchangepassword_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERIODCHANGEPASSWORD");
               GX_FocusControl = edtavPeriodchangepassword_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24PeriodChangePassword = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24PeriodChangePassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24PeriodChangePassword), 4, 0)));
            }
            else
            {
               AV24PeriodChangePassword = (short)(context.localUtil.CToN( cgiGet( edtavPeriodchangepassword_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24PeriodChangePassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24PeriodChangePassword), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumtimetochangepasswords_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumtimetochangepasswords_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMTIMETOCHANGEPASSWORDS");
               GX_FocusControl = edtavMinimumtimetochangepasswords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19MinimumTimeToChangePasswords = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19MinimumTimeToChangePasswords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19MinimumTimeToChangePasswords), 4, 0)));
            }
            else
            {
               AV19MinimumTimeToChangePasswords = (short)(context.localUtil.CToN( cgiGet( edtavMinimumtimetochangepasswords_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19MinimumTimeToChangePasswords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19MinimumTimeToChangePasswords), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumlengthpassword_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumlengthpassword_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMLENGTHPASSWORD");
               GX_FocusControl = edtavMinimumlengthpassword_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18MinimumLengthPassword = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MinimumLengthPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18MinimumLengthPassword), 2, 0)));
            }
            else
            {
               AV18MinimumLengthPassword = (short)(context.localUtil.CToN( cgiGet( edtavMinimumlengthpassword_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MinimumLengthPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18MinimumLengthPassword), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumnumericalcharacterpassword_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumnumericalcharacterpassword_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMNUMERICALCHARACTERPASSWORD");
               GX_FocusControl = edtavMinimumnumericalcharacterpassword_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7MinimumNumericalCharacterPassword = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7MinimumNumericalCharacterPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7MinimumNumericalCharacterPassword), 2, 0)));
            }
            else
            {
               AV7MinimumNumericalCharacterPassword = (short)(context.localUtil.CToN( cgiGet( edtavMinimumnumericalcharacterpassword_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7MinimumNumericalCharacterPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7MinimumNumericalCharacterPassword), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumuppercasecharacterspassword_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumuppercasecharacterspassword_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMUPPERCASECHARACTERSPASSWORD");
               GX_FocusControl = edtavMinimumuppercasecharacterspassword_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9MinimumUpperCaseCharactersPassword = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9MinimumUpperCaseCharactersPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9MinimumUpperCaseCharactersPassword), 2, 0)));
            }
            else
            {
               AV9MinimumUpperCaseCharactersPassword = (short)(context.localUtil.CToN( cgiGet( edtavMinimumuppercasecharacterspassword_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9MinimumUpperCaseCharactersPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9MinimumUpperCaseCharactersPassword), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumspecialcharacterspassword_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumspecialcharacterspassword_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMSPECIALCHARACTERSPASSWORD");
               GX_FocusControl = edtavMinimumspecialcharacterspassword_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8MinimumSpecialCharactersPassword = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8MinimumSpecialCharactersPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8MinimumSpecialCharactersPassword), 2, 0)));
            }
            else
            {
               AV8MinimumSpecialCharactersPassword = (short)(context.localUtil.CToN( cgiGet( edtavMinimumspecialcharacterspassword_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8MinimumSpecialCharactersPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8MinimumSpecialCharactersPassword), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMaximumpasswordhistoryentries_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMaximumpasswordhistoryentries_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMAXIMUMPASSWORDHISTORYENTRIES");
               GX_FocusControl = edtavMaximumpasswordhistoryentries_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6MaximumPasswordHistoryEntries = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6MaximumPasswordHistoryEntries", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6MaximumPasswordHistoryEntries), 4, 0)));
            }
            else
            {
               AV6MaximumPasswordHistoryEntries = (short)(context.localUtil.CToN( cgiGet( edtavMaximumpasswordhistoryentries_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6MaximumPasswordHistoryEntries", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6MaximumPasswordHistoryEntries), 4, 0)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11272 */
         E11272 ();
         if (returnInSub) return;
      }

      protected void E11272( )
      {
         /* Start Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV25SecurityPolicy.load( (int)(AV15Id));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")));
            AV15Id = AV25SecurityPolicy.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")));
            AV14GUID = AV25SecurityPolicy.gxTpr_Guid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GUID", AV14GUID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14GUID, ""))));
            AV20Name = AV25SecurityPolicy.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Name", AV20Name);
            AV5AllowMultipleConcurrentWebSessions = AV25SecurityPolicy.gxTpr_Allowmultipleconcurrentwebsessions;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowMultipleConcurrentWebSessions", StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0));
            AV26WebSessionTimeOut = AV25SecurityPolicy.gxTpr_Websessiontimeout;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26WebSessionTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26WebSessionTimeOut), 4, 0)));
            AV22OauthTokenExpire = AV25SecurityPolicy.gxTpr_Oauthtokenexpire;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22OauthTokenExpire", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22OauthTokenExpire), 6, 0)));
            AV23OauthTokenMaximumRenovations = AV25SecurityPolicy.gxTpr_Oauthtokenmaximumrenovations;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23OauthTokenMaximumRenovations", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23OauthTokenMaximumRenovations), 3, 0)));
            AV24PeriodChangePassword = AV25SecurityPolicy.gxTpr_Periodchangepassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24PeriodChangePassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24PeriodChangePassword), 4, 0)));
            AV19MinimumTimeToChangePasswords = AV25SecurityPolicy.gxTpr_Minimumtimetochangepasswords;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19MinimumTimeToChangePasswords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19MinimumTimeToChangePasswords), 4, 0)));
            AV18MinimumLengthPassword = AV25SecurityPolicy.gxTpr_Minimumlengthpassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MinimumLengthPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18MinimumLengthPassword), 2, 0)));
            AV7MinimumNumericalCharacterPassword = AV25SecurityPolicy.gxTpr_Minimumnumericcharacterspassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7MinimumNumericalCharacterPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7MinimumNumericalCharacterPassword), 2, 0)));
            AV9MinimumUpperCaseCharactersPassword = AV25SecurityPolicy.gxTpr_Minimumuppercasecharacterspassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9MinimumUpperCaseCharactersPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9MinimumUpperCaseCharactersPassword), 2, 0)));
            AV8MinimumSpecialCharactersPassword = AV25SecurityPolicy.gxTpr_Minimumspecialcharacterspassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8MinimumSpecialCharactersPassword", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8MinimumSpecialCharactersPassword), 2, 0)));
            AV6MaximumPasswordHistoryEntries = AV25SecurityPolicy.gxTpr_Maximumpasswordhistoryentries;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6MaximumPasswordHistoryEntries", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6MaximumPasswordHistoryEntries), 4, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            cmbavAllowmultipleconcurrentwebsessions.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAllowmultipleconcurrentwebsessions_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAllowmultipleconcurrentwebsessions.Enabled), 5, 0)));
            edtavWebsessiontimeout_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWebsessiontimeout_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWebsessiontimeout_Enabled), 5, 0)));
            edtavOauthtokenexpire_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOauthtokenexpire_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOauthtokenexpire_Enabled), 5, 0)));
            edtavOauthtokenmaximumrenovations_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOauthtokenmaximumrenovations_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOauthtokenmaximumrenovations_Enabled), 5, 0)));
            edtavPeriodchangepassword_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPeriodchangepassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPeriodchangepassword_Enabled), 5, 0)));
            edtavMinimumtimetochangepasswords_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMinimumtimetochangepasswords_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMinimumtimetochangepasswords_Enabled), 5, 0)));
            edtavMinimumlengthpassword_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMinimumlengthpassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMinimumlengthpassword_Enabled), 5, 0)));
            edtavMinimumnumericalcharacterpassword_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMinimumnumericalcharacterpassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMinimumnumericalcharacterpassword_Enabled), 5, 0)));
            edtavMinimumuppercasecharacterspassword_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMinimumuppercasecharacterspassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMinimumuppercasecharacterspassword_Enabled), 5, 0)));
            edtavMinimumspecialcharacterspassword_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMinimumspecialcharacterspassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMinimumspecialcharacterspassword_Enabled), 5, 0)));
            edtavMaximumpasswordhistoryentries_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMaximumpasswordhistoryentries_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMaximumpasswordhistoryentries_Enabled), 5, 0)));
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12272 */
         E12272 ();
         if (returnInSub) return;
      }

      protected void E12272( )
      {
         /* Enter Routine */
         AV25SecurityPolicy.gxTpr_Id = (int)(AV15Id);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            AV25SecurityPolicy.gxTpr_Name = AV20Name;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Allowmultipleconcurrentwebsessions = AV5AllowMultipleConcurrentWebSessions;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Websessiontimeout = AV26WebSessionTimeOut;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Oauthtokenexpire = AV22OauthTokenExpire;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Oauthtokenmaximumrenovations = AV23OauthTokenMaximumRenovations;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Periodchangepassword = AV24PeriodChangePassword;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Minimumtimetochangepasswords = AV19MinimumTimeToChangePasswords;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Minimumlengthpassword = AV18MinimumLengthPassword;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Minimumnumericcharacterspassword = AV7MinimumNumericalCharacterPassword;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Minimumuppercasecharacterspassword = AV9MinimumUpperCaseCharactersPassword;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Minimumspecialcharacterspassword = AV8MinimumSpecialCharactersPassword;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.gxTpr_Maximumpasswordhistoryentries = AV6MaximumPasswordHistoryEntries;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25SecurityPolicy", AV25SecurityPolicy);
            AV25SecurityPolicy.save();
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV25SecurityPolicy.delete();
         }
         if ( AV25SecurityPolicy.success() )
         {
            context.CommitDataStores( "GAMExampleEntrySecurityPolicy");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV15Id});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            AV12Errors = AV25SecurityPolicy.geterrors();
            AV30GXV1 = 1;
            while ( AV30GXV1 <= AV12Errors.Count )
            {
               AV11Error = ((SdtGAMError)AV12Errors.Item(AV30GXV1));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV11Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV30GXV1 = (int)(AV30GXV1+1);
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E13272( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_272( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbl_Internalname, tblTbl_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:17px;width:150px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbid_Internalname, "Id", "", "", lblTbid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:300px")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Id), 12, 0, ",", "")), ((edtavId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavId_Jsonclick, 0, "Attribute", "", "", "", 1, edtavId_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbguid_Internalname, "GUID", "", "", lblTbguid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV14GUID), StringUtil.RTrim( context.localUtil.Format( AV14GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavGuid_Enabled, 0, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Name", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV20Name), StringUtil.RTrim( context.localUtil.Format( AV20Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 1, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitleonlyweb_Internalname, "Only Web", "", "", lblTbtitleonlyweb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#000000;", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTballowmultipleconcurrentwebsessions_Internalname, "Allow multiple concurrent sessions", "", "", lblTballowmultipleconcurrentwebsessions_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAllowmultipleconcurrentwebsessions, cmbavAllowmultipleconcurrentwebsessions_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0)), 1, cmbavAllowmultipleconcurrentwebsessions_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavAllowmultipleconcurrentwebsessions.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_GAMExampleEntrySecurityPolicy.htm");
            cmbavAllowmultipleconcurrentwebsessions.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5AllowMultipleConcurrentWebSessions), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAllowmultipleconcurrentwebsessions_Internalname, "Values", (String)(cmbavAllowmultipleconcurrentwebsessions.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwebsessiontimeout_Internalname, "Session timeout (minutes)", "", "", lblTbwebsessiontimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWebsessiontimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26WebSessionTimeOut), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26WebSessionTimeOut), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWebsessiontimeout_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWebsessiontimeout_Enabled, 1, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitleonlysd_Internalname, "Only Smart Devices", "", "", lblTbtitleonlysd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#000000;", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTboauthtokenexpire_Internalname, "Token expire (minutes)", "", "", lblTboauthtokenexpire_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOauthtokenexpire_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22OauthTokenExpire), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22OauthTokenExpire), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOauthtokenexpire_Jsonclick, 0, "Attribute", "", "", "", 1, edtavOauthtokenexpire_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTboauthtoikenrenews_Internalname, "Token maximum renovations", "", "", lblTboauthtoikenrenews_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOauthtokenmaximumrenovations_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23OauthTokenMaximumRenovations), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23OauthTokenMaximumRenovations), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOauthtokenmaximumrenovations_Jsonclick, 0, "Attribute", "", "", "", 1, edtavOauthtokenmaximumrenovations_Enabled, 1, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitlegeneral_Internalname, "General", "", "", lblTbtitlegeneral_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#000000;", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbperiodchangepwd_Internalname, "Period change password (days)", "", "", lblTbperiodchangepwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPeriodchangepassword_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24PeriodChangePassword), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24PeriodChangePassword), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPeriodchangepassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPeriodchangepassword_Enabled, 1, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbminwaitbetpasschanges_Internalname, "Minimum waiting time between password changes", "", "", lblTbminwaitbetpasschanges_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumtimetochangepasswords_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19MinimumTimeToChangePasswords), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19MinimumTimeToChangePasswords), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumtimetochangepasswords_Jsonclick, 0, "Attribute", "", "", "", 1, edtavMinimumtimetochangepasswords_Enabled, 1, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:23px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbminimumlengthpassword_Internalname, "Minimum password length", "", "", lblTbminimumlengthpassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumlengthpassword_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18MinimumLengthPassword), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18MinimumLengthPassword), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumlengthpassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavMinimumlengthpassword_Enabled, 1, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:23px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbminimumnumericcharacterspassword_Internalname, "Minumum number of numeric characters in passwords", "", "", lblTbminimumnumericcharacterspassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumnumericalcharacterpassword_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7MinimumNumericalCharacterPassword), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7MinimumNumericalCharacterPassword), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumnumericalcharacterpassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavMinimumnumericalcharacterpassword_Enabled, 1, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbminimumuppercasecharacterspassword_Internalname, "Minimum number of uppercase characters in paswords", "", "", lblTbminimumuppercasecharacterspassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumuppercasecharacterspassword_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9MinimumUpperCaseCharactersPassword), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9MinimumUpperCaseCharactersPassword), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumuppercasecharacterspassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavMinimumuppercasecharacterspassword_Enabled, 1, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbminimumspecialcharacterspassword_Internalname, "Minimum number of special characters in paswords", "", "", lblTbminimumspecialcharacterspassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumspecialcharacterspassword_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8MinimumSpecialCharactersPassword), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8MinimumSpecialCharactersPassword), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumspecialcharacterspassword_Jsonclick, 0, "Attribute", "", "", "", 1, edtavMinimumspecialcharacterspassword_Enabled, 1, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbmaximumpasswordhistoryentries_Internalname, "Maximum password history entries", "", "", lblTbmaximumpasswordhistoryentries_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMaximumpasswordhistoryentries_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6MaximumPasswordHistoryEntries), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6MaximumPasswordHistoryEntries), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMaximumpasswordhistoryentries_Jsonclick, 0, "Attribute", "", "", "", 1, edtavMaximumpasswordhistoryentries_Enabled, 1, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_101_272( true) ;
         }
         else
         {
            wb_table2_101_272( false) ;
         }
         return  ;
      }

      protected void wb_table2_101_272e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_272e( true) ;
         }
         else
         {
            wb_table1_3_272e( false) ;
         }
      }

      protected void wb_table2_101_272( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntrySecurityPolicy.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_101_272e( true) ;
         }
         else
         {
            wb_table2_101_272e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV15Id = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Id), "ZZZZZZZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA272( ) ;
         WS272( ) ;
         WE272( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823114661");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleentrysecuritypolicy.js", "?202042823114661");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbid_Internalname = "TBID";
         edtavId_Internalname = "vID";
         lblTbguid_Internalname = "TBGUID";
         edtavGuid_Internalname = "vGUID";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbtitleonlyweb_Internalname = "TBTITLEONLYWEB";
         lblTballowmultipleconcurrentwebsessions_Internalname = "TBALLOWMULTIPLECONCURRENTWEBSESSIONS";
         cmbavAllowmultipleconcurrentwebsessions_Internalname = "vALLOWMULTIPLECONCURRENTWEBSESSIONS";
         lblTbwebsessiontimeout_Internalname = "TBWEBSESSIONTIMEOUT";
         edtavWebsessiontimeout_Internalname = "vWEBSESSIONTIMEOUT";
         lblTbtitleonlysd_Internalname = "TBTITLEONLYSD";
         lblTboauthtokenexpire_Internalname = "TBOAUTHTOKENEXPIRE";
         edtavOauthtokenexpire_Internalname = "vOAUTHTOKENEXPIRE";
         lblTboauthtoikenrenews_Internalname = "TBOAUTHTOIKENRENEWS";
         edtavOauthtokenmaximumrenovations_Internalname = "vOAUTHTOKENMAXIMUMRENOVATIONS";
         lblTbtitlegeneral_Internalname = "TBTITLEGENERAL";
         lblTbperiodchangepwd_Internalname = "TBPERIODCHANGEPWD";
         edtavPeriodchangepassword_Internalname = "vPERIODCHANGEPASSWORD";
         lblTbminwaitbetpasschanges_Internalname = "TBMINWAITBETPASSCHANGES";
         edtavMinimumtimetochangepasswords_Internalname = "vMINIMUMTIMETOCHANGEPASSWORDS";
         lblTbminimumlengthpassword_Internalname = "TBMINIMUMLENGTHPASSWORD";
         edtavMinimumlengthpassword_Internalname = "vMINIMUMLENGTHPASSWORD";
         lblTbminimumnumericcharacterspassword_Internalname = "TBMINIMUMNUMERICCHARACTERSPASSWORD";
         edtavMinimumnumericalcharacterpassword_Internalname = "vMINIMUMNUMERICALCHARACTERPASSWORD";
         lblTbminimumuppercasecharacterspassword_Internalname = "TBMINIMUMUPPERCASECHARACTERSPASSWORD";
         edtavMinimumuppercasecharacterspassword_Internalname = "vMINIMUMUPPERCASECHARACTERSPASSWORD";
         lblTbminimumspecialcharacterspassword_Internalname = "TBMINIMUMSPECIALCHARACTERSPASSWORD";
         edtavMinimumspecialcharacterspassword_Internalname = "vMINIMUMSPECIALCHARACTERSPASSWORD";
         lblTbmaximumpasswordhistoryentries_Internalname = "TBMAXIMUMPASSWORDHISTORYENTRIES";
         edtavMaximumpasswordhistoryentries_Internalname = "vMAXIMUMPASSWORDHISTORYENTRIES";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         tblTbl_Internalname = "TBL";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         bttBtnconfirm_Visible = 1;
         edtavMaximumpasswordhistoryentries_Jsonclick = "";
         edtavMinimumspecialcharacterspassword_Jsonclick = "";
         edtavMinimumuppercasecharacterspassword_Jsonclick = "";
         edtavMinimumnumericalcharacterpassword_Jsonclick = "";
         edtavMinimumlengthpassword_Jsonclick = "";
         edtavMinimumtimetochangepasswords_Jsonclick = "";
         edtavPeriodchangepassword_Jsonclick = "";
         edtavOauthtokenmaximumrenovations_Jsonclick = "";
         edtavOauthtokenexpire_Jsonclick = "";
         edtavWebsessiontimeout_Jsonclick = "";
         cmbavAllowmultipleconcurrentwebsessions_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavGuid_Jsonclick = "";
         edtavGuid_Enabled = 1;
         edtavId_Jsonclick = "";
         edtavId_Enabled = 0;
         bttBtnconfirm_Caption = "Confirmar";
         edtavMaximumpasswordhistoryentries_Enabled = 1;
         edtavMinimumspecialcharacterspassword_Enabled = 1;
         edtavMinimumuppercasecharacterspassword_Enabled = 1;
         edtavMinimumnumericalcharacterpassword_Enabled = 1;
         edtavMinimumlengthpassword_Enabled = 1;
         edtavMinimumtimetochangepasswords_Enabled = 1;
         edtavPeriodchangepassword_Enabled = 1;
         edtavOauthtokenmaximumrenovations_Enabled = 1;
         edtavOauthtokenexpire_Enabled = 1;
         edtavWebsessiontimeout_Enabled = 1;
         cmbavAllowmultipleconcurrentwebsessions.Enabled = 1;
         edtavName_Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Security Policy ";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14GUID = "";
         AV20Name = "";
         AV25SecurityPolicy = new SdtGAMSecurityPolicy(context);
         AV12Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV11Error = new SdtGAMError(context);
         sStyleString = "";
         lblTbid_Jsonclick = "";
         lblTbguid_Jsonclick = "";
         TempTags = "";
         lblTbname_Jsonclick = "";
         lblTbtitleonlyweb_Jsonclick = "";
         lblTballowmultipleconcurrentwebsessions_Jsonclick = "";
         lblTbwebsessiontimeout_Jsonclick = "";
         lblTbtitleonlysd_Jsonclick = "";
         lblTboauthtokenexpire_Jsonclick = "";
         lblTboauthtoikenrenews_Jsonclick = "";
         lblTbtitlegeneral_Jsonclick = "";
         lblTbperiodchangepwd_Jsonclick = "";
         lblTbminwaitbetpasschanges_Jsonclick = "";
         lblTbminimumlengthpassword_Jsonclick = "";
         lblTbminimumnumericcharacterspassword_Jsonclick = "";
         lblTbminimumuppercasecharacterspassword_Jsonclick = "";
         lblTbminimumspecialcharacterspassword_Jsonclick = "";
         lblTbmaximumpasswordhistoryentries_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleentrysecuritypolicy__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV5AllowMultipleConcurrentWebSessions ;
      private short AV26WebSessionTimeOut ;
      private short AV23OauthTokenMaximumRenovations ;
      private short AV24PeriodChangePassword ;
      private short AV19MinimumTimeToChangePasswords ;
      private short AV18MinimumLengthPassword ;
      private short AV7MinimumNumericalCharacterPassword ;
      private short AV9MinimumUpperCaseCharactersPassword ;
      private short AV8MinimumSpecialCharactersPassword ;
      private short AV6MaximumPasswordHistoryEntries ;
      private short nGXWrapped ;
      private int edtavId_Enabled ;
      private int edtavGuid_Enabled ;
      private int AV22OauthTokenExpire ;
      private int bttBtnconfirm_Visible ;
      private int edtavName_Enabled ;
      private int edtavWebsessiontimeout_Enabled ;
      private int edtavOauthtokenexpire_Enabled ;
      private int edtavOauthtokenmaximumrenovations_Enabled ;
      private int edtavPeriodchangepassword_Enabled ;
      private int edtavMinimumtimetochangepasswords_Enabled ;
      private int edtavMinimumlengthpassword_Enabled ;
      private int edtavMinimumnumericalcharacterpassword_Enabled ;
      private int edtavMinimumuppercasecharacterspassword_Enabled ;
      private int edtavMinimumspecialcharacterspassword_Enabled ;
      private int edtavMaximumpasswordhistoryentries_Enabled ;
      private int AV30GXV1 ;
      private int idxLst ;
      private long AV15Id ;
      private long wcpOAV15Id ;
      private String Gx_mode ;
      private String wcpOGx_mode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavGuid_Internalname ;
      private String edtavId_Internalname ;
      private String AV14GUID ;
      private String AV20Name ;
      private String edtavName_Internalname ;
      private String cmbavAllowmultipleconcurrentwebsessions_Internalname ;
      private String edtavWebsessiontimeout_Internalname ;
      private String edtavOauthtokenexpire_Internalname ;
      private String edtavOauthtokenmaximumrenovations_Internalname ;
      private String edtavPeriodchangepassword_Internalname ;
      private String edtavMinimumtimetochangepasswords_Internalname ;
      private String edtavMinimumlengthpassword_Internalname ;
      private String edtavMinimumnumericalcharacterpassword_Internalname ;
      private String edtavMinimumuppercasecharacterspassword_Internalname ;
      private String edtavMinimumspecialcharacterspassword_Internalname ;
      private String edtavMaximumpasswordhistoryentries_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String sStyleString ;
      private String tblTbl_Internalname ;
      private String lblTbid_Internalname ;
      private String lblTbid_Jsonclick ;
      private String edtavId_Jsonclick ;
      private String lblTbguid_Internalname ;
      private String lblTbguid_Jsonclick ;
      private String TempTags ;
      private String edtavGuid_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbtitleonlyweb_Internalname ;
      private String lblTbtitleonlyweb_Jsonclick ;
      private String lblTballowmultipleconcurrentwebsessions_Internalname ;
      private String lblTballowmultipleconcurrentwebsessions_Jsonclick ;
      private String cmbavAllowmultipleconcurrentwebsessions_Jsonclick ;
      private String lblTbwebsessiontimeout_Internalname ;
      private String lblTbwebsessiontimeout_Jsonclick ;
      private String edtavWebsessiontimeout_Jsonclick ;
      private String lblTbtitleonlysd_Internalname ;
      private String lblTbtitleonlysd_Jsonclick ;
      private String lblTboauthtokenexpire_Internalname ;
      private String lblTboauthtokenexpire_Jsonclick ;
      private String edtavOauthtokenexpire_Jsonclick ;
      private String lblTboauthtoikenrenews_Internalname ;
      private String lblTboauthtoikenrenews_Jsonclick ;
      private String edtavOauthtokenmaximumrenovations_Jsonclick ;
      private String lblTbtitlegeneral_Internalname ;
      private String lblTbtitlegeneral_Jsonclick ;
      private String lblTbperiodchangepwd_Internalname ;
      private String lblTbperiodchangepwd_Jsonclick ;
      private String edtavPeriodchangepassword_Jsonclick ;
      private String lblTbminwaitbetpasschanges_Internalname ;
      private String lblTbminwaitbetpasschanges_Jsonclick ;
      private String edtavMinimumtimetochangepasswords_Jsonclick ;
      private String lblTbminimumlengthpassword_Internalname ;
      private String lblTbminimumlengthpassword_Jsonclick ;
      private String edtavMinimumlengthpassword_Jsonclick ;
      private String lblTbminimumnumericcharacterspassword_Internalname ;
      private String lblTbminimumnumericcharacterspassword_Jsonclick ;
      private String edtavMinimumnumericalcharacterpassword_Jsonclick ;
      private String lblTbminimumuppercasecharacterspassword_Internalname ;
      private String lblTbminimumuppercasecharacterspassword_Jsonclick ;
      private String edtavMinimumuppercasecharacterspassword_Jsonclick ;
      private String lblTbminimumspecialcharacterspassword_Internalname ;
      private String lblTbminimumspecialcharacterspassword_Jsonclick ;
      private String edtavMinimumspecialcharacterspassword_Jsonclick ;
      private String lblTbmaximumpasswordhistoryentries_Internalname ;
      private String lblTbmaximumpasswordhistoryentries_Jsonclick ;
      private String edtavMaximumpasswordhistoryentries_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private long aP1_Id ;
      private GXCombobox cmbavAllowmultipleconcurrentwebsessions ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV12Errors ;
      private GXWebForm Form ;
      private SdtGAMError AV11Error ;
      private SdtGAMSecurityPolicy AV25SecurityPolicy ;
   }

   public class gamexampleentrysecuritypolicy__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
