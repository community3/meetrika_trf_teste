/*
               File: type_SdtAmbienteTecnologicoTecnologias
        Description: Ambiente Tecnologico Tecnologias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:16.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AmbienteTecnologicoTecnologias" )]
   [XmlType(TypeName =  "AmbienteTecnologicoTecnologias" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtAmbienteTecnologicoTecnologias : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAmbienteTecnologicoTecnologias( )
      {
         /* Constructor for serialization */
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Mode = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z = "";
      }

      public SdtAmbienteTecnologicoTecnologias( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV351AmbienteTecnologico_Codigo ,
                        int AV131Tecnologia_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV351AmbienteTecnologico_Codigo,(int)AV131Tecnologia_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"AmbienteTecnologico_Codigo", typeof(int)}, new Object[]{"Tecnologia_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "AmbienteTecnologicoTecnologias");
         metadata.Set("BT", "AmbienteTecnologicoTecnologias");
         metadata.Set("PK", "[ \"AmbienteTecnologico_Codigo\",\"Tecnologia_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AmbienteTecnologico_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Tecnologia_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologico_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologico_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologicotecnologias_ativo_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAmbienteTecnologicoTecnologias deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAmbienteTecnologicoTecnologias)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAmbienteTecnologicoTecnologias obj ;
         obj = this;
         obj.gxTpr_Ambientetecnologico_codigo = deserialized.gxTpr_Ambientetecnologico_codigo;
         obj.gxTpr_Ambientetecnologico_descricao = deserialized.gxTpr_Ambientetecnologico_descricao;
         obj.gxTpr_Ambientetecnologico_areatrabalhocod = deserialized.gxTpr_Ambientetecnologico_areatrabalhocod;
         obj.gxTpr_Tecnologia_codigo = deserialized.gxTpr_Tecnologia_codigo;
         obj.gxTpr_Tecnologia_nome = deserialized.gxTpr_Tecnologia_nome;
         obj.gxTpr_Ambientetecnologicotecnologias_ativo = deserialized.gxTpr_Ambientetecnologicotecnologias_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Ambientetecnologico_codigo_Z = deserialized.gxTpr_Ambientetecnologico_codigo_Z;
         obj.gxTpr_Ambientetecnologico_descricao_Z = deserialized.gxTpr_Ambientetecnologico_descricao_Z;
         obj.gxTpr_Ambientetecnologico_areatrabalhocod_Z = deserialized.gxTpr_Ambientetecnologico_areatrabalhocod_Z;
         obj.gxTpr_Tecnologia_codigo_Z = deserialized.gxTpr_Tecnologia_codigo_Z;
         obj.gxTpr_Tecnologia_nome_Z = deserialized.gxTpr_Tecnologia_nome_Z;
         obj.gxTpr_Ambientetecnologicotecnologias_ativo_Z = deserialized.gxTpr_Ambientetecnologicotecnologias_ativo_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Codigo") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Descricao") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_AreaTrabalhoCod") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Codigo") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Nome") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologicoTecnologias_Ativo") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Codigo_Z") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Descricao_Z") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Codigo_Z") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Nome_Z") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologicoTecnologias_Ativo_Z") )
               {
                  gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "AmbienteTecnologicoTecnologias";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AmbienteTecnologico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("AmbienteTecnologico_Descricao", StringUtil.RTrim( gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("AmbienteTecnologico_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Tecnologia_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Tecnologia_Nome", StringUtil.RTrim( gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("AmbienteTecnologicoTecnologias_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAmbienteTecnologicoTecnologias_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologico_Descricao_Z", StringUtil.RTrim( gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologico_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Tecnologia_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Tecnologia_Nome_Z", StringUtil.RTrim( gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologicoTecnologias_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AmbienteTecnologico_Codigo", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo, false);
         AddObjectProperty("AmbienteTecnologico_Descricao", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao, false);
         AddObjectProperty("AmbienteTecnologico_AreaTrabalhoCod", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod, false);
         AddObjectProperty("Tecnologia_Codigo", gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo, false);
         AddObjectProperty("Tecnologia_Nome", gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome, false);
         AddObjectProperty("AmbienteTecnologicoTecnologias_Ativo", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAmbienteTecnologicoTecnologias_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtAmbienteTecnologicoTecnologias_Initialized, false);
            AddObjectProperty("AmbienteTecnologico_Codigo_Z", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z, false);
            AddObjectProperty("AmbienteTecnologico_Descricao_Z", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z, false);
            AddObjectProperty("AmbienteTecnologico_AreaTrabalhoCod_Z", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z, false);
            AddObjectProperty("Tecnologia_Codigo_Z", gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z, false);
            AddObjectProperty("Tecnologia_Nome_Z", gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z, false);
            AddObjectProperty("AmbienteTecnologicoTecnologias_Ativo_Z", gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Codigo" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Codigo"   )]
      public int gxTpr_Ambientetecnologico_codigo
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo ;
         }

         set {
            if ( gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo != value )
            {
               gxTv_SdtAmbienteTecnologicoTecnologias_Mode = "INS";
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z_SetNull( );
            }
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Descricao" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Descricao"   )]
      public String gxTpr_Ambientetecnologico_descricao
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "AmbienteTecnologico_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_AreaTrabalhoCod"   )]
      public int gxTpr_Ambientetecnologico_areatrabalhocod
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tecnologia_Codigo" )]
      [  XmlElement( ElementName = "Tecnologia_Codigo"   )]
      public int gxTpr_Tecnologia_codigo
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo ;
         }

         set {
            if ( gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo != value )
            {
               gxTv_SdtAmbienteTecnologicoTecnologias_Mode = "INS";
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z_SetNull( );
               this.gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z_SetNull( );
            }
            gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tecnologia_Nome" )]
      [  XmlElement( ElementName = "Tecnologia_Nome"   )]
      public String gxTpr_Tecnologia_nome
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "AmbienteTecnologicoTecnologias_Ativo" )]
      [  XmlElement( ElementName = "AmbienteTecnologicoTecnologias_Ativo"   )]
      public bool gxTpr_Ambientetecnologicotecnologias_ativo
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Mode ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Mode_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Initialized ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Initialized_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Codigo_Z" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Codigo_Z"   )]
      public int gxTpr_Ambientetecnologico_codigo_Z
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Descricao_Z" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Descricao_Z"   )]
      public String gxTpr_Ambientetecnologico_descricao_Z
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Ambientetecnologico_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tecnologia_Codigo_Z" )]
      [  XmlElement( ElementName = "Tecnologia_Codigo_Z"   )]
      public int gxTpr_Tecnologia_codigo_Z
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tecnologia_Nome_Z" )]
      [  XmlElement( ElementName = "Tecnologia_Nome_Z"   )]
      public String gxTpr_Tecnologia_nome_Z
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologicoTecnologias_Ativo_Z" )]
      [  XmlElement( ElementName = "AmbienteTecnologicoTecnologias_Ativo_Z"   )]
      public bool gxTpr_Ambientetecnologicotecnologias_ativo_Z
      {
         get {
            return gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z ;
         }

         set {
            gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z = value;
         }

      }

      public void gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z_SetNull( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Mode = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z = "";
         gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "ambientetecnologicotecnologias", "GeneXus.Programs.ambientetecnologicotecnologias_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtAmbienteTecnologicoTecnologias_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo ;
      private int gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod ;
      private int gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo ;
      private int gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_codigo_Z ;
      private int gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_areatrabalhocod_Z ;
      private int gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_codigo_Z ;
      private String gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome ;
      private String gxTv_SdtAmbienteTecnologicoTecnologias_Mode ;
      private String gxTv_SdtAmbienteTecnologicoTecnologias_Tecnologia_nome_Z ;
      private String sTagName ;
      private bool gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo ;
      private bool gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologicotecnologias_ativo_Z ;
      private String gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao ;
      private String gxTv_SdtAmbienteTecnologicoTecnologias_Ambientetecnologico_descricao_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"AmbienteTecnologicoTecnologias", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtAmbienteTecnologicoTecnologias_RESTInterface : GxGenericCollectionItem<SdtAmbienteTecnologicoTecnologias>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAmbienteTecnologicoTecnologias_RESTInterface( ) : base()
      {
      }

      public SdtAmbienteTecnologicoTecnologias_RESTInterface( SdtAmbienteTecnologicoTecnologias psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AmbienteTecnologico_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Ambientetecnologico_codigo
      {
         get {
            return sdt.gxTpr_Ambientetecnologico_codigo ;
         }

         set {
            sdt.gxTpr_Ambientetecnologico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AmbienteTecnologico_Descricao" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Ambientetecnologico_descricao
      {
         get {
            return sdt.gxTpr_Ambientetecnologico_descricao ;
         }

         set {
            sdt.gxTpr_Ambientetecnologico_descricao = (String)(value);
         }

      }

      [DataMember( Name = "AmbienteTecnologico_AreaTrabalhoCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Ambientetecnologico_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Ambientetecnologico_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Ambientetecnologico_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tecnologia_Codigo" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tecnologia_codigo
      {
         get {
            return sdt.gxTpr_Tecnologia_codigo ;
         }

         set {
            sdt.gxTpr_Tecnologia_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tecnologia_Nome" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Tecnologia_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tecnologia_nome) ;
         }

         set {
            sdt.gxTpr_Tecnologia_nome = (String)(value);
         }

      }

      [DataMember( Name = "AmbienteTecnologicoTecnologias_Ativo" , Order = 5 )]
      [GxSeudo()]
      public bool gxTpr_Ambientetecnologicotecnologias_ativo
      {
         get {
            return sdt.gxTpr_Ambientetecnologicotecnologias_ativo ;
         }

         set {
            sdt.gxTpr_Ambientetecnologicotecnologias_ativo = value;
         }

      }

      public SdtAmbienteTecnologicoTecnologias sdt
      {
         get {
            return (SdtAmbienteTecnologicoTecnologias)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAmbienteTecnologicoTecnologias() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 14 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
