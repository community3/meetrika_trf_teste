/*
               File: PromptAreaTrabalho
        Description: Selecione �rea de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:45.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptareatrabalho : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptareatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptareatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutAreaTrabalho_Codigo ,
                           ref String aP1_InOutAreaTrabalho_Descricao )
      {
         this.AV7InOutAreaTrabalho_Codigo = aP0_InOutAreaTrabalho_Codigo;
         this.AV8InOutAreaTrabalho_Descricao = aP1_InOutAreaTrabalho_Descricao;
         executePrivate();
         aP0_InOutAreaTrabalho_Codigo=this.AV7InOutAreaTrabalho_Codigo;
         aP1_InOutAreaTrabalho_Descricao=this.AV8InOutAreaTrabalho_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbAreaTrabalho_CalculoPFinal = new GXCombobox();
         cmbAreaTrabalho_ValidaOSFM = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_47 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_47_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_47_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17AreaTrabalho_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
               AV87Organizacao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
               AV88Contratante_CNPJ1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
               AV51Contratante_RazaoSocial1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV87Organizacao_Nome1, AV88Contratante_CNPJ1, AV51Contratante_RazaoSocial1) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutAreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAreaTrabalho_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutAreaTrabalho_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAreaTrabalho_Descricao", AV8InOutAreaTrabalho_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA0U2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS0U2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE0U2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021294577");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptareatrabalho.aspx") + "?" + UrlEncode("" +AV7InOutAreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutAreaTrabalho_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_DESCRICAO1", AV17AreaTrabalho_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vORGANIZACAO_NOME1", StringUtil.RTrim( AV87Organizacao_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATANTE_CNPJ1", AV88Contratante_CNPJ1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATANTE_RAZAOSOCIAL1", StringUtil.RTrim( AV51Contratante_RazaoSocial1));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_47", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_47), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "vINOUTAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutAreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTAREATRABALHO_DESCRICAO", AV8InOutAreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm0U2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptAreaTrabalho" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione �rea de Trabalho" ;
      }

      protected void WB0U0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_0U2( true) ;
         }
         else
         {
            wb_table1_2_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void START0U2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione �rea de Trabalho", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0U0( ) ;
      }

      protected void WS0U2( )
      {
         START0U2( ) ;
         EVT0U2( ) ;
      }

      protected void EVT0U2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110U2 */
                           E110U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120U2 */
                           E120U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130U2 */
                           E130U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140U2 */
                           E140U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_47_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
                           SubsflControlProps_472( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV91Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                           A6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtAreaTrabalho_Descricao_Internalname));
                           A1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_OrganizacaoCod_Internalname), ",", "."));
                           n1216AreaTrabalho_OrganizacaoCod = false;
                           A1214Organizacao_Nome = StringUtil.Upper( cgiGet( edtOrganizacao_Nome_Internalname));
                           n1214Organizacao_Nome = false;
                           A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratante_PessoaCod_Internalname), ",", "."));
                           A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                           A24Estado_Nome = StringUtil.Upper( cgiGet( edtEstado_Nome_Internalname));
                           A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
                           n25Municipio_Codigo = false;
                           A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                           A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
                           A13Contratante_WebSite = cgiGet( edtContratante_WebSite_Internalname);
                           n13Contratante_WebSite = false;
                           A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
                           n12Contratante_CNPJ = false;
                           A11Contratante_IE = cgiGet( edtContratante_IE_Internalname);
                           A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
                           n9Contratante_RazaoSocial = false;
                           A272AreaTrabalho_ContagensQtdGeral = (short)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_ContagensQtdGeral_Internalname), ",", "."));
                           n272AreaTrabalho_ContagensQtdGeral = false;
                           cmbAreaTrabalho_CalculoPFinal.Name = cmbAreaTrabalho_CalculoPFinal_Internalname;
                           cmbAreaTrabalho_CalculoPFinal.CurrentValue = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
                           A642AreaTrabalho_CalculoPFinal = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
                           A830AreaTrabalho_ServicoPadrao = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_ServicoPadrao_Internalname), ",", "."));
                           n830AreaTrabalho_ServicoPadrao = false;
                           cmbAreaTrabalho_ValidaOSFM.Name = cmbAreaTrabalho_ValidaOSFM_Internalname;
                           cmbAreaTrabalho_ValidaOSFM.CurrentValue = cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname);
                           A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname));
                           n834AreaTrabalho_ValidaOSFM = false;
                           A855AreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_DiasParaPagar_Internalname), ",", "."));
                           n855AreaTrabalho_DiasParaPagar = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E150U2 */
                                 E150U2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E160U2 */
                                 E160U2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E170U2 */
                                 E170U2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Areatrabalho_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO1"), AV17AreaTrabalho_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Organizacao_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME1"), AV87Organizacao_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratante_cnpj1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTE_CNPJ1"), AV88Contratante_CNPJ1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratante_razaosocial1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTE_RAZAOSOCIAL1"), AV51Contratante_RazaoSocial1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E180U2 */
                                       E180U2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE0U2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0U2( ) ;
            }
         }
      }

      protected void PA0U2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AREATRABALHO_DESCRICAO", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector1.addItem("ORGANIZACAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATANTE_CNPJ", "CNPJ", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATANTE_RAZAOSOCIAL", "Raz�o Social", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            GXCCtl = "AREATRABALHO_CALCULOPFINAL_" + sGXsfl_47_idx;
            cmbAreaTrabalho_CalculoPFinal.Name = GXCCtl;
            cmbAreaTrabalho_CalculoPFinal.WebTags = "";
            cmbAreaTrabalho_CalculoPFinal.addItem("MB", "Menor PFB", 0);
            cmbAreaTrabalho_CalculoPFinal.addItem("BM", "PFB FM", 0);
            if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
            {
               A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
            }
            GXCCtl = "AREATRABALHO_VALIDAOSFM_" + sGXsfl_47_idx;
            cmbAreaTrabalho_ValidaOSFM.Name = GXCCtl;
            cmbAreaTrabalho_ValidaOSFM.WebTags = "";
            cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
            {
               A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
               n834AreaTrabalho_ValidaOSFM = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_472( ) ;
         while ( nGXsfl_47_idx <= nRC_GXsfl_47 )
         {
            sendrow_472( ) ;
            nGXsfl_47_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_47_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_47_idx+1));
            sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
            SubsflControlProps_472( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17AreaTrabalho_Descricao1 ,
                                       String AV87Organizacao_Nome1 ,
                                       String AV88Contratante_CNPJ1 ,
                                       String AV51Contratante_RazaoSocial1 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0U2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_ORGANIZACAOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_ORGANIZACAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CALCULOPFINAL", StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_SERVICOPADRAO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_SERVICOPADRAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( "", A834AreaTrabalho_ValidaOSFM));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_VALIDAOSFM", StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DIASPARAPAGAR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DIASPARAPAGAR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0U2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0U2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 47;
         /* Execute user event: E160U2 */
         E160U2 ();
         nGXsfl_47_idx = 1;
         sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
         SubsflControlProps_472( ) ;
         nGXsfl_47_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_472( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17AreaTrabalho_Descricao1 ,
                                                 AV87Organizacao_Nome1 ,
                                                 AV88Contratante_CNPJ1 ,
                                                 AV51Contratante_RazaoSocial1 ,
                                                 A6AreaTrabalho_Descricao ,
                                                 A1214Organizacao_Nome ,
                                                 A12Contratante_CNPJ ,
                                                 A9Contratante_RazaoSocial ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17AreaTrabalho_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17AreaTrabalho_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
            lV17AreaTrabalho_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17AreaTrabalho_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
            lV87Organizacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV87Organizacao_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
            lV87Organizacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV87Organizacao_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
            lV88Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV88Contratante_CNPJ1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
            lV88Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV88Contratante_CNPJ1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
            lV51Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV51Contratante_RazaoSocial1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
            lV51Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV51Contratante_RazaoSocial1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
            /* Using cursor H000U3 */
            pr_default.execute(0, new Object[] {lV17AreaTrabalho_Descricao1, lV17AreaTrabalho_Descricao1, lV87Organizacao_Nome1, lV87Organizacao_Nome1, lV88Contratante_CNPJ1, lV88Contratante_CNPJ1, lV51Contratante_RazaoSocial1, lV51Contratante_RazaoSocial1, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_47_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A29Contratante_Codigo = H000U3_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H000U3_n29Contratante_Codigo[0];
               A855AreaTrabalho_DiasParaPagar = H000U3_A855AreaTrabalho_DiasParaPagar[0];
               n855AreaTrabalho_DiasParaPagar = H000U3_n855AreaTrabalho_DiasParaPagar[0];
               A834AreaTrabalho_ValidaOSFM = H000U3_A834AreaTrabalho_ValidaOSFM[0];
               n834AreaTrabalho_ValidaOSFM = H000U3_n834AreaTrabalho_ValidaOSFM[0];
               A830AreaTrabalho_ServicoPadrao = H000U3_A830AreaTrabalho_ServicoPadrao[0];
               n830AreaTrabalho_ServicoPadrao = H000U3_n830AreaTrabalho_ServicoPadrao[0];
               A642AreaTrabalho_CalculoPFinal = H000U3_A642AreaTrabalho_CalculoPFinal[0];
               A9Contratante_RazaoSocial = H000U3_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000U3_n9Contratante_RazaoSocial[0];
               A11Contratante_IE = H000U3_A11Contratante_IE[0];
               A12Contratante_CNPJ = H000U3_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000U3_n12Contratante_CNPJ[0];
               A13Contratante_WebSite = H000U3_A13Contratante_WebSite[0];
               n13Contratante_WebSite = H000U3_n13Contratante_WebSite[0];
               A31Contratante_Telefone = H000U3_A31Contratante_Telefone[0];
               A26Municipio_Nome = H000U3_A26Municipio_Nome[0];
               A25Municipio_Codigo = H000U3_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000U3_n25Municipio_Codigo[0];
               A24Estado_Nome = H000U3_A24Estado_Nome[0];
               A23Estado_UF = H000U3_A23Estado_UF[0];
               A335Contratante_PessoaCod = H000U3_A335Contratante_PessoaCod[0];
               A1214Organizacao_Nome = H000U3_A1214Organizacao_Nome[0];
               n1214Organizacao_Nome = H000U3_n1214Organizacao_Nome[0];
               A1216AreaTrabalho_OrganizacaoCod = H000U3_A1216AreaTrabalho_OrganizacaoCod[0];
               n1216AreaTrabalho_OrganizacaoCod = H000U3_n1216AreaTrabalho_OrganizacaoCod[0];
               A6AreaTrabalho_Descricao = H000U3_A6AreaTrabalho_Descricao[0];
               A5AreaTrabalho_Codigo = H000U3_A5AreaTrabalho_Codigo[0];
               A272AreaTrabalho_ContagensQtdGeral = H000U3_A272AreaTrabalho_ContagensQtdGeral[0];
               n272AreaTrabalho_ContagensQtdGeral = H000U3_n272AreaTrabalho_ContagensQtdGeral[0];
               A11Contratante_IE = H000U3_A11Contratante_IE[0];
               A13Contratante_WebSite = H000U3_A13Contratante_WebSite[0];
               n13Contratante_WebSite = H000U3_n13Contratante_WebSite[0];
               A31Contratante_Telefone = H000U3_A31Contratante_Telefone[0];
               A25Municipio_Codigo = H000U3_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000U3_n25Municipio_Codigo[0];
               A335Contratante_PessoaCod = H000U3_A335Contratante_PessoaCod[0];
               A26Municipio_Nome = H000U3_A26Municipio_Nome[0];
               A23Estado_UF = H000U3_A23Estado_UF[0];
               A24Estado_Nome = H000U3_A24Estado_Nome[0];
               A9Contratante_RazaoSocial = H000U3_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000U3_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H000U3_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000U3_n12Contratante_CNPJ[0];
               A1214Organizacao_Nome = H000U3_A1214Organizacao_Nome[0];
               n1214Organizacao_Nome = H000U3_n1214Organizacao_Nome[0];
               A272AreaTrabalho_ContagensQtdGeral = H000U3_A272AreaTrabalho_ContagensQtdGeral[0];
               n272AreaTrabalho_ContagensQtdGeral = H000U3_n272AreaTrabalho_ContagensQtdGeral[0];
               /* Execute user event: E170U2 */
               E170U2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 47;
            WB0U0( ) ;
         }
         nGXsfl_47_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17AreaTrabalho_Descricao1 ,
                                              AV87Organizacao_Nome1 ,
                                              AV88Contratante_CNPJ1 ,
                                              AV51Contratante_RazaoSocial1 ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A12Contratante_CNPJ ,
                                              A9Contratante_RazaoSocial ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17AreaTrabalho_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17AreaTrabalho_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
         lV17AreaTrabalho_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17AreaTrabalho_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
         lV87Organizacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV87Organizacao_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
         lV87Organizacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV87Organizacao_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
         lV88Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV88Contratante_CNPJ1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
         lV88Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV88Contratante_CNPJ1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
         lV51Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV51Contratante_RazaoSocial1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
         lV51Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV51Contratante_RazaoSocial1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
         /* Using cursor H000U5 */
         pr_default.execute(1, new Object[] {lV17AreaTrabalho_Descricao1, lV17AreaTrabalho_Descricao1, lV87Organizacao_Nome1, lV87Organizacao_Nome1, lV88Contratante_CNPJ1, lV88Contratante_CNPJ1, lV51Contratante_RazaoSocial1, lV51Contratante_RazaoSocial1});
         GRID_nRecordCount = H000U5_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV87Organizacao_Nome1, AV88Contratante_CNPJ1, AV51Contratante_RazaoSocial1) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV87Organizacao_Nome1, AV88Contratante_CNPJ1, AV51Contratante_RazaoSocial1) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV87Organizacao_Nome1, AV88Contratante_CNPJ1, AV51Contratante_RazaoSocial1) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV87Organizacao_Nome1, AV88Contratante_CNPJ1, AV51Contratante_RazaoSocial1) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV87Organizacao_Nome1, AV88Contratante_CNPJ1, AV51Contratante_RazaoSocial1) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0U0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E150U2 */
         E150U2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17AreaTrabalho_Descricao1 = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
            AV87Organizacao_Nome1 = StringUtil.Upper( cgiGet( edtavOrganizacao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
            AV88Contratante_CNPJ1 = cgiGet( edtavContratante_cnpj1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
            AV51Contratante_RazaoSocial1 = StringUtil.Upper( cgiGet( edtavContratante_razaosocial1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
            /* Read saved values. */
            nRC_GXsfl_47 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_47"), ",", "."));
            AV85GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV86GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO1"), AV17AreaTrabalho_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME1"), AV87Organizacao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTE_CNPJ1"), AV88Contratante_CNPJ1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATANTE_RAZAOSOCIAL1"), AV51Contratante_RazaoSocial1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E150U2 */
         E150U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E150U2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         Form.Caption = "Select �rea de Trabalho";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Pessoa Jur�dica", 0);
         cmbavOrderedby.addItem("4", "Nome do Estado", 0);
         cmbavOrderedby.addItem("5", "C�digo", 0);
         cmbavOrderedby.addItem("6", "Munic�pio", 0);
         cmbavOrderedby.addItem("7", "Telefone", 0);
         cmbavOrderedby.addItem("8", "Site", 0);
         cmbavOrderedby.addItem("9", "CNPJ", 0);
         cmbavOrderedby.addItem("10", "IE", 0);
         cmbavOrderedby.addItem("11", "Contratante", 0);
         cmbavOrderedby.addItem("12", "Calculo P. Final", 0);
         cmbavOrderedby.addItem("13", "padr�o", 0);
         cmbavOrderedby.addItem("14", "OS FM", 0);
         cmbavOrderedby.addItem("15", "para pagar", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E160U2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         edtAreaTrabalho_Descricao_Titleformat = 2;
         edtAreaTrabalho_Descricao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "�rea de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Descricao_Internalname, "Title", edtAreaTrabalho_Descricao_Title);
         edtOrganizacao_Nome_Titleformat = 2;
         edtOrganizacao_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Nome_Internalname, "Title", edtOrganizacao_Nome_Title);
         edtContratante_PessoaCod_Titleformat = 2;
         edtContratante_PessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa Jur�dica", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_PessoaCod_Internalname, "Title", edtContratante_PessoaCod_Title);
         edtEstado_Nome_Titleformat = 2;
         edtEstado_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome do Estado", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_Nome_Internalname, "Title", edtEstado_Nome_Title);
         edtMunicipio_Codigo_Titleformat = 2;
         edtMunicipio_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Title", edtMunicipio_Codigo_Title);
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Munic�pio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         edtContratante_Telefone_Titleformat = 2;
         edtContratante_Telefone_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Telefone", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Telefone_Internalname, "Title", edtContratante_Telefone_Title);
         edtContratante_WebSite_Titleformat = 2;
         edtContratante_WebSite_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Site", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_WebSite_Internalname, "Title", edtContratante_WebSite_Title);
         edtContratante_CNPJ_Titleformat = 2;
         edtContratante_CNPJ_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV13OrderedBy==9) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CNPJ", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_CNPJ_Internalname, "Title", edtContratante_CNPJ_Title);
         edtContratante_IE_Titleformat = 2;
         edtContratante_IE_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV13OrderedBy==10) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "IE", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_IE_Internalname, "Title", edtContratante_IE_Title);
         edtContratante_RazaoSocial_Titleformat = 2;
         edtContratante_RazaoSocial_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV13OrderedBy==11) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contratante", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_RazaoSocial_Internalname, "Title", edtContratante_RazaoSocial_Title);
         cmbAreaTrabalho_CalculoPFinal_Titleformat = 2;
         cmbAreaTrabalho_CalculoPFinal.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV13OrderedBy==12) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Calculo P. Final", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Title", cmbAreaTrabalho_CalculoPFinal.Title.Text);
         edtAreaTrabalho_ServicoPadrao_Titleformat = 2;
         edtAreaTrabalho_ServicoPadrao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV13OrderedBy==13) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "padr�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_ServicoPadrao_Internalname, "Title", edtAreaTrabalho_ServicoPadrao_Title);
         cmbAreaTrabalho_ValidaOSFM_Titleformat = 2;
         cmbAreaTrabalho_ValidaOSFM.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV13OrderedBy==14) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "OS FM", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Title", cmbAreaTrabalho_ValidaOSFM.Title.Text);
         edtAreaTrabalho_DiasParaPagar_Titleformat = 2;
         edtAreaTrabalho_DiasParaPagar_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV13OrderedBy==15) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "para pagar", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_DiasParaPagar_Internalname, "Title", edtAreaTrabalho_DiasParaPagar_Title);
         AV85GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85GridCurrentPage), 10, 0)));
         AV86GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E110U2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV84PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV84PageToGo) ;
         }
      }

      private void E170U2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV91Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 47;
         }
         sendrow_472( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_47_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(47, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E180U2 */
         E180U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E180U2( )
      {
         /* Enter Routine */
         AV7InOutAreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAreaTrabalho_Codigo), 6, 0)));
         AV8InOutAreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAreaTrabalho_Descricao", AV8InOutAreaTrabalho_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutAreaTrabalho_Codigo,(String)AV8InOutAreaTrabalho_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E120U2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E140U2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E130U2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAreatrabalho_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao1_Visible), 5, 0)));
         edtavOrganizacao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome1_Visible), 5, 0)));
         edtavContratante_cnpj1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj1_Visible), 5, 0)));
         edtavContratante_razaosocial1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 )
         {
            edtavAreatrabalho_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
         {
            edtavOrganizacao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 )
         {
            edtavContratante_cnpj1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            edtavContratante_razaosocial1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17AreaTrabalho_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17AreaTrabalho_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV87Organizacao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Organizacao_Nome1", AV87Organizacao_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV88Contratante_CNPJ1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88Contratante_CNPJ1", AV88Contratante_CNPJ1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV51Contratante_RazaoSocial1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contratante_RazaoSocial1", AV51Contratante_RazaoSocial1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S132( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_0U2( true) ;
         }
         else
         {
            wb_table2_5_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_0U2( true) ;
         }
         else
         {
            wb_table3_41_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0U2e( true) ;
         }
         else
         {
            wb_table1_2_0U2e( false) ;
         }
      }

      protected void wb_table3_41_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_44_0U2( true) ;
         }
         else
         {
            wb_table4_44_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table4_44_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_0U2e( true) ;
         }
         else
         {
            wb_table3_41_0U2e( false) ;
         }
      }

      protected void wb_table4_44_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"47\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAreaTrabalho_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAreaTrabalho_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAreaTrabalho_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Organiza��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtOrganizacao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtOrganizacao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtOrganizacao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "UF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_WebSite_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_WebSite_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_WebSite_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_CNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_CNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_CNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_IE_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_IE_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_IE_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_RazaoSocial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_RazaoSocial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_RazaoSocial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Qtd. Contagens Geral") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAreaTrabalho_CalculoPFinal_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAreaTrabalho_CalculoPFinal.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAreaTrabalho_CalculoPFinal.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAreaTrabalho_ServicoPadrao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAreaTrabalho_ServicoPadrao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAreaTrabalho_ServicoPadrao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAreaTrabalho_ValidaOSFM_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAreaTrabalho_ValidaOSFM.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAreaTrabalho_ValidaOSFM.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(64), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAreaTrabalho_DiasParaPagar_Titleformat == 0 )
               {
                  context.SendWebValue( edtAreaTrabalho_DiasParaPagar_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAreaTrabalho_DiasParaPagar_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A6AreaTrabalho_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAreaTrabalho_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAreaTrabalho_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1214Organizacao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtOrganizacao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtOrganizacao_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A24Estado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A31Contratante_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Telefone_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A13Contratante_WebSite);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_WebSite_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_WebSite_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A12Contratante_CNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_CNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_CNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A11Contratante_IE));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_IE_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_IE_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A9Contratante_RazaoSocial));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_RazaoSocial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_RazaoSocial_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAreaTrabalho_CalculoPFinal.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAreaTrabalho_CalculoPFinal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAreaTrabalho_ServicoPadrao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAreaTrabalho_ServicoPadrao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAreaTrabalho_ValidaOSFM.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAreaTrabalho_ValidaOSFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAreaTrabalho_DiasParaPagar_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAreaTrabalho_DiasParaPagar_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 47 )
         {
            wbEnd = 0;
            nRC_GXsfl_47 = (short)(nGXsfl_47_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_44_0U2e( true) ;
         }
         else
         {
            wb_table4_44_0U2e( false) ;
         }
      }

      protected void wb_table2_5_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptAreaTrabalho.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_0U2( true) ;
         }
         else
         {
            wb_table5_14_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0U2e( true) ;
         }
         else
         {
            wb_table2_5_0U2e( false) ;
         }
      }

      protected void wb_table5_14_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_0U2( true) ;
         }
         else
         {
            wb_table6_19_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_0U2e( true) ;
         }
         else
         {
            wb_table5_14_0U2e( false) ;
         }
      }

      protected void wb_table6_19_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptAreaTrabalho.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_0U2( true) ;
         }
         else
         {
            wb_table7_28_0U2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_0U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_0U2e( true) ;
         }
         else
         {
            wb_table6_19_0U2e( false) ;
         }
      }

      protected void wb_table7_28_0U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_47_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptAreaTrabalho.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_descricao1_Internalname, AV17AreaTrabalho_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17AreaTrabalho_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAreatrabalho_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrganizacao_nome1_Internalname, StringUtil.RTrim( AV87Organizacao_Nome1), StringUtil.RTrim( context.localUtil.Format( AV87Organizacao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrganizacao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavOrganizacao_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_cnpj1_Internalname, AV88Contratante_CNPJ1, StringUtil.RTrim( context.localUtil.Format( AV88Contratante_CNPJ1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_cnpj1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_cnpj1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_47_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_razaosocial1_Internalname, StringUtil.RTrim( AV51Contratante_RazaoSocial1), StringUtil.RTrim( context.localUtil.Format( AV51Contratante_RazaoSocial1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_razaosocial1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_razaosocial1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_0U2e( true) ;
         }
         else
         {
            wb_table7_28_0U2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutAreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAreaTrabalho_Codigo), 6, 0)));
         AV8InOutAreaTrabalho_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAreaTrabalho_Descricao", AV8InOutAreaTrabalho_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0U2( ) ;
         WS0U2( ) ;
         WE0U2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021294842");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptareatrabalho.js", "?202053021294843");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_472( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_47_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_47_idx;
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO_"+sGXsfl_47_idx;
         edtAreaTrabalho_OrganizacaoCod_Internalname = "AREATRABALHO_ORGANIZACAOCOD_"+sGXsfl_47_idx;
         edtOrganizacao_Nome_Internalname = "ORGANIZACAO_NOME_"+sGXsfl_47_idx;
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD_"+sGXsfl_47_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_47_idx;
         edtEstado_Nome_Internalname = "ESTADO_NOME_"+sGXsfl_47_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_47_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_47_idx;
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE_"+sGXsfl_47_idx;
         edtContratante_WebSite_Internalname = "CONTRATANTE_WEBSITE_"+sGXsfl_47_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_47_idx;
         edtContratante_IE_Internalname = "CONTRATANTE_IE_"+sGXsfl_47_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_47_idx;
         edtAreaTrabalho_ContagensQtdGeral_Internalname = "AREATRABALHO_CONTAGENSQTDGERAL_"+sGXsfl_47_idx;
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL_"+sGXsfl_47_idx;
         edtAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO_"+sGXsfl_47_idx;
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM_"+sGXsfl_47_idx;
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR_"+sGXsfl_47_idx;
      }

      protected void SubsflControlProps_fel_472( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_47_fel_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_47_fel_idx;
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO_"+sGXsfl_47_fel_idx;
         edtAreaTrabalho_OrganizacaoCod_Internalname = "AREATRABALHO_ORGANIZACAOCOD_"+sGXsfl_47_fel_idx;
         edtOrganizacao_Nome_Internalname = "ORGANIZACAO_NOME_"+sGXsfl_47_fel_idx;
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD_"+sGXsfl_47_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_47_fel_idx;
         edtEstado_Nome_Internalname = "ESTADO_NOME_"+sGXsfl_47_fel_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_47_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_47_fel_idx;
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE_"+sGXsfl_47_fel_idx;
         edtContratante_WebSite_Internalname = "CONTRATANTE_WEBSITE_"+sGXsfl_47_fel_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_47_fel_idx;
         edtContratante_IE_Internalname = "CONTRATANTE_IE_"+sGXsfl_47_fel_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_47_fel_idx;
         edtAreaTrabalho_ContagensQtdGeral_Internalname = "AREATRABALHO_CONTAGENSQTDGERAL_"+sGXsfl_47_fel_idx;
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL_"+sGXsfl_47_fel_idx;
         edtAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO_"+sGXsfl_47_fel_idx;
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM_"+sGXsfl_47_fel_idx;
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR_"+sGXsfl_47_fel_idx;
      }

      protected void sendrow_472( )
      {
         SubsflControlProps_472( ) ;
         WB0U0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_47_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_47_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_47_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 48,'',false,'',47)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV91Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV91Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_47_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Descricao_Internalname,(String)A6AreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_OrganizacaoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_OrganizacaoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtOrganizacao_Nome_Internalname,StringUtil.RTrim( A1214Organizacao_Nome),StringUtil.RTrim( context.localUtil.Format( A1214Organizacao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtOrganizacao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_Nome_Internalname,StringUtil.RTrim( A24Estado_Nome),StringUtil.RTrim( context.localUtil.Format( A24Estado_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Telefone_Internalname,StringUtil.RTrim( A31Contratante_Telefone),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)gxphoneLink,(String)"",(String)"",(String)"",(String)edtContratante_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"tel",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Phone",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_WebSite_Internalname,(String)A13Contratante_WebSite,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_WebSite_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_CNPJ_Internalname,(String)A12Contratante_CNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_CNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_IE_Internalname,StringUtil.RTrim( A11Contratante_IE),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_IE_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"IE",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_RazaoSocial_Internalname,StringUtil.RTrim( A9Contratante_RazaoSocial),StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_RazaoSocial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_ContagensQtdGeral_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A272AreaTrabalho_ContagensQtdGeral), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_ContagensQtdGeral_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_47_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "AREATRABALHO_CALCULOPFINAL_" + sGXsfl_47_idx;
               cmbAreaTrabalho_CalculoPFinal.Name = GXCCtl;
               cmbAreaTrabalho_CalculoPFinal.WebTags = "";
               cmbAreaTrabalho_CalculoPFinal.addItem("MB", "Menor PFB", 0);
               cmbAreaTrabalho_CalculoPFinal.addItem("BM", "PFB FM", 0);
               if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
               {
                  A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAreaTrabalho_CalculoPFinal,(String)cmbAreaTrabalho_CalculoPFinal_Internalname,StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal),(short)1,(String)cmbAreaTrabalho_CalculoPFinal_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAreaTrabalho_CalculoPFinal.CurrentValue = StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Values", (String)(cmbAreaTrabalho_CalculoPFinal.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_ServicoPadrao_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_ServicoPadrao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_47_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "AREATRABALHO_VALIDAOSFM_" + sGXsfl_47_idx;
               cmbAreaTrabalho_ValidaOSFM.Name = GXCCtl;
               cmbAreaTrabalho_ValidaOSFM.WebTags = "";
               cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
               {
                  A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
                  n834AreaTrabalho_ValidaOSFM = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAreaTrabalho_ValidaOSFM,(String)cmbAreaTrabalho_ValidaOSFM_Internalname,StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM),(short)1,(String)cmbAreaTrabalho_ValidaOSFM_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAreaTrabalho_ValidaOSFM.CurrentValue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Values", (String)(cmbAreaTrabalho_ValidaOSFM.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_DiasParaPagar_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_DiasParaPagar_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)64,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)47,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DESCRICAO"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_ORGANIZACAOCOD"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CALCULOPFINAL"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_SERVICOPADRAO"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_VALIDAOSFM"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, A834AreaTrabalho_ValidaOSFM));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DIASPARAPAGAR"+"_"+sGXsfl_47_idx, GetSecureSignedToken( sGXsfl_47_idx, context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_47_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_47_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_47_idx+1));
            sGXsfl_47_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_47_idx), 4, 0)), 4, "0");
            SubsflControlProps_472( ) ;
         }
         /* End function sendrow_472 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavAreatrabalho_descricao1_Internalname = "vAREATRABALHO_DESCRICAO1";
         edtavOrganizacao_nome1_Internalname = "vORGANIZACAO_NOME1";
         edtavContratante_cnpj1_Internalname = "vCONTRATANTE_CNPJ1";
         edtavContratante_razaosocial1_Internalname = "vCONTRATANTE_RAZAOSOCIAL1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO";
         edtAreaTrabalho_OrganizacaoCod_Internalname = "AREATRABALHO_ORGANIZACAOCOD";
         edtOrganizacao_Nome_Internalname = "ORGANIZACAO_NOME";
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD";
         edtEstado_UF_Internalname = "ESTADO_UF";
         edtEstado_Nome_Internalname = "ESTADO_NOME";
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE";
         edtContratante_WebSite_Internalname = "CONTRATANTE_WEBSITE";
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ";
         edtContratante_IE_Internalname = "CONTRATANTE_IE";
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL";
         edtAreaTrabalho_ContagensQtdGeral_Internalname = "AREATRABALHO_CONTAGENSQTDGERAL";
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL";
         edtAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO";
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM";
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAreaTrabalho_DiasParaPagar_Jsonclick = "";
         cmbAreaTrabalho_ValidaOSFM_Jsonclick = "";
         edtAreaTrabalho_ServicoPadrao_Jsonclick = "";
         cmbAreaTrabalho_CalculoPFinal_Jsonclick = "";
         edtAreaTrabalho_ContagensQtdGeral_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         edtContratante_IE_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_WebSite_Jsonclick = "";
         edtContratante_Telefone_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtEstado_Nome_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtContratante_PessoaCod_Jsonclick = "";
         edtOrganizacao_Nome_Jsonclick = "";
         edtAreaTrabalho_OrganizacaoCod_Jsonclick = "";
         edtAreaTrabalho_Descricao_Jsonclick = "";
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratante_razaosocial1_Jsonclick = "";
         edtavContratante_cnpj1_Jsonclick = "";
         edtavOrganizacao_nome1_Jsonclick = "";
         edtavAreatrabalho_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtAreaTrabalho_DiasParaPagar_Titleformat = 0;
         cmbAreaTrabalho_ValidaOSFM_Titleformat = 0;
         edtAreaTrabalho_ServicoPadrao_Titleformat = 0;
         cmbAreaTrabalho_CalculoPFinal_Titleformat = 0;
         edtContratante_RazaoSocial_Titleformat = 0;
         edtContratante_IE_Titleformat = 0;
         edtContratante_CNPJ_Titleformat = 0;
         edtContratante_WebSite_Titleformat = 0;
         edtContratante_Telefone_Titleformat = 0;
         edtMunicipio_Nome_Titleformat = 0;
         edtMunicipio_Codigo_Titleformat = 0;
         edtEstado_Nome_Titleformat = 0;
         edtContratante_PessoaCod_Titleformat = 0;
         edtOrganizacao_Nome_Titleformat = 0;
         edtAreaTrabalho_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratante_razaosocial1_Visible = 1;
         edtavContratante_cnpj1_Visible = 1;
         edtavOrganizacao_nome1_Visible = 1;
         edtavAreatrabalho_descricao1_Visible = 1;
         edtAreaTrabalho_DiasParaPagar_Title = "para pagar";
         cmbAreaTrabalho_ValidaOSFM.Title.Text = "OS FM";
         edtAreaTrabalho_ServicoPadrao_Title = "padr�o";
         cmbAreaTrabalho_CalculoPFinal.Title.Text = "Calculo P. Final";
         edtContratante_RazaoSocial_Title = "Contratante";
         edtContratante_IE_Title = "IE";
         edtContratante_CNPJ_Title = "CNPJ";
         edtContratante_WebSite_Title = "Site";
         edtContratante_Telefone_Title = "Telefone";
         edtMunicipio_Nome_Title = "Munic�pio";
         edtMunicipio_Codigo_Title = "C�digo";
         edtEstado_Nome_Title = "Nome do Estado";
         edtContratante_PessoaCod_Title = "Pessoa Jur�dica";
         edtOrganizacao_Nome_Title = "Nome";
         edtAreaTrabalho_Descricao_Title = "�rea de Trabalho";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione �rea de Trabalho";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV87Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV88Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV51Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtAreaTrabalho_Descricao_Titleformat',ctrl:'AREATRABALHO_DESCRICAO',prop:'Titleformat'},{av:'edtAreaTrabalho_Descricao_Title',ctrl:'AREATRABALHO_DESCRICAO',prop:'Title'},{av:'edtOrganizacao_Nome_Titleformat',ctrl:'ORGANIZACAO_NOME',prop:'Titleformat'},{av:'edtOrganizacao_Nome_Title',ctrl:'ORGANIZACAO_NOME',prop:'Title'},{av:'edtContratante_PessoaCod_Titleformat',ctrl:'CONTRATANTE_PESSOACOD',prop:'Titleformat'},{av:'edtContratante_PessoaCod_Title',ctrl:'CONTRATANTE_PESSOACOD',prop:'Title'},{av:'edtEstado_Nome_Titleformat',ctrl:'ESTADO_NOME',prop:'Titleformat'},{av:'edtEstado_Nome_Title',ctrl:'ESTADO_NOME',prop:'Title'},{av:'edtMunicipio_Codigo_Titleformat',ctrl:'MUNICIPIO_CODIGO',prop:'Titleformat'},{av:'edtMunicipio_Codigo_Title',ctrl:'MUNICIPIO_CODIGO',prop:'Title'},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'edtContratante_Telefone_Titleformat',ctrl:'CONTRATANTE_TELEFONE',prop:'Titleformat'},{av:'edtContratante_Telefone_Title',ctrl:'CONTRATANTE_TELEFONE',prop:'Title'},{av:'edtContratante_WebSite_Titleformat',ctrl:'CONTRATANTE_WEBSITE',prop:'Titleformat'},{av:'edtContratante_WebSite_Title',ctrl:'CONTRATANTE_WEBSITE',prop:'Title'},{av:'edtContratante_CNPJ_Titleformat',ctrl:'CONTRATANTE_CNPJ',prop:'Titleformat'},{av:'edtContratante_CNPJ_Title',ctrl:'CONTRATANTE_CNPJ',prop:'Title'},{av:'edtContratante_IE_Titleformat',ctrl:'CONTRATANTE_IE',prop:'Titleformat'},{av:'edtContratante_IE_Title',ctrl:'CONTRATANTE_IE',prop:'Title'},{av:'edtContratante_RazaoSocial_Titleformat',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Titleformat'},{av:'edtContratante_RazaoSocial_Title',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Title'},{av:'cmbAreaTrabalho_CalculoPFinal'},{av:'edtAreaTrabalho_ServicoPadrao_Titleformat',ctrl:'AREATRABALHO_SERVICOPADRAO',prop:'Titleformat'},{av:'edtAreaTrabalho_ServicoPadrao_Title',ctrl:'AREATRABALHO_SERVICOPADRAO',prop:'Title'},{av:'cmbAreaTrabalho_ValidaOSFM'},{av:'edtAreaTrabalho_DiasParaPagar_Titleformat',ctrl:'AREATRABALHO_DIASPARAPAGAR',prop:'Titleformat'},{av:'edtAreaTrabalho_DiasParaPagar_Title',ctrl:'AREATRABALHO_DIASPARAPAGAR',prop:'Title'},{av:'AV85GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV86GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV87Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV88Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV51Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E170U2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E180U2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutAreaTrabalho_Codigo',fld:'vINOUTAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutAreaTrabalho_Descricao',fld:'vINOUTAREATRABALHO_DESCRICAO',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E120U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV87Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV88Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV51Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E140U2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'edtavContratante_cnpj1_Visible',ctrl:'vCONTRATANTE_CNPJ1',prop:'Visible'},{av:'edtavContratante_razaosocial1_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E130U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV87Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV88Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV51Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'edtavContratante_cnpj1_Visible',ctrl:'vCONTRATANTE_CNPJ1',prop:'Visible'},{av:'edtavContratante_razaosocial1_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV87Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV88Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV51Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutAreaTrabalho_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17AreaTrabalho_Descricao1 = "";
         AV87Organizacao_Nome1 = "";
         AV88Contratante_CNPJ1 = "";
         AV51Contratante_RazaoSocial1 = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV91Select_GXI = "";
         A6AreaTrabalho_Descricao = "";
         A1214Organizacao_Nome = "";
         A23Estado_UF = "";
         A24Estado_Nome = "";
         A26Municipio_Nome = "";
         A31Contratante_Telefone = "";
         A13Contratante_WebSite = "";
         A12Contratante_CNPJ = "";
         A11Contratante_IE = "";
         A9Contratante_RazaoSocial = "";
         A642AreaTrabalho_CalculoPFinal = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17AreaTrabalho_Descricao1 = "";
         lV87Organizacao_Nome1 = "";
         lV88Contratante_CNPJ1 = "";
         lV51Contratante_RazaoSocial1 = "";
         H000U3_A29Contratante_Codigo = new int[1] ;
         H000U3_n29Contratante_Codigo = new bool[] {false} ;
         H000U3_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         H000U3_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         H000U3_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         H000U3_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         H000U3_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H000U3_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H000U3_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         H000U3_A9Contratante_RazaoSocial = new String[] {""} ;
         H000U3_n9Contratante_RazaoSocial = new bool[] {false} ;
         H000U3_A11Contratante_IE = new String[] {""} ;
         H000U3_A12Contratante_CNPJ = new String[] {""} ;
         H000U3_n12Contratante_CNPJ = new bool[] {false} ;
         H000U3_A13Contratante_WebSite = new String[] {""} ;
         H000U3_n13Contratante_WebSite = new bool[] {false} ;
         H000U3_A31Contratante_Telefone = new String[] {""} ;
         H000U3_A26Municipio_Nome = new String[] {""} ;
         H000U3_A25Municipio_Codigo = new int[1] ;
         H000U3_n25Municipio_Codigo = new bool[] {false} ;
         H000U3_A24Estado_Nome = new String[] {""} ;
         H000U3_A23Estado_UF = new String[] {""} ;
         H000U3_A335Contratante_PessoaCod = new int[1] ;
         H000U3_A1214Organizacao_Nome = new String[] {""} ;
         H000U3_n1214Organizacao_Nome = new bool[] {false} ;
         H000U3_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         H000U3_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         H000U3_A6AreaTrabalho_Descricao = new String[] {""} ;
         H000U3_A5AreaTrabalho_Codigo = new int[1] ;
         H000U3_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         H000U3_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         H000U5_AGRID_nRecordCount = new long[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxphoneLink = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptareatrabalho__default(),
            new Object[][] {
                new Object[] {
               H000U3_A29Contratante_Codigo, H000U3_n29Contratante_Codigo, H000U3_A855AreaTrabalho_DiasParaPagar, H000U3_n855AreaTrabalho_DiasParaPagar, H000U3_A834AreaTrabalho_ValidaOSFM, H000U3_n834AreaTrabalho_ValidaOSFM, H000U3_A830AreaTrabalho_ServicoPadrao, H000U3_n830AreaTrabalho_ServicoPadrao, H000U3_A642AreaTrabalho_CalculoPFinal, H000U3_A9Contratante_RazaoSocial,
               H000U3_n9Contratante_RazaoSocial, H000U3_A11Contratante_IE, H000U3_A12Contratante_CNPJ, H000U3_n12Contratante_CNPJ, H000U3_A13Contratante_WebSite, H000U3_n13Contratante_WebSite, H000U3_A31Contratante_Telefone, H000U3_A26Municipio_Nome, H000U3_A25Municipio_Codigo, H000U3_n25Municipio_Codigo,
               H000U3_A24Estado_Nome, H000U3_A23Estado_UF, H000U3_A335Contratante_PessoaCod, H000U3_A1214Organizacao_Nome, H000U3_n1214Organizacao_Nome, H000U3_A1216AreaTrabalho_OrganizacaoCod, H000U3_n1216AreaTrabalho_OrganizacaoCod, H000U3_A6AreaTrabalho_Descricao, H000U3_A5AreaTrabalho_Codigo, H000U3_A272AreaTrabalho_ContagensQtdGeral,
               H000U3_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               H000U5_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_47 ;
      private short nGXsfl_47_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A272AreaTrabalho_ContagensQtdGeral ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_47_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAreaTrabalho_Descricao_Titleformat ;
      private short edtOrganizacao_Nome_Titleformat ;
      private short edtContratante_PessoaCod_Titleformat ;
      private short edtEstado_Nome_Titleformat ;
      private short edtMunicipio_Codigo_Titleformat ;
      private short edtMunicipio_Nome_Titleformat ;
      private short edtContratante_Telefone_Titleformat ;
      private short edtContratante_WebSite_Titleformat ;
      private short edtContratante_CNPJ_Titleformat ;
      private short edtContratante_IE_Titleformat ;
      private short edtContratante_RazaoSocial_Titleformat ;
      private short cmbAreaTrabalho_CalculoPFinal_Titleformat ;
      private short edtAreaTrabalho_ServicoPadrao_Titleformat ;
      private short cmbAreaTrabalho_ValidaOSFM_Titleformat ;
      private short edtAreaTrabalho_DiasParaPagar_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutAreaTrabalho_Codigo ;
      private int wcpOAV7InOutAreaTrabalho_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A5AreaTrabalho_Codigo ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int A335Contratante_PessoaCod ;
      private int A25Municipio_Codigo ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A29Contratante_Codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV84PageToGo ;
      private int edtavAreatrabalho_descricao1_Visible ;
      private int edtavOrganizacao_nome1_Visible ;
      private int edtavContratante_cnpj1_Visible ;
      private int edtavContratante_razaosocial1_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV85GridCurrentPage ;
      private long AV86GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_47_idx="0001" ;
      private String AV87Organizacao_Nome1 ;
      private String AV51Contratante_RazaoSocial1 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtAreaTrabalho_Descricao_Internalname ;
      private String edtAreaTrabalho_OrganizacaoCod_Internalname ;
      private String A1214Organizacao_Nome ;
      private String edtOrganizacao_Nome_Internalname ;
      private String edtContratante_PessoaCod_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String A24Estado_Nome ;
      private String edtEstado_Nome_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String A31Contratante_Telefone ;
      private String edtContratante_Telefone_Internalname ;
      private String edtContratante_WebSite_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String A11Contratante_IE ;
      private String edtContratante_IE_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String edtAreaTrabalho_ContagensQtdGeral_Internalname ;
      private String cmbAreaTrabalho_CalculoPFinal_Internalname ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String edtAreaTrabalho_ServicoPadrao_Internalname ;
      private String cmbAreaTrabalho_ValidaOSFM_Internalname ;
      private String edtAreaTrabalho_DiasParaPagar_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV87Organizacao_Nome1 ;
      private String lV51Contratante_RazaoSocial1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavAreatrabalho_descricao1_Internalname ;
      private String edtavOrganizacao_nome1_Internalname ;
      private String edtavContratante_cnpj1_Internalname ;
      private String edtavContratante_razaosocial1_Internalname ;
      private String edtAreaTrabalho_Descricao_Title ;
      private String edtOrganizacao_Nome_Title ;
      private String edtContratante_PessoaCod_Title ;
      private String edtEstado_Nome_Title ;
      private String edtMunicipio_Codigo_Title ;
      private String edtMunicipio_Nome_Title ;
      private String edtContratante_Telefone_Title ;
      private String edtContratante_WebSite_Title ;
      private String edtContratante_CNPJ_Title ;
      private String edtContratante_IE_Title ;
      private String edtContratante_RazaoSocial_Title ;
      private String edtAreaTrabalho_ServicoPadrao_Title ;
      private String edtAreaTrabalho_DiasParaPagar_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String TempTags ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavAreatrabalho_descricao1_Jsonclick ;
      private String edtavOrganizacao_nome1_Jsonclick ;
      private String edtavContratante_cnpj1_Jsonclick ;
      private String edtavContratante_razaosocial1_Jsonclick ;
      private String sGXsfl_47_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String edtAreaTrabalho_Descricao_Jsonclick ;
      private String edtAreaTrabalho_OrganizacaoCod_Jsonclick ;
      private String edtOrganizacao_Nome_Jsonclick ;
      private String edtContratante_PessoaCod_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String edtEstado_Nome_Jsonclick ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String gxphoneLink ;
      private String edtContratante_Telefone_Jsonclick ;
      private String edtContratante_WebSite_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String edtContratante_IE_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String edtAreaTrabalho_ContagensQtdGeral_Jsonclick ;
      private String cmbAreaTrabalho_CalculoPFinal_Jsonclick ;
      private String edtAreaTrabalho_ServicoPadrao_Jsonclick ;
      private String cmbAreaTrabalho_ValidaOSFM_Jsonclick ;
      private String edtAreaTrabalho_DiasParaPagar_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n1214Organizacao_Nome ;
      private bool n25Municipio_Codigo ;
      private bool n13Contratante_WebSite ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n272AreaTrabalho_ContagensQtdGeral ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n29Contratante_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutAreaTrabalho_Descricao ;
      private String wcpOAV8InOutAreaTrabalho_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17AreaTrabalho_Descricao1 ;
      private String AV88Contratante_CNPJ1 ;
      private String AV91Select_GXI ;
      private String A6AreaTrabalho_Descricao ;
      private String A13Contratante_WebSite ;
      private String A12Contratante_CNPJ ;
      private String lV17AreaTrabalho_Descricao1 ;
      private String lV88Contratante_CNPJ1 ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutAreaTrabalho_Codigo ;
      private String aP1_InOutAreaTrabalho_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbAreaTrabalho_CalculoPFinal ;
      private GXCombobox cmbAreaTrabalho_ValidaOSFM ;
      private IDataStoreProvider pr_default ;
      private int[] H000U3_A29Contratante_Codigo ;
      private bool[] H000U3_n29Contratante_Codigo ;
      private short[] H000U3_A855AreaTrabalho_DiasParaPagar ;
      private bool[] H000U3_n855AreaTrabalho_DiasParaPagar ;
      private bool[] H000U3_A834AreaTrabalho_ValidaOSFM ;
      private bool[] H000U3_n834AreaTrabalho_ValidaOSFM ;
      private int[] H000U3_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H000U3_n830AreaTrabalho_ServicoPadrao ;
      private String[] H000U3_A642AreaTrabalho_CalculoPFinal ;
      private String[] H000U3_A9Contratante_RazaoSocial ;
      private bool[] H000U3_n9Contratante_RazaoSocial ;
      private String[] H000U3_A11Contratante_IE ;
      private String[] H000U3_A12Contratante_CNPJ ;
      private bool[] H000U3_n12Contratante_CNPJ ;
      private String[] H000U3_A13Contratante_WebSite ;
      private bool[] H000U3_n13Contratante_WebSite ;
      private String[] H000U3_A31Contratante_Telefone ;
      private String[] H000U3_A26Municipio_Nome ;
      private int[] H000U3_A25Municipio_Codigo ;
      private bool[] H000U3_n25Municipio_Codigo ;
      private String[] H000U3_A24Estado_Nome ;
      private String[] H000U3_A23Estado_UF ;
      private int[] H000U3_A335Contratante_PessoaCod ;
      private String[] H000U3_A1214Organizacao_Nome ;
      private bool[] H000U3_n1214Organizacao_Nome ;
      private int[] H000U3_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] H000U3_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] H000U3_A6AreaTrabalho_Descricao ;
      private int[] H000U3_A5AreaTrabalho_Codigo ;
      private short[] H000U3_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] H000U3_n272AreaTrabalho_ContagensQtdGeral ;
      private long[] H000U5_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class promptareatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000U3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17AreaTrabalho_Descricao1 ,
                                             String AV87Organizacao_Nome1 ,
                                             String AV88Contratante_CNPJ1 ,
                                             String AV51Contratante_RazaoSocial1 ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             String A12Contratante_CNPJ ,
                                             String A9Contratante_RazaoSocial ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contratante_Codigo], T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T1.[AreaTrabalho_ServicoPadrao], T1.[AreaTrabalho_CalculoPFinal], T5.[Pessoa_Nome] AS Contratante_RazaoSocial, T2.[Contratante_IE], T5.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_WebSite], T2.[Contratante_Telefone], T3.[Municipio_Nome], T2.[Municipio_Codigo], T4.[Estado_Nome], T3.[Estado_UF], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T6.[Organizacao_Nome], T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T1.[AreaTrabalho_Descricao], T1.[AreaTrabalho_Codigo], COALESCE( T7.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral";
         sFromString = " FROM (((((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Estado] T4 WITH (NOLOCK) ON T4.[Estado_UF] = T3.[Estado_UF]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T6 WITH (NOLOCK) ON T6.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T7 ON T7.[Contagem_AreaTrabalhoCod] = T1.[AreaTrabalho_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AreaTrabalho_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV17AreaTrabalho_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV17AreaTrabalho_Descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AreaTrabalho_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV17AreaTrabalho_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV17AreaTrabalho_Descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87Organizacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Organizacao_Nome] like @lV87Organizacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Organizacao_Nome] like @lV87Organizacao_Nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87Organizacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Organizacao_Nome] like '%' + @lV87Organizacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Organizacao_Nome] like '%' + @lV87Organizacao_Nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88Contratante_CNPJ1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Docto] like @lV88Contratante_CNPJ1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Docto] like @lV88Contratante_CNPJ1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88Contratante_CNPJ1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Docto] like '%' + @lV88Contratante_CNPJ1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Docto] like '%' + @lV88Contratante_CNPJ1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contratante_RazaoSocial1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV51Contratante_RazaoSocial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV51Contratante_RazaoSocial1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contratante_RazaoSocial1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV51Contratante_RazaoSocial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV51Contratante_RazaoSocial1)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Organizacao_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Organizacao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Estado_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Estado_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Codigo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Municipio_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_Telefone]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_Telefone] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_WebSite]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_WebSite] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_IE]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_IE] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_CalculoPFinal]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_CalculoPFinal] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_ServicoPadrao]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_ServicoPadrao] DESC";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_ValidaOSFM]";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_ValidaOSFM] DESC";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_DiasParaPagar]";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_DiasParaPagar] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H000U5( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17AreaTrabalho_Descricao1 ,
                                             String AV87Organizacao_Nome1 ,
                                             String AV88Contratante_CNPJ1 ,
                                             String AV51Contratante_RazaoSocial1 ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             String A12Contratante_CNPJ ,
                                             String A9Contratante_RazaoSocial ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Estado] T4 WITH (NOLOCK) ON T4.[Estado_UF] = T3.[Estado_UF]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Organizacao] T6 WITH (NOLOCK) ON T6.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T7 ON T7.[Contagem_AreaTrabalhoCod] = T1.[AreaTrabalho_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AreaTrabalho_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV17AreaTrabalho_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like @lV17AreaTrabalho_Descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AreaTrabalho_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV17AreaTrabalho_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AreaTrabalho_Descricao] like '%' + @lV17AreaTrabalho_Descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87Organizacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Organizacao_Nome] like @lV87Organizacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Organizacao_Nome] like @lV87Organizacao_Nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87Organizacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Organizacao_Nome] like '%' + @lV87Organizacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Organizacao_Nome] like '%' + @lV87Organizacao_Nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88Contratante_CNPJ1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Docto] like @lV88Contratante_CNPJ1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Docto] like @lV88Contratante_CNPJ1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88Contratante_CNPJ1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Docto] like '%' + @lV88Contratante_CNPJ1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Docto] like '%' + @lV88Contratante_CNPJ1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contratante_RazaoSocial1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV51Contratante_RazaoSocial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV51Contratante_RazaoSocial1)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contratante_RazaoSocial1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV51Contratante_RazaoSocial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV51Contratante_RazaoSocial1)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000U3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] );
               case 1 :
                     return conditional_H000U5(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (bool)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000U3 ;
          prmH000U3 = new Object[] {
          new Object[] {"@lV17AreaTrabalho_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV17AreaTrabalho_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87Organizacao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87Organizacao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV88Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV88Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV51Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV51Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000U5 ;
          prmH000U5 = new Object[] {
          new Object[] {"@lV17AreaTrabalho_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV17AreaTrabalho_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87Organizacao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87Organizacao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV88Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV88Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV51Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV51Contratante_RazaoSocial1",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000U3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000U3,11,0,true,false )
             ,new CursorDef("H000U5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000U5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 2) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((String[]) buf[21])[0] = rslt.getString(14, 2) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((String[]) buf[23])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((String[]) buf[27])[0] = rslt.getVarchar(18) ;
                ((int[]) buf[28])[0] = rslt.getInt(19) ;
                ((short[]) buf[29])[0] = rslt.getShort(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
