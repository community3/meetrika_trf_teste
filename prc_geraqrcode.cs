/*
               File: PRC_GeraQRCode
        Description: PRC_Gera QRCode
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:36.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_geraqrcode : GXProcedure
   {
      public prc_geraqrcode( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_geraqrcode( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           String aP1_PdfFolder ,
                           String aP2_FileName ,
                           String aP3_AbsolutePath ,
                           out String aP4_ImageName ,
                           out String aP5_data )
      {
         this.AV17Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV16PdfFolder = aP1_PdfFolder;
         this.AV11FileName = aP2_FileName;
         this.AV9AbsolutePath = aP3_AbsolutePath;
         this.AV15ImageName = "" ;
         this.AV10data = "" ;
         initialize();
         executePrivate();
         aP4_ImageName=this.AV15ImageName;
         aP5_data=this.AV10data;
      }

      public String executeUdp( int aP0_Usuario_Codigo ,
                                String aP1_PdfFolder ,
                                String aP2_FileName ,
                                String aP3_AbsolutePath ,
                                out String aP4_ImageName )
      {
         this.AV17Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV16PdfFolder = aP1_PdfFolder;
         this.AV11FileName = aP2_FileName;
         this.AV9AbsolutePath = aP3_AbsolutePath;
         this.AV15ImageName = "" ;
         this.AV10data = "" ;
         initialize();
         executePrivate();
         aP4_ImageName=this.AV15ImageName;
         aP5_data=this.AV10data;
         return AV10data ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 String aP1_PdfFolder ,
                                 String aP2_FileName ,
                                 String aP3_AbsolutePath ,
                                 out String aP4_ImageName ,
                                 out String aP5_data )
      {
         prc_geraqrcode objprc_geraqrcode;
         objprc_geraqrcode = new prc_geraqrcode();
         objprc_geraqrcode.AV17Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_geraqrcode.AV16PdfFolder = aP1_PdfFolder;
         objprc_geraqrcode.AV11FileName = aP2_FileName;
         objprc_geraqrcode.AV9AbsolutePath = aP3_AbsolutePath;
         objprc_geraqrcode.AV15ImageName = "" ;
         objprc_geraqrcode.AV10data = "" ;
         objprc_geraqrcode.context.SetSubmitInitialConfig(context);
         objprc_geraqrcode.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_geraqrcode);
         aP4_ImageName=this.AV15ImageName;
         aP5_data=this.AV10data;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_geraqrcode)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10data = StringUtil.Trim( AV14HTTPRequest.BaseURL) + "autenticidade.aspx";
         GxCbb.generateimage( "Byte",  5,  7,  "L",  AV10data);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9AbsolutePath)) )
         {
            AV15ImageName = StringUtil.Trim( AV9AbsolutePath) + StringUtil.Trim( AV16PdfFolder) + "\\" + StringUtil.Trim( AV11FileName) + StringUtil.Trim( StringUtil.Str( (decimal)(AV17Usuario_Codigo), 6, 0)) + ".bmp";
            AV8File.Source = AV15ImageName;
            if ( AV8File.Exists() )
            {
               AV8File.Delete();
            }
            GxCbb.save( AV15ImageName);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV14HTTPRequest = new GxHttpRequest( context);
         GxCbb = new SdtGxCbb(context);
         AV8File = new GxFile(context.GetPhysicalPath());
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV17Usuario_Codigo ;
      private String AV16PdfFolder ;
      private String AV11FileName ;
      private String AV10data ;
      private String AV15ImageName ;
      private String AV9AbsolutePath ;
      private String aP4_ImageName ;
      private String aP5_data ;
      private GxHttpRequest AV14HTTPRequest ;
      private GxFile AV8File ;
      private SdtGxCbb GxCbb ;
   }

}
