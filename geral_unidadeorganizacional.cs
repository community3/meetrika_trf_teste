/*
               File: Geral_UnidadeOrganizacional
        Description: Unidade Organizacional
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/28/2020 12:10:24.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_unidadeorganizacional : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ESTADO_UF") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAESTADO_UF2380( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            AV7UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUNIDADEORGANIZACIONAL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UnidadeOrganizacional_Codigo), "ZZZZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAUNIDADEORGANIZACIONAL_VINCULADA2380( AV7UnidadeOrganizacional_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel3"+"_"+"UNIDADEORGANIZACINAL_ARVORE") == 0 )
         {
            A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n613UnidadeOrganizacional_Vinculada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX3ASAUNIDADEORGANIZACINAL_ARVORE2380( A613UnidadeOrganizacional_Vinculada) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_21") == 0 )
         {
            A609TpUo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_21( A609TpUo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A23Estado_UF = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A23Estado_UF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_23") == 0 )
         {
            A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n613UnidadeOrganizacional_Vinculada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_23( A613UnidadeOrganizacional_Vinculada) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeOrganizacional_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUNIDADEORGANIZACIONAL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UnidadeOrganizacional_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynTpUo_Codigo.Name = "TPUO_CODIGO";
         dynTpUo_Codigo.WebTags = "";
         dynTpUo_Codigo.removeAllItems();
         /* Using cursor T00237 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            dynTpUo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T00237_A609TpUo_Codigo[0]), 6, 0)), T00237_A610TpUo_Nome[0], 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( dynTpUo_Codigo.ItemCount > 0 )
         {
            A609TpUo_Codigo = (int)(NumberUtil.Val( dynTpUo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
         }
         dynEstado_UF.Name = "ESTADO_UF";
         dynEstado_UF.WebTags = "";
         dynUnidadeOrganizacional_Vinculada.Name = "UNIDADEORGANIZACIONAL_VINCULADA";
         dynUnidadeOrganizacional_Vinculada.WebTags = "";
         chkUnidadeOrganizacional_Ativo.Name = "UNIDADEORGANIZACIONAL_ATIVO";
         chkUnidadeOrganizacional_Ativo.WebTags = "";
         chkUnidadeOrganizacional_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeOrganizacional_Ativo_Internalname, "TitleCaption", chkUnidadeOrganizacional_Ativo.Caption);
         chkUnidadeOrganizacional_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Unidade Organizacional", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynTpUo_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public geral_unidadeorganizacional( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public geral_unidadeorganizacional( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_UnidadeOrganizacional_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7UnidadeOrganizacional_Codigo = aP1_UnidadeOrganizacional_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynTpUo_Codigo = new GXCombobox();
         dynEstado_UF = new GXCombobox();
         dynUnidadeOrganizacional_Vinculada = new GXCombobox();
         chkUnidadeOrganizacional_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynTpUo_Codigo.ItemCount > 0 )
         {
            A609TpUo_Codigo = (int)(NumberUtil.Val( dynTpUo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
         }
         if ( dynEstado_UF.ItemCount > 0 )
         {
            A23Estado_UF = dynEstado_UF.getValidValue(A23Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         }
         if ( dynUnidadeOrganizacional_Vinculada.ItemCount > 0 )
         {
            A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( dynUnidadeOrganizacional_Vinculada.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0))), "."));
            n613UnidadeOrganizacional_Vinculada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2380( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2380e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUnidadeOrganizacional_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ",", "")), ((edtUnidadeOrganizacional_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUnidadeOrganizacional_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtUnidadeOrganizacional_Codigo_Visible, edtUnidadeOrganizacional_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_UnidadeOrganizacional.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2380( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2380( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2380e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_2380( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_2380e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2380e( true) ;
         }
         else
         {
            wb_table1_2_2380e( false) ;
         }
      }

      protected void wb_table3_41_2380( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_2380e( true) ;
         }
         else
         {
            wb_table3_41_2380e( false) ;
         }
      }

      protected void wb_table2_5_2380( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2380( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2380e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2380e( true) ;
         }
         else
         {
            wb_table2_5_2380e( false) ;
         }
      }

      protected void wb_table4_13_2380( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktpuo_codigo_Internalname, "Tipo UO", "", "", lblTextblocktpuo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTpUo_Codigo, dynTpUo_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)), 1, dynTpUo_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTpUo_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_Geral_UnidadeOrganizacional.htm");
            dynTpUo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTpUo_Codigo_Internalname, "Values", (String)(dynTpUo_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidadeorganizacional_nome_Internalname, "Nome", "", "", lblTextblockunidadeorganizacional_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUnidadeOrganizacional_Nome_Internalname, StringUtil.RTrim( A612UnidadeOrganizacional_Nome), StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUnidadeOrganizacional_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUnidadeOrganizacional_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockestado_uf_Internalname, "UF", "", "", lblTextblockestado_uf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynEstado_UF, dynEstado_UF_Internalname, StringUtil.RTrim( A23Estado_UF), 1, dynEstado_UF_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, dynEstado_UF.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_Geral_UnidadeOrganizacional.htm");
            dynEstado_UF.CurrentValue = StringUtil.RTrim( A23Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Values", (String)(dynEstado_UF.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidadeorganizacional_vinculada_Internalname, "UO Vinculada", "", "", lblTextblockunidadeorganizacional_vinculada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynUnidadeOrganizacional_Vinculada, dynUnidadeOrganizacional_Vinculada_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)), 1, dynUnidadeOrganizacional_Vinculada_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynUnidadeOrganizacional_Vinculada.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_Geral_UnidadeOrganizacional.htm");
            dynUnidadeOrganizacional_Vinculada.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUnidadeOrganizacional_Vinculada_Internalname, "Values", (String)(dynUnidadeOrganizacional_Vinculada.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidadeorganizacional_ativo_Internalname, "Ativa?", "", "", lblTextblockunidadeorganizacional_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockunidadeorganizacional_ativo_Visible, 1, 0, "HLP_Geral_UnidadeOrganizacional.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUnidadeOrganizacional_Ativo_Internalname, StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo), "", "", chkUnidadeOrganizacional_Ativo.Visible, chkUnidadeOrganizacional_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(38, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2380e( true) ;
         }
         else
         {
            wb_table4_13_2380e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11232 */
         E11232 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynTpUo_Codigo.CurrentValue = cgiGet( dynTpUo_Codigo_Internalname);
               A609TpUo_Codigo = (int)(NumberUtil.Val( cgiGet( dynTpUo_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
               A612UnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtUnidadeOrganizacional_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A612UnidadeOrganizacional_Nome", A612UnidadeOrganizacional_Nome);
               dynEstado_UF.CurrentValue = cgiGet( dynEstado_UF_Internalname);
               A23Estado_UF = cgiGet( dynEstado_UF_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
               dynUnidadeOrganizacional_Vinculada.CurrentValue = cgiGet( dynUnidadeOrganizacional_Vinculada_Internalname);
               A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( cgiGet( dynUnidadeOrganizacional_Vinculada_Internalname), "."));
               n613UnidadeOrganizacional_Vinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
               n613UnidadeOrganizacional_Vinculada = ((0==A613UnidadeOrganizacional_Vinculada) ? true : false);
               A629UnidadeOrganizacional_Ativo = StringUtil.StrToBool( cgiGet( chkUnidadeOrganizacional_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A629UnidadeOrganizacional_Ativo", A629UnidadeOrganizacional_Ativo);
               A611UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
               /* Read saved values. */
               Z611UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z611UnidadeOrganizacional_Codigo"), ",", "."));
               Z612UnidadeOrganizacional_Nome = cgiGet( "Z612UnidadeOrganizacional_Nome");
               Z629UnidadeOrganizacional_Ativo = StringUtil.StrToBool( cgiGet( "Z629UnidadeOrganizacional_Ativo"));
               Z609TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z609TpUo_Codigo"), ",", "."));
               Z23Estado_UF = cgiGet( "Z23Estado_UF");
               Z613UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( "Z613UnidadeOrganizacional_Vinculada"), ",", "."));
               n613UnidadeOrganizacional_Vinculada = ((0==A613UnidadeOrganizacional_Vinculada) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N609TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( "N609TpUo_Codigo"), ",", "."));
               N23Estado_UF = cgiGet( "N23Estado_UF");
               N613UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( "N613UnidadeOrganizacional_Vinculada"), ",", "."));
               n613UnidadeOrganizacional_Vinculada = ((0==A613UnidadeOrganizacional_Vinculada) ? true : false);
               A1174UnidadeOrganizacinal_Arvore = cgiGet( "UNIDADEORGANIZACINAL_ARVORE");
               AV7UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( "vUNIDADEORGANIZACIONAL_CODIGO"), ",", "."));
               AV11Insert_TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TPUO_CODIGO"), ",", "."));
               AV12Insert_Estado_UF = cgiGet( "vINSERT_ESTADO_UF");
               AV13Insert_UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( "vINSERT_UNIDADEORGANIZACIONAL_VINCULADA"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A610TpUo_Nome = cgiGet( "TPUO_NOME");
               A1082UnidadeOrganizacional_VinculadaNom = cgiGet( "UNIDADEORGANIZACIONAL_VINCULADANOM");
               n1082UnidadeOrganizacional_VinculadaNom = false;
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Geral_UnidadeOrganizacional";
               A611UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A611UnidadeOrganizacional_Codigo != Z611UnidadeOrganizacional_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("geral_unidadeorganizacional:[SecurityCheckFailed value for]"+"UnidadeOrganizacional_Codigo:"+context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("geral_unidadeorganizacional:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A611UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode80 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode80;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound80 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_230( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "UNIDADEORGANIZACIONAL_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtUnidadeOrganizacional_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11232 */
                           E11232 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12232 */
                           E12232 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12232 */
            E12232 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2380( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2380( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_230( )
      {
         BeforeValidate2380( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2380( ) ;
            }
            else
            {
               CheckExtendedTable2380( ) ;
               CloseExtendedTableCursors2380( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption230( )
      {
      }

      protected void E11232( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "TpUo_Codigo") == 0 )
               {
                  AV11Insert_TpUo_Codigo = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_TpUo_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Estado_UF") == 0 )
               {
                  AV12Insert_Estado_UF = AV14TrnContextAtt.gxTpr_Attributevalue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Estado_UF", AV12Insert_Estado_UF);
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "UnidadeOrganizacional_Vinculada") == 0 )
               {
                  AV13Insert_UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_UnidadeOrganizacional_Vinculada), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtUnidadeOrganizacional_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacional_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeOrganizacional_Codigo_Visible), 5, 0)));
      }

      protected void E12232( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwgeral_unidadeorganizacional.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2380( short GX_JID )
      {
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z612UnidadeOrganizacional_Nome = T00233_A612UnidadeOrganizacional_Nome[0];
               Z629UnidadeOrganizacional_Ativo = T00233_A629UnidadeOrganizacional_Ativo[0];
               Z609TpUo_Codigo = T00233_A609TpUo_Codigo[0];
               Z23Estado_UF = T00233_A23Estado_UF[0];
               Z613UnidadeOrganizacional_Vinculada = T00233_A613UnidadeOrganizacional_Vinculada[0];
            }
            else
            {
               Z612UnidadeOrganizacional_Nome = A612UnidadeOrganizacional_Nome;
               Z629UnidadeOrganizacional_Ativo = A629UnidadeOrganizacional_Ativo;
               Z609TpUo_Codigo = A609TpUo_Codigo;
               Z23Estado_UF = A23Estado_UF;
               Z613UnidadeOrganizacional_Vinculada = A613UnidadeOrganizacional_Vinculada;
            }
         }
         if ( GX_JID == -20 )
         {
            Z611UnidadeOrganizacional_Codigo = A611UnidadeOrganizacional_Codigo;
            Z612UnidadeOrganizacional_Nome = A612UnidadeOrganizacional_Nome;
            Z629UnidadeOrganizacional_Ativo = A629UnidadeOrganizacional_Ativo;
            Z609TpUo_Codigo = A609TpUo_Codigo;
            Z23Estado_UF = A23Estado_UF;
            Z613UnidadeOrganizacional_Vinculada = A613UnidadeOrganizacional_Vinculada;
            Z610TpUo_Nome = A610TpUo_Nome;
            Z1082UnidadeOrganizacional_VinculadaNom = A1082UnidadeOrganizacional_VinculadaNom;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAESTADO_UF_html2380( ) ;
         edtUnidadeOrganizacional_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacional_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeOrganizacional_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV15Pgmname = "Geral_UnidadeOrganizacional";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         edtUnidadeOrganizacional_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacional_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeOrganizacional_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         GXAUNIDADEORGANIZACIONAL_VINCULADA_html2380( AV7UnidadeOrganizacional_Codigo) ;
         if ( ! (0==AV7UnidadeOrganizacional_Codigo) )
         {
            A611UnidadeOrganizacional_Codigo = AV7UnidadeOrganizacional_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_TpUo_Codigo) )
         {
            dynTpUo_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTpUo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTpUo_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynTpUo_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTpUo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTpUo_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV12Insert_Estado_UF)) )
         {
            dynEstado_UF.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynEstado_UF.Enabled), 5, 0)));
         }
         else
         {
            dynEstado_UF.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynEstado_UF.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_UnidadeOrganizacional_Vinculada) )
         {
            dynUnidadeOrganizacional_Vinculada.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUnidadeOrganizacional_Vinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUnidadeOrganizacional_Vinculada.Enabled), 5, 0)));
         }
         else
         {
            dynUnidadeOrganizacional_Vinculada.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUnidadeOrganizacional_Vinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUnidadeOrganizacional_Vinculada.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkUnidadeOrganizacional_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeOrganizacional_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUnidadeOrganizacional_Ativo.Visible), 5, 0)));
         lblTextblockunidadeorganizacional_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockunidadeorganizacional_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockunidadeorganizacional_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_UnidadeOrganizacional_Vinculada) )
         {
            A613UnidadeOrganizacional_Vinculada = AV13Insert_UnidadeOrganizacional_Vinculada;
            n613UnidadeOrganizacional_Vinculada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV12Insert_Estado_UF)) )
         {
            A23Estado_UF = AV12Insert_Estado_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_TpUo_Codigo) )
         {
            A609TpUo_Codigo = AV11Insert_TpUo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A629UnidadeOrganizacional_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A629UnidadeOrganizacional_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A629UnidadeOrganizacional_Ativo", A629UnidadeOrganizacional_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00236 */
            pr_default.execute(4, new Object[] {n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada});
            A1082UnidadeOrganizacional_VinculadaNom = T00236_A1082UnidadeOrganizacional_VinculadaNom[0];
            n1082UnidadeOrganizacional_VinculadaNom = T00236_n1082UnidadeOrganizacional_VinculadaNom[0];
            pr_default.close(4);
            if ( ! T00233_n613UnidadeOrganizacional_Vinculada[0] )
            {
               GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
               new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
               A1174UnidadeOrganizacinal_Arvore = GXt_char1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
            }
            else
            {
               A1174UnidadeOrganizacinal_Arvore = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
            }
            /* Using cursor T00234 */
            pr_default.execute(2, new Object[] {A609TpUo_Codigo});
            A610TpUo_Nome = T00234_A610TpUo_Nome[0];
            pr_default.close(2);
         }
      }

      protected void Load2380( )
      {
         /* Using cursor T00238 */
         pr_default.execute(6, new Object[] {A611UnidadeOrganizacional_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound80 = 1;
            A612UnidadeOrganizacional_Nome = T00238_A612UnidadeOrganizacional_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A612UnidadeOrganizacional_Nome", A612UnidadeOrganizacional_Nome);
            A610TpUo_Nome = T00238_A610TpUo_Nome[0];
            A1082UnidadeOrganizacional_VinculadaNom = T00238_A1082UnidadeOrganizacional_VinculadaNom[0];
            n1082UnidadeOrganizacional_VinculadaNom = T00238_n1082UnidadeOrganizacional_VinculadaNom[0];
            A629UnidadeOrganizacional_Ativo = T00238_A629UnidadeOrganizacional_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A629UnidadeOrganizacional_Ativo", A629UnidadeOrganizacional_Ativo);
            A609TpUo_Codigo = T00238_A609TpUo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
            A23Estado_UF = T00238_A23Estado_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            A613UnidadeOrganizacional_Vinculada = T00238_A613UnidadeOrganizacional_Vinculada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            n613UnidadeOrganizacional_Vinculada = T00238_n613UnidadeOrganizacional_Vinculada[0];
            ZM2380( -20) ;
         }
         pr_default.close(6);
         OnLoadActions2380( ) ;
      }

      protected void OnLoadActions2380( )
      {
         if ( ! T00233_n613UnidadeOrganizacional_Vinculada[0] )
         {
            GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
            new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         }
         else
         {
            A1174UnidadeOrganizacinal_Arvore = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         }
      }

      protected void CheckExtendedTable2380( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00234 */
         pr_default.execute(2, new Object[] {A609TpUo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tipo Unidade Organizacional'.", "ForeignKeyNotFound", 1, "TPUO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTpUo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A610TpUo_Nome = T00234_A610TpUo_Nome[0];
         pr_default.close(2);
         /* Using cursor T00235 */
         pr_default.execute(3, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T00236 */
         pr_default.execute(4, new Object[] {n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A613UnidadeOrganizacional_Vinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Unidade Organizacional_Unidade Vinculada'.", "ForeignKeyNotFound", 1, "UNIDADEORGANIZACIONAL_VINCULADA");
               AnyError = 1;
               GX_FocusControl = dynUnidadeOrganizacional_Vinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1082UnidadeOrganizacional_VinculadaNom = T00236_A1082UnidadeOrganizacional_VinculadaNom[0];
         n1082UnidadeOrganizacional_VinculadaNom = T00236_n1082UnidadeOrganizacional_VinculadaNom[0];
         pr_default.close(4);
         if ( ! T00233_n613UnidadeOrganizacional_Vinculada[0] )
         {
            GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
            new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         }
         else
         {
            A1174UnidadeOrganizacinal_Arvore = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         }
      }

      protected void CloseExtendedTableCursors2380( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_21( int A609TpUo_Codigo )
      {
         /* Using cursor T00239 */
         pr_default.execute(7, new Object[] {A609TpUo_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tipo Unidade Organizacional'.", "ForeignKeyNotFound", 1, "TPUO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTpUo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A610TpUo_Nome = T00239_A610TpUo_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A610TpUo_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_22( String A23Estado_UF )
      {
         /* Using cursor T002310 */
         pr_default.execute(8, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_23( int A613UnidadeOrganizacional_Vinculada )
      {
         /* Using cursor T002311 */
         pr_default.execute(9, new Object[] {n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A613UnidadeOrganizacional_Vinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Unidade Organizacional_Unidade Vinculada'.", "ForeignKeyNotFound", 1, "UNIDADEORGANIZACIONAL_VINCULADA");
               AnyError = 1;
               GX_FocusControl = dynUnidadeOrganizacional_Vinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1082UnidadeOrganizacional_VinculadaNom = T002311_A1082UnidadeOrganizacional_VinculadaNom[0];
         n1082UnidadeOrganizacional_VinculadaNom = T002311_n1082UnidadeOrganizacional_VinculadaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1082UnidadeOrganizacional_VinculadaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey2380( )
      {
         /* Using cursor T002312 */
         pr_default.execute(10, new Object[] {A611UnidadeOrganizacional_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound80 = 1;
         }
         else
         {
            RcdFound80 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00233 */
         pr_default.execute(1, new Object[] {A611UnidadeOrganizacional_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2380( 20) ;
            RcdFound80 = 1;
            A611UnidadeOrganizacional_Codigo = T00233_A611UnidadeOrganizacional_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
            A612UnidadeOrganizacional_Nome = T00233_A612UnidadeOrganizacional_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A612UnidadeOrganizacional_Nome", A612UnidadeOrganizacional_Nome);
            A629UnidadeOrganizacional_Ativo = T00233_A629UnidadeOrganizacional_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A629UnidadeOrganizacional_Ativo", A629UnidadeOrganizacional_Ativo);
            A609TpUo_Codigo = T00233_A609TpUo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
            A23Estado_UF = T00233_A23Estado_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            A613UnidadeOrganizacional_Vinculada = T00233_A613UnidadeOrganizacional_Vinculada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            n613UnidadeOrganizacional_Vinculada = T00233_n613UnidadeOrganizacional_Vinculada[0];
            Z611UnidadeOrganizacional_Codigo = A611UnidadeOrganizacional_Codigo;
            sMode80 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2380( ) ;
            if ( AnyError == 1 )
            {
               RcdFound80 = 0;
               InitializeNonKey2380( ) ;
            }
            Gx_mode = sMode80;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound80 = 0;
            InitializeNonKey2380( ) ;
            sMode80 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode80;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2380( ) ;
         if ( RcdFound80 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound80 = 0;
         /* Using cursor T002313 */
         pr_default.execute(11, new Object[] {A611UnidadeOrganizacional_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T002313_A611UnidadeOrganizacional_Codigo[0] < A611UnidadeOrganizacional_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T002313_A611UnidadeOrganizacional_Codigo[0] > A611UnidadeOrganizacional_Codigo ) ) )
            {
               A611UnidadeOrganizacional_Codigo = T002313_A611UnidadeOrganizacional_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
               RcdFound80 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound80 = 0;
         /* Using cursor T002314 */
         pr_default.execute(12, new Object[] {A611UnidadeOrganizacional_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T002314_A611UnidadeOrganizacional_Codigo[0] > A611UnidadeOrganizacional_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T002314_A611UnidadeOrganizacional_Codigo[0] < A611UnidadeOrganizacional_Codigo ) ) )
            {
               A611UnidadeOrganizacional_Codigo = T002314_A611UnidadeOrganizacional_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
               RcdFound80 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2380( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynTpUo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2380( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound80 == 1 )
            {
               if ( A611UnidadeOrganizacional_Codigo != Z611UnidadeOrganizacional_Codigo )
               {
                  A611UnidadeOrganizacional_Codigo = Z611UnidadeOrganizacional_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "UNIDADEORGANIZACIONAL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUnidadeOrganizacional_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynTpUo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2380( ) ;
                  GX_FocusControl = dynTpUo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A611UnidadeOrganizacional_Codigo != Z611UnidadeOrganizacional_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynTpUo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2380( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "UNIDADEORGANIZACIONAL_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtUnidadeOrganizacional_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynTpUo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2380( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A611UnidadeOrganizacional_Codigo != Z611UnidadeOrganizacional_Codigo )
         {
            A611UnidadeOrganizacional_Codigo = Z611UnidadeOrganizacional_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "UNIDADEORGANIZACIONAL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUnidadeOrganizacional_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynTpUo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2380( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00232 */
            pr_default.execute(0, new Object[] {A611UnidadeOrganizacional_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_UnidadeOrganizacional"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z612UnidadeOrganizacional_Nome, T00232_A612UnidadeOrganizacional_Nome[0]) != 0 ) || ( Z629UnidadeOrganizacional_Ativo != T00232_A629UnidadeOrganizacional_Ativo[0] ) || ( Z609TpUo_Codigo != T00232_A609TpUo_Codigo[0] ) || ( StringUtil.StrCmp(Z23Estado_UF, T00232_A23Estado_UF[0]) != 0 ) || ( Z613UnidadeOrganizacional_Vinculada != T00232_A613UnidadeOrganizacional_Vinculada[0] ) )
            {
               if ( StringUtil.StrCmp(Z612UnidadeOrganizacional_Nome, T00232_A612UnidadeOrganizacional_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("geral_unidadeorganizacional:[seudo value changed for attri]"+"UnidadeOrganizacional_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z612UnidadeOrganizacional_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00232_A612UnidadeOrganizacional_Nome[0]);
               }
               if ( Z629UnidadeOrganizacional_Ativo != T00232_A629UnidadeOrganizacional_Ativo[0] )
               {
                  GXUtil.WriteLog("geral_unidadeorganizacional:[seudo value changed for attri]"+"UnidadeOrganizacional_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z629UnidadeOrganizacional_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00232_A629UnidadeOrganizacional_Ativo[0]);
               }
               if ( Z609TpUo_Codigo != T00232_A609TpUo_Codigo[0] )
               {
                  GXUtil.WriteLog("geral_unidadeorganizacional:[seudo value changed for attri]"+"TpUo_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z609TpUo_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00232_A609TpUo_Codigo[0]);
               }
               if ( StringUtil.StrCmp(Z23Estado_UF, T00232_A23Estado_UF[0]) != 0 )
               {
                  GXUtil.WriteLog("geral_unidadeorganizacional:[seudo value changed for attri]"+"Estado_UF");
                  GXUtil.WriteLogRaw("Old: ",Z23Estado_UF);
                  GXUtil.WriteLogRaw("Current: ",T00232_A23Estado_UF[0]);
               }
               if ( Z613UnidadeOrganizacional_Vinculada != T00232_A613UnidadeOrganizacional_Vinculada[0] )
               {
                  GXUtil.WriteLog("geral_unidadeorganizacional:[seudo value changed for attri]"+"UnidadeOrganizacional_Vinculada");
                  GXUtil.WriteLogRaw("Old: ",Z613UnidadeOrganizacional_Vinculada);
                  GXUtil.WriteLogRaw("Current: ",T00232_A613UnidadeOrganizacional_Vinculada[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Geral_UnidadeOrganizacional"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2380( )
      {
         BeforeValidate2380( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2380( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2380( 0) ;
            CheckOptimisticConcurrency2380( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2380( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2380( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002315 */
                     pr_default.execute(13, new Object[] {A612UnidadeOrganizacional_Nome, A629UnidadeOrganizacional_Ativo, A609TpUo_Codigo, A23Estado_UF, n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada});
                     A611UnidadeOrganizacional_Codigo = T002315_A611UnidadeOrganizacional_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_UnidadeOrganizacional") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption230( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2380( ) ;
            }
            EndLevel2380( ) ;
         }
         CloseExtendedTableCursors2380( ) ;
      }

      protected void Update2380( )
      {
         BeforeValidate2380( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2380( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2380( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2380( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2380( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002316 */
                     pr_default.execute(14, new Object[] {A612UnidadeOrganizacional_Nome, A629UnidadeOrganizacional_Ativo, A609TpUo_Codigo, A23Estado_UF, n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada, A611UnidadeOrganizacional_Codigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_UnidadeOrganizacional") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_UnidadeOrganizacional"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2380( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2380( ) ;
         }
         CloseExtendedTableCursors2380( ) ;
      }

      protected void DeferredUpdate2380( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2380( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2380( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2380( ) ;
            AfterConfirm2380( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2380( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002317 */
                  pr_default.execute(15, new Object[] {A611UnidadeOrganizacional_Codigo});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("Geral_UnidadeOrganizacional") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode80 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2380( ) ;
         Gx_mode = sMode80;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2380( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002318 */
            pr_default.execute(16, new Object[] {A609TpUo_Codigo});
            A610TpUo_Nome = T002318_A610TpUo_Nome[0];
            pr_default.close(16);
            /* Using cursor T002319 */
            pr_default.execute(17, new Object[] {n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada});
            A1082UnidadeOrganizacional_VinculadaNom = T002319_A1082UnidadeOrganizacional_VinculadaNom[0];
            n1082UnidadeOrganizacional_VinculadaNom = T002319_n1082UnidadeOrganizacional_VinculadaNom[0];
            pr_default.close(17);
            if ( ! T00233_n613UnidadeOrganizacional_Vinculada[0] )
            {
               GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
               new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
               A1174UnidadeOrganizacinal_Arvore = GXt_char1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
            }
            else
            {
               A1174UnidadeOrganizacinal_Arvore = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002320 */
            pr_default.execute(18, new Object[] {A611UnidadeOrganizacional_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidade Organizacional"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor T002321 */
            pr_default.execute(19, new Object[] {A611UnidadeOrganizacional_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor T002322 */
            pr_default.execute(20, new Object[] {A611UnidadeOrganizacional_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Cargos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T002323 */
            pr_default.execute(21, new Object[] {A611UnidadeOrganizacional_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T002324 */
            pr_default.execute(22, new Object[] {A611UnidadeOrganizacional_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
         }
      }

      protected void EndLevel2380( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2380( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(17);
            context.CommitDataStores( "Geral_UnidadeOrganizacional");
            if ( AnyError == 0 )
            {
               ConfirmValues230( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(17);
            context.RollbackDataStores( "Geral_UnidadeOrganizacional");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2380( )
      {
         /* Scan By routine */
         /* Using cursor T002325 */
         pr_default.execute(23);
         RcdFound80 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound80 = 1;
            A611UnidadeOrganizacional_Codigo = T002325_A611UnidadeOrganizacional_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2380( )
      {
         /* Scan next routine */
         pr_default.readNext(23);
         RcdFound80 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound80 = 1;
            A611UnidadeOrganizacional_Codigo = T002325_A611UnidadeOrganizacional_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2380( )
      {
         pr_default.close(23);
      }

      protected void AfterConfirm2380( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2380( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2380( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2380( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2380( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2380( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2380( )
      {
         dynTpUo_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTpUo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTpUo_Codigo.Enabled), 5, 0)));
         edtUnidadeOrganizacional_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacional_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeOrganizacional_Nome_Enabled), 5, 0)));
         dynEstado_UF.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynEstado_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynEstado_UF.Enabled), 5, 0)));
         dynUnidadeOrganizacional_Vinculada.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUnidadeOrganizacional_Vinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUnidadeOrganizacional_Vinculada.Enabled), 5, 0)));
         chkUnidadeOrganizacional_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeOrganizacional_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUnidadeOrganizacional_Ativo.Enabled), 5, 0)));
         edtUnidadeOrganizacional_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeOrganizacional_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeOrganizacional_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues230( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052812102553");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7UnidadeOrganizacional_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z611UnidadeOrganizacional_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z612UnidadeOrganizacional_Nome", StringUtil.RTrim( Z612UnidadeOrganizacional_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "Z629UnidadeOrganizacional_Ativo", Z629UnidadeOrganizacional_Ativo);
         GxWebStd.gx_hidden_field( context, "Z609TpUo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z609TpUo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z23Estado_UF", StringUtil.RTrim( Z23Estado_UF));
         GxWebStd.gx_hidden_field( context, "Z613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z613UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N609TpUo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N23Estado_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "N613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACINAL_ARVORE", StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore));
         GxWebStd.gx_hidden_field( context, "vUNIDADEORGANIZACIONAL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UnidadeOrganizacional_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TPUO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_TpUo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_ESTADO_UF", StringUtil.RTrim( AV12Insert_Estado_UF));
         GxWebStd.gx_hidden_field( context, "vINSERT_UNIDADEORGANIZACIONAL_VINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_UnidadeOrganizacional_Vinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TPUO_NOME", StringUtil.RTrim( A610TpUo_Nome));
         GxWebStd.gx_hidden_field( context, "UNIDADEORGANIZACIONAL_VINCULADANOM", StringUtil.RTrim( A1082UnidadeOrganizacional_VinculadaNom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUNIDADEORGANIZACIONAL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UnidadeOrganizacional_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Geral_UnidadeOrganizacional";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("geral_unidadeorganizacional:[SendSecurityCheck value for]"+"UnidadeOrganizacional_Codigo:"+context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("geral_unidadeorganizacional:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7UnidadeOrganizacional_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Geral_UnidadeOrganizacional" ;
      }

      public override String GetPgmdesc( )
      {
         return "Unidade Organizacional" ;
      }

      protected void InitializeNonKey2380( )
      {
         A609TpUo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
         A23Estado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         A613UnidadeOrganizacional_Vinculada = 0;
         n613UnidadeOrganizacional_Vinculada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
         n613UnidadeOrganizacional_Vinculada = ((0==A613UnidadeOrganizacional_Vinculada) ? true : false);
         A1174UnidadeOrganizacinal_Arvore = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         A612UnidadeOrganizacional_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A612UnidadeOrganizacional_Nome", A612UnidadeOrganizacional_Nome);
         A610TpUo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A610TpUo_Nome", A610TpUo_Nome);
         A1082UnidadeOrganizacional_VinculadaNom = "";
         n1082UnidadeOrganizacional_VinculadaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1082UnidadeOrganizacional_VinculadaNom", A1082UnidadeOrganizacional_VinculadaNom);
         A629UnidadeOrganizacional_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A629UnidadeOrganizacional_Ativo", A629UnidadeOrganizacional_Ativo);
         Z612UnidadeOrganizacional_Nome = "";
         Z629UnidadeOrganizacional_Ativo = false;
         Z609TpUo_Codigo = 0;
         Z23Estado_UF = "";
         Z613UnidadeOrganizacional_Vinculada = 0;
      }

      protected void InitAll2380( )
      {
         A611UnidadeOrganizacional_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A611UnidadeOrganizacional_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0)));
         InitializeNonKey2380( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A629UnidadeOrganizacional_Ativo = i629UnidadeOrganizacional_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A629UnidadeOrganizacional_Ativo", A629UnidadeOrganizacional_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052812102566");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("geral_unidadeorganizacional.js", "?202052812102566");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktpuo_codigo_Internalname = "TEXTBLOCKTPUO_CODIGO";
         dynTpUo_Codigo_Internalname = "TPUO_CODIGO";
         lblTextblockunidadeorganizacional_nome_Internalname = "TEXTBLOCKUNIDADEORGANIZACIONAL_NOME";
         edtUnidadeOrganizacional_Nome_Internalname = "UNIDADEORGANIZACIONAL_NOME";
         lblTextblockestado_uf_Internalname = "TEXTBLOCKESTADO_UF";
         dynEstado_UF_Internalname = "ESTADO_UF";
         lblTextblockunidadeorganizacional_vinculada_Internalname = "TEXTBLOCKUNIDADEORGANIZACIONAL_VINCULADA";
         dynUnidadeOrganizacional_Vinculada_Internalname = "UNIDADEORGANIZACIONAL_VINCULADA";
         lblTextblockunidadeorganizacional_ativo_Internalname = "TEXTBLOCKUNIDADEORGANIZACIONAL_ATIVO";
         chkUnidadeOrganizacional_Ativo_Internalname = "UNIDADEORGANIZACIONAL_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtUnidadeOrganizacional_Codigo_Internalname = "UNIDADEORGANIZACIONAL_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Unidade Organizacional";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Unidade Organizacional";
         chkUnidadeOrganizacional_Ativo.Enabled = 1;
         chkUnidadeOrganizacional_Ativo.Visible = 1;
         lblTextblockunidadeorganizacional_ativo_Visible = 1;
         dynUnidadeOrganizacional_Vinculada_Jsonclick = "";
         dynUnidadeOrganizacional_Vinculada.Enabled = 1;
         dynEstado_UF_Jsonclick = "";
         dynEstado_UF.Enabled = 1;
         edtUnidadeOrganizacional_Nome_Jsonclick = "";
         edtUnidadeOrganizacional_Nome_Enabled = 1;
         dynTpUo_Codigo_Jsonclick = "";
         dynTpUo_Codigo.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtUnidadeOrganizacional_Codigo_Jsonclick = "";
         edtUnidadeOrganizacional_Codigo_Enabled = 0;
         edtUnidadeOrganizacional_Codigo_Visible = 1;
         chkUnidadeOrganizacional_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATPUO_CODIGO231( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATPUO_CODIGO_data231( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATPUO_CODIGO_html231( )
      {
         int gxdynajaxvalue ;
         GXDLATPUO_CODIGO_data231( ) ;
         gxdynajaxindex = 1;
         dynTpUo_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTpUo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATPUO_CODIGO_data231( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002326 */
         pr_default.execute(24);
         while ( (pr_default.getStatus(24) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002326_A609TpUo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002326_A610TpUo_Nome[0]));
            pr_default.readNext(24);
         }
         pr_default.close(24);
      }

      protected void GXDLAESTADO_UF2380( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAESTADO_UF_data2380( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAESTADO_UF_html2380( )
      {
         String gxdynajaxvalue ;
         GXDLAESTADO_UF_data2380( ) ;
         gxdynajaxindex = 1;
         dynEstado_UF.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynEstado_UF.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAESTADO_UF_data2380( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002327 */
         pr_default.execute(25);
         while ( (pr_default.getStatus(25) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T002327_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002327_A24Estado_Nome[0]));
            pr_default.readNext(25);
         }
         pr_default.close(25);
      }

      protected void GXDLAUNIDADEORGANIZACIONAL_VINCULADA2380( int AV7UnidadeOrganizacional_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAUNIDADEORGANIZACIONAL_VINCULADA_data2380( AV7UnidadeOrganizacional_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAUNIDADEORGANIZACIONAL_VINCULADA_html2380( int AV7UnidadeOrganizacional_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLAUNIDADEORGANIZACIONAL_VINCULADA_data2380( AV7UnidadeOrganizacional_Codigo) ;
         gxdynajaxindex = 1;
         dynUnidadeOrganizacional_Vinculada.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynUnidadeOrganizacional_Vinculada.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAUNIDADEORGANIZACIONAL_VINCULADA_data2380( int AV7UnidadeOrganizacional_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002328 */
         pr_default.execute(26, new Object[] {AV7UnidadeOrganizacional_Codigo});
         while ( (pr_default.getStatus(26) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002328_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002328_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(26);
         }
         pr_default.close(26);
      }

      protected void GX3ASAUNIDADEORGANIZACINAL_ARVORE2380( int A613UnidadeOrganizacional_Vinculada )
      {
         if ( ! T00233_n613UnidadeOrganizacional_Vinculada[0] )
         {
            GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
            new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A613UnidadeOrganizacional_Vinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)));
            A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         }
         else
         {
            A1174UnidadeOrganizacinal_Arvore = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1174UnidadeOrganizacinal_Arvore", A1174UnidadeOrganizacinal_Arvore);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Tpuo_codigo( GXCombobox dynGX_Parm1 ,
                                     String GX_Parm2 )
      {
         dynTpUo_Codigo = dynGX_Parm1;
         A609TpUo_Codigo = (int)(NumberUtil.Val( dynTpUo_Codigo.CurrentValue, "."));
         A610TpUo_Nome = GX_Parm2;
         /* Using cursor T002329 */
         pr_default.execute(27, new Object[] {A609TpUo_Codigo});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tipo Unidade Organizacional'.", "ForeignKeyNotFound", 1, "TPUO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTpUo_Codigo_Internalname;
         }
         A610TpUo_Nome = T002329_A610TpUo_Nome[0];
         pr_default.close(27);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A610TpUo_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A610TpUo_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Estado_uf( GXCombobox dynGX_Parm1 )
      {
         dynEstado_UF = dynGX_Parm1;
         A23Estado_UF = dynEstado_UF.CurrentValue;
         /* Using cursor T002330 */
         pr_default.execute(28, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "ESTADO_UF");
            AnyError = 1;
            GX_FocusControl = dynEstado_UF_Internalname;
         }
         pr_default.close(28);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Unidadeorganizacional_vinculada( GXCombobox dynGX_Parm1 ,
                                                         String GX_Parm2 ,
                                                         String GX_Parm3 )
      {
         dynUnidadeOrganizacional_Vinculada = dynGX_Parm1;
         A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( dynUnidadeOrganizacional_Vinculada.CurrentValue, "."));
         n613UnidadeOrganizacional_Vinculada = false;
         A1082UnidadeOrganizacional_VinculadaNom = GX_Parm2;
         n1082UnidadeOrganizacional_VinculadaNom = false;
         A1174UnidadeOrganizacinal_Arvore = GX_Parm3;
         /* Using cursor T002331 */
         pr_default.execute(29, new Object[] {n613UnidadeOrganizacional_Vinculada, A613UnidadeOrganizacional_Vinculada});
         if ( (pr_default.getStatus(29) == 101) )
         {
            if ( ! ( (0==A613UnidadeOrganizacional_Vinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Unidade Organizacional_Unidade Vinculada'.", "ForeignKeyNotFound", 1, "UNIDADEORGANIZACIONAL_VINCULADA");
               AnyError = 1;
               GX_FocusControl = dynUnidadeOrganizacional_Vinculada_Internalname;
            }
         }
         A1082UnidadeOrganizacional_VinculadaNom = T002331_A1082UnidadeOrganizacional_VinculadaNom[0];
         n1082UnidadeOrganizacional_VinculadaNom = T002331_n1082UnidadeOrganizacional_VinculadaNom[0];
         pr_default.close(29);
         if ( ! T00233_n613UnidadeOrganizacional_Vinculada[0] )
         {
            GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
            new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
            A1174UnidadeOrganizacinal_Arvore = GXt_char1;
         }
         else
         {
            A1174UnidadeOrganizacinal_Arvore = "";
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1082UnidadeOrganizacional_VinculadaNom = "";
            n1082UnidadeOrganizacional_VinculadaNom = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A1082UnidadeOrganizacional_VinculadaNom));
         isValidOutput.Add(StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7UnidadeOrganizacional_Codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12232',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(27);
         pr_default.close(16);
         pr_default.close(28);
         pr_default.close(29);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z612UnidadeOrganizacional_Nome = "";
         Z23Estado_UF = "";
         N23Estado_UF = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A23Estado_UF = "";
         GXKey = "";
         T00237_A609TpUo_Codigo = new int[1] ;
         T00237_A610TpUo_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktpuo_codigo_Jsonclick = "";
         lblTextblockunidadeorganizacional_nome_Jsonclick = "";
         A612UnidadeOrganizacional_Nome = "";
         lblTextblockestado_uf_Jsonclick = "";
         lblTextblockunidadeorganizacional_vinculada_Jsonclick = "";
         lblTextblockunidadeorganizacional_ativo_Jsonclick = "";
         A1174UnidadeOrganizacinal_Arvore = "";
         AV12Insert_Estado_UF = "";
         A610TpUo_Nome = "";
         A1082UnidadeOrganizacional_VinculadaNom = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode80 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z610TpUo_Nome = "";
         Z1082UnidadeOrganizacional_VinculadaNom = "";
         T00236_A1082UnidadeOrganizacional_VinculadaNom = new String[] {""} ;
         T00236_n1082UnidadeOrganizacional_VinculadaNom = new bool[] {false} ;
         T00233_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         T00234_A610TpUo_Nome = new String[] {""} ;
         T00238_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T00238_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T00238_A610TpUo_Nome = new String[] {""} ;
         T00238_A1082UnidadeOrganizacional_VinculadaNom = new String[] {""} ;
         T00238_n1082UnidadeOrganizacional_VinculadaNom = new bool[] {false} ;
         T00238_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         T00238_A609TpUo_Codigo = new int[1] ;
         T00238_A23Estado_UF = new String[] {""} ;
         T00238_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         T00238_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         T00235_A23Estado_UF = new String[] {""} ;
         T00239_A610TpUo_Nome = new String[] {""} ;
         T002310_A23Estado_UF = new String[] {""} ;
         T002311_A1082UnidadeOrganizacional_VinculadaNom = new String[] {""} ;
         T002311_n1082UnidadeOrganizacional_VinculadaNom = new bool[] {false} ;
         T002312_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T00233_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T00233_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T00233_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         T00233_A609TpUo_Codigo = new int[1] ;
         T00233_A23Estado_UF = new String[] {""} ;
         T00233_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         T002313_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T002314_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T00232_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T00232_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T00232_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         T00232_A609TpUo_Codigo = new int[1] ;
         T00232_A23Estado_UF = new String[] {""} ;
         T00232_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         T00232_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         T002315_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T002318_A610TpUo_Nome = new String[] {""} ;
         T002319_A1082UnidadeOrganizacional_VinculadaNom = new String[] {""} ;
         T002319_n1082UnidadeOrganizacional_VinculadaNom = new bool[] {false} ;
         T002320_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         T002320_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         T002321_A192Contagem_Codigo = new int[1] ;
         T002322_A617Cargo_Codigo = new int[1] ;
         T002323_A621Funcao_Codigo = new int[1] ;
         T002324_A155Servico_Codigo = new int[1] ;
         T002325_A611UnidadeOrganizacional_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002326_A609TpUo_Codigo = new int[1] ;
         T002326_A610TpUo_Nome = new String[] {""} ;
         T002327_A23Estado_UF = new String[] {""} ;
         T002327_A24Estado_Nome = new String[] {""} ;
         T002328_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T002328_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T002329_A610TpUo_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002330_A23Estado_UF = new String[] {""} ;
         T002331_A1082UnidadeOrganizacional_VinculadaNom = new String[] {""} ;
         T002331_n1082UnidadeOrganizacional_VinculadaNom = new bool[] {false} ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_unidadeorganizacional__default(),
            new Object[][] {
                new Object[] {
               T00232_A611UnidadeOrganizacional_Codigo, T00232_A612UnidadeOrganizacional_Nome, T00232_A629UnidadeOrganizacional_Ativo, T00232_A609TpUo_Codigo, T00232_A23Estado_UF, T00232_A613UnidadeOrganizacional_Vinculada, T00232_n613UnidadeOrganizacional_Vinculada
               }
               , new Object[] {
               T00233_A611UnidadeOrganizacional_Codigo, T00233_A612UnidadeOrganizacional_Nome, T00233_A629UnidadeOrganizacional_Ativo, T00233_A609TpUo_Codigo, T00233_A23Estado_UF, T00233_A613UnidadeOrganizacional_Vinculada, T00233_n613UnidadeOrganizacional_Vinculada
               }
               , new Object[] {
               T00234_A610TpUo_Nome
               }
               , new Object[] {
               T00235_A23Estado_UF
               }
               , new Object[] {
               T00236_A1082UnidadeOrganizacional_VinculadaNom, T00236_n1082UnidadeOrganizacional_VinculadaNom
               }
               , new Object[] {
               T00237_A609TpUo_Codigo, T00237_A610TpUo_Nome
               }
               , new Object[] {
               T00238_A611UnidadeOrganizacional_Codigo, T00238_A612UnidadeOrganizacional_Nome, T00238_A610TpUo_Nome, T00238_A1082UnidadeOrganizacional_VinculadaNom, T00238_n1082UnidadeOrganizacional_VinculadaNom, T00238_A629UnidadeOrganizacional_Ativo, T00238_A609TpUo_Codigo, T00238_A23Estado_UF, T00238_A613UnidadeOrganizacional_Vinculada, T00238_n613UnidadeOrganizacional_Vinculada
               }
               , new Object[] {
               T00239_A610TpUo_Nome
               }
               , new Object[] {
               T002310_A23Estado_UF
               }
               , new Object[] {
               T002311_A1082UnidadeOrganizacional_VinculadaNom, T002311_n1082UnidadeOrganizacional_VinculadaNom
               }
               , new Object[] {
               T002312_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               T002313_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               T002314_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               T002315_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002318_A610TpUo_Nome
               }
               , new Object[] {
               T002319_A1082UnidadeOrganizacional_VinculadaNom, T002319_n1082UnidadeOrganizacional_VinculadaNom
               }
               , new Object[] {
               T002320_A613UnidadeOrganizacional_Vinculada
               }
               , new Object[] {
               T002321_A192Contagem_Codigo
               }
               , new Object[] {
               T002322_A617Cargo_Codigo
               }
               , new Object[] {
               T002323_A621Funcao_Codigo
               }
               , new Object[] {
               T002324_A155Servico_Codigo
               }
               , new Object[] {
               T002325_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               T002326_A609TpUo_Codigo, T002326_A610TpUo_Nome
               }
               , new Object[] {
               T002327_A23Estado_UF, T002327_A24Estado_Nome
               }
               , new Object[] {
               T002328_A611UnidadeOrganizacional_Codigo, T002328_A612UnidadeOrganizacional_Nome
               }
               , new Object[] {
               T002329_A610TpUo_Nome
               }
               , new Object[] {
               T002330_A23Estado_UF
               }
               , new Object[] {
               T002331_A1082UnidadeOrganizacional_VinculadaNom, T002331_n1082UnidadeOrganizacional_VinculadaNom
               }
            }
         );
         Z629UnidadeOrganizacional_Ativo = true;
         A629UnidadeOrganizacional_Ativo = true;
         i629UnidadeOrganizacional_Ativo = true;
         AV15Pgmname = "Geral_UnidadeOrganizacional";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound80 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7UnidadeOrganizacional_Codigo ;
      private int Z611UnidadeOrganizacional_Codigo ;
      private int Z609TpUo_Codigo ;
      private int Z613UnidadeOrganizacional_Vinculada ;
      private int N609TpUo_Codigo ;
      private int N613UnidadeOrganizacional_Vinculada ;
      private int AV7UnidadeOrganizacional_Codigo ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int A609TpUo_Codigo ;
      private int trnEnded ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int edtUnidadeOrganizacional_Codigo_Enabled ;
      private int edtUnidadeOrganizacional_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtUnidadeOrganizacional_Nome_Enabled ;
      private int lblTextblockunidadeorganizacional_ativo_Visible ;
      private int AV11Insert_TpUo_Codigo ;
      private int AV13Insert_UnidadeOrganizacional_Vinculada ;
      private int AV16GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z612UnidadeOrganizacional_Nome ;
      private String Z23Estado_UF ;
      private String N23Estado_UF ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A23Estado_UF ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkUnidadeOrganizacional_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynTpUo_Codigo_Internalname ;
      private String edtUnidadeOrganizacional_Codigo_Internalname ;
      private String edtUnidadeOrganizacional_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktpuo_codigo_Internalname ;
      private String lblTextblocktpuo_codigo_Jsonclick ;
      private String dynTpUo_Codigo_Jsonclick ;
      private String lblTextblockunidadeorganizacional_nome_Internalname ;
      private String lblTextblockunidadeorganizacional_nome_Jsonclick ;
      private String edtUnidadeOrganizacional_Nome_Internalname ;
      private String A612UnidadeOrganizacional_Nome ;
      private String edtUnidadeOrganizacional_Nome_Jsonclick ;
      private String lblTextblockestado_uf_Internalname ;
      private String lblTextblockestado_uf_Jsonclick ;
      private String dynEstado_UF_Internalname ;
      private String dynEstado_UF_Jsonclick ;
      private String lblTextblockunidadeorganizacional_vinculada_Internalname ;
      private String lblTextblockunidadeorganizacional_vinculada_Jsonclick ;
      private String dynUnidadeOrganizacional_Vinculada_Internalname ;
      private String dynUnidadeOrganizacional_Vinculada_Jsonclick ;
      private String lblTextblockunidadeorganizacional_ativo_Internalname ;
      private String lblTextblockunidadeorganizacional_ativo_Jsonclick ;
      private String A1174UnidadeOrganizacinal_Arvore ;
      private String AV12Insert_Estado_UF ;
      private String A610TpUo_Nome ;
      private String A1082UnidadeOrganizacional_VinculadaNom ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode80 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z610TpUo_Nome ;
      private String Z1082UnidadeOrganizacional_VinculadaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private String GXt_char1 ;
      private bool Z629UnidadeOrganizacional_Ativo ;
      private bool entryPointCalled ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool n1082UnidadeOrganizacional_VinculadaNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i629UnidadeOrganizacional_Ativo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T00237_A609TpUo_Codigo ;
      private String[] T00237_A610TpUo_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynTpUo_Codigo ;
      private GXCombobox dynEstado_UF ;
      private GXCombobox dynUnidadeOrganizacional_Vinculada ;
      private GXCheckbox chkUnidadeOrganizacional_Ativo ;
      private String[] T00236_A1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T00236_n1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T00233_n613UnidadeOrganizacional_Vinculada ;
      private String[] T00234_A610TpUo_Nome ;
      private int[] T00238_A611UnidadeOrganizacional_Codigo ;
      private String[] T00238_A612UnidadeOrganizacional_Nome ;
      private String[] T00238_A610TpUo_Nome ;
      private String[] T00238_A1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T00238_n1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T00238_A629UnidadeOrganizacional_Ativo ;
      private int[] T00238_A609TpUo_Codigo ;
      private String[] T00238_A23Estado_UF ;
      private int[] T00238_A613UnidadeOrganizacional_Vinculada ;
      private bool[] T00238_n613UnidadeOrganizacional_Vinculada ;
      private String[] T00235_A23Estado_UF ;
      private String[] T00239_A610TpUo_Nome ;
      private String[] T002310_A23Estado_UF ;
      private String[] T002311_A1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T002311_n1082UnidadeOrganizacional_VinculadaNom ;
      private int[] T002312_A611UnidadeOrganizacional_Codigo ;
      private int[] T00233_A611UnidadeOrganizacional_Codigo ;
      private String[] T00233_A612UnidadeOrganizacional_Nome ;
      private bool[] T00233_A629UnidadeOrganizacional_Ativo ;
      private int[] T00233_A609TpUo_Codigo ;
      private String[] T00233_A23Estado_UF ;
      private int[] T00233_A613UnidadeOrganizacional_Vinculada ;
      private int[] T002313_A611UnidadeOrganizacional_Codigo ;
      private int[] T002314_A611UnidadeOrganizacional_Codigo ;
      private int[] T00232_A611UnidadeOrganizacional_Codigo ;
      private String[] T00232_A612UnidadeOrganizacional_Nome ;
      private bool[] T00232_A629UnidadeOrganizacional_Ativo ;
      private int[] T00232_A609TpUo_Codigo ;
      private String[] T00232_A23Estado_UF ;
      private int[] T00232_A613UnidadeOrganizacional_Vinculada ;
      private bool[] T00232_n613UnidadeOrganizacional_Vinculada ;
      private int[] T002315_A611UnidadeOrganizacional_Codigo ;
      private String[] T002318_A610TpUo_Nome ;
      private String[] T002319_A1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T002319_n1082UnidadeOrganizacional_VinculadaNom ;
      private int[] T002320_A613UnidadeOrganizacional_Vinculada ;
      private bool[] T002320_n613UnidadeOrganizacional_Vinculada ;
      private int[] T002321_A192Contagem_Codigo ;
      private int[] T002322_A617Cargo_Codigo ;
      private int[] T002323_A621Funcao_Codigo ;
      private int[] T002324_A155Servico_Codigo ;
      private int[] T002325_A611UnidadeOrganizacional_Codigo ;
      private int[] T002326_A609TpUo_Codigo ;
      private String[] T002326_A610TpUo_Nome ;
      private String[] T002327_A23Estado_UF ;
      private String[] T002327_A24Estado_Nome ;
      private int[] T002328_A611UnidadeOrganizacional_Codigo ;
      private String[] T002328_A612UnidadeOrganizacional_Nome ;
      private String[] T002329_A610TpUo_Nome ;
      private String[] T002330_A23Estado_UF ;
      private String[] T002331_A1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] T002331_n1082UnidadeOrganizacional_VinculadaNom ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class geral_unidadeorganizacional__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00237 ;
          prmT00237 = new Object[] {
          } ;
          Object[] prmT00238 ;
          prmT00238 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00234 ;
          prmT00234 = new Object[] {
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00235 ;
          prmT00235 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT00236 ;
          prmT00236 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00239 ;
          prmT00239 = new Object[] {
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002310 ;
          prmT002310 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT002311 ;
          prmT002311 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002312 ;
          prmT002312 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00233 ;
          prmT00233 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002313 ;
          prmT002313 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002314 ;
          prmT002314 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00232 ;
          prmT00232 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002315 ;
          prmT002315 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@UnidadeOrganizacional_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002316 ;
          prmT002316 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@UnidadeOrganizacional_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002317 ;
          prmT002317 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002318 ;
          prmT002318 = new Object[] {
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002319 ;
          prmT002319 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002320 ;
          prmT002320 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002321 ;
          prmT002321 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002322 ;
          prmT002322 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002323 ;
          prmT002323 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002324 ;
          prmT002324 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002325 ;
          prmT002325 = new Object[] {
          } ;
          Object[] prmT002326 ;
          prmT002326 = new Object[] {
          } ;
          Object[] prmT002327 ;
          prmT002327 = new Object[] {
          } ;
          Object[] prmT002328 ;
          prmT002328 = new Object[] {
          new Object[] {"@AV7UnidadeOrganizacional_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002329 ;
          prmT002329 = new Object[] {
          new Object[] {"@TpUo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002330 ;
          prmT002330 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT002331 ;
          prmT002331 = new Object[] {
          new Object[] {"@UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00232", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo], [TpUo_Codigo], [Estado_UF], [UnidadeOrganizacional_Vinculada] AS UnidadeOrganizacional_Vinculada FROM [Geral_UnidadeOrganizacional] WITH (UPDLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00232,1,0,true,false )
             ,new CursorDef("T00233", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo], [TpUo_Codigo], [Estado_UF], [UnidadeOrganizacional_Vinculada] AS UnidadeOrganizacional_Vinculada FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00233,1,0,true,false )
             ,new CursorDef("T00234", "SELECT [TpUo_Nome] FROM [Geral_tp_uo] WITH (NOLOCK) WHERE [TpUo_Codigo] = @TpUo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00234,1,0,true,false )
             ,new CursorDef("T00235", "SELECT [Estado_UF] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT00235,1,0,true,false )
             ,new CursorDef("T00236", "SELECT [UnidadeOrganizacional_Nome] AS UnidadeOrganizacional_VinculadaNom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Vinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT00236,1,0,true,false )
             ,new CursorDef("T00237", "SELECT [TpUo_Codigo], [TpUo_Nome] FROM [Geral_tp_uo] WITH (NOLOCK) ORDER BY [TpUo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT00237,0,0,true,false )
             ,new CursorDef("T00238", "SELECT TM1.[UnidadeOrganizacional_Codigo], TM1.[UnidadeOrganizacional_Nome], T2.[TpUo_Nome], T3.[UnidadeOrganizacional_Nome] AS UnidadeOrganizacional_VinculadaNom, TM1.[UnidadeOrganizacional_Ativo], TM1.[TpUo_Codigo], TM1.[Estado_UF], TM1.[UnidadeOrganizacional_Vinculada] AS UnidadeOrganizacional_Vinculada FROM (([Geral_UnidadeOrganizacional] TM1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = TM1.[TpUo_Codigo]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = TM1.[UnidadeOrganizacional_Vinculada]) WHERE TM1.[UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Codigo ORDER BY TM1.[UnidadeOrganizacional_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00238,100,0,true,false )
             ,new CursorDef("T00239", "SELECT [TpUo_Nome] FROM [Geral_tp_uo] WITH (NOLOCK) WHERE [TpUo_Codigo] = @TpUo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00239,1,0,true,false )
             ,new CursorDef("T002310", "SELECT [Estado_UF] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT002310,1,0,true,false )
             ,new CursorDef("T002311", "SELECT [UnidadeOrganizacional_Nome] AS UnidadeOrganizacional_VinculadaNom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Vinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT002311,1,0,true,false )
             ,new CursorDef("T002312", "SELECT [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002312,1,0,true,false )
             ,new CursorDef("T002313", "SELECT TOP 1 [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ( [UnidadeOrganizacional_Codigo] > @UnidadeOrganizacional_Codigo) ORDER BY [UnidadeOrganizacional_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002313,1,0,true,true )
             ,new CursorDef("T002314", "SELECT TOP 1 [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ( [UnidadeOrganizacional_Codigo] < @UnidadeOrganizacional_Codigo) ORDER BY [UnidadeOrganizacional_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002314,1,0,true,true )
             ,new CursorDef("T002315", "INSERT INTO [Geral_UnidadeOrganizacional]([UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo], [TpUo_Codigo], [Estado_UF], [UnidadeOrganizacional_Vinculada]) VALUES(@UnidadeOrganizacional_Nome, @UnidadeOrganizacional_Ativo, @TpUo_Codigo, @Estado_UF, @UnidadeOrganizacional_Vinculada); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002315)
             ,new CursorDef("T002316", "UPDATE [Geral_UnidadeOrganizacional] SET [UnidadeOrganizacional_Nome]=@UnidadeOrganizacional_Nome, [UnidadeOrganizacional_Ativo]=@UnidadeOrganizacional_Ativo, [TpUo_Codigo]=@TpUo_Codigo, [Estado_UF]=@Estado_UF, [UnidadeOrganizacional_Vinculada]=@UnidadeOrganizacional_Vinculada  WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Codigo", GxErrorMask.GX_NOMASK,prmT002316)
             ,new CursorDef("T002317", "DELETE FROM [Geral_UnidadeOrganizacional]  WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Codigo", GxErrorMask.GX_NOMASK,prmT002317)
             ,new CursorDef("T002318", "SELECT [TpUo_Nome] FROM [Geral_tp_uo] WITH (NOLOCK) WHERE [TpUo_Codigo] = @TpUo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002318,1,0,true,false )
             ,new CursorDef("T002319", "SELECT [UnidadeOrganizacional_Nome] AS UnidadeOrganizacional_VinculadaNom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Vinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT002319,1,0,true,false )
             ,new CursorDef("T002320", "SELECT TOP 1 [UnidadeOrganizacional_Codigo] AS UnidadeOrganizacional_Vinculada FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Vinculada] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002320,1,0,true,true )
             ,new CursorDef("T002321", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_UnidadeOrganizacional] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002321,1,0,true,true )
             ,new CursorDef("T002322", "SELECT TOP 1 [Cargo_Codigo] FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_UOCod] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002322,1,0,true,true )
             ,new CursorDef("T002323", "SELECT TOP 1 [Funcao_Codigo] FROM [Geral_Funcao] WITH (NOLOCK) WHERE [Funcao_UOCod] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002323,1,0,true,true )
             ,new CursorDef("T002324", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_UO] = @UnidadeOrganizacional_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002324,1,0,true,true )
             ,new CursorDef("T002325", "SELECT [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) ORDER BY [UnidadeOrganizacional_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002325,100,0,true,false )
             ,new CursorDef("T002326", "SELECT [TpUo_Codigo], [TpUo_Nome] FROM [Geral_tp_uo] WITH (NOLOCK) ORDER BY [TpUo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002326,0,0,true,false )
             ,new CursorDef("T002327", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002327,0,0,true,false )
             ,new CursorDef("T002328", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ([UnidadeOrganizacional_Codigo] <> @AV7UnidadeOrganizacional_Codigo) AND ([UnidadeOrganizacional_Ativo] = 1) ORDER BY [UnidadeOrganizacional_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002328,0,0,true,false )
             ,new CursorDef("T002329", "SELECT [TpUo_Nome] FROM [Geral_tp_uo] WITH (NOLOCK) WHERE [TpUo_Codigo] = @TpUo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002329,1,0,true,false )
             ,new CursorDef("T002330", "SELECT [Estado_UF] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT002330,1,0,true,false )
             ,new CursorDef("T002331", "SELECT [UnidadeOrganizacional_Nome] AS UnidadeOrganizacional_VinculadaNom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @UnidadeOrganizacional_Vinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT002331,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 2) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                stmt.SetParameter(6, (int)parms[6]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
