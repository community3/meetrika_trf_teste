/*
               File: WP_Lote_Nome
        Description: Nome do Lote de Aceite
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:20:16.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_lote_nome : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_lote_nome( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_lote_nome( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Codigo ,
                           int aP2_Lote_UserCod ,
                           short aP3_OrderedBy ,
                           bool aP4_OrderedDsc ,
                           String aP5_GridStateXML ,
                           short aP6_QtdDmn ,
                           decimal aP7_ValorTotal )
      {
         this.AV13Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV24Codigo = aP1_Codigo;
         this.AV10Lote_UserCod = aP2_Lote_UserCod;
         this.AV16OrderedBy = aP3_OrderedBy;
         this.AV17OrderedDsc = aP4_OrderedDsc;
         this.AV18GridStateXML = aP5_GridStateXML;
         this.AV22QtdDmn = aP6_QtdDmn;
         this.AV23ValorTotal = aP7_ValorTotal;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV24Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Codigo), "ZZZZZ9")));
                  AV10Lote_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Lote_UserCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_USERCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Lote_UserCod), "ZZZZZ9")));
                  AV16OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16OrderedBy), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORDEREDBY", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16OrderedBy), "ZZZ9")));
                  AV17OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17OrderedDsc", AV17OrderedDsc);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORDEREDDSC", GetSecureSignedToken( "", AV17OrderedDsc));
                  AV18GridStateXML = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GridStateXML", AV18GridStateXML);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDSTATEXML", GetSecureSignedToken( "", AV18GridStateXML));
                  AV22QtdDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22QtdDmn), 4, 0)));
                  AV23ValorTotal = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ValorTotal", StringUtil.LTrim( StringUtil.Str( AV23ValorTotal, 18, 5)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABT2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBT2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216201614");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_lote_nome.aspx") + "?" + UrlEncode("" +AV13Contratada_AreaTrabalhoCod) + "," + UrlEncode("" +AV24Codigo) + "," + UrlEncode("" +AV10Lote_UserCod) + "," + UrlEncode("" +AV16OrderedBy) + "," + UrlEncode(StringUtil.BoolToStr(AV17OrderedDsc)) + "," + UrlEncode(StringUtil.RTrim(AV18GridStateXML)) + "," + UrlEncode("" +AV22QtdDmn) + "," + UrlEncode(StringUtil.Str(AV23ValorTotal,18,5))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDSTATEXML", AV18GridStateXML);
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLOTE_USERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Lote_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vORDEREDDSC", AV17OrderedDsc);
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Lote_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV7Lote_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11Usuario, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_USERCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Lote_UserCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORDEREDBY", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16OrderedBy), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORDEREDDSC", GetSecureSignedToken( "", AV17OrderedDsc));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRIDSTATEXML", GetSecureSignedToken( "", AV18GridStateXML));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_USERCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Lote_UserCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORDEREDBY", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16OrderedBy), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORDEREDDSC", GetSecureSignedToken( "", AV17OrderedDsc));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRIDSTATEXML", GetSecureSignedToken( "", AV18GridStateXML));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcloteanexos == null ) )
         {
            WebComp_Wcloteanexos.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBT2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBT2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_lote_nome.aspx") + "?" + UrlEncode("" +AV13Contratada_AreaTrabalhoCod) + "," + UrlEncode("" +AV24Codigo) + "," + UrlEncode("" +AV10Lote_UserCod) + "," + UrlEncode("" +AV16OrderedBy) + "," + UrlEncode(StringUtil.BoolToStr(AV17OrderedDsc)) + "," + UrlEncode(StringUtil.RTrim(AV18GridStateXML)) + "," + UrlEncode("" +AV22QtdDmn) + "," + UrlEncode(StringUtil.Str(AV23ValorTotal,18,5)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Lote_Nome" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nome do Lote de Aceite" ;
      }

      protected void WBBT0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_BT2( true) ;
         }
         else
         {
            wb_table1_2_BT2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BT2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTBT2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Nome do Lote de Aceite", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBT0( ) ;
      }

      protected void WSBT2( )
      {
         STARTBT2( ) ;
         EVTBT2( ) ;
      }

      protected void EVTBT2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11BT2 */
                              E11BT2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12BT2 */
                                    E12BT2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CANCELAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13BT2 */
                              E13BT2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14BT2 */
                              E14BT2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 41 )
                        {
                           WebComp_Wcloteanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
                           WebComp_Wcloteanexos.ComponentInit();
                           WebComp_Wcloteanexos.Name = "WC_ContagemResultadoEvidencias";
                           WebComp_Wcloteanexos_Component = "WC_ContagemResultadoEvidencias";
                           WebComp_Wcloteanexos.componentprocess("W0041", "", sEvt);
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBT2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABT2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavLote_numero_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBT2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavLote_numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero_Enabled), 5, 0)));
         edtavLote_data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_data_Enabled), 5, 0)));
         edtavUsuario_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_Enabled), 5, 0)));
      }

      protected void RFBT2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Wcloteanexos_Component, "") == 0 )
            {
               WebComp_Wcloteanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
               WebComp_Wcloteanexos.ComponentInit();
               WebComp_Wcloteanexos.Name = "WC_ContagemResultadoEvidencias";
               WebComp_Wcloteanexos_Component = "WC_ContagemResultadoEvidencias";
            }
            if ( ( StringUtil.Len( WebComp_Wcloteanexos_Component) != 0 ) && ( StringUtil.StrCmp(WebComp_Wcloteanexos_Component, "WC_ContagemResultadoEvidencias") == 0 ) )
            {
               WebComp_Wcloteanexos.setjustcreated();
               WebComp_Wcloteanexos.componentprepare(new Object[] {(String)"W0041",(String)""});
               WebComp_Wcloteanexos.componentbind(new Object[] {});
            }
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0041"+"");
               WebComp_Wcloteanexos.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcloteanexos_Component) != 0 )
               {
                  WebComp_Wcloteanexos.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00BT2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A483ContagemResultado_StatusCnt = H00BT2_A483ContagemResultado_StatusCnt[0];
               /* Execute user event: E14BT2 */
               E14BT2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBBT0( ) ;
         }
      }

      protected void STRUPBT0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavLote_numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero_Enabled), 5, 0)));
         edtavLote_data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_data_Enabled), 5, 0)));
         edtavUsuario_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11BT2 */
         E11BT2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV6Lote_Numero = cgiGet( edtavLote_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Lote_Numero", AV6Lote_Numero);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Lote_Numero, ""))));
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "vLOTE_DATA");
               GX_FocusControl = edtavLote_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7Lote_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Lote_Data", context.localUtil.TToC( AV7Lote_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV7Lote_Data, "99/99/99 99:99")));
            }
            else
            {
               AV7Lote_Data = context.localUtil.CToT( cgiGet( edtavLote_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Lote_Data", context.localUtil.TToC( AV7Lote_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV7Lote_Data, "99/99/99 99:99")));
            }
            AV11Usuario = StringUtil.Upper( cgiGet( edtavUsuario_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Usuario", AV11Usuario);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11Usuario, "@!"))));
            AV8Nome = StringUtil.Upper( cgiGet( edtavNome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Nome", AV8Nome);
            AV25ParecerFinal = cgiGet( edtavParecerfinal_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ParecerFinal", AV25ParecerFinal);
            AV26Comentarios = cgiGet( edtavComentarios_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Comentarios", AV26Comentarios);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11BT2 */
         E11BT2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11BT2( )
      {
         /* Start Routine */
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcloteanexos_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wcloteanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcloteanexos.ComponentInit();
            WebComp_Wcloteanexos.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcloteanexos_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wcloteanexos_Component) != 0 )
         {
            WebComp_Wcloteanexos.setjustcreated();
            WebComp_Wcloteanexos.componentprepare(new Object[] {(String)"W0041",(String)""});
            WebComp_Wcloteanexos.componentbind(new Object[] {});
         }
         /* Using cursor H00BT3 */
         pr_default.execute(1, new Object[] {AV24Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00BT3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00BT3_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = H00BT3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00BT3_n1603ContagemResultado_CntCod[0];
            A39Contratada_Codigo = H00BT3_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00BT3_n39Contratada_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00BT3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00BT3_n490ContagemResultado_ContratadaCod[0];
            A530Contratada_Lote = H00BT3_A530Contratada_Lote[0];
            n530Contratada_Lote = H00BT3_n530Contratada_Lote[0];
            A456ContagemResultado_Codigo = H00BT3_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = H00BT3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00BT3_n1603ContagemResultado_CntCod[0];
            A39Contratada_Codigo = H00BT3_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00BT3_n39Contratada_Codigo[0];
            A530Contratada_Lote = H00BT3_A530Contratada_Lote[0];
            n530Contratada_Lote = H00BT3_n530Contratada_Lote[0];
            /* Using cursor H00BT4 */
            pr_default.execute(2, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A39Contratada_Codigo = H00BT4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00BT4_n39Contratada_Codigo[0];
               AV12Contratada_Lote = (short)(A530Contratada_Lote+1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         AV7Lote_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Lote_Data", context.localUtil.TToC( AV7Lote_Data, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV7Lote_Data, "99/99/99 99:99")));
         AV6Lote_Numero = StringUtil.Trim( StringUtil.Str( (decimal)(AV12Contratada_Lote), 4, 0)) + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV7Lote_Data)), 10, 0)), 2, "0") + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV7Lote_Data)), 10, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Lote_Numero", AV6Lote_Numero);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Lote_Numero, ""))));
         /* Using cursor H00BT5 */
         pr_default.execute(3, new Object[] {AV10Lote_UserCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A57Usuario_PessoaCod = H00BT5_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00BT5_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00BT5_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00BT5_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H00BT5_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00BT5_n58Usuario_PessoaNom[0];
            AV11Usuario = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Usuario", AV11Usuario);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11Usuario, "@!"))));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12BT2 */
         E12BT2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12BT2( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8Nome)) )
         {
            GX_msglist.addItem("O nome do lote � obrigat�rio!");
            GX_FocusControl = edtavNome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            AV19WebSession.Set("ParecerFinal", AV25ParecerFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ParecerFinal", AV25ParecerFinal);
            AV19WebSession.Set("Comentarios", AV26Comentarios);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Comentarios", AV26Comentarios);
            new prc_agruparparafaturamento(context ).execute(  AV13Contratada_AreaTrabalhoCod,  0,  AV18GridStateXML,  "O",  AV8Nome) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GridStateXML", AV18GridStateXML);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDSTATEXML", GetSecureSignedToken( "", AV18GridStateXML));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Nome", AV8Nome);
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13BT2( )
      {
         /* 'Cancelar' Routine */
         AV5Lote_Nome = "";
         AV19WebSession.Remove("ArquivosEvd");
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E14BT2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_BT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(400), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(800), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "N�mero:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero_Internalname, StringUtil.RTrim( AV6Lote_Numero), StringUtil.RTrim( context.localUtil.Format( AV6Lote_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,9);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_numero_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Data:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_data_Internalname, context.localUtil.TToC( AV7Lote_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV7Lote_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_data_Jsonclick, 0, "Attribute", "", "", "", 1, edtavLote_data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Lote_Nome.htm");
            GxWebStd.gx_bitmap( context, edtavLote_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavLote_data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavQtddmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22QtdDmn), 4, 0, ",", "")), context.localUtil.Format( (decimal)(AV22QtdDmn), "ZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtddmn_Jsonclick, 0, "Attribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "demandas", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Valor total R$", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavValortotal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV23ValorTotal, 18, 5, ",", "")), context.localUtil.Format( AV23ValorTotal, "ZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavValortotal_Jsonclick, 0, "Attribute", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  style=\""+CSSHelper.Prettify( "height:25px")+"\" class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Respons�vel::", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_Internalname, StringUtil.RTrim( AV11Usuario), StringUtil.RTrim( context.localUtil.Format( AV11Usuario, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_Jsonclick, 0, "", "", "", "", 1, edtavUsuario_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 0, -1, -1, true, "Nome", "left", true, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:24px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Nome:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNome_Internalname, StringUtil.RTrim( AV8Nome), StringUtil.RTrim( context.localUtil.Format( AV8Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNome_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "Parecer final:", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavParecerfinal_Internalname, AV25ParecerFinal, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, 1, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "Coment�rios:", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavComentarios_Internalname, AV26Comentarios, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", 0, 1, 1, 0, 80, "chr", 6, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0041"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcloteanexos_Component) != 0 )
               {
                  if ( ! context.isAjaxRequest( ) )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0041"+"");
                  }
                  WebComp_Wcloteanexos.componentdraw();
                  if ( ! context.isAjaxRequest( ) )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Lote_Nome.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Cancelar", bttButton2_Jsonclick, 5, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CANCELAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Lote_Nome.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BT2e( true) ;
         }
         else
         {
            wb_table1_2_BT2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13Contratada_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")));
         AV24Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Codigo), "ZZZZZ9")));
         AV10Lote_UserCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Lote_UserCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_USERCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Lote_UserCod), "ZZZZZ9")));
         AV16OrderedBy = Convert.ToInt16(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16OrderedBy), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORDEREDBY", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16OrderedBy), "ZZZ9")));
         AV17OrderedDsc = (bool)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17OrderedDsc", AV17OrderedDsc);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORDEREDDSC", GetSecureSignedToken( "", AV17OrderedDsc));
         AV18GridStateXML = (String)getParm(obj,5);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GridStateXML", AV18GridStateXML);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDSTATEXML", GetSecureSignedToken( "", AV18GridStateXML));
         AV22QtdDmn = Convert.ToInt16(getParm(obj,6));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22QtdDmn), 4, 0)));
         AV23ValorTotal = (decimal)(Convert.ToDecimal((decimal)getParm(obj,7)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ValorTotal", StringUtil.LTrim( StringUtil.Str( AV23ValorTotal, 18, 5)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABT2( ) ;
         WSBT2( ) ;
         WEBT2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( StringUtil.StrCmp(WebComp_Wcloteanexos_Component, "") == 0 )
         {
            WebComp_Wcloteanexos = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcloteanexos.ComponentInit();
            WebComp_Wcloteanexos.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcloteanexos_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( ! ( WebComp_Wcloteanexos == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcloteanexos_Component) != 0 )
            {
               WebComp_Wcloteanexos.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216201654");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_lote_nome.js", "?20206216201655");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavLote_numero_Internalname = "vLOTE_NUMERO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavLote_data_Internalname = "vLOTE_DATA";
         edtavQtddmn_Internalname = "vQTDDMN";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavValortotal_Internalname = "vVALORTOTAL";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavUsuario_Internalname = "vUSUARIO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavNome_Internalname = "vNOME";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         edtavParecerfinal_Internalname = "vPARECERFINAL";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         edtavComentarios_Internalname = "vCOMENTARIOS";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavNome_Jsonclick = "";
         edtavUsuario_Jsonclick = "";
         edtavUsuario_Backcolor = (int)(0x0);
         edtavUsuario_Enabled = 1;
         edtavValortotal_Jsonclick = "";
         edtavQtddmn_Jsonclick = "";
         edtavLote_data_Jsonclick = "";
         edtavLote_data_Enabled = 1;
         edtavLote_numero_Jsonclick = "";
         edtavLote_numero_Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Nome do Lote de Aceite";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12BT2',iparms:[{av:'AV8Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV25ParecerFinal',fld:'vPARECERFINAL',pic:'',nv:''},{av:'AV26Comentarios',fld:'vCOMENTARIOS',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A483ContagemResultado_StatusCnt',fld:'CONTAGEMRESULTADO_STATUSCNT',pic:'Z9',nv:0},{av:'AV18GridStateXML',fld:'vGRIDSTATEXML',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'CANCELAR'","{handler:'E13BT2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV18GridStateXML = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV6Lote_Numero = "";
         AV7Lote_Data = (DateTime)(DateTime.MinValue);
         AV11Usuario = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         WebComp_Wcloteanexos_Component = "";
         scmdbuf = "";
         H00BT2_A456ContagemResultado_Codigo = new int[1] ;
         H00BT2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00BT2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00BT2_A483ContagemResultado_StatusCnt = new short[1] ;
         AV8Nome = "";
         AV25ParecerFinal = "";
         AV26Comentarios = "";
         H00BT3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00BT3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00BT3_A1603ContagemResultado_CntCod = new int[1] ;
         H00BT3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00BT3_A39Contratada_Codigo = new int[1] ;
         H00BT3_n39Contratada_Codigo = new bool[] {false} ;
         H00BT3_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00BT3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00BT3_A530Contratada_Lote = new short[1] ;
         H00BT3_n530Contratada_Lote = new bool[] {false} ;
         H00BT3_A456ContagemResultado_Codigo = new int[1] ;
         H00BT4_A39Contratada_Codigo = new int[1] ;
         H00BT4_n39Contratada_Codigo = new bool[] {false} ;
         H00BT5_A57Usuario_PessoaCod = new int[1] ;
         H00BT5_A1Usuario_Codigo = new int[1] ;
         H00BT5_A58Usuario_PessoaNom = new String[] {""} ;
         H00BT5_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         AV19WebSession = context.GetSession();
         AV5Lote_Nome = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_lote_nome__default(),
            new Object[][] {
                new Object[] {
               H00BT2_A456ContagemResultado_Codigo, H00BT2_A473ContagemResultado_DataCnt, H00BT2_A511ContagemResultado_HoraCnt, H00BT2_A483ContagemResultado_StatusCnt
               }
               , new Object[] {
               H00BT3_A1553ContagemResultado_CntSrvCod, H00BT3_n1553ContagemResultado_CntSrvCod, H00BT3_A1603ContagemResultado_CntCod, H00BT3_n1603ContagemResultado_CntCod, H00BT3_A39Contratada_Codigo, H00BT3_n39Contratada_Codigo, H00BT3_A490ContagemResultado_ContratadaCod, H00BT3_n490ContagemResultado_ContratadaCod, H00BT3_A530Contratada_Lote, H00BT3_n530Contratada_Lote,
               H00BT3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00BT4_A39Contratada_Codigo
               }
               , new Object[] {
               H00BT5_A57Usuario_PessoaCod, H00BT5_A1Usuario_Codigo, H00BT5_A58Usuario_PessoaNom, H00BT5_n58Usuario_PessoaNom
               }
            }
         );
         WebComp_Wcloteanexos = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavLote_numero_Enabled = 0;
         edtavLote_data_Enabled = 0;
         edtavUsuario_Enabled = 0;
      }

      private short AV16OrderedBy ;
      private short AV22QtdDmn ;
      private short wcpOAV16OrderedBy ;
      private short wcpOAV22QtdDmn ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A483ContagemResultado_StatusCnt ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A530Contratada_Lote ;
      private short AV12Contratada_Lote ;
      private short nGXWrapped ;
      private int AV13Contratada_AreaTrabalhoCod ;
      private int AV24Codigo ;
      private int AV10Lote_UserCod ;
      private int wcpOAV13Contratada_AreaTrabalhoCod ;
      private int wcpOAV24Codigo ;
      private int wcpOAV10Lote_UserCod ;
      private int edtavLote_numero_Enabled ;
      private int edtavLote_data_Enabled ;
      private int edtavUsuario_Enabled ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A39Contratada_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int idxLst ;
      private int edtavUsuario_Backcolor ;
      private decimal AV23ValorTotal ;
      private decimal wcpOAV23ValorTotal ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV6Lote_Numero ;
      private String AV11Usuario ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String WebComp_Wcloteanexos_Component ;
      private String edtavLote_numero_Internalname ;
      private String edtavLote_data_Internalname ;
      private String edtavUsuario_Internalname ;
      private String scmdbuf ;
      private String AV8Nome ;
      private String edtavNome_Internalname ;
      private String edtavParecerfinal_Internalname ;
      private String edtavComentarios_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String AV5Lote_Nome ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String edtavLote_numero_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavLote_data_Jsonclick ;
      private String edtavQtddmn_Internalname ;
      private String edtavQtddmn_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtavValortotal_Internalname ;
      private String edtavValortotal_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavUsuario_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavNome_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private DateTime AV7Lote_Data ;
      private bool AV17OrderedDsc ;
      private bool wcpOAV17OrderedDsc ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n39Contratada_Codigo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n530Contratada_Lote ;
      private bool n58Usuario_PessoaNom ;
      private String AV18GridStateXML ;
      private String wcpOAV18GridStateXML ;
      private String AV25ParecerFinal ;
      private String AV26Comentarios ;
      private GXWebComponent WebComp_Wcloteanexos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00BT2_A456ContagemResultado_Codigo ;
      private DateTime[] H00BT2_A473ContagemResultado_DataCnt ;
      private String[] H00BT2_A511ContagemResultado_HoraCnt ;
      private short[] H00BT2_A483ContagemResultado_StatusCnt ;
      private int[] H00BT3_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00BT3_n1553ContagemResultado_CntSrvCod ;
      private int[] H00BT3_A1603ContagemResultado_CntCod ;
      private bool[] H00BT3_n1603ContagemResultado_CntCod ;
      private int[] H00BT3_A39Contratada_Codigo ;
      private bool[] H00BT3_n39Contratada_Codigo ;
      private int[] H00BT3_A490ContagemResultado_ContratadaCod ;
      private bool[] H00BT3_n490ContagemResultado_ContratadaCod ;
      private short[] H00BT3_A530Contratada_Lote ;
      private bool[] H00BT3_n530Contratada_Lote ;
      private int[] H00BT3_A456ContagemResultado_Codigo ;
      private int[] H00BT4_A39Contratada_Codigo ;
      private bool[] H00BT4_n39Contratada_Codigo ;
      private int[] H00BT5_A57Usuario_PessoaCod ;
      private int[] H00BT5_A1Usuario_Codigo ;
      private String[] H00BT5_A58Usuario_PessoaNom ;
      private bool[] H00BT5_n58Usuario_PessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV19WebSession ;
      private GXWebForm Form ;
   }

   public class wp_lote_nome__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BT2 ;
          prmH00BT2 = new Object[] {
          } ;
          Object[] prmH00BT3 ;
          prmH00BT3 = new Object[] {
          new Object[] {"@AV24Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BT4 ;
          prmH00BT4 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BT5 ;
          prmH00BT5 = new Object[] {
          new Object[] {"@AV10Lote_UserCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BT2", "SELECT [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_StatusCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BT2,100,0,true,false )
             ,new CursorDef("H00BT3", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contratada_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Contratada_Lote], T1.[ContagemResultado_Codigo] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV24Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BT3,1,0,true,true )
             ,new CursorDef("H00BT4", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BT4,1,0,false,true )
             ,new CursorDef("H00BT5", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV10Lote_UserCod ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BT5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
