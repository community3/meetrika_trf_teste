/*
               File: PRC_TemPndParaHomologar
        Description: Tem pendÍncia para homologar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:59.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_tempndparahomologar : GXProcedure
   {
      public prc_tempndparahomologar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_tempndparahomologar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_OSVinculada ,
                           out bool aP1_Tem )
      {
         this.A602ContagemResultado_OSVinculada = aP0_ContagemResultado_OSVinculada;
         this.AV9Tem = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_OSVinculada=this.A602ContagemResultado_OSVinculada;
         aP1_Tem=this.AV9Tem;
      }

      public bool executeUdp( ref int aP0_ContagemResultado_OSVinculada )
      {
         this.A602ContagemResultado_OSVinculada = aP0_ContagemResultado_OSVinculada;
         this.AV9Tem = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_OSVinculada=this.A602ContagemResultado_OSVinculada;
         aP1_Tem=this.AV9Tem;
         return AV9Tem ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_OSVinculada ,
                                 out bool aP1_Tem )
      {
         prc_tempndparahomologar objprc_tempndparahomologar;
         objprc_tempndparahomologar = new prc_tempndparahomologar();
         objprc_tempndparahomologar.A602ContagemResultado_OSVinculada = aP0_ContagemResultado_OSVinculada;
         objprc_tempndparahomologar.AV9Tem = false ;
         objprc_tempndparahomologar.context.SetSubmitInitialConfig(context);
         objprc_tempndparahomologar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_tempndparahomologar);
         aP0_ContagemResultado_OSVinculada=this.A602ContagemResultado_OSVinculada;
         aP1_Tem=this.AV9Tem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_tempndparahomologar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new prc_dpnparahomologar(context ).execute( ref  A602ContagemResultado_OSVinculada, out  AV9Tem, out  Gx_msg) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gx_msg = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A602ContagemResultado_OSVinculada ;
      private String Gx_msg ;
      private bool AV9Tem ;
      private int aP0_ContagemResultado_OSVinculada ;
      private bool aP1_Tem ;
   }

}
