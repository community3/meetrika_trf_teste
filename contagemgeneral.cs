/*
               File: ContagemGeneral
        Description: Contagem General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:23.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contagem_Codigo )
      {
         this.A192Contagem_Codigo = aP0_Contagem_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagem_Tecnica = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
         cmbContagem_Status = new GXCombobox();
         cmbContagem_Lock = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A192Contagem_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAI32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContagemGeneral";
               context.Gx_err = 0;
               /* Using cursor H00I33 */
               pr_default.execute(0, new Object[] {A192Contagem_Codigo});
               if ( (pr_default.getStatus(0) != 101) )
               {
                  A265Contagem_QtdItensAprovados = H00I33_A265Contagem_QtdItensAprovados[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
                  n265Contagem_QtdItensAprovados = H00I33_n265Contagem_QtdItensAprovados[0];
               }
               else
               {
                  A265Contagem_QtdItensAprovados = 0;
                  n265Contagem_QtdItensAprovados = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
               }
               pr_default.close(0);
               /* Using cursor H00I35 */
               pr_default.execute(1, new Object[] {A192Contagem_Codigo});
               if ( (pr_default.getStatus(1) != 101) )
               {
                  A264Contagem_QtdItens = H00I35_A264Contagem_QtdItens[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
                  n264Contagem_QtdItens = H00I35_n264Contagem_QtdItens[0];
               }
               else
               {
                  A264Contagem_QtdItens = 0;
                  n264Contagem_QtdItens = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
               }
               pr_default.close(1);
               WSI32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205211822335");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemgeneral.aspx") + "?" + UrlEncode("" +A192Contagem_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_PROPOSITO", A199Contagem_Proposito);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_ESCOPO", A200Contagem_Escopo);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_FRONTEIRA", A201Contagem_Fronteira);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_OBSERVACAO", A202Contagem_Observacao);
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA192Contagem_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA192Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_CONTRATADACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DATACRIACAO", GetSecureSignedToken( sPrefix, A197Contagem_DataCriacao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_NOTAS", GetSecureSignedToken( sPrefix, A1059Contagem_Notas));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOCONTADORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DEMANDA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A946Contagem_Link, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A947Contagem_Fator, "9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DEFLATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_LOCK", GetSecureSignedToken( sPrefix, A948Contagem_Lock));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1111Contagem_DataHomologacao, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_CONSIDERACOES", GetSecureSignedToken( sPrefix, A1112Contagem_Consideracoes));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1120Contagem_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFBA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1113Contagem_PFBA, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFLA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1114Contagem_PFLA, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFBD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1115Contagem_PFBD, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PFLD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1116Contagem_PFLD, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_APLICABILIDADE", GetSecureSignedToken( sPrefix, A1809Contagem_Aplicabilidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1810Contagem_Versao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_ARQUIVOIMP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1812Contagem_ArquivoImp, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_REFERENCIAINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1813Contagem_ReferenciaINM), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_AMBIENTETECNOLOGICO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1814Contagem_AmbienteTecnologico), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A939Contagem_ProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_PROPOSITO_Enabled", StringUtil.BoolToStr( Contagem_proposito_Enabled));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_ESCOPO_Enabled", StringUtil.BoolToStr( Contagem_escopo_Enabled));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_FRONTEIRA_Enabled", StringUtil.BoolToStr( Contagem_fronteira_Enabled));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagem_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemgeneral:[SendSecurityCheck value for]"+"Contagem_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("contagemgeneral:[SendSecurityCheck value for]"+"Contagem_UsuarioContadorCod:"+context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormI32( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemgeneral.js", "?20205211822351");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem General" ;
      }

      protected void WBI30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemgeneral.aspx");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
            }
            wb_table1_2_I32( true) ;
         }
         else
         {
            wb_table1_2_I32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_I32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ProjetoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A939Contagem_ProjetoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A939Contagem_ProjetoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ProjetoCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_ProjetoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTI32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPI30( ) ;
            }
         }
      }

      protected void WSI32( )
      {
         STARTI32( ) ;
         EVTI32( ) ;
      }

      protected void EVTI32( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11I32 */
                                    E11I32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12I32 */
                                    E12I32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13I32 */
                                    E13I32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14I32 */
                                    E14I32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPI30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEI32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormI32( ) ;
            }
         }
      }

      protected void PAI32( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContagem_Tecnica.Name = "CONTAGEM_TECNICA";
            cmbContagem_Tecnica.WebTags = "";
            cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
            cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
            cmbContagem_Tecnica.addItem("2", "Estimada", 0);
            cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
            if ( cmbContagem_Tecnica.ItemCount > 0 )
            {
               A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
               n195Contagem_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
            }
            cmbContagem_Tipo.Name = "CONTAGEM_TIPO";
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
            }
            cmbContagem_Status.Name = "CONTAGEM_STATUS";
            cmbContagem_Status.WebTags = "";
            if ( cmbContagem_Status.ItemCount > 0 )
            {
               A262Contagem_Status = cmbContagem_Status.getValidValue(A262Contagem_Status);
               n262Contagem_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A262Contagem_Status", A262Contagem_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
            }
            cmbContagem_Lock.Name = "CONTAGEM_LOCK";
            cmbContagem_Lock.WebTags = "";
            cmbContagem_Lock.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbContagem_Lock.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbContagem_Lock.ItemCount > 0 )
            {
               A948Contagem_Lock = StringUtil.StrToBool( cmbContagem_Lock.getValidValue(StringUtil.BoolToStr( A948Contagem_Lock)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A948Contagem_Lock", A948Contagem_Lock);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_LOCK", GetSecureSignedToken( sPrefix, A948Contagem_Lock));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagem_Tecnica.ItemCount > 0 )
         {
            A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
         }
         if ( cmbContagem_Tipo.ItemCount > 0 )
         {
            A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
         }
         if ( cmbContagem_Status.ItemCount > 0 )
         {
            A262Contagem_Status = cmbContagem_Status.getValidValue(A262Contagem_Status);
            n262Contagem_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A262Contagem_Status", A262Contagem_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
         }
         if ( cmbContagem_Lock.ItemCount > 0 )
         {
            A948Contagem_Lock = StringUtil.StrToBool( cmbContagem_Lock.getValidValue(StringUtil.BoolToStr( A948Contagem_Lock)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A948Contagem_Lock", A948Contagem_Lock);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_LOCK", GetSecureSignedToken( sPrefix, A948Contagem_Lock));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFI32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContagemGeneral";
         context.Gx_err = 0;
      }

      protected void RFI32( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00I38 */
            pr_default.execute(2, new Object[] {A192Contagem_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A939Contagem_ProjetoCod = H00I38_A939Contagem_ProjetoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A939Contagem_ProjetoCod), "ZZZZZ9")));
               n939Contagem_ProjetoCod = H00I38_n939Contagem_ProjetoCod[0];
               A1814Contagem_AmbienteTecnologico = H00I38_A1814Contagem_AmbienteTecnologico[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_AMBIENTETECNOLOGICO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1814Contagem_AmbienteTecnologico), "ZZZZZ9")));
               n1814Contagem_AmbienteTecnologico = H00I38_n1814Contagem_AmbienteTecnologico[0];
               A1813Contagem_ReferenciaINM = H00I38_A1813Contagem_ReferenciaINM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_REFERENCIAINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1813Contagem_ReferenciaINM), "ZZZZZ9")));
               n1813Contagem_ReferenciaINM = H00I38_n1813Contagem_ReferenciaINM[0];
               A1812Contagem_ArquivoImp = H00I38_A1812Contagem_ArquivoImp[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1812Contagem_ArquivoImp", A1812Contagem_ArquivoImp);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_ARQUIVOIMP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1812Contagem_ArquivoImp, ""))));
               n1812Contagem_ArquivoImp = H00I38_n1812Contagem_ArquivoImp[0];
               A1811Contagem_ServicoCod = H00I38_A1811Contagem_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9")));
               n1811Contagem_ServicoCod = H00I38_n1811Contagem_ServicoCod[0];
               A1810Contagem_Versao = H00I38_A1810Contagem_Versao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1810Contagem_Versao", A1810Contagem_Versao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1810Contagem_Versao, ""))));
               n1810Contagem_Versao = H00I38_n1810Contagem_Versao[0];
               A1809Contagem_Aplicabilidade = H00I38_A1809Contagem_Aplicabilidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1809Contagem_Aplicabilidade", A1809Contagem_Aplicabilidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_APLICABILIDADE", GetSecureSignedToken( sPrefix, A1809Contagem_Aplicabilidade));
               n1809Contagem_Aplicabilidade = H00I38_n1809Contagem_Aplicabilidade[0];
               A1116Contagem_PFLD = H00I38_A1116Contagem_PFLD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFLD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1116Contagem_PFLD, "ZZ,ZZZ,ZZ9.999")));
               n1116Contagem_PFLD = H00I38_n1116Contagem_PFLD[0];
               A1115Contagem_PFBD = H00I38_A1115Contagem_PFBD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFBD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1115Contagem_PFBD, "ZZ,ZZZ,ZZ9.999")));
               n1115Contagem_PFBD = H00I38_n1115Contagem_PFBD[0];
               A1114Contagem_PFLA = H00I38_A1114Contagem_PFLA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFLA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1114Contagem_PFLA, "ZZ,ZZZ,ZZ9.999")));
               n1114Contagem_PFLA = H00I38_n1114Contagem_PFLA[0];
               A1113Contagem_PFBA = H00I38_A1113Contagem_PFBA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFBA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1113Contagem_PFBA, "ZZ,ZZZ,ZZ9.999")));
               n1113Contagem_PFBA = H00I38_n1113Contagem_PFBA[0];
               A1120Contagem_Descricao = H00I38_A1120Contagem_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1120Contagem_Descricao", A1120Contagem_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1120Contagem_Descricao, ""))));
               n1120Contagem_Descricao = H00I38_n1120Contagem_Descricao[0];
               A1112Contagem_Consideracoes = H00I38_A1112Contagem_Consideracoes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1112Contagem_Consideracoes", A1112Contagem_Consideracoes);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_CONSIDERACOES", GetSecureSignedToken( sPrefix, A1112Contagem_Consideracoes));
               n1112Contagem_Consideracoes = H00I38_n1112Contagem_Consideracoes[0];
               A1111Contagem_DataHomologacao = H00I38_A1111Contagem_DataHomologacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1111Contagem_DataHomologacao, "99/99/99 99:99")));
               n1111Contagem_DataHomologacao = H00I38_n1111Contagem_DataHomologacao[0];
               A948Contagem_Lock = H00I38_A948Contagem_Lock[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A948Contagem_Lock", A948Contagem_Lock);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_LOCK", GetSecureSignedToken( sPrefix, A948Contagem_Lock));
               A942Contagem_ProjetoSigla = H00I38_A942Contagem_ProjetoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
               n942Contagem_ProjetoSigla = H00I38_n942Contagem_ProjetoSigla[0];
               A949Contagem_SistemaCoord = H00I38_A949Contagem_SistemaCoord[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
               n949Contagem_SistemaCoord = H00I38_n949Contagem_SistemaCoord[0];
               A941Contagem_SistemaSigla = H00I38_A941Contagem_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
               n941Contagem_SistemaSigla = H00I38_n941Contagem_SistemaSigla[0];
               A940Contagem_SistemaCod = H00I38_A940Contagem_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")));
               n940Contagem_SistemaCod = H00I38_n940Contagem_SistemaCod[0];
               A1117Contagem_Deflator = H00I38_A1117Contagem_Deflator[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DEFLATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")));
               n1117Contagem_Deflator = H00I38_n1117Contagem_Deflator[0];
               A947Contagem_Fator = H00I38_A947Contagem_Fator[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A947Contagem_Fator, "9.99")));
               n947Contagem_Fator = H00I38_n947Contagem_Fator[0];
               A946Contagem_Link = H00I38_A946Contagem_Link[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A946Contagem_Link", A946Contagem_Link);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A946Contagem_Link, ""))));
               n946Contagem_Link = H00I38_n946Contagem_Link[0];
               A945Contagem_Demanda = H00I38_A945Contagem_Demanda[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A945Contagem_Demanda", A945Contagem_Demanda);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DEMANDA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, ""))));
               n945Contagem_Demanda = H00I38_n945Contagem_Demanda[0];
               A262Contagem_Status = H00I38_A262Contagem_Status[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A262Contagem_Status", A262Contagem_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
               n262Contagem_Status = H00I38_n262Contagem_Status[0];
               A215Contagem_UsuarioContadorPessoaNom = H00I38_A215Contagem_UsuarioContadorPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
               n215Contagem_UsuarioContadorPessoaNom = H00I38_n215Contagem_UsuarioContadorPessoaNom[0];
               A214Contagem_UsuarioContadorPessoaCod = H00I38_A214Contagem_UsuarioContadorPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
               n214Contagem_UsuarioContadorPessoaCod = H00I38_n214Contagem_UsuarioContadorPessoaCod[0];
               A213Contagem_UsuarioContadorCod = H00I38_A213Contagem_UsuarioContadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOCONTADORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")));
               n213Contagem_UsuarioContadorCod = H00I38_n213Contagem_UsuarioContadorCod[0];
               A1119Contagem_Divergencia = H00I38_A1119Contagem_Divergencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")));
               n1119Contagem_Divergencia = H00I38_n1119Contagem_Divergencia[0];
               A944Contagem_PFL = H00I38_A944Contagem_PFL[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
               n944Contagem_PFL = H00I38_n944Contagem_PFL[0];
               A943Contagem_PFB = H00I38_A943Contagem_PFB[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
               n943Contagem_PFB = H00I38_n943Contagem_PFB[0];
               A1059Contagem_Notas = H00I38_A1059Contagem_Notas[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1059Contagem_Notas", A1059Contagem_Notas);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_NOTAS", GetSecureSignedToken( sPrefix, A1059Contagem_Notas));
               n1059Contagem_Notas = H00I38_n1059Contagem_Notas[0];
               A202Contagem_Observacao = H00I38_A202Contagem_Observacao[0];
               n202Contagem_Observacao = H00I38_n202Contagem_Observacao[0];
               A201Contagem_Fronteira = H00I38_A201Contagem_Fronteira[0];
               n201Contagem_Fronteira = H00I38_n201Contagem_Fronteira[0];
               A200Contagem_Escopo = H00I38_A200Contagem_Escopo[0];
               n200Contagem_Escopo = H00I38_n200Contagem_Escopo[0];
               A199Contagem_Proposito = H00I38_A199Contagem_Proposito[0];
               n199Contagem_Proposito = H00I38_n199Contagem_Proposito[0];
               A196Contagem_Tipo = H00I38_A196Contagem_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
               n196Contagem_Tipo = H00I38_n196Contagem_Tipo[0];
               A195Contagem_Tecnica = H00I38_A195Contagem_Tecnica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
               n195Contagem_Tecnica = H00I38_n195Contagem_Tecnica[0];
               A194Contagem_AreaTrabalhoDes = H00I38_A194Contagem_AreaTrabalhoDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
               n194Contagem_AreaTrabalhoDes = H00I38_n194Contagem_AreaTrabalhoDes[0];
               A197Contagem_DataCriacao = H00I38_A197Contagem_DataCriacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DATACRIACAO", GetSecureSignedToken( sPrefix, A197Contagem_DataCriacao));
               A1118Contagem_ContratadaCod = H00I38_A1118Contagem_ContratadaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_CONTRATADACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")));
               n1118Contagem_ContratadaCod = H00I38_n1118Contagem_ContratadaCod[0];
               A193Contagem_AreaTrabalhoCod = H00I38_A193Contagem_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")));
               A265Contagem_QtdItensAprovados = H00I38_A265Contagem_QtdItensAprovados[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
               n265Contagem_QtdItensAprovados = H00I38_n265Contagem_QtdItensAprovados[0];
               A264Contagem_QtdItens = H00I38_A264Contagem_QtdItens[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
               n264Contagem_QtdItens = H00I38_n264Contagem_QtdItens[0];
               A942Contagem_ProjetoSigla = H00I38_A942Contagem_ProjetoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
               n942Contagem_ProjetoSigla = H00I38_n942Contagem_ProjetoSigla[0];
               A949Contagem_SistemaCoord = H00I38_A949Contagem_SistemaCoord[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
               n949Contagem_SistemaCoord = H00I38_n949Contagem_SistemaCoord[0];
               A941Contagem_SistemaSigla = H00I38_A941Contagem_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
               n941Contagem_SistemaSigla = H00I38_n941Contagem_SistemaSigla[0];
               A214Contagem_UsuarioContadorPessoaCod = H00I38_A214Contagem_UsuarioContadorPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
               n214Contagem_UsuarioContadorPessoaCod = H00I38_n214Contagem_UsuarioContadorPessoaCod[0];
               A215Contagem_UsuarioContadorPessoaNom = H00I38_A215Contagem_UsuarioContadorPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
               n215Contagem_UsuarioContadorPessoaNom = H00I38_n215Contagem_UsuarioContadorPessoaNom[0];
               A194Contagem_AreaTrabalhoDes = H00I38_A194Contagem_AreaTrabalhoDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
               n194Contagem_AreaTrabalhoDes = H00I38_n194Contagem_AreaTrabalhoDes[0];
               A265Contagem_QtdItensAprovados = H00I38_A265Contagem_QtdItensAprovados[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
               n265Contagem_QtdItensAprovados = H00I38_n265Contagem_QtdItensAprovados[0];
               A264Contagem_QtdItens = H00I38_A264Contagem_QtdItens[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
               n264Contagem_QtdItens = H00I38_n264Contagem_QtdItens[0];
               /* Execute user event: E12I32 */
               E12I32 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            WBI30( ) ;
         }
      }

      protected void STRUPI30( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContagemGeneral";
         context.Gx_err = 0;
         /* Using cursor H00I310 */
         pr_default.execute(3, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A265Contagem_QtdItensAprovados = H00I310_A265Contagem_QtdItensAprovados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            n265Contagem_QtdItensAprovados = H00I310_n265Contagem_QtdItensAprovados[0];
         }
         else
         {
            A265Contagem_QtdItensAprovados = 0;
            n265Contagem_QtdItensAprovados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
         }
         pr_default.close(3);
         /* Using cursor H00I312 */
         pr_default.execute(4, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A264Contagem_QtdItens = H00I312_A264Contagem_QtdItens[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            n264Contagem_QtdItens = H00I312_n264Contagem_QtdItens[0];
         }
         else
         {
            A264Contagem_QtdItens = 0;
            n264Contagem_QtdItens = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
         }
         pr_default.close(4);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11I32 */
         E11I32 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")));
            A1118Contagem_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ContratadaCod_Internalname), ",", "."));
            n1118Contagem_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_CONTRATADACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")));
            A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DATACRIACAO", GetSecureSignedToken( sPrefix, A197Contagem_DataCriacao));
            A194Contagem_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContagem_AreaTrabalhoDes_Internalname));
            n194Contagem_AreaTrabalhoDes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
            cmbContagem_Tecnica.CurrentValue = cgiGet( cmbContagem_Tecnica_Internalname);
            A195Contagem_Tecnica = cgiGet( cmbContagem_Tecnica_Internalname);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
            cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
            A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
            A1059Contagem_Notas = cgiGet( edtContagem_Notas_Internalname);
            n1059Contagem_Notas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1059Contagem_Notas", A1059Contagem_Notas);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_NOTAS", GetSecureSignedToken( sPrefix, A1059Contagem_Notas));
            A943Contagem_PFB = context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".");
            n943Contagem_PFB = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
            A944Contagem_PFL = context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".");
            n944Contagem_PFL = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
            A1119Contagem_Divergencia = context.localUtil.CToN( cgiGet( edtContagem_Divergencia_Internalname), ",", ".");
            n1119Contagem_Divergencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")));
            A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
            n213Contagem_UsuarioContadorCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOCONTADORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")));
            A214Contagem_UsuarioContadorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorPessoaCod_Internalname), ",", "."));
            n214Contagem_UsuarioContadorPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            A215Contagem_UsuarioContadorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioContadorPessoaNom_Internalname));
            n215Contagem_UsuarioContadorPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
            cmbContagem_Status.CurrentValue = cgiGet( cmbContagem_Status_Internalname);
            A262Contagem_Status = cgiGet( cmbContagem_Status_Internalname);
            n262Contagem_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A262Contagem_Status", A262Contagem_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
            A945Contagem_Demanda = cgiGet( edtContagem_Demanda_Internalname);
            n945Contagem_Demanda = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A945Contagem_Demanda", A945Contagem_Demanda);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DEMANDA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, ""))));
            A946Contagem_Link = cgiGet( edtContagem_Link_Internalname);
            n946Contagem_Link = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A946Contagem_Link", A946Contagem_Link);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A946Contagem_Link, ""))));
            A947Contagem_Fator = context.localUtil.CToN( cgiGet( edtContagem_Fator_Internalname), ",", ".");
            n947Contagem_Fator = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A947Contagem_Fator, "9.99")));
            A1117Contagem_Deflator = context.localUtil.CToN( cgiGet( edtContagem_Deflator_Internalname), ",", ".");
            n1117Contagem_Deflator = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DEFLATOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")));
            A940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", "."));
            n940Contagem_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")));
            A941Contagem_SistemaSigla = StringUtil.Upper( cgiGet( edtContagem_SistemaSigla_Internalname));
            n941Contagem_SistemaSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
            A949Contagem_SistemaCoord = StringUtil.Upper( cgiGet( edtContagem_SistemaCoord_Internalname));
            n949Contagem_SistemaCoord = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
            A942Contagem_ProjetoSigla = StringUtil.Upper( cgiGet( edtContagem_ProjetoSigla_Internalname));
            n942Contagem_ProjetoSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
            A264Contagem_QtdItens = (short)(context.localUtil.CToN( cgiGet( edtContagem_QtdItens_Internalname), ",", "."));
            n264Contagem_QtdItens = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            A265Contagem_QtdItensAprovados = (short)(context.localUtil.CToN( cgiGet( edtContagem_QtdItensAprovados_Internalname), ",", "."));
            n265Contagem_QtdItensAprovados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            cmbContagem_Lock.CurrentValue = cgiGet( cmbContagem_Lock_Internalname);
            A948Contagem_Lock = StringUtil.StrToBool( cgiGet( cmbContagem_Lock_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A948Contagem_Lock", A948Contagem_Lock);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_LOCK", GetSecureSignedToken( sPrefix, A948Contagem_Lock));
            A1111Contagem_DataHomologacao = context.localUtil.CToT( cgiGet( edtContagem_DataHomologacao_Internalname), 0);
            n1111Contagem_DataHomologacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1111Contagem_DataHomologacao, "99/99/99 99:99")));
            A1112Contagem_Consideracoes = cgiGet( edtContagem_Consideracoes_Internalname);
            n1112Contagem_Consideracoes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1112Contagem_Consideracoes", A1112Contagem_Consideracoes);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_CONSIDERACOES", GetSecureSignedToken( sPrefix, A1112Contagem_Consideracoes));
            A1120Contagem_Descricao = cgiGet( edtContagem_Descricao_Internalname);
            n1120Contagem_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1120Contagem_Descricao", A1120Contagem_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1120Contagem_Descricao, ""))));
            A1113Contagem_PFBA = context.localUtil.CToN( cgiGet( edtContagem_PFBA_Internalname), ",", ".");
            n1113Contagem_PFBA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFBA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1113Contagem_PFBA, "ZZ,ZZZ,ZZ9.999")));
            A1114Contagem_PFLA = context.localUtil.CToN( cgiGet( edtContagem_PFLA_Internalname), ",", ".");
            n1114Contagem_PFLA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFLA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1114Contagem_PFLA, "ZZ,ZZZ,ZZ9.999")));
            A1115Contagem_PFBD = context.localUtil.CToN( cgiGet( edtContagem_PFBD_Internalname), ",", ".");
            n1115Contagem_PFBD = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFBD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1115Contagem_PFBD, "ZZ,ZZZ,ZZ9.999")));
            A1116Contagem_PFLD = context.localUtil.CToN( cgiGet( edtContagem_PFLD_Internalname), ",", ".");
            n1116Contagem_PFLD = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PFLD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1116Contagem_PFLD, "ZZ,ZZZ,ZZ9.999")));
            A1809Contagem_Aplicabilidade = cgiGet( edtContagem_Aplicabilidade_Internalname);
            n1809Contagem_Aplicabilidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1809Contagem_Aplicabilidade", A1809Contagem_Aplicabilidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_APLICABILIDADE", GetSecureSignedToken( sPrefix, A1809Contagem_Aplicabilidade));
            A1810Contagem_Versao = cgiGet( edtContagem_Versao_Internalname);
            n1810Contagem_Versao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1810Contagem_Versao", A1810Contagem_Versao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1810Contagem_Versao, ""))));
            A1811Contagem_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ServicoCod_Internalname), ",", "."));
            n1811Contagem_ServicoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9")));
            A1812Contagem_ArquivoImp = cgiGet( edtContagem_ArquivoImp_Internalname);
            n1812Contagem_ArquivoImp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1812Contagem_ArquivoImp", A1812Contagem_ArquivoImp);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_ARQUIVOIMP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1812Contagem_ArquivoImp, ""))));
            A1813Contagem_ReferenciaINM = (int)(context.localUtil.CToN( cgiGet( edtContagem_ReferenciaINM_Internalname), ",", "."));
            n1813Contagem_ReferenciaINM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_REFERENCIAINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1813Contagem_ReferenciaINM), "ZZZZZ9")));
            A1814Contagem_AmbienteTecnologico = (int)(context.localUtil.CToN( cgiGet( edtContagem_AmbienteTecnologico_Internalname), ",", "."));
            n1814Contagem_AmbienteTecnologico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_AMBIENTETECNOLOGICO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1814Contagem_AmbienteTecnologico), "ZZZZZ9")));
            A939Contagem_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ProjetoCod_Internalname), ",", "."));
            n939Contagem_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A939Contagem_ProjetoCod), "ZZZZZ9")));
            /* Read saved values. */
            A199Contagem_Proposito = cgiGet( sPrefix+"CONTAGEM_PROPOSITO");
            A200Contagem_Escopo = cgiGet( sPrefix+"CONTAGEM_ESCOPO");
            A201Contagem_Fronteira = cgiGet( sPrefix+"CONTAGEM_FRONTEIRA");
            A202Contagem_Observacao = cgiGet( sPrefix+"CONTAGEM_OBSERVACAO");
            wcpOA192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA192Contagem_Codigo"), ",", "."));
            Contagem_proposito_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"CONTAGEM_PROPOSITO_Enabled"));
            Contagem_escopo_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"CONTAGEM_ESCOPO_Enabled"));
            Contagem_fronteira_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"CONTAGEM_FRONTEIRA_Enabled"));
            Contagem_observacao_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"CONTAGEM_OBSERVACAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemGeneral";
            A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9");
            A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
            n213Contagem_UsuarioContadorCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOCONTADORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemgeneral:[SecurityCheckFailed value for]"+"Contagem_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9"));
               GXUtil.WriteLog("contagemgeneral:[SecurityCheckFailed value for]"+"Contagem_UsuarioContadorCod:"+context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11I32 */
         E11I32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11I32( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12I32( )
      {
         /* Load Routine */
         edtContagem_AreaTrabalhoDes_Link = formatLink("viewareatrabalho.aspx") + "?" + UrlEncode("" +A193Contagem_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_AreaTrabalhoDes_Internalname, "Link", edtContagem_AreaTrabalhoDes_Link);
         edtContagem_UsuarioContadorPessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A213Contagem_UsuarioContadorCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioContadorPessoaNom_Internalname, "Link", edtContagem_UsuarioContadorPessoaNom_Link);
         edtContagem_ProjetoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_ProjetoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ProjetoCod_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13I32( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A192Contagem_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14I32( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A192Contagem_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contagem";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Contagem_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_I32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_I32( true) ;
         }
         else
         {
            wb_table2_8_I32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_I32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_226_I32( true) ;
         }
         else
         {
            wb_table3_226_I32( false) ;
         }
         return  ;
      }

      protected void wb_table3_226_I32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_I32e( true) ;
         }
         else
         {
            wb_table1_2_I32e( false) ;
         }
      }

      protected void wb_table3_226_I32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 229,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 231,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_226_I32e( true) ;
         }
         else
         {
            wb_table3_226_I32e( false) ;
         }
      }

      protected void wb_table2_8_I32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_codigo_Internalname, "C�digo", "", "", lblTextblockcontagem_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhocod_Internalname, "Cod. �rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_contratadacod_Internalname, "Contratada", "", "", lblTextblockcontagem_contratadacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ContratadaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datacriacao_Internalname, "Data da contagem", "", "", lblTextblockcontagem_datacriacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagem_DataCriacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataCriacao_Internalname, context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"), context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataCriacao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContagemGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataCriacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhodes_Internalname, "�rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhodes_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoDes_Internalname, A194Contagem_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( A194Contagem_AreaTrabalhoDes, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagem_AreaTrabalhoDes_Link, "", "", "", edtContagem_AreaTrabalhoDes_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tecnica_Internalname, "T�cnica de Contagem", "", "", lblTextblockcontagem_tecnica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tecnica, cmbContagem_Tecnica_Internalname, StringUtil.RTrim( A195Contagem_Tecnica), 1, cmbContagem_Tecnica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemGeneral.htm");
            cmbContagem_Tecnica.CurrentValue = StringUtil.RTrim( A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Tecnica_Internalname, "Values", (String)(cmbContagem_Tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tipo_Internalname, "Tipo de Contagem", "", "", lblTextblockcontagem_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tipo, cmbContagem_Tipo_Internalname, StringUtil.RTrim( A196Contagem_Tipo), 1, cmbContagem_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemGeneral.htm");
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_proposito_Internalname, "Prop�sito", "", "", lblTextblockcontagem_proposito_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONTAGEM_PROPOSITOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_escopo_Internalname, "da Contagem", "", "", lblTextblockcontagem_escopo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONTAGEM_ESCOPOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_fronteira_Internalname, "Fronteira", "", "", lblTextblockcontagem_fronteira_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONTAGEM_FRONTEIRAContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_observacao_Internalname, "Oberserva��es", "", "", lblTextblockcontagem_observacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONTAGEM_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_notas_Internalname, "Notas", "", "", lblTextblockcontagem_notas_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Notas_Internalname, A1059Contagem_Notas, "", "", 1, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", 0, true, "Html", "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfb_Internalname, "PFB", "", "", lblTextblockcontagem_pfb_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ",", "")), context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFB_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfl_Internalname, "PFL", "", "", lblTextblockcontagem_pfl_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ",", "")), context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFL_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_divergencia_Internalname, "Contagem_Divergencia", "", "", lblTextblockcontagem_divergencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Divergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A1119Contagem_Divergencia, 6, 2, ",", "")), context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Divergencia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorcod_Internalname, "Usu�rio Contador", "", "", lblTextblockcontagem_usuariocontadorcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorpessoacod_Internalname, "Pessoa Contador", "", "", lblTextblockcontagem_usuariocontadorpessoacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorpessoanom_Internalname, "do Contador", "", "", lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaNom_Internalname, StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom), StringUtil.RTrim( context.localUtil.Format( A215Contagem_UsuarioContadorPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagem_UsuarioContadorPessoaNom_Link, "", "", "", edtContagem_UsuarioContadorPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_status_Internalname, "Status", "", "", lblTextblockcontagem_status_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Status, cmbContagem_Status_Internalname, StringUtil.RTrim( A262Contagem_Status), 1, cmbContagem_Status_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemGeneral.htm");
            cmbContagem_Status.CurrentValue = StringUtil.RTrim( A262Contagem_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Status_Internalname, "Values", (String)(cmbContagem_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_demanda_Internalname, "Demanda", "", "", lblTextblockcontagem_demanda_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Demanda_Internalname, A945Contagem_Demanda, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Demanda_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_link_Internalname, "Link", "", "", lblTextblockcontagem_link_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Link_Internalname, A946Contagem_Link, "", "", 0, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "255", -1, "", "", -1, true, "", "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_fator_Internalname, "Fator", "", "", lblTextblockcontagem_fator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Fator_Internalname, StringUtil.LTrim( StringUtil.NToC( A947Contagem_Fator, 4, 2, ",", "")), context.localUtil.Format( A947Contagem_Fator, "9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Fator_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_deflator_Internalname, "Deflator", "", "", lblTextblockcontagem_deflator_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Deflator_Internalname, StringUtil.LTrim( StringUtil.NToC( A1117Contagem_Deflator, 6, 2, ",", "")), context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Deflator_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemasigla_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemasigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaSigla_Internalname, StringUtil.RTrim( A941Contagem_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( A941Contagem_SistemaSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaSigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemacoord_Internalname, "gestora", "", "", lblTextblockcontagem_sistemacoord_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCoord_Internalname, A949Contagem_SistemaCoord, StringUtil.RTrim( context.localUtil.Format( A949Contagem_SistemaCoord, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCoord_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_projetosigla_Internalname, "Projeto", "", "", lblTextblockcontagem_projetosigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ProjetoSigla_Internalname, StringUtil.RTrim( A942Contagem_ProjetoSigla), StringUtil.RTrim( context.localUtil.Format( A942Contagem_ProjetoSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ProjetoSigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_qtditens_Internalname, "de Itens", "", "", lblTextblockcontagem_qtditens_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_QtdItens_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A264Contagem_QtdItens), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A264Contagem_QtdItens), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_QtdItens_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_qtditensaprovados_Internalname, "Itens Aprovados", "", "", lblTextblockcontagem_qtditensaprovados_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_QtdItensAprovados_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A265Contagem_QtdItensAprovados), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A265Contagem_QtdItensAprovados), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_QtdItensAprovados_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_lock_Internalname, "Bloqueado", "", "", lblTextblockcontagem_lock_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Lock, cmbContagem_Lock_Internalname, StringUtil.BoolToStr( A948Contagem_Lock), 1, cmbContagem_Lock_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemGeneral.htm");
            cmbContagem_Lock.CurrentValue = StringUtil.BoolToStr( A948Contagem_Lock);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Lock_Internalname, "Values", (String)(cmbContagem_Lock.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datahomologacao_Internalname, "Homologa��o", "", "", lblTextblockcontagem_datahomologacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagem_DataHomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataHomologacao_Internalname, context.localUtil.TToC( A1111Contagem_DataHomologacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1111Contagem_DataHomologacao, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataHomologacao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataHomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_consideracoes_Internalname, "Considera��es", "", "", lblTextblockcontagem_consideracoes_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Consideracoes_Internalname, A1112Contagem_Consideracoes, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_descricao_Internalname, "Descricao", "", "", lblTextblockcontagem_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Descricao_Internalname, A1120Contagem_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfba_Internalname, "PFBA", "", "", lblTextblockcontagem_pfba_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFBA_Internalname, StringUtil.LTrim( StringUtil.NToC( A1113Contagem_PFBA, 14, 5, ",", "")), context.localUtil.Format( A1113Contagem_PFBA, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFBA_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfla_Internalname, "PFLA", "", "", lblTextblockcontagem_pfla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFLA_Internalname, StringUtil.LTrim( StringUtil.NToC( A1114Contagem_PFLA, 14, 5, ",", "")), context.localUtil.Format( A1114Contagem_PFLA, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFLA_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfbd_Internalname, "PFBD", "", "", lblTextblockcontagem_pfbd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFBD_Internalname, StringUtil.LTrim( StringUtil.NToC( A1115Contagem_PFBD, 14, 5, ",", "")), context.localUtil.Format( A1115Contagem_PFBD, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFBD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfld_Internalname, "PFLD", "", "", lblTextblockcontagem_pfld_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_PFLD_Internalname, StringUtil.LTrim( StringUtil.NToC( A1116Contagem_PFLD, 14, 5, ",", "")), context.localUtil.Format( A1116Contagem_PFLD, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFLD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_aplicabilidade_Internalname, "Contagem_Aplicabilidade", "", "", lblTextblockcontagem_aplicabilidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Aplicabilidade_Internalname, A1809Contagem_Aplicabilidade, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1048576", -1, "", "", -1, true, "", "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_versao_Internalname, "Contagem_Versao", "", "", lblTextblockcontagem_versao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Versao_Internalname, A1810Contagem_Versao, StringUtil.RTrim( context.localUtil.Format( A1810Contagem_Versao, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Versao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_servicocod_Internalname, "Cod", "", "", lblTextblockcontagem_servicocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1811Contagem_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ServicoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_arquivoimp_Internalname, "Imp", "", "", lblTextblockcontagem_arquivoimp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_ArquivoImp_Internalname, A1812Contagem_ArquivoImp, "", "", 0, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_referenciainm_Internalname, "INM", "", "", lblTextblockcontagem_referenciainm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ReferenciaINM_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1813Contagem_ReferenciaINM), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1813Contagem_ReferenciaINM), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ReferenciaINM_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_ambientetecnologico_Internalname, "Tecnologico", "", "", lblTextblockcontagem_ambientetecnologico_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AmbienteTecnologico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1814Contagem_AmbienteTecnologico), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AmbienteTecnologico_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_I32e( true) ;
         }
         else
         {
            wb_table2_8_I32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A192Contagem_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAI32( ) ;
         WSI32( ) ;
         WEI32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA192Contagem_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAI32( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAI32( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A192Contagem_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
         wcpOA192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA192Contagem_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A192Contagem_Codigo != wcpOA192Contagem_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA192Contagem_Codigo = A192Contagem_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA192Contagem_Codigo = cgiGet( sPrefix+"A192Contagem_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA192Contagem_Codigo) > 0 )
         {
            A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA192Contagem_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
         else
         {
            A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A192Contagem_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAI32( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSI32( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSI32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A192Contagem_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA192Contagem_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A192Contagem_Codigo_CTRL", StringUtil.RTrim( sCtrlA192Contagem_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEI32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205211822645");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemgeneral.js", "?20205211822645");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagem_codigo_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_CODIGO";
         edtContagem_Codigo_Internalname = sPrefix+"CONTAGEM_CODIGO";
         lblTextblockcontagem_areatrabalhocod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_AREATRABALHOCOD";
         edtContagem_AreaTrabalhoCod_Internalname = sPrefix+"CONTAGEM_AREATRABALHOCOD";
         lblTextblockcontagem_contratadacod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_CONTRATADACOD";
         edtContagem_ContratadaCod_Internalname = sPrefix+"CONTAGEM_CONTRATADACOD";
         lblTextblockcontagem_datacriacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DATACRIACAO";
         edtContagem_DataCriacao_Internalname = sPrefix+"CONTAGEM_DATACRIACAO";
         lblTextblockcontagem_areatrabalhodes_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_AREATRABALHODES";
         edtContagem_AreaTrabalhoDes_Internalname = sPrefix+"CONTAGEM_AREATRABALHODES";
         lblTextblockcontagem_tecnica_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_TECNICA";
         cmbContagem_Tecnica_Internalname = sPrefix+"CONTAGEM_TECNICA";
         lblTextblockcontagem_tipo_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_TIPO";
         cmbContagem_Tipo_Internalname = sPrefix+"CONTAGEM_TIPO";
         lblTextblockcontagem_proposito_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PROPOSITO";
         Contagem_proposito_Internalname = sPrefix+"CONTAGEM_PROPOSITO";
         lblTextblockcontagem_escopo_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_ESCOPO";
         Contagem_escopo_Internalname = sPrefix+"CONTAGEM_ESCOPO";
         lblTextblockcontagem_fronteira_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_FRONTEIRA";
         Contagem_fronteira_Internalname = sPrefix+"CONTAGEM_FRONTEIRA";
         lblTextblockcontagem_observacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_OBSERVACAO";
         Contagem_observacao_Internalname = sPrefix+"CONTAGEM_OBSERVACAO";
         lblTextblockcontagem_notas_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_NOTAS";
         edtContagem_Notas_Internalname = sPrefix+"CONTAGEM_NOTAS";
         lblTextblockcontagem_pfb_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PFB";
         edtContagem_PFB_Internalname = sPrefix+"CONTAGEM_PFB";
         lblTextblockcontagem_pfl_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PFL";
         edtContagem_PFL_Internalname = sPrefix+"CONTAGEM_PFL";
         lblTextblockcontagem_divergencia_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DIVERGENCIA";
         edtContagem_Divergencia_Internalname = sPrefix+"CONTAGEM_DIVERGENCIA";
         lblTextblockcontagem_usuariocontadorcod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorCod_Internalname = sPrefix+"CONTAGEM_USUARIOCONTADORCOD";
         lblTextblockcontagem_usuariocontadorpessoacod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_USUARIOCONTADORPESSOACOD";
         edtContagem_UsuarioContadorPessoaCod_Internalname = sPrefix+"CONTAGEM_USUARIOCONTADORPESSOACOD";
         lblTextblockcontagem_usuariocontadorpessoanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_USUARIOCONTADORPESSOANOM";
         edtContagem_UsuarioContadorPessoaNom_Internalname = sPrefix+"CONTAGEM_USUARIOCONTADORPESSOANOM";
         lblTextblockcontagem_status_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_STATUS";
         cmbContagem_Status_Internalname = sPrefix+"CONTAGEM_STATUS";
         lblTextblockcontagem_demanda_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DEMANDA";
         edtContagem_Demanda_Internalname = sPrefix+"CONTAGEM_DEMANDA";
         lblTextblockcontagem_link_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_LINK";
         edtContagem_Link_Internalname = sPrefix+"CONTAGEM_LINK";
         lblTextblockcontagem_fator_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_FATOR";
         edtContagem_Fator_Internalname = sPrefix+"CONTAGEM_FATOR";
         lblTextblockcontagem_deflator_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DEFLATOR";
         edtContagem_Deflator_Internalname = sPrefix+"CONTAGEM_DEFLATOR";
         lblTextblockcontagem_sistemacod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_SISTEMACOD";
         edtContagem_SistemaCod_Internalname = sPrefix+"CONTAGEM_SISTEMACOD";
         lblTextblockcontagem_sistemasigla_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_SISTEMASIGLA";
         edtContagem_SistemaSigla_Internalname = sPrefix+"CONTAGEM_SISTEMASIGLA";
         lblTextblockcontagem_sistemacoord_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_SISTEMACOORD";
         edtContagem_SistemaCoord_Internalname = sPrefix+"CONTAGEM_SISTEMACOORD";
         lblTextblockcontagem_projetosigla_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PROJETOSIGLA";
         edtContagem_ProjetoSigla_Internalname = sPrefix+"CONTAGEM_PROJETOSIGLA";
         lblTextblockcontagem_qtditens_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_QTDITENS";
         edtContagem_QtdItens_Internalname = sPrefix+"CONTAGEM_QTDITENS";
         lblTextblockcontagem_qtditensaprovados_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_QTDITENSAPROVADOS";
         edtContagem_QtdItensAprovados_Internalname = sPrefix+"CONTAGEM_QTDITENSAPROVADOS";
         lblTextblockcontagem_lock_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_LOCK";
         cmbContagem_Lock_Internalname = sPrefix+"CONTAGEM_LOCK";
         lblTextblockcontagem_datahomologacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DATAHOMOLOGACAO";
         edtContagem_DataHomologacao_Internalname = sPrefix+"CONTAGEM_DATAHOMOLOGACAO";
         lblTextblockcontagem_consideracoes_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_CONSIDERACOES";
         edtContagem_Consideracoes_Internalname = sPrefix+"CONTAGEM_CONSIDERACOES";
         lblTextblockcontagem_descricao_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DESCRICAO";
         edtContagem_Descricao_Internalname = sPrefix+"CONTAGEM_DESCRICAO";
         lblTextblockcontagem_pfba_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PFBA";
         edtContagem_PFBA_Internalname = sPrefix+"CONTAGEM_PFBA";
         lblTextblockcontagem_pfla_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PFLA";
         edtContagem_PFLA_Internalname = sPrefix+"CONTAGEM_PFLA";
         lblTextblockcontagem_pfbd_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PFBD";
         edtContagem_PFBD_Internalname = sPrefix+"CONTAGEM_PFBD";
         lblTextblockcontagem_pfld_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_PFLD";
         edtContagem_PFLD_Internalname = sPrefix+"CONTAGEM_PFLD";
         lblTextblockcontagem_aplicabilidade_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_APLICABILIDADE";
         edtContagem_Aplicabilidade_Internalname = sPrefix+"CONTAGEM_APLICABILIDADE";
         lblTextblockcontagem_versao_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_VERSAO";
         edtContagem_Versao_Internalname = sPrefix+"CONTAGEM_VERSAO";
         lblTextblockcontagem_servicocod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_SERVICOCOD";
         edtContagem_ServicoCod_Internalname = sPrefix+"CONTAGEM_SERVICOCOD";
         lblTextblockcontagem_arquivoimp_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_ARQUIVOIMP";
         edtContagem_ArquivoImp_Internalname = sPrefix+"CONTAGEM_ARQUIVOIMP";
         lblTextblockcontagem_referenciainm_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_REFERENCIAINM";
         edtContagem_ReferenciaINM_Internalname = sPrefix+"CONTAGEM_REFERENCIAINM";
         lblTextblockcontagem_ambientetecnologico_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_AMBIENTETECNOLOGICO";
         edtContagem_AmbienteTecnologico_Internalname = sPrefix+"CONTAGEM_AMBIENTETECNOLOGICO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagem_ProjetoCod_Internalname = sPrefix+"CONTAGEM_PROJETOCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagem_AmbienteTecnologico_Jsonclick = "";
         edtContagem_ReferenciaINM_Jsonclick = "";
         edtContagem_ServicoCod_Jsonclick = "";
         edtContagem_Versao_Jsonclick = "";
         edtContagem_PFLD_Jsonclick = "";
         edtContagem_PFBD_Jsonclick = "";
         edtContagem_PFLA_Jsonclick = "";
         edtContagem_PFBA_Jsonclick = "";
         edtContagem_DataHomologacao_Jsonclick = "";
         cmbContagem_Lock_Jsonclick = "";
         edtContagem_QtdItensAprovados_Jsonclick = "";
         edtContagem_QtdItens_Jsonclick = "";
         edtContagem_ProjetoSigla_Jsonclick = "";
         edtContagem_SistemaCoord_Jsonclick = "";
         edtContagem_SistemaSigla_Jsonclick = "";
         edtContagem_SistemaCod_Jsonclick = "";
         edtContagem_Deflator_Jsonclick = "";
         edtContagem_Fator_Jsonclick = "";
         edtContagem_Demanda_Jsonclick = "";
         cmbContagem_Status_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         edtContagem_Divergencia_Jsonclick = "";
         edtContagem_PFL_Jsonclick = "";
         edtContagem_PFB_Jsonclick = "";
         Contagem_observacao_Enabled = Convert.ToBoolean( 0);
         Contagem_fronteira_Enabled = Convert.ToBoolean( 0);
         Contagem_escopo_Enabled = Convert.ToBoolean( 0);
         Contagem_proposito_Enabled = Convert.ToBoolean( 0);
         cmbContagem_Tipo_Jsonclick = "";
         cmbContagem_Tecnica_Jsonclick = "";
         edtContagem_AreaTrabalhoDes_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_ContratadaCod_Jsonclick = "";
         edtContagem_AreaTrabalhoCod_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtContagem_UsuarioContadorPessoaNom_Link = "";
         edtContagem_AreaTrabalhoDes_Link = "";
         edtContagem_ProjetoCod_Jsonclick = "";
         edtContagem_ProjetoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13I32',iparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14I32',iparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         scmdbuf = "";
         H00I33_A265Contagem_QtdItensAprovados = new short[1] ;
         H00I33_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         H00I35_A264Contagem_QtdItens = new short[1] ;
         H00I35_n264Contagem_QtdItens = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A199Contagem_Proposito = "";
         A200Contagem_Escopo = "";
         A201Contagem_Fronteira = "";
         A202Contagem_Observacao = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A1059Contagem_Notas = "";
         A262Contagem_Status = "";
         A945Contagem_Demanda = "";
         A946Contagem_Link = "";
         A1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         A1112Contagem_Consideracoes = "";
         A1120Contagem_Descricao = "";
         A1809Contagem_Aplicabilidade = "";
         A1810Contagem_Versao = "";
         A1812Contagem_ArquivoImp = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00I38_A192Contagem_Codigo = new int[1] ;
         H00I38_A939Contagem_ProjetoCod = new int[1] ;
         H00I38_n939Contagem_ProjetoCod = new bool[] {false} ;
         H00I38_A1814Contagem_AmbienteTecnologico = new int[1] ;
         H00I38_n1814Contagem_AmbienteTecnologico = new bool[] {false} ;
         H00I38_A1813Contagem_ReferenciaINM = new int[1] ;
         H00I38_n1813Contagem_ReferenciaINM = new bool[] {false} ;
         H00I38_A1812Contagem_ArquivoImp = new String[] {""} ;
         H00I38_n1812Contagem_ArquivoImp = new bool[] {false} ;
         H00I38_A1811Contagem_ServicoCod = new int[1] ;
         H00I38_n1811Contagem_ServicoCod = new bool[] {false} ;
         H00I38_A1810Contagem_Versao = new String[] {""} ;
         H00I38_n1810Contagem_Versao = new bool[] {false} ;
         H00I38_A1809Contagem_Aplicabilidade = new String[] {""} ;
         H00I38_n1809Contagem_Aplicabilidade = new bool[] {false} ;
         H00I38_A1116Contagem_PFLD = new decimal[1] ;
         H00I38_n1116Contagem_PFLD = new bool[] {false} ;
         H00I38_A1115Contagem_PFBD = new decimal[1] ;
         H00I38_n1115Contagem_PFBD = new bool[] {false} ;
         H00I38_A1114Contagem_PFLA = new decimal[1] ;
         H00I38_n1114Contagem_PFLA = new bool[] {false} ;
         H00I38_A1113Contagem_PFBA = new decimal[1] ;
         H00I38_n1113Contagem_PFBA = new bool[] {false} ;
         H00I38_A1120Contagem_Descricao = new String[] {""} ;
         H00I38_n1120Contagem_Descricao = new bool[] {false} ;
         H00I38_A1112Contagem_Consideracoes = new String[] {""} ;
         H00I38_n1112Contagem_Consideracoes = new bool[] {false} ;
         H00I38_A1111Contagem_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         H00I38_n1111Contagem_DataHomologacao = new bool[] {false} ;
         H00I38_A948Contagem_Lock = new bool[] {false} ;
         H00I38_A942Contagem_ProjetoSigla = new String[] {""} ;
         H00I38_n942Contagem_ProjetoSigla = new bool[] {false} ;
         H00I38_A949Contagem_SistemaCoord = new String[] {""} ;
         H00I38_n949Contagem_SistemaCoord = new bool[] {false} ;
         H00I38_A941Contagem_SistemaSigla = new String[] {""} ;
         H00I38_n941Contagem_SistemaSigla = new bool[] {false} ;
         H00I38_A940Contagem_SistemaCod = new int[1] ;
         H00I38_n940Contagem_SistemaCod = new bool[] {false} ;
         H00I38_A1117Contagem_Deflator = new decimal[1] ;
         H00I38_n1117Contagem_Deflator = new bool[] {false} ;
         H00I38_A947Contagem_Fator = new decimal[1] ;
         H00I38_n947Contagem_Fator = new bool[] {false} ;
         H00I38_A946Contagem_Link = new String[] {""} ;
         H00I38_n946Contagem_Link = new bool[] {false} ;
         H00I38_A945Contagem_Demanda = new String[] {""} ;
         H00I38_n945Contagem_Demanda = new bool[] {false} ;
         H00I38_A262Contagem_Status = new String[] {""} ;
         H00I38_n262Contagem_Status = new bool[] {false} ;
         H00I38_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         H00I38_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         H00I38_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         H00I38_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         H00I38_A213Contagem_UsuarioContadorCod = new int[1] ;
         H00I38_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         H00I38_A1119Contagem_Divergencia = new decimal[1] ;
         H00I38_n1119Contagem_Divergencia = new bool[] {false} ;
         H00I38_A944Contagem_PFL = new decimal[1] ;
         H00I38_n944Contagem_PFL = new bool[] {false} ;
         H00I38_A943Contagem_PFB = new decimal[1] ;
         H00I38_n943Contagem_PFB = new bool[] {false} ;
         H00I38_A1059Contagem_Notas = new String[] {""} ;
         H00I38_n1059Contagem_Notas = new bool[] {false} ;
         H00I38_A202Contagem_Observacao = new String[] {""} ;
         H00I38_n202Contagem_Observacao = new bool[] {false} ;
         H00I38_A201Contagem_Fronteira = new String[] {""} ;
         H00I38_n201Contagem_Fronteira = new bool[] {false} ;
         H00I38_A200Contagem_Escopo = new String[] {""} ;
         H00I38_n200Contagem_Escopo = new bool[] {false} ;
         H00I38_A199Contagem_Proposito = new String[] {""} ;
         H00I38_n199Contagem_Proposito = new bool[] {false} ;
         H00I38_A196Contagem_Tipo = new String[] {""} ;
         H00I38_n196Contagem_Tipo = new bool[] {false} ;
         H00I38_A195Contagem_Tecnica = new String[] {""} ;
         H00I38_n195Contagem_Tecnica = new bool[] {false} ;
         H00I38_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         H00I38_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         H00I38_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00I38_A1118Contagem_ContratadaCod = new int[1] ;
         H00I38_n1118Contagem_ContratadaCod = new bool[] {false} ;
         H00I38_A193Contagem_AreaTrabalhoCod = new int[1] ;
         H00I38_A265Contagem_QtdItensAprovados = new short[1] ;
         H00I38_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         H00I38_A264Contagem_QtdItens = new short[1] ;
         H00I38_n264Contagem_QtdItens = new bool[] {false} ;
         A942Contagem_ProjetoSigla = "";
         A949Contagem_SistemaCoord = "";
         A941Contagem_SistemaSigla = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         A194Contagem_AreaTrabalhoDes = "";
         H00I310_A265Contagem_QtdItensAprovados = new short[1] ;
         H00I310_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         H00I312_A264Contagem_QtdItens = new short[1] ;
         H00I312_n264Contagem_QtdItens = new bool[] {false} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontagem_codigo_Jsonclick = "";
         lblTextblockcontagem_areatrabalhocod_Jsonclick = "";
         lblTextblockcontagem_contratadacod_Jsonclick = "";
         lblTextblockcontagem_datacriacao_Jsonclick = "";
         lblTextblockcontagem_areatrabalhodes_Jsonclick = "";
         lblTextblockcontagem_tecnica_Jsonclick = "";
         lblTextblockcontagem_tipo_Jsonclick = "";
         lblTextblockcontagem_proposito_Jsonclick = "";
         lblTextblockcontagem_escopo_Jsonclick = "";
         lblTextblockcontagem_fronteira_Jsonclick = "";
         lblTextblockcontagem_observacao_Jsonclick = "";
         lblTextblockcontagem_notas_Jsonclick = "";
         lblTextblockcontagem_pfb_Jsonclick = "";
         lblTextblockcontagem_pfl_Jsonclick = "";
         lblTextblockcontagem_divergencia_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorcod_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorpessoacod_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick = "";
         lblTextblockcontagem_status_Jsonclick = "";
         lblTextblockcontagem_demanda_Jsonclick = "";
         lblTextblockcontagem_link_Jsonclick = "";
         lblTextblockcontagem_fator_Jsonclick = "";
         lblTextblockcontagem_deflator_Jsonclick = "";
         lblTextblockcontagem_sistemacod_Jsonclick = "";
         lblTextblockcontagem_sistemasigla_Jsonclick = "";
         lblTextblockcontagem_sistemacoord_Jsonclick = "";
         lblTextblockcontagem_projetosigla_Jsonclick = "";
         lblTextblockcontagem_qtditens_Jsonclick = "";
         lblTextblockcontagem_qtditensaprovados_Jsonclick = "";
         lblTextblockcontagem_lock_Jsonclick = "";
         lblTextblockcontagem_datahomologacao_Jsonclick = "";
         lblTextblockcontagem_consideracoes_Jsonclick = "";
         lblTextblockcontagem_descricao_Jsonclick = "";
         lblTextblockcontagem_pfba_Jsonclick = "";
         lblTextblockcontagem_pfla_Jsonclick = "";
         lblTextblockcontagem_pfbd_Jsonclick = "";
         lblTextblockcontagem_pfld_Jsonclick = "";
         lblTextblockcontagem_aplicabilidade_Jsonclick = "";
         lblTextblockcontagem_versao_Jsonclick = "";
         lblTextblockcontagem_servicocod_Jsonclick = "";
         lblTextblockcontagem_arquivoimp_Jsonclick = "";
         lblTextblockcontagem_referenciainm_Jsonclick = "";
         lblTextblockcontagem_ambientetecnologico_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA192Contagem_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemgeneral__default(),
            new Object[][] {
                new Object[] {
               H00I33_A265Contagem_QtdItensAprovados, H00I33_n265Contagem_QtdItensAprovados
               }
               , new Object[] {
               H00I35_A264Contagem_QtdItens, H00I35_n264Contagem_QtdItens
               }
               , new Object[] {
               H00I38_A192Contagem_Codigo, H00I38_A939Contagem_ProjetoCod, H00I38_n939Contagem_ProjetoCod, H00I38_A1814Contagem_AmbienteTecnologico, H00I38_n1814Contagem_AmbienteTecnologico, H00I38_A1813Contagem_ReferenciaINM, H00I38_n1813Contagem_ReferenciaINM, H00I38_A1812Contagem_ArquivoImp, H00I38_n1812Contagem_ArquivoImp, H00I38_A1811Contagem_ServicoCod,
               H00I38_n1811Contagem_ServicoCod, H00I38_A1810Contagem_Versao, H00I38_n1810Contagem_Versao, H00I38_A1809Contagem_Aplicabilidade, H00I38_n1809Contagem_Aplicabilidade, H00I38_A1116Contagem_PFLD, H00I38_n1116Contagem_PFLD, H00I38_A1115Contagem_PFBD, H00I38_n1115Contagem_PFBD, H00I38_A1114Contagem_PFLA,
               H00I38_n1114Contagem_PFLA, H00I38_A1113Contagem_PFBA, H00I38_n1113Contagem_PFBA, H00I38_A1120Contagem_Descricao, H00I38_n1120Contagem_Descricao, H00I38_A1112Contagem_Consideracoes, H00I38_n1112Contagem_Consideracoes, H00I38_A1111Contagem_DataHomologacao, H00I38_n1111Contagem_DataHomologacao, H00I38_A948Contagem_Lock,
               H00I38_A942Contagem_ProjetoSigla, H00I38_n942Contagem_ProjetoSigla, H00I38_A949Contagem_SistemaCoord, H00I38_n949Contagem_SistemaCoord, H00I38_A941Contagem_SistemaSigla, H00I38_n941Contagem_SistemaSigla, H00I38_A940Contagem_SistemaCod, H00I38_n940Contagem_SistemaCod, H00I38_A1117Contagem_Deflator, H00I38_n1117Contagem_Deflator,
               H00I38_A947Contagem_Fator, H00I38_n947Contagem_Fator, H00I38_A946Contagem_Link, H00I38_n946Contagem_Link, H00I38_A945Contagem_Demanda, H00I38_n945Contagem_Demanda, H00I38_A262Contagem_Status, H00I38_n262Contagem_Status, H00I38_A215Contagem_UsuarioContadorPessoaNom, H00I38_n215Contagem_UsuarioContadorPessoaNom,
               H00I38_A214Contagem_UsuarioContadorPessoaCod, H00I38_n214Contagem_UsuarioContadorPessoaCod, H00I38_A213Contagem_UsuarioContadorCod, H00I38_n213Contagem_UsuarioContadorCod, H00I38_A1119Contagem_Divergencia, H00I38_n1119Contagem_Divergencia, H00I38_A944Contagem_PFL, H00I38_n944Contagem_PFL, H00I38_A943Contagem_PFB, H00I38_n943Contagem_PFB,
               H00I38_A1059Contagem_Notas, H00I38_n1059Contagem_Notas, H00I38_A202Contagem_Observacao, H00I38_n202Contagem_Observacao, H00I38_A201Contagem_Fronteira, H00I38_n201Contagem_Fronteira, H00I38_A200Contagem_Escopo, H00I38_n200Contagem_Escopo, H00I38_A199Contagem_Proposito, H00I38_n199Contagem_Proposito,
               H00I38_A196Contagem_Tipo, H00I38_n196Contagem_Tipo, H00I38_A195Contagem_Tecnica, H00I38_n195Contagem_Tecnica, H00I38_A194Contagem_AreaTrabalhoDes, H00I38_n194Contagem_AreaTrabalhoDes, H00I38_A197Contagem_DataCriacao, H00I38_A1118Contagem_ContratadaCod, H00I38_n1118Contagem_ContratadaCod, H00I38_A193Contagem_AreaTrabalhoCod,
               H00I38_A265Contagem_QtdItensAprovados, H00I38_n265Contagem_QtdItensAprovados, H00I38_A264Contagem_QtdItens, H00I38_n264Contagem_QtdItens
               }
               , new Object[] {
               H00I310_A265Contagem_QtdItensAprovados, H00I310_n265Contagem_QtdItensAprovados
               }
               , new Object[] {
               H00I312_A264Contagem_QtdItens, H00I312_n264Contagem_QtdItens
               }
            }
         );
         AV14Pgmname = "ContagemGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContagemGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A265Contagem_QtdItensAprovados ;
      private short A264Contagem_QtdItens ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A192Contagem_Codigo ;
      private int wcpOA192Contagem_Codigo ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A1118Contagem_ContratadaCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A940Contagem_SistemaCod ;
      private int A1811Contagem_ServicoCod ;
      private int A1813Contagem_ReferenciaINM ;
      private int A1814Contagem_AmbienteTecnologico ;
      private int A939Contagem_ProjetoCod ;
      private int edtContagem_ProjetoCod_Visible ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Contagem_Codigo ;
      private int idxLst ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private decimal A1119Contagem_Divergencia ;
      private decimal A947Contagem_Fator ;
      private decimal A1117Contagem_Deflator ;
      private decimal A1113Contagem_PFBA ;
      private decimal A1114Contagem_PFLA ;
      private decimal A1115Contagem_PFBD ;
      private decimal A1116Contagem_PFLD ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A195Contagem_Tecnica ;
      private String A196Contagem_Tipo ;
      private String A262Contagem_Status ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContagem_ProjetoCod_Internalname ;
      private String edtContagem_ProjetoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A942Contagem_ProjetoSigla ;
      private String A941Contagem_SistemaSigla ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String edtContagem_AreaTrabalhoCod_Internalname ;
      private String edtContagem_ContratadaCod_Internalname ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_AreaTrabalhoDes_Internalname ;
      private String cmbContagem_Tecnica_Internalname ;
      private String cmbContagem_Tipo_Internalname ;
      private String edtContagem_Notas_Internalname ;
      private String edtContagem_PFB_Internalname ;
      private String edtContagem_PFL_Internalname ;
      private String edtContagem_Divergencia_Internalname ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaNom_Internalname ;
      private String cmbContagem_Status_Internalname ;
      private String edtContagem_Demanda_Internalname ;
      private String edtContagem_Link_Internalname ;
      private String edtContagem_Fator_Internalname ;
      private String edtContagem_Deflator_Internalname ;
      private String edtContagem_SistemaCod_Internalname ;
      private String edtContagem_SistemaSigla_Internalname ;
      private String edtContagem_SistemaCoord_Internalname ;
      private String edtContagem_ProjetoSigla_Internalname ;
      private String edtContagem_QtdItens_Internalname ;
      private String edtContagem_QtdItensAprovados_Internalname ;
      private String cmbContagem_Lock_Internalname ;
      private String edtContagem_DataHomologacao_Internalname ;
      private String edtContagem_Consideracoes_Internalname ;
      private String edtContagem_Descricao_Internalname ;
      private String edtContagem_PFBA_Internalname ;
      private String edtContagem_PFLA_Internalname ;
      private String edtContagem_PFBD_Internalname ;
      private String edtContagem_PFLD_Internalname ;
      private String edtContagem_Aplicabilidade_Internalname ;
      private String edtContagem_Versao_Internalname ;
      private String edtContagem_ServicoCod_Internalname ;
      private String edtContagem_ArquivoImp_Internalname ;
      private String edtContagem_ReferenciaINM_Internalname ;
      private String edtContagem_AmbienteTecnologico_Internalname ;
      private String hsh ;
      private String edtContagem_AreaTrabalhoDes_Link ;
      private String edtContagem_UsuarioContadorPessoaNom_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagem_codigo_Internalname ;
      private String lblTextblockcontagem_codigo_Jsonclick ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhocod_Internalname ;
      private String lblTextblockcontagem_areatrabalhocod_Jsonclick ;
      private String edtContagem_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblockcontagem_contratadacod_Internalname ;
      private String lblTextblockcontagem_contratadacod_Jsonclick ;
      private String edtContagem_ContratadaCod_Jsonclick ;
      private String lblTextblockcontagem_datacriacao_Internalname ;
      private String lblTextblockcontagem_datacriacao_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhodes_Internalname ;
      private String lblTextblockcontagem_areatrabalhodes_Jsonclick ;
      private String edtContagem_AreaTrabalhoDes_Jsonclick ;
      private String lblTextblockcontagem_tecnica_Internalname ;
      private String lblTextblockcontagem_tecnica_Jsonclick ;
      private String cmbContagem_Tecnica_Jsonclick ;
      private String lblTextblockcontagem_tipo_Internalname ;
      private String lblTextblockcontagem_tipo_Jsonclick ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String lblTextblockcontagem_proposito_Internalname ;
      private String lblTextblockcontagem_proposito_Jsonclick ;
      private String lblTextblockcontagem_escopo_Internalname ;
      private String lblTextblockcontagem_escopo_Jsonclick ;
      private String lblTextblockcontagem_fronteira_Internalname ;
      private String lblTextblockcontagem_fronteira_Jsonclick ;
      private String lblTextblockcontagem_observacao_Internalname ;
      private String lblTextblockcontagem_observacao_Jsonclick ;
      private String lblTextblockcontagem_notas_Internalname ;
      private String lblTextblockcontagem_notas_Jsonclick ;
      private String lblTextblockcontagem_pfb_Internalname ;
      private String lblTextblockcontagem_pfb_Jsonclick ;
      private String edtContagem_PFB_Jsonclick ;
      private String lblTextblockcontagem_pfl_Internalname ;
      private String lblTextblockcontagem_pfl_Jsonclick ;
      private String edtContagem_PFL_Jsonclick ;
      private String lblTextblockcontagem_divergencia_Internalname ;
      private String lblTextblockcontagem_divergencia_Jsonclick ;
      private String edtContagem_Divergencia_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorcod_Internalname ;
      private String lblTextblockcontagem_usuariocontadorcod_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorpessoacod_Internalname ;
      private String lblTextblockcontagem_usuariocontadorpessoacod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaCod_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Internalname ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaNom_Jsonclick ;
      private String lblTextblockcontagem_status_Internalname ;
      private String lblTextblockcontagem_status_Jsonclick ;
      private String cmbContagem_Status_Jsonclick ;
      private String lblTextblockcontagem_demanda_Internalname ;
      private String lblTextblockcontagem_demanda_Jsonclick ;
      private String edtContagem_Demanda_Jsonclick ;
      private String lblTextblockcontagem_link_Internalname ;
      private String lblTextblockcontagem_link_Jsonclick ;
      private String lblTextblockcontagem_fator_Internalname ;
      private String lblTextblockcontagem_fator_Jsonclick ;
      private String edtContagem_Fator_Jsonclick ;
      private String lblTextblockcontagem_deflator_Internalname ;
      private String lblTextblockcontagem_deflator_Jsonclick ;
      private String edtContagem_Deflator_Jsonclick ;
      private String lblTextblockcontagem_sistemacod_Internalname ;
      private String lblTextblockcontagem_sistemacod_Jsonclick ;
      private String edtContagem_SistemaCod_Jsonclick ;
      private String lblTextblockcontagem_sistemasigla_Internalname ;
      private String lblTextblockcontagem_sistemasigla_Jsonclick ;
      private String edtContagem_SistemaSigla_Jsonclick ;
      private String lblTextblockcontagem_sistemacoord_Internalname ;
      private String lblTextblockcontagem_sistemacoord_Jsonclick ;
      private String edtContagem_SistemaCoord_Jsonclick ;
      private String lblTextblockcontagem_projetosigla_Internalname ;
      private String lblTextblockcontagem_projetosigla_Jsonclick ;
      private String edtContagem_ProjetoSigla_Jsonclick ;
      private String lblTextblockcontagem_qtditens_Internalname ;
      private String lblTextblockcontagem_qtditens_Jsonclick ;
      private String edtContagem_QtdItens_Jsonclick ;
      private String lblTextblockcontagem_qtditensaprovados_Internalname ;
      private String lblTextblockcontagem_qtditensaprovados_Jsonclick ;
      private String edtContagem_QtdItensAprovados_Jsonclick ;
      private String lblTextblockcontagem_lock_Internalname ;
      private String lblTextblockcontagem_lock_Jsonclick ;
      private String cmbContagem_Lock_Jsonclick ;
      private String lblTextblockcontagem_datahomologacao_Internalname ;
      private String lblTextblockcontagem_datahomologacao_Jsonclick ;
      private String edtContagem_DataHomologacao_Jsonclick ;
      private String lblTextblockcontagem_consideracoes_Internalname ;
      private String lblTextblockcontagem_consideracoes_Jsonclick ;
      private String lblTextblockcontagem_descricao_Internalname ;
      private String lblTextblockcontagem_descricao_Jsonclick ;
      private String lblTextblockcontagem_pfba_Internalname ;
      private String lblTextblockcontagem_pfba_Jsonclick ;
      private String edtContagem_PFBA_Jsonclick ;
      private String lblTextblockcontagem_pfla_Internalname ;
      private String lblTextblockcontagem_pfla_Jsonclick ;
      private String edtContagem_PFLA_Jsonclick ;
      private String lblTextblockcontagem_pfbd_Internalname ;
      private String lblTextblockcontagem_pfbd_Jsonclick ;
      private String edtContagem_PFBD_Jsonclick ;
      private String lblTextblockcontagem_pfld_Internalname ;
      private String lblTextblockcontagem_pfld_Jsonclick ;
      private String edtContagem_PFLD_Jsonclick ;
      private String lblTextblockcontagem_aplicabilidade_Internalname ;
      private String lblTextblockcontagem_aplicabilidade_Jsonclick ;
      private String lblTextblockcontagem_versao_Internalname ;
      private String lblTextblockcontagem_versao_Jsonclick ;
      private String edtContagem_Versao_Jsonclick ;
      private String lblTextblockcontagem_servicocod_Internalname ;
      private String lblTextblockcontagem_servicocod_Jsonclick ;
      private String edtContagem_ServicoCod_Jsonclick ;
      private String lblTextblockcontagem_arquivoimp_Internalname ;
      private String lblTextblockcontagem_arquivoimp_Jsonclick ;
      private String lblTextblockcontagem_referenciainm_Internalname ;
      private String lblTextblockcontagem_referenciainm_Jsonclick ;
      private String edtContagem_ReferenciaINM_Jsonclick ;
      private String lblTextblockcontagem_ambientetecnologico_Internalname ;
      private String lblTextblockcontagem_ambientetecnologico_Jsonclick ;
      private String edtContagem_AmbienteTecnologico_Jsonclick ;
      private String sCtrlA192Contagem_Codigo ;
      private String Contagem_proposito_Internalname ;
      private String Contagem_escopo_Internalname ;
      private String Contagem_fronteira_Internalname ;
      private String Contagem_observacao_Internalname ;
      private DateTime A1111Contagem_DataHomologacao ;
      private DateTime A197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool n265Contagem_QtdItensAprovados ;
      private bool n264Contagem_QtdItens ;
      private bool toggleJsOutput ;
      private bool A948Contagem_Lock ;
      private bool Contagem_proposito_Enabled ;
      private bool Contagem_escopo_Enabled ;
      private bool Contagem_fronteira_Enabled ;
      private bool Contagem_observacao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n262Contagem_Status ;
      private bool n939Contagem_ProjetoCod ;
      private bool n1814Contagem_AmbienteTecnologico ;
      private bool n1813Contagem_ReferenciaINM ;
      private bool n1812Contagem_ArquivoImp ;
      private bool n1811Contagem_ServicoCod ;
      private bool n1810Contagem_Versao ;
      private bool n1809Contagem_Aplicabilidade ;
      private bool n1116Contagem_PFLD ;
      private bool n1115Contagem_PFBD ;
      private bool n1114Contagem_PFLA ;
      private bool n1113Contagem_PFBA ;
      private bool n1120Contagem_Descricao ;
      private bool n1112Contagem_Consideracoes ;
      private bool n1111Contagem_DataHomologacao ;
      private bool n942Contagem_ProjetoSigla ;
      private bool n949Contagem_SistemaCoord ;
      private bool n941Contagem_SistemaSigla ;
      private bool n940Contagem_SistemaCod ;
      private bool n1117Contagem_Deflator ;
      private bool n947Contagem_Fator ;
      private bool n946Contagem_Link ;
      private bool n945Contagem_Demanda ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n1119Contagem_Divergencia ;
      private bool n944Contagem_PFL ;
      private bool n943Contagem_PFB ;
      private bool n1059Contagem_Notas ;
      private bool n202Contagem_Observacao ;
      private bool n201Contagem_Fronteira ;
      private bool n200Contagem_Escopo ;
      private bool n199Contagem_Proposito ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n1118Contagem_ContratadaCod ;
      private bool returnInSub ;
      private String A199Contagem_Proposito ;
      private String A200Contagem_Escopo ;
      private String A201Contagem_Fronteira ;
      private String A202Contagem_Observacao ;
      private String A1059Contagem_Notas ;
      private String A1112Contagem_Consideracoes ;
      private String A1809Contagem_Aplicabilidade ;
      private String A945Contagem_Demanda ;
      private String A946Contagem_Link ;
      private String A1120Contagem_Descricao ;
      private String A1810Contagem_Versao ;
      private String A1812Contagem_ArquivoImp ;
      private String A949Contagem_SistemaCoord ;
      private String A194Contagem_AreaTrabalhoDes ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagem_Tecnica ;
      private GXCombobox cmbContagem_Tipo ;
      private GXCombobox cmbContagem_Status ;
      private GXCombobox cmbContagem_Lock ;
      private IDataStoreProvider pr_default ;
      private short[] H00I33_A265Contagem_QtdItensAprovados ;
      private bool[] H00I33_n265Contagem_QtdItensAprovados ;
      private short[] H00I35_A264Contagem_QtdItens ;
      private bool[] H00I35_n264Contagem_QtdItens ;
      private int[] H00I38_A192Contagem_Codigo ;
      private int[] H00I38_A939Contagem_ProjetoCod ;
      private bool[] H00I38_n939Contagem_ProjetoCod ;
      private int[] H00I38_A1814Contagem_AmbienteTecnologico ;
      private bool[] H00I38_n1814Contagem_AmbienteTecnologico ;
      private int[] H00I38_A1813Contagem_ReferenciaINM ;
      private bool[] H00I38_n1813Contagem_ReferenciaINM ;
      private String[] H00I38_A1812Contagem_ArquivoImp ;
      private bool[] H00I38_n1812Contagem_ArquivoImp ;
      private int[] H00I38_A1811Contagem_ServicoCod ;
      private bool[] H00I38_n1811Contagem_ServicoCod ;
      private String[] H00I38_A1810Contagem_Versao ;
      private bool[] H00I38_n1810Contagem_Versao ;
      private String[] H00I38_A1809Contagem_Aplicabilidade ;
      private bool[] H00I38_n1809Contagem_Aplicabilidade ;
      private decimal[] H00I38_A1116Contagem_PFLD ;
      private bool[] H00I38_n1116Contagem_PFLD ;
      private decimal[] H00I38_A1115Contagem_PFBD ;
      private bool[] H00I38_n1115Contagem_PFBD ;
      private decimal[] H00I38_A1114Contagem_PFLA ;
      private bool[] H00I38_n1114Contagem_PFLA ;
      private decimal[] H00I38_A1113Contagem_PFBA ;
      private bool[] H00I38_n1113Contagem_PFBA ;
      private String[] H00I38_A1120Contagem_Descricao ;
      private bool[] H00I38_n1120Contagem_Descricao ;
      private String[] H00I38_A1112Contagem_Consideracoes ;
      private bool[] H00I38_n1112Contagem_Consideracoes ;
      private DateTime[] H00I38_A1111Contagem_DataHomologacao ;
      private bool[] H00I38_n1111Contagem_DataHomologacao ;
      private bool[] H00I38_A948Contagem_Lock ;
      private String[] H00I38_A942Contagem_ProjetoSigla ;
      private bool[] H00I38_n942Contagem_ProjetoSigla ;
      private String[] H00I38_A949Contagem_SistemaCoord ;
      private bool[] H00I38_n949Contagem_SistemaCoord ;
      private String[] H00I38_A941Contagem_SistemaSigla ;
      private bool[] H00I38_n941Contagem_SistemaSigla ;
      private int[] H00I38_A940Contagem_SistemaCod ;
      private bool[] H00I38_n940Contagem_SistemaCod ;
      private decimal[] H00I38_A1117Contagem_Deflator ;
      private bool[] H00I38_n1117Contagem_Deflator ;
      private decimal[] H00I38_A947Contagem_Fator ;
      private bool[] H00I38_n947Contagem_Fator ;
      private String[] H00I38_A946Contagem_Link ;
      private bool[] H00I38_n946Contagem_Link ;
      private String[] H00I38_A945Contagem_Demanda ;
      private bool[] H00I38_n945Contagem_Demanda ;
      private String[] H00I38_A262Contagem_Status ;
      private bool[] H00I38_n262Contagem_Status ;
      private String[] H00I38_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] H00I38_n215Contagem_UsuarioContadorPessoaNom ;
      private int[] H00I38_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] H00I38_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] H00I38_A213Contagem_UsuarioContadorCod ;
      private bool[] H00I38_n213Contagem_UsuarioContadorCod ;
      private decimal[] H00I38_A1119Contagem_Divergencia ;
      private bool[] H00I38_n1119Contagem_Divergencia ;
      private decimal[] H00I38_A944Contagem_PFL ;
      private bool[] H00I38_n944Contagem_PFL ;
      private decimal[] H00I38_A943Contagem_PFB ;
      private bool[] H00I38_n943Contagem_PFB ;
      private String[] H00I38_A1059Contagem_Notas ;
      private bool[] H00I38_n1059Contagem_Notas ;
      private String[] H00I38_A202Contagem_Observacao ;
      private bool[] H00I38_n202Contagem_Observacao ;
      private String[] H00I38_A201Contagem_Fronteira ;
      private bool[] H00I38_n201Contagem_Fronteira ;
      private String[] H00I38_A200Contagem_Escopo ;
      private bool[] H00I38_n200Contagem_Escopo ;
      private String[] H00I38_A199Contagem_Proposito ;
      private bool[] H00I38_n199Contagem_Proposito ;
      private String[] H00I38_A196Contagem_Tipo ;
      private bool[] H00I38_n196Contagem_Tipo ;
      private String[] H00I38_A195Contagem_Tecnica ;
      private bool[] H00I38_n195Contagem_Tecnica ;
      private String[] H00I38_A194Contagem_AreaTrabalhoDes ;
      private bool[] H00I38_n194Contagem_AreaTrabalhoDes ;
      private DateTime[] H00I38_A197Contagem_DataCriacao ;
      private int[] H00I38_A1118Contagem_ContratadaCod ;
      private bool[] H00I38_n1118Contagem_ContratadaCod ;
      private int[] H00I38_A193Contagem_AreaTrabalhoCod ;
      private short[] H00I38_A265Contagem_QtdItensAprovados ;
      private bool[] H00I38_n265Contagem_QtdItensAprovados ;
      private short[] H00I38_A264Contagem_QtdItens ;
      private bool[] H00I38_n264Contagem_QtdItens ;
      private short[] H00I310_A265Contagem_QtdItensAprovados ;
      private bool[] H00I310_n265Contagem_QtdItensAprovados ;
      private short[] H00I312_A264Contagem_QtdItens ;
      private bool[] H00I312_n264Contagem_QtdItens ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contagemgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00I33 ;
          prmH00I33 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00I35 ;
          prmH00I35 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00I38 ;
          prmH00I38 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00I38 ;
          cmdBufferH00I38=" SELECT T1.[Contagem_Codigo], T1.[Contagem_ProjetoCod] AS Contagem_ProjetoCod, T1.[Contagem_AmbienteTecnologico], T1.[Contagem_ReferenciaINM], T1.[Contagem_ArquivoImp], T1.[Contagem_ServicoCod], T1.[Contagem_Versao], T1.[Contagem_Aplicabilidade], T1.[Contagem_PFLD], T1.[Contagem_PFBD], T1.[Contagem_PFLA], T1.[Contagem_PFBA], T1.[Contagem_Descricao], T1.[Contagem_Consideracoes], T1.[Contagem_DataHomologacao], T1.[Contagem_Lock], T2.[Projeto_Sigla] AS Contagem_ProjetoSigla, T3.[Sistema_Coordenacao] AS Contagem_SistemaCoord, T3.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Demanda], T1.[Contagem_Status], T5.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T4.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Observacao], T1.[Contagem_Fronteira], T1.[Contagem_Escopo], T1.[Contagem_Proposito], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T6.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_DataCriacao], T1.[Contagem_ContratadaCod], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, COALESCE( T7.[Contagem_QtdItensAprovados], 0) AS Contagem_QtdItensAprovados, COALESCE( T8.[Contagem_QtdItensAprovados], 0) AS Contagem_QtdItens FROM ((((((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Projeto] T2 WITH (NOLOCK) ON T2.[Projeto_Codigo] = T1.[Contagem_ProjetoCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) "
          + " ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T6 WITH (NOLOCK) ON T6.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod]) LEFT JOIN (SELECT COUNT(*) AS Contagem_QtdItensAprovados, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T7 ON T7.[Contagem_Codigo] = T1.[Contagem_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Contagem_QtdItensAprovados, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T8 ON T8.[Contagem_Codigo] = T1.[Contagem_Codigo]) WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ORDER BY T1.[Contagem_Codigo]" ;
          Object[] prmH00I310 ;
          prmH00I310 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00I312 ;
          prmH00I312 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00I33", "SELECT COALESCE( T1.[Contagem_QtdItensAprovados], 0) AS Contagem_QtdItensAprovados FROM (SELECT COUNT(*) AS Contagem_QtdItensAprovados, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I33,1,0,true,true )
             ,new CursorDef("H00I35", "SELECT COALESCE( T1.[Contagem_QtdItensAprovados], 0) AS Contagem_QtdItens FROM (SELECT COUNT(*) AS Contagem_QtdItensAprovados, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I35,1,0,true,true )
             ,new CursorDef("H00I38", cmdBufferH00I38,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I38,1,0,true,true )
             ,new CursorDef("H00I310", "SELECT COALESCE( T1.[Contagem_QtdItensAprovados], 0) AS Contagem_QtdItensAprovados FROM (SELECT COUNT(*) AS Contagem_QtdItensAprovados, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I310,1,0,true,true )
             ,new CursorDef("H00I312", "SELECT COALESCE( T1.[Contagem_QtdItensAprovados], 0) AS Contagem_QtdItens FROM (SELECT COUNT(*) AS Contagem_QtdItensAprovados, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00I312,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[27])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((bool[]) buf[29])[0] = rslt.getBool(16) ;
                ((String[]) buf[30])[0] = rslt.getString(17, 15) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getString(19, 25) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((decimal[]) buf[38])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((String[]) buf[42])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((String[]) buf[44])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((String[]) buf[46])[0] = rslt.getString(25, 1) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((String[]) buf[48])[0] = rslt.getString(26, 100) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((int[]) buf[50])[0] = rslt.getInt(27) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(27);
                ((int[]) buf[52])[0] = rslt.getInt(28) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(28);
                ((decimal[]) buf[54])[0] = rslt.getDecimal(29) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(29);
                ((decimal[]) buf[56])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(30);
                ((decimal[]) buf[58])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(31);
                ((String[]) buf[60])[0] = rslt.getLongVarchar(32) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(32);
                ((String[]) buf[62])[0] = rslt.getLongVarchar(33) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(33);
                ((String[]) buf[64])[0] = rslt.getLongVarchar(34) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(34);
                ((String[]) buf[66])[0] = rslt.getLongVarchar(35) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(35);
                ((String[]) buf[68])[0] = rslt.getLongVarchar(36) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(36);
                ((String[]) buf[70])[0] = rslt.getString(37, 1) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(37);
                ((String[]) buf[72])[0] = rslt.getString(38, 1) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(38);
                ((String[]) buf[74])[0] = rslt.getVarchar(39) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(39);
                ((DateTime[]) buf[76])[0] = rslt.getGXDate(40) ;
                ((int[]) buf[77])[0] = rslt.getInt(41) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(41);
                ((int[]) buf[79])[0] = rslt.getInt(42) ;
                ((short[]) buf[80])[0] = rslt.getShort(43) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(43);
                ((short[]) buf[82])[0] = rslt.getShort(44) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(44);
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
