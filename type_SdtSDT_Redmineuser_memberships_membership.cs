/*
               File: type_SdtSDT_Redmineuser_memberships_membership
        Description: SDT_Redmineuser
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineuser.memberships.membership" )]
   [XmlType(TypeName =  "SDT_Redmineuser.memberships.membership" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineuser_memberships_membership_project ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineuser_memberships_membership_roles ))]
   [Serializable]
   public class SdtSDT_Redmineuser_memberships_membership : GxUserType
   {
      public SdtSDT_Redmineuser_memberships_membership( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_Redmineuser_memberships_membership( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineuser_memberships_membership deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineuser_memberships_membership)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineuser_memberships_membership obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Project = deserialized.gxTpr_Project;
         obj.gxTpr_Roles = deserialized.gxTpr_Roles;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_memberships_membership_Id = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "project") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Project == null )
                  {
                     gxTv_SdtSDT_Redmineuser_memberships_membership_Project = new SdtSDT_Redmineuser_memberships_membership_project(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineuser_memberships_membership_Project.readxml(oReader, "project");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "roles") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Roles == null )
                  {
                     gxTv_SdtSDT_Redmineuser_memberships_membership_Roles = new SdtSDT_Redmineuser_memberships_membership_roles(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineuser_memberships_membership_Roles.readxml(oReader, "roles");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineuser.memberships.membership";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineuser_memberships_membership_Id), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Project != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineuser_memberships_membership_Project.writexml(oWriter, "project", sNameSpace1);
         }
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Roles != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineuser_memberships_membership_Roles.writexml(oWriter, "roles", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineuser_memberships_membership_Id, false);
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Project != null )
         {
            AddObjectProperty("project", gxTv_SdtSDT_Redmineuser_memberships_membership_Project, false);
         }
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Roles != null )
         {
            AddObjectProperty("roles", gxTv_SdtSDT_Redmineuser_memberships_membership_Roles, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public int gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineuser_memberships_membership_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_Id = (int)(value);
         }

      }

      [  SoapElement( ElementName = "project" )]
      [  XmlElement( ElementName = "project"   )]
      public SdtSDT_Redmineuser_memberships_membership_project gxTpr_Project
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Project == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_membership_Project = new SdtSDT_Redmineuser_memberships_membership_project(context);
            }
            return gxTv_SdtSDT_Redmineuser_memberships_membership_Project ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_Project = value;
         }

      }

      public void gxTv_SdtSDT_Redmineuser_memberships_membership_Project_SetNull( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_membership_Project = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineuser_memberships_membership_Project_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Project == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "roles" )]
      [  XmlElement( ElementName = "roles"   )]
      public SdtSDT_Redmineuser_memberships_membership_roles gxTpr_Roles
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Roles == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_membership_Roles = new SdtSDT_Redmineuser_memberships_membership_roles(context);
            }
            return gxTv_SdtSDT_Redmineuser_memberships_membership_Roles ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_Roles = value;
         }

      }

      public void gxTv_SdtSDT_Redmineuser_memberships_membership_Roles_SetNull( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_membership_Roles = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineuser_memberships_membership_Roles_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineuser_memberships_membership_Roles == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Redmineuser_memberships_membership_Id ;
      protected String sTagName ;
      protected SdtSDT_Redmineuser_memberships_membership_project gxTv_SdtSDT_Redmineuser_memberships_membership_Project=null ;
      protected SdtSDT_Redmineuser_memberships_membership_roles gxTv_SdtSDT_Redmineuser_memberships_membership_Roles=null ;
   }

   [DataContract(Name = @"SDT_Redmineuser.memberships.membership", Namespace = "")]
   public class SdtSDT_Redmineuser_memberships_membership_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineuser_memberships_membership>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineuser_memberships_membership_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineuser_memberships_membership_RESTInterface( SdtSDT_Redmineuser_memberships_membership psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Id), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Id = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "project" , Order = 1 )]
      public SdtSDT_Redmineuser_memberships_membership_project_RESTInterface gxTpr_Project
      {
         get {
            return new SdtSDT_Redmineuser_memberships_membership_project_RESTInterface(sdt.gxTpr_Project) ;
         }

         set {
            sdt.gxTpr_Project = value.sdt;
         }

      }

      [DataMember( Name = "roles" , Order = 2 )]
      public SdtSDT_Redmineuser_memberships_membership_roles_RESTInterface gxTpr_Roles
      {
         get {
            return new SdtSDT_Redmineuser_memberships_membership_roles_RESTInterface(sdt.gxTpr_Roles) ;
         }

         set {
            sdt.gxTpr_Roles = value.sdt;
         }

      }

      public SdtSDT_Redmineuser_memberships_membership sdt
      {
         get {
            return (SdtSDT_Redmineuser_memberships_membership)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineuser_memberships_membership() ;
         }
      }

   }

}
