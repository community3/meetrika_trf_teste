/*
               File: AgendaAtendimentoWC
        Description: Agenda Atendimento WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:7.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class agendaatendimentowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public agendaatendimentowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public agendaatendimentowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AgendaAtendimento_CntSrcCod ,
                           DateTime aP1_AgendaAtendimento_Data )
      {
         this.AV7AgendaAtendimento_CntSrcCod = aP0_AgendaAtendimento_CntSrcCod;
         this.AV8AgendaAtendimento_Data = aP1_AgendaAtendimento_Data;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0)));
                  AV8AgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8AgendaAtendimento_Data", context.localUtil.Format(AV8AgendaAtendimento_Data, "99/99/99"));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7AgendaAtendimento_CntSrcCod,(DateTime)AV8AgendaAtendimento_Data});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_11_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV21TFContagemResultado_Demanda = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContagemResultado_Demanda", AV21TFContagemResultado_Demanda);
                  AV22TFContagemResultado_Demanda_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultado_Demanda_Sel", AV22TFContagemResultado_Demanda_Sel);
                  AV25TFContagemResultado_DemandaFM = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContagemResultado_DemandaFM", AV25TFContagemResultado_DemandaFM);
                  AV26TFContagemResultado_DemandaFM_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContagemResultado_DemandaFM_Sel", AV26TFContagemResultado_DemandaFM_Sel);
                  AV29TFAgendaAtendimento_QtdUnd = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5)));
                  AV30TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5)));
                  AV7AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0)));
                  AV8AgendaAtendimento_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8AgendaAtendimento_Data", context.localUtil.Format(AV8AgendaAtendimento_Data, "99/99/99"));
                  AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace);
                  AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
                  AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace", AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace);
                  AV38Pgmname = GetNextPar( );
                  A1209AgendaAtendimento_CodDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1209AgendaAtendimento_CodDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0)));
                  AV19Total = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Total", StringUtil.LTrim( StringUtil.Str( AV19Total, 14, 5)));
                  A1186AgendaAtendimento_QtdUnd = NumberUtil.Val( GetNextPar( ), ".");
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, AV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV38Pgmname, A1209AgendaAtendimento_CodDmn, AV19Total, A1186AgendaAtendimento_QtdUnd, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAIH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV38Pgmname = "AgendaAtendimentoWC";
               context.Gx_err = 0;
               edtavTotal_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
               WSIH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Agenda Atendimento WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621611726");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("agendaatendimentowc.aspx") + "?" + UrlEncode("" +AV7AgendaAtendimento_CntSrcCod) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8AgendaAtendimento_Data))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDA", AV21TFContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL", AV22TFContagemResultado_Demanda_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDAFM", AV25TFContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL", AV26TFContagemResultado_DemandaFM_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAGENDAATENDIMENTO_QTDUND", StringUtil.LTrim( StringUtil.NToC( AV29TFAgendaAtendimento_QtdUnd, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAGENDAATENDIMENTO_QTDUND_TO", StringUtil.LTrim( StringUtil.NToC( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA", AV20ContagemResultado_DemandaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA", AV20ContagemResultado_DemandaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV24ContagemResultado_DemandaFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", AV24ContagemResultado_DemandaFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA", AV28AgendaAtendimento_QtdUndTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA", AV28AgendaAtendimento_QtdUndTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7AgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV8AgendaAtendimento_Data", context.localUtil.DToC( wcpOAV8AgendaAtendimento_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAGENDAATENDIMENTO_CNTSRCCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAGENDAATENDIMENTO_DATA", context.localUtil.DToC( AV8AgendaAtendimento_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV38Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"AGENDAATENDIMENTO_CODDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1209AgendaAtendimento_CodDmn), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Caption", StringUtil.RTrim( Ddo_contagemresultado_demanda_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demanda_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Cls", StringUtil.RTrim( Ddo_contagemresultado_demanda_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demanda_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demanda_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demanda_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demanda_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_demanda_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demanda_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demanda_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demanda_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demanda_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_demandafm_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Caption", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Tooltip", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Cls", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_set", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_set", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Dropdownoptionstype", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includesortasc", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includesortdsc", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Sortedstatus", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includefilter", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filtertype", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filterisrange", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includedatalist", StringUtil.BoolToStr( Ddo_agendaatendimento_qtdund_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Sortasc", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Sortdsc", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Cleanfilter", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterfrom", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterto", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Searchbuttontext", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demanda_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demanda_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demanda_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultado_demandafm_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Activeeventkey", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_get", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_get", StringUtil.RTrim( Ddo_agendaatendimento_qtdund_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIH2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("agendaatendimentowc.js", "?2020621611821");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AgendaAtendimentoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Agenda Atendimento WC" ;
      }

      protected void WBIH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "agendaatendimentowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_IH2( true) ;
         }
         else
         {
            wb_table1_2_IH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_CntSrcCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1183AgendaAtendimento_CntSrcCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_CntSrcCod_Jsonclick, 0, "Attribute", "", "", "", edtAgendaAtendimento_CntSrcCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtAgendaAtendimento_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAgendaAtendimento_Data_Internalname, context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"), context.localUtil.Format( A1184AgendaAtendimento_Data, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAgendaAtendimento_Data_Jsonclick, 0, "Attribute", "", "", "", edtAgendaAtendimento_Data_Visible, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_AgendaAtendimentoWC.htm");
            GxWebStd.gx_bitmap( context, edtAgendaAtendimento_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtAgendaAtendimento_Data_Visible==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_AgendaAtendimentoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demanda_Internalname, AV21TFContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV21TFContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demanda_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demanda_sel_Internalname, AV22TFContagemResultado_Demanda_Sel, StringUtil.RTrim( context.localUtil.Format( AV22TFContagemResultado_Demanda_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demanda_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demanda_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_Internalname, AV25TFContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV25TFContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_demandafm_sel_Internalname, AV26TFContagemResultado_DemandaFM_Sel, StringUtil.RTrim( context.localUtil.Format( AV26TFContagemResultado_DemandaFM_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_demandafm_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_demandafm_sel_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_qtdund_Internalname, StringUtil.LTrim( StringUtil.NToC( AV29TFAgendaAtendimento_QtdUnd, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV29TFAgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_qtdund_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_qtdund_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AgendaAtendimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfagendaatendimento_qtdund_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV30TFAgendaAtendimento_QtdUnd_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfagendaatendimento_qtdund_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfagendaatendimento_qtdund_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AgendaAtendimentoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", 0, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AgendaAtendimentoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", 0, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AgendaAtendimentoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AGENDAATENDIMENTO_QTDUNDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AgendaAtendimentoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTIH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Agenda Atendimento WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPIH0( ) ;
            }
         }
      }

      protected void WSIH2( )
      {
         STARTIH2( ) ;
         EVTIH2( ) ;
      }

      protected void EVTIH2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11IH2 */
                                    E11IH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12IH2 */
                                    E12IH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13IH2 */
                                    E13IH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AGENDAATENDIMENTO_QTDUND.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14IH2 */
                                    E14IH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTotal_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIH0( ) ;
                              }
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                              n457ContagemResultado_Demanda = false;
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              A1186AgendaAtendimento_QtdUnd = context.localUtil.CToN( cgiGet( edtAgendaAtendimento_QtdUnd_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTotal_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15IH2 */
                                          E15IH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTotal_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16IH2 */
                                          E16IH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTotal_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17IH2 */
                                          E17IH2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultado_demanda Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDA"), AV21TFContagemResultado_Demanda) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultado_demanda_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL"), AV22TFContagemResultado_Demanda_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultado_demandafm Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV25TFContagemResultado_DemandaFM) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultado_demandafm_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV26TFContagemResultado_DemandaFM_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfagendaatendimento_qtdund Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAGENDAATENDIMENTO_QTDUND"), ",", ".") != AV29TFAgendaAtendimento_QtdUnd )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfagendaatendimento_qtdund_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAGENDAATENDIMENTO_QTDUND_TO"), ",", ".") != AV30TFAgendaAtendimento_QtdUnd_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTotal_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPIH0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTotal_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEIH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIH2( ) ;
            }
         }
      }

      protected void PAIH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTotal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV21TFContagemResultado_Demanda ,
                                       String AV22TFContagemResultado_Demanda_Sel ,
                                       String AV25TFContagemResultado_DemandaFM ,
                                       String AV26TFContagemResultado_DemandaFM_Sel ,
                                       decimal AV29TFAgendaAtendimento_QtdUnd ,
                                       decimal AV30TFAgendaAtendimento_QtdUnd_To ,
                                       int AV7AgendaAtendimento_CntSrcCod ,
                                       DateTime AV8AgendaAtendimento_Data ,
                                       String AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace ,
                                       String AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ,
                                       String AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace ,
                                       String AV38Pgmname ,
                                       int A1209AgendaAtendimento_CodDmn ,
                                       decimal AV19Total ,
                                       decimal A1186AgendaAtendimento_QtdUnd ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIH2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AGENDAATENDIMENTO_QTDUND", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AGENDAATENDIMENTO_QTDUND", StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV38Pgmname = "AgendaAtendimentoWC";
         context.Gx_err = 0;
         edtavTotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
      }

      protected void RFIH2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E16IH2 */
         E16IH2 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV22TFContagemResultado_Demanda_Sel ,
                                                 AV21TFContagemResultado_Demanda ,
                                                 AV26TFContagemResultado_DemandaFM_Sel ,
                                                 AV25TFContagemResultado_DemandaFM ,
                                                 AV29TFAgendaAtendimento_QtdUnd ,
                                                 AV30TFAgendaAtendimento_QtdUnd_To ,
                                                 A457ContagemResultado_Demanda ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A1186AgendaAtendimento_QtdUnd ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A1183AgendaAtendimento_CntSrcCod ,
                                                 AV7AgendaAtendimento_CntSrcCod ,
                                                 A1184AgendaAtendimento_Data ,
                                                 AV8AgendaAtendimento_Data },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE
                                                 }
            });
            lV21TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV21TFContagemResultado_Demanda), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContagemResultado_Demanda", AV21TFContagemResultado_Demanda);
            lV25TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV25TFContagemResultado_DemandaFM), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContagemResultado_DemandaFM", AV25TFContagemResultado_DemandaFM);
            /* Using cursor H00IH2 */
            pr_default.execute(0, new Object[] {AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, lV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, lV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_11_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1209AgendaAtendimento_CodDmn = H00IH2_A1209AgendaAtendimento_CodDmn[0];
               A1183AgendaAtendimento_CntSrcCod = H00IH2_A1183AgendaAtendimento_CntSrcCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
               A1184AgendaAtendimento_Data = H00IH2_A1184AgendaAtendimento_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
               A1186AgendaAtendimento_QtdUnd = H00IH2_A1186AgendaAtendimento_QtdUnd[0];
               A493ContagemResultado_DemandaFM = H00IH2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00IH2_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00IH2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00IH2_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00IH2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00IH2_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00IH2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00IH2_n457ContagemResultado_Demanda[0];
               /* Execute user event: E17IH2 */
               E17IH2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 11;
            WBIH0( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV22TFContagemResultado_Demanda_Sel ,
                                              AV21TFContagemResultado_Demanda ,
                                              AV26TFContagemResultado_DemandaFM_Sel ,
                                              AV25TFContagemResultado_DemandaFM ,
                                              AV29TFAgendaAtendimento_QtdUnd ,
                                              AV30TFAgendaAtendimento_QtdUnd_To ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A1183AgendaAtendimento_CntSrcCod ,
                                              AV7AgendaAtendimento_CntSrcCod ,
                                              A1184AgendaAtendimento_Data ,
                                              AV8AgendaAtendimento_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE
                                              }
         });
         lV21TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV21TFContagemResultado_Demanda), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContagemResultado_Demanda", AV21TFContagemResultado_Demanda);
         lV25TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV25TFContagemResultado_DemandaFM), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContagemResultado_DemandaFM", AV25TFContagemResultado_DemandaFM);
         /* Using cursor H00IH3 */
         pr_default.execute(1, new Object[] {AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, lV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, lV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To});
         GRID_nRecordCount = H00IH3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, AV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV38Pgmname, A1209AgendaAtendimento_CodDmn, AV19Total, A1186AgendaAtendimento_QtdUnd, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, AV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV38Pgmname, A1209AgendaAtendimento_CodDmn, AV19Total, A1186AgendaAtendimento_QtdUnd, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, AV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV38Pgmname, A1209AgendaAtendimento_CodDmn, AV19Total, A1186AgendaAtendimento_QtdUnd, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, AV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV38Pgmname, A1209AgendaAtendimento_CodDmn, AV19Total, A1186AgendaAtendimento_QtdUnd, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV21TFContagemResultado_Demanda, AV22TFContagemResultado_Demanda_Sel, AV25TFContagemResultado_DemandaFM, AV26TFContagemResultado_DemandaFM_Sel, AV29TFAgendaAtendimento_QtdUnd, AV30TFAgendaAtendimento_QtdUnd_To, AV7AgendaAtendimento_CntSrcCod, AV8AgendaAtendimento_Data, AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, AV38Pgmname, A1209AgendaAtendimento_CodDmn, AV19Total, A1186AgendaAtendimento_QtdUnd, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIH0( )
      {
         /* Before Start, stand alone formulas. */
         AV38Pgmname = "AgendaAtendimentoWC";
         context.Gx_err = 0;
         edtavTotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15IH2 */
         E15IH2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV32DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA"), AV20ContagemResultado_DemandaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA"), AV24ContagemResultado_DemandaFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA"), AV28AgendaAtendimento_QtdUndTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTOTAL");
               GX_FocusControl = edtavTotal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19Total = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Total", StringUtil.LTrim( StringUtil.Str( AV19Total, 14, 5)));
            }
            else
            {
               AV19Total = context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Total", StringUtil.LTrim( StringUtil.Str( AV19Total, 14, 5)));
            }
            A1183AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( edtAgendaAtendimento_CntSrcCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1183AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1183AgendaAtendimento_CntSrcCod), 6, 0)));
            A1184AgendaAtendimento_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAgendaAtendimento_Data_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1184AgendaAtendimento_Data", context.localUtil.Format(A1184AgendaAtendimento_Data, "99/99/99"));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            else
            {
               AV14OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV21TFContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContagemResultado_Demanda", AV21TFContagemResultado_Demanda);
            AV22TFContagemResultado_Demanda_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultado_demanda_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultado_Demanda_Sel", AV22TFContagemResultado_Demanda_Sel);
            AV25TFContagemResultado_DemandaFM = cgiGet( edtavTfcontagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContagemResultado_DemandaFM", AV25TFContagemResultado_DemandaFM);
            AV26TFContagemResultado_DemandaFM_Sel = cgiGet( edtavTfcontagemresultado_demandafm_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContagemResultado_DemandaFM_Sel", AV26TFContagemResultado_DemandaFM_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_QTDUND");
               GX_FocusControl = edtavTfagendaatendimento_qtdund_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFAgendaAtendimento_QtdUnd = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5)));
            }
            else
            {
               AV29TFAgendaAtendimento_QtdUnd = context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAGENDAATENDIMENTO_QTDUND_TO");
               GX_FocusControl = edtavTfagendaatendimento_qtdund_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30TFAgendaAtendimento_QtdUnd_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5)));
            }
            else
            {
               AV30TFAgendaAtendimento_QtdUnd_To = context.localUtil.CToN( cgiGet( edtavTfagendaatendimento_qtdund_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5)));
            }
            AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace);
            AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
            AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = cgiGet( edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace", AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            AV34GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV35GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7AgendaAtendimento_CntSrcCod"), ",", "."));
            wcpOAV8AgendaAtendimento_Data = context.localUtil.CToD( cgiGet( sPrefix+"wcpOAV8AgendaAtendimento_Data"), 0);
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultado_demanda_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Caption");
            Ddo_contagemresultado_demanda_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Tooltip");
            Ddo_contagemresultado_demanda_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Cls");
            Ddo_contagemresultado_demanda_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_set");
            Ddo_contagemresultado_demanda_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_set");
            Ddo_contagemresultado_demanda_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Dropdownoptionstype");
            Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demanda_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includesortasc"));
            Ddo_contagemresultado_demanda_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includesortdsc"));
            Ddo_contagemresultado_demanda_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Sortedstatus");
            Ddo_contagemresultado_demanda_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includefilter"));
            Ddo_contagemresultado_demanda_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filtertype");
            Ddo_contagemresultado_demanda_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filterisrange"));
            Ddo_contagemresultado_demanda_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Includedatalist"));
            Ddo_contagemresultado_demanda_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Datalisttype");
            Ddo_contagemresultado_demanda_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Datalistproc");
            Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demanda_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Sortasc");
            Ddo_contagemresultado_demanda_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Sortdsc");
            Ddo_contagemresultado_demanda_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Loadingdata");
            Ddo_contagemresultado_demanda_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Cleanfilter");
            Ddo_contagemresultado_demanda_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Noresultsfound");
            Ddo_contagemresultado_demanda_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Searchbuttontext");
            Ddo_contagemresultado_demandafm_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Caption");
            Ddo_contagemresultado_demandafm_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Tooltip");
            Ddo_contagemresultado_demandafm_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Cls");
            Ddo_contagemresultado_demandafm_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_set");
            Ddo_contagemresultado_demandafm_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_set");
            Ddo_contagemresultado_demandafm_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Dropdownoptionstype");
            Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Titlecontrolidtoreplace");
            Ddo_contagemresultado_demandafm_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortasc"));
            Ddo_contagemresultado_demandafm_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includesortdsc"));
            Ddo_contagemresultado_demandafm_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortedstatus");
            Ddo_contagemresultado_demandafm_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includefilter"));
            Ddo_contagemresultado_demandafm_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filtertype");
            Ddo_contagemresultado_demandafm_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filterisrange"));
            Ddo_contagemresultado_demandafm_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Includedatalist"));
            Ddo_contagemresultado_demandafm_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalisttype");
            Ddo_contagemresultado_demandafm_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistproc");
            Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultado_demandafm_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortasc");
            Ddo_contagemresultado_demandafm_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Sortdsc");
            Ddo_contagemresultado_demandafm_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Loadingdata");
            Ddo_contagemresultado_demandafm_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Cleanfilter");
            Ddo_contagemresultado_demandafm_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Noresultsfound");
            Ddo_contagemresultado_demandafm_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Searchbuttontext");
            Ddo_agendaatendimento_qtdund_Caption = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Caption");
            Ddo_agendaatendimento_qtdund_Tooltip = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Tooltip");
            Ddo_agendaatendimento_qtdund_Cls = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Cls");
            Ddo_agendaatendimento_qtdund_Filteredtext_set = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_set");
            Ddo_agendaatendimento_qtdund_Filteredtextto_set = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_set");
            Ddo_agendaatendimento_qtdund_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Dropdownoptionstype");
            Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Titlecontrolidtoreplace");
            Ddo_agendaatendimento_qtdund_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includesortasc"));
            Ddo_agendaatendimento_qtdund_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includesortdsc"));
            Ddo_agendaatendimento_qtdund_Sortedstatus = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Sortedstatus");
            Ddo_agendaatendimento_qtdund_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includefilter"));
            Ddo_agendaatendimento_qtdund_Filtertype = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filtertype");
            Ddo_agendaatendimento_qtdund_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filterisrange"));
            Ddo_agendaatendimento_qtdund_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Includedatalist"));
            Ddo_agendaatendimento_qtdund_Sortasc = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Sortasc");
            Ddo_agendaatendimento_qtdund_Sortdsc = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Sortdsc");
            Ddo_agendaatendimento_qtdund_Cleanfilter = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Cleanfilter");
            Ddo_agendaatendimento_qtdund_Rangefilterfrom = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterfrom");
            Ddo_agendaatendimento_qtdund_Rangefilterto = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Rangefilterto");
            Ddo_agendaatendimento_qtdund_Searchbuttontext = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultado_demanda_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Activeeventkey");
            Ddo_contagemresultado_demanda_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Filteredtext_get");
            Ddo_contagemresultado_demanda_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA_Selectedvalue_get");
            Ddo_contagemresultado_demandafm_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Activeeventkey");
            Ddo_contagemresultado_demandafm_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Filteredtext_get");
            Ddo_contagemresultado_demandafm_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM_Selectedvalue_get");
            Ddo_agendaatendimento_qtdund_Activeeventkey = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Activeeventkey");
            Ddo_agendaatendimento_qtdund_Filteredtext_get = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtext_get");
            Ddo_agendaatendimento_qtdund_Filteredtextto_get = cgiGet( sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDA"), AV21TFContagemResultado_Demanda) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDA_SEL"), AV22TFContagemResultado_Demanda_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDAFM"), AV25TFContagemResultado_DemandaFM) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADO_DEMANDAFM_SEL"), AV26TFContagemResultado_DemandaFM_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAGENDAATENDIMENTO_QTDUND"), ",", ".") != AV29TFAgendaAtendimento_QtdUnd )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAGENDAATENDIMENTO_QTDUND_TO"), ",", ".") != AV30TFAgendaAtendimento_QtdUnd_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15IH2 */
         E15IH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15IH2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontagemresultado_demanda_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultado_demanda_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demanda_Visible), 5, 0)));
         edtavTfcontagemresultado_demanda_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultado_demanda_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demanda_sel_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultado_demandafm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_Visible), 5, 0)));
         edtavTfcontagemresultado_demandafm_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultado_demandafm_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_demandafm_sel_Visible), 5, 0)));
         edtavTfagendaatendimento_qtdund_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfagendaatendimento_qtdund_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_qtdund_Visible), 5, 0)));
         edtavTfagendaatendimento_qtdund_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfagendaatendimento_qtdund_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfagendaatendimento_qtdund_to_Visible), 5, 0)));
         Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_Demanda";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demanda_Titlecontrolidtoreplace);
         AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace = Ddo_contagemresultado_demanda_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace", AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_DemandaFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace);
         AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace", AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace);
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace = subGrid_Internalname+"_AgendaAtendimento_QtdUnd";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "TitleControlIdToReplace", Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace);
         AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace", AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace);
         edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible), 5, 0)));
         edtAgendaAtendimento_CntSrcCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAgendaAtendimento_CntSrcCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_CntSrcCod_Visible), 5, 0)));
         edtAgendaAtendimento_Data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAgendaAtendimento_Data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAgendaAtendimento_Data_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV32DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV32DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E16IH2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV20ContagemResultado_DemandaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28AgendaAtendimento_QtdUndTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_Demanda_Titleformat = 2;
         edtContagemResultado_Demanda_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Refer�ncia", AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Demanda_Internalname, "Title", edtContagemResultado_Demanda_Title);
         edtContagemResultado_DemandaFM_Titleformat = 2;
         edtContagemResultado_DemandaFM_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "OS Interna", AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_DemandaFM_Internalname, "Title", edtContagemResultado_DemandaFM_Title);
         edtAgendaAtendimento_QtdUnd_Titleformat = 2;
         edtAgendaAtendimento_QtdUnd_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidades", AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAgendaAtendimento_QtdUnd_Internalname, "Title", edtAgendaAtendimento_QtdUnd_Title);
         AV34GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridCurrentPage), 10, 0)));
         AV35GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridPageCount), 10, 0)));
         AV17WebSession.Set("Caller", "AgnAtn");
         AV19Total = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Total", StringUtil.LTrim( StringUtil.Str( AV19Total, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20ContagemResultado_DemandaTitleFilterData", AV20ContagemResultado_DemandaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ContagemResultado_DemandaFMTitleFilterData", AV24ContagemResultado_DemandaFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28AgendaAtendimento_QtdUndTitleFilterData", AV28AgendaAtendimento_QtdUndTitleFilterData);
      }

      protected void E11IH2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV33PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV33PageToGo) ;
         }
      }

      protected void E12IH2( )
      {
         /* Ddo_contagemresultado_demanda_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_demanda_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_demanda_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demanda_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFContagemResultado_Demanda = Ddo_contagemresultado_demanda_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContagemResultado_Demanda", AV21TFContagemResultado_Demanda);
            AV22TFContagemResultado_Demanda_Sel = Ddo_contagemresultado_demanda_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultado_Demanda_Sel", AV22TFContagemResultado_Demanda_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13IH2( )
      {
         /* Ddo_contagemresultado_demandafm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_demandafm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_demandafm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_demandafm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFContagemResultado_DemandaFM = Ddo_contagemresultado_demandafm_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContagemResultado_DemandaFM", AV25TFContagemResultado_DemandaFM);
            AV26TFContagemResultado_DemandaFM_Sel = Ddo_contagemresultado_demandafm_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContagemResultado_DemandaFM_Sel", AV26TFContagemResultado_DemandaFM_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14IH2( )
      {
         /* Ddo_agendaatendimento_qtdund_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_agendaatendimento_qtdund_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_agendaatendimento_qtdund_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_qtdund_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_agendaatendimento_qtdund_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_agendaatendimento_qtdund_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFAgendaAtendimento_QtdUnd = NumberUtil.Val( Ddo_agendaatendimento_qtdund_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5)));
            AV30TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( Ddo_agendaatendimento_qtdund_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E17IH2( )
      {
         /* Grid_Load Routine */
         edtContagemResultado_Demanda_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A1209AgendaAtendimento_CodDmn) + "," + UrlEncode(StringUtil.RTrim(""));
         AV19Total = (decimal)(AV19Total+A1186AgendaAtendimento_QtdUnd);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Total", StringUtil.LTrim( StringUtil.Str( AV19Total, 14, 5)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 11;
         }
         sendrow_112( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(11, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultado_demanda_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
         Ddo_contagemresultado_demandafm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
         Ddo_agendaatendimento_qtdund_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_contagemresultado_demanda_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "SortedStatus", Ddo_contagemresultado_demanda_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_contagemresultado_demandafm_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "SortedStatus", Ddo_contagemresultado_demandafm_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_agendaatendimento_qtdund_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "SortedStatus", Ddo_agendaatendimento_qtdund_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV38Pgmname+"GridState"), "") == 0 )
         {
            AV12GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV38Pgmname+"GridState"), "");
         }
         else
         {
            AV12GridState.FromXml(AV16Session.Get(AV38Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV12GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV12GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV39GXV1 = 1;
         while ( AV39GXV1 <= AV12GridState.gxTpr_Filtervalues.Count )
         {
            AV13GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV12GridState.gxTpr_Filtervalues.Item(AV39GXV1));
            if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV21TFContagemResultado_Demanda = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContagemResultado_Demanda", AV21TFContagemResultado_Demanda);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultado_Demanda)) )
               {
                  Ddo_contagemresultado_demanda_Filteredtext_set = AV21TFContagemResultado_Demanda;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "FilteredText_set", Ddo_contagemresultado_demanda_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV22TFContagemResultado_Demanda_Sel = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContagemResultado_Demanda_Sel", AV22TFContagemResultado_Demanda_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContagemResultado_Demanda_Sel)) )
               {
                  Ddo_contagemresultado_demanda_Selectedvalue_set = AV22TFContagemResultado_Demanda_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demanda_Internalname, "SelectedValue_set", Ddo_contagemresultado_demanda_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV25TFContagemResultado_DemandaFM = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContagemResultado_DemandaFM", AV25TFContagemResultado_DemandaFM);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContagemResultado_DemandaFM)) )
               {
                  Ddo_contagemresultado_demandafm_Filteredtext_set = AV25TFContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "FilteredText_set", Ddo_contagemresultado_demandafm_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV26TFContagemResultado_DemandaFM_Sel = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContagemResultado_DemandaFM_Sel", AV26TFContagemResultado_DemandaFM_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContagemResultado_DemandaFM_Sel)) )
               {
                  Ddo_contagemresultado_demandafm_Selectedvalue_set = AV26TFContagemResultado_DemandaFM_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultado_demandafm_Internalname, "SelectedValue_set", Ddo_contagemresultado_demandafm_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_QTDUND") == 0 )
            {
               AV29TFAgendaAtendimento_QtdUnd = NumberUtil.Val( AV13GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFAgendaAtendimento_QtdUnd", StringUtil.LTrim( StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5)));
               AV30TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( AV13GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFAgendaAtendimento_QtdUnd_To", StringUtil.LTrim( StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV29TFAgendaAtendimento_QtdUnd) )
               {
                  Ddo_agendaatendimento_qtdund_Filteredtext_set = StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "FilteredText_set", Ddo_agendaatendimento_qtdund_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV30TFAgendaAtendimento_QtdUnd_To) )
               {
                  Ddo_agendaatendimento_qtdund_Filteredtextto_set = StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_agendaatendimento_qtdund_Internalname, "FilteredTextTo_set", Ddo_agendaatendimento_qtdund_Filteredtextto_set);
               }
            }
            AV39GXV1 = (int)(AV39GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV12GridState.FromXml(AV16Session.Get(AV38Pgmname+"GridState"), "");
         AV12GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV12GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV12GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultado_Demanda)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDA";
            AV13GridStateFilterValue.gxTpr_Value = AV21TFContagemResultado_Demanda;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContagemResultado_Demanda_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDA_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV22TFContagemResultado_Demanda_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContagemResultado_DemandaFM)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM";
            AV13GridStateFilterValue.gxTpr_Value = AV25TFContagemResultado_DemandaFM;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContagemResultado_DemandaFM_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_DEMANDAFM_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV26TFContagemResultado_DemandaFM_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV29TFAgendaAtendimento_QtdUnd) && (Convert.ToDecimal(0)==AV30TFAgendaAtendimento_QtdUnd_To) ) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFAGENDAATENDIMENTO_QTDUND";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV29TFAgendaAtendimento_QtdUnd, 14, 5);
            AV13GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV30TFAgendaAtendimento_QtdUnd_To, 14, 5);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! (0==AV7AgendaAtendimento_CntSrcCod) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "PARM_&AGENDAATENDIMENTO_CNTSRCCOD";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! (DateTime.MinValue==AV8AgendaAtendimento_Data) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "PARM_&AGENDAATENDIMENTO_DATA";
            AV13GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV8AgendaAtendimento_Data, 2, "/");
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV38Pgmname+"GridState",  AV12GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10TrnContext.gxTpr_Callerobject = AV38Pgmname;
         AV10TrnContext.gxTpr_Callerondelete = true;
         AV10TrnContext.gxTpr_Callerurl = AV9HTTPRequest.ScriptName+"?"+AV9HTTPRequest.QueryString;
         AV10TrnContext.gxTpr_Transactionname = "AgendaAtendimento";
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11TrnContextAtt.gxTpr_Attributename = "AgendaAtendimento_CntSrcCod";
         AV11TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0);
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11TrnContextAtt.gxTpr_Attributename = "AgendaAtendimento_Data";
         AV11TrnContextAtt.gxTpr_Attributevalue = context.localUtil.DToC( AV8AgendaAtendimento_Data, 2, "/");
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV16Session.Set("TrnContext", AV10TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_IH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_IH2( true) ;
         }
         else
         {
            wb_table2_8_IH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_IH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_20_IH2( true) ;
         }
         else
         {
            wb_table3_20_IH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_IH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IH2e( true) ;
         }
         else
         {
            wb_table1_2_IH2e( false) ;
         }
      }

      protected void wb_table3_20_IH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktotal_Internalname, "Total:", "", "", lblTextblocktotal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AgendaAtendimentoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_25_IH2( true) ;
         }
         else
         {
            wb_table4_25_IH2( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_IH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_IH2e( true) ;
         }
         else
         {
            wb_table3_20_IH2e( false) ;
         }
      }

      protected void wb_table4_25_IH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtotal_Internalname, tblTablemergedtotal_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV19Total, 14, 5, ",", "")), ((edtavTotal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19Total, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV19Total, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTotal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTotal_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_AgendaAtendimentoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTotal_righttext_Internalname, "-", "", "", lblTotal_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AgendaAtendimentoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_IH2e( true) ;
         }
         else
         {
            wb_table4_25_IH2e( false) ;
         }
      }

      protected void wb_table2_8_IH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Demanda_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Demanda_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Demanda_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_DemandaFM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_DemandaFM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_DemandaFM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAgendaAtendimento_QtdUnd_Titleformat == 0 )
               {
                  context.SendWebValue( edtAgendaAtendimento_QtdUnd_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAgendaAtendimento_QtdUnd_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Demanda_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Demanda_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultado_Demanda_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_DemandaFM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_DemandaFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAgendaAtendimento_QtdUnd_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAgendaAtendimento_QtdUnd_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_IH2e( true) ;
         }
         else
         {
            wb_table2_8_IH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7AgendaAtendimento_CntSrcCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0)));
         AV8AgendaAtendimento_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8AgendaAtendimento_Data", context.localUtil.Format(AV8AgendaAtendimento_Data, "99/99/99"));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIH2( ) ;
         WSIH2( ) ;
         WEIH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7AgendaAtendimento_CntSrcCod = (String)((String)getParm(obj,0));
         sCtrlAV8AgendaAtendimento_Data = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAIH2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "agendaatendimentowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAIH2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7AgendaAtendimento_CntSrcCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0)));
            AV8AgendaAtendimento_Data = (DateTime)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8AgendaAtendimento_Data", context.localUtil.Format(AV8AgendaAtendimento_Data, "99/99/99"));
         }
         wcpOAV7AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7AgendaAtendimento_CntSrcCod"), ",", "."));
         wcpOAV8AgendaAtendimento_Data = context.localUtil.CToD( cgiGet( sPrefix+"wcpOAV8AgendaAtendimento_Data"), 0);
         if ( ! GetJustCreated( ) && ( ( AV7AgendaAtendimento_CntSrcCod != wcpOAV7AgendaAtendimento_CntSrcCod ) || ( AV8AgendaAtendimento_Data != wcpOAV8AgendaAtendimento_Data ) ) )
         {
            setjustcreated();
         }
         wcpOAV7AgendaAtendimento_CntSrcCod = AV7AgendaAtendimento_CntSrcCod;
         wcpOAV8AgendaAtendimento_Data = AV8AgendaAtendimento_Data;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7AgendaAtendimento_CntSrcCod = cgiGet( sPrefix+"AV7AgendaAtendimento_CntSrcCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7AgendaAtendimento_CntSrcCod) > 0 )
         {
            AV7AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7AgendaAtendimento_CntSrcCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AgendaAtendimento_CntSrcCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0)));
         }
         else
         {
            AV7AgendaAtendimento_CntSrcCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7AgendaAtendimento_CntSrcCod_PARM"), ",", "."));
         }
         sCtrlAV8AgendaAtendimento_Data = cgiGet( sPrefix+"AV8AgendaAtendimento_Data_CTRL");
         if ( StringUtil.Len( sCtrlAV8AgendaAtendimento_Data) > 0 )
         {
            AV8AgendaAtendimento_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( sCtrlAV8AgendaAtendimento_Data), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8AgendaAtendimento_Data", context.localUtil.Format(AV8AgendaAtendimento_Data, "99/99/99"));
         }
         else
         {
            AV8AgendaAtendimento_Data = context.localUtil.CToD( cgiGet( sPrefix+"AV8AgendaAtendimento_Data_PARM"), 0);
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAIH2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSIH2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSIH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7AgendaAtendimento_CntSrcCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AgendaAtendimento_CntSrcCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7AgendaAtendimento_CntSrcCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7AgendaAtendimento_CntSrcCod_CTRL", StringUtil.RTrim( sCtrlAV7AgendaAtendimento_CntSrcCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV8AgendaAtendimento_Data_PARM", context.localUtil.DToC( AV8AgendaAtendimento_Data, 0, "/"));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV8AgendaAtendimento_Data)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV8AgendaAtendimento_Data_CTRL", StringUtil.RTrim( sCtrlAV8AgendaAtendimento_Data));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEIH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216111071");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("agendaatendimentowc.js", "?20206216111071");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtContagemResultado_Demanda_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_11_idx;
         edtContagemResultado_DemandaFM_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_11_idx;
         edtAgendaAtendimento_QtdUnd_Internalname = sPrefix+"AGENDAATENDIMENTO_QTDUND_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtContagemResultado_Demanda_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_11_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_11_fel_idx;
         edtAgendaAtendimento_QtdUnd_Internalname = sPrefix+"AGENDAATENDIMENTO_QTDUND_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBIH0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_11_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultado_Demanda_Link,(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAgendaAtendimento_QtdUnd_Internalname,StringUtil.LTrim( StringUtil.NToC( A1186AgendaAtendimento_QtdUnd, 14, 5, ",", "")),context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAgendaAtendimento_QtdUnd_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AGENDAATENDIMENTO_QTDUND"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( A1186AgendaAtendimento_QtdUnd, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         edtContagemResultado_Demanda_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_DemandaFM_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDAFM";
         edtAgendaAtendimento_QtdUnd_Internalname = sPrefix+"AGENDAATENDIMENTO_QTDUND";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         lblTextblocktotal_Internalname = sPrefix+"TEXTBLOCKTOTAL";
         edtavTotal_Internalname = sPrefix+"vTOTAL";
         lblTotal_righttext_Internalname = sPrefix+"TOTAL_RIGHTTEXT";
         tblTablemergedtotal_Internalname = sPrefix+"TABLEMERGEDTOTAL";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtAgendaAtendimento_CntSrcCod_Internalname = sPrefix+"AGENDAATENDIMENTO_CNTSRCCOD";
         edtAgendaAtendimento_Data_Internalname = sPrefix+"AGENDAATENDIMENTO_DATA";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontagemresultado_demanda_Internalname = sPrefix+"vTFCONTAGEMRESULTADO_DEMANDA";
         edtavTfcontagemresultado_demanda_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADO_DEMANDA_SEL";
         edtavTfcontagemresultado_demandafm_Internalname = sPrefix+"vTFCONTAGEMRESULTADO_DEMANDAFM";
         edtavTfcontagemresultado_demandafm_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADO_DEMANDAFM_SEL";
         edtavTfagendaatendimento_qtdund_Internalname = sPrefix+"vTFAGENDAATENDIMENTO_QTDUND";
         edtavTfagendaatendimento_qtdund_to_Internalname = sPrefix+"vTFAGENDAATENDIMENTO_QTDUND_TO";
         Ddo_contagemresultado_demanda_Internalname = sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDA";
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultado_demandafm_Internalname = sPrefix+"DDO_CONTAGEMRESULTADO_DEMANDAFM";
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE";
         Ddo_agendaatendimento_qtdund_Internalname = sPrefix+"DDO_AGENDAATENDIMENTO_QTDUND";
         edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAgendaAtendimento_QtdUnd_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultado_Demanda_Link = "";
         edtAgendaAtendimento_QtdUnd_Titleformat = 0;
         edtContagemResultado_DemandaFM_Titleformat = 0;
         edtContagemResultado_Demanda_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavTotal_Jsonclick = "";
         edtavTotal_Enabled = 1;
         edtAgendaAtendimento_QtdUnd_Title = "Unidades";
         edtContagemResultado_DemandaFM_Title = "OS Interna";
         edtContagemResultado_Demanda_Title = "N� Refer�ncia";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible = 1;
         edtavTfagendaatendimento_qtdund_to_Jsonclick = "";
         edtavTfagendaatendimento_qtdund_to_Visible = 1;
         edtavTfagendaatendimento_qtdund_Jsonclick = "";
         edtavTfagendaatendimento_qtdund_Visible = 1;
         edtavTfcontagemresultado_demandafm_sel_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_sel_Visible = 1;
         edtavTfcontagemresultado_demandafm_Jsonclick = "";
         edtavTfcontagemresultado_demandafm_Visible = 1;
         edtavTfcontagemresultado_demanda_sel_Jsonclick = "";
         edtavTfcontagemresultado_demanda_sel_Visible = 1;
         edtavTfcontagemresultado_demanda_Jsonclick = "";
         edtavTfcontagemresultado_demanda_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtAgendaAtendimento_Data_Jsonclick = "";
         edtAgendaAtendimento_Data_Visible = 1;
         edtAgendaAtendimento_CntSrcCod_Jsonclick = "";
         edtAgendaAtendimento_CntSrcCod_Visible = 1;
         Ddo_agendaatendimento_qtdund_Searchbuttontext = "Pesquisar";
         Ddo_agendaatendimento_qtdund_Rangefilterto = "At�";
         Ddo_agendaatendimento_qtdund_Rangefilterfrom = "Desde";
         Ddo_agendaatendimento_qtdund_Cleanfilter = "Limpar pesquisa";
         Ddo_agendaatendimento_qtdund_Sortdsc = "Ordenar de Z � A";
         Ddo_agendaatendimento_qtdund_Sortasc = "Ordenar de A � Z";
         Ddo_agendaatendimento_qtdund_Includedatalist = Convert.ToBoolean( 0);
         Ddo_agendaatendimento_qtdund_Filterisrange = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Filtertype = "Numeric";
         Ddo_agendaatendimento_qtdund_Includefilter = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Includesortasc = Convert.ToBoolean( -1);
         Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace = "";
         Ddo_agendaatendimento_qtdund_Dropdownoptionstype = "GridTitleSettings";
         Ddo_agendaatendimento_qtdund_Cls = "ColumnSettings";
         Ddo_agendaatendimento_qtdund_Tooltip = "Op��es";
         Ddo_agendaatendimento_qtdund_Caption = "";
         Ddo_contagemresultado_demandafm_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demandafm_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demandafm_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demandafm_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demandafm_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_demandafm_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demandafm_Datalistproc = "GetAgendaAtendimentoWCFilterData";
         Ddo_contagemresultado_demandafm_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demandafm_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demandafm_Filtertype = "Character";
         Ddo_contagemresultado_demandafm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demandafm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demandafm_Cls = "ColumnSettings";
         Ddo_contagemresultado_demandafm_Tooltip = "Op��es";
         Ddo_contagemresultado_demandafm_Caption = "";
         Ddo_contagemresultado_demanda_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_demanda_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultado_demanda_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_demanda_Loadingdata = "Carregando dados...";
         Ddo_contagemresultado_demanda_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_demanda_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultado_demanda_Datalistproc = "GetAgendaAtendimentoWCFilterData";
         Ddo_contagemresultado_demanda_Datalisttype = "Dynamic";
         Ddo_contagemresultado_demanda_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultado_demanda_Filtertype = "Character";
         Ddo_contagemresultado_demanda_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_demanda_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_demanda_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_demanda_Cls = "ColumnSettings";
         Ddo_contagemresultado_demanda_Tooltip = "Op��es";
         Ddo_contagemresultado_demanda_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1186AgendaAtendimento_QtdUnd',fld:'AGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV22TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV25TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV26TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV29TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7AgendaAtendimento_CntSrcCod',fld:'vAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV8AgendaAtendimento_Data',fld:'vAGENDAATENDIMENTO_DATA',pic:'',nv:''}],oparms:[{av:'AV20ContagemResultado_DemandaTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDATITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV28AgendaAtendimento_QtdUndTitleFilterData',fld:'vAGENDAATENDIMENTO_QTDUNDTITLEFILTERDATA',pic:'',nv:null},{av:'edtContagemResultado_Demanda_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'edtContagemResultado_Demanda_Title',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{av:'edtContagemResultado_DemandaFM_Titleformat',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'edtContagemResultado_DemandaFM_Title',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{av:'edtAgendaAtendimento_QtdUnd_Titleformat',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Titleformat'},{av:'edtAgendaAtendimento_QtdUnd_Title',ctrl:'AGENDAATENDIMENTO_QTDUND',prop:'Title'},{av:'AV34GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV35GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV22TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV25TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV26TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV29TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7AgendaAtendimento_CntSrcCod',fld:'vAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV8AgendaAtendimento_Data',fld:'vAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1186AgendaAtendimento_QtdUnd',fld:'AGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDA.ONOPTIONCLICKED","{handler:'E12IH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV22TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV25TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV26TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV29TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7AgendaAtendimento_CntSrcCod',fld:'vAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV8AgendaAtendimento_Data',fld:'vAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1186AgendaAtendimento_QtdUnd',fld:'AGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultado_demanda_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demanda_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demanda_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'AV21TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV22TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED","{handler:'E13IH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV22TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV25TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV26TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV29TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7AgendaAtendimento_CntSrcCod',fld:'vAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV8AgendaAtendimento_Data',fld:'vAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1186AgendaAtendimento_QtdUnd',fld:'AGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultado_demandafm_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_demandafm_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_demandafm_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'},{av:'AV25TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV26TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AGENDAATENDIMENTO_QTDUND.ONOPTIONCLICKED","{handler:'E14IH2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContagemResultado_Demanda',fld:'vTFCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV22TFContagemResultado_Demanda_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDA_SEL',pic:'@!',nv:''},{av:'AV25TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV26TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV29TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7AgendaAtendimento_CntSrcCod',fld:'vAGENDAATENDIMENTO_CNTSRCCOD',pic:'ZZZZZ9',nv:0},{av:'AV8AgendaAtendimento_Data',fld:'vAGENDAATENDIMENTO_DATA',pic:'',nv:''},{av:'AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace',fld:'vDDO_AGENDAATENDIMENTO_QTDUNDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1186AgendaAtendimento_QtdUnd',fld:'AGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'Ddo_agendaatendimento_qtdund_Activeeventkey',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'ActiveEventKey'},{av:'Ddo_agendaatendimento_qtdund_Filteredtext_get',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'FilteredText_get'},{av:'Ddo_agendaatendimento_qtdund_Filteredtextto_get',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_agendaatendimento_qtdund_Sortedstatus',ctrl:'DDO_AGENDAATENDIMENTO_QTDUND',prop:'SortedStatus'},{av:'AV29TFAgendaAtendimento_QtdUnd',fld:'vTFAGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30TFAgendaAtendimento_QtdUnd_To',fld:'vTFAGENDAATENDIMENTO_QTDUND_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemresultado_demanda_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDA',prop:'SortedStatus'},{av:'Ddo_contagemresultado_demandafm_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17IH2',iparms:[{av:'A1209AgendaAtendimento_CodDmn',fld:'AGENDAATENDIMENTO_CODDMN',pic:'ZZZZZ9',nv:0},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1186AgendaAtendimento_QtdUnd',fld:'AGENDAATENDIMENTO_QTDUND',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0}],oparms:[{av:'edtContagemResultado_Demanda_Link',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Link'},{av:'AV19Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8AgendaAtendimento_Data = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultado_demanda_Activeeventkey = "";
         Ddo_contagemresultado_demanda_Filteredtext_get = "";
         Ddo_contagemresultado_demanda_Selectedvalue_get = "";
         Ddo_contagemresultado_demandafm_Activeeventkey = "";
         Ddo_contagemresultado_demandafm_Filteredtext_get = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_get = "";
         Ddo_agendaatendimento_qtdund_Activeeventkey = "";
         Ddo_agendaatendimento_qtdund_Filteredtext_get = "";
         Ddo_agendaatendimento_qtdund_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV21TFContagemResultado_Demanda = "";
         AV22TFContagemResultado_Demanda_Sel = "";
         AV25TFContagemResultado_DemandaFM = "";
         AV26TFContagemResultado_DemandaFM_Sel = "";
         AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace = "";
         AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "";
         AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace = "";
         AV38Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV32DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV20ContagemResultado_DemandaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContagemResultado_DemandaFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28AgendaAtendimento_QtdUndTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultado_demanda_Filteredtext_set = "";
         Ddo_contagemresultado_demanda_Selectedvalue_set = "";
         Ddo_contagemresultado_demanda_Sortedstatus = "";
         Ddo_contagemresultado_demandafm_Filteredtext_set = "";
         Ddo_contagemresultado_demandafm_Selectedvalue_set = "";
         Ddo_contagemresultado_demandafm_Sortedstatus = "";
         Ddo_agendaatendimento_qtdund_Filteredtext_set = "";
         Ddo_agendaatendimento_qtdund_Filteredtextto_set = "";
         Ddo_agendaatendimento_qtdund_Sortedstatus = "";
         GX_FocusControl = "";
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV21TFContagemResultado_Demanda = "";
         lV25TFContagemResultado_DemandaFM = "";
         H00IH2_A1209AgendaAtendimento_CodDmn = new int[1] ;
         H00IH2_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         H00IH2_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         H00IH2_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         H00IH2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00IH2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00IH2_A457ContagemResultado_Demanda = new String[] {""} ;
         H00IH2_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00IH3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV17WebSession = context.GetSession();
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV12GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         lblTextblocktotal_Jsonclick = "";
         lblTotal_righttext_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7AgendaAtendimento_CntSrcCod = "";
         sCtrlAV8AgendaAtendimento_Data = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.agendaatendimentowc__default(),
            new Object[][] {
                new Object[] {
               H00IH2_A1209AgendaAtendimento_CodDmn, H00IH2_A1183AgendaAtendimento_CntSrcCod, H00IH2_A1184AgendaAtendimento_Data, H00IH2_A1186AgendaAtendimento_QtdUnd, H00IH2_A493ContagemResultado_DemandaFM, H00IH2_n493ContagemResultado_DemandaFM, H00IH2_A457ContagemResultado_Demanda, H00IH2_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00IH3_AGRID_nRecordCount
               }
            }
         );
         AV38Pgmname = "AgendaAtendimentoWC";
         /* GeneXus formulas. */
         AV38Pgmname = "AgendaAtendimentoWC";
         context.Gx_err = 0;
         edtavTotal_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultado_Demanda_Titleformat ;
      private short edtContagemResultado_DemandaFM_Titleformat ;
      private short edtAgendaAtendimento_QtdUnd_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7AgendaAtendimento_CntSrcCod ;
      private int wcpOAV7AgendaAtendimento_CntSrcCod ;
      private int subGrid_Rows ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int edtavTotal_Enabled ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultado_demanda_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultado_demandafm_Datalistupdateminimumcharacters ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private int edtAgendaAtendimento_CntSrcCod_Visible ;
      private int edtAgendaAtendimento_Data_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontagemresultado_demanda_Visible ;
      private int edtavTfcontagemresultado_demanda_sel_Visible ;
      private int edtavTfcontagemresultado_demandafm_Visible ;
      private int edtavTfcontagemresultado_demandafm_sel_Visible ;
      private int edtavTfagendaatendimento_qtdund_Visible ;
      private int edtavTfagendaatendimento_qtdund_to_Visible ;
      private int edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV33PageToGo ;
      private int AV39GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV34GridCurrentPage ;
      private long AV35GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV29TFAgendaAtendimento_QtdUnd ;
      private decimal AV30TFAgendaAtendimento_QtdUnd_To ;
      private decimal AV19Total ;
      private decimal A1186AgendaAtendimento_QtdUnd ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultado_demanda_Activeeventkey ;
      private String Ddo_contagemresultado_demanda_Filteredtext_get ;
      private String Ddo_contagemresultado_demanda_Selectedvalue_get ;
      private String Ddo_contagemresultado_demandafm_Activeeventkey ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_get ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_get ;
      private String Ddo_agendaatendimento_qtdund_Activeeventkey ;
      private String Ddo_agendaatendimento_qtdund_Filteredtext_get ;
      private String Ddo_agendaatendimento_qtdund_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_11_idx="0001" ;
      private String AV38Pgmname ;
      private String GXKey ;
      private String edtavTotal_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultado_demanda_Caption ;
      private String Ddo_contagemresultado_demanda_Tooltip ;
      private String Ddo_contagemresultado_demanda_Cls ;
      private String Ddo_contagemresultado_demanda_Filteredtext_set ;
      private String Ddo_contagemresultado_demanda_Selectedvalue_set ;
      private String Ddo_contagemresultado_demanda_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demanda_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demanda_Sortedstatus ;
      private String Ddo_contagemresultado_demanda_Filtertype ;
      private String Ddo_contagemresultado_demanda_Datalisttype ;
      private String Ddo_contagemresultado_demanda_Datalistproc ;
      private String Ddo_contagemresultado_demanda_Sortasc ;
      private String Ddo_contagemresultado_demanda_Sortdsc ;
      private String Ddo_contagemresultado_demanda_Loadingdata ;
      private String Ddo_contagemresultado_demanda_Cleanfilter ;
      private String Ddo_contagemresultado_demanda_Noresultsfound ;
      private String Ddo_contagemresultado_demanda_Searchbuttontext ;
      private String Ddo_contagemresultado_demandafm_Caption ;
      private String Ddo_contagemresultado_demandafm_Tooltip ;
      private String Ddo_contagemresultado_demandafm_Cls ;
      private String Ddo_contagemresultado_demandafm_Filteredtext_set ;
      private String Ddo_contagemresultado_demandafm_Selectedvalue_set ;
      private String Ddo_contagemresultado_demandafm_Dropdownoptionstype ;
      private String Ddo_contagemresultado_demandafm_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_demandafm_Sortedstatus ;
      private String Ddo_contagemresultado_demandafm_Filtertype ;
      private String Ddo_contagemresultado_demandafm_Datalisttype ;
      private String Ddo_contagemresultado_demandafm_Datalistproc ;
      private String Ddo_contagemresultado_demandafm_Sortasc ;
      private String Ddo_contagemresultado_demandafm_Sortdsc ;
      private String Ddo_contagemresultado_demandafm_Loadingdata ;
      private String Ddo_contagemresultado_demandafm_Cleanfilter ;
      private String Ddo_contagemresultado_demandafm_Noresultsfound ;
      private String Ddo_contagemresultado_demandafm_Searchbuttontext ;
      private String Ddo_agendaatendimento_qtdund_Caption ;
      private String Ddo_agendaatendimento_qtdund_Tooltip ;
      private String Ddo_agendaatendimento_qtdund_Cls ;
      private String Ddo_agendaatendimento_qtdund_Filteredtext_set ;
      private String Ddo_agendaatendimento_qtdund_Filteredtextto_set ;
      private String Ddo_agendaatendimento_qtdund_Dropdownoptionstype ;
      private String Ddo_agendaatendimento_qtdund_Titlecontrolidtoreplace ;
      private String Ddo_agendaatendimento_qtdund_Sortedstatus ;
      private String Ddo_agendaatendimento_qtdund_Filtertype ;
      private String Ddo_agendaatendimento_qtdund_Sortasc ;
      private String Ddo_agendaatendimento_qtdund_Sortdsc ;
      private String Ddo_agendaatendimento_qtdund_Cleanfilter ;
      private String Ddo_agendaatendimento_qtdund_Rangefilterfrom ;
      private String Ddo_agendaatendimento_qtdund_Rangefilterto ;
      private String Ddo_agendaatendimento_qtdund_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtAgendaAtendimento_CntSrcCod_Internalname ;
      private String edtAgendaAtendimento_CntSrcCod_Jsonclick ;
      private String edtAgendaAtendimento_Data_Internalname ;
      private String edtAgendaAtendimento_Data_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontagemresultado_demanda_Internalname ;
      private String edtavTfcontagemresultado_demanda_Jsonclick ;
      private String edtavTfcontagemresultado_demanda_sel_Internalname ;
      private String edtavTfcontagemresultado_demanda_sel_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_Internalname ;
      private String edtavTfcontagemresultado_demandafm_Jsonclick ;
      private String edtavTfcontagemresultado_demandafm_sel_Internalname ;
      private String edtavTfcontagemresultado_demandafm_sel_Jsonclick ;
      private String edtavTfagendaatendimento_qtdund_Internalname ;
      private String edtavTfagendaatendimento_qtdund_Jsonclick ;
      private String edtavTfagendaatendimento_qtdund_to_Internalname ;
      private String edtavTfagendaatendimento_qtdund_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contagemresultado_demandatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultado_demandafmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_agendaatendimento_qtdundtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtAgendaAtendimento_QtdUnd_Internalname ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultado_demanda_Internalname ;
      private String Ddo_contagemresultado_demandafm_Internalname ;
      private String Ddo_agendaatendimento_qtdund_Internalname ;
      private String edtContagemResultado_Demanda_Title ;
      private String edtContagemResultado_DemandaFM_Title ;
      private String edtAgendaAtendimento_QtdUnd_Title ;
      private String edtContagemResultado_Demanda_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblocktotal_Internalname ;
      private String lblTextblocktotal_Jsonclick ;
      private String tblTablemergedtotal_Internalname ;
      private String edtavTotal_Jsonclick ;
      private String lblTotal_righttext_Internalname ;
      private String lblTotal_righttext_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7AgendaAtendimento_CntSrcCod ;
      private String sCtrlAV8AgendaAtendimento_Data ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtAgendaAtendimento_QtdUnd_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8AgendaAtendimento_Data ;
      private DateTime wcpOAV8AgendaAtendimento_Data ;
      private DateTime A1184AgendaAtendimento_Data ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultado_demanda_Includesortasc ;
      private bool Ddo_contagemresultado_demanda_Includesortdsc ;
      private bool Ddo_contagemresultado_demanda_Includefilter ;
      private bool Ddo_contagemresultado_demanda_Filterisrange ;
      private bool Ddo_contagemresultado_demanda_Includedatalist ;
      private bool Ddo_contagemresultado_demandafm_Includesortasc ;
      private bool Ddo_contagemresultado_demandafm_Includesortdsc ;
      private bool Ddo_contagemresultado_demandafm_Includefilter ;
      private bool Ddo_contagemresultado_demandafm_Filterisrange ;
      private bool Ddo_contagemresultado_demandafm_Includedatalist ;
      private bool Ddo_agendaatendimento_qtdund_Includesortasc ;
      private bool Ddo_agendaatendimento_qtdund_Includesortdsc ;
      private bool Ddo_agendaatendimento_qtdund_Includefilter ;
      private bool Ddo_agendaatendimento_qtdund_Filterisrange ;
      private bool Ddo_agendaatendimento_qtdund_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV21TFContagemResultado_Demanda ;
      private String AV22TFContagemResultado_Demanda_Sel ;
      private String AV25TFContagemResultado_DemandaFM ;
      private String AV26TFContagemResultado_DemandaFM_Sel ;
      private String AV23ddo_ContagemResultado_DemandaTitleControlIdToReplace ;
      private String AV27ddo_ContagemResultado_DemandaFMTitleControlIdToReplace ;
      private String AV31ddo_AgendaAtendimento_QtdUndTitleControlIdToReplace ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String lV21TFContagemResultado_Demanda ;
      private String lV25TFContagemResultado_DemandaFM ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00IH2_A1209AgendaAtendimento_CodDmn ;
      private int[] H00IH2_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] H00IH2_A1184AgendaAtendimento_Data ;
      private decimal[] H00IH2_A1186AgendaAtendimento_QtdUnd ;
      private String[] H00IH2_A493ContagemResultado_DemandaFM ;
      private bool[] H00IH2_n493ContagemResultado_DemandaFM ;
      private String[] H00IH2_A457ContagemResultado_Demanda ;
      private bool[] H00IH2_n457ContagemResultado_Demanda ;
      private long[] H00IH3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      private IGxSession AV17WebSession ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ContagemResultado_DemandaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ContagemResultado_DemandaFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28AgendaAtendimento_QtdUndTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV11TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV12GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV13GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV32DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class agendaatendimentowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IH2( IGxContext context ,
                                             String AV22TFContagemResultado_Demanda_Sel ,
                                             String AV21TFContagemResultado_Demanda ,
                                             String AV26TFContagemResultado_DemandaFM_Sel ,
                                             String AV25TFContagemResultado_DemandaFM ,
                                             decimal AV29TFAgendaAtendimento_QtdUnd ,
                                             decimal AV30TFAgendaAtendimento_QtdUnd_To ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A1183AgendaAtendimento_CntSrcCod ,
                                             int AV7AgendaAtendimento_CntSrcCod ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             DateTime AV8AgendaAtendimento_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [13] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_QtdUnd], T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda]";
         sFromString = " FROM ([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[AgendaAtendimento_CntSrcCod] = @AV7AgendaAtendimento_CntSrcCod)";
         sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] = @AV8AgendaAtendimento_Data)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV21TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV22TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV25TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV26TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFAgendaAtendimento_QtdUnd) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV29TFAgendaAtendimento_QtdUnd)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFAgendaAtendimento_QtdUnd_To) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV30TFAgendaAtendimento_QtdUnd_To)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T2.[ContagemResultado_Demanda]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod] DESC, T1.[AgendaAtendimento_Data] DESC, T2.[ContagemResultado_Demanda] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T2.[ContagemResultado_DemandaFM]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod] DESC, T1.[AgendaAtendimento_Data] DESC, T2.[ContagemResultado_DemandaFM] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_QtdUnd]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod] DESC, T1.[AgendaAtendimento_Data] DESC, T1.[AgendaAtendimento_QtdUnd] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CodDmn]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IH3( IGxContext context ,
                                             String AV22TFContagemResultado_Demanda_Sel ,
                                             String AV21TFContagemResultado_Demanda ,
                                             String AV26TFContagemResultado_DemandaFM_Sel ,
                                             String AV25TFContagemResultado_DemandaFM ,
                                             decimal AV29TFAgendaAtendimento_QtdUnd ,
                                             decimal AV30TFAgendaAtendimento_QtdUnd_To ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A1183AgendaAtendimento_CntSrcCod ,
                                             int AV7AgendaAtendimento_CntSrcCod ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             DateTime AV8AgendaAtendimento_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [8] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn])";
         scmdbuf = scmdbuf + " WHERE (T1.[AgendaAtendimento_CntSrcCod] = @AV7AgendaAtendimento_CntSrcCod)";
         scmdbuf = scmdbuf + " and (T1.[AgendaAtendimento_Data] = @AV8AgendaAtendimento_Data)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV21TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV22TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV25TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV26TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFAgendaAtendimento_QtdUnd) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV29TFAgendaAtendimento_QtdUnd)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFAgendaAtendimento_QtdUnd_To) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV30TFAgendaAtendimento_QtdUnd_To)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IH2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] );
               case 1 :
                     return conditional_H00IH3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IH2 ;
          prmH00IH2 = new Object[] {
          new Object[] {"@AV7AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV21TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV22TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV25TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV26TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV29TFAgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV30TFAgendaAtendimento_QtdUnd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IH3 ;
          prmH00IH3 = new Object[] {
          new Object[] {"@AV7AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV21TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV22TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV25TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV26TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV29TFAgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV30TFAgendaAtendimento_QtdUnd_To",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IH2,11,0,true,false )
             ,new CursorDef("H00IH3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IH3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                return;
       }
    }

 }

}
