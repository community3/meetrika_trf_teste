/*
               File: ContratoServicosUnidConversao
        Description: Unidades de Convers�o do Servi�os do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:56:33.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosunidconversao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO") == 0 )
         {
            AV12Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Contrato_Codigo), 6, 0)));
            AV13ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoServicos_UnidadeContratada), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATOSERVICOSUNIDCONVERSAO_CODIGO59231( AV12Contrato_Codigo, AV13ContratoServicos_UnidadeContratada) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A160ContratoServicos_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A2110ContratoServicosUnidConversao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicos_Codigo), "ZZZZZ9")));
               AV8ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratoServicosUnidConversao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicosUnidConversao_Codigo), "ZZZZZ9")));
               AV12Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Contrato_Codigo), 6, 0)));
               AV13ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoServicos_UnidadeContratada), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContratoServicosUnidConversao_Codigo.Name = "CONTRATOSERVICOSUNIDCONVERSAO_CODIGO";
         dynContratoServicosUnidConversao_Codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Unidades de Convers�o do Servi�os do Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosunidconversao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosunidconversao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicos_Codigo ,
                           int aP2_ContratoServicosUnidConversao_Codigo ,
                           ref int aP3_Contrato_Codigo ,
                           ref int aP4_ContratoServicos_UnidadeContratada )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicos_Codigo = aP1_ContratoServicos_Codigo;
         this.AV8ContratoServicosUnidConversao_Codigo = aP2_ContratoServicosUnidConversao_Codigo;
         this.AV12Contrato_Codigo = aP3_Contrato_Codigo;
         this.AV13ContratoServicos_UnidadeContratada = aP4_ContratoServicos_UnidadeContratada;
         executePrivate();
         aP3_Contrato_Codigo=this.AV12Contrato_Codigo;
         aP4_ContratoServicos_UnidadeContratada=this.AV13ContratoServicos_UnidadeContratada;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContratoServicosUnidConversao_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContratoServicosUnidConversao_Codigo.ItemCount > 0 )
         {
            A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( dynContratoServicosUnidConversao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_59231( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_59231e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicos_Codigo_Visible, edtContratoServicos_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosUnidConversao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_59231( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosunidconversaotitle_Internalname, "Convers�o USC", "", "", lblContratoservicosunidconversaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_59231( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_59231e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_59231( true) ;
         }
         return  ;
      }

      protected void wb_table3_29_59231e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_59231e( true) ;
         }
         else
         {
            wb_table1_2_59231e( false) ;
         }
      }

      protected void wb_table3_29_59231( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_59231e( true) ;
         }
         else
         {
            wb_table3_29_59231e( false) ;
         }
      }

      protected void wb_table2_8_59231( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_59231( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_59231e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_59231e( true) ;
         }
         else
         {
            wb_table2_8_59231e( false) ;
         }
      }

      protected void wb_table4_16_59231( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosunidconversao_codigo_Internalname, "Unidade", "", "", lblTextblockcontratoservicosunidconversao_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratoServicosUnidConversao_Codigo, dynContratoServicosUnidConversao_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)), 1, dynContratoServicosUnidConversao_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratoServicosUnidConversao_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_ContratoServicosUnidConversao.htm");
            dynContratoServicosUnidConversao_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosUnidConversao_Codigo_Internalname, "Values", (String)(dynContratoServicosUnidConversao_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosunidconversao_conversao_Internalname, "Convers�o", "", "", lblTextblockcontratoservicosunidconversao_conversao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosUnidConversao_Conversao_Internalname, StringUtil.LTrim( StringUtil.NToC( A2114ContratoServicosUnidConversao_Conversao, 14, 5, ",", "")), ((edtContratoServicosUnidConversao_Conversao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosUnidConversao_Conversao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosUnidConversao_Conversao_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicosUnidConversao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_59231e( true) ;
         }
         else
         {
            wb_table4_16_59231e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11592 */
         E11592 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynContratoServicosUnidConversao_Codigo.CurrentValue = cgiGet( dynContratoServicosUnidConversao_Codigo_Internalname);
               A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( cgiGet( dynContratoServicosUnidConversao_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosUnidConversao_Conversao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosUnidConversao_Conversao_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosUnidConversao_Conversao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2114ContratoServicosUnidConversao_Conversao = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
               }
               else
               {
                  A2114ContratoServicosUnidConversao_Conversao = context.localUtil.CToN( cgiGet( edtContratoServicosUnidConversao_Conversao_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A160ContratoServicos_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               }
               else
               {
                  A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z160ContratoServicos_Codigo"), ",", "."));
               Z2110ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2110ContratoServicosUnidConversao_Codigo"), ",", "."));
               Z2114ContratoServicosUnidConversao_Conversao = context.localUtil.CToN( cgiGet( "Z2114ContratoServicosUnidConversao_Conversao"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOS_CODIGO"), ",", "."));
               AV8ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSUNIDCONVERSAO_CODIGO"), ",", "."));
               AV12Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATO_CODIGO"), ",", "."));
               AV13ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOS_UNIDADECONTRATADA"), ",", "."));
               A2111ContratoServicosUnidConversao_Nome = cgiGet( "CONTRATOSERVICOSUNIDCONVERSAO_NOME");
               n2111ContratoServicosUnidConversao_Nome = false;
               A2112ContratoServicosUnidConversao_Sigla = cgiGet( "CONTRATOSERVICOSUNIDCONVERSAO_SIGLA");
               n2112ContratoServicosUnidConversao_Sigla = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosUnidConversao";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A2110ContratoServicosUnidConversao_Codigo != Z2110ContratoServicosUnidConversao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicosunidconversao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode231 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode231;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound231 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_590( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSERVICOS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11592 */
                           E11592 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12592 */
                           E12592 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12592 */
            E12592 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll59231( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes59231( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_590( )
      {
         BeforeValidate59231( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls59231( ) ;
            }
            else
            {
               CheckExtendedTable59231( ) ;
               CloseExtendedTableCursors59231( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption590( )
      {
      }

      protected void E11592( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtContratoServicos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Visible), 5, 0)));
      }

      protected void E12592( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicosunidconversao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV12Contrato_Codigo,(int)AV13ContratoServicos_UnidadeContratada});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM59231( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2114ContratoServicosUnidConversao_Conversao = T00593_A2114ContratoServicosUnidConversao_Conversao[0];
            }
            else
            {
               Z2114ContratoServicosUnidConversao_Conversao = A2114ContratoServicosUnidConversao_Conversao;
            }
         }
         if ( GX_JID == -9 )
         {
            Z2114ContratoServicosUnidConversao_Conversao = A2114ContratoServicosUnidConversao_Conversao;
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z2110ContratoServicosUnidConversao_Codigo = A2110ContratoServicosUnidConversao_Codigo;
            Z2111ContratoServicosUnidConversao_Nome = A2111ContratoServicosUnidConversao_Nome;
            Z2112ContratoServicosUnidConversao_Sigla = A2112ContratoServicosUnidConversao_Sigla;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicos_Codigo) )
         {
            A160ContratoServicos_Codigo = AV7ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         if ( ! (0==AV7ContratoServicos_Codigo) )
         {
            edtContratoServicos_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContratoServicos_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7ContratoServicos_Codigo) )
         {
            edtContratoServicos_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8ContratoServicosUnidConversao_Codigo) )
         {
            A2110ContratoServicosUnidConversao_Codigo = AV8ContratoServicosUnidConversao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         }
         if ( ! (0==AV8ContratoServicosUnidConversao_Codigo) )
         {
            dynContratoServicosUnidConversao_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosUnidConversao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosUnidConversao_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynContratoServicosUnidConversao_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosUnidConversao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosUnidConversao_Codigo.Enabled), 5, 0)));
         }
         if ( ! (0==AV8ContratoServicosUnidConversao_Codigo) )
         {
            dynContratoServicosUnidConversao_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosUnidConversao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosUnidConversao_Codigo.Enabled), 5, 0)));
         }
         GXACONTRATOSERVICOSUNIDCONVERSAO_CODIGO_html59231( AV12Contrato_Codigo, AV13ContratoServicos_UnidadeContratada) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00595 */
            pr_default.execute(3, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
            A2111ContratoServicosUnidConversao_Nome = T00595_A2111ContratoServicosUnidConversao_Nome[0];
            n2111ContratoServicosUnidConversao_Nome = T00595_n2111ContratoServicosUnidConversao_Nome[0];
            A2112ContratoServicosUnidConversao_Sigla = T00595_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = T00595_n2112ContratoServicosUnidConversao_Sigla[0];
            pr_default.close(3);
         }
      }

      protected void Load59231( )
      {
         /* Using cursor T00596 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound231 = 1;
            A2111ContratoServicosUnidConversao_Nome = T00596_A2111ContratoServicosUnidConversao_Nome[0];
            n2111ContratoServicosUnidConversao_Nome = T00596_n2111ContratoServicosUnidConversao_Nome[0];
            A2112ContratoServicosUnidConversao_Sigla = T00596_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = T00596_n2112ContratoServicosUnidConversao_Sigla[0];
            A2114ContratoServicosUnidConversao_Conversao = T00596_A2114ContratoServicosUnidConversao_Conversao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
            ZM59231( -9) ;
         }
         pr_default.close(4);
         OnLoadActions59231( ) ;
      }

      protected void OnLoadActions59231( )
      {
      }

      protected void CheckExtendedTable59231( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00594 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00595 */
         pr_default.execute(3, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Unidade de Convers�o'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSUNIDCONVERSAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2111ContratoServicosUnidConversao_Nome = T00595_A2111ContratoServicosUnidConversao_Nome[0];
         n2111ContratoServicosUnidConversao_Nome = T00595_n2111ContratoServicosUnidConversao_Nome[0];
         A2112ContratoServicosUnidConversao_Sigla = T00595_A2112ContratoServicosUnidConversao_Sigla[0];
         n2112ContratoServicosUnidConversao_Sigla = T00595_n2112ContratoServicosUnidConversao_Sigla[0];
         pr_default.close(3);
         if ( (Convert.ToDecimal(0)==A2114ContratoServicosUnidConversao_Conversao) )
         {
            GX_msglist.addItem("Convers�o � obrigat�rio.", 1, "CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosUnidConversao_Conversao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors59231( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A160ContratoServicos_Codigo )
      {
         /* Using cursor T00597 */
         pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_11( int A2110ContratoServicosUnidConversao_Codigo )
      {
         /* Using cursor T00598 */
         pr_default.execute(6, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Unidade de Convers�o'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSUNIDCONVERSAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2111ContratoServicosUnidConversao_Nome = T00598_A2111ContratoServicosUnidConversao_Nome[0];
         n2111ContratoServicosUnidConversao_Nome = T00598_n2111ContratoServicosUnidConversao_Nome[0];
         A2112ContratoServicosUnidConversao_Sigla = T00598_A2112ContratoServicosUnidConversao_Sigla[0];
         n2112ContratoServicosUnidConversao_Sigla = T00598_n2112ContratoServicosUnidConversao_Sigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2111ContratoServicosUnidConversao_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey59231( )
      {
         /* Using cursor T00599 */
         pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound231 = 1;
         }
         else
         {
            RcdFound231 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00593 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM59231( 9) ;
            RcdFound231 = 1;
            A2114ContratoServicosUnidConversao_Conversao = T00593_A2114ContratoServicosUnidConversao_Conversao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
            A160ContratoServicos_Codigo = T00593_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A2110ContratoServicosUnidConversao_Codigo = T00593_A2110ContratoServicosUnidConversao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z2110ContratoServicosUnidConversao_Codigo = A2110ContratoServicosUnidConversao_Codigo;
            sMode231 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load59231( ) ;
            if ( AnyError == 1 )
            {
               RcdFound231 = 0;
               InitializeNonKey59231( ) ;
            }
            Gx_mode = sMode231;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound231 = 0;
            InitializeNonKey59231( ) ;
            sMode231 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode231;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey59231( ) ;
         if ( RcdFound231 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound231 = 0;
         /* Using cursor T005910 */
         pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T005910_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T005910_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T005910_A2110ContratoServicosUnidConversao_Codigo[0] < A2110ContratoServicosUnidConversao_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T005910_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T005910_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T005910_A2110ContratoServicosUnidConversao_Codigo[0] > A2110ContratoServicosUnidConversao_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T005910_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A2110ContratoServicosUnidConversao_Codigo = T005910_A2110ContratoServicosUnidConversao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
               RcdFound231 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound231 = 0;
         /* Using cursor T005911 */
         pr_default.execute(9, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T005911_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T005911_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T005911_A2110ContratoServicosUnidConversao_Codigo[0] > A2110ContratoServicosUnidConversao_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T005911_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T005911_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T005911_A2110ContratoServicosUnidConversao_Codigo[0] < A2110ContratoServicosUnidConversao_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T005911_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A2110ContratoServicosUnidConversao_Codigo = T005911_A2110ContratoServicosUnidConversao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
               RcdFound231 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey59231( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert59231( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound231 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A2110ContratoServicosUnidConversao_Codigo != Z2110ContratoServicosUnidConversao_Codigo ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A2110ContratoServicosUnidConversao_Codigo = Z2110ContratoServicosUnidConversao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update59231( ) ;
                  GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A2110ContratoServicosUnidConversao_Codigo != Z2110ContratoServicosUnidConversao_Codigo ) )
               {
                  /* Insert record */
                  GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert59231( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert59231( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A2110ContratoServicosUnidConversao_Codigo != Z2110ContratoServicosUnidConversao_Codigo ) )
         {
            A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A2110ContratoServicosUnidConversao_Codigo = Z2110ContratoServicosUnidConversao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency59231( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00592 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosUnidConversao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2114ContratoServicosUnidConversao_Conversao != T00592_A2114ContratoServicosUnidConversao_Conversao[0] ) )
            {
               if ( Z2114ContratoServicosUnidConversao_Conversao != T00592_A2114ContratoServicosUnidConversao_Conversao[0] )
               {
                  GXUtil.WriteLog("contratoservicosunidconversao:[seudo value changed for attri]"+"ContratoServicosUnidConversao_Conversao");
                  GXUtil.WriteLogRaw("Old: ",Z2114ContratoServicosUnidConversao_Conversao);
                  GXUtil.WriteLogRaw("Current: ",T00592_A2114ContratoServicosUnidConversao_Conversao[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosUnidConversao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert59231( )
      {
         BeforeValidate59231( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable59231( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM59231( 0) ;
            CheckOptimisticConcurrency59231( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm59231( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert59231( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005912 */
                     pr_default.execute(10, new Object[] {A2114ContratoServicosUnidConversao_Conversao, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosUnidConversao") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption590( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load59231( ) ;
            }
            EndLevel59231( ) ;
         }
         CloseExtendedTableCursors59231( ) ;
      }

      protected void Update59231( )
      {
         BeforeValidate59231( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable59231( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency59231( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm59231( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate59231( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005913 */
                     pr_default.execute(11, new Object[] {A2114ContratoServicosUnidConversao_Conversao, A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosUnidConversao") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosUnidConversao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate59231( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel59231( ) ;
         }
         CloseExtendedTableCursors59231( ) ;
      }

      protected void DeferredUpdate59231( )
      {
      }

      protected void delete( )
      {
         BeforeValidate59231( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency59231( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls59231( ) ;
            AfterConfirm59231( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete59231( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005914 */
                  pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosUnidConversao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode231 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel59231( ) ;
         Gx_mode = sMode231;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls59231( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005915 */
            pr_default.execute(13, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
            A2111ContratoServicosUnidConversao_Nome = T005915_A2111ContratoServicosUnidConversao_Nome[0];
            n2111ContratoServicosUnidConversao_Nome = T005915_n2111ContratoServicosUnidConversao_Nome[0];
            A2112ContratoServicosUnidConversao_Sigla = T005915_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = T005915_n2112ContratoServicosUnidConversao_Sigla[0];
            pr_default.close(13);
         }
      }

      protected void EndLevel59231( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete59231( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "ContratoServicosUnidConversao");
            if ( AnyError == 0 )
            {
               ConfirmValues590( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "ContratoServicosUnidConversao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart59231( )
      {
         /* Scan By routine */
         /* Using cursor T005916 */
         pr_default.execute(14);
         RcdFound231 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound231 = 1;
            A160ContratoServicos_Codigo = T005916_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A2110ContratoServicosUnidConversao_Codigo = T005916_A2110ContratoServicosUnidConversao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext59231( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound231 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound231 = 1;
            A160ContratoServicos_Codigo = T005916_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A2110ContratoServicosUnidConversao_Codigo = T005916_A2110ContratoServicosUnidConversao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd59231( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm59231( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert59231( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate59231( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete59231( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete59231( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate59231( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes59231( )
      {
         dynContratoServicosUnidConversao_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoServicosUnidConversao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoServicosUnidConversao_Codigo.Enabled), 5, 0)));
         edtContratoServicosUnidConversao_Conversao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosUnidConversao_Conversao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosUnidConversao_Conversao_Enabled), 5, 0)));
         edtContratoServicos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues590( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812563513");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicos_Codigo) + "," + UrlEncode("" +AV8ContratoServicosUnidConversao_Codigo) + "," + UrlEncode("" +AV12Contrato_Codigo) + "," + UrlEncode("" +AV13ContratoServicos_UnidadeContratada)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2110ContratoServicosUnidConversao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.NToC( Z2114ContratoServicosUnidConversao_Conversao, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSUNIDCONVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContratoServicosUnidConversao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_UNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13ContratoServicos_UnidadeContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSUNIDCONVERSAO_NOME", StringUtil.RTrim( A2111ContratoServicosUnidConversao_Nome));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSUNIDCONVERSAO_SIGLA", StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratoServicosUnidConversao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosUnidConversao";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosunidconversao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicos_Codigo) + "," + UrlEncode("" +AV8ContratoServicosUnidConversao_Codigo) + "," + UrlEncode("" +AV12Contrato_Codigo) + "," + UrlEncode("" +AV13ContratoServicos_UnidadeContratada) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosUnidConversao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Unidades de Convers�o do Servi�os do Contrato" ;
      }

      protected void InitializeNonKey59231( )
      {
         A2111ContratoServicosUnidConversao_Nome = "";
         n2111ContratoServicosUnidConversao_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2111ContratoServicosUnidConversao_Nome", A2111ContratoServicosUnidConversao_Nome);
         A2112ContratoServicosUnidConversao_Sigla = "";
         n2112ContratoServicosUnidConversao_Sigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2112ContratoServicosUnidConversao_Sigla", A2112ContratoServicosUnidConversao_Sigla);
         A2114ContratoServicosUnidConversao_Conversao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
         Z2114ContratoServicosUnidConversao_Conversao = 0;
      }

      protected void InitAll59231( )
      {
         A160ContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         A2110ContratoServicosUnidConversao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         InitializeNonKey59231( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812563530");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosunidconversao.js", "?202051812563530");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblContratoservicosunidconversaotitle_Internalname = "CONTRATOSERVICOSUNIDCONVERSAOTITLE";
         lblTextblockcontratoservicosunidconversao_codigo_Internalname = "TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_CODIGO";
         dynContratoServicosUnidConversao_Codigo_Internalname = "CONTRATOSERVICOSUNIDCONVERSAO_CODIGO";
         lblTextblockcontratoservicosunidconversao_conversao_Internalname = "TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO";
         edtContratoServicosUnidConversao_Conversao_Internalname = "CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicos_Codigo_Internalname = "CONTRATOSERVICOS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Unidades de Convers�o do Servi�os do Contrato";
         edtContratoServicosUnidConversao_Conversao_Jsonclick = "";
         edtContratoServicosUnidConversao_Conversao_Enabled = 1;
         dynContratoServicosUnidConversao_Codigo_Jsonclick = "";
         dynContratoServicosUnidConversao_Codigo.Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Enabled = 1;
         edtContratoServicos_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATOSERVICOSUNIDCONVERSAO_CODIGO59231( int AV12Contrato_Codigo ,
                                                                     int AV13ContratoServicos_UnidadeContratada )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOSERVICOSUNIDCONVERSAO_CODIGO_data59231( AV12Contrato_Codigo, AV13ContratoServicos_UnidadeContratada) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOSERVICOSUNIDCONVERSAO_CODIGO_html59231( int AV12Contrato_Codigo ,
                                                                        int AV13ContratoServicos_UnidadeContratada )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOSERVICOSUNIDCONVERSAO_CODIGO_data59231( AV12Contrato_Codigo, AV13ContratoServicos_UnidadeContratada) ;
         gxdynajaxindex = 1;
         dynContratoServicosUnidConversao_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoServicosUnidConversao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOSERVICOSUNIDCONVERSAO_CODIGO_data59231( int AV12Contrato_Codigo ,
                                                                          int AV13ContratoServicos_UnidadeContratada )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T005917 */
         pr_default.execute(15, new Object[] {AV13ContratoServicos_UnidadeContratada, AV12Contrato_Codigo});
         while ( (pr_default.getStatus(15) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005917_A1204ContratoUnidades_UndMedCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T005917_A1205ContratoUnidades_UndMedNom[0]));
            pr_default.readNext(15);
         }
         pr_default.close(15);
      }

      public void Valid_Contratoservicosunidconversao_codigo( GXCombobox dynGX_Parm1 ,
                                                              String GX_Parm2 ,
                                                              String GX_Parm3 )
      {
         dynContratoServicosUnidConversao_Codigo = dynGX_Parm1;
         A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( dynContratoServicosUnidConversao_Codigo.CurrentValue, "."));
         A2111ContratoServicosUnidConversao_Nome = GX_Parm2;
         n2111ContratoServicosUnidConversao_Nome = false;
         A2112ContratoServicosUnidConversao_Sigla = GX_Parm3;
         n2112ContratoServicosUnidConversao_Sigla = false;
         /* Using cursor T005918 */
         pr_default.execute(16, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Unidade de Convers�o'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSUNIDCONVERSAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynContratoServicosUnidConversao_Codigo_Internalname;
         }
         A2111ContratoServicosUnidConversao_Nome = T005918_A2111ContratoServicosUnidConversao_Nome[0];
         n2111ContratoServicosUnidConversao_Nome = T005918_n2111ContratoServicosUnidConversao_Nome[0];
         A2112ContratoServicosUnidConversao_Sigla = T005918_A2112ContratoServicosUnidConversao_Sigla[0];
         n2112ContratoServicosUnidConversao_Sigla = T005918_n2112ContratoServicosUnidConversao_Sigla[0];
         pr_default.close(16);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2111ContratoServicosUnidConversao_Nome = "";
            n2111ContratoServicosUnidConversao_Nome = false;
            A2112ContratoServicosUnidConversao_Sigla = "";
            n2112ContratoServicosUnidConversao_Sigla = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A2111ContratoServicosUnidConversao_Nome));
         isValidOutput.Add(StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicos_codigo( int GX_Parm1 )
      {
         A160ContratoServicos_Codigo = GX_Parm1;
         /* Using cursor T005919 */
         pr_default.execute(17, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8ContratoServicosUnidConversao_Codigo',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV12Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12592',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(16);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         lblContratoservicosunidconversaotitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockcontratoservicosunidconversao_codigo_Jsonclick = "";
         lblTextblockcontratoservicosunidconversao_conversao_Jsonclick = "";
         A2111ContratoServicosUnidConversao_Nome = "";
         A2112ContratoServicosUnidConversao_Sigla = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode231 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z2111ContratoServicosUnidConversao_Nome = "";
         Z2112ContratoServicosUnidConversao_Sigla = "";
         T00595_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         T00595_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         T00595_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         T00595_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         T00596_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         T00596_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         T00596_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         T00596_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         T00596_A2114ContratoServicosUnidConversao_Conversao = new decimal[1] ;
         T00596_A160ContratoServicos_Codigo = new int[1] ;
         T00596_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T00594_A160ContratoServicos_Codigo = new int[1] ;
         T00597_A160ContratoServicos_Codigo = new int[1] ;
         T00598_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         T00598_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         T00598_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         T00598_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         T00599_A160ContratoServicos_Codigo = new int[1] ;
         T00599_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T00593_A2114ContratoServicosUnidConversao_Conversao = new decimal[1] ;
         T00593_A160ContratoServicos_Codigo = new int[1] ;
         T00593_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T005910_A160ContratoServicos_Codigo = new int[1] ;
         T005910_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T005911_A160ContratoServicos_Codigo = new int[1] ;
         T005911_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T00592_A2114ContratoServicosUnidConversao_Conversao = new decimal[1] ;
         T00592_A160ContratoServicos_Codigo = new int[1] ;
         T00592_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T005915_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         T005915_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         T005915_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         T005915_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         T005916_A160ContratoServicos_Codigo = new int[1] ;
         T005916_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005917_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T005917_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T005917_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T005917_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T005918_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         T005918_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         T005918_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         T005918_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T005919_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosunidconversao__default(),
            new Object[][] {
                new Object[] {
               T00592_A2114ContratoServicosUnidConversao_Conversao, T00592_A160ContratoServicos_Codigo, T00592_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T00593_A2114ContratoServicosUnidConversao_Conversao, T00593_A160ContratoServicos_Codigo, T00593_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T00594_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T00595_A2111ContratoServicosUnidConversao_Nome, T00595_n2111ContratoServicosUnidConversao_Nome, T00595_A2112ContratoServicosUnidConversao_Sigla, T00595_n2112ContratoServicosUnidConversao_Sigla
               }
               , new Object[] {
               T00596_A2111ContratoServicosUnidConversao_Nome, T00596_n2111ContratoServicosUnidConversao_Nome, T00596_A2112ContratoServicosUnidConversao_Sigla, T00596_n2112ContratoServicosUnidConversao_Sigla, T00596_A2114ContratoServicosUnidConversao_Conversao, T00596_A160ContratoServicos_Codigo, T00596_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T00597_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T00598_A2111ContratoServicosUnidConversao_Nome, T00598_n2111ContratoServicosUnidConversao_Nome, T00598_A2112ContratoServicosUnidConversao_Sigla, T00598_n2112ContratoServicosUnidConversao_Sigla
               }
               , new Object[] {
               T00599_A160ContratoServicos_Codigo, T00599_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T005910_A160ContratoServicos_Codigo, T005910_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T005911_A160ContratoServicos_Codigo, T005911_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005915_A2111ContratoServicosUnidConversao_Nome, T005915_n2111ContratoServicosUnidConversao_Nome, T005915_A2112ContratoServicosUnidConversao_Sigla, T005915_n2112ContratoServicosUnidConversao_Sigla
               }
               , new Object[] {
               T005916_A160ContratoServicos_Codigo, T005916_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T005917_A1204ContratoUnidades_UndMedCod, T005917_A1205ContratoUnidades_UndMedNom, T005917_n1205ContratoUnidades_UndMedNom, T005917_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               T005918_A2111ContratoServicosUnidConversao_Nome, T005918_n2111ContratoServicosUnidConversao_Nome, T005918_A2112ContratoServicosUnidConversao_Sigla, T005918_n2112ContratoServicosUnidConversao_Sigla
               }
               , new Object[] {
               T005919_A160ContratoServicos_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound231 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicos_Codigo ;
      private int wcpOAV8ContratoServicosUnidConversao_Codigo ;
      private int wcpOAV12Contrato_Codigo ;
      private int wcpOAV13ContratoServicos_UnidadeContratada ;
      private int Z160ContratoServicos_Codigo ;
      private int Z2110ContratoServicosUnidConversao_Codigo ;
      private int AV12Contrato_Codigo ;
      private int AV13ContratoServicos_UnidadeContratada ;
      private int A160ContratoServicos_Codigo ;
      private int A2110ContratoServicosUnidConversao_Codigo ;
      private int AV7ContratoServicos_Codigo ;
      private int AV8ContratoServicosUnidConversao_Codigo ;
      private int trnEnded ;
      private int edtContratoServicos_Codigo_Visible ;
      private int edtContratoServicos_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtContratoServicosUnidConversao_Conversao_Enabled ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z2114ContratoServicosUnidConversao_Conversao ;
      private decimal A2114ContratoServicosUnidConversao_Conversao ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynContratoServicosUnidConversao_Codigo_Internalname ;
      private String TempTags ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblContratoservicosunidconversaotitle_Internalname ;
      private String lblContratoservicosunidconversaotitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_codigo_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_codigo_Jsonclick ;
      private String dynContratoServicosUnidConversao_Codigo_Jsonclick ;
      private String lblTextblockcontratoservicosunidconversao_conversao_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_conversao_Jsonclick ;
      private String edtContratoServicosUnidConversao_Conversao_Internalname ;
      private String edtContratoServicosUnidConversao_Conversao_Jsonclick ;
      private String A2111ContratoServicosUnidConversao_Nome ;
      private String A2112ContratoServicosUnidConversao_Sigla ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode231 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z2111ContratoServicosUnidConversao_Nome ;
      private String Z2112ContratoServicosUnidConversao_Sigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2111ContratoServicosUnidConversao_Nome ;
      private bool n2112ContratoServicosUnidConversao_Sigla ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV11WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP3_Contrato_Codigo ;
      private int aP4_ContratoServicos_UnidadeContratada ;
      private GXCombobox dynContratoServicosUnidConversao_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T00595_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] T00595_n2111ContratoServicosUnidConversao_Nome ;
      private String[] T00595_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] T00595_n2112ContratoServicosUnidConversao_Sigla ;
      private String[] T00596_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] T00596_n2111ContratoServicosUnidConversao_Nome ;
      private String[] T00596_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] T00596_n2112ContratoServicosUnidConversao_Sigla ;
      private decimal[] T00596_A2114ContratoServicosUnidConversao_Conversao ;
      private int[] T00596_A160ContratoServicos_Codigo ;
      private int[] T00596_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] T00594_A160ContratoServicos_Codigo ;
      private int[] T00597_A160ContratoServicos_Codigo ;
      private String[] T00598_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] T00598_n2111ContratoServicosUnidConversao_Nome ;
      private String[] T00598_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] T00598_n2112ContratoServicosUnidConversao_Sigla ;
      private int[] T00599_A160ContratoServicos_Codigo ;
      private int[] T00599_A2110ContratoServicosUnidConversao_Codigo ;
      private decimal[] T00593_A2114ContratoServicosUnidConversao_Conversao ;
      private int[] T00593_A160ContratoServicos_Codigo ;
      private int[] T00593_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] T005910_A160ContratoServicos_Codigo ;
      private int[] T005910_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] T005911_A160ContratoServicos_Codigo ;
      private int[] T005911_A2110ContratoServicosUnidConversao_Codigo ;
      private decimal[] T00592_A2114ContratoServicosUnidConversao_Conversao ;
      private int[] T00592_A160ContratoServicos_Codigo ;
      private int[] T00592_A2110ContratoServicosUnidConversao_Codigo ;
      private String[] T005915_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] T005915_n2111ContratoServicosUnidConversao_Nome ;
      private String[] T005915_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] T005915_n2112ContratoServicosUnidConversao_Sigla ;
      private int[] T005916_A160ContratoServicos_Codigo ;
      private int[] T005916_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] T005917_A1204ContratoUnidades_UndMedCod ;
      private String[] T005917_A1205ContratoUnidades_UndMedNom ;
      private bool[] T005917_n1205ContratoUnidades_UndMedNom ;
      private int[] T005917_A1207ContratoUnidades_ContratoCod ;
      private String[] T005918_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] T005918_n2111ContratoServicosUnidConversao_Nome ;
      private String[] T005918_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] T005918_n2112ContratoServicosUnidConversao_Sigla ;
      private int[] T005919_A160ContratoServicos_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratoservicosunidconversao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00596 ;
          prmT00596 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00594 ;
          prmT00594 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00595 ;
          prmT00595 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00597 ;
          prmT00597 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00598 ;
          prmT00598 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00599 ;
          prmT00599 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00593 ;
          prmT00593 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005910 ;
          prmT005910 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005911 ;
          prmT005911 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00592 ;
          prmT00592 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005912 ;
          prmT005912 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Conversao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005913 ;
          prmT005913 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Conversao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005914 ;
          prmT005914 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005915 ;
          prmT005915 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005916 ;
          prmT005916 = new Object[] {
          } ;
          Object[] prmT005917 ;
          prmT005917 = new Object[] {
          new Object[] {"@AV13ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005918 ;
          prmT005918 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005919 ;
          prmT005919 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00592", "SELECT [ContratoServicosUnidConversao_Conversao], [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM [ContratoServicosUnidConversao] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00592,1,0,true,false )
             ,new CursorDef("T00593", "SELECT [ContratoServicosUnidConversao_Conversao], [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00593,1,0,true,false )
             ,new CursorDef("T00594", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00594,1,0,true,false )
             ,new CursorDef("T00595", "SELECT [UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome, [UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicosUnidConversao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00595,1,0,true,false )
             ,new CursorDef("T00596", "SELECT T2.[UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome, T2.[UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla, TM1.[ContratoServicosUnidConversao_Conversao], TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM ([ContratoServicosUnidConversao] TM1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = TM1.[ContratoServicosUnidConversao_Codigo]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosUnidConversao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00596,100,0,true,false )
             ,new CursorDef("T00597", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00597,1,0,true,false )
             ,new CursorDef("T00598", "SELECT [UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome, [UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicosUnidConversao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00598,1,0,true,false )
             ,new CursorDef("T00599", "SELECT [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00599,1,0,true,false )
             ,new CursorDef("T005910", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] > @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosUnidConversao_Codigo] > @ContratoServicosUnidConversao_Codigo) ORDER BY [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005910,1,0,true,true )
             ,new CursorDef("T005911", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] < @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosUnidConversao_Codigo] < @ContratoServicosUnidConversao_Codigo) ORDER BY [ContratoServicos_Codigo] DESC, [ContratoServicosUnidConversao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005911,1,0,true,true )
             ,new CursorDef("T005912", "INSERT INTO [ContratoServicosUnidConversao]([ContratoServicosUnidConversao_Conversao], [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo]) VALUES(@ContratoServicosUnidConversao_Conversao, @ContratoServicos_Codigo, @ContratoServicosUnidConversao_Codigo)", GxErrorMask.GX_NOMASK,prmT005912)
             ,new CursorDef("T005913", "UPDATE [ContratoServicosUnidConversao] SET [ContratoServicosUnidConversao_Conversao]=@ContratoServicosUnidConversao_Conversao  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo", GxErrorMask.GX_NOMASK,prmT005913)
             ,new CursorDef("T005914", "DELETE FROM [ContratoServicosUnidConversao]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo", GxErrorMask.GX_NOMASK,prmT005914)
             ,new CursorDef("T005915", "SELECT [UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome, [UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicosUnidConversao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005915,1,0,true,false )
             ,new CursorDef("T005916", "SELECT [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo FROM [ContratoServicosUnidConversao] WITH (NOLOCK) ORDER BY [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005916,100,0,true,false )
             ,new CursorDef("T005917", "SELECT T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_ContratoCod] FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod]) WHERE (T1.[ContratoUnidades_UndMedCod] <> @AV13ContratoServicos_UnidadeContratada) AND (T1.[ContratoUnidades_ContratoCod] = @AV12Contrato_Codigo) ORDER BY T2.[UnidadeMedicao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005917,0,0,true,false )
             ,new CursorDef("T005918", "SELECT [UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome, [UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicosUnidConversao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005918,1,0,true,false )
             ,new CursorDef("T005919", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005919,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
