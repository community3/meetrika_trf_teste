/*
               File: PromptAmbienteTecnologico
        Description: Selecione Ambiente Tecnologico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:57:26.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptambientetecnologico : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptambientetecnologico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptambientetecnologico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutAmbienteTecnologico_Codigo ,
                           ref String aP1_InOutAmbienteTecnologico_Descricao )
      {
         this.AV7InOutAmbienteTecnologico_Codigo = aP0_InOutAmbienteTecnologico_Codigo;
         this.AV8InOutAmbienteTecnologico_Descricao = aP1_InOutAmbienteTecnologico_Descricao;
         executePrivate();
         aP0_InOutAmbienteTecnologico_Codigo=this.AV7InOutAmbienteTecnologico_Codigo;
         aP1_InOutAmbienteTecnologico_Descricao=this.AV8InOutAmbienteTecnologico_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavAmbientetecnologico_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkAmbienteTecnologico_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_69 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_69_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_69_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV33AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17AmbienteTecnologico_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AmbienteTecnologico_Descricao1", AV17AmbienteTecnologico_Descricao1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV22AmbienteTecnologico_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22AmbienteTecnologico_Descricao2", AV22AmbienteTecnologico_Descricao2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV27AmbienteTecnologico_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV35TFAmbienteTecnologico_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAmbienteTecnologico_Descricao", AV35TFAmbienteTecnologico_Descricao);
               AV36TFAmbienteTecnologico_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAmbienteTecnologico_Descricao_Sel", AV36TFAmbienteTecnologico_Descricao_Sel);
               AV39TFAmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFAmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
               AV40TFAmbienteTecnologico_AreaTrabalhoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFAmbienteTecnologico_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0)));
               AV43TFAmbienteTecnologico_AreaTrabalhoDes = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAmbienteTecnologico_AreaTrabalhoDes", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
               AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel", AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel);
               AV47TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0));
               AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
               AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace", AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace);
               AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace", AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace);
               AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace", AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace);
               AV56Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutAmbienteTecnologico_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAmbienteTecnologico_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutAmbienteTecnologico_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAmbienteTecnologico_Descricao", AV8InOutAmbienteTecnologico_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA9V2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV56Pgmname = "PromptAmbienteTecnologico";
               context.Gx_err = 0;
               WS9V2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE9V2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623572665");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptambientetecnologico.aspx") + "?" + UrlEncode("" +AV7InOutAmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutAmbienteTecnologico_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_DESCRICAO1", AV17AmbienteTecnologico_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_DESCRICAO2", AV22AmbienteTecnologico_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_DESCRICAO3", AV27AmbienteTecnologico_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO", AV35TFAmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL", AV36TFAmbienteTecnologico_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHODES", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL", AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_69", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_69), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV34AmbienteTecnologico_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV34AmbienteTecnologico_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_AREATRABALHOCODTITLEFILTERDATA", AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_AREATRABALHOCODTITLEFILTERDATA", AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_AREATRABALHODESTITLEFILTERDATA", AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_AREATRABALHODESTITLEFILTERDATA", AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA", AV46AmbienteTecnologico_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA", AV46AmbienteTecnologico_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV56Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTAMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutAmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTAMBIENTETECNOLOGICO_DESCRICAO", AV8InOutAmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_areatrabalhocod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhodes_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhodes_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhodes_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhodes_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_areatrabalhodes_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalistproc", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_areatrabalhodes_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_ativo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9V2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptAmbienteTecnologico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Ambiente Tecnologico" ;
      }

      protected void WB9V0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_9V2( true) ;
         }
         else
         {
            wb_table1_2_9V2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_Internalname, AV35TFAmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( AV35TFAmbienteTecnologico_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_sel_Internalname, AV36TFAmbienteTecnologico_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV36TFAmbienteTecnologico_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_areatrabalhocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_areatrabalhocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_areatrabalhocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_areatrabalhocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_areatrabalhocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_areatrabalhodes_Internalname, AV43TFAmbienteTecnologico_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( AV43TFAmbienteTecnologico_AreaTrabalhoDes, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_areatrabalhodes_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_areatrabalhodes_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_areatrabalhodes_sel_Internalname, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, StringUtil.RTrim( context.localUtil.Format( AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_areatrabalhodes_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_areatrabalhodes_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_AREATRABALHOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Internalname, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_AREATRABALHODESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Internalname, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptAmbienteTecnologico.htm");
         }
         wbLoad = true;
      }

      protected void START9V2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Ambiente Tecnologico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9V0( ) ;
      }

      protected void WS9V2( )
      {
         START9V2( ) ;
         EVT9V2( ) ;
      }

      protected void EVT9V2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E119V2 */
                           E119V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E129V2 */
                           E129V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E139V2 */
                           E139V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E149V2 */
                           E149V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E159V2 */
                           E159V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E169V2 */
                           E169V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E179V2 */
                           E179V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E189V2 */
                           E189V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E199V2 */
                           E199V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E209V2 */
                           E209V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E219V2 */
                           E219V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E229V2 */
                           E229V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E239V2 */
                           E239V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E249V2 */
                           E249V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E259V2 */
                           E259V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_69_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
                           SubsflControlProps_692( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV55Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_Codigo_Internalname), ",", "."));
                           A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
                           A728AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_AreaTrabalhoCod_Internalname), ",", "."));
                           A729AmbienteTecnologico_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_AreaTrabalhoDes_Internalname));
                           n729AmbienteTecnologico_AreaTrabalhoDes = false;
                           A353AmbienteTecnologico_Ativo = StringUtil.StrToBool( cgiGet( chkAmbienteTecnologico_Ativo_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E269V2 */
                                 E269V2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E279V2 */
                                 E279V2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E289V2 */
                                 E289V2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ambientetecnologico_areatrabalhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vAMBIENTETECNOLOGICO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV33AmbienteTecnologico_AreaTrabalhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ambientetecnologico_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO1"), AV17AmbienteTecnologico_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ambientetecnologico_descricao2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO2"), AV22AmbienteTecnologico_Descricao2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ambientetecnologico_descricao3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO3"), AV27AmbienteTecnologico_Descricao3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV35TFAmbienteTecnologico_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV36TFAmbienteTecnologico_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_areatrabalhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV39TFAmbienteTecnologico_AreaTrabalhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_areatrabalhocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV40TFAmbienteTecnologico_AreaTrabalhoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_areatrabalhodes Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHODES"), AV43TFAmbienteTecnologico_AreaTrabalhoDes) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_areatrabalhodes_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL"), AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV47TFAmbienteTecnologico_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E299V2 */
                                       E299V2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE9V2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9V2( ) ;
            }
         }
      }

      protected void PA9V2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavAmbientetecnologico_areatrabalhocod.Name = "vAMBIENTETECNOLOGICO_AREATRABALHOCOD";
            dynavAmbientetecnologico_areatrabalhocod.WebTags = "";
            dynavAmbientetecnologico_areatrabalhocod.removeAllItems();
            /* Using cursor H009V2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavAmbientetecnologico_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H009V2_A5AreaTrabalho_Codigo[0]), 6, 0)), H009V2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavAmbientetecnologico_areatrabalhocod.ItemCount > 0 )
            {
               AV33AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavAmbientetecnologico_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            GXCCtl = "AMBIENTETECNOLOGICO_ATIVO_" + sGXsfl_69_idx;
            chkAmbienteTecnologico_Ativo.Name = GXCCtl;
            chkAmbienteTecnologico_Ativo.WebTags = "";
            chkAmbienteTecnologico_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "TitleCaption", chkAmbienteTecnologico_Ativo.Caption);
            chkAmbienteTecnologico_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD9V1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_data9V1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_html9V1( )
      {
         int gxdynajaxvalue ;
         GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_data9V1( ) ;
         gxdynajaxindex = 1;
         dynavAmbientetecnologico_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAmbientetecnologico_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAmbientetecnologico_areatrabalhocod.ItemCount > 0 )
         {
            AV33AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavAmbientetecnologico_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_data9V1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H009V3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H009V3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H009V3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_692( ) ;
         while ( nGXsfl_69_idx <= nRC_GXsfl_69 )
         {
            sendrow_692( ) ;
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV33AmbienteTecnologico_AreaTrabalhoCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17AmbienteTecnologico_Descricao1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV22AmbienteTecnologico_Descricao2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       String AV27AmbienteTecnologico_Descricao3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV35TFAmbienteTecnologico_Descricao ,
                                       String AV36TFAmbienteTecnologico_Descricao_Sel ,
                                       int AV39TFAmbienteTecnologico_AreaTrabalhoCod ,
                                       int AV40TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                       String AV43TFAmbienteTecnologico_AreaTrabalhoDes ,
                                       String AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                       short AV47TFAmbienteTecnologico_Ativo_Sel ,
                                       String AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ,
                                       String AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace ,
                                       String AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace ,
                                       String AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace ,
                                       String AV56Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9V2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_DESCRICAO", A352AmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_ATIVO", GetSecureSignedToken( "", A353AmbienteTecnologico_Ativo));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_ATIVO", StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavAmbientetecnologico_areatrabalhocod.ItemCount > 0 )
         {
            AV33AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavAmbientetecnologico_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9V2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV56Pgmname = "PromptAmbienteTecnologico";
         context.Gx_err = 0;
      }

      protected void RF9V2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 69;
         /* Execute user event: E279V2 */
         E279V2 ();
         nGXsfl_69_idx = 1;
         sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
         SubsflControlProps_692( ) ;
         nGXsfl_69_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_692( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV33AmbienteTecnologico_AreaTrabalhoCod ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV17AmbienteTecnologico_Descricao1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV22AmbienteTecnologico_Descricao2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV27AmbienteTecnologico_Descricao3 ,
                                                 AV36TFAmbienteTecnologico_Descricao_Sel ,
                                                 AV35TFAmbienteTecnologico_Descricao ,
                                                 AV39TFAmbienteTecnologico_AreaTrabalhoCod ,
                                                 AV40TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                                 AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                                 AV43TFAmbienteTecnologico_AreaTrabalhoDes ,
                                                 AV47TFAmbienteTecnologico_Ativo_Sel ,
                                                 A728AmbienteTecnologico_AreaTrabalhoCod ,
                                                 A352AmbienteTecnologico_Descricao ,
                                                 A729AmbienteTecnologico_AreaTrabalhoDes ,
                                                 A353AmbienteTecnologico_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17AmbienteTecnologico_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AmbienteTecnologico_Descricao1", AV17AmbienteTecnologico_Descricao1);
            lV22AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22AmbienteTecnologico_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22AmbienteTecnologico_Descricao2", AV22AmbienteTecnologico_Descricao2);
            lV27AmbienteTecnologico_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV27AmbienteTecnologico_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
            lV35TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV35TFAmbienteTecnologico_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAmbienteTecnologico_Descricao", AV35TFAmbienteTecnologico_Descricao);
            lV43TFAmbienteTecnologico_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV43TFAmbienteTecnologico_AreaTrabalhoDes), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAmbienteTecnologico_AreaTrabalhoDes", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
            /* Using cursor H009V4 */
            pr_default.execute(2, new Object[] {AV33AmbienteTecnologico_AreaTrabalhoCod, lV17AmbienteTecnologico_Descricao1, lV22AmbienteTecnologico_Descricao2, lV27AmbienteTecnologico_Descricao3, lV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, lV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_69_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A353AmbienteTecnologico_Ativo = H009V4_A353AmbienteTecnologico_Ativo[0];
               A729AmbienteTecnologico_AreaTrabalhoDes = H009V4_A729AmbienteTecnologico_AreaTrabalhoDes[0];
               n729AmbienteTecnologico_AreaTrabalhoDes = H009V4_n729AmbienteTecnologico_AreaTrabalhoDes[0];
               A728AmbienteTecnologico_AreaTrabalhoCod = H009V4_A728AmbienteTecnologico_AreaTrabalhoCod[0];
               A352AmbienteTecnologico_Descricao = H009V4_A352AmbienteTecnologico_Descricao[0];
               A351AmbienteTecnologico_Codigo = H009V4_A351AmbienteTecnologico_Codigo[0];
               A729AmbienteTecnologico_AreaTrabalhoDes = H009V4_A729AmbienteTecnologico_AreaTrabalhoDes[0];
               n729AmbienteTecnologico_AreaTrabalhoDes = H009V4_n729AmbienteTecnologico_AreaTrabalhoDes[0];
               /* Execute user event: E289V2 */
               E289V2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 69;
            WB9V0( ) ;
         }
         nGXsfl_69_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV33AmbienteTecnologico_AreaTrabalhoCod ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV17AmbienteTecnologico_Descricao1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV22AmbienteTecnologico_Descricao2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV27AmbienteTecnologico_Descricao3 ,
                                              AV36TFAmbienteTecnologico_Descricao_Sel ,
                                              AV35TFAmbienteTecnologico_Descricao ,
                                              AV39TFAmbienteTecnologico_AreaTrabalhoCod ,
                                              AV40TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                              AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                              AV43TFAmbienteTecnologico_AreaTrabalhoDes ,
                                              AV47TFAmbienteTecnologico_Ativo_Sel ,
                                              A728AmbienteTecnologico_AreaTrabalhoCod ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A729AmbienteTecnologico_AreaTrabalhoDes ,
                                              A353AmbienteTecnologico_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17AmbienteTecnologico_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AmbienteTecnologico_Descricao1", AV17AmbienteTecnologico_Descricao1);
         lV22AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22AmbienteTecnologico_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22AmbienteTecnologico_Descricao2", AV22AmbienteTecnologico_Descricao2);
         lV27AmbienteTecnologico_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV27AmbienteTecnologico_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
         lV35TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV35TFAmbienteTecnologico_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAmbienteTecnologico_Descricao", AV35TFAmbienteTecnologico_Descricao);
         lV43TFAmbienteTecnologico_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV43TFAmbienteTecnologico_AreaTrabalhoDes), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAmbienteTecnologico_AreaTrabalhoDes", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
         /* Using cursor H009V5 */
         pr_default.execute(3, new Object[] {AV33AmbienteTecnologico_AreaTrabalhoCod, lV17AmbienteTecnologico_Descricao1, lV22AmbienteTecnologico_Descricao2, lV27AmbienteTecnologico_Descricao3, lV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, lV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel});
         GRID_nRecordCount = H009V5_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9V0( )
      {
         /* Before Start, stand alone formulas. */
         AV56Pgmname = "PromptAmbienteTecnologico";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E269V2 */
         E269V2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV49DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA"), AV34AmbienteTecnologico_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_AREATRABALHOCODTITLEFILTERDATA"), AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_AREATRABALHODESTITLEFILTERDATA"), AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA"), AV46AmbienteTecnologico_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavAmbientetecnologico_areatrabalhocod.Name = dynavAmbientetecnologico_areatrabalhocod_Internalname;
            dynavAmbientetecnologico_areatrabalhocod.CurrentValue = cgiGet( dynavAmbientetecnologico_areatrabalhocod_Internalname);
            AV33AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavAmbientetecnologico_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17AmbienteTecnologico_Descricao1 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AmbienteTecnologico_Descricao1", AV17AmbienteTecnologico_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV22AmbienteTecnologico_Descricao2 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22AmbienteTecnologico_Descricao2", AV22AmbienteTecnologico_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            AV27AmbienteTecnologico_Descricao3 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV35TFAmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAmbienteTecnologico_Descricao", AV35TFAmbienteTecnologico_Descricao);
            AV36TFAmbienteTecnologico_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAmbienteTecnologico_Descricao_Sel", AV36TFAmbienteTecnologico_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD");
               GX_FocusControl = edtavTfambientetecnologico_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFAmbienteTecnologico_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFAmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV39TFAmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFAmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_areatrabalhocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_areatrabalhocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO");
               GX_FocusControl = edtavTfambientetecnologico_areatrabalhocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFAmbienteTecnologico_AreaTrabalhoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFAmbienteTecnologico_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0)));
            }
            else
            {
               AV40TFAmbienteTecnologico_AreaTrabalhoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_areatrabalhocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFAmbienteTecnologico_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0)));
            }
            AV43TFAmbienteTecnologico_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_areatrabalhodes_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAmbienteTecnologico_AreaTrabalhoDes", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
            AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_areatrabalhodes_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel", AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_ATIVO_SEL");
               GX_FocusControl = edtavTfambientetecnologico_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFAmbienteTecnologico_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0));
            }
            else
            {
               AV47TFAmbienteTecnologico_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0));
            }
            AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
            AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace", AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace);
            AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace", AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace);
            AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace", AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_69 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_69"), ",", "."));
            AV51GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV52GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_ambientetecnologico_descricao_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption");
            Ddo_ambientetecnologico_descricao_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip");
            Ddo_ambientetecnologico_descricao_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls");
            Ddo_ambientetecnologico_descricao_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set");
            Ddo_ambientetecnologico_descricao_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set");
            Ddo_ambientetecnologico_descricao_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype");
            Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc"));
            Ddo_ambientetecnologico_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc"));
            Ddo_ambientetecnologico_descricao_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus");
            Ddo_ambientetecnologico_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter"));
            Ddo_ambientetecnologico_descricao_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype");
            Ddo_ambientetecnologico_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange"));
            Ddo_ambientetecnologico_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist"));
            Ddo_ambientetecnologico_descricao_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype");
            Ddo_ambientetecnologico_descricao_Datalistfixedvalues = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistfixedvalues");
            Ddo_ambientetecnologico_descricao_Datalistproc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc");
            Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_descricao_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc");
            Ddo_ambientetecnologico_descricao_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc");
            Ddo_ambientetecnologico_descricao_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata");
            Ddo_ambientetecnologico_descricao_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter");
            Ddo_ambientetecnologico_descricao_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterfrom");
            Ddo_ambientetecnologico_descricao_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Rangefilterto");
            Ddo_ambientetecnologico_descricao_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound");
            Ddo_ambientetecnologico_descricao_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext");
            Ddo_ambientetecnologico_areatrabalhocod_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Caption");
            Ddo_ambientetecnologico_areatrabalhocod_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Tooltip");
            Ddo_ambientetecnologico_areatrabalhocod_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Cls");
            Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtext_set");
            Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtextto_set");
            Ddo_ambientetecnologico_areatrabalhocod_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Dropdownoptionstype");
            Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_areatrabalhocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includesortasc"));
            Ddo_ambientetecnologico_areatrabalhocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includesortdsc"));
            Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Sortedstatus");
            Ddo_ambientetecnologico_areatrabalhocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includefilter"));
            Ddo_ambientetecnologico_areatrabalhocod_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filtertype");
            Ddo_ambientetecnologico_areatrabalhocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filterisrange"));
            Ddo_ambientetecnologico_areatrabalhocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Includedatalist"));
            Ddo_ambientetecnologico_areatrabalhocod_Datalistfixedvalues = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Datalistfixedvalues");
            Ddo_ambientetecnologico_areatrabalhocod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_areatrabalhocod_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Sortasc");
            Ddo_ambientetecnologico_areatrabalhocod_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Sortdsc");
            Ddo_ambientetecnologico_areatrabalhocod_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Loadingdata");
            Ddo_ambientetecnologico_areatrabalhocod_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Cleanfilter");
            Ddo_ambientetecnologico_areatrabalhocod_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Rangefilterfrom");
            Ddo_ambientetecnologico_areatrabalhocod_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Rangefilterto");
            Ddo_ambientetecnologico_areatrabalhocod_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Noresultsfound");
            Ddo_ambientetecnologico_areatrabalhocod_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Searchbuttontext");
            Ddo_ambientetecnologico_areatrabalhodes_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Caption");
            Ddo_ambientetecnologico_areatrabalhodes_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Tooltip");
            Ddo_ambientetecnologico_areatrabalhodes_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Cls");
            Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filteredtext_set");
            Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Selectedvalue_set");
            Ddo_ambientetecnologico_areatrabalhodes_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Dropdownoptionstype");
            Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_areatrabalhodes_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includesortasc"));
            Ddo_ambientetecnologico_areatrabalhodes_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includesortdsc"));
            Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Sortedstatus");
            Ddo_ambientetecnologico_areatrabalhodes_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includefilter"));
            Ddo_ambientetecnologico_areatrabalhodes_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filtertype");
            Ddo_ambientetecnologico_areatrabalhodes_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filterisrange"));
            Ddo_ambientetecnologico_areatrabalhodes_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Includedatalist"));
            Ddo_ambientetecnologico_areatrabalhodes_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalisttype");
            Ddo_ambientetecnologico_areatrabalhodes_Datalistfixedvalues = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalistfixedvalues");
            Ddo_ambientetecnologico_areatrabalhodes_Datalistproc = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalistproc");
            Ddo_ambientetecnologico_areatrabalhodes_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_areatrabalhodes_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Sortasc");
            Ddo_ambientetecnologico_areatrabalhodes_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Sortdsc");
            Ddo_ambientetecnologico_areatrabalhodes_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Loadingdata");
            Ddo_ambientetecnologico_areatrabalhodes_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Cleanfilter");
            Ddo_ambientetecnologico_areatrabalhodes_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Rangefilterfrom");
            Ddo_ambientetecnologico_areatrabalhodes_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Rangefilterto");
            Ddo_ambientetecnologico_areatrabalhodes_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Noresultsfound");
            Ddo_ambientetecnologico_areatrabalhodes_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Searchbuttontext");
            Ddo_ambientetecnologico_ativo_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Caption");
            Ddo_ambientetecnologico_ativo_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Tooltip");
            Ddo_ambientetecnologico_ativo_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Cls");
            Ddo_ambientetecnologico_ativo_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_set");
            Ddo_ambientetecnologico_ativo_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Dropdownoptionstype");
            Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortasc"));
            Ddo_ambientetecnologico_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortdsc"));
            Ddo_ambientetecnologico_ativo_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortedstatus");
            Ddo_ambientetecnologico_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includefilter"));
            Ddo_ambientetecnologico_ativo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Filterisrange"));
            Ddo_ambientetecnologico_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includedatalist"));
            Ddo_ambientetecnologico_ativo_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalisttype");
            Ddo_ambientetecnologico_ativo_Datalistfixedvalues = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalistfixedvalues");
            Ddo_ambientetecnologico_ativo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_ativo_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortasc");
            Ddo_ambientetecnologico_ativo_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortdsc");
            Ddo_ambientetecnologico_ativo_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Loadingdata");
            Ddo_ambientetecnologico_ativo_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Cleanfilter");
            Ddo_ambientetecnologico_ativo_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Rangefilterfrom");
            Ddo_ambientetecnologico_ativo_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Rangefilterto");
            Ddo_ambientetecnologico_ativo_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Noresultsfound");
            Ddo_ambientetecnologico_ativo_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_ambientetecnologico_descricao_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey");
            Ddo_ambientetecnologico_descricao_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get");
            Ddo_ambientetecnologico_descricao_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get");
            Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Activeeventkey");
            Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtext_get");
            Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD_Filteredtextto_get");
            Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Activeeventkey");
            Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Filteredtext_get");
            Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES_Selectedvalue_get");
            Ddo_ambientetecnologico_ativo_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Activeeventkey");
            Ddo_ambientetecnologico_ativo_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAMBIENTETECNOLOGICO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV33AmbienteTecnologico_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO1"), AV17AmbienteTecnologico_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO2"), AV22AmbienteTecnologico_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO3"), AV27AmbienteTecnologico_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV35TFAmbienteTecnologico_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV36TFAmbienteTecnologico_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV39TFAmbienteTecnologico_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV40TFAmbienteTecnologico_AreaTrabalhoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHODES"), AV43TFAmbienteTecnologico_AreaTrabalhoDes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL"), AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV47TFAmbienteTecnologico_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E269V2 */
         E269V2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E269V2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfambientetecnologico_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_sel_Visible), 5, 0)));
         edtavTfambientetecnologico_areatrabalhocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_areatrabalhocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_areatrabalhocod_Visible), 5, 0)));
         edtavTfambientetecnologico_areatrabalhocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_areatrabalhocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_areatrabalhocod_to_Visible), 5, 0)));
         edtavTfambientetecnologico_areatrabalhodes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_areatrabalhodes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_areatrabalhodes_Visible), 5, 0)));
         edtavTfambientetecnologico_areatrabalhodes_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_areatrabalhodes_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_areatrabalhodes_sel_Visible), 5, 0)));
         edtavTfambientetecnologico_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_ativo_sel_Visible), 5, 0)));
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace);
         AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_AreaTrabalhoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace);
         AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace = Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace", AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_AreaTrabalhoDes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace);
         AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace = Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace", AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace);
         AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace", AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Ambiente Tecnologico";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Tecnol�gico", 0);
         cmbavOrderedby.addItem("2", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("3", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("4", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV49DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV49DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E279V2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46AmbienteTecnologico_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAmbienteTecnologico_Descricao_Titleformat = 2;
         edtAmbienteTecnologico_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tecnol�gico", AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Descricao_Internalname, "Title", edtAmbienteTecnologico_Descricao_Title);
         edtAmbienteTecnologico_AreaTrabalhoCod_Titleformat = 2;
         edtAmbienteTecnologico_AreaTrabalhoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�rea de Trabalho", AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_AreaTrabalhoCod_Internalname, "Title", edtAmbienteTecnologico_AreaTrabalhoCod_Title);
         edtAmbienteTecnologico_AreaTrabalhoDes_Titleformat = 2;
         edtAmbienteTecnologico_AreaTrabalhoDes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�rea de Trabalho", AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_AreaTrabalhoDes_Internalname, "Title", edtAmbienteTecnologico_AreaTrabalhoDes_Title);
         chkAmbienteTecnologico_Ativo_Titleformat = 2;
         chkAmbienteTecnologico_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "Title", chkAmbienteTecnologico_Ativo.Title.Text);
         AV51GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridCurrentPage), 10, 0)));
         AV52GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34AmbienteTecnologico_DescricaoTitleFilterData", AV34AmbienteTecnologico_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData", AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData", AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46AmbienteTecnologico_AtivoTitleFilterData", AV46AmbienteTecnologico_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E119V2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV50PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV50PageToGo) ;
         }
      }

      protected void E129V2( )
      {
         /* Ddo_ambientetecnologico_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFAmbienteTecnologico_Descricao = Ddo_ambientetecnologico_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAmbienteTecnologico_Descricao", AV35TFAmbienteTecnologico_Descricao);
            AV36TFAmbienteTecnologico_Descricao_Sel = Ddo_ambientetecnologico_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAmbienteTecnologico_Descricao_Sel", AV36TFAmbienteTecnologico_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E139V2( )
      {
         /* Ddo_ambientetecnologico_areatrabalhocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFAmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFAmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            AV40TFAmbienteTecnologico_AreaTrabalhoCod_To = (int)(NumberUtil.Val( Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFAmbienteTecnologico_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E149V2( )
      {
         /* Ddo_ambientetecnologico_areatrabalhodes_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFAmbienteTecnologico_AreaTrabalhoDes = Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAmbienteTecnologico_AreaTrabalhoDes", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
            AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel = Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel", AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E159V2( )
      {
         /* Ddo_ambientetecnologico_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( Ddo_ambientetecnologico_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E289V2( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV55Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 69;
         }
         sendrow_692( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_69_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(69, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E299V2 */
         E299V2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E299V2( )
      {
         /* Enter Routine */
         AV7InOutAmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAmbienteTecnologico_Codigo), 6, 0)));
         AV8InOutAmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAmbienteTecnologico_Descricao", AV8InOutAmbienteTecnologico_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutAmbienteTecnologico_Codigo,(String)AV8InOutAmbienteTecnologico_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E169V2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E219V2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E179V2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E229V2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E239V2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E189V2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E249V2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E199V2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV33AmbienteTecnologico_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17AmbienteTecnologico_Descricao1, AV20DynamicFiltersSelector2, AV22AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV35TFAmbienteTecnologico_Descricao, AV36TFAmbienteTecnologico_Descricao_Sel, AV39TFAmbienteTecnologico_AreaTrabalhoCod, AV40TFAmbienteTecnologico_AreaTrabalhoCod_To, AV43TFAmbienteTecnologico_AreaTrabalhoDes, AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel, AV47TFAmbienteTecnologico_Ativo_Sel, AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace, AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace, AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV56Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E259V2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E209V2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavAmbientetecnologico_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAmbientetecnologico_areatrabalhocod_Internalname, "Values", dynavAmbientetecnologico_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus);
         Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus);
         Ddo_ambientetecnologico_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_ambientetecnologico_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "SortedStatus", Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_ambientetecnologico_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAmbientetecnologico_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAmbientetecnologico_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavAmbientetecnologico_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV22AmbienteTecnologico_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22AmbienteTecnologico_Descricao2", AV22AmbienteTecnologico_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV27AmbienteTecnologico_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV33AmbienteTecnologico_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         AV35TFAmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAmbienteTecnologico_Descricao", AV35TFAmbienteTecnologico_Descricao);
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
         AV36TFAmbienteTecnologico_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFAmbienteTecnologico_Descricao_Sel", AV36TFAmbienteTecnologico_Descricao_Sel);
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
         AV39TFAmbienteTecnologico_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFAmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "FilteredText_set", Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set);
         AV40TFAmbienteTecnologico_AreaTrabalhoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFAmbienteTecnologico_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0)));
         Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhocod_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set);
         AV43TFAmbienteTecnologico_AreaTrabalhoDes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAmbienteTecnologico_AreaTrabalhoDes", AV43TFAmbienteTecnologico_AreaTrabalhoDes);
         Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "FilteredText_set", Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set);
         AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel", AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel);
         Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_areatrabalhodes_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set);
         AV47TFAmbienteTecnologico_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0));
         Ddo_ambientetecnologico_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17AmbienteTecnologico_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AmbienteTecnologico_Descricao1", AV17AmbienteTecnologico_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV17AmbienteTecnologico_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AmbienteTecnologico_Descricao1", AV17AmbienteTecnologico_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
               {
                  AV22AmbienteTecnologico_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22AmbienteTecnologico_Descricao2", AV22AmbienteTecnologico_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
                  {
                     AV27AmbienteTecnologico_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV33AmbienteTecnologico_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "AMBIENTETECNOLOGICO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFAmbienteTecnologico_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFAmbienteTecnologico_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFAmbienteTecnologico_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFAmbienteTecnologico_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV39TFAmbienteTecnologico_AreaTrabalhoCod) && (0==AV40TFAmbienteTecnologico_AreaTrabalhoCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV39TFAmbienteTecnologico_AreaTrabalhoCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV40TFAmbienteTecnologico_AreaTrabalhoCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFAmbienteTecnologico_AreaTrabalhoDes)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_AREATRABALHODES";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFAmbienteTecnologico_AreaTrabalhoDes;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV47TFAmbienteTecnologico_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFAmbienteTecnologico_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV56Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AmbienteTecnologico_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17AmbienteTecnologico_Descricao1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22AmbienteTecnologico_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22AmbienteTecnologico_Descricao2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27AmbienteTecnologico_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27AmbienteTecnologico_Descricao3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_9V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_9V2( true) ;
         }
         else
         {
            wb_table2_5_9V2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_9V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_63_9V2( true) ;
         }
         else
         {
            wb_table3_63_9V2( false) ;
         }
         return  ;
      }

      protected void wb_table3_63_9V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9V2e( true) ;
         }
         else
         {
            wb_table1_2_9V2e( false) ;
         }
      }

      protected void wb_table3_63_9V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_66_9V2( true) ;
         }
         else
         {
            wb_table4_66_9V2( false) ;
         }
         return  ;
      }

      protected void wb_table4_66_9V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_63_9V2e( true) ;
         }
         else
         {
            wb_table3_63_9V2e( false) ;
         }
      }

      protected void wb_table4_66_9V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"69\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tecnol�gico") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_AreaTrabalhoDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_AreaTrabalhoDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_AreaTrabalhoDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAmbienteTecnologico_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAmbienteTecnologico_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAmbienteTecnologico_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A352AmbienteTecnologico_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A729AmbienteTecnologico_AreaTrabalhoDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_AreaTrabalhoDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_AreaTrabalhoDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAmbienteTecnologico_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAmbienteTecnologico_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 69 )
         {
            wbEnd = 0;
            nRC_GXsfl_69 = (short)(nGXsfl_69_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_66_9V2e( true) ;
         }
         else
         {
            wb_table4_66_9V2e( false) ;
         }
      }

      protected void wb_table2_5_9V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptAmbienteTecnologico.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_9V2( true) ;
         }
         else
         {
            wb_table5_14_9V2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_9V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_9V2e( true) ;
         }
         else
         {
            wb_table2_5_9V2e( false) ;
         }
      }

      protected void wb_table5_14_9V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextambientetecnologico_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextambientetecnologico_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAmbientetecnologico_areatrabalhocod, dynavAmbientetecnologico_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0)), 1, dynavAmbientetecnologico_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_PromptAmbienteTecnologico.htm");
            dynavAmbientetecnologico_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33AmbienteTecnologico_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAmbientetecnologico_areatrabalhocod_Internalname, "Values", (String)(dynavAmbientetecnologico_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_9V2( true) ;
         }
         else
         {
            wb_table6_23_9V2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_9V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_9V2e( true) ;
         }
         else
         {
            wb_table5_14_9V2e( false) ;
         }
      }

      protected void wb_table6_23_9V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptAmbienteTecnologico.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao1_Internalname, AV17AmbienteTecnologico_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17AmbienteTecnologico_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAmbienteTecnologico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_PromptAmbienteTecnologico.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao2_Internalname, AV22AmbienteTecnologico_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV22AmbienteTecnologico_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAmbienteTecnologico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptAmbienteTecnologico.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao3_Internalname, AV27AmbienteTecnologico_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV27AmbienteTecnologico_Descricao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_9V2e( true) ;
         }
         else
         {
            wb_table6_23_9V2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutAmbienteTecnologico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutAmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutAmbienteTecnologico_Codigo), 6, 0)));
         AV8InOutAmbienteTecnologico_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutAmbienteTecnologico_Descricao", AV8InOutAmbienteTecnologico_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9V2( ) ;
         WS9V2( ) ;
         WE9V2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623573435");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptambientetecnologico.js", "?20205623573435");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_idx;
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO_"+sGXsfl_69_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_69_idx;
         edtAmbienteTecnologico_AreaTrabalhoCod_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHOCOD_"+sGXsfl_69_idx;
         edtAmbienteTecnologico_AreaTrabalhoDes_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHODES_"+sGXsfl_69_idx;
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO_"+sGXsfl_69_idx;
      }

      protected void SubsflControlProps_fel_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_fel_idx;
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO_"+sGXsfl_69_fel_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_69_fel_idx;
         edtAmbienteTecnologico_AreaTrabalhoCod_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHOCOD_"+sGXsfl_69_fel_idx;
         edtAmbienteTecnologico_AreaTrabalhoDes_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHODES_"+sGXsfl_69_fel_idx;
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO_"+sGXsfl_69_fel_idx;
      }

      protected void sendrow_692( )
      {
         SubsflControlProps_692( ) ;
         WB9V0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_69_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_69_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_69_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 70,'',false,'',69)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV55Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV55Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_69_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Descricao_Internalname,(String)A352AmbienteTecnologico_Descricao,StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_AreaTrabalhoDes_Internalname,(String)A729AmbienteTecnologico_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A729AmbienteTecnologico_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAmbienteTecnologico_Ativo_Internalname,StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_CODIGO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_AREATRABALHOCOD"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_ATIVO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, A353AmbienteTecnologico_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         /* End function sendrow_692 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextambientetecnologico_areatrabalhocod_Internalname = "FILTERTEXTAMBIENTETECNOLOGICO_AREATRABALHOCOD";
         dynavAmbientetecnologico_areatrabalhocod_Internalname = "vAMBIENTETECNOLOGICO_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAmbientetecnologico_descricao1_Internalname = "vAMBIENTETECNOLOGICO_DESCRICAO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAmbientetecnologico_descricao2_Internalname = "vAMBIENTETECNOLOGICO_DESCRICAO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAmbientetecnologico_descricao3_Internalname = "vAMBIENTETECNOLOGICO_DESCRICAO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO";
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO";
         edtAmbienteTecnologico_AreaTrabalhoCod_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHOCOD";
         edtAmbienteTecnologico_AreaTrabalhoDes_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHODES";
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfambientetecnologico_descricao_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO";
         edtavTfambientetecnologico_descricao_sel_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
         edtavTfambientetecnologico_areatrabalhocod_Internalname = "vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD";
         edtavTfambientetecnologico_areatrabalhocod_to_Internalname = "vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO";
         edtavTfambientetecnologico_areatrabalhodes_Internalname = "vTFAMBIENTETECNOLOGICO_AREATRABALHODES";
         edtavTfambientetecnologico_areatrabalhodes_sel_Internalname = "vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL";
         edtavTfambientetecnologico_ativo_sel_Internalname = "vTFAMBIENTETECNOLOGICO_ATIVO_SEL";
         Ddo_ambientetecnologico_descricao_Internalname = "DDO_AMBIENTETECNOLOGICO_DESCRICAO";
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_areatrabalhocod_Internalname = "DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD";
         edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_areatrabalhodes_Internalname = "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES";
         edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_ativo_Internalname = "DDO_AMBIENTETECNOLOGICO_ATIVO";
         edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAmbienteTecnologico_AreaTrabalhoDes_Jsonclick = "";
         edtAmbienteTecnologico_AreaTrabalhoCod_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         edtAmbienteTecnologico_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavAmbientetecnologico_descricao3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavAmbientetecnologico_descricao2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavAmbientetecnologico_descricao1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavAmbientetecnologico_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkAmbienteTecnologico_Ativo_Titleformat = 0;
         edtAmbienteTecnologico_AreaTrabalhoDes_Titleformat = 0;
         edtAmbienteTecnologico_AreaTrabalhoCod_Titleformat = 0;
         edtAmbienteTecnologico_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavAmbientetecnologico_descricao3_Visible = 1;
         edtavAmbientetecnologico_descricao2_Visible = 1;
         edtavAmbientetecnologico_descricao1_Visible = 1;
         chkAmbienteTecnologico_Ativo.Title.Text = "Ativo";
         edtAmbienteTecnologico_AreaTrabalhoDes_Title = "�rea de Trabalho";
         edtAmbienteTecnologico_AreaTrabalhoCod_Title = "�rea de Trabalho";
         edtAmbienteTecnologico_Descricao_Title = "Tecnol�gico";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkAmbienteTecnologico_Ativo.Caption = "";
         edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfambientetecnologico_ativo_sel_Jsonclick = "";
         edtavTfambientetecnologico_ativo_sel_Visible = 1;
         edtavTfambientetecnologico_areatrabalhodes_sel_Jsonclick = "";
         edtavTfambientetecnologico_areatrabalhodes_sel_Visible = 1;
         edtavTfambientetecnologico_areatrabalhodes_Jsonclick = "";
         edtavTfambientetecnologico_areatrabalhodes_Visible = 1;
         edtavTfambientetecnologico_areatrabalhocod_to_Jsonclick = "";
         edtavTfambientetecnologico_areatrabalhocod_to_Visible = 1;
         edtavTfambientetecnologico_areatrabalhocod_Jsonclick = "";
         edtavTfambientetecnologico_areatrabalhocod_Visible = 1;
         edtavTfambientetecnologico_descricao_sel_Jsonclick = "";
         edtavTfambientetecnologico_descricao_sel_Visible = 1;
         edtavTfambientetecnologico_descricao_Jsonclick = "";
         edtavTfambientetecnologico_descricao_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_ambientetecnologico_ativo_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_ativo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_ativo_Rangefilterto = "At�";
         Ddo_ambientetecnologico_ativo_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_ativo_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_ativo_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_ambientetecnologico_ativo_Datalisttype = "FixedValues";
         Ddo_ambientetecnologico_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_ativo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_ativo_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_ativo_Tooltip = "Op��es";
         Ddo_ambientetecnologico_ativo_Caption = "";
         Ddo_ambientetecnologico_areatrabalhodes_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_areatrabalhodes_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_areatrabalhodes_Rangefilterto = "At�";
         Ddo_ambientetecnologico_areatrabalhodes_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_areatrabalhodes_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_areatrabalhodes_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_areatrabalhodes_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_areatrabalhodes_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_areatrabalhodes_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_areatrabalhodes_Datalistproc = "GetPromptAmbienteTecnologicoFilterData";
         Ddo_ambientetecnologico_areatrabalhodes_Datalistfixedvalues = "";
         Ddo_ambientetecnologico_areatrabalhodes_Datalisttype = "Dynamic";
         Ddo_ambientetecnologico_areatrabalhodes_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhodes_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_areatrabalhodes_Filtertype = "Character";
         Ddo_ambientetecnologico_areatrabalhodes_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhodes_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhodes_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_areatrabalhodes_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_areatrabalhodes_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_areatrabalhodes_Tooltip = "Op��es";
         Ddo_ambientetecnologico_areatrabalhodes_Caption = "";
         Ddo_ambientetecnologico_areatrabalhocod_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_areatrabalhocod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_areatrabalhocod_Rangefilterto = "At�";
         Ddo_ambientetecnologico_areatrabalhocod_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_areatrabalhocod_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_areatrabalhocod_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_areatrabalhocod_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_areatrabalhocod_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_areatrabalhocod_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_areatrabalhocod_Datalistfixedvalues = "";
         Ddo_ambientetecnologico_areatrabalhocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_areatrabalhocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhocod_Filtertype = "Numeric";
         Ddo_ambientetecnologico_areatrabalhocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_areatrabalhocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_areatrabalhocod_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_areatrabalhocod_Tooltip = "Op��es";
         Ddo_ambientetecnologico_areatrabalhocod_Caption = "";
         Ddo_ambientetecnologico_descricao_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_descricao_Rangefilterto = "At�";
         Ddo_ambientetecnologico_descricao_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_descricao_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_descricao_Datalistproc = "GetPromptAmbienteTecnologicoFilterData";
         Ddo_ambientetecnologico_descricao_Datalistfixedvalues = "";
         Ddo_ambientetecnologico_descricao_Datalisttype = "Dynamic";
         Ddo_ambientetecnologico_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_descricao_Filtertype = "Character";
         Ddo_ambientetecnologico_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_descricao_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_descricao_Tooltip = "Op��es";
         Ddo_ambientetecnologico_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Ambiente Tecnologico";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''}],oparms:[{av:'AV34AmbienteTecnologico_DescricaoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData',fld:'vAMBIENTETECNOLOGICO_AREATRABALHODESTITLEFILTERDATA',pic:'',nv:null},{av:'AV46AmbienteTecnologico_AtivoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtAmbienteTecnologico_Descricao_Titleformat',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Titleformat'},{av:'edtAmbienteTecnologico_Descricao_Title',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Title'},{av:'edtAmbienteTecnologico_AreaTrabalhoCod_Titleformat',ctrl:'AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtAmbienteTecnologico_AreaTrabalhoCod_Title',ctrl:'AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'Title'},{av:'edtAmbienteTecnologico_AreaTrabalhoDes_Titleformat',ctrl:'AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'Titleformat'},{av:'edtAmbienteTecnologico_AreaTrabalhoDes_Title',ctrl:'AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'Title'},{av:'chkAmbienteTecnologico_Ativo_Titleformat',ctrl:'AMBIENTETECNOLOGICO_ATIVO',prop:'Titleformat'},{av:'chkAmbienteTecnologico_Ativo.Title.Text',ctrl:'AMBIENTETECNOLOGICO_ATIVO',prop:'Title'},{av:'AV51GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV52GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED","{handler:'E129V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_ambientetecnologico_descricao_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD.ONOPTIONCLICKED","{handler:'E139V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_get',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_AREATRABALHODES.ONOPTIONCLICKED","{handler:'E149V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'SortedStatus'},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_ATIVO.ONOPTIONCLICKED","{handler:'E159V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_ambientetecnologico_ativo_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_ativo_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E289V2',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E299V2',iparms:[{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A352AmbienteTecnologico_Descricao',fld:'AMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutAmbienteTecnologico_Codigo',fld:'vINOUTAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutAmbienteTecnologico_Descricao',fld:'vINOUTAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E169V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E219V2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E179V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E229V2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E239V2',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E189V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E249V2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E199V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E259V2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E209V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_set'},{av:'AV36TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV39TFAmbienteTecnologico_AreaTrabalhoCod',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'FilteredText_set'},{av:'AV40TFAmbienteTecnologico_AreaTrabalhoCod_To',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHOCOD',prop:'FilteredTextTo_set'},{av:'AV43TFAmbienteTecnologico_AreaTrabalhoDes',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'FilteredText_set'},{av:'AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel',fld:'vTFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_AREATRABALHODES',prop:'SelectedValue_set'},{av:'AV47TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_ambientetecnologico_ativo_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutAmbienteTecnologico_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_ambientetecnologico_descricao_Activeeventkey = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_get = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_get = "";
         Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey = "";
         Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_get = "";
         Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_get = "";
         Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey = "";
         Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_get = "";
         Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_get = "";
         Ddo_ambientetecnologico_ativo_Activeeventkey = "";
         Ddo_ambientetecnologico_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17AmbienteTecnologico_Descricao1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22AmbienteTecnologico_Descricao2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27AmbienteTecnologico_Descricao3 = "";
         AV35TFAmbienteTecnologico_Descricao = "";
         AV36TFAmbienteTecnologico_Descricao_Sel = "";
         AV43TFAmbienteTecnologico_AreaTrabalhoDes = "";
         AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel = "";
         AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = "";
         AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace = "";
         AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace = "";
         AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = "";
         AV56Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV49DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46AmbienteTecnologico_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set = "";
         Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set = "";
         Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus = "";
         Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set = "";
         Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set = "";
         Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus = "";
         Ddo_ambientetecnologico_ativo_Selectedvalue_set = "";
         Ddo_ambientetecnologico_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV55Select_GXI = "";
         A352AmbienteTecnologico_Descricao = "";
         A729AmbienteTecnologico_AreaTrabalhoDes = "";
         scmdbuf = "";
         H009V2_A5AreaTrabalho_Codigo = new int[1] ;
         H009V2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H009V3_A5AreaTrabalho_Codigo = new int[1] ;
         H009V3_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV17AmbienteTecnologico_Descricao1 = "";
         lV22AmbienteTecnologico_Descricao2 = "";
         lV27AmbienteTecnologico_Descricao3 = "";
         lV35TFAmbienteTecnologico_Descricao = "";
         lV43TFAmbienteTecnologico_AreaTrabalhoDes = "";
         H009V4_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         H009V4_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         H009V4_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         H009V4_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         H009V4_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H009V4_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009V5_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextambientetecnologico_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptambientetecnologico__default(),
            new Object[][] {
                new Object[] {
               H009V2_A5AreaTrabalho_Codigo, H009V2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H009V3_A5AreaTrabalho_Codigo, H009V3_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H009V4_A353AmbienteTecnologico_Ativo, H009V4_A729AmbienteTecnologico_AreaTrabalhoDes, H009V4_n729AmbienteTecnologico_AreaTrabalhoDes, H009V4_A728AmbienteTecnologico_AreaTrabalhoCod, H009V4_A352AmbienteTecnologico_Descricao, H009V4_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               H009V5_AGRID_nRecordCount
               }
            }
         );
         AV56Pgmname = "PromptAmbienteTecnologico";
         /* GeneXus formulas. */
         AV56Pgmname = "PromptAmbienteTecnologico";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_69 ;
      private short nGXsfl_69_idx=1 ;
      private short AV13OrderedBy ;
      private short AV47TFAmbienteTecnologico_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_69_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAmbienteTecnologico_Descricao_Titleformat ;
      private short edtAmbienteTecnologico_AreaTrabalhoCod_Titleformat ;
      private short edtAmbienteTecnologico_AreaTrabalhoDes_Titleformat ;
      private short chkAmbienteTecnologico_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutAmbienteTecnologico_Codigo ;
      private int wcpOAV7InOutAmbienteTecnologico_Codigo ;
      private int subGrid_Rows ;
      private int AV33AmbienteTecnologico_AreaTrabalhoCod ;
      private int AV39TFAmbienteTecnologico_AreaTrabalhoCod ;
      private int AV40TFAmbienteTecnologico_AreaTrabalhoCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_ambientetecnologico_areatrabalhocod_Datalistupdateminimumcharacters ;
      private int Ddo_ambientetecnologico_areatrabalhodes_Datalistupdateminimumcharacters ;
      private int Ddo_ambientetecnologico_ativo_Datalistupdateminimumcharacters ;
      private int edtavTfambientetecnologico_descricao_Visible ;
      private int edtavTfambientetecnologico_descricao_sel_Visible ;
      private int edtavTfambientetecnologico_areatrabalhocod_Visible ;
      private int edtavTfambientetecnologico_areatrabalhocod_to_Visible ;
      private int edtavTfambientetecnologico_areatrabalhodes_Visible ;
      private int edtavTfambientetecnologico_areatrabalhodes_sel_Visible ;
      private int edtavTfambientetecnologico_ativo_sel_Visible ;
      private int edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV50PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavAmbientetecnologico_descricao1_Visible ;
      private int edtavAmbientetecnologico_descricao2_Visible ;
      private int edtavAmbientetecnologico_descricao3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV51GridCurrentPage ;
      private long AV52GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_ambientetecnologico_descricao_Activeeventkey ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_get ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_get ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Activeeventkey ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_get ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_get ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Activeeventkey ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_get ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_get ;
      private String Ddo_ambientetecnologico_ativo_Activeeventkey ;
      private String Ddo_ambientetecnologico_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_69_idx="0001" ;
      private String AV56Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_ambientetecnologico_descricao_Caption ;
      private String Ddo_ambientetecnologico_descricao_Tooltip ;
      private String Ddo_ambientetecnologico_descricao_Cls ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_set ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_descricao_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_descricao_Sortedstatus ;
      private String Ddo_ambientetecnologico_descricao_Filtertype ;
      private String Ddo_ambientetecnologico_descricao_Datalisttype ;
      private String Ddo_ambientetecnologico_descricao_Datalistfixedvalues ;
      private String Ddo_ambientetecnologico_descricao_Datalistproc ;
      private String Ddo_ambientetecnologico_descricao_Sortasc ;
      private String Ddo_ambientetecnologico_descricao_Sortdsc ;
      private String Ddo_ambientetecnologico_descricao_Loadingdata ;
      private String Ddo_ambientetecnologico_descricao_Cleanfilter ;
      private String Ddo_ambientetecnologico_descricao_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_descricao_Rangefilterto ;
      private String Ddo_ambientetecnologico_descricao_Noresultsfound ;
      private String Ddo_ambientetecnologico_descricao_Searchbuttontext ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Caption ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Tooltip ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Cls ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Filteredtext_set ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Filteredtextto_set ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Sortedstatus ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Filtertype ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Datalistfixedvalues ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Sortasc ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Sortdsc ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Loadingdata ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Cleanfilter ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Rangefilterto ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Noresultsfound ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Searchbuttontext ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Caption ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Tooltip ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Cls ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Filteredtext_set ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Sortedstatus ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Filtertype ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Datalisttype ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Datalistfixedvalues ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Datalistproc ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Sortasc ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Sortdsc ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Loadingdata ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Cleanfilter ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Rangefilterto ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Noresultsfound ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Searchbuttontext ;
      private String Ddo_ambientetecnologico_ativo_Caption ;
      private String Ddo_ambientetecnologico_ativo_Tooltip ;
      private String Ddo_ambientetecnologico_ativo_Cls ;
      private String Ddo_ambientetecnologico_ativo_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_ativo_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_ativo_Sortedstatus ;
      private String Ddo_ambientetecnologico_ativo_Datalisttype ;
      private String Ddo_ambientetecnologico_ativo_Datalistfixedvalues ;
      private String Ddo_ambientetecnologico_ativo_Sortasc ;
      private String Ddo_ambientetecnologico_ativo_Sortdsc ;
      private String Ddo_ambientetecnologico_ativo_Loadingdata ;
      private String Ddo_ambientetecnologico_ativo_Cleanfilter ;
      private String Ddo_ambientetecnologico_ativo_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_ativo_Rangefilterto ;
      private String Ddo_ambientetecnologico_ativo_Noresultsfound ;
      private String Ddo_ambientetecnologico_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfambientetecnologico_descricao_Internalname ;
      private String edtavTfambientetecnologico_descricao_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_sel_Internalname ;
      private String edtavTfambientetecnologico_descricao_sel_Jsonclick ;
      private String edtavTfambientetecnologico_areatrabalhocod_Internalname ;
      private String edtavTfambientetecnologico_areatrabalhocod_Jsonclick ;
      private String edtavTfambientetecnologico_areatrabalhocod_to_Internalname ;
      private String edtavTfambientetecnologico_areatrabalhocod_to_Jsonclick ;
      private String edtavTfambientetecnologico_areatrabalhodes_Internalname ;
      private String edtavTfambientetecnologico_areatrabalhodes_Jsonclick ;
      private String edtavTfambientetecnologico_areatrabalhodes_sel_Internalname ;
      private String edtavTfambientetecnologico_areatrabalhodes_sel_Jsonclick ;
      private String edtavTfambientetecnologico_ativo_sel_Internalname ;
      private String edtavTfambientetecnologico_ativo_sel_Jsonclick ;
      private String edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_areatrabalhocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_areatrabalhodestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtAmbienteTecnologico_AreaTrabalhoCod_Internalname ;
      private String edtAmbienteTecnologico_AreaTrabalhoDes_Internalname ;
      private String chkAmbienteTecnologico_Ativo_Internalname ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavAmbientetecnologico_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAmbientetecnologico_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAmbientetecnologico_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAmbientetecnologico_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_ambientetecnologico_descricao_Internalname ;
      private String Ddo_ambientetecnologico_areatrabalhocod_Internalname ;
      private String Ddo_ambientetecnologico_areatrabalhodes_Internalname ;
      private String Ddo_ambientetecnologico_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Title ;
      private String edtAmbienteTecnologico_AreaTrabalhoCod_Title ;
      private String edtAmbienteTecnologico_AreaTrabalhoDes_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextambientetecnologico_areatrabalhocod_Internalname ;
      private String lblFiltertextambientetecnologico_areatrabalhocod_Jsonclick ;
      private String dynavAmbientetecnologico_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavAmbientetecnologico_descricao1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavAmbientetecnologico_descricao2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAmbientetecnologico_descricao3_Jsonclick ;
      private String sGXsfl_69_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtAmbienteTecnologico_Codigo_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String edtAmbienteTecnologico_AreaTrabalhoCod_Jsonclick ;
      private String edtAmbienteTecnologico_AreaTrabalhoDes_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_ambientetecnologico_descricao_Includesortasc ;
      private bool Ddo_ambientetecnologico_descricao_Includesortdsc ;
      private bool Ddo_ambientetecnologico_descricao_Includefilter ;
      private bool Ddo_ambientetecnologico_descricao_Filterisrange ;
      private bool Ddo_ambientetecnologico_descricao_Includedatalist ;
      private bool Ddo_ambientetecnologico_areatrabalhocod_Includesortasc ;
      private bool Ddo_ambientetecnologico_areatrabalhocod_Includesortdsc ;
      private bool Ddo_ambientetecnologico_areatrabalhocod_Includefilter ;
      private bool Ddo_ambientetecnologico_areatrabalhocod_Filterisrange ;
      private bool Ddo_ambientetecnologico_areatrabalhocod_Includedatalist ;
      private bool Ddo_ambientetecnologico_areatrabalhodes_Includesortasc ;
      private bool Ddo_ambientetecnologico_areatrabalhodes_Includesortdsc ;
      private bool Ddo_ambientetecnologico_areatrabalhodes_Includefilter ;
      private bool Ddo_ambientetecnologico_areatrabalhodes_Filterisrange ;
      private bool Ddo_ambientetecnologico_areatrabalhodes_Includedatalist ;
      private bool Ddo_ambientetecnologico_ativo_Includesortasc ;
      private bool Ddo_ambientetecnologico_ativo_Includesortdsc ;
      private bool Ddo_ambientetecnologico_ativo_Includefilter ;
      private bool Ddo_ambientetecnologico_ativo_Filterisrange ;
      private bool Ddo_ambientetecnologico_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool A353AmbienteTecnologico_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV8InOutAmbienteTecnologico_Descricao ;
      private String wcpOAV8InOutAmbienteTecnologico_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17AmbienteTecnologico_Descricao1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV22AmbienteTecnologico_Descricao2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV27AmbienteTecnologico_Descricao3 ;
      private String AV35TFAmbienteTecnologico_Descricao ;
      private String AV36TFAmbienteTecnologico_Descricao_Sel ;
      private String AV43TFAmbienteTecnologico_AreaTrabalhoDes ;
      private String AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel ;
      private String AV37ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ;
      private String AV41ddo_AmbienteTecnologico_AreaTrabalhoCodTitleControlIdToReplace ;
      private String AV45ddo_AmbienteTecnologico_AreaTrabalhoDesTitleControlIdToReplace ;
      private String AV48ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace ;
      private String AV55Select_GXI ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A729AmbienteTecnologico_AreaTrabalhoDes ;
      private String lV17AmbienteTecnologico_Descricao1 ;
      private String lV22AmbienteTecnologico_Descricao2 ;
      private String lV27AmbienteTecnologico_Descricao3 ;
      private String lV35TFAmbienteTecnologico_Descricao ;
      private String lV43TFAmbienteTecnologico_AreaTrabalhoDes ;
      private String AV31Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutAmbienteTecnologico_Codigo ;
      private String aP1_InOutAmbienteTecnologico_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavAmbientetecnologico_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkAmbienteTecnologico_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H009V2_A5AreaTrabalho_Codigo ;
      private String[] H009V2_A6AreaTrabalho_Descricao ;
      private int[] H009V3_A5AreaTrabalho_Codigo ;
      private String[] H009V3_A6AreaTrabalho_Descricao ;
      private bool[] H009V4_A353AmbienteTecnologico_Ativo ;
      private String[] H009V4_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] H009V4_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private int[] H009V4_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] H009V4_A352AmbienteTecnologico_Descricao ;
      private int[] H009V4_A351AmbienteTecnologico_Codigo ;
      private long[] H009V5_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34AmbienteTecnologico_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38AmbienteTecnologico_AreaTrabalhoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42AmbienteTecnologico_AreaTrabalhoDesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46AmbienteTecnologico_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV49DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptambientetecnologico__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009V4( IGxContext context ,
                                             int AV33AmbienteTecnologico_AreaTrabalhoCod ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV17AmbienteTecnologico_Descricao1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV22AmbienteTecnologico_Descricao2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             String AV27AmbienteTecnologico_Descricao3 ,
                                             String AV36TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV35TFAmbienteTecnologico_Descricao ,
                                             int AV39TFAmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV40TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                             String AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                             String AV43TFAmbienteTecnologico_AreaTrabalhoDes ,
                                             short AV47TFAmbienteTecnologico_Ativo_Sel ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A729AmbienteTecnologico_AreaTrabalhoDes ,
                                             bool A353AmbienteTecnologico_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[AmbienteTecnologico_Ativo], T2.[AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes, T1.[AmbienteTecnologico_AreaTrabalhoCod] AS AmbienteTecnologico_AreaTrabalhoCod, T1.[AmbienteTecnologico_Descricao], T1.[AmbienteTecnologico_Codigo]";
         sFromString = " FROM ([AmbienteTecnologico] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[AmbienteTecnologico_AreaTrabalhoCod])";
         sOrderString = "";
         if ( ! (0==AV33AmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV33AmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV33AmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AmbienteTecnologico_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV17AmbienteTecnologico_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV17AmbienteTecnologico_Descricao1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22AmbienteTecnologico_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV22AmbienteTecnologico_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV22AmbienteTecnologico_Descricao2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27AmbienteTecnologico_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV27AmbienteTecnologico_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV27AmbienteTecnologico_Descricao3)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFAmbienteTecnologico_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like @lV35TFAmbienteTecnologico_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like @lV35TFAmbienteTecnologico_Descricao)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFAmbienteTecnologico_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] = @AV36TFAmbienteTecnologico_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] = @AV36TFAmbienteTecnologico_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV39TFAmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV39TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV39TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV40TFAmbienteTecnologico_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV40TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV40TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFAmbienteTecnologico_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV43TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV43TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] = @AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV47TFAmbienteTecnologico_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
         }
         if ( AV47TFAmbienteTecnologico_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AmbienteTecnologico_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H009V5( IGxContext context ,
                                             int AV33AmbienteTecnologico_AreaTrabalhoCod ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV17AmbienteTecnologico_Descricao1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV22AmbienteTecnologico_Descricao2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             String AV27AmbienteTecnologico_Descricao3 ,
                                             String AV36TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV35TFAmbienteTecnologico_Descricao ,
                                             int AV39TFAmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV40TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                             String AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                             String AV43TFAmbienteTecnologico_AreaTrabalhoDes ,
                                             short AV47TFAmbienteTecnologico_Ativo_Sel ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A729AmbienteTecnologico_AreaTrabalhoDes ,
                                             bool A353AmbienteTecnologico_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([AmbienteTecnologico] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[AmbienteTecnologico_AreaTrabalhoCod])";
         if ( ! (0==AV33AmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV33AmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV33AmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AmbienteTecnologico_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV17AmbienteTecnologico_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV17AmbienteTecnologico_Descricao1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22AmbienteTecnologico_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV22AmbienteTecnologico_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV22AmbienteTecnologico_Descricao2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27AmbienteTecnologico_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV27AmbienteTecnologico_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV27AmbienteTecnologico_Descricao3)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFAmbienteTecnologico_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like @lV35TFAmbienteTecnologico_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like @lV35TFAmbienteTecnologico_Descricao)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFAmbienteTecnologico_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] = @AV36TFAmbienteTecnologico_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] = @AV36TFAmbienteTecnologico_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV39TFAmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV39TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV39TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV40TFAmbienteTecnologico_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV40TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV40TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFAmbienteTecnologico_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV43TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV43TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] = @AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV47TFAmbienteTecnologico_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
         }
         if ( AV47TFAmbienteTecnologico_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H009V4(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 3 :
                     return conditional_H009V5(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009V2 ;
          prmH009V2 = new Object[] {
          } ;
          Object[] prmH009V3 ;
          prmH009V3 = new Object[] {
          } ;
          Object[] prmH009V4 ;
          prmH009V4 = new Object[] {
          new Object[] {"@AV33AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV22AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV27AmbienteTecnologico_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV36TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV39TFAmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFAmbienteTecnologico_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV43TFAmbienteTecnologico_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH009V5 ;
          prmH009V5 = new Object[] {
          new Object[] {"@AV33AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV22AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV27AmbienteTecnologico_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV36TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV39TFAmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFAmbienteTecnologico_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV43TFAmbienteTecnologico_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV44TFAmbienteTecnologico_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009V2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009V2,0,0,true,false )
             ,new CursorDef("H009V3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009V3,0,0,true,false )
             ,new CursorDef("H009V4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009V4,11,0,true,false )
             ,new CursorDef("H009V5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009V5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
